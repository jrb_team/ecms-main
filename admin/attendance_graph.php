<?php
		include_once('../db/createdb.php');
		include_once('../db/attendance_class.php');
		$dbobject = new DB();
		$Attendance = new Attendance();
		$dbobject->getCon();
	
if(!isset($_GET['acyear'])){
    $acyear=$dbobject->get_acyear();
    
}else{
    $acyear=$_GET['acyear'];
 }
$status=$_GET['status'];
$type=$_GET['type'];
if(!isset($_GET['period'])){
    $period="today";
}else{
    $period=$_GET['period'];
}

$period_type=$_GET['period_type'];
$period_type2=$_GET['period_type2'];
if(!isset($_GET['period'])){
    $seltype="School_wise";
}else{
    $seltype=$_GET['seltype'];
}
$seltype2=$_GET['seltype2'];
if($_GET['tabID']==""){
	$tabID=1;
}else{
	$tabID=$_GET['tabID'];
}

$SchoolDays=$Attendance->SchoolDays();
$working_days=$Attendance->WorkingDays($acyear,$classid);
$gender_array=array("M"=>"BOYS","F"=>"GIRLS");
$status_array=array("Present","Late","Absent");
$attendancePeriod=$Attendance->getAttendancePeriod("");
$date_from="";
$date_to="";
if($period=='today'){
    $month_row['smonth'] = date('Y-m-d');
	//echo "<br>";
	$month_row['emonth']  = date('Y-m-d');
}
elseif($period=='sem')
{
$month_row=$dbobject->sem_date($period_type,$acyear);
$date_from=$month_row['smonth'];
$date_to=$month_row['emonth'];
}elseif($period=='month'){
	$year = date("Y"); 	
	$rmonth = ucfirst($_GET['period_type']); 	
	$timestamp    = strtotime(''.$rmonth.' '.$year.'');
	$month_row['smonth'] = date('Y-m-01', $timestamp);
	//echo "<br>";
	$month_row['emonth']  = date('Y-m-t', $timestamp); // A leap year!
}elseif($period=='date'){
	$month_row['smonth'] = date('Y-m-d', strtotime($_GET['period_type']));
	//echo "<br>";
	$month_row['emonth']  = date('Y-m-d', strtotime($_GET['period_type']));
}elseif($period=="Custom"){
	$month_row['smonth'] = date('Y-m-d', strtotime($_GET['period_type']));
	//echo "<br>";
	$month_row['emonth']  = date('Y-m-d', strtotime($_GET['period_type2']));
}
else
{
$monthQry="select min(`smonth`) AS `smonth`,max(`emonth`) AS `emonth` from terms where `acyear`='".$acyear."'";
if($period=='term'){
  $monthQry.=" AND `term_name`='".$period_type."'";
}
$get_month=$dbobject->select($monthQry);
$month_row=$dbobject->fetch_array($get_month);
}
$date_from=$month_row['smonth'];
$date_to=$month_row['emonth'];
$i=0;


		

		$Present=0;
		$Absent=0;		
		$Late=0;		
		$gtot=0;				
		$gtot_=array();	
		$Present_F=0;		
		$Present_M=0;	
		$Absent_F=0;		
		$Absent_M=0;	
		$Late_F=0;		
		$Late_M=0;
        if($seltype=="School_wise"){
            $select_class=$dbobject->get_classlistByAcyear($acyear);
        }
		elseif($seltype=="section"){
			$section_det=$dbobject->selectall("sclass_section",array("section"=>$seltype2,"acyear"=>$acyear));
			$classlist=$dbobject->get_classByAcyearSection($acyear,$section_det['id']);
			$c=0;
			if(!empty($classlist)){
				foreach($classlist as $cl){
					$classList=$dbobject->get_classByClassno($cl['classno']);
					if(!empty($classList)){
						foreach($classList as $c){
							$select_class[$i]['classid']=$c['classid'];
							$i++;
						}
					}
				}
			}
		}else if($seltype=="classwise"){
			$select_class=$dbobject->get_classByClassno($seltype2);
		}else if($seltype=="homeroom"){
			$class_det=$dbobject->selectall("sclass",array("classid"=>$seltype2));
			$select_class[0]['classid']=$class_det['classid'];
		}
		else{
			$select_class=$dbobject->get_classlistByAcyear($acyear);
		}
		foreach($select_class as $row)
		{
						$classid=$row['classid'];
						$strength=$dbobject->retrieve_strength2($classid,$acyear);
						$str_classid[$classid]=$classid;
						if($strength['strength']!=0)
						{					
							$gtot=$gtot+$strength['strength'];				
							$gtot_['M']=$gtot_['M']+$strength['M'];					
							$gtot_['F']=$gtot_['F']+$strength['F'];							
						
						}	
		}
	//  $working_days=$Attendance->working_days_attendance_table("","","",$acyear);		
      $query_format=$dbobject->query_format($str_classid);
	  $working_days=$Attendance->working_days_attendance_table("date_range",$date_from,$date_to,$acyear);
      $attendance_data=$Attendance->Get_AttendanceData($acyear,$seltype,$query_format,$period,$date_from,$date_to);
	   
			$attn_Present_total=$attendance_data['Present'];
			$attn_Absent_total=$attendance_data['Absent'];
			$attn_Late_total=$attendance_data['Late'];
			$attn_total=array();
			$attn_Present_total_per=0;
			$attn_Late_total_per=0;
//----------------------------------------------------------------------------------------------------
			if(!empty($attendancePeriod)){
				foreach($attendancePeriod as $p){
					$attn_Present_total_per=$attn_Present_total_per+$attendance_data['Present_M_time']['M'][$p]+$attendance_data['Present_M_time']['F'][$p];
					$attn_Late_total_per=$attn_Late_total_per+$attendance_data['Late_M_time']['M'][$p]+$attendance_data['Late_M_time']['F'][$p];
					$attn_Absent_total_per=$attn_Absent_total_per+$attendance_data['Absent_M_time']['M'][$p]+$attendance_data['Absent_M_time']['F'][$p];

					$attn_total['M'][$p]=$attn_total['M'][$p]+$attendance_data['Present_M_time']['M'][$p]+$attendance_data['Late_M_time']['M'][$p]+$attendance_data['Absent_M_time']['M'][$p];
					$attn_total['F'][$p]=$attn_total['F'][$p]+$attendance_data['Present_M_time']['F'][$p]+$attendance_data['Late_M_time']['F'][$p]+$attendance_data['Absent_M_time']['F'][$p];
				}
			}
		


if(count($attendancePeriod)>1){
	$colsp=count($gender_array)+4;
    $colsp=$colsp+count($attendancePeriod);	
}else{
	$colsp=count($gender_array)+2;
}
$container1.="<tr>";
if(!empty($gender_array)){
	if(count($attendancePeriod)>1){
		$c_pan=count($attendancePeriod)+2;
	}else{
		$c_pan=count($attendancePeriod)+1;
	}
    foreach($gender_array as $g){
    }
    foreach($gender_array as $g){
    }
    foreach($gender_array as $g){
    }
}
$total_absent=0;
$total_absent_B=0;
$total_present_B=0;
$total_late_B=0;
$per=0;
foreach($status_array as $s){
	if(!empty($gender_array)){
		foreach($gender_array as $g=>$gender){
		if(!empty($attendancePeriod)){
			foreach($attendancePeriod as $p){
				if(!isset($attendance_data['Late_M_time'][$g][$p])){
					$attendance_data['Late_M_time'][$g][$p]=0;
				}
				if(!isset($attendance_data['Present_M_time'][$g][$p])){
					$attendance_data['Present_M_time'][$g][$p]=0;
				}
				$total_absent=$gtot_[$g]-($attendance_data['Present_M_time'][$g][$p]+$attendance_data['Late_M_time'][$g][$p]);
				if($s=="Late"){
					$per=round(($attendance_data['Late_M_time'][$g][$p]/$gtot_[$g])*100,2);
					$total_late_B=$total_late_B+$per;
				}elseif($s=="Present"){
						$per=round((($attendance_data['Present_M_time'][$g][$p]+$attendance_data['Late_M_time'][$g][$p])/$gtot_[$g])*100,2);
						$total_present_B=$total_present_B+$per;
				}else{
                        $per=round(($total_absent/$gtot_[$g])*100,2);
						$total_absent_B=$total_absent_B+$per;
					  
					}
				
			}
		}
		}
		}

}
$total_percentage=0;
if(!empty($gender_array)){
	foreach($gender_array as $g=>$gender){
	if(!empty($attendancePeriod)){
		foreach($attendancePeriod as $p){
			$total_percentage=$total_percentage+$attn_total[$g][$p];
		}
	}
}
}

$out=round(($total_present_B/4),2)."-".round(($total_absent_B/4),2)."-".$total_late_B;
//$out="0-0-0";

/*$tstr=$dbobject->get_staff_strength();
$tpresent=$attendance->getTotalAttendacStaff($date,"Present");
$tlate=$attendance->getTotalAttendacStaff($date,"Late");
$tabsent=$tstr-$tpresent-$tlate;*/
$out.="||0-0-0";
echo $out;
?>
