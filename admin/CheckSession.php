<?php
date_default_timezone_set('UTC');
require_once('authentication.php');
authenticate();
if($_SESSION['activity_time']==""){
    session_start();
     $_SESSION['activity_time']=date('Y-m-d H:i:s');
     session_write_close();
}
$start_date = new DateTime($_SESSION['activity_time']);
$since_start = $start_date->diff(new DateTime(date('Y-m-d H:i:s')));
if($since_start->i>=30){
    $res['Status']="Error";
    $res['message']="Logged out";
}else{
    session_start();
     $_SESSION['activity_time']=date('Y-m-d H:i:s');
     session_write_close();
     $res['Status']="Success";
     $res['message']="Session updated";
}
echo json_encode($res);
?>