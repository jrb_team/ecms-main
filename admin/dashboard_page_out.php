<?php
include_once('../db/createdb.php');
include_once('../db/fee_class.php');
include_once('../db/fee_report_class.php');
$dbobject = new DB();
if (!$dbobject->getCon())
{
echo "Connection Failed";
die(mysql_error());
}
else
{
    $Fee = new Fee();
    $currency=$Fee->currency();	
}
?>
<div class="row l-r-10">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-5">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="dashboard-summery-one mg-b-20 hero-example9" style="box-shadow: 0px 0px 5px 2px rgb(255 160 1 / 40%);">
                        <div class="row">
                            <div class="item-icon bg-light-yellow">
                                <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                            </div>
                            <div class="col-sm-12">
                                <div class="item-content" style="padding: 7px 0px;">
                                    <div class="item-title">Total Students</div>
                                    <!-- <div class="item-number"><span class="counter" id='student_strength'></span></div> -->
                                    <div style="color: #b6b4b4;display: inline-block;text-align: left;float: left;margin-left: 59px;">New<br> <span style="font-size: 14px;color: #111111;" id='str_new'>0</span></div>
                                    <div style="color: #b6b4b4;display: inline-block;text-align: right;">Enrolled<br> <span style="font-size: 14px;color: #111111;" id='str_enrol'>0</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="padding: 0px 10px 0px 6px;">
                    <div class="dashboard-summery-one mg-b-20 hero-example10" style="box-shadow: 0px 0px 5px 2px rgb(60 184 120 / 40%);">
                        <div class="row">
                            <div class="item-icon bg-light-green ">
                                <i class="fa fa-money" aria-hidden="true"></i>
                            </div>
                            <div class="col-sm-12">
                                <div class="item-content" style="padding: 7px 0px;">
                                    <div class="item-title">Total Fee</div>
                                    <div style="color: #b6b4b4;display: inline-block;text-align: left;float: left;margin-left: 59px;">New<br> <span style="font-size: 14px;color: #111111;"><a href="fee_collection_report.php?page=fee_collection_report.php" id="grand_total_fee_new" style="color: #111111;"><?php echo $currency.number_format(0, 2, '.', ','); ?></a></span></div>
                                    <div style="color: #b6b4b4;display: inline-block;text-align: right;">Enrolled<br> <span style="font-size: 14px;color: #111111;"><a href="fee_collection_report.php?page=fee_collection_report.php" id="grand_total_fee" style="color: #111111;"><?php echo $currency.number_format(0, 2, '.', ','); ?></a></span></div>
                                    <!-- <div class="item-number"><span class="counter" id='total_fee'>$0.00</span></div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-7">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4" style="padding: 0px 8px 0px 15px;">
                    <div class="dashboard-summery-one mg-b-20 hero-example11" style="box-shadow: 0px 0px 5px 2px rgb(63 122 252 / 40%);">
                        <div class="row">
                            <div class="item-icon bg-light-blue">
                                <i class="fa fa-suitcase" aria-hidden="true"></i>
                            </div>
                            <div class="col-sm-12">
                                <div class="item-content" style="padding: 7px 0px;">
                                    <div class="item-title">Today's Collection</div>
                                    <!-- <div class="item-number"><span class="counter" id='today_collection'>$0.00</span></div> -->
                                    <div style="color: #b6b4b4;display: inline-block;text-align: left;float: left;margin-left: 59px;">New<br> <span style="font-size: 14px;color: #111111;"><a href="fee_collection_report.php?page=fee_collection_report.php" id='today_paid_new' style="color: #05cc05;"><?php echo $currency.number_format(0, 2, '.', ','); ?></a></span></div>
                                    <div style="color: #b6b4b4;display: inline-block;text-align: right;">Enrolled<br> <span style="font-size: 14px;color: #111111;"><a href="fee_collection_report.php?page=fee_collection_report.php" id='today_paid' style="color: #05cc05;"><?php echo $currency.number_format(0, 2, '.', ','); ?></a></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4" style='cursor:pointer'>
                    <div class="dashboard-summery-one mg-b-20 hero-example12" style="box-shadow: 0px 0px 5px 2px rgb(255 2 4 / 40%);">
                        <div class="row">
                            <div class="item-icon bg-light-red">
                                <i class="fa fa-usd" aria-hidden="true"></i>
                            </div>
                            <div class="col-sm-12">
                                <div class="item-content" style="padding: 7px 0px;">
                                    <div class="item-title">Total Disc./Refund.</div>
                                    <!-- <div class="item-number"><span class="counter" id='total_discount'>$0.00</span></div> -->
                                    <div style="color: #b6b4b4;display: inline-block;text-align: left;float: left;margin-left: 59px;">New<br><a href='refund_discount.php' style="color: red;"><span style="font-size: 14px;" >$0.00</span></a></div>
                                    <div style="color: #b6b4b4;display: inline-block;text-align: right;">Enrolled<br> <a href='refund_discount.php' id='total_discount' style="color: red;font-size: 14px;"><span style="font-size: 14px;">$0.00</span></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-4" style='cursor:pointer' onclick="location.href='fee_collection_report.php'">
                    <div class="dashboard-summery-one mg-b-20 hero-example13" style="box-shadow: 0px 0px 5px 2px rgb(198 61 252 / 40%);">
                        <div class="row">
                            <div class="item-icon bg-light-pink">
                                <img src="img/money.svg" style="width: 30px;">
                            </div>
                            <div class="col-sm-12">
                                <div class="item-content" style="padding: 7px 0px;">
                                    <div class="item-title">Total Collection</div>
                                    <!-- <div class="item-number"><span class="counter" id='total_collection'>$0.00</span></div> -->
                                    <div style="color: #b6b4b4;display: inline-block;text-align: left;float: left;margin-left: 59px;">New<br> <span style="font-size: 14px;color: #111111;"><a href="fee_collection_report.php?type=other&acyear=2021&status=undefined-School_wise--1-today&period=today&period_type=-1&seltype=School_wise&seltype2=-1&period_type2=&instalment_mode=-1&fee_type=-1&fee_type_head=-1&section_name_type=School_wise" id="TotalPaid_New" style="color: #05cc05;"><?php echo $currency.number_format(0, 2, '.', ','); ?></a></span></div>
                                    <div style="color: #b6b4b4;display: inline-block;text-align: right;">Enrolled<br> <span style="font-size: 14px;color: #111111;"><a href="fee_collection_report.php?type=db_student&acyear=2021&status=undefined-School_wise--1-today&period=today&period_type=-1&seltype=School_wise&seltype2=-1&period_type2=&instalment_mode=-1&fee_type=-1&fee_type_head=-1&section_name_type=School_wise" id="TotalPaid" style="color: #05cc05;"><?php echo $currency.number_format(0, 2, '.', ','); ?></a></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
<div class="row l-r-10 row-custom-margin">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6"> 

    <div class="panel panel-default" style="border-radius: 25px !important;box-shadow: 0px 0px 5px 2px rgb(28 175 154 / 26%) !important;">
        <div class="panel-heading ui-draggable-handle">
            <h2 class="title pull-left">Today's Attendance</h2>
        </div>
        <div class="panel-body" style="padding-top: 0px;">
            <br>
            <div class="widget-default widget-carousel;" style="text-align:center;width:100%;">
                <center>
                    <div class="owl-carousel" id="owl-example" style='text-align:center;width:100%'>
                  
                        <div>
                                                           
                            <div class="widget-title">
							Student Attendance
                                <div id="containerpie1"  style="width: 100%; height:280px;"></div>
								<?php
								include_once('../admin/total_strength.php');
								?>
                            </div>                                                                        
                      
                        </div>
                        <div>                                    
                            <div class="widget-title">
							Teacher Attendance
                              <div id="containerpie2" style="width: 100%; height:280px"></div>
							  TOTAL :180
                            </div>
                            
                        </div>
                    </div>                            
                 </center>                               
                </div>    
        </div>
    </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
        <div class="panel panel-default hero-example15" style="border-radius: 25px !important;box-shadow: 0px 0px 5px 2px rgb(28 175 154 / 26%) !important;">
        <div class="panel-heading ui-draggable-handle">
            <h2 class="title pull-left">Users Activity</h2>
        </div>
        <div class="panel-body" style="padding-top: 0px;">
            <div class="table-responsive" style="width: 100%;height: 362px;"> 
                <table class="table table-bordered" style="text-align: center;">
                    <thead>
                        <tr>
                            <th style="text-align: center;" >User</th>
                            <th style="text-align: center;">Status</th>
                            <th style="text-align: center;">Last Login</th>
                            <th style="text-align: center;">Last Logout</th>
                            <th style="text-align: center;">Duration</th>
                        </tr>
                    </thead>
                    <tbody id='user_details'>
                       
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
</div>

<div class="row l-r-10">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

            <div class="panel panel-default hero-example14" style="border-radius: 25px !important;box-shadow: 0px 0px 5px 2px rgb(28 175 154 / 26%) !important;">
        <div class="panel-heading ui-draggable-handle">
            <h2 class="title pull-left">Fee Status</h2>
        </div>
        <div class="panel-body">
        <div class="owl-carousel" id="owl-example" style='text-align:center;width:100%'>
        <div class="widget-title">
        Enrolled
            <div id="container2" style="width: 100%;height: 322px;"></div>
        </div>
        <div class="widget-title">
          New
            <div id="container5" style="width: 100%;height: 322px;"></div>
        </div>
        </div>
        </div> 
    </div>

    </div>
    </div>
                                    

                    
                    <!-- START DASHBOARD CHART -->
				<!--	<div id="container_mapmain" style="min-width: 310px; height: 400px; margin: 0 auto; background:none;"></div>-->
					<div class="block-full-width">
                                                                       
                    </div>