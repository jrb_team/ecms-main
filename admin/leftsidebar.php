<?php
		include_once('../db/createdb.php');
		$dbobject = new DB();
		if (!$dbobject->getCon())
		{
				echo "Connection Failed";
		}
		$school_info=$dbobject->selectall("schoolinfo",array("id"=>1));
		$imagebytes=$school_info['school_logo'];
		
?>
 <ul class="x-navigation x-navigation-main-menu hero-example17">
    <?php include_once('menu/menu.php')?>
</ul>
<script>

	$(document).ready(function(){
	        $(".page-container.page-container-wide .page-sidebar").css("width", "60px");
            // $(".page-sidebar ul.x-navigation-main-menu").css("width", "60px");
          $("ul.x-navigation-main-menu").mouseover(function(){
            $(".page-container.page-container-wide .page-sidebar").css("width", "250px");
            $(".page-sidebar ul.x-navigation-main-menu").css("width", "250px");
            $(".x-navigation-main-menu li a").css("text-align", "left");
            $(".x-navigation-main-menu li a span.xn-text").show();
          });
          $("ul.x-navigation-main-menu").mouseout(function(){
            $(".x-navigation-main-menu li a span.xn-text").hide();
            $(".page-container.page-container-wide .page-sidebar").css("width", "60px");
            $(".page-sidebar ul.x-navigation-main-menu").css("width", "60px");
          });
$("ul.ajax-submenu-list").mCustomScrollbar({
    axis:"x" // horizontal scrollbar
});

$(".toggle-menu-btn").click(function(){
    $(".page-sidebar").toggleClass("show-menu");
    $(".page-sidebar ul.x-navigation-main-menu").toggleClass("show-ul");
    $(".toggle-menu-btn").toggleClass("collapsed");
});

$('ul.x-navigation-main-menu').css('height', $(window).height());
	});
	function show_menu(val){
	     $.ajax({
        url: "ajax-sub-menu.php",
        type: "GET",
        data: {id:val} ,
        success: function (response) {
        $("#ajax-sub-menu").html(response);
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
	}
</script>