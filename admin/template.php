<?php
/*require_once('authentication.php');
authenticate();
include_once('../db/createdb.php');
$dbobject = new DB();
$dbobject->getCon();	
$staff_det=$dbobject->selectall("teacher",array("id"=>$_SESSION['id']));
if($staff_det['login']=="0"){
    header("Location:login/index.php");
}
else{*/
?>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>eCMS || <?php if($_SESSION['main_menu']!="") { echo $_SESSION['main_menu'];} ?>  <?php if($_SESSION['sub_menu']!="") { echo "|| ".$_SESSION['sub_menu']; } ?></title> 
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="icon" href="../img/pay_logo.png" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link href="css/jquery-ui.css" rel="stylesheet">
        <link rel="stylesheet" id="theme" href="../admin/css/theme-default.css"/>
        <link rel="stylesheet" id="theme" href="../admin/css/menu-icons/flaticon.css"/>
        <link rel="stylesheet" type="text/css" href="../admin/css/shepherd.css"/>
        <!-- EOF CSS INCLUDE --> 
        <script type="text/javascript" src="../admin/js/plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="../admin/js/highcharts.js"></script>
        <script type="text/javascript" src="../admin/js/map.js"></script>
        <script src="../admin/js/data.js"></script>
        <script src="../admin/js/exporting.js"></script>
        <script src="../admin/js/world.js"></script>	  
        

 <!-- ALER BOX INCLUDE --> 	
   
    <!--<script src="demo/libs/bundled.js"></script>-->

    <link rel="stylesheet"
          href="../alert/demo/demo.css">

    <!-- Add the minified version of files from the /dist/ folder. -->
    <!-- jquery-confirm files -->
    <link rel="stylesheet"
          type="text/css"
          href="../alert/css/jquery-confirm.css"/>
    <script type="text/javascript"
            src="../alert/js/jquery-confirm.js"></script>
    <!--<script type="text/javascript"-->
	<style>
	th {
		font-size: 150%;
	}
	</style>
  </head>
  <body>
       <!-- START PAGE CONTAINER -->
                <div class="page-container page-navigation-toggled">
            
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
					<?php include_once('leftsidebar.php')?>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <?php include_once('navigation_vertical.php')?>
                <!-- END X-NAVIGATION VERTICAL -->                     
                <div id="ajax-sub-menu" class="hero-example19">
                    
                    </div>
                    <div id="SessionModal" class="modal fade" role="dialog" data-keyboard="false" data-backdrop="static">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                        <div class="modal-body time-out-popup">
                            <button type="button" class="close" data-dismiss="modal" style="top: 16px;right: 15px;">&times;</button>
                            <img src="../admin/img/deadline.png">
                            <h4 class="modal-title">Session Time-out</h4>
                            <p>Your Session will logout in <span id='time_session'></span> Minute</p>
                            <div class="footer-popup">
                                <button type="button" class="btn" onClick='Update_Session();'>Continue</button>
                            <a href="login/logout.php" class="btn">Logout</a>
                            </div>
                        </div>

                        </div>

                    </div>
                    </div>
				<?php main(); ?>
                    
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="login/logout.php" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="RenewModel" class="create-assignment-popup modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content" id="Renewmodal_content">

    </div>

  </div>
</div>
<div class="modal fade bd-example-modal-xl" id="renewDirectDebit" style='width:80%;margin: auto;' tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document"  style='width:100%'>
    <div class="modal-content"  id='renew_directDebit'>

    </div>
  </div>
</div> 
        <!-- END MESSAGE BOX-->
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="audio/alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="audio/fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                  
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        
        <script type="text/javascript" src="../admin/js/plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../admin/js/plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->

        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src='../admin/js/plugins/icheck/icheck.min.js'></script>        
        <script type="text/javascript" src="../admin/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="../admin/js/plugins/scrolltotop/scrolltopcontrol.js"></script>
        

        <script type='text/javascript' src='../admin/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'></script>
        <script type='text/javascript' src='../admin/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script>                
        <script type='text/javascript' src='../admin/js/plugins/bootstrap/bootstrap-datepicker.js'></script>  
        <script type="text/javascript" src="../admin/js/plugins/bootstrap/bootstrap-timepicker.min.js"></script>
		<script type="text/javascript" src="../admin/js/plugins/bootstrap/bootstrap-colorpicker.js"></script>		
        <script type="text/javascript" src="../admin/js/plugins/owl/owl.carousel.min.js"></script>      
        <script type="text/javascript" src="js/plugins/bootstrap/bootstrap-file-input.js"></script>
        <script type="text/javascript" src="../admin/js/plugins/bootstrap/bootstrap-select.js"></script>
        <script type="text/javascript" src="../admin/js/plugins/tagsinput/jquery.tagsinput.min.js"></script>		
        
        <script type="text/javascript" src="../admin/js/plugins/moment.min.js"></script>
        <script type="text/javascript" src="../admin/js/plugins/daterangepicker/daterangepicker.js"></script>
        <!-- END THIS PAGE PLUGINS-->        

        <!-- START TEMPLATE -->
        <script type="text/javascript" src="../admin/js/settings.js"></script>
        
        <script type="text/javascript" src="../admin/js/plugins1.js"></script>        
        <script type="text/javascript" src="../admin/js/actions.js"></script>
        <script src="http://code.highcharts.com/modules/exporting.js"></script> 
        <script type="text/javascript" src="../admin/js/shepherd.js"></script>
  <script>
$(document).ready(function(){
    var idleTime = 0;
    var time=1800;
    btime=1500;
    Update_Session();
    $(document).ready(function () {
        renewSub(0);
        $(this).mousemove(function (e) {
            if($('#SessionModal').hasClass('in')==false){
                idleTime = 0;
               // Update_Session();   
            }
    });
        $(this).keypress(function (e) {
            if($('#SessionModal').hasClass('in')==false){
            idleTime = 0;
            //Update_Session();
        }
        });
        
var interval = setInterval(function() {

    idleTime++;
    // Display 'counter' wherever you want to display it.
    if(idleTime==btime){
        $('#SessionModal').modal('show'); 
    }
    if (idleTime >= time) {
     		clearInterval(interval);
             window.location.href = "login/logout.php";  
        return;
    }else{
       var count_down=Math.floor((time-idleTime) / 60) + ":" + ((time-idleTime) % 60 ? (time-idleTime) % 60 : '00')
    	$('#time_session').text(count_down);
    }
}, 1000);

        // Zero the idle timer on mouse movement.

    });
    CheckSession();
 }); 
 function Update_Session(){
    $.ajax({
            url: "../admin/Update_Session.php",
            type: "POST",
            dataType: 'json',
            data:{},

            success:function(response)
            {
               if(response.status=="Success"){
             
                idleTime=0;
                $('#SessionModal').modal('hide');
               }else{
                window.location.href = "login/logout.php";  
               }
            }

            });
 }
function disable_tour(){
    $.ajax({
        url: "../admin/UpdateTourStatus.php",
        type: "GET",
        data: {} ,
        success: function (response) {
            $("#tour_status").val(response);
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
}
function CheckSession(){
    $.ajax({
        url: "../admin/CheckSession.php",
        type: "GET",
        data: {} ,
        dataType:'json',
        success: function (response) {
            if(response.Status=="Error"){
            window.location.href = "login/logout.php?type=1";
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
}
function Userguide(val){
    if(val==""){
        $("#userGuide_content").html("");
        $(".user-guide-box .user-guide-box-body").hide();
    }else{
    $.ajax({
        url: "../admin/SearchUserGuide.php",
        type: "GET",
        data: {val:val} ,
        dataType:'html',
        success: function (response) {
            $(".user-guide-box .user-guide-box-body").show();
            $("#userGuide_content").html(response);
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
    }
}
function Noaccess(){
$.confirm({
    title: 'Warning!',
    content: 'Access denied. Please contact admin',
    type: 'red',
    typeAnimated: true,
    buttons: {
        close: function () {
        }
    }
});
}
function renewSub(val){
    if(val==0){
        $.ajax({
				type: "GET",
				url: "../admin/CheckRenew_.php",
                data: {} ,
                dataType:"json",
				success: function(data)
				{
                    if(data.status=="nearly expire"){
                        openRenewModel(1);
                    }else if(data.status=="expired"){
                        $('#RenewModel').modal({backdrop: 'static', keyboard: false})  
                        openRenewModel(0);
                    }
				}
				});
    }else{
        openRenewModel(1);
    }
}
function openRenewModel(val){
    $('#RenewModel').modal('show');
                $.ajax({
				type: "GET",
				url: "../admin/Renewmodal_.php",
                data: {type:val} ,
				success: function(data)
				{
					$('#Renewmodal_content').html(data);
				}
				});
}
function EnableDirectDebitRenew(){
        $.ajax({
            url: "../admin/ddr-formRenew.php",
            type: "GET",
            data: {} ,
            success: function (response) {
            $("#renew_directDebit").html(response);
            },
            error: function(jqXHR, textStatus, errorThrown) {
            console.log(textStatus, errorThrown);
            }
        });
    }
    
      </script>
        <!-- END TEMPLATE -->
        
    <!-- END SCRIPTS -->         
    </body>
</html>
<?php
//}
?>