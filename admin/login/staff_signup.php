<?php
/* ***************************************
Code For Allowing Only 1 Admin at a time
require_once('checkadmin.php');
checklogin();
******************************************/
require_once('checkreg.php');
$cust_name=check_registration();
$challenge = "";
for ($i = 0; $i < 80; $i++) {
    $challenge .= dechex(rand(0, 15));
}
?>

<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">        
        <!-- META SECTION -->
        <title>ecMS - Administrator</title>            
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="../../favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->  
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="../css/theme-default.css"/>
		<!-- EOF CSS INCLUDE -->  
		<script type="text/javascript" src="md5.js"></script>
		 <script language="JavaScript" type="text/javascript">
		  function Submit()
		  {
		 		 submitForm.response.value=hex_md5(submitForm.challenge.value+submitForm.password.value);
		  }
		  
		/// JQUERYSSSSSSS
</script>                                  
    </head>
    <body> 
        <div class="login-container" style="padding-top:10px;background-color:#fff;">
            <div class="login-box animated fadeInDown" style="padding-top:10px;width:90%;">
               
                <div class="login-body">
                    <div class="login-title"><strong><center>STAFF SIGN-UP</center></strong></div>
                    <div class="block">  
                               <form id="addstaff"  class="form-horizontal" onsubmit="return validate();" action="addstaff_action.php" method="POST" enctype="multipart/form-data" >                                              
								<input type="hidden" class="form-control datepicker" name="date"/>
								
								<div class="table-responsive">
          <!--                       <div class="col-md-12">-->
								
										<!--<center>-->
										<!--<img class="profile-pic"  id="blah" src="../img/people-img1.png" width="130" height="130" alt="Photo" />-->
										<!--<input type="file" class="fileinput btn-danger" name="staffimage" id="staffimage" data-filename-placement="inside" title="Browse file for upload"/>-->
										<!--</center></div>-->
								
								<div class="col-md-2" style="margin-top:7px;"><strong style="color:#fff;">User Id :*</strong></div>
								<div class="col-md-4" style="margin-top:7px;">
								<input type="text" class="form-control" name="userid" id="userid" onblur="check_staffid(this.value);" required/></div>
								
								<div class="col-md-2" style="margin-top:7px;"><strong style="color:#fff;">Password:*</strong></div>
								<div class="col-md-4" style="margin-top:7px;"><input type="password" class="form-control" name="password" id="password" required/></div>
								<div class="col-md-2" style="margin-top:7px;"><strong style="color:#fff;">Confirm Password:*</strong></div>
								<div class="col-md-4" style="margin-top:7px;"><input type="password" class="form-control" name="cpassword" id="cpassword" required/></div>
								
								
								<div class="col-md-2" style="margin-top:7px;"><strong style="color:#fff;"> Surname :*</strong></div>
								<div class="col-md-4" style="margin-top:7px;"><input type="text" class="form-control" name="sname" id="sname" required/></div>
								
								<div class="col-md-2" style="margin-top:7px;"><strong style="color:#fff;"> First Name :*</strong></div>
								<div class="col-md-4" style="margin-top:7px;"><input type="text" class="form-control" name="name" id="name" required/></div>
								
								<div class="col-md-2" style="margin-top:7px;"><strong style="color:#fff;"> Middle Name :</strong></div>
								<div class="col-md-4" style="margin-top:7px;"><input type="text" class="form-control" name="mname" id="mname"/></div>
								
								<div class="col-md-2" style="margin-top:7px;"><strong style="color:#fff;">Gender:</strong></div>
								<div class="col-md-4" style="margin-top:7px;">
								    <select class="form-control select" id="gender" name="gender" required>
								        <option value="">--Select--</option>
								        <option value="Male">Male</option>
								        <option value="Female">Female</option>
								    </select>
								</div>
								
								<div class="col-md-2" style="margin-top:7px;"><strong style="color:#fff;">Date of birth:</strong></div>
								<div class="col-md-4" style="margin-top:7px;"><input type="date" class="form-control" id="dob" name="dob" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy" required onchange="Calc_age(this.value);"/></div>
								
								<div class="col-md-2" style="margin-top:7px;"><strong style="color:#fff;">Email:</strong></div> 
								<div class="col-md-4" style="margin-top:7px;"><input type="text" class="form-control" name="email" id="email" required ></div>
								<div class="col-md-2" style="margin-top:7px;"><strong style="color:#fff;">Mobile No:</strong></div>
								
								<div class="col-md-4" style="margin-top:7px;"><input class="mask_phone form-control"  type="text"  name="mobile" id="mobile" required></div>
								<div class="col-md-2" style="margin-top:7px;"><strong style="color:#fff;">Home Phone:</strong></div>
								
								<div class="col-md-4" style="margin-top:7px;"><input class="mask_phone form-control"  type="text"  name="hphone" id="hphone" required></div>
								<div class="col-md-2" style="margin-top:7px;"><strong style="color:#fff;">Address:</strong></div>								
								<div class="col-md-4" style="margin-top:7px;">
								    <textarea name="res_adderss" id="res_address" style="width:100%;" required></textarea>
								</div>
								
								<div class="col-md-2" style="margin-top:7px;"><strong style="color:#fff;">Suburb*</strong></div>
								<div class="col-md-4" style="margin-top:7px;">
								    <input type="text" name="suburb" id="suburb" class="form-control" required>
								</div>
								<div class="col-md-2" style="margin-top:7px;"><strong style="color:#fff;">State*</strong></div>
								<div class="col-md-4" style="margin-top:7px;">
								    <input type="text" name="state" id="state" class="form-control" required>
								</div>
								
								<div class="col-md-2" style="margin-top:7px;"><strong style="color:#fff;">Post code*</strong></div>
								<div class="col-md-4" style="margin-top:7px;">
								    <input type="text" name="pin" id="pin" class="form-control" required>
								</div>
								<div class="col-md-2" style="margin-top:7px;"><strong style="color:#fff;">Tax File No:</strong></div>
								<div class="col-md-4" style="margin-top:7px;"><input type="text" class="form-control" name="Tax_File_No" id="Tax_File_No"/></div>
								
								<div class="col-md-2" style="margin-top:7px;"><strong style="color:#fff;"> VIT Reg :</strong></div>
								<div class="col-md-4" style="margin-top:7px;"><input type="text" class="form-control" name="VIT_Reg" id="VIT_Reg" /></div>
								
								<div class="col-md-2" style="margin-top:7px;"><strong style="color:#fff;">Expiry :</strong></div>
								<div class="col-md-4" style="margin-top:7px;"><input type="date" class="form-control" id="Expiry" name="Expiry" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"/></div>								
								
								
								<div class="col-md-2" style="margin-top:7px;"><strong style="color:#fff;">WWC :</strong></div>
								<div class="col-md-4" style="margin-top:7px;"><input type="text" class="form-control" name="WWC" id="WWC"/></div>	
								<div class="col-md-2" style="margin-top:7px;"><strong style="color:#fff;">Expiry :</strong></div>
								<div class="col-md-4" style="margin-top:7px;"><input type="date" class="form-control" id="Expiry2" name="Expiry2" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy"/></div>									
								<div class="col-md-6" style="margin-top:7px;">
								</div>
								
								<div class="col-md-12" style="margin-top:22px;"><center><button type="submit" class="btn btn-sucess" style="width:50%;">SIGN-UP</button></center></div>
								 										
                                </div>                                               
                                </form>
                            <!-- END JQUERY VALIDATION PLUGIN -->
                           </div>
                </div>
                <div class="login-footer">
                    <div class="pull-left">
                        &copy; <?php echo date('Y'); ?> JR BOON SOLUTIONS
                    </div>
                    <div class="pull-right">
                        <a href="http://www.ecms-demo.com">About</a> |
                        <a href="#">Privacy</a> |
                        <a href="http://www.ecms-demo.com">Contact Us</a>
                    </div>
                </div>
            </div>    
        </div>  
    </body>
</html><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
$( "#mobile" ).keyup(function() {
  var a=$( "#mobile" ).val();
  $.ajax({
      url:"check_user_mobile.php",
      method:"POST",
      data:{
        userid:a
      },
      success:function(response){
      if(response==1){
       alert("Mobile Number Already Used, Try Another");
       $( "#mobile" ).val("");
      }
      }
    });
});
$( "#userid" ).keyup(function() {
  var a=$( "#userid" ).val();
  $.ajax({
      url:"check_user_id.php",
      method:"POST",
      data:{
        userid:a
      },
      success:function(response){
      if(response==1){
       alert("Userid Already Taken Try Another");
       $( "#userid" ).val("");
      }
      }
    });
});
$( "#email" ).keyup(function() {
  var a=$( "#email" ).val();
  $.ajax({
      url:"check_user_email.php",
      method:"POST",
      data:{
        userid:a
      },
      success:function(response){
      if(response==1){
       alert("Email ID Already Used, Try Another");
       $( "#email" ).val("");
      }
      }
    });
});
function validate() {
  var userid = document.forms["addstaff"]["userid"].value;
  if(userid.length < 6){
    alert("UserID Must have 6 Characters");
    document.getElementById("userid").focus();
    return false; 
  }
  var x = document.forms["addstaff"]["password"].value;
  var upperCaseLetters = /[A-Z]/g;
  var numbers = /[0-9]/g;
  if(x.match(upperCaseLetters)) {
  }else{
    alert("Password Must Have a CAPITAL Letter");
    document.getElementById("password").focus();
    return false;
  }
  if(x.match(numbers)) {}else{
    alert("Password Must Have a Number");
    document.getElementById("password").focus();
    return false;
  }
  if(x.length < 6){
    alert("Password Length must be greater than 6");
    document.getElementById("password").focus();
    return false;
  }
  var y = document.forms["addstaff"]["cpassword"].value;
  if (x != y) {
    alert("Password Not Matching");
    document.getElementById("cpassword").focus();
    return false;
  }
}
</script>
