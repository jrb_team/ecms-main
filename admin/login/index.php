<?php
/* ***************************************
Code For Allowing Only 1 Admin at a time
require_once('checkadmin.php');
checklogin();
******************************************/
require_once('checkreg.php');
$cust_name=check_registration();
$challenge = "";
for ($i = 0; $i < 80; $i++) {
    $challenge .= dechex(rand(0, 15));
}
?>
<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">        
        <!-- META SECTION -->
        <title>eCMS</title>            
        
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="../../img/pay_logo.png" type="image/x-icon" />
        <!-- END META SECTION -->  
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="../css/theme-default.css"/>
		<!-- EOF CSS INCLUDE -->  
		<script type="text/javascript" src="md5.js"></script>
		 <script language="JavaScript" type="text/javascript">
		  function Submit()
		  {
		 		 submitForm.response.value=hex_md5(submitForm.challenge.value+submitForm.password.value);
		  }
		  
		/// JQUERYSSSSSSS
</script>                                  
    </head>
    <body> 
        <div class="login-container">
            <div class="login-box animated fadeInDown">
                <div class="login-logo"></div>
                <div class="login-body">
                    <div class="login-title"><strong>Admin Login</strong></div>
                    <form action="auth.php" class="form-horizontal" name="submitForm" method="POST" >
					<input name="challenge" type="hidden"  value="<?php echo $challenge;?>"/>
					<input name="response" type="hidden" />
					<?php if($_GET['status']==1) { echo "<font color='#FFF'>Invalid Username or Password</font>";}
					      else{ echo "<font color='#FFF'>login with your username and password</font>";}?>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="username" placeholder="Username" autofocus/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-link btn-block" onClick='ResetPassWord();'>Forgot your password?</a>
                        </div>
                        <div class="col-md-6">
                         <button class="btn btn-info btn-block" onClick	="Submit()">Log In</button>
                        </div>
                    </div>
                    </form>
                </div>
                <div class="login-footer">
                    <div class="pull-left">
                        &copy; <?php echo date('Y'); ?> JR Boon Solutions
                    </div> 
                    <div class="pull-right">
                        <a href="http://jrboonsolutions.com/">About</a> |
                        <a href="http://jrboonsolutions.com/privacy_policy_AU.html">Privacy</a> |
                        <a href="http://jrboonsolutions.com/srs/">Contact Us</a>
                    </div>
                </div>
            </div>    
        </div>  
    </body>
</html>
<script type="text/javascript" src="../js/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="../js/plugins/jquery/jquery-ui.min.js"></script>
<link rel="stylesheet"
          type="text/css"
          href="../../alert/css/jquery-confirm.css"/>
    <script type="text/javascript"
            src="../../alert/js/jquery-confirm.js"></script>
<script>
  function ResetPassWord(){
    $.confirm({
    title: 'Message!',
    content: 'Temporary password will send to your registered email id.',
    buttons: {
        confirm: function () {
          $.ajax({
            url: "ResetPassword.php",
            type: "GET",
            data: {} ,
            dataType:"json",
            success: function (response) {
              if(response.Status=="Success"){
                $.alert('Password sent Successfully.Please check your registerd email');
              }else{
                $.alert(response.Message);
              }
            },
            error: function(jqXHR, textStatus, errorThrown) {
              console.log(textStatus, errorThrown);
            }
        });
        },
        cancel: function () {
          
        }
    }
});
  }
  </script>