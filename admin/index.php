<?php
error_reporting(E_ALL);
error_reporting(-1);
ini_set('error_reporting', E_ALL);
require_once('authentication.php');
authenticate();
if($_SESSION['unsecurity']!='main'){
    header('Location: login/index.php');
}else{
    
}
$_SESSION['main_menu']="Dashboard";
$_SESSION['sub_menu']="";
require_once("template.php");
function main()
{
include_once('../db/createdb.php');
include_once('../db/reminder_class.php');
include_once('../db/teacher_class.php');
$dbobject = new DB();
$dbobject->getCon();	
$reminder = new Reminder();
$Teacher = new Teacher();
$acyear=$dbobject->get_acyear();
$userid=$_SESSION['userid'];
$usertype=$_SESSION['usertype'];
if($_GET['page']!="")
{
$page=$_GET['page'];
}
else
{
$page="dashboard";	
}

$school_info=$dbobject->selectall("schoolinfo",array("id"=>1));
?>
<style>   
#container_map {
    height: 500px;
    min-width: 310px;
    max-width: 800px;
    margin: 0 auto;
}
.loading {
    margin-top: 5em;
    text-align: center;
    color: gray;
}	
#datatable{display:none}
.modalDialog {
    position: fixed;
    font-family: Arial, Helvetica, sans-serif;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    background: rgba(0, 0, 0, 0.8);
    z-index: 99999;
    opacity:0;
    -webkit-transition: opacity 400ms ease-in;
    -moz-transition: opacity 400ms ease-in;
    transition: opacity 400ms ease-in;
    pointer-events: none;
}
.modalDialog:target {
    opacity:1;
    pointer-events: auto;
}
.modalDialog > div {
    width: 500px;
    position: relative;
    margin: 10% auto;
    padding: 25px 20px 25px 20px;
    border-radius: 0px;
    background: #fff;
    color: #000;

}
.close {
    background: none;
    color: #000 !important;
    line-height: 25px;
    position: absolute;
      right: 6px;
    text-align: center;
    top: -15px;
    width: 24px;
    text-decoration: none;
    font-weight: bold;
    -webkit-border-radius: 12px;
    -moz-border-radius: 12px;
    border-radius: 12px;
    -moz-box-shadow: 1px 1px 3px #000;
    -webkit-box-shadow: 1px 1px 3px #000;
    box-shadow: 1px 1px 3px #000;
}
.close:hover {
    background: none;
}
.full-menu-tour{
  transform: translate(60px, 230px) !important;
}
.tab-menu-search{
    width: 100%;
    float: left;
    margin: 0px 0px 30px 0px;
    position: relative;
}
</style>          
<!--<section class="wrapper row">
<div class="col-sm-12">
    <div class="breadcrumbs-area">
        <h3>Dashboard</h3>

        <ul class="breadcrumb">        
            <li>
                <a href="#"><i class="fa fa-home"></i> Dashboard</a>
            </li>
        </ul>

    </div>
</div> -->                   
                    <div class="col-sm-12">
                        <div class="row l-r-10">
                          <div class="col-sm-4">
                                <div class="tab-menu-search move-left hero-example6">
                                   <div class="input-group home-line-form-search">
                                        <form action="student_profile_home.php" method="GET" onSubmit="return validate_studentSearch();" autocomplete="off">
                                          <input type='hidden' id='familyCode_studentSearch' name='family_id'/>
                                            <input type="text" class="form-control" placeholder="Search Student (Type first name)" name="studentid" id="studentid"  onKeyUp="GetStudentsInd(this.value,1)" value="" />
                                            <input type="submit" value="Search" /><br/>
                    					          </form>
                                    </div>
                                    <div id="hint_studentind" ></div>
                               </div>
                          </div>
                          <div class="col-sm-4">
                          <div class="tab-menu-search hero-example7">
                                   <div class="input-group home-line-form-search">
                                        <form action="student_profile_home.php" method="GET" onSubmit="return validate_student();" autocomplete="off">
                                            <input type="text" class="form-control" placeholder="Search Family (Type family Id)" name="family_id" id="sid"  onKeyUp="GetStudents(this.value,2)" value="" />
                                            <input type="submit" value="Search" /><br/>
                    					          </form>
                                    </div>
                                    <div id="hint_student" ></div>
                               </div>
                          </div>
                          <div class="col-sm-4">
                                <div class="tab-menu-search hero-example8">
                                   <div class="input-group home-line-form-search">
                                      <form action="teacher_profile.php" method="GET" onSubmit="return validate_teacher();" autocomplete="off">
                                        <input type="text" placeholder="Search Staff (Type first name)" class="form-control" name="tid" id="tid" onKeyUp="GetTeacher(this.value)">
                                        <input type="submit" value="Search" />
                                      </form>
                                    </div>
                                    <div id="hint_teacher"></div>
                               </div>
                          </div>
                        </div>
                    </div>
                         <div class="col-md-12">
                             <!-- <ul class="nav nav-tabs custom-nav-tabs mrg-top-0" role="tablist"> -->
                             <!-- <input type='hidden' id='tour_status' name='tour' value='<?php //echo $school_info['tour_status'];?>'/> -->
                               <!-- <li class="<?php // if($page=="dashboard"){ echo "active"; } else { echo "" ;} ?>"><a href="index.php" >Dashboard</a></li>-->
                               <!--<li class="menu-tab <?php // if($page=="menu"){ echo "active"; } else { echo "" ;} ?>"><a href="#tab-second" role="tab" data-toggle="tab">Menu</a></li>  -->        
							<!-- </ul> -->
                            <!-- START SIMPLE DATATABLE -->
                               <div class="tab-content">
								<div class="<?php if($page=="dashboard"){ echo "tab-pane active"; } else { echo "tab-pane" ;} ?>" id="tab-first">							   
								 <?php require_once('dashboard_page_out.php');?>

								 </div>
								 <div class="<?php if($page=="menu"){ echo "tab-pane active"; } else { echo "tab-pane" ;} ?>" id="tab-second">
									<?php require_once('menu_out.php');?>
                                  </div>    
								</div>
                            <!-- END SIMPLE DATATABLE -->
                    </div>               
                    <!-- END DASHBOARD CHART -->
</section>
<!-- Modal -->
<div id="UserActivity" class="create-assignment-popup modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" style="height: 24px;margin: 9px 5px;">&times;</button>
        <h4 class="modal-title" id='useractivity_name'>Users Activity</h4>
      </div>
      <div class='custom-radio-btn' style="padding: 15px 15px 0px 15px;">
    <?php 
    $today_date=Date('d-m-Y');
    echo "<select name='type' id='type' class='form-control' onChange='getPeriodUserActivity(this.value);' style='display: inline-block;width: auto;'><option value='today'>Today</option>
    <option value='week'>This Week</option>
    <option value='month'>Month</option></select>
    <div id='period_div' style='display: inline-block;'>
    <select name='period' id='period_options' class='form-control'><option value='".$today_date."'>".$today_date."</option></select>
    </div>
    <input type='button' class='btn btn-primary' onClick='getDataUserActivity();' value='Get Data'/>"; 
    ?>
    </div>
      <div class="modal-body" id='useractivity_log'>
      </div>
    </div>

  </div>
</div>
                    

        <script src="js/dashboard_js.js"></script> 
		<script src="js/jquery.js"></script>
<script src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>
       <script type="text/javascript" src="js/plugins/datatables/jquery.dataTables.min.js"></script>
	   
	   <script>
    $(document).ready(function(){
      $("#main_menu").html("<?php echo $_SESSION['main_menu'];?>");
    });
</script>

<script>
    $(document).ready(function(){
      
        $("a.go-to-menu").click(function(){
           $("ul.custom-nav-tabs li").removeClass("active");
           $("#tab-first").removeClass("active");
           $("ul.custom-nav-tabs li.menu-tab").addClass("active");
           $("#tab-second").addClass("active");
           
        });

        $('.shepherd-element .shepherd-footer button').click(function($e) {
            $e.preventDefault();
        });
        GetDashboardData();
        userActivity();
    });
</script>
			<script type="text/javascript" language="javascript">
			$("#date").datepicker();
			$(".timepicker").timepicker();
function userActivity(){
  $.ajax({
       type: "GET",
       url: "../admin/GetUserActivity.php",
       data:{}, // serializes the form's elements.
      dataType:"html",
       success: function(data)
       {
        $("#user_details").html(data);
       }
     });
}
function GetDashboardData(){
  $.ajax({
       type: "GET",
       url: "../admin/GetDashboardData.php",
       data:{}, // serializes the form's elements.
      dataType:"json",
       success: function(data)
       {
        $("#total_discount").html(data.total_discount); 
        $("#str_enrol").html(data.str_enrol);
        $("#str_new").html(data.str_new);
        $("#student_strength").html(data.strength);
        $("#grand_total_fee_new").html(data.grand_total_fee_new);
        $("#grand_total_fee").html(data.grand_total_fee); 
        $("#today_paid_new").html(data.today_paid_new); 
        $("#today_paid").html(data.today_paid); 
        $("#TotalPaid_New").html(data.TotalPaid_New); 
        $("#TotalPaid").html(data.TotalPaid); 
        saveFeeData();
       }
     });
}
function saveFeeData(){
    $.ajax({
       type: "GET",
       url: "../admin/saveFeeData.php",
       data:{}, // serializes the form's elements.
      dataType:"json",
       success: function(data)
       {
       }
     });
}
function GetStudentsInd(id,type){
					if (id=="")
				  {
				  document.getElementById("hint_studentind").innerHTML="";
				  return;
				  } 
				  var x="getstudents_studentID1.php?id="+id+"&type="+type;
				 if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
						xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
						xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
						  
						xmlhttp.open("GET",x,true);
						xmlhttp.send();     
						xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						document.getElementById("hint_studentind").innerHTML=xmlhttp.responseText;
					}
				  }
				
}
function GetId_stud(val,family_code)
				{
					document.getElementById("studentid").value=val;
          document.getElementById("familyCode_studentSearch").value=family_code;
					document.getElementById("hint_studentind").innerHTML="";
				}
function open_model2()
{
window.location="add_reminder.php";
}
function reminder()
{
window.location="reminder.php";
}	
function edit_rem(id)
{
	window.location="reminder_edit.php?id="+id+"&val=1";
}
function del_rem(id)
{
    var r=confirm("Are you Sure To Delete");
			if (r==true)
			  {	
	window.location="reminder_action.php?id="+id+"&val=2";
			  }
}		
				 function GetStudents(id)
				  {
					if (id=="")
				  {
				  document.getElementById("hint_student").innerHTML="";
				  return;
				  } 
				  var x="getstudents.php?id="+id;
				 if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
						xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
						xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
						  
						xmlhttp.open("GET",x,true);
						xmlhttp.send();     
						xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						document.getElementById("hint_student").innerHTML=xmlhttp.responseText;
					}
				  }
				}
				function GetId(val)
				{
					document.getElementById("sid").value=val;
					document.getElementById("hint_student").innerHTML="";
				}
				function GetTeacher(id)
				  {
					if (id=="")
				  {
				  document.getElementById("hint_teacher").innerHTML="";
				  return;
				  } 
          var x="getteacher_dashboard.php?id="+id;
				 if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
						xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
						xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
						  
						xmlhttp.open("GET",x,true);
						xmlhttp.send();     
						xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						document.getElementById("hint_teacher").innerHTML=xmlhttp.responseText;
					}
				  }
				}
				function GettId(val)
				{
					document.getElementById("tid").value=val;
					document.getElementById("hint_teacher").innerHTML="";
				}
			function validate_student()
			{
			var sid=document.getElementById("sid").value;
			if(sid=="")
			{
			alert("This field is required");
			document.getElementById("sid").focus();
			return false;
			}
			}
      function validate_studentSearch()
			{
			var studentid=document.getElementById("studentid").value;
			if(studentid=="")
			{
			alert("This field is required");
			document.getElementById("studentid").focus();
			return false;
			}
			}
			function validate_teacher()
			{
			var tid=document.getElementById("tid").value;
			if(tid=="")
			{
			alert("This field is required");
			document.getElementById("tid").focus();
			return false;
			}
			}
			
		</script>
<script>
var table = $('#customers2').DataTable();
var tour_status=$("#tour_status").val(); 
// #column3_search is a <input type="text"> element
$('#column3_search').on( 'keyup', function () {
    table
        .columns(1 )
        .search( this.value )
        .draw();
} );
</script> 
<script>
    $(document).ready(function(){
      $(".start-tour").click(function(){
        tour.start();
      });

      

		const tour = new Shepherd.Tour({
      useModalOverlay: true,
  defaultStepOptions: {
    cancelIcon: {
      enabled: true
    },
    classes: 'class-1 class-2',
    scrollTo: { behavior: 'smooth', block: 'center' }
  }
});

tour.addStep({
  title: 'Menu',
  text: `Click here to view all the Menu.`,
  attachTo: {
    element: '.hero-example',
    on: 'bottom'
  },
  buttons: [
    {
      action() {
        return this.complete();
      },
      classes: 'shepherd-button-secondary hide-me-btn',
      text: '<i class="fa fa-eye-slash" aria-hidden="true" onClick="disable_tour();"></i>' 
    },
    {
      action() {
        return this.next();
      },
      text: 'Next'
    }
  ],
  id: 'creating'
});

tour.addStep(	{ 
  title: 'Dashboard Tour & User Guide',
  text: `Click here to start the Tour or User Guide`,
  attachTo: {
    element: '.hero-example2',
    on: 'bottom'
  },
  buttons: [
    {
      action() {
        return this.complete();
      },
      classes: 'shepherd-button-secondary hide-me-btn',
      text: '<i class="fa fa-eye-slash" aria-hidden="true" onClick="disable_tour();"></i>' 
    },
    {
      action() {
        return this.back();
      },
      classes: 'shepherd-button-secondary',
      text: 'Back'
    },
    {
      action() {
        return this.next();
      },
      text: 'Next'
    }
  ],
  id: 'creating'
});

tour.addStep(	{ 
  title: 'Messages',
  text: `Click here to view the Messages.`,
  attachTo: {
    element: '.hero-example3',
    on: 'bottom'
  },
  buttons: [
    {
      action() {
        return this.complete();
      },
      classes: 'shepherd-button-secondary hide-me-btn',
      text: '<i class="fa fa-eye-slash" aria-hidden="true" onClick="disable_tour();"></i>' 
    },
    {
      action() {
        return this.back();
      },
      classes: 'shepherd-button-secondary',
      text: 'Back'
    },
    {
      action() {
        return this.next();
      },
      text: 'Next'
    }
  ],
  id: 'creating'
});

tour.addStep(	{ 
  title: 'Notifications',
  text: `Click here to view the Notifications.`,
  attachTo: {
    element: '.hero-example4',
    on: 'bottom'
  },
  buttons: [
    {
      action() {
        return this.complete();
      },
      classes: 'shepherd-button-secondary hide-me-btn',
      text: '<i class="fa fa-eye-slash" aria-hidden="true" onClick="disable_tour();"></i>' 
    },
    {
      action() {
        return this.back();
      },
      classes: 'shepherd-button-secondary',
      text: 'Back'
    },
    {
      action() {
        return this.next();
      },
      text: 'Next'
    }
  ],
  id: 'creating'
});

tour.addStep(	{ 
  title: 'Logout',
  text: `Click here to Logout.`,
  attachTo: {
    element: '.hero-example5',
    on: 'bottom'
  },
  buttons: [
    {
      action() {
        return this.complete();
      },
      classes: 'shepherd-button-secondary hide-me-btn',
      text: '<i class="fa fa-eye-slash" aria-hidden="true" onClick="disable_tour();"></i>' 
    },
    {
      action() {
        return this.back();
      },
      classes: 'shepherd-button-secondary',
      text: 'Back'
    },
    {
      action() {
        return this.next();
      },
      text: 'Next'
    }
  ],
  id: 'creating'
});

tour.addStep(	{ 
  title: 'Search Student',
  text: `Search Student here.`,
  attachTo: {
    element: '.hero-example6',
    on: 'bottom'
  },
  buttons: [
    {
      action() {
        return this.complete();
      },
      classes: 'shepherd-button-secondary hide-me-btn',
      text: '<i class="fa fa-eye-slash" aria-hidden="true" onClick="disable_tour();"></i>' 
    },
    {
      action() {
        return this.back();
      },
      classes: 'shepherd-button-secondary',
      text: 'Back'
    },
    {
      action() {
        return this.next();
      },
      text: 'Next'
    }
  ],
  id: 'creating'
});

tour.addStep(	{ 
  title: 'Search Family',
  text: `Search Family here.`,
  attachTo: {
    element: '.hero-example7',
    on: 'bottom'
  },
  buttons: [
    {
      action() {
        return this.complete();
      },
      classes: 'shepherd-button-secondary hide-me-btn',
      text: '<i class="fa fa-eye-slash" aria-hidden="true" onClick="disable_tour();"></i>' 
    },
    {
      action() {
        return this.back();
      },
      classes: 'shepherd-button-secondary',
      text: 'Back'
    },
    {
      action() {
        return this.next();
      },
      text: 'Next'
    }
  ],
  id: 'creating'
});

tour.addStep(	{ 
  title: 'Search Staff',
  text: `Search Staff here.`,
  attachTo: {
    element: '.hero-example8',
    on: 'bottom'
  },
  buttons: [
    {
      action() {
        return this.complete();
      },
      classes: 'shepherd-button-secondary hide-me-btn',
      text: '<i class="fa fa-eye-slash" aria-hidden="true" onClick="disable_tour();"></i>' 
    },
    {
      action() {
        return this.back();
      },
      classes: 'shepherd-button-secondary',
      text: 'Back'
    },
    {
      action() {
        return this.next();
      },
      text: 'Next'
    }
  ],
  id: 'creating'
});

tour.addStep(	{ 
  title: 'Total Students',
  text: `Total Students strength.`,
  attachTo: {
    element: '.hero-example9',
    on: 'bottom'
  },
  buttons: [
    {
      action() {
        return this.complete();
      },
      classes: 'shepherd-button-secondary hide-me-btn',
      text: '<i class="fa fa-eye-slash" aria-hidden="true" onClick="disable_tour();"></i>' 
    },
    {
      action() {
        return this.back();
      },
      classes: 'shepherd-button-secondary',
      text: 'Back'
    },
    {
      action() {
        return this.next();
      },
      text: 'Next'
    }
  ],
  id: 'creating'
});

tour.addStep(	{ 
  title: 'Total Fee',
  text: `Total Fee Allocated.`,
  attachTo: {
    element: '.hero-example10',
    on: 'bottom'
  },
  buttons: [
    {
      action() {
        return this.complete();
      },
      classes: 'shepherd-button-secondary hide-me-btn',
      text: '<i class="fa fa-eye-slash" aria-hidden="true" onClick="disable_tour();"></i>' 
    },
    {
      action() {
        return this.back();
      },
      classes: 'shepherd-button-secondary',
      text: 'Back'
    },
    {
      action() {
        return this.next();
      },
      text: 'Next'
    }
  ],
  id: 'creating'
});

tour.addStep(	{ 
  title: "Today's Collection",
  text: `Today's Fee Collection.`,
  attachTo: {
    element: '.hero-example11',
    on: 'bottom'
  },
  buttons: [
    {
      action() {
        return this.complete();
      },
      classes: 'shepherd-button-secondary hide-me-btn',
      text: '<i class="fa fa-eye-slash" aria-hidden="true" onClick="disable_tour();"></i>' 
    },
    {
      action() {
        return this.back();
      },
      classes: 'shepherd-button-secondary',
      text: 'Back'
    },
    {
      action() {
        return this.next();
      },
      text: 'Next'
    }
  ],
  id: 'creating'
});

tour.addStep(	{ 
  title: 'Total Dis.',
  text: `Total Discount Applied.`,
  attachTo: {
    element: '.hero-example12',
    on: 'bottom'
  },
  buttons: [
    {
      action() {
        return this.complete();
      },
      classes: 'shepherd-button-secondary hide-me-btn',
      text: '<i class="fa fa-eye-slash" aria-hidden="true" onClick="disable_tour();"></i>' 
    },
    {
      action() {
        return this.back();
      },
      classes: 'shepherd-button-secondary',
      text: 'Back'
    },
    {
      action() {
        return this.next();
      },
      text: 'Next'
    }
  ],
  id: 'creating'
});

tour.addStep(	{ 
  title: 'Total Collection',
  text: `Total Fee Collection.`,
  attachTo: {
    element: '.hero-example13',
    on: 'bottom'
  },
  buttons: [
    {
      action() {
        return this.complete();
      },
      classes: 'shepherd-button-secondary hide-me-btn',
      text: '<i class="fa fa-eye-slash" aria-hidden="true" onClick="disable_tour();"></i>' 
    },
    {
      action() {
        return this.back();
      },
      classes: 'shepherd-button-secondary',
      text: 'Back'
    },
    {
      action() {
        return this.next();
      },
      text: 'Next'
    }
  ],
  id: 'creating'
});

tour.addStep(	{ 
  title: 'Fee Status',
  text: `Click here to view Fee Status.`,
  attachTo: {
    element: '.hero-example14',
    on: 'bottom'
  },
  buttons: [
    {
      action() {
        return this.complete();
      },
      classes: 'shepherd-button-secondary hide-me-btn',
      text: '<i class="fa fa-eye-slash" aria-hidden="true" onClick="disable_tour();"></i>' 
    },
    {
      action() {
        return this.back();
      },
      classes: 'shepherd-button-secondary',
      text: 'Back'
    },
    {
      action() {
        return this.next();
      },
      text: 'Next'
    }
  ],
  id: 'creating'
});

tour.addStep(	{ 
  title: 'Users Activity',
  text: `Last login details.`, 
  attachTo: {
    element: '.hero-example15',
    on: 'top'
  },
  buttons: [
    {
      action() {
        return this.complete();
      },
      classes: 'shepherd-button-secondary hide-me-btn',
      text: '<i class="fa fa-eye-slash" aria-hidden="true" onClick="disable_tour();"></i>' 
    },
    {
      action() {
        return this.back();
      },
      classes: 'shepherd-button-secondary',
      text: 'Back'
    },
    {
      action() {
        return this.next();
      },
      text: 'Next'
    }
  ],
  id: 'creating'
});
  // classes: 'full-menu-tour',

tour.addStep(	{ 
  title: 'Login History',
  text: `Click the username to get the Activity logs.`,

  attachTo: {
    element: '.hero-example16',
    on: 'top'
  },
  buttons: [
    {
      action() {
        return this.complete();
      },
      classes: 'shepherd-button-secondary hide-me-btn',
      text: '<i class="fa fa-eye-slash" aria-hidden="true" onClick="disable_tour();"></i>' 
    },
    {
      action() {
        return this.back();
      },
      classes: 'shepherd-button-secondary',
      text: 'Back'
    },
    {
      action() {
        return this.next();
      },
      text: 'Next'
    }
  ],
  id: 'creating'
});

tour.addStep(	{ 
  title: 'Main Modules',
  text: `Main Modules are available here.`,
  classes: 'full-menu-tour',
  attachTo: {
    element: '.hero-example17',
    on: 'right'
  },
  buttons: [
    {
      action() {
        return this.complete();
      },
      classes: 'shepherd-button-secondary hide-me-btn',
      text: '<i class="fa fa-eye-slash" aria-hidden="true" onClick="disable_tour();"></i>' 
    },
    {
      action() {
        return this.back();
      },
      classes: 'shepherd-button-secondary',
      text: 'Back'
    },
    {
      action() {
        return this.next();
      },
      text: 'Next'
    }
  ],
  id: 'creating'
});

tour.addStep(	{ 
  title: 'General Settings',
  text: `Sub Modules can be viewed, when you click Next.`,
  attachTo: {
    element: '.hero-example18',
    on: 'bottom'
  },
  buttons: [
    {
      action() {
        return this.complete();
      },
      classes: 'shepherd-button-secondary hide-me-btn',
      text: '<i class="fa fa-eye-slash" aria-hidden="true" onClick="disable_tour();"></i>' 
    },
    {
      action() {
        return this.back();
      },
      classes: 'shepherd-button-secondary',
      text: 'Back'
    },
    {
      action() {
        window.location.href = "year_term.php?page=year_term.php&tour=active";
       // return this.next();
      },
      text: 'Next'
    }
  ],
  id: 'creating'
});

if(tour_status=="active"){
  tour.start();
}
    });

    $(document).ready(function () {
		
    $.ajax({
    url: '../admin/feepaid_unpaid_js.php',
    type: 'GET',
    data:{feetype:"db_student"},
    async: true,
    dataType: "json",
    success: function (data) {
      feepaid_unpaid_chart(data,"container2");
    }
    });
    $.ajax({
    url: '../admin/feepaid_unpaid_js.php',
    type: 'GET',
    data:{feetype:"other"},
    async: true,
    dataType: "json",
    success: function (data) {
      feepaid_unpaid_chart(data,"container5");
    }
    });
    function feepaid_unpaid_chart (output_data,div) {
        if(div=="container2"){
            paid=output_data.TotalPaid_graph;
            Due=output_data.Total_Due_graph;
            OverDue=output_data.Total_OverDue_graph;
            Future=output_data.Total_FuturePayment_graph;
        }else{
            paid=output_data.TotalPaid_graph_new;
            Due=output_data.Total_Due_graph_new;
            OverDue=output_data.Total_OverDue_graph_new;
            Future=output_data.Total_FuturePayment_graph_new;
        }
      $("#total_collection").html(output_data.Total_collection);
      var output_data = [['Paid', paid],['Due',  Due],['OverDue',  OverDue],['Future',  Future]];
      $('#'+div).highcharts({

          chart: {
              plotBackgroundColor: null,
              plotBorderWidth: null,
              plotShadow: false,
              type: 'pie'
          },
          legend: {
                  align: 'left',
                  layout: 'vertical',
                  verticalAlign: 'top',
                  x: 0,
                  y: 0
                },
        
          plotOptions: {
              pie: {
                size:'80%',
                allowPointSelect: true,
                cursor: 'pointer',
                innerSize: '50%',
                dataLabels: {
                    enabled: true,
                    format: '{point.name}<br>{point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                
        point: {
        events:{
                    click: function (event) {
           //alert(this.id + " " + this.x);
           if (this.x == 0) {
             //window.location="fee_collection_report.php?types=paid_unpaid&acyear=2016-17&stype=combined&type=section_wise&classno=-1&feetype=academic&term=I+Term&ptype=paid&btnadd=Go#";
             window.location="fee_collection_report.php";
          } else {
             //window.location="fee_collection_report.php?types=paid_unpaid&acyear=2016-17&stype=combined&type=section_wise&classno=-1&feetype=academic&term=I+Term&ptype=unpaid&btnadd=Go#";
             window.location="fee_collection_report.php";
          }      

                  }
                  }
        },
                  showInLegend: true
              }
          },
		  colors: ['#1CAF9A', '#FFA500', '#FF0000', '#0000FF'],
          series: [{
      innerSize: '50%',
              name: 'Report',
              colorByPoint: true,
          data: output_data
          }],
    navigation: {
           buttonOptions: {
              enabled: false
          }
      }
    
      });
    }

      // Build the chart
     
  });
  function LoadUserActivity(userid){
    $.ajax({
       type: "GET",
       url: "../admin/GetUserActivityLog.php",
       data:{userid:userid}, // serializes the form's elements.
      dataType:"json",
       success: function(data)
       {
        $("#useractivity_name").html(data.name);
        $("#useractivity_log").html(data.data);
       }
     });
    }
    function getPeriodUserActivity(val){
        $.ajax({
			url: "../admin/getPeriodUserActivity.php",
			type: "GET",
			data: {type:val} ,
			success: function (response) {
			$('#period_div').html(response);
			},
			error: function(jqXHR, textStatus, errorThrown) {
			console.log(textStatus, errorThrown);
			}
		});
}
function ViewMoreStudent(from,to){
    to=to+7;
    id=document.getElementById("studentid").value;
    $.ajax({
        url: "../admin/getstudents_studentID.php",
        type: "GET",
	    	data: {id:id,from:from,to:to} ,
        success: function (response) {
          $('#ind_studentTable tr:last').remove();
        //  $('#hint_student1').find('tr:last').prev().after(response);
        $("#ind_studentTable").append(response);
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
	}
  function ViewMoreFamily(from,to){
    to=to+7;
    id=document.getElementById("sid").value;
    $.ajax({
        url: "../admin/getstudentsFamilycode.php",
        type: "GET",
	    	data: {id:id,from:from,to:to} ,
        success: function (response) {
          $('#Famiy_studentTable tr:last').remove();
        //  $('#hint_student1').find('tr:last').prev().after(response);
        $("#Famiy_studentTable").append(response);
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
	}
  function ViewMoreTeacher(from,to){
    to=to+7;
    id=document.getElementById("tid").value;
    $.ajax({
        url: "../admin/getteacher_dashboard.php",
        type: "GET",
	    	data: {id:id,from:from,to:to} ,
        success: function (response) {
          $('#Staff_Table tr:last').remove();
        //  $('#hint_student1').find('tr:last').prev().after(response);
        $("#Staff_Table").append(response);
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
	}
</script>
    <!-- END SCRIPTS -->    
 <?php } ?>