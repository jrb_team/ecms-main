<?php
require_once('../db/createdb.php');
require_once('../db/notification.php');			
$dbobject = new DB();
$dbobject->getCon();
$notification=new notification();

$sid=$student_det['sid'];
$parent_id=$student_det['parent_id'];
$user_to="admin";
$notify_det=$notification->notify_det("all",$user_to,1);
$notification_read_status=$notification->notification_read_status("all",$user_to,1);
$schoolinfo=$dbobject->selectall("schoolinfo",array("id"=>1));
$count=$notify_det['count'];
if($count=="0")
{
	$count="";	
}					
$data=$notify_det['data'];		
?>
<div class="page-topbar">
    <button class="hero-example toggle-menu-btn collapsed">
        <span></span>
        <span></span>
        <span></span>
    </button>
    <a href="#" class="logo-area">
        <img src="uploads/<?php echo $schoolinfo['image_location']; ?>">
    </a>
    <h3 class="page-nav-title" id='main_menu'></h3>
    <div class="user-guide-box">
        <div class="user-guide-box-head">
            <input id="tag-search" type="text" placeholder="Search..." autofocus onKeyUp='Userguide(this.value)'>
            <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
            <a href="#" class="user-guide-box-close-btn">
                <img src="img/close.png">
            </a>
        </div>
        <div class="user-guide-box-body" id="userGuide_content">
        </div>
    </div>
    <ul class="x-navigation x-navigation-horizontal x-navigation-panel top-nav-list">
    <li class="xn-icon-button hero-example2">
        <a href="#" data-toggle="tooltip" title="Dashboard Tour, User Guide" data-placement="bottom"><i class="fa fa-info-circle shake-new" aria-hidden="true"></i></a>
    <ul class="t-dropdown">
        <li>
            <a class="start-tour" href="#">Dashboard Tour</a>
        </li>
        <li>
            <a class="user-g-btn" href="#">User Guide</a>
        </li>
    </ul>
    </li>
        <li class="xn-icon-button hero-example3" data-toggle="tooltip" title="Message" data-placement="bottom">
            <a href="#" >
                <i class="fa fa-envelope" aria-hidden="true"></i>
                <!--<span class="badge badge-accent">7</span>-->
            </a>
            <div class="informer informer-warning"></div>
            <div class="panel panel-primary drop_menu animated zoomIn xn-drop-left xn-panel-dragging">
                <div class="panel-body list-group scroll" style="height: 200px;">                                
                       <div class="message-ul-list">
                           <!--<div class="message-li">
                               <a href="#">
                                            <div class="user-img">
                                                <img src="../admin/img/avatar.png" class="img-circle img-inline">
                                            </div>
                                            <div>
                                                <span class="name">
                                                    <strong>John Doe</strong>
                                                    <span class="time small">- 15 mins ago</span>
                                                </span>
                                                <span class="desc small">
                                                    this is a dummy text.
                                                </span>
                                            </div>
                                        </a>
                           </div>
                           <div class="message-li">
                               <a href="#">
                                            <div class="user-img">
                                                <img src="../admin/img/avatar.png" class="img-circle img-inline">
                                            </div>
                                            <div>
                                                <span class="name">
                                                    <strong>John Doe</strong>
                                                    <span class="time small">- 15 mins ago</span>
                                                </span>
                                                <span class="desc small">
                                                    this is a dummy text.
                                                </span>
                                            </div>
                                        </a>
                           </div>
                           <div class="message-li">
                               <a href="#">
                                            <div class="user-img">
                                                <img src="../admin/img/avatar.png" class="img-circle img-inline">
                                            </div>
                                            <div>
                                                <span class="name">
                                                    <strong>John Doe</strong>
                                                    <span class="time small">- 15 mins ago</span>
                                                </span>
                                                <span class="desc small">
                                                    this is a dummy text.
                                                </span>
                                            </div>
                                        </a>
                           </div>
                           <div class="message-li">
                               <a href="#">
                                            <div class="user-img">
                                                <img src="../admin/img/avatar.png" class="img-circle img-inline">
                                            </div>
                                            <div>
                                                <span class="name">
                                                    <strong>John Doe</strong>
                                                    <span class="time small">- 15 mins ago</span>
                                                </span>
                                                <span class="desc small">
                                                    this is a dummy text.
                                                </span>
                                            </div>
                                        </a>
                           </div>-->
                       </div>         
                </div>     
                <div class="panel-footer text-center">
                    <a href="message_inbox.php?page=message_inbox.php">Show All Messages</a>
                </div>                            
            </div>                        
        </li>
                    <!-- TOGGLE NAVIGATION -->
                   <!-- <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>-->
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SEARCH -->
                    <!--<li class="xn-search">
                        <form role="form">
                            <input type="text" name="search" placeholder="Search..."/>
                        </form>
                    </li>  
                    
                    <li class="xn-search">
                        <form role="form">
                            <input type="text" name="search" placeholder="Search..."/>
                        </form>
                    </li> -->  
                    <!-- END SEARCH -->
                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right hero-example5">
                        <a href="#" class="mb-control" data-box="#mb-signout" data-toggle="tooltip" title="Logout" data-placement="bottom"><i class="fa fa-power-off" aria-hidden="true"></i></a>                        
                    </li> 
                    <!-- TASKS -->
					<?php


					
					?>
                    <li class="xn-icon-button pull-right hero-example4">
                        <a href="#" data-toggle="tooltip" title="Notification" data-placement="bottom">
                            <i class="fa fa-bell" aria-hidden="true"></i>
                        <span class="badge badge-accent"><?php echo $count;?></span>
                        </a>
					   
                        <div class="panel panel-primary drop_menu animated zoomIn xn-drop-left xn-panel-dragging">
                            <div class="panel-heading">
                                    You have <strong><?php if($count!=""){ echo ''.$count.' ';}?></strong> new notifications.
                            </div>
                            <div class="panel-body list-group scroll" style="height: 200px;">                                
                                               
                              <?php
                                    if(!empty($data))
									{
										foreach($data as $dt)
										{
											$type=$dt['type'];
											$type_id=$dt['type_id'];
											$nid=$dt['nid'];
											$date_issued=$dt['date_issued'];
											echo $notify_link=$notification->notify_link($type,$type_id,$nid,$date_issued);
											
										}
									}
                                    if(!empty($notification_read_status))
									{
										foreach($notification_read_status as $dt2)
										{
											 $time_read=$dt2['time_read'];
											 $date_read=date('d-m-Y',strtotime($dt2['date_read']));
											 $type=$dt2['type'];
											 $user_from=$dt2['user_from'];
											 $userid_from=$dt2['userid_from'];
											 if($user_from=="parent")
											 {
												 $parent_det=$dbobject->selectall("parent",array("id"=>$userid_from));
												 $name=$parent_det['fname'];
											 }
										//	 echo "<b> Notification seen from ".$user_from."  ".$name." on ".$date_read." at ".$time_read."</b>";
										}
									}										
							  ?>
								
                            </div>     
                            <div class="panel-footer text-center">
                            </div>                             
                        </div>                        
                    </li>
                    <!-- END TASKS -->
                </ul>
</div>
<script>
    $(document).ready(function(){
        $("a.start-tour, a.user-g-btn").click(function(){
        $("ul.top-nav-list li").removeClass("active");
      });

        $(".user-g-btn").click(function(){
            $(".user-guide-box").css("display", "inline-block");
            // $(".top-nav-list").hide();
        });
        $(".user-guide-box-close-btn").click(function(){
            $(".user-guide-box").css("display", "none");
            // $(".top-nav-list").show();
        });

        $("#tag-search").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#myList li a").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });

        $("a.show-content-btn").click(function(){
            $(".show-content").toggleClass("in");
        });
        $("a.show-content-btn-one").click(function(){
            $(".show-content-one").toggleClass("in");
        });
        $("a.show-content-btn-two").click(function(){
            $(".show-content-two").toggleClass("in");
        });
        $("a.show-content-btn-three").click(function(){
            $(".show-content-three").toggleClass("in");
        });

    });
    
</script>