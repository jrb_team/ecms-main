<?php
include_once('../db/createdb.php');
$dbobject = new DB();
if (!$dbobject->getCon())
{
echo "Connection Failed";
die(mysql_error());
}
else
{
    $table="";
    $sel_staff=$dbobject->select("SELECT `id`,`userid`,`name`,`middle_name`,`lname`,`login`,`ldate`,`ltime`,`logout_date`,`logout_time` FROM `teacher` WHERE `status`='1' order by `name`");
    while($row=$dbobject->fetch_array($sel_staff)){
        $interval="";
        $table.="<tr><td class='hero-example16'><a href='#' data-toggle='modal' onClick='LoadUserActivity(".$row['id'].")' data-target='#UserActivity' style='color: #333;'><strong>".$row['name']." ".$row['lname']."</strong></a></td><td>";
        if($row['login']=="1"){
            $table.="<span class='label label-success'>Logged in</span>";
        }
        else{
            $table.="<span class='label label-danger'>Logged out</span>";
        }
        $table.="</td>";
        $table.="<td style='font-size: 12px;'>";
        if($row['ldate']!='0000-00-00'){
            $table.=date('d-m-Y',strtotime($row['ldate']))." ".$row['ltime'];
        }
        $table.="</td><td style='font-size: 12px;'>";
        if($row['logout_date']!='0000-00-00'){
            $table.=date('d-m-Y',strtotime($row['logout_date']))." ".$row['logout_time'];
            if($row['ltime']!='00:00:00' && $row['logout_time']!='00:00:00'){
                if($row['userid']!='admin'){
                $time1 = new DateTime($row['ltime']);
                $time2 = new DateTime($row['logout_time']);
                $timediff = $time1->diff($time2);
                $interval=$timediff->format('%h:%i:%s');
                }else{
                    $interval ="";
                }
            }else{
                $interval ="";
            }
        }
        $table.="</td><td>".$interval."</td></tr>";
    }
}
echo $table;
?>
                             
                        