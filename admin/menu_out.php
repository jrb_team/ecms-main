<?php
 require_once('../db/createdb.php');
 $dbobject = new DB();
 $dbobject->getCon();
 $stf_id=$_SESSION['userid'];
 $queryw = "SELECT *  FROM admin_privillage_control WHERE `staff_id` ='".$stf_id."'";
 $rsa=$dbobject->select($queryw);

 if(isset($_GET['val'])){
    $val=$_GET['val'];
}
else{
    $val="";
}
 
?>
<div>
<div class="row">
    <div class="col-sm-12">
        <div class="timeline-centered">
            <article class="timeline-entry">
                <div class="timeline-entry-inner">
                    <div class="timeline-icon bg-success">
                        <i class="fa fa-circle" aria-hidden="true"></i>
                        <div class="timeline-icon-label">
                            General Settings
                        </div>
                    </div>
                    <div class="timeline-label">
                        <div class="mobile-title">
                            General Settings
                        </div>
                         <!-- START WIDGET MESSAGES -->
                         <ul>
                             <li> 
                        <div class="b-t-n" onclick="location.href='year_term.php';">
                            <div class="widget-title"><i class="flaticon-calendar"></i><span>Year & Term</span></div>   
                        </div>
                        </li>
														<?php
							$qry=$dbobject->select("SELECT * FROM `general_setting` WHERE `type`='classsetting'");
							$row=$dbobject->fetch_array($qry);
							$value=$row['value'];
							if($value=="sectionwise")
							{
							?>
							<li>
                            <div class="b-t-n" onclick="location.href='sclass_section_out.php';">							
							<?php } else
							{  ?>
                            <div class="b-t-n" onclick="location.href='add_edit_class.php';">						
						    <?php
							}
							
							?>

                                        
<div class="widget-title">
                                        <i class="flaticon-classroom"></i>
                                <span>Section / Class Room Management</span>
                                        </div>
                            </div> 
                            </li>
                            <li>
                        <div class="b-t-n" onclick="location.href='department_management_section.php?back=index.php';">
                        <div class="widget-title">
                                <i class="flaticon-portfolio"></i>
                                <span>Portfolio Management</span>
                                </div> 
                        </div>
                        </li>
                        
                        <li> 
                            <div class="b-t-n" onclick="location.href='password1.php';">
                            <div class="widget-title">
                                    <i class="flaticon-password"></i>
                                <span>Change Password</span>
                                    </div> 
                            </div>
                        </li>
                        <li> 
                            <div class="b-t-n" onclick="location.href='reciept_format.php';"> 
                                <div class="widget-title">
                                <i class="flaticon-test"></i>
                                <span>Reciept Format</span>
                                </div>
                            </div>
                        </li>
                        <li> 
                            <div class="b-t-n" onclick="location.href='template_create.php';">
                                <div class="widget-title">
                                    <i class="flaticon-envelope"></i>
                                <span>Email Templates</span>
                                    </div> 
                            </div>
                        </li>
                        <li> 
                            <div class="b-t-n" onclick="location.href='school_info.php';">
                                <div class="widget-title">
                                        <i class="flaticon-approve"></i>
                                <span>School Info</span>
                                        </div> 
                            </div>
                        </li>
                        <li>
                        <div class="b-t-n" onclick="location.href='mail_settings.php';">
                            <div class="widget-title">
                            <i class="fa fa-cogs" aria-hidden="true"></i>
                                <span>Email Settings</span>
                                </div>
                        </div>
                        </li>
                        </ul>
                    </div>
                </div>
            </article>
            
           
            
            
            <article class="timeline-entry">
                <div class="timeline-entry-inner">
                    <div class="timeline-icon bg-success">
                        <i class="fa fa-circle" aria-hidden="true"></i>
                        <div class="timeline-icon-label">
                            Alert
                        </div>
                    </div>
                    <div class="timeline-label">
                        <div class="mobile-title">
                            Alert
                        </div>
                        <ul>
                            <li>
                                <div class="b-t-n" onclick="location.href='sms_.php';">
                            <div class="widget-title">
                                <i class="flaticon-chat"></i>
                                <span>Send SMS</span>
                                </div>
                        </div>
                            </li>
                            <li>
                                <div class="b-t-n" onclick="location.href='sms_report.php';">
                                <div class="widget-title">
                                <i class="flaticon-message"></i>
                                <span>SMS Report</span>
                                </div>
                        </div>
                            </li>
                            <li>
                                <div class="b-t-n" onclick="location.href='group.php';">
                                <div class="widget-title">
                                <i class="flaticon-group"></i>
                                <span>Group</span>
                                </div>
                        </div>
                            </li>
                            <li>
                                <div class="b-t-n" onclick="location.href='mail_inbox.php';">
                                <div class="widget-title">
                                <i class="flaticon-speech-bubble"></i>
                                <span>Message</span>
                                </div>
                        </div>
                            </li>
                            <li>
                                <div class="b-t-n" onclick="location.href='email_compose_admin.php';">
                                <div class="widget-title">
                                <i class="flaticon-email"></i>
                                <span>Email</span>
                                </div>
                        </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </article>
            
            
            <article class="timeline-entry">
                <div class="timeline-entry-inner">
                    <div class="timeline-icon bg-success">
                        <i class="fa fa-circle" aria-hidden="true"></i>
                        <div class="timeline-icon-label">
                            Fee Management
                        </div>
                    </div>
                    <div class="timeline-label">
                        <div class="mobile-title">
                            Fee Management
                        </div>
                        <ul>
                            <li>
                            <div class="b-t-n" onclick="location.href='create_fees.php';">
                            <div class="widget-title">
                                <i class="flaticon-wage"></i>
                                <span>Create / Edit Fees</span>
                                </div>
                        </div>
                            </li>
                            <li>
                            <div class="b-t-n" data-toggle="modal" data-target="#exampleModal2">
                            <div class="widget-title">
                                <i class="flaticon-wage"></i>
                                <span>Fee Collection</span>
                                </div>
                        </div>
                            </li>
                            <li>
                            <div class="b-t-n" onclick="location.href='fee_collection_report.php';">
                            <div class="widget-title">
                                <i class="flaticon-stock-market"></i>
                                <span>Fee Report</span>
                                </div>
                        </div>
                            </li>
                            <li>
                            <div class="b-t-n" onclick="location.href='#openModal3';">
                            <div class="widget-title">
                                <i class="flaticon-certificate"></i>
                                <span>Fee Certificate</span>
                                </div>
                        </div>
                            </li>
                            <li>
                            <div class="b-t-n" onclick="location.href='academic_nonacademic_group.php';">
                            <div class="widget-title">
                                <i class="flaticon-appraisal-form"></i>
                                <span>Academic / Non Academic Group</span>
                                </div>
                        </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </article>

             <article class="timeline-entry">
                <div class="timeline-entry-inner">
                    <div class="timeline-icon bg-success">
                        <i class="fa fa-circle" aria-hidden="true"></i>
                        <div class="timeline-icon-label">
                            Payment Settings
                        </div>
                    </div>
                    <div class="timeline-label">
                        <div class="mobile-title">
                            Payment Settings
                        </div>
                        <ul>
                         
                            <li>
                                <div class="b-t-n" onclick="location.href='payment_settings.php';">
                            <div class="widget-title">
                            <i class="fa fa-cogs" aria-hidden="true"></i>
                                <span>Add Settings</span>
                                </div>
                        </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </article>

            <article class="timeline-entry">
                <div class="timeline-entry-inner">
                    <div class="timeline-icon bg-success">
                        <i class="fa fa-circle" aria-hidden="true"></i>
                        <div class="timeline-icon-label">
                            Staff Management
                        </div>
                    </div>
                    <div class="timeline-label">
                        <div class="mobile-title">
                            Staff Management
                        </div>
                        <div class="b-t-n" onclick="location.href='selectstaff.php';">
                        <div class="widget-title">
                                <i class="flaticon-id-card"></i>
                                <span>Staff Details</span>
                                </div>
                        </div>                         
                        <div class="b-t-n" onclick="location.href='teacher_import.php';">
                            <div class="widget-title">
                            <i class="fa fa-cloud-download" aria-hidden="true"></i>
                                <span>Staff Import</span>
                            </div>
                        </div>
                    </div>
                </div>
            </article>

            <article class="timeline-entry">
                <div class="timeline-entry-inner">
                    <div class="timeline-icon bg-success">
                        <i class="fa fa-circle" aria-hidden="true"></i>
                        <div class="timeline-icon-label">
                            Student Management
                        </div>
                    </div>
                    <div class="timeline-label">
                        <div class="mobile-title">
                           Student Management
                        </div>
                        <ul>
                            <li>
                                <div class="b-t-n" onclick="location.href='addstudent.php';">
                                    <div class="widget-title">
                                        <i class="flaticon-add-user"></i>
                                <span>Add New Student</span>
                                    </div>
                                </div> 
                            </li>
                            <li>
                                <div class="b-t-n" data-toggle="modal" data-target="#exampleModal" onClick='FocusToSearch();'>
                                    <div class="widget-title">
                                        <i class="flaticon-edit"></i>
                                <span>Edit Student Profile</span>
                                        </div>
                                </div> 
                            </li>
                            <li>
                                <div class="b-t-n" onclick="location.href='viewclass_.php';">
                                    <div class="widget-title">
                                        <i class="flaticon-skills"></i>
                                <span>Move / Promote</span>
                                        </div>
                                </div>
                            </li>
                            <li>
                                <div class="b-t-n"  onclick="location.href='student_tansfer.php';">
                                    <div class="widget-title">
                                        <i class="flaticon-exit"></i>
                                <span>Transfer/Exit</span>
                                        </div>
                                </div>
                            </li>
                            <li>
                            <div class="b-t-n" onclick="location.href='upload_student.php';">
                                    <div class="widget-title">
                                        <i class="flaticon-import"></i>
                                <span>Student Import</span>
                                        </div>
                                </div>
                            </li>                           
                        </ul>
                    </div>
                </div>
            </article>            
        </div>
    </div>
</div>
</div>                	 
<div>
</div>
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Fee Collection</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action='update_fees.php' method='GET' autocomplete="off">
      <div class="modal-body">
      <div class="form-group">
        Search By
        <label class="form-check-label" for="flexRadioDefault1">
   Family Code
  </label> <input type='radio' id='search_type' name='type' value='family' checked/>
  <label class="form-check-label" for="flexRadioDefault1">
   Application No
  </label>
        <input type='radio' value='applicaion_no' name='type'/>
    </div>
      <div class="form-group">
			        <input class="form-control" type="text" name="family_code" id="studid3" onKeyUp="GetStudents3(this.value)"/>
			        <div id="hint_student3" ></div>
                </div>
      </div>
      <div class="modal-footer">
        <button type="Submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
    </form>
  </div>
</div>

<!-- Modal -->
<div class="modal fade student-search-model" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel">Search Student</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
        <?php if($val==2){
            echo "<center><h5 style='color:red'>Invalid Student</h5></center>";
        }
if($val==3){
            echo "<center><h5 style='color:green'>Student Cancelled</h5></center>";
        }		
        ?>
            
        <form class="edit-student-popup" name="search_student" id="search_student" action="student_det.php" autocomplete="off" >
        <input type='hidden' name='res_val' id='res_val' value='<?php echo $val?>'/>
        <input type="hidden" name="redirect" id="redirect" value="index.php?page=menu"/>
            <div class="input-group">
              <input type="text" placeholder="Student Name" class="form-control" name="studentid" id="sid1" onKeyUp="GetStudents1(this.value)" style="width:100%" autofocus/>
              <span class="input-group-btn">
                <input type="submit" value="Submit" class='btn btn-success'/>
              </span>
            </div>
            
            
            <div id="hint_student1" ></div>
            
            
            
        </div>
        </div>
    </div>
    </div>
    
    
    <div id="openModal3" class="modalDialog">
        <div style="padding: 20px 20px 25px 20px;"><a href="#close" title="Close" class="close" style="top: 12px;">x</a>
			<h3 style=" margin-top: 0;">Search Student</h3>
			<form class="design-form-custom" name="search_student" id="search_student" action="viewcrtfe_pdf.php" autocomplete="off" >
                <div class="form-group">
                    <label>Studentid</label>
			        <input class="form-control" type="text" name="studid" id="studid3" onKeyUp="GetStudents2(this.value,3)"/>
			        <div id="hint_student3" ></div>
                </div>
                <div class="form-group">
                    <label>Academic Year</label>
			        <select class="form-control select" name="acyear" id="acyear"><option value="2016-17">2016-17</option><option value="2017-18">2017-18</option></select>
                </div>
                <div class="form-group">
                    <label>Parent Type</label>
			        <select class="form-control select" name="parent_type" id="parent_type"><option value="fname">Father</option><option value="mname">Mother</option></select>
                </div>
                <input type="submit" class="btn btn-primary" value="submit"/>
			</form>
         </div>
     </div>	
                         
                <!-- END PAGE CONTENT WRAPPER --> 	
<script>  
$('#exampleModal').on('shown.bs.modal', function() {
  $('#sid1').focus();
})
$(document).ready(function() { 
    var res_val=$('#res_val').val();
        if(res_val==2){
            $('#exampleModal').modal('show');
        }
        if(res_val==3){
            $('#exampleModal').modal('show');
        }        
                $(".change_formStatus").click(function() { 
                    if($(this).prop("checked") == true){
                        $.ajax({url: "updateFormStatus.php?val=1&type="+$(this).val(), 
                            success: function(result){
                           
                        }});
                    }
                    else if($(this).prop("checked") == false){
                        $.ajax({url: "updateFormStatus.php?val=0&type="+$(this).val(), 
                            success: function(result){
                     
                        }});
                    }
                }); 
            });
            function GetStudents1(id)
				  {
					if (id=="")
				  {
				  document.getElementById("hint_student1").innerHTML="";
				  return;
				  } 
				  var x="getstudents_studentID.php?id="+id;
				 if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
						xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
						xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
						  
						xmlhttp.open("GET",x,true);
						xmlhttp.send();     
						xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						document.getElementById("hint_student1").innerHTML=xmlhttp.responseText;
					}
				  }
				}
				function GetId_stud(val)
				{
					document.getElementById("sid1").value=val;
					document.getElementById("hint_student1").innerHTML="";
				}
</script>
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 42px;
  height: 18px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 10px;
  width: 10px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
.student-search-model .modal-header{
    background: #0e695e;
    background: -moz-linear-gradient(left, #0e695e 0%, #00b792 100%);
    background: -webkit-linear-gradient(left, #0e695e 0%,#00b792 100%);
    background: linear-gradient(to right, #0e695e 0%,#00b792 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0e695e', endColorstr='#00b792',GradientType=1 );
    cursor: move;
    padding: 20px;
    color: #ffffff;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
}
.student-search-model .modal-header h3{
    color: #fff;
    display: inline-block;
    margin: 0px;
    font-size: 17px;
}
.student-search-model .modal-header button{
    box-shadow: none;
    color: #fff !important;
    opacity: 1;
}
.student-search-model .modal-content{
    border: none;
} 
.student-search-model  #search_student input[type='submit']{
    background: #0e695e;
    background: -moz-linear-gradient(left, #0e695e 0%, #00b792 100%);
    background: -webkit-linear-gradient(left, #0e695e 0%,#00b792 100%);
    background: linear-gradient(to right, #0e695e 0%,#00b792 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#0e695e', endColorstr='#00b792',GradientType=1 );
    border-top-left-radius: 0px;
    border-bottom-left-radius: 0px;
    color: #fff;
}
ul.reminder-popup-list{
    padding: 0px;
    margin: 0px;
    list-style-type: none;
}
ul.reminder-popup-list li .form-group{
    margin-bottom: 10px;
}
ul.reminder-popup-list li .form-group label{
    font-weight: 500;
}
table.no-border-table tr td{
    border-top: none !important;
}
</style>				
<link href="css/style.css" rel="stylesheet">				
<script type="text/javascript" src="js/plugins/bootstrap/bootstrap-select.js"></script>
<link href="css/fixed_header.css" rel="stylesheet" media="screen" /> 
<script>
function GetStudents3(val){
  var type=document.querySelector('input[name="type"]:checked').value;
					if (val=="")
				  {
				  document.getElementById("hint_student3").innerHTML="";
				  return;
				  } 
				  var x="getstudentsAndApplication.php?id="+val+"&branch_id=1&type="+type;
				 if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
						xmlhttp=new XMLHttpRequest();
				  }
				else
				  {// code for IE6, IE5
						xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
				  }
						  
						xmlhttp.open("GET",x,true);
						xmlhttp.send();     
						xmlhttp.onreadystatechange=function()
				  {
				  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						document.getElementById("hint_student3").innerHTML=xmlhttp.responseText;
					}
				  }
}
function GetFamily(family_code){
    document.getElementById("studid3").value=family_code;
    document.getElementById("hint_student3").innerHTML="";
}
</script>