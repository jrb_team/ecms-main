$(function () {
    $('#container_line1').highcharts({
        chart: {
            type: 'areaspline'
        },
        title: {
            text: 'Class Performance'
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 150,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        xAxis: {
            categories: [
                'V A',
                'V B',
                'V C',
                'VI A',
                'VI B',
                'VI C',
                'VII A',
				'VII B',
				'VII C',
				'VII D',
				'VIII A',
				'VIII B',
				'VIII C',
				'VIII D',
				'IX A',
				'IX B',
				'IX C',
				'X A',
				'X B',
				'X C',
				'X D',
				'XI A',
				'XI B',
				'XII A',
				'XII B'
            ],
            plotBands: [{ // visualize the weekend
                from: 4.5,
                to: 6.5,
                color: 'rgba(68, 170, 213, .2)'
            }]
        },
        yAxis: {
            title: {
                text: 'Percentage'
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: ' Percentage'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5
            },
        series: {
            cursor: 'pointer',
            point: {
                events: {
                    click: function () {
                        //alert('Category: ' + this.category +' term:'+this.series.name);
						window.location="index_cumulative.php?classdet="+this.category+"&exam="+this.series.name;
	
                    }
					
                }
            }
			
        }
        },
        series: [{
            name: 'I Term',
            data: [3, 4, 3, 5, 4, 10, 12,3, 4, 3, 5, 4, 10, 12,3, 4, 3, 5, 4, 10, 12,3, 4, 3, 5]
        }, {
            name: 'II Term',
            data: [1, 3, 4, 3, 3, 5, 4,3, 4, 3, 8, 4, 10, 5,3, 4, 3,1, 3, 4, 3, 3, 5, 4,3]
        }],
		navigation: {
             buttonOptions: {
                enabled: false
            }}
    });
});
$(function () {

    $(document).ready(function () {
			var result = null;
				$.ajax({
				async:"false",
				url:"attendance_graph.php",
				type:'POST',
				success:function(data){
				//alert(data);
				res = data.split("||");
				get_attendance_data(res[0]);
				get_staff_attendance_data(res[1]);
				//return data;
				},
				error:function(data){
				alert("error");
				}
				});
				
    });
});

function get_staff_attendance_data(d)
{
    
d=d.split("-");
present=Number(d[0]);
absent=Number(d[1]);
late=Number(d[2]);
var data = [];
 $('#containerpie2').highcharts({
	
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
          
          
            plotOptions: {
                pie: {
                    colors: [
                        '#1CAF9A', 
                        '#33414E', 
                        '#FF0000'
                      ],
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
						 distance: -20,
                    style: {
                        fontWeight: 'bold',
                        color: 'white',
                        textShadow: '0px 1px 2px black'
                    },
						formatter: function() {
                        return Math.round(this.percentage) + ' %';
                    }
                    },
					point: {
					events:{
                      click: function (event) {
					  // alert(this.id + " " + this.x);
	 if (this.x == 0) {
        window.location="student_attn_report.php?page=student_attn_report.php";
    } else if (this.x == 1) {
        window.location="student_attn_report.php?page=student_attn_report.php";
    } else {
        window.location="student_attn_report.php?page=student_attn_report.php";
    }    

	                }
					}
						
					
					},
					series:[
          {
             "data": data,
		  }],
					
                    showInLegend: true
                }
            },
			
			events: {
				click: function () {
					alert("ghg");
                    //reloadFlash();
                    
                },
			},
            series: [{
				innerSize: '50%',
                name: 'Report',
                colorByPoint: true,
            data: [
                ['Present', present],
				['Late',   late],
				['Absent', absent]
    
            ]
            }],
			navigation: {
             buttonOptions: {
                enabled: false
            }
        }
			
        });
        // Build the chart
}
		
		
$(function () {

    var seriesData = [{
        code: 'AF',
        value: 10
    }, {
        code: 'PK',
        value: 115
    }];

    // Initiate the chart
    $('#container_map').highcharts('Map', {


        mapNavigation: {
            enabled: true,
            enableDoubleClickZoomTo: true
        },

        colorAxis: {
            min: 1,
            max: 1000,
            type: 'logarithmic'
        },

        series: [{
            data: seriesData,
            mapData: Highcharts.maps['custom/world'],
            joinBy: ['iso-a2', 'code'],
        }],
		navigation: {
             buttonOptions: {
                enabled: false
            }}
    });
    
    

});
function get_attendance_data(d)
{
d=d.split("-");
present=Number(d[0]);
absent=Number(d[1]);
late=Number(d[2]);
var data = [];
 $('#containerpie1').highcharts({
	
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
          
          
            plotOptions: {
                pie: {
                    colors: [
                        '#1CAF9A', 
                        '#33414E', 
                        '#FF0000'
                      ],
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
						 distance: -20,
                    style: {
                        fontWeight: 'bold',
                        color: 'white',
                        textShadow: '0px 1px 2px black'
                    },
						formatter: function() {
                        return Math.round(this.percentage) + ' %';
                    }
                    },
					point: {
					events:{
                      click: function (event) {
					  // alert(this.id + " " + this.x);
	 if (this.x == 0) {
        window.location="student_attn_report.php?page=student_attn_report.php";
    } else if (this.x == 1) {
        window.location="student_attn_report.php?page=student_attn_report.php";
    } else {
        window.location="student_attn_report.php?page=student_attn_report.php";
    }    

	                }
					}
						
					
					},
					series:[
          {
             "data": data,
		  }],
					
                    showInLegend: true
                }
            },
			
			events: {
				click: function () {
					alert("ghg");
                    //reloadFlash();
                    
                },
			},
            series: [{
				innerSize: '50%',
                name: 'Report',
                colorByPoint: true,
            data: [
                ['Present', present],
				['Late',   late],
				['Absent', absent]
    
            ]
            }],
			navigation: {
             buttonOptions: {
                enabled: false
            }
        }
			
        });
        // Build the chart
}
  $(function () {

    $(document).ready(function () {
	 var data = [];

        // Build the chart
        $('#containerpie4').highcharts({
	
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
          
          
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
						 distance: -20,
                    style: {
                        fontWeight: 'bold',
                        color: 'white',
                        textShadow: '0px 1px 2px black'
                    },
						formatter: function() {
                        return Math.round(this.percentage) + ' %';
                    }
                    },
					point: {
					events:{
                      click: function (event) {
					   //alert(this.id + " " + this.x);
					    // alert('Category: ' + this.name   +' value:'+this.y);
	                  var str=this.name;
	                  var res=str.split(" ");
					  if(res[1]=="+")
					  {
						  res[1]="plus";
					  }
	
                  window.location="index_grade.php?grade="+res[0]+"&grade2="+res[1];
    
	                }
					}
						
					
					},
					series:[
          {
             "data": data,
		  }],
					
                    showInLegend: true
                }
            },
			
			events: {
				click: function () {
					//alert("ghg");
                    //reloadFlash();
                    
                },
			},
            series: [{
				innerSize: '50%',
                name: 'Report',
                colorByPoint: true,
            data: [
			    ['A +',16.33],
				['A ',18.03],
                ['B +',10.38],
				['B ',10.38],
				['C +',10.38],
				['C ',10.38],
				['D ',10.38],
              
             
            ]
            }],
			navigation: {
             buttonOptions: {
                enabled: false
            }
        }
			
        });
    });
});
   $(function () {

    $(document).ready(function () {
	 var data = [];

        // Build the chart
        $('#containerpie5').highcharts({
	
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
          
          
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
						 distance: -20,
                    style: {
                        fontWeight: 'bold',
                        color: 'white',
                        textShadow: '0px 1px 2px black'
                    },
						formatter: function() {
                        return Math.round(this.percentage) + ' %';
                    }
                    },
					point: {
					events:{
                      click: function (event) {
					   //alert(this.id + " " + this.x);
					    // alert('Category: ' + this.name   +' value:'+this.y);
	                  var str=this.name;
	                  var res=str.split(" ");
					  if(res[1]=="+")
					  {
						  res[1]="plus";
					  }
	
                  window.location="index_grade.php?grade="+res[0]+"&grade2="+res[1];
    
	                }
					}
						
					
					},
					series:[
          {
             "data": data,
		  }],
					
                    showInLegend: true
                }
            },
			
			events: {
				click: function () {
					//alert("ghg");
                    //reloadFlash();
                    
                },
			},
            series: [{
				innerSize: '50%',
                name: 'Report',
                colorByPoint: true,
            data: [
			    ['A +',16.33],
				['A ',18.03],
                ['B +',10.38],
				['B ',10.38],
				['C +',10.38],
				['C ',10.38],
				['D ',10.38],
              
             
            ]
            }],
			navigation: {
             buttonOptions: {
                enabled: false
            }
        }
			
        });
    });
});
   $(function () {

    $(document).ready(function () {
	 var data = [];

        // Build the chart
        $('#containerpie6').highcharts({
	
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
          
          
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
						 distance: -20,
                    style: {
                        fontWeight: 'bold',
                        color: 'white',
                        textShadow: '0px 1px 2px black'
                    },
						formatter: function() {
                        return Math.round(this.percentage) + ' %';
                    }
                    },
					point: {
					events:{
                      click: function (event) {
					   //alert(this.id + " " + this.x);
					    // alert('Category: ' + this.name   +' value:'+this.y);
	                  var str=this.name;
	                  var res=str.split(" ");
					  if(res[1]=="+")
					  {
						  res[1]="plus";
					  }
	
                  window.location="index_grade.php?grade="+res[0]+"&grade2="+res[1];
    
	                }
					}
						
					
					},
					series:[
          {
             "data": data,
		  }],
					
                    showInLegend: true
                }
            },
			
			events: {
				click: function () {
					//alert("ghg");
                    //reloadFlash();
                    
                },
			},
            series: [{
				innerSize: '50%',
                name: 'Report',
                colorByPoint: true,
            data: [
			    ['A +',16.33],
				['A ',18.03],
                               ['B +',10.38],
				['B ',10.38],
				['C +',10.38],
				['C ',10.38],
				['D ',10.38],
              
             
            ]
            }],
			navigation: {
             buttonOptions: {
                enabled: false
            }
        }
			
        });
    });
});