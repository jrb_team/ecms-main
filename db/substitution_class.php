<?php
include_once('createdb.php');
include_once('teacher_class.php');
class substitution
{
	function attendence_Substitution($tid,$date)
	 {
		$dbobject = new DB();
		$dbobject->getCon(); 
		$teacher_det=$dbobject->selectall("teacher",array("id"=>$tid));
		$sql=$dbobject->select("SELECT *FROM  `devicelogs` WHERE  `LogDate` LIKE  '%".$date."%' and `userid`='".$teacher_det['deviceid']."'");
		$chk=mysql_num_rows($sql);
		if($chk==0)
		{
		$status="Absent";	
		}
		else
		{
		  $status="Present";
		  $substitution_remark=$this->substitution_remark($tid,$date);  
		  if($substitution_remark!="")
		  {
			 $status=$substitution_remark; 
		  }
          	
		}		
		return $status;
	 }
	function Substitution_classno_by_dep($dep)
	 {
        
		// for section
		if($dep=="KINDERGARTEN")
		{
			$classno[0]=1;
			$classno[1]=2;
			$classno_qry="`classno` IN ('1','2')";
		}
        elseif($dep=="L P SECTION")
		{
			$classno[0]=3;
			$classno[1]=4;			
			$classno[2]=5;			
			$classno[3]=6;	
            $classno_qry="`classno` IN ('3','4','5','6')";			
		}	
        elseif($dep=="U P SECTION")
		{
			$classno[0]=7;
			$classno[1]=8;			
			$classno[2]=9;			
			$classno[3]=10;	
            $classno_qry="`classno` IN ('7','8','9','10')";				
		}	
        elseif($dep=="H S SECTION")
		{
			$classno[0]=11;
			$classno[1]=12;
            $classno_qry="`classno` IN ('11','12')";				
		}
        elseif($dep=="HSS SECTION")
		{
			$classno[0]=13;
			$classno[1]=14;	
            $classno_qry="`classno` IN ('13','14')";			
		}
		$data['classno']=$classno;
		$data['qry']=$classno_qry;
        return $data;	
	 }	 
	function insert_timetable_substitution($classid,$day,$period,$subject,$subject_type,$subject_teacher,$acyear,$userid,$date,$classno,$subject_teacher_substituted)
	{
		$dbobject = new DB();
		$dbobject->getCon();
        $teacher_det=$dbobject->selectall("teacher",array("id"=>$subject_teacher));
		$dep=$teacher_det['dep'];		
		$sql="SELECT * FROM `timetable_substitution` WHERE  `day`='".$day."'  and `period`='".$period."'  ";
		//$sql.="and `subject_teacher`='".$subject_teacher."'";
		$sql.=" and `acyear`='".$acyear."' and `date`='".$date."'";
		$sql.=" and `subject_teacher_substituted`='".$subject_teacher_substituted."'";
	    //echo $sql."<br>";
		$qry=$dbobject->select($sql);
		$chk=mysql_num_rows($qry);
		if($chk==0)
		{
			$sql2="SELECT * FROM `timetable_substitution` WHERE  `day`='".$day."'  and `period`='".$period."'  ";
			$sql2.="and `subject_teacher`='".$subject_teacher."'";
			$sql2.=" and `acyear`='".$acyear."' and `date`='".$date."'";
			 //echo $sql2."<br>";
			$qry2=$dbobject->select($sql2);
			$chk2=mysql_num_rows($qry2);
			if($chk2==0)
			{
				if($classno>=1 && $classno<=12)
				{
					if($period!=9) // no 9th period for class kg to 10 class
					{
						$sql2="INSERT INTO `timetable_substitution` (`classid`, `day`, `period`, `subject`, `subject_teacher`, `subject_type`, `userid`, `date`,`acyear`,`subject_teacher_substituted`)";
						$sql2.=" VALUES ('".$classid."','".$day."','".$period."','".$subject."','".$subject_teacher."','".$subject_type."','".$userid."','".$date."','".$acyear."','".$subject_teacher_substituted."')";
					//if($subject!="AQUATICS GIRLS" && $subject!="AQUATICS BOYS"  && $subject!="AQUATICS" && $subject!="AQUATIC (BOYS)" && $subject!="AQUATIC (GIRLS)")
					if($subject!="ICT")
					{					
						 $datewise_restriction=$this->check_substitution_datewise_restriction($classid,$day,$period,$subject,$subject_teacher_substituted,$subject_type,$acyear,$date);
						 if($datewise_restriction==0)
						 {
						$insert=$dbobject->exe_qry($sql2);	 
						 }
					}
						//echo $sql2;
						//echo "<span style='color:red'>-ins</span><br>";							
					}
				}
				else
				{
				$sql2="INSERT INTO `timetable_substitution` (`classid`, `day`, `period`, `subject`, `subject_teacher`, `subject_type`, `userid`, `date`,`acyear`,`subject_teacher_substituted`)";
				$sql2.=" VALUES ('".$classid."','".$day."','".$period."','".$subject."','".$subject_teacher."','".$subject_type."','".$userid."','".$date."','".$acyear."','".$subject_teacher_substituted."')";
					//if($subject!="AQUATICS GIRLS" && $subject!="AQUATICS BOYS"  && $subject!="AQUATICS" && $subject!="AQUATIC (BOYS)" && $subject!="AQUATIC (GIRLS)")
					if($subject!="ICT")
					{
						if($period==9)
						{
						   if($dep=="HSS SECTION")
						   {
								  $datewise_restriction=$this->check_substitution_datewise_restriction($classid,$day,$period,$subject,$subject_teacher_substituted,$subject_type,$acyear,$date);;
								 if($datewise_restriction==0)
								 {
								$insert=$dbobject->exe_qry($sql2);	 
								 } 
						   }							
						}
						else
						{
							  $datewise_restriction=$this->check_substitution_datewise_restriction($classid,$day,$period,$subject,$subject_teacher_substituted,$subject_type,$acyear,$date);;
							 if($datewise_restriction==0)
							 {
							$insert=$dbobject->exe_qry($sql2);	 
							 }
						}
					}
				
				//echo $sql2;
				//echo "<span style='color:red'>-ins</span><br>";				
				}
			}

		}
		return $insert;
	}
	function update_timetable_substitution($classid,$day,$period,$subject,$subject_type,$subject_teacher,$acyear,$userid,$date,$classno,$subject_teacher_substituted)
	{
		$dbobject = new DB();
		$dbobject->getCon();		 
		$sql="SELECT * FROM `timetable_substitution` WHERE  `day`='".$day."'  and `period`='".$period."'  ";
		//$sql.="and `subject_teacher`='".$subject_teacher."'";
		$sql.=" and `acyear`='".$acyear."' and `date`='".$date."'";
		$sql.=" and `subject_teacher_substituted`='".$subject_teacher_substituted."'";
	    $qry=$dbobject->select($sql);
		$chk=mysql_num_rows($qry);
		if($chk==0)
		{
			$sql2="SELECT * FROM `timetable_substitution` WHERE  `day`='".$day."'  and `period`='".$period."'  ";
			$sql2.="and `subject_teacher`='".$subject_teacher."'";
			$sql2.=" and `acyear`='".$acyear."' and `date`='".$date."'";
			$qry2=$dbobject->select($sql2);
			$chk2=mysql_num_rows($qry2);
			if($chk2==0)
			{
			if($classno>=1 && $classno<=12)
			{
				if($period!=9) // no 9th period for class kg to 10 class
				{
					$sql2="INSERT INTO `timetable_substitution` (`classid`, `day`, `period`, `subject`, `subject_teacher`, `subject_type`, `userid`, `date`,`acyear`,`subject_teacher_substituted`)";
					$sql2.=" VALUES ('".$classid."','".$day."','".$period."','".$subject."','".$subject_teacher."','".$subject_type."','".$userid."','".$date."','".$acyear."','".$subject_teacher_substituted."')";
					$insert=$dbobject->exe_qry($sql2);
					//echo $sql2;
					//echo "<br>";						
				}
			}
			else
			{
			$sql2="INSERT INTO `timetable_substitution` (`classid`, `day`, `period`, `subject`, `subject_teacher`, `subject_type`, `userid`, `date`,`acyear`,`subject_teacher_substituted`)";
			$sql2.=" VALUES ('".$classid."','".$day."','".$period."','".$subject."','".$subject_teacher."','".$subject_type."','".$userid."','".$date."','".$acyear."','".$subject_teacher_substituted."')";
		    $insert=$dbobject->exe_qry($sql2);
			//echo $sql2;
			//echo "<br>";				
			}
			}


		}
			else
			{
				$row=$dbobject->fetch_array($qry);
				$id=$row['id'];
				//echo "UPDATE `timetable_substitution` SET `subject_teacher`='".$subject_teacher."' where`id`='".$id."'";
				$upt=$dbobject->exe_qry("UPDATE `timetable_substitution` SET `subject`='".$subject."',`subject_type`='".$subject_type."',`subject_teacher`='".$subject_teacher."' where`id`='".$id."'");
			}		
		return $insert;
	}	
	function timetable_substitution_count($day,$date,$acyear,$tid)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$qry=$dbobject->select("SELECT * FROM `timetable_substitution` WHERE  `date`='".$date."'  and `subject_teacher`='".$tid."'");
	   // $chk=mysql_num_rows($qry)+1;
	    $chk=mysql_num_rows($qry);
		return $chk;
	}
	function timetable_substitution_count2($day,$date,$acyear,$tid)
	{
        $chk=$this->timetable_substitution_count($day,$date,$acyear,$tid);
		return $chk;
	}	
    function check_teacher_substitution_day_restriction($userid,$day)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$sql="SELECT * FROM `teacher_substitution_day_restriction` WHERE `userid`='".$userid."' and `day`='".$day."'";
	    $qry=$dbobject->select($sql);
		return $chk=mysql_num_rows($qry);		
	}
    function check_teacher_substitution_max_period($userid)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$sql="SELECT * FROM `teacher_substitution_max_period` WHERE `userid`='".$userid."'";
	    $qry=$dbobject->select($sql);
		$row=$dbobject->fetch_array($qry);
		$chk=mysql_num_rows($qry);			
        $data['check']=$chk;		
        $data['max_period']=$row['max_period'];
        return $data;		 
	}	
    function check_class_for_substitution($teacherid,$classno,$acyear,$dep)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$sql="SELECT * FROM `teacher_subject` WHERE `classno`='".$classno."' and `teacherid`='".$teacherid."' ";
		$sql.="and `acyear`='".$acyear."'";
	    $qry=$dbobject->select($sql);
		$chk=mysql_num_rows($qry);
		return $chk;
	}	
	function teacher_periods_daywise_details($day,$acyear,$tid)
	 {
		$dbobject = new DB();
		$dbobject->getCon(); 
		$periods=$dbobject->get_periods($acyear,$classid);
				if(!empty($periods))
				{
					foreach($periods as $p)
					{
						  	$p_det=$dbobject->selectall("period_settings",array("period_name"=>$p));
						    $pid=$p_det['id'];
					    	$sql="select * from  `timetable` where `day`='".$day."' and `acyear`='".$acyear."'";
							$sql.=" and `period`='".$pid."'";									
							$sql.=" and `subject_teacher`='".$tid."'";							
							$sql.=" order by `subject_teacher`";		
							$qry=$dbobject->select($sql);
							$chk=mysql_num_rows($qry);
							$row=$dbobject->fetch_array($qry);
							$classdet=$dbobject->selectall("sclass",array("classid"=>$row['classid']));
							$teacher_det=$dbobject->selectall("teacher",array("id"=>$tid));
							$dep=$teacher_det['dep'];
                            if($chk==0)
							{
								if($dep=="HSS SECTION")
								{
									$data_temp['period_id'][]=$pid;
									$data_temp['period_name'][]=$p;
									$data_temp['classno'][]=$classdet['classno'];
									$data_temp['classid'][]=$classdet['classid'];								
									$data_temp['subject'][]=$row['subject'];
									$data_temp['subject_type'][]=$row['subject_type'];
									$data_temp['day'][]=$row['day'];
									$data_temp['subject_teacher']=$tid;
									$data_temp['dep']=$teacher_det['dep']; 
									$data_temp['count']++;									
								}
								else
								{
									if($pid!=9)
									{
										$data_temp['period_id'][]=$pid;
										$data_temp['period_name'][]=$p;
										$data_temp['classno'][]=$classdet['classno'];
										$data_temp['classid'][]=$classdet['classid'];								
										$data_temp['subject'][]=$row['subject'];
										$data_temp['subject_type'][]=$row['subject_type'];
										$data_temp['day'][]=$row['day'];
										$data_temp['subject_teacher']=$tid;
										$data_temp['dep']=$teacher_det['dep']; 
										$data_temp['count']++;										
									}
								}

							}
                            elseif($chk>=1)
							{
								$data_temp2['period_id'][]=$pid;
								$data_temp2['period_name'][]=$p;
								$data_temp2['classno'][]=$classdet['classno'];
								$data_temp2['classid'][]=$classdet['classid'];								
								$data_temp2['subject'][]=$row['subject'];
								$data_temp2['subject_type'][]=$row['subject_type'];
                                $data_temp2['day'][]=$row['day'];
                                $data_temp2['subject_teacher']=$tid;   								
                                $data_temp2['dep']=$teacher_det['dep'];   								
								$data_temp2['count']++;
							}
					}
				}
		$data['free_period']=$data_temp;
		$data['alloted_period']=$data_temp2;
		return $data;
    }

	function substitue_list_by_periods_for_teacher($day,$acyear,$tid,$date)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$month_start_end_days=$dbobject->month_start_end_days($date);
		$first_day=$month_start_end_days['first_day'];
		$last_day=$month_start_end_days['last_day'];		
		$Teacher=new Teacher();		
		$Teacher_list=$Teacher->Teacher_list_BY_acyear($acyear);
		if(!empty($Teacher_list))
		{
			foreach($Teacher_list as $list)
			{
				$qry=$dbobject->select("select * from  `teacher_substitution_restriction` where `userid`='".$list['userid']."'");
				$chk=mysql_num_rows($qry);
				if($chk==0)
				{
					if($tid!=$list['id'])
					{
					
					    $attendence_Substitution=$this->attendence_Substitution($list['id'],$date);
						if($attendence_Substitution!="Absent" && $attendence_Substitution!="onduty")
						{
						$teacher_periods_daywise_details=$this->teacher_periods_daywise_details($day,$acyear,$list['id']);
						$monthly_substitute_count=$this->teacher_substitue_monthly_count($first_day,$last_day,$list['id']);
						$data_det[$list['id']]['free_period']=$teacher_periods_daywise_details['free_period'];
						$data_det[$list['id']]['alloted_period']=$teacher_periods_daywise_details['alloted_period'];			
						$sort_array[$list['id']]['count_index']=$teacher_periods_daywise_details['free_period']['count'] - $monthly_substitute_count/30;
						}					
					}
				}				
			}
		}
		$array_msort=$dbobject->array_msort($sort_array, array('count_index'=>SORT_DESC));		
		$data['data']=$data_det;
		$data['sorted_data']=$array_msort;
		///print_r($array_msort);
		return $data;
	}
	function substitue_list_of_restricted_teacher($day,$acyear,$tid,$date)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$qry=$dbobject->select("select * from  `teacher_substitution_restriction` where `priority`='2'");
		while($row=$dbobject->fetch_array($qry))
			{
               $list=$dbobject->selectall("teacher",array("userid"=>$row['userid']));
				if($tid!=$list['id']){
				$attendence_Substitution=$this->attendence_Substitution($list['id'],$date);
				if($attendence_Substitution!="Absent" && $attendence_Substitution!="onduty")
				{	
				$teacher_periods_daywise_details=$this->teacher_periods_daywise_details($day,$acyear,$list['id']);
				$data_det[$list['id']]['free_period']=$teacher_periods_daywise_details['free_period'];
				$data_det[$list['id']]['alloted_period']=$teacher_periods_daywise_details['alloted_period'];			
				$sort_array[$list['id']]['count_index']=$teacher_periods_daywise_details['free_period']['count'];	
				}
			    }
								
			}
		
		$array_msort=$dbobject->array_msort($sort_array, array('count_index'=>SORT_DESC));		
		$data['data']=$data_det;
		$data['sorted_data']=$array_msort;
		return $data;
	}
function timetable_substitution_daywise($day,$date)
{
		$dbobject = new DB();
		$dbobject->getCon();	
	//$sql="SELECT distinct  `subject_teacher` FROM  `timetable_substitution` where `day`='".$day."' and `date`='".$date."'";
	$sql="SELECT distinct  `subject_teacher` FROM  `timetable_substitution` where `date`='".$date."'";
	$qry=$dbobject->select($sql);
	$i=0;
	while($row=$dbobject->fetch_array($qry))
	{

	 
	 $teacher_det=$dbobject->selectall("teacher",array("id"=>$row['subject_teacher']));
	 $data[$i]['userid']=$teacher_det['userid'];
	 $data[$i]['tid']=$teacher_det['id'];
	 $data[$i]['name']=$teacher_det['name'];
     $i++;	
	}
	return $data;
}	
function teacher_period_class_substitution($userid,$day,$pid,$acyear,$date)
{
	$dbobject = new DB();
	$dbobject->getCon(); 
	$teacher_det=$dbobject->selectall("teacher",array("userid"=>$userid));	
	$tid=$teacher_det['id'];
	$teacher_period_class_data=$this->teacher_period_class_data_substitution($tid,$day,$pid,$acyear,$date);
    $data=$teacher_period_class_data[0];	
	if(!empty($data))
				{
					$i=0;
					foreach($data as $classname=>$sub)
					{
						if(!empty($sub))
							{
								foreach($sub as $s)
								{
								$det[$i]['class']=$classname;
								$det[$i]['subject']=$s;
								$i++;											
								}
							}
						
					}
				}
				return $det;
}
function teacher_period_class_data_substitution($tid,$day,$pid,$acyear,$date)
{
	$dbobject = new DB();
	$dbobject->getCon();
    //"select * from `timetable_substitution` where `day`='".$day."' and `period`='".$pid."' and `acyear`='".$acyear."' and `subject_teacher`='".$tid."' and `date`='".$date."'"	
	$sel_period=$dbobject->select("select * from `timetable_substitution` where  `period`='".$pid."' and `acyear`='".$acyear."' and `subject_teacher`='".$tid."' and `date`='".$date."'");
	$num_rows=mysql_num_rows($sel_period);
	$i=0;
	while($row=$dbobject->fetch_array($sel_period))
	{
		$subid=$row['subject'];
		$type=$row['subject_type'];
		$subject_data=$type."-".$subid;
		$classid=$row['classid'];
		$classdet=$dbobject->selectall("sclass",array("classid"=>$row['classid']));
		include_once('admin_class.php');
		$admin = new Admin($classdet['classno']);
		$tablename=$admin->subject_table();						 
		$subject=$dbobject->sel_subject($subject_data,$tablename);
		include_once('sub_name.php');
		$subname=new Subject($subject);
		$sub=$subname->sub_table();
	    $data_temp[$classdet['classname']." ".$classdet['division']][$sub]=$sub;
	    $data_temp2[$row['id']]['class']=$classdet['classname']." ".$classdet['division'];
	    $data_temp2[$row['id']]['subject']=$sub;
	}	
	$data[0]=$data_temp;
	$data[1]=$data_temp2;
	return $data;
}
function periods_teacher_daywise_data($day,$period,$acyear,$tid,$classid,$section)
{
		$dbobject = new DB();
		$dbobject->getCon(); 		
		$sql="select * from  `timetable` where `day`='".$day."' and `acyear`='".$acyear."'";
		if($period!="" && $period!="-1")
		{
		$sql.=" and `period`='".$period."'";	
		}		
		if($tid!="" && $tid!="-1")
		{
		$sql.=" and `subject_teacher`='".$tid."'";	
		}
		if($classid!="" && $classid!="-1")
		{
		   if($section=="")
		   {
		   $sql.=" and `classid`='".$classid."'";
		   }		   
		}
		if($section!="" && $section!="-1")
		{
			//
		}		
        $sql.=" order by `subject_teacher`";		
		$qry=$dbobject->select($sql);
		$i=0;
		while($row=$dbobject->fetch_array($qry))
		{
			$teacher[$i]['id']=$row['id'];
			$teacher[$i]['subject_teacher']=$row['subject_teacher'];
			$teacher[$i]['period']=$row['period'];
			$teacher[$i]['subject']=$row['subject'];
			$teacher[$i]['subject_type']=$row['subject_type'];
			$teacher[$i]['classid']=$row['classid'];
			$teacher[$i]['day']=$row['day'];
			$i++;
		}
return $teacher;		
}
    function date_in_week()
	{
		$today = time();
		$wday = date('w', $today);   
		$datemon = date('Y-m-d', $today - ($wday - 1)*86400);
		$datetue = date('Y-m-d', $today - ($wday - 2)*86400);
		$datewed = date('Y-m-d', $today - ($wday - 3)*86400);
		$datethu = date('Y-m-d', $today - ($wday - 4)*86400);
		$datefri = date('Y-m-d', $today - ($wday - 5)*86400);
		$data[0]=$datemon;
		$data[1]=$datetue;
		$data[2]=$datewed;
		$data[3]=$datethu;
		$data[4]=$datefri;
        return $data;		
	}
    function teacher_substitue_monthly_count($first_day,$last_day,$tid)
    {
		$dbobject = new DB();
		$dbobject->getCon(); 
		$sql="SELECT * FROM `timetable_substitution` WHERE `date`>='".$first_day."' and `date`<='".$last_day."' and `subject_teacher`='".$tid."'";
        $qry=$dbobject->select($sql);
		$chk=mysql_num_rows($qry);
		return $chk;
	}	
    function check_classno_array_for_substitution($teacherid,$classno_array,$acyear,$dep)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		if(!empty($classno_array))
		{
			$query_format=$dbobject->query_format($classno_array);
			//echo $query_format."<br>";
			$sql="SELECT * FROM `teacher_subject` WHERE `classno` IN (".$query_format.") and `teacherid`='".$teacherid."' ";
			$sql.="and `acyear`='".$acyear."'";
			$qry=$dbobject->select($sql);
			$chk=mysql_num_rows($qry);			
		}
		else
		{
		$chk=0;	
		}

		return $chk;
	}	
    function day_array_data()
	{
		$day_array["Mon"]="Monday";
		$day_array["Tue"]="Tuesday";
		$day_array["Wed"]="Wednesday";
		$day_array["Thu"]="Thursday";
		$day_array["Fri"]="Friday";
		//$day_array["Sat"]="Saturday";
		//$day_array["Sun"]="Sunday";
		return  $day_array;
	}
function check_day_timetable($day,$acyear)
{
	   $dbobject = new DB();
	   $dbobject->getCon();
       $sql="SELECT * FROM `timetable` WHERE `day`='".$day."' and `acyear`='".$acyear."'";		
	   $qry= $dbobject->select($sql);
	   $check=mysql_num_rows($qry);
	   return  $check;
}
function timetable_imported($date)
{
	   $dbobject = new DB();
	   $dbobject->getCon();
       $sql="SELECT * FROM `timetable_imported` WHERE `date`='".$date."'";		
	   $qry= $dbobject->select($sql);
       $row=$dbobject->fetch_array($qry);
	   $data['day']=$row['day'];
	   $data['imported_day']=$row['imported_day'];
	   $data['date']=$row['date'];
	   $data['userid']=$row['userid'];
	   return $data;
}
function teacher_list_susbstitiution($day,$date)
{
		$dbobject = new DB();
		$dbobject->getCon();	
	//$sql="SELECT distinct  `subject_teacher` FROM  `timetable_substitution` where `day`='".$day."' and `date`='".$date."'";
	$sql="SELECT distinct  `subject_teacher_substituted` FROM  `timetable_substitution` where `date`='".$date."'";
	$qry=$dbobject->select($sql);
	$i=0;
	while($row=$dbobject->fetch_array($qry))
	{

	 
	 $teacher_det=$dbobject->selectall("teacher",array("id"=>$row['subject_teacher_substituted']));
	 $data[$i]['userid']=$teacher_det['userid'];
	 $data[$i]['tid']=$teacher_det['id'];
	 $data[$i]['name']=$teacher_det['name'];
     $i++;	
	}
	return $data;
}
function teacher_list_susbstitiution_period($userid,$day,$pid,$acyear,$date)
{
	$dbobject = new DB();
	$dbobject->getCon(); 
	$teacher_det=$dbobject->selectall("teacher",array("userid"=>$userid));	
	$tid=$teacher_det['id'];
	$teacher_period_class_data=$this->teacher_list_susbstitiution_period_data($tid,$day,$pid,$acyear,$date);
    $data=$teacher_period_class_data[0];	
	if(!empty($data))
				{
					$i=0;
					foreach($data as $classname=>$sub)
					{
						if(!empty($sub))
							{
								foreach($sub as $s)
								{
								$det[$i]['class']=$classname;
								$det[$i]['subject']=$s;
								$i++;											
								}
							}
						
					}
				}
				return $det;
}
function teacher_list_susbstitiution_period_data($tid,$day,$pid,$acyear,$date)
{
	$dbobject = new DB();
	$dbobject->getCon();
    //"select * from `timetable_substitution` where `day`='".$day."' and `period`='".$pid."' and `acyear`='".$acyear."' and `subject_teacher`='".$tid."' and `date`='".$date."'"	
	$sel_period=$dbobject->select("select * from `timetable_substitution` where  `period`='".$pid."' and `acyear`='".$acyear."' and `subject_teacher_substituted`='".$tid."' and `date`='".$date."'");
	$num_rows=mysql_num_rows($sel_period);
	$i=0;
	while($row=$dbobject->fetch_array($sel_period))
	{
		$subid=$row['subject'];
		$type=$row['subject_type'];
		$subject_data=$type."-".$subid;
		$classid=$row['classid'];
		$classdet=$dbobject->selectall("sclass",array("classid"=>$row['classid']));
		include_once('admin_class.php');
		$admin = new Admin($classdet['classno']);
		$tablename=$admin->subject_table();						 
		$subject=$dbobject->sel_subject($subject_data,$tablename);
		include_once('sub_name.php');
		$subname=new Subject($subject);
		$sub=$subname->sub_table();
	    $data_temp[$classdet['classname']." ".$classdet['division']][$sub]=$sub;
	    $data_temp2[$row['id']]['class']=$classdet['classname']." ".$classdet['division'];
	    $data_temp2[$row['id']]['subject']=$sub;
	}	
	$data[0]=$data_temp;
	$data[1]=$data_temp2;
	return $data;
}
function substituted_teacherid($tid,$day,$pid,$acyear,$date)
{
    $substituted_teacherid_data=$this->substituted_teacherid_data($tid,$day,$pid,$acyear,$date); 
	$subject_teacher=$substituted_teacherid_data['subject_teacher'];
	return $subject_teacher;
}
function substituted_teacherid_data($tid,$day,$pid,$acyear,$date)
{
	$dbobject = new DB();
	$dbobject->getCon();	
	$sel_period=$dbobject->select("select * from `timetable_substitution` where  `period`='".$pid."' and `acyear`='".$acyear."' and `subject_teacher_substituted`='".$tid."' and `date`='".$date."'");	
	$row=$dbobject->fetch_array($sel_period);
	return $row;
}
function check_day_substitution($tid,$day,$pid,$acyear,$date)
{
	$dbobject = new DB();
	$dbobject->getCon();	
	//echo "select * from `timetable_substitution` where  `period`='".$pid."' and `acyear`='".$acyear."' and `subject_teacher`='".$tid."' and `date`='".$date."'";
	$sel_period=$dbobject->select("select * from `timetable_substitution` where  `period`='".$pid."' and `acyear`='".$acyear."' and `subject_teacher`='".$tid."' and `date`='".$date."'");	
	$chk=mysql_num_rows($sel_period);	
	return $chk;
}
function current_day_substitution($day,$pid,$acyear,$date,$subject_teacher_substituted)
{
	$dbobject = new DB();
	$dbobject->getCon();	
	//echo "select * from `timetable_substitution` where  `period`='".$pid."' and `acyear`='".$acyear."' and `subject_teacher`='".$tid."' and `date`='".$date."'";
	//echo "select * from `timetable_substitution` where  `period`='".$pid."' and `date`='".$date."'";
	$sel_period=$dbobject->select("select * from `timetable_substitution` where  `period`='".$pid."' and `date`='".$date."' and `subject_teacher_substituted`='".$subject_teacher_substituted."'");	
	$row=$dbobject->fetch_array($sel_period);
	$subject_teacher=$row['subject_teacher'];
	return $subject_teacher;
}
	function insert_substitution_remark($tid,$remark,$date,$day,$acyear)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$qry=$dbobject->select("DELETE FROM `teacher_attendence` WHERE  `tid`='".$tid."' and `date`='".$date."'");
		if($remark!="-1" && $remark!="")
		{
		$insert=$dbobject->exe_qry("INSERT INTO `teacher_attendence`(`tid`, `date`,  `acyear`,`remark`) VALUES ('".$tid."','".$date."','".$acyear."','".$remark."')");			
		}		
		return $insert;
	}
	function substitution_remark($tid,$date)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$qry=$dbobject->select("SELECT * FROM `teacher_attendence` WHERE `tid`='".$tid."' and `date`='".$date."'");
	    $row=$dbobject->fetch_array($qry);
	   $remark=$row['remark'];

		return $remark;
	}	
	function insert_substitution_datewise_restriction($classid,$day,$period,$subject,$subject_teacher,$subject_type,$acyear,$userid,$date)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$chk=$this->check_substitution_datewise_restriction($classid,$day,$period,$subject,$subject_teacher,$subject_type,$acyear,$date);
		$sql="INSERT INTO `teacher_substitution_datewise_restriction`(`classid`, `day`, `period`, `subject`, `subject_teacher`, `subject_type`, `acyear`, `userid`, `date`) ";
		$sql.="VALUES ('".$classid."','".$day."','".$period."','".$subject."','".$subject_teacher."','".$subject_type."','".$acyear."','".$userid."','".$date."')";
		if($chk==0)
		{
		$qry=$dbobject->exe_qry($sql);	
		}

		//echo "ins";

	}
	function delete_substitution_datewise_restriction($classid,$day,$period,$subject,$subject_teacher,$subject_type,$acyear,$userid,$date)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$dbobject = new DB();
		$dbobject->getCon();
		$chk_sql="DELETE FROM `teacher_substitution_datewise_restriction` WHERE `classid`='".$classid."' and `day`='".$day."' and `period`='".$period."' ";
		$chk_sql.="and `subject`='".$subject."' and `subject_teacher`='".$subject_teacher."' and `subject_type`='".$subject_type."' ";
		$chk_sql.="and `acyear`='".$acyear."' and `date`='".$date."'";
		$delete=$dbobject->exe_qry($chk_sql);
        //echo "del";
	}	
	function check_substitution_datewise_restriction($classid,$day,$period,$subject,$subject_teacher,$subject_type,$acyear,$date)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$chk_sql="SELECT * FROM `teacher_substitution_datewise_restriction` WHERE `classid`='".$classid."' and `day`='".$day."' and `period`='".$period."' ";
		$chk_sql.="and `subject`='".$subject."' and `subject_teacher`='".$subject_teacher."' and `subject_type`='".$subject_type."' ";
		$chk_sql.="and `acyear`='".$acyear."' and `date`='".$date."'";
		//echo $chk_sql;
		$chk_qry=$dbobject->exe_qry($chk_sql);
		return $chk=mysql_num_rows($chk_qry);

		

	}	
}
?>