<?php
include_once('admin_class.php');
$class = $_GET['sele_class'];
$sid = $_GET['sel_student'];
$exam = $_GET['sel_exam'];
$acyear=$_GET['acyear'];

//#############retrieving student,s and school's information for report header details###########
$selschool = "select name,mob,email,affiliation_no,academic_year from schoolinfo;";
$selrs = $dbobj->select($selschool);
$selrow = $dbobj->fetch_array($selrs);
$school = $selrow['name'];
$contact = $selrow['mob'];
$academic_year=$selrow['academic_year'];
$aff_no=$selrow['affiliation_no'];
$email = $selrow['email'];

$selname = "select * from student where sid = ".$sid.";";
$result = $dbobj->select($selname);
$row = $dbobj->fetch_array($result);
$stname = $row['sname'];
$initial = $row['initial'];
$studentid = $row['studentid'];
$stud_photoid=$row['simgid'];
$parent_id=$row['parent_id'];

$parent=$dbobj->selectall("parent",array("id"=>$parent_id));
$school_info=$dbobj->selectall("schoolinfo",array("id"=>1));

$query = "select classname,division,classno from sclass where classid = ".$class.";";
$rs =$dbobj->select($query);
$row = $dbobj->fetch_array($rs);
$classname = $row['classname'];
$division = $row['division'];
$classno = $row['classno'];

$admin = new Admin($classno);
$tablename=$admin->subject_table();

$form = "<table style='width:100%;border-collapse:collapse' align='center' border='1'><tr><th>Subject</th>";
$selsub=$dbobj->select("select * from ".$tablename." order by `sub_id` ");
$s=1;
while($sub_row=$dbobj->fetch_array($selsub))
{
	$sub_id[$s]=$sub_row['sub_id'];
	$sub_name[$s]=$sub_row['subject'];
	$s++;
}

$sel_exm=$dbobj->select("select * from terms where classno='".$classno."' and `exam_type`!='attendance_only'");
$t=1;
while($exm_row=$dbobj->fetch_array($sel_exm))
{
	$term_id[$t]=$exm_row['id'];
	$sub_type=$exm_row['sub_type'];
	if($sub_type=='subject_only')
	{
		$colspan="2";
	}
	elseif($sub_type=='subject_or_subhead')
	{	
	$colspan="4";
	}
$form.= "<th colspan='".$colspan."' style='text-align:center'>".$exm_row['term_name']."</th>";		
$t++;
}
$form.= "<th>Final Result</th></tr><tr><th>MAX.MARKS</th>";
		foreach($term_id as $tid)
		{
			$max_mark=0;
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			$examtype=$term_row['exam_type'];
			if($examtype=="term")
			{
				$form.="<th style='text-align:center'>80</th><th style='text-align:center'>20</th><th style='text-align:center'>100</th>";
			}
			elseif($examtype=="mid_term")
			{
				$form.="<th style='text-align:center'>50</th>";
			}
			
			$form.="<th style='text-align:center' rowspan='2'>Grade</th>";
		}

$form.= "<th style='text-align=centre' rowspan='2'>%</th></tr>";
$form.= "<tr><th>PASS MARK</th>";
		foreach($term_id as $tid)
		{
			$min_mark=0;
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			$examtype=$term_row['exam_type'];
			if($examtype=="term")
			{
				$form.="<th style='text-align:center'>32</th><th style='text-align:center'>8</th><th style='text-align:center'>40</th>";
			}
			elseif($examtype=="mid_term")
			{
				$form.="<th style='text-align:center'>20</th>";
			}
		}
$form.= "</tr>";

	$form.= "";
	$i=1;
	$grant_mark=0;
	$grant_max=0;
	$grant_per=0;
	foreach($sub_id as $sub)
	{
		$g_tot=0;
		$g_max=0;
		$form.= "<tr><th>".$sub_name[$i]."</th>";	
		foreach($term_id as $tid)
		{
		$tot=0;
		$max_tot=0;
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			$exam_type=$term_row['exam_type'];
			if($sub_type=="subject_only")
			{	
				$sel_examdet=$dbobj->select("select * from `examschedule` where `examid`='".$tid."' and `classno`='".$classno."' and `sub_id`='".$sub."' and `acc_year`='".$acyear."'");
				$examdet_row=$dbobj->fetch_array($sel_examdet);
				$max_mark=$examdet_row['maxmark'];
				$min_mark=$examdet_row['minmark'];
				$sel_mark=$dbobj->select("select * from `report_card` where `sid`='".$sid."' and `classid`='".$class."' and `examid`='".$tid."' and `sub_id`='".$sub."' and `assessment`='".$exam_type."' and `ac_year`='".$acyear."'");
				$mark_row=$dbobj->fetch_array($sel_mark);
				if($mark_row['mark']<$min_mark)
				{
					$form.= "<td style='color:red;text-align:center'>".$mark_row['mark']."</td>";
				}
				else
				{
					$form.= "<td style='text-align:center'>".$mark_row['mark']."</td>";
				}
				$m_tot[$tid]=$m_tot[$tid]+$mark_row['mark'];
				$maximum_tot[$tid]=$maximum_tot[$tid]+$max_mark;
				$g_tot=$g_tot+$mark_row['mark'];
				$g_max=$g_max+$max_mark;
				$form.= "<td style='text-align:center'>".grade_calculate($mark_row['mark'],$max_mark,$classno)."</td>";
			}
			elseif($sub_type=="subject_or_subhead")
			{
				$subject=$dbobj->selectall($tablename,array("sub_id"=>$sub));
				if($subject['sub_head']=="1")
				{
					$sel_subhd=$dbobj->select("select * from `sub_heads` where `subj_id`='".$sub."' and `classno`='".$classno."'");
					$total=0;
					$max_total=0;
					while($subhd_row=$dbobj->fetch_array($sel_subhd))
					{
					
						$subhd_id=$subhd_row['id'];
						$sel_examdet=$dbobj->select("select * from `examschedule` where `examid`='".$tid."' and `classno`='".$classno."' and `sub_id`='".$sub."' and `subhead_id`='".$subhd_id."' and `acc_year`='".$acyear."'");
						$examdet_row=$dbobj->fetch_array($sel_examdet);
						$max_mark=$examdet_row['maxmark'];
						$min_mark=$examdet_row['minmark'];
						$sel_mark=$dbobj->select("select * from `report_card` where `sid`='".$sid."' and `classid`='".$class."' and `examid`='".$tid."' and `sub_id`='".$sub."' and `subhead_id`='".$subhd_id."' and `assessment`='".$exam_type."' and `ac_year`='".$acyear."'");
						$mark_row=$dbobj->fetch_array($sel_mark);
						if($mark_row['mark']<$min_mark)
						{
							$form.= "<td style='color:red;text-align:center'>".$mark_row['mark']."</td>";
						}
						else
						{
							$form.= "<td style='text-align:center'>".$mark_row['mark']."</td>";
						}
						$total=$total+$mark_row['mark'];
						$max_total=$max_total+$max_mark;
						$m_tot[$tid]=$m_tot[$tid]+$mark_row['mark'];
						$g_tot=$g_tot+$mark_row['mark'];
					}
					$g_max=$g_max+$max_total;
					$maximum_tot[$tid]=$maximum_tot[$tid]+$max_total;
				}
				else
				{
					$sel_examdet=$dbobj->select("select * from `examschedule` where `examid`='".$tid."' and `classno`='".$classno."' and `sub_id`='".$sub."' and `acc_year`='".$acyear."'");
					$examdet_row=$dbobj->fetch_array($sel_examdet);
					$max_mark=$examdet_row['maxmark'];
					$min_mark=$examdet_row['minmark'];
					$sel_mark=$dbobj->select("select * from `report_card` where `sid`='".$sid."' and `classid`='".$class."' and `examid`='".$tid."' and `sub_id`='".$sub."' and `assessment`='".$exam_type."' and `ac_year`='".$acyear."'");
					$mark_row=$dbobj->fetch_array($sel_mark);
					if($mark_row['mark']<$min_mark)
					{
						$form.= "<td style='color:red;text-align:center'>".$mark_row['mark']."</td>";
					}
					else
					{
						$form.= "<td style='text-align:center'>".$mark_row['mark']."</td>";
					}
					$form.= "<td style='text-align:center'>--</td>";
					$total=$mark_row['mark'];
					$max_total=$max_mark;
					$m_tot[$tid]=$m_tot[$tid]+$mark_row['mark'];
					$maximum_tot[$tid]=$maximum_tot[$tid]+$max_mark;
					$g_tot=$g_tot+$mark_row['mark'];
					$g_max=$g_max+$max_mark;
				}
				$form.= "<td style='text-align:center'>".$total."</td><td style='text-align:center'>".grade_calculate($total,$max_total,$classno)."</td>";
			}
					
						
		}
		
	//$form.= "<th></th></tr>";
	if($g_max!=0){
	$round=round(($g_tot/$g_max)*100,2);
	$form.= "<th>".$round."</th></tr>";
	$grant_mark=$grant_mark+$g_tot;
	$grant_max=$grant_max+$g_max;
	$grant_per=$grant_per+$round;
	$i++;
	}
	}

				
$form.= "<tr><th>Total</th>";
		foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_only")
			{
				$form.="<td colspan='2' align='center'><b>".$m_tot[$tid]."</b></td>";
			}
			elseif($sub_type=="subject_or_subhead")
			{
				
				$sel_subhd=$dbobj->select("select distinct `head_name` from `sub_heads` where  `classno`='".$classno."'");
				$num_rows=mysql_num_rows($sel_subhd);
				$form.="<td colspan='4' align='center'><b>".$m_tot[$tid]."</b></td>";
			}
		}
$form.= "<th colspan='2' align='center'><b>".$grant_mark."</b></th></tr>";
$form.= "<tr><th>Percentage</th>";
		foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_only")
			{
				$form.="<td colspan='2' align='center'><b>";
				if($maximum_tot[$tid]!=0)
				{
				$form.=round(($m_tot[$tid]/$maximum_tot[$tid])*100,2);
				}
				$form.="</b></td>";
			}
			elseif($sub_type=="subject_or_subhead")
			{
				
				$sel_subhd=$dbobj->select("select distinct `head_name` from `sub_heads` where  `classno`='".$classno."'");
				$num_rows=mysql_num_rows($sel_subhd);
				$form.="<td colspan='4' align='center'><b>";
				if($maximum_tot[$tid]!=0)
				{
					//$form.=$maximum_tot[$tid];
					$form.=round(($m_tot[$tid]/$maximum_tot[$tid])*100,2);
				}
				$form.="</b></td>";
			}
		}
		if($grant_max!=0){
			$per=round(($grant_mark/$grant_max)*100,2);
		}
$form.= "<th colspan='2' align='center'><b>".$per."</b></th></tr>";
$form.= "<tr><th>Grade</th>";
foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_only")
			{
				$form.="<td colspan='2' align='center'><b>".grade_calculate($m_tot[$tid],$maximum_tot[$tid],$classno)."</b></td>";
			}
			elseif($sub_type=="subject_or_subhead")
			{
				
				$sel_subhd=$dbobj->select("select distinct `head_name` from `sub_heads` where  `classno`='".$classno."'");
				$num_rows=mysql_num_rows($sel_subhd);
				$form.="<td colspan='4' align='center'><b>".grade_calculate($m_tot[$tid],$maximum_tot[$tid],$classno)."</b></td>";
			}
		}
$form.= "<th></th></tr>";
$form.= "<tr><th>Date Of Report</th>";
foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_only")
			{
				$form.="<td colspan='2'></td>";
			}
			elseif($sub_type=="subject_or_subhead")
			{
				
				$sel_subhd=$dbobj->select("select distinct `head_name` from `sub_heads` where  `classno`='".$classno."'");
				$num_rows=mysql_num_rows($sel_subhd);
				$form.="<td colspan='4'></td>";
			}
		}
$form.= "<th></th></tr>";
$form.= "<tr><th>Class Teacher's Signature</th>";
foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_only")
			{
				$form.="<td colspan='2'></td>";
			}
			elseif($sub_type=="subject_or_subhead")
			{
				
				$sel_subhd=$dbobj->select("select distinct `head_name` from `sub_heads` where  `classno`='".$classno."'");
				$num_rows=mysql_num_rows($sel_subhd);
				$form.="<td colspan='4'></td>";
			}
		}
$form.= "<th></th></tr>";
$form.= "<tr><th>Principal's Signature</th>";
foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_only")
			{
				$form.="<td colspan='2'></td>";
			}
			elseif($sub_type=="subject_or_subhead")
			{
				
				$sel_subhd=$dbobj->select("select distinct `head_name` from `sub_heads` where  `classno`='".$classno."'");
				$num_rows=mysql_num_rows($sel_subhd);
				$form.="<td colspan='4'></td>";
			}
		}
$form.= "<th></th></tr>";
$form.= "</table>";
//-----------------------------------------------------------------------------------------------------------------------------

$sel_term_only=$dbobj->select("select *  from `terms` where `classno`='".$classno."' and `exam_type`='term' "); 
$i=1;
while($term_only_row=$dbobj->fetch_array($sel_term_only))
{
	$term_only_id[$i]=$term_only_row['id'];
	$term_only_name[$i]=$term_only_row['term_name'];
	$i++;
}
//--------------------------------Remark-----------------------------------------------------------------------------------
$j=1;
$rem.="<table align='center' border='1' style='width:1200px;font-size:13px;border-collapse:collapse'><tr><th colspan='2' style='color:#B71D6F;'><b>REMARKS</b></th></tr>";
foreach ($term_only_id as $tid)
   {
	$rem.="<tr><td style='width:110px'><b>".strtoupper($term_only_name[$j])."</b></td>";
	$sel_remark=$dbobj->select("select * from remark where `sid`='".$sid."' and `term`='".$tid."' and `cls`='".$class."'");
	$rem_arry=$dbobj->fetch_array($sel_remark);
	$rem.="<td>".$rem_arry['remrk']."</td></tr>";
	$j++;
   }
   $rem.="</table>";
//-------------------------NON-ACADEMIC------------------------------------------------------
//-------------------------NON-ACADEMIC------------------------------------------------------
					if($classno<=6)
					{
						$section="LP";
					}
					else
					{
						$section="HS";
					}
$sel_non_acc=$dbobj->select("select * from `performance_area` where `section`='".$section."' order by `id` ");
		while($acc_row=$dbobj->fetch_array($sel_non_acc))
		{
			$head=$acc_row['areaname'];
			foreach($term_only_id as $tid)
			{
				$term_det=$dbobj->selectall("terms",array("id"=>$tid));
				$sel_non_grade=$dbobj->select("select `grade` from `performance_evaluation` where sid='".$sid."' and  `headid`='".$acc_row['id']."' and`term`='".$tid."' and `classid`='".$class."' and `acc_year`='".$acyear."'");
				$grade_row=$dbobj->fetch_array($sel_non_grade);
				$grade=$grade_row['grade'];
				$nonacc[$head][$term_det['term_name']]=$grade;
			}
			
		}
	  /*--------------------------------Last Page---------------------*/
//------------------------------EXTRA-NON-ACADEMIC------------------------------------------------

$extra_non_acc=$dbobj->select("select * from `extra_non` where `classno`='".$classno."' order by `id` ");
		while($non_row=$dbobj->fetch_array($extra_non_acc))
		{
			$head=$non_row['head'];
			foreach($term_only_id as $tid)
			{
				$term_det=$dbobj->selectall("terms",array("id"=>$tid));
				$sel_extra_grade=$dbobj->select("select `grade` from `extra_non_grades` where sid='".$sid."' and  `headid`='".$non_row['id']."' and`term`='".$tid."' and `classid`='".$class."' and `acc_year`='".$acyear."'");
				$grade_row=$dbobj->fetch_array($sel_extra_grade);
				$grade=$grade_row['grade'];
				$enonacc[$head][$term_det['term_name']]=$grade;
			}
		}

	  /*--------------------------------Last Page---------------------*/

				  
				  
				 
		 
//---------------------------------------------Attendance-----------------------------------
  if($classno=="12" || $classno=="14")
{

	$sel_term_for_attendance=$dbobj->select("select *  from `terms` where `classno`='".$classno."' and `term_name` IN ('I Term','II Term','III Term') "); 
}
else
{
	$sel_term_for_attendance=$dbobj->select("select *  from `terms` where `classno`='".$classno."' and `exam_type`='term' "); 
}
while($att_term_row=$dbobj->fetch_array($sel_term_for_attendance))
{
	 if($att_term_row['term_name']=="I Term" || $att_term_row['term_name']=="II Term")
	 {
		$sdate=date('2015-m-d',strtotime($att_term_row['smonth']));
	    $edate=date('2015-m-d',strtotime($att_term_row['emonth'])); 
	 }
	 else
	 {
		$sdate=date('2016-m-d',strtotime($att_term_row['smonth']));
	    $edate=date('2016-m-d',strtotime($att_term_row['emonth']));  
	 }
	   $t=$att_term_row['id'];
	  $sel_attandance=$dbobj->select("select distinct `date`  from `attendence` where `studentid`='".$studentid."' and `classid`='".$class."' and `status`!='Absent' and `acyear`='".$acyear."' and `date` >= '$sdate' and `date` <= '$edate'");
	  $attn[$t]=mysql_num_rows($sel_attandance);
	  $sel_attandance=$dbobj->select("select distinct `date`  from `attendence` where  `classid`='".$class."' and `status`!='Absent' and `acyear`='".$acyear."' and `date` >= '$sdate' and `date` <= '$edate'");
	  $att_tot[$t]=mysql_num_rows($sel_attandance);
	  $att_per[$t]=round(($attn[$t]/$att_tot[$t])*100,2);
	  
}
   //$att.="<table align='center' style='border-collapse:collapse' border='1'><tr><th colspan='4'>Attendance</th></tr><tr><td></td>".$t_name."</tr>";
$att="<table  style='border-collapse:collapse;' width='650px' border='1'><tr><th colspan='5' style='font-size:14px;height:33px'>ATTENDANCE</th></tr><tr><th style='text-align:left;'>COMPONENTS</th></td>".$t_name."<td><b>TOTAL</b></td></tr>";
$att.="<tr><td><b>WORKING DAYS</b></td>";
	   foreach($att_tot as $t)
	   {
		 $att.="<td>".$t."</td>";  
		 $work_days= $work_days+$t;
	   }
$att.="<td>".$work_days."</td></tr>";
$att.="<tr><td><b>ATTENDED DAYS</b></td>";
	   foreach($attn as $t)
	   {
		 $att.="<td>".$t."</td>";  
		 $attended_days= $attended_days+$t;
	   }

$att.="<td>".$attended_days."</td></tr>";
$att.="<tr><td><b>PERCENTAGE</b></td>";
	   foreach($att_per as $t)
	   {
		 $att.="<td>".$t."</td>";  
		 $tot_per= $tot_per+$t;
	   }
	$att.="<td>".round(($tot_per)/3,2)."</td></tr></table>";
//----------------------------------Grade Calculate------------------------------------------
function grade_calculate($val,$val2,$classno)
{
	if($val!="" && $val2!="" && $val!="0" && $val2!="0")
	{
	$ma=($val/$val2)*100;
	$sel = mysql_query("select * from grades where classno = '$classno'");
	while($rowsel = mysql_fetch_array($sel))
		{
			$min_mark = $rowsel['min_mark'];
			$min_mark = $min_mark.".00";
			$max_mark = $rowsel['max_mark'];
			$max_mark = $max_mark."00";
			$gradename = $rowsel['gradename'];
			if($ma>=$min_mark && $ma<=$max_mark )
			{
				return $gradename;
			}
		}
	}
	
}
?>
<?php
$main="<table style='border:3px solid #B71D6F'  width='100%'>
<tr><td class='ftd' width='50%' style='border:1px solid #B71D6F'>
			<table style='font-size:12px'><tr><td>
			<table border='1' class='grade' align='center' style='text-align:left;'>
				<tr><td colspan='3' align='center' style='width:300px;color:#B71D6F'><b>ACADEMIC ASSESSMENT</b></td><td colspan='2' align='center' style='color:#B71D6F'><b>NON-ACADEMIC ASSESSMENT</b></td></tr>
				<tr><td  style='width:100px'><b>MARKS</b></td><td align='left'><b>GRADE</b></td><td><b>REMARK</b></td><td><b>GRADE</b></td><td ><b>REMARK</b></td></tr>
				<tr><td  style='border-bottom-style:none;'><b>90% AND ABOVE<b></td><td  style='border-bottom-style:none;text-align:left' ><b>A+</b></td><td  style='border-bottom-style:none;'><b>EXCELLENT</b></td><td style='border-bottom-style:none;'><b>A</b></td><td  style='border-bottom-style:none;'><b>VERY GOOD</b></td></tr>
				<tr><td  style='border-top-style:none;border-bottom-style:none;'><b>80 TO 89%<b></td><td  style='border-top-style:none;border-bottom-style:none;text-align:left'><b>A<b></td><td  style='border-top-style:none;border-bottom-style:none;'><b>V.GOOD</b></td><td style='border-bottom-style:none;border-top-style:none;'><b>B</b></td><td  style='border-top-style:none;border-bottom-style:none;'><b>GOOD</b></td></tr>
				<tr><td style='border-top-style:none;border-bottom-style:none;'><b>70 TO 79%<b></td><td  style='border-top-style:none;border-bottom-style:none;text-align:left'><b>B+</b></td><td style='border-top-style:none;border-bottom-style:none;'><b>GOOD</b></td><td style='border-bottom-style:none;border-top-style:none;'><b>C</b></td><td style='border-top-style:none;border-bottom-style:none;'><b>AVERAGE</b></td></tr>
				<tr><td  style='border-top-style:none;border-bottom-style:none;'><b>60 TO 69%<b></td><td  style='border-top-style:none;border-bottom-style:none;text-align:left'><b>B</b></td><td  style='border-top-style:none;border-bottom-style:none;'><b>FAIR</b></td><td style='border-bottom-style:none;border-top-style:none;'><b>D</b></td><td  style='border-top-style:none;border-bottom-style:none;'><b>POOR</b></td></tr>
				<tr><td  style='border-top-style:none;border-bottom-style:none;'><b>50 TO 59%<b></td><td  style='border-top-style:none;border-bottom-style:none;text-align:left'><b>C+</b></td><td  style='border-top-style:none;border-bottom-style:none;'><b>AVERAGE</b></td><td style='border-bottom-style:none;border-top-style:none;'></td><td  style='border-top-style:none;border-bottom-style:none;'></td></tr>
				<tr><td  style='border-top-style:none;border-bottom-style:none;'><b>40 TO 49%<b></td><td  style='border-top-style:none;border-bottom-style:none;text-align:left'><b>C</b></td><td  style='border-top-style:none;border-bottom-style:none;'><b>SATISFACTORY</b></td><td style='border-bottom-style:none;border-top-style:none;'></td><td  style='border-top-style:none;border-bottom-style:none;'></td></tr>
				<tr><td  style='border-top-style:none;'><b>BELOW 40%<b></td><td style='border-top-style:none;'><b>D</b></td><td style='border-top-style:none;text-align:left'><b>FAILED</b></td><td align='center' style='border-top-style:none;'></td><td align='center' style='border-top-style:none;'></td></tr>
			</table>
			<tr><td>
			<table border='1' class='grade' align='center'>
					<tr><td align='center' style='font-size:15px;border-bottom-style:none;color:#B71D6F' colspan='2'><b><u>CRITERIA FOR PROMOTION</u></b></td></tr>
					 <tr><td align='left' style='border-bottom-style:none;border-right-style:none;border-top-style:none;' valign='top'><b>01.</td><td style='border-bottom-style:none;border-left-style:none;border-top-style:none;'><b>THE ACADEMIC YEAR IS SEGMENTED AS TERM I, TERM II AND TERM III.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;' valign='top'><b>02.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>ALL SUBJECTS HAVE COMPONENTS OF INTERNAL ASSESSMENT ON THE BASIS OF ASSIGNMENTS/PROJECTS/CLASS TESTS/PRACTICAL WORKS.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;' valign='top'><b>03.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>NORMALLY A STUDENT SHOULD GAIN SEPARATE MINIMUM PASS MARK IN EACH SUBJECT. HE/SHE MAY BE CONSIDERED FOR PROMOTION EVEN IF HE/SHE FAILS IN ONE SUBJECT OTHER THAN MORAL SCIENCE, ENGLISH AND A POOR GRADE IN SUPW & C S.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;' valign='top'><b>04.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>PROMOTION TO A HIGHER CLASS IS BASED ON ATTENDANCE, CUMULATIVE PASS PERCENTAGE IN ALL MID TERM, TERMINAL EXAMINATIONS AND A GOOD GRADE IN NON ACADEMICS. </b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;' valign='top'><b>05.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>PUPILS WHO ABSENT THEMSELVES FROM EXAMINATIONS WITHOUT GRAVE REASONS WILL BE CONSIDERED AS FAILED. NO RE TEST WILL BE CONDUCTED FOR THE ABSENTEES.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;' valign='top'><b>06.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>SERIOUS ACTION INCLUDING SUSPENSION AND DISMISSAL WILL BE TAKEN AGAINST PUPILS WHO RESORT TO UNFAIR MEANS DURING EXAMINATIONS.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;' valign='top'><b>07.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>PARENTS/GUARDIANS SHOULD FOLLOW UP THE WARD’S PROGRESS IN ACADEMICS AND NON ACADEMICS AND TAKE NECESSARY STEPS IN ORDER TO ACHIEVE FURTHER IMPROVEMENT.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;' valign='top'><b>08.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>THE PROGRESS REPORT CARD SHOULD BE COLLECTED, ACKNOWLEDGED AND RETURNED ON THE STIPULATED DATES.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;'valign='top'><b>09.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>THE SCHOOL RESERVES THE RIGHT TO COMPEL PARENTS/GUARDIANS TO WITHDRAW THEIR WARDS IF THE PROGRESS IN ACADEMICS IS UNSATISFACTORY, ATTENDANCE IS IRREGULAR OR THE CONDUCT IS HARMFUL TO OTHER STUDENTS. NO STUDENT CAN REPEAT THE YEAR MORE THAN TWO YEARS.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-right-style:none;' valign='top'><b>10.</td><td style='border-top-style:none;'><b>THE DECISION OF THE PRINCIPAL IN ALL MATTERS IS FINAL AND BINDING.</b></td></tr>
			</table>
			</td></tr>
			<tr><td>
		<div style='text-align:center;font-size:11px;' width='100px'><b>I, the undersigned agree to abide by these norms and other rules with regard to examinations and promotion procedures.</b></div>

		</td></tr>
		<tr>
		<td>
		<table class='grade' style='border:none;'>
		<tr><td width='50%' style='text-align:left;'><table><tr><td style='width:220px;height:30px;border-style: solid;border-width: 1px;'></td></tr></table></td><td style='text-align:right;'><table><tr><td style='width:220px;height:30px;border-style: solid;border-width: 1px;'></td></tr></table></td></tr>
		<tr><td width='50%' style='text-align:left;'><b>PLACE</b></td><td style='text-align:right;'><b>NAME OF PARENT/GUARDIAN</b></td></tr>
		<tr><td width='50%' style='text-align:left;'><table><tr><td style='width:220px;height:30px;border-style: solid;border-width: 1px;'></td></tr></table></td><td style='text-align:right;'><table><tr><td style='width:220px;height:30px;border-style: solid;border-width: 1px;'></td></tr></table></td></tr>
		<tr><td width='50%' style='text-align:left;'><b>DATE</b></td><td style='text-align:right;'><b>SIGNATURE OF PARENT/GUARDIAN</b></td></tr>
		</table>
		</td>
		</tr>
			</table>
</td><td class='ftd' valign='top' width='50%' style='border:1px solid #B71D6F'>
<table  style='margin-top:5px auto;'>
	 <tr>
		<td align='center' colspan='2'><img src='../img/donbosco.jpg' style='width:120px' /></td>
	 </tr>
	 <tr>
		<td colspan='2' align='center' style='font-size:21px; color:#B71D6F;'><b>DON BOSCO SENIOR SECONDARY SCHOOL</b></td>
	</tr>
	<tr>
		<td align='center' colspan='2' align='center' style='font-size:16px;'><b>(I.C.S.E & I.S.C.KE-051/2000)<b></td>
	</tr>	
	<tr>
		<td align='center' colspan='2' align='center' style='font-size:16px;'><b>VADUTHALA, KOCHI-682 023</b></td>
	</tr>
	<tr>
		<td align='center' colspan='2' align='center' style='font-size:16px;'><b>PHONE:H.S:2435308, L.P: 2436738, PRINCIPAL:2436602</b></td>
	</tr>
	<tr>
		<td align='center' colspan='2' align='center' style='font-size:15px;'><b>E-MAIL: sdbschool@gmail.com, info@donboscoschoolvaduthala.com</b></td>
	</tr>
	<tr>
		<td align='center' colspan='2' align='center'  style='font-size:16px;'><b>WEBSITE : www.donboscoschoolvaduthala.com</b></td>
	</tr>
	<tr><td colspan='2'></td></tr>
	<tr><td align='center' colspan='2' style='font-size:21px; color:#B71D6F;' align='center'><b>PROGRESS REPORT</b></td></tr>
	<tr><td align='center' colspan='2' align='center' style='font-size:21px;color:#B71D6F;'><b>".$academic_year."</b></td></tr>
	<tr><td align='center' colspan='2' ><div class='photo'>";
	if($stud_photoid!="0")
		{
		$sel_photo=$dbobj->select("select * from `student_img` where `id`='".$stud_photoid."'");
		$photo_row=$dbobj->fetch_array($sel_photo);
		$imagebytes=$photo_row['image'];
		$type=$photo_row['type'];
		if($type=='')
		{
		$image= "<img  boreder='0' src='../imagetmp/default.jpg' width='130' height='130' alt='Photo' />";
		}
		else
		{
			$fh=fopen("../imagetmp/tmp".$type."","w");
			fwrite($fh,$imagebytes,strlen($imagebytes));
			$image="<img  border='0' src='../imagetmp/tmp".$type."' width='100' height='100' alt='Photo' />";
		}
		}
		else
		{
			$image= "<img  boreder='0' src='../imagetmp/default.jpg' width='100' height='100' alt='Photo' />";
		}
$main.=$image."</div></td></tr>
<tr><td colspan='2' align='center' style='font-size:20px;color:#B71D6F;'><b>".strtoupper($stname)." ".strtoupper($initial)."</b></td></tr>
<tr><td align='center'>
<table  width='500px' border='0' style='margin-top:35px;'>
		<tr><td ><b>CLASS: ".$classname."</b></td><td width='150px'><b>DIVISION: ".$division."</b></td><td width='150px'><b>ROLL NUMBER:  </b></td><td style='text-align:right'><b>ID.NO : ".$studentid."</b></td></tr>	
		</table>
		<table width='500px'>
		<tr><td><b>DATE OF BIRTH: ".$dob."</b></td></tr>	
		</table >
		<table width='500px'>
		<tr><td width='90px'><b>HOUSE: ".$huse."</b></td><td width='200px'><b>CLUB: ".$clb."</b></td><td width='200px'><b>SUPW: </b></td></tr>	
		</table>
		<table width='500px'>
		<tr><td ><b>FATHER’S  NAME: ".strtoupper($parent['fname'])."<b></td><td style='text-align:right'><b>MOTHER'S NAME: ".strtoupper($parent['mname'])."<b></td></tr>	
		<tr><td colspan='2'><b>ADDRESS: ".$adrs."</b></td></tr>
		</table>
		<table width='500px'>
		<tr><td colspan='2' width='350px'></td><td><b>PIN: </b></td></tr>
		</table>
		<table width='500px' border='0'>
		<tr><td width='350px'><b>PHONE NUMBER LAND: ".$parent['phone']."</b></td><td style='text-align:right'><b>MOBILE: ".$parent['fmob']."</b></td></tr>	
		</table>
		<table width='500px' style='text-align:center'>
		<tr><td></td><td></td><td></td></tr>
		<tr><td style='text-align:left'><table><tr><td style='width:150px;height:30px;border-style: solid;border-width: 1px;'></td></tr></table></td><td><table><tr><td style='width:150px;height:30px;border-style: solid;border-width: 1px;'></td></tr></table></td><td style='text-align:right'><table><tr><td style='width:150px;height:30px;border-style: solid;border-width: 1px;'></td></tr></table></td></tr>
		 <tr><td><b>FATHER'S SIGN</b></td><td><b>MOTHER'S SIGN</b></td><td><b>GUARDIAN'S SIGN</b></td></tr>
		</table>
</td></tr></table>
</td></tr>
</table>
</td></tr>
</table>";
$second="<table style='border:3px solid #B71D6F;border-collapse:collapse'><tr><td><table align='center' border='1' style='border-collapse:collapse;width:100%;font-size:12px'>
<tr><th style='text-align:center;font-size:14px;color:#B71D6F;' colspan='19'><b>ACADEMIC PERFORMANCE</b> </th></tr>".$form."
<tr><th style='width:50%;font-size:14px;color:#B71D6F;' colspan='9'><b>NON-ACADEMIC PERFORMANCE</b></th><th style='width:50%;font-size:14px;color:#B71D6F;' colspan='10'><b>NON ACADEMIC PERFORMANCE</b></th></tr>
<tr ><td valign='top' colspan='9'><table border='1' style='border-collapse:collapse;' width='700px'><tr><th style='text-align:left;'>COMPONENTS</th><th>I TERM</th><th>II TERM</th><th>III TERM</th><th style='text-align:left;'>COMPONENTS</th><th>I TERM</th><th>II TERM</th><th>III TERM</th></tr>
																									<tr><td><b>CONDUCT</b></td><td>".$nonacc['CONDUCT']['I Term']."</td><td>".$nonacc['CONDUCT']['II Term']."</td><td>".$nonacc['CONDUCT']['III Term']."</td><td><b>GROUPS&MOVEMENTS</b></td><td>".$nonacc['GROUPS & MOVEMENTS']['I Term']."</td><td>".$nonacc['GROUPS & MOVEMENTS']['II Term']."</td><td>".$nonacc['GROUPS & MOVEMENTS']['III Term']."</td></tr>
																									<tr><td><b>RESPONSIBILITY</b></td><td>".$nonacc['RESPONSIBILITY']['I Term']."</td><td>".$nonacc['RESPONSIBILITY']['II Term']."</td><td>".$nonacc['RESPONSIBILITY']['III Term']."</td><td><b>LISTENING ENGLISH</b></td><td>".$enonacc['LISTENING ENGLISH']['I Term']."</td><td>".$enonacc['LISTENING ENGLISH']['II Term']."</td><td>".$enonacc['LISTENING ENGLISH']['III Term']."</td></tr>
																									<tr><td><b>PUNCTUALITY</b></td><td>".$nonacc['PUNCTUALITY']['I Term']."</td><td>".$nonacc['PUNCTUALITY']['II Term']."</td><td>".$nonacc['PUNCTUALITY']['III Term']."</td><td><b>SPEAKING ENGLISH</b></td><td>".$enonacc['SPEAKING ENGLISH']['I Term']."</td><td>".$enonacc['SPEAKING ENGLISH']['II Term']."</td><td>".$enonacc['SPEAKING ENGLISH']['III Term']."</td></tr>
																									<tr><td><b>DISCIPLINE</b></td><td>".$nonacc['DISCIPLINE']['I Term']."</td><td>".$nonacc['DISCIPLINE']['II Term']."</td><td>".$nonacc['DISCIPLINE']['III Term']."</td><td><b>READING ENGLISH</b></td><td>".$enonacc['READING ENGLISH']['I Term']."</td><td>".$enonacc['READING ENGLISH']['II Term']."</td><td>".$enonacc['READING ENGLISH']['III Term']."</td></tr>
																									<tr><td><b>LEADERSHIP</b></td><td>".$nonacc['LEADERSHIP']['I Term']."</td><td>".$nonacc['LEADERSHIP']['II Term']."</td><td>".$nonacc['LEADERSHIP']['III Term']."</td><td><b>LISTENING MALAYALAM</b></td><td>".$enonacc['LISTENING MALAYALAM']['I Term']."</td><td>".$enonacc['LISTENING MALAYALAM']['II Term']."</td><td>".$enonacc['LISTENING MALAYALAM']['III Term']."</td></tr>
																									<tr><td><b>E C A</b></td><td>".$nonacc['ECA']['I Term']."</td><td>".$nonacc['ECA']['II Term']."</td><td>".$nonacc['ECA']['III Term']."</td><td><b>SPEAKING MALAYALAM</b></td><td>".$enonacc['SPEAKING MALAYALAM']['I Term']."</td><td>".$enonacc['SPEAKING MALAYALAM']['II Term']."</td><td>".$enonacc['SPEAKING MALAYALAM']['III Term']."</td></tr>
																									<tr><td><b>AQUATICS</b></td><td>".$nonacc['AQUATICS']['I Term']."</td><td>".$nonacc['AQUATICS']['II Term']."</td><td>".$nonacc['AQUATICS']['III Term']."</td><td><b>READING  MALAYALAM</b></td><td>".$enonacc['READING  MALAYALAM']['I Term']."</td><td>".$enonacc['READING  MALAYALAM']['II Term']."</td><td>".$enonacc['READING  MALAYALAM']['III Term']."</td></tr>
																									<tr><td><b>SUPW & C.SERVICE</b></td><td>".$nonacc['SUPW & C.SERVICE']['I Term']."</td><td>".$nonacc['SUPW & C.SERVICE']['II Term']."</td><td>".$nonacc['SUPW & C.SERVICE']['III Term']."</td><td><b>LISTENING HINDI</b></td><td>".$enonacc['LISTENING HINDI']['I Term']."</td><td>".$enonacc['LISTENING HINDI']['II Term']."</td><td>".$enonacc['LISTENING HINDI']['III Term']."</td></tr>
																																										
																									</table>";

$second.="</td><td colspan='10'><table border='1' style='border-collapse:collapse;' width='650px'>
<tr><th style='text-align:left;'>COMPONENTS</th><th>I TERM</th><th>II TERM</th><th>III TERM</th></tr>
<tr><td><b>SPEAKING HINDI</b></td><td>".$enonacc['SPEAKING HINDI']['I Term']."</td><td>".$enonacc['SPEAKING HINDI']['II Term']."</td><td>".$enonacc['SPEAKING HINDI']['III Term']."</td></tr>
<tr><td><b>READING HINDI</b></td><td>".$enonacc['READING HINDI']['I Term']."</td><td>".$enonacc['READING HINDI']['II Term']."</td><td>".$enonacc['READING HINDI']['III Term']."</td></tr>
</table>".$att."
</td></tr>";
$second.="<tr><td colspan='19' valign='top' style='border-bottom-style:none;'>".$rem."</td></tr><br><br>
<tr><th colspan='19' style='border-top-style:none;border-bottom-style:none;'></th></tr>
<tr><th colspan='19' style='border-top-style:none;border-bottom-style:none;'></th></tr>
<tr><th colspan='19' style='border-top-style:none;border-bottom-style:none;'></th></tr>
<tr><th colspan='19' style='border-top-style:none;border-bottom-style:none;'></th></tr>
<tr><th colspan='19' style='border-top-style:none;border-bottom-style:none;'></th></tr>
<tr><th colspan='19' style='border-top-style:none;'><b>SIGNATURE OF CLASS TEACHER &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SCHOOL SEAL &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SIGNATURE OF HEADMASTER</b></th></tr>
</table></td></tr></table>";
?>
<?php
include("../MPDF/mpdf.php");
 
$mpdf=new mPDF(); 

$mpdf->SetDisplayMode('fullpage','two');

//$mpdf->mirrorMargins = 1;
// LOAD a stylesheet
$stylesheet = file_get_contents('../css/progress_final1.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->setHeader();	// Clear headers before adding page
$mpdf->AddPage('A4','','','','',1,1,1,1,1,1);
$mpdf->WriteHTML($main);
$mpdf->AddPage('A4','','','','',1,1,11,3,3,3);
$mpdf->WriteHTML($second);
$mpdf->setHeader();	// Clear headers before adding page

$mpdf->Output('mpdf.pdf','I');
exit;
?>
