<?php
include_once('../db/createdb.php');
class Store
{

	public function __construct()
	{
		$this->classno=$classno;
		
	}
	function Get_subcategory($cat)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$i=0;
		$sel=$dbobject->select("select * from `sub_datas` where `category`='".$cat."'");
		while($row=$dbobject->fetch_array($sel))
		{
			$subcat[$i]=$row['datas'];
			$i++;
		}
		return $subcat;
	}
	function get_allproduct()
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$i=0;
		$sel=$dbobject->select("select * from `products` order by `pname`");
		while($row=$dbobject->fetch_array($sel))
		{
			$products[$i]['id']=$row['id'];
			$products[$i]['pname']=$row['pname'];
			$i++;
		}
		return $products;
	}
	function get_allproduct2()
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$i=0;
		$sel=$dbobject->select("select * from `products2` order by `pname`");
		while($row=$dbobject->fetch_array($sel))
		{
			$products[$i]['id']=$row['id'];
			$products[$i]['pname']=$row['pname'];
			$i++;
		}
		return $products;
	}	
	function get_manufacturer($product)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$i=0;
		$sel=$dbobject->select("select distinct `manufacture` from `products` order by `manufacture`");
		while($row=$dbobject->fetch_array($sel))
		{
			$man[$i]=$row['manufacture'];
			$i++;
		}
		return $man;
	}
	function get_allProducts()
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$i=0;
		$sel=$dbobject->select("select * from `products`  order by `pname`");
		while($row=$dbobject->fetch_array($sel))
		{
			$product[$i]['id']=$row['id'];
			$product[$i]['pname']=$row['pname'];
			$product[$i]['manufacture']=$row['manufacture'];
			$product[$i]['cat']=$row['cat'];			
			$product[$i]['sale_price']=$row['sale_price'];			
			$i++;
		}
		return $product;
	}
	function get_allProducts2()
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$i=0;
		$sel=$dbobject->select("select * from `products2`  order by `pname`");
		while($row=$dbobject->fetch_array($sel))
		{
			$product[$i]['id']=$row['id'];
			$product[$i]['pname']=$row['pname'];
			$product[$i]['manufacture']=$row['manufacture'];	
			$product[$i]['cat']=$row['cat'];			
			$product[$i]['sale_price']=$row['sale_price'];				
			$i++;
		}
		return $product;
	}
	function get_allProducts3()
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$i=0;
		$sel=$dbobject->select("select * from `products3`  order by `pname`");
		while($row=$dbobject->fetch_array($sel))
		{
			$product[$i]['id']=$row['id'];
			$product[$i]['pname']=$row['pname'];
			$product[$i]['manufacture']=$row['manufacture'];
			$product[$i]['cat']=$row['cat'];			
			$product[$i]['sale_price']=$row['sale_price'];				
			$i++;
		}
		return $product;
	}	
    function store_purchaseDatewise($date_from,$date_to)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		
		$date_from= date('Y-m-d', strtotime($date_from));
		$date_to= date('Y-m-d', strtotime($date_to));
		//echo "SELECT * FROM `store_purchase_details` WHERE `date`>='".$date_from."' and `date`<='".$date_to."'";
		$i=0;
		$sel=$dbobject->select("SELECT * FROM `store_purchase_details` WHERE `date`>='".$date_from."' and `date`<='".$date_to."'");
		while($row=$dbobject->fetch_array($sel))
		{
			$store[$i]['date']=$row['date'];
			$store[$i]['product_id']=$row['product_id'];
			$store[$i]['manufacturer']=$row['manufacturer'];
			$store[$i]['number_of_items']=$row['number_of_items'];
			$store[$i]['stock']=$row['stock'];
			$store[$i]['costprice']=$row['costprice'];
			$store[$i]['sale_price']=$row['sale_price'];
			$store[$i]['mrp']=$row['mrp'];
			$store[$i]['entry_by']=$row['entry_by'];
			$store[$i]['entry_date']=$row['entry_date'];	
			$store[$i]['id']=$row['id'];			
			$i++;
		}
		return $store;
	}
    function store_purchaseDatewise2($date_from,$date_to)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		
		$date_from= date('Y-m-d', strtotime($date_from));
		$date_to= date('Y-m-d', strtotime($date_to));
		//echo "SELECT * FROM `store_purchase_details` WHERE `date`>='".$date_from."' and `date`<='".$date_to."'";
		$i=0;
		$sel=$dbobject->select("SELECT * FROM `store_purchase_details2` WHERE `date`>='".$date_from."' and `date`<='".$date_to."'");
		while($row=$dbobject->fetch_array($sel))
		{
			$store[$i]['date']=$row['date'];
			$store[$i]['product_id']=$row['product_id'];
			$store[$i]['manufacturer']=$row['manufacturer'];
			$store[$i]['number_of_items']=$row['number_of_items'];
			$store[$i]['stock']=$row['stock'];
			$store[$i]['costprice']=$row['costprice'];
			$store[$i]['sale_price']=$row['sale_price'];
			$store[$i]['mrp']=$row['mrp'];
			$store[$i]['entry_by']=$row['entry_by'];
			$store[$i]['entry_date']=$row['entry_date'];	
			$store[$i]['id']=$row['id'];			
			$i++;
		}
		return $store;
	}	
    function store_purchase_return_Datewise($date_from,$date_to,$store_type)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		
		$date_from= date('Y-m-d', strtotime($date_from));
		$date_to= date('Y-m-d', strtotime($date_to));
		//echo "SELECT * FROM `store_purchase_details` WHERE `date`>='".$date_from."' and `date`<='".$date_to."'";
		$i=0;
		//echo "SELECT * FROM `store_purchase_return` WHERE `cerdit_date`>='".$date_from."' and `cerdit_date`<='".$date_to."' and `store_type`='".$store_type."'";
		$sel=$dbobject->select("SELECT * FROM `store_purchase_return` WHERE `cerdit_date`>='".$date_from."' and `cerdit_date`<='".$date_to."' and `store_type`='".$store_type."'");
		while($row=$dbobject->fetch_array($sel))
		{
			$store[$i]['qty']=$row['qty'];
			$store[$i]['cerdit_date']=$row['cerdit_date'];
			$store[$i]['entry_date']=$row['entry_date'];
			$store[$i]['entry_by']=$row['entry_by'];
			$store[$i]['pid']=$row['pid'];
			$store[$i]['store_type']=$row['store_type'];
			$store[$i]['id']=$row['id'];			
			$store[$i]['supplier_name']=$row['supplier_name'];			
			$store[$i]['cerdit_no']=$row['cerdit_no'];			
			$store[$i]['remark']=$row['remark'];			
			$store[$i]['acyear']=$row['acyear'];			
			$i++;
		}
		return $store;
	}
    function store_salesDatewise($date_from,$date_to)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		
		$date_from= date('Y-m-d', strtotime($date_from));
		$date_to= date('Y-m-d', strtotime($date_to));
		//echo "SELECT * FROM `store_purchase_details` WHERE `date`>='".$date_from."' and `date`<='".$date_to."'";
		$i=0;
		$sel=$dbobject->select("SELECT * FROM `sales` WHERE `salesdate`>='".$date_from."' and `salesdate`<='".$date_to."'");
		while($row=$dbobject->fetch_array($sel))
		{
			$store[$i]['salesdate']=$row['salesdate'];
			$store[$i]['productid']=$row['productid'];
			$store[$i]['qty']=$row['qty'];
			$store[$i]['price']=$row['price'];
			$store[$i]['paid_amount']=$row['paid_amount'];
			$store[$i]['pay_type']=$row['pay_type'];
			$store[$i]['sid']=$row['sid'];
			$store[$i]['studentid']=$row['studentid'];
			$store[$i]['transaction_id']=$row['transaction_id'];
            $i++;
		}
		return $store;
	}	
	function Product_Byid($prodid)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$i=0;
		$sel=$dbobject->select("select * from `products` where `id`='".$prodid."'");
		$row=$dbobject->fetch_array($sel);
		
		    $product['id']=$row['id'];
			$product['pname']=$row['pname'];
			$product['manufacture']=$row['manufacture'];
			$product['descr']=$row['descr'];
			$product['cat']=$row['cat'];
			$product['sub_cat']=$row['sub_cat'];				
		
		return $row;
	}
	function Product_Byid2($prodid)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$i=0;
		$sel=$dbobject->select("select * from `products2` where `id`='".$prodid."'");
		$row=$dbobject->fetch_array($sel);
		
		    $product['id']=$row['id'];
			$product['pname']=$row['pname'];
			$product['manufacture']=$row['manufacture'];
			$product['descr']=$row['descr'];
			$product['cat']=$row['cat'];
			$product['sub_cat']=$row['sub_cat'];				
		
		return $row;
	}
	function Product_Byid3($prodid)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$i=0;
		$sel=$dbobject->select("select * from `products3` where `id`='".$prodid."'");
		$row=$dbobject->fetch_array($sel);
		
		    $product['id']=$row['id'];
			$product['pname']=$row['pname'];
			$product['manufacture']=$row['manufacture'];
			$product['descr']=$row['descr'];
			$product['cat']=$row['cat'];
			$product['sub_cat']=$row['sub_cat'];				
		
		return $row;
	}	
	function stock_totalBymanufacture($prodid)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		//$i=0;
		$sel=$dbobject->select("SELECT * FROM `store_purchase_details` WHERE `product_id`='".$prodid."'");
        while($row=$dbobject->fetch_array($sel))
		{
			$manufacturer=$row['manufacturer'];
			$tstock[$manufacturer]=$tstock[$manufacturer]+$row['stock'];
			//$tnumber[$manufacturer]=$tnumber[$manufacturer]+$row['number_of_items'];

	   //  $i++;
		}
		$prodet=$tstock;
		//$prodet['stock']=$tstock;
		//$prodet['number_of_items']=$tnumber;
		
		return $prodet;
	}
	function product_stock($product_id)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		//		$acyear=$dbobject->get_acyear();
		$acyear=$this->get_acyear();
		$sel_price=$dbobject->select("SELECT * FROM `stock_item` where `product_id`='".$product_id."' and `acyear`='".$acyear."'");
		$row=$dbobject->fetch_array($sel_price);	
		return $row['qty'];
	}
	function product_stock2($product_id)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		//		$acyear=$dbobject->get_acyear();
		$acyear=$this->get_acyear();
		$sel_price=$dbobject->select("SELECT * FROM `stock_item2` where `product_id`='".$product_id."' and `acyear`='".$acyear."'");
		$row=$dbobject->fetch_array($sel_price);	
		return $row['qty'];
	}	
	function store_stockByid($storeid)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		//echo "SELECT * FROM `store_purchase_details` WHERE `id`='".$storeid."'";
		$sel=$dbobject->select("SELECT * FROM `store_purchase_details` WHERE `id`='".$storeid."'");
        $row=$dbobject->fetch_array($sel);
		$product=$row['stock'];
		return $product;
	}
	function get_product_price($pid)
	{
		 $dbobject = new DB();
		 $dbobject->getCon();
		 $sel_price=$dbobject->select("select * from `products` where `id`='".$pid."'");
		 $row=$dbobject->fetch_array($sel_price);
		return $t=$row['sale_price'];
	}
	function get_product_price2($pid)
	{
		 $dbobject = new DB();
		 $dbobject->getCon();
		 $sel_price=$dbobject->select("select * from `products2` where `id`='".$pid."'");
		 $row=$dbobject->fetch_array($sel_price);
		return $t=$row['sale_price'];
	}
	function get_product_price3($pid)
	{
		 $dbobject = new DB();
		 $dbobject->getCon();
		 $sel_price=$dbobject->select("select * from `products3` where `id`='".$pid."'");
		 $row=$dbobject->fetch_array($sel_price);
		return $t=$row['sale_price'];
	}
	function get_product_stock($pid)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		//		$acyear=$dbobject->get_acyear();
		$acyear=$this->get_acyear();
		$sel_price=$dbobject->select("SELECT * FROM `stock_item` where `product_id`='".$pid."' and `acyear`='".$acyear."'");
		$row=$dbobject->fetch_array($sel_price);
		$t['id']=$row['id'];
		$t['stock']=$row['qty'];
		return $t;
	}
	function get_product_stock2($pid)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		//		$acyear=$dbobject->get_acyear();
		$acyear=$this->get_acyear();
		$sel_price=$dbobject->select("SELECT * FROM `stock_item2` where `product_id`='".$pid."' and `acyear`='".$acyear."'");
		$row=$dbobject->fetch_array($sel_price);
		$t['id']=$row['id'];
		$t['stock']=$row['qty'];
		return $t;
	}
	function purchase_order()
	{
		$dbobject = new DB();
		$dbobject->getCon();
		//		$acyear=$dbobject->get_acyear();
		$acyear=$this->get_acyear();		
		$sel_price=$dbobject->select("select * from `purchase_order` where `acyear`='".$acyear."'");
		$i=0;
		while($row=$dbobject->fetch_array($sel_price))
		{
			$i++;
			$det[$i]['product_id']=$row['product_id'];
			$det[$i]['qty']=$row['qty'];
			$det[$i]['qamount']=$row['qamount'];	
			$det[$i]['date']=$row['date'];
			$det[$i]['id']=$row['id'];
			
			
		}		
		return $det;
	}
	function purchase_order2()
	{
		$dbobject = new DB();
		$dbobject->getCon();
		//		$acyear=$dbobject->get_acyear();
		$acyear=$this->get_acyear();		
		$sel_price=$dbobject->select("select * from `purchase_order2` where `acyear`='".$acyear."'");
		$i=0;
		while($row=$dbobject->fetch_array($sel_price))
		{
			$i++;
			$det[$i]['product_id']=$row['product_id'];
			$det[$i]['qty']=$row['qty'];
			$det[$i]['qamount']=$row['qamount'];	
			$det[$i]['date']=$row['date'];
			$det[$i]['id']=$row['id'];
			
			
		}		
		return $det;
	}	
	function stock_update_product_edit($product_id,$stock,$current_stock)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		//		$acyear=$dbobject->get_acyear();
		$acyear=$this->get_acyear();
		//echo "SELECT * FROM `stock_item` where `product_id`='".$product_id."'";
		$sel_price=$dbobject->select("SELECT * FROM `stock_item` where `product_id`='".$product_id."' and `acyear`='".$acyear."'");
		$check=$dbobject->mysql_num_row($sel_price);
		if($check==0)
		{
			$insert=$dbobject->exe_qry("INSERT INTO `stock_item`( `product_id`,`qty`, `acyear`) VALUES ('".$product_id."','".$stock."','".$acyear."')");
		}
		else
		{
					$row=$dbobject->fetch_array($sel_price);	
					$id=$row['id'];
					//echo $old_stock=$row['qty']."old<br>";
					$old_stock=$row['qty'];
					//echo $current_stock." crt<br>";
					//echo $stock." nw<br>";
					if($current_stock>$stock)
					{
						$stock=$current_stock-$stock;
						//echo $new_stock=$old_stock-$stock;
						$new_stock=$old_stock-$stock;
						$val=1;
					}
					elseif($stock>$current_stock)
					{
						$stock=$stock-$current_stock;
						//echo $stock=$stock-$current_stock;
						//echo " st<br>";
						//echo $new_stock=$old_stock+$stock;
						$new_stock=$old_stock+$stock;
						$val=1;
					}
					if($val==1)
					{
						$updt=$dbobject->exe_qry("UPDATE `stock_item` SET `qty`='".$new_stock."' WHERE `id`='".$id."'");
					}
					
		}
	
        		
	}
	function store_product_return($storeid)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$sel_price=$dbobject->select("select * from `store_purchase_return` where `pid`='".$storeid."' ");
		$total_return_stock=0;
		while($row=$dbobject->fetch_array($sel_price))
		{
			 $return_stock=$row['return_stock'];
		     $total_return_stock=$total_return_stock+$return_stock;
		}
		return $total_return_stock;

	        		
	}
	function stock_update_product_return($product_id,$stock)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		//		$acyear=$dbobject->get_acyear();
		$acyear=$this->get_acyear();
		//echo "SELECT * FROM `stock_item` where `product_id`='".$product_id."'";
		$sel_price=$dbobject->select("SELECT * FROM `stock_item` where `product_id`='".$product_id."' and `acyear`='".$acyear."'");	
		$row=$dbobject->fetch_array($sel_price);	
		$id=$row['id'];
        $old_stock=$row['qty'];
       $new_stock=$old_stock-$stock;		
		$updt=$dbobject->exe_qry("UPDATE `stock_item` SET `qty`='".$new_stock."' WHERE `id`='".$id."'");	
	        		
	}	
	function stock_update_product_addition($product_id,$stock)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		//		$acyear=$dbobject->get_acyear();
		$acyear=$this->get_acyear();
		//echo "SELECT * FROM `stock_item` where `product_id`='".$product_id."'";
		$sel_price=$dbobject->select("SELECT * FROM `stock_item` where `product_id`='".$product_id."' and `acyear`='".$acyear."'");	
		$check=$dbobject->mysql_num_row($sel_price);
		if($check==0)
		{
			$insert=$dbobject->exe_qry("INSERT INTO `stock_item`( `product_id`,`qty`, `acyear`) VALUES ('".$product_id."','".$stock."','".$acyear."')");
		}
		else
		{
					$row=$dbobject->fetch_array($sel_price);	
					$id=$row['id'];
					$old_stock=$row['qty'];
					$new_stock=$old_stock+$stock;			
					$updt=$dbobject->exe_qry("UPDATE `stock_item` SET `qty`='".$new_stock."' WHERE `id`='".$id."'");
					
					
		}
        		
	}	
	function stock_update_product_addition2($product_id,$stock)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		//		$acyear=$dbobject->get_acyear();
		$acyear=$this->get_acyear();
		//echo "SELECT * FROM `stock_item` where `product_id`='".$product_id."'";
		$sel_price=$dbobject->select("SELECT * FROM `stock_item2` where `product_id`='".$product_id."' and `acyear`='".$acyear."'");	
		$check=$dbobject->mysql_num_row($sel_price);
		if($check==0)
		{
			$insert=$dbobject->exe_qry("INSERT INTO `stock_item2`( `product_id`,`qty`, `acyear`) VALUES ('".$product_id."','".$stock."','".$acyear."')");
		}
		else
		{
					$row=$dbobject->fetch_array($sel_price);	
					$id=$row['id'];
					$old_stock=$row['qty'];
					$new_stock=$old_stock+$stock;			
					$updt=$dbobject->exe_qry("UPDATE `stock_item2` SET `qty`='".$new_stock."' WHERE `id`='".$id."'");
					
					
		}
        		
	}	
	function opening_stock($product_id,$stock)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		//		$acyear=$dbobject->get_acyear();
		$acyear=$this->get_acyear();
		$sel_price=$dbobject->select("SELECT * FROM `opening_stock` where `product_id`='".$product_id."' and `acyear`='".$acyear."'");
		$check=$dbobject->mysql_num_row($sel_price);
		if($check==0)
		{
			$insert=$dbobject->exe_qry("INSERT INTO `opening_stock`( `product_id`,`qty`, `acyear`) VALUES ('".$product_id."','".$stock."','".$acyear."')");
		    $current_stock=0;
			$val=2;
		}
		else
		{
					$row=$dbobject->fetch_array($sel_price);	
					$id=$row['id'];
					$old_stock=$row['qty'];
					$updt=$dbobject->exe_qry("UPDATE `opening_stock` SET `qty`='".$stock."' WHERE `id`='".$id."'");
					//echo $old_stock."old<br>";
					//echo $stock." stk<br>";					
					if($old_stock>$stock)
					{
						$stock=$old_stock-$stock;
						//echo $stock."-";
						$val=1;
					}
					elseif($stock>$old_stock)
					{
						$stock=$stock-$old_stock;
						//echo $stock;
						//echo " +<br>";
						$val=2;
						
					}
					
		}
	   $updt_stockitem=$this->stock_update_opening_stock($product_id,$stock,$val);
        		
	}
	function stock_update_opening_stock($product_id,$stock,$val)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		//		$acyear=$dbobject->get_acyear();
		$acyear=$this->get_acyear();
		//echo "SELECT * FROM `stock_item` where `product_id`='".$product_id."'";
		$sel_price=$dbobject->select("SELECT * FROM `stock_item` where `product_id`='".$product_id."' and `acyear`='".$acyear."'");
		$check=$dbobject->mysql_num_row($sel_price);
		if($check==0)
		{
			$insert=$dbobject->exe_qry("INSERT INTO `stock_item`( `product_id`,`qty`, `acyear`) VALUES ('".$product_id."','".$stock."','".$acyear."')");
		}
		else
		{
					$row=$dbobject->fetch_array($sel_price);	
					$id=$row['id'];
					$old_stock=$row['qty']."old<br>";
					$old_stock=$row['qty'];
					//echo $current_stock." crt<br>";
					//echo $stock." nw<br>";
					if($val==1)
					{
						//$stock=$current_stock-$stock;
						$new_stock=$old_stock-$stock;
						$upt_chk=1;
						
					}
					elseif($val==2)
					{
						//echo $stock=$stock-$current_stock;
						//echo " st<br>";
						$new_stock=$old_stock+$stock;
						$upt_chk=1;
						
					}
					if($upt_chk==1)
					{
						$updt=$dbobject->exe_qry("UPDATE `stock_item` SET `qty`='".$new_stock."' WHERE `id`='".$id."'");
					}
					
		}
	
        		
	}	
	function opening_stock2($product_id,$stock)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		//		$acyear=$dbobject->get_acyear();
		$acyear=$this->get_acyear();
		$sel_price=$dbobject->select("SELECT * FROM `opening_stock2` where `product_id`='".$product_id."' and `acyear`='".$acyear."'");
		$check=$dbobject->mysql_num_row($sel_price);
		if($check==0)
		{
			$insert=$dbobject->exe_qry("INSERT INTO `opening_stock2`( `product_id`,`qty`, `acyear`) VALUES ('".$product_id."','".$stock."','".$acyear."')");
		    $current_stock=0;
			$val=2;
		}
		else
		{
					$row=$dbobject->fetch_array($sel_price);	
					$id=$row['id'];
					$old_stock=$row['qty'];
					$updt=$dbobject->exe_qry("UPDATE `opening_stock2` SET `qty`='".$stock."' WHERE `id`='".$id."'");
					//echo $old_stock."old<br>";
					//echo $stock." stk<br>";					
					if($old_stock>$stock)
					{
						$stock=$old_stock-$stock;
						//echo $stock."-";
						$val=1;
					}
					elseif($stock>$old_stock)
					{
						$stock=$stock-$old_stock;
						//echo $stock;
						//echo " +<br>";
						$val=2;
						
					}
				//	echo $val;
		}
	   $updt_stockitem=$this->stock_update_opening_stock2($product_id,$stock,$val);
        		
	}
	function stock_update_opening_stock2($product_id,$stock,$val)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		//		$acyear=$dbobject->get_acyear();
		$acyear=$this->get_acyear();
		//echo "SELECT * FROM `stock_item` where `product_id`='".$product_id."'";
		$sel_price=$dbobject->select("SELECT * FROM `stock_item2` where `product_id`='".$product_id."' and `acyear`='".$acyear."'");
		$check=$dbobject->mysql_num_row($sel_price);
		if($check==0)
		{
			$insert=$dbobject->exe_qry("INSERT INTO `stock_item2`( `product_id`,`qty`, `acyear`) VALUES ('".$product_id."','".$stock."','".$acyear."')");
		}
		else
		{
					$row=$dbobject->fetch_array($sel_price);	
					$id=$row['id'];
					$old_stock=$row['qty']."old<br>";
					$old_stock=$row['qty'];
					//echo $current_stock." crt<br>";
					//echo $stock." nw<br>";
					if($val==1)
					{
						//$stock=$current_stock-$stock;
						$new_stock=$old_stock-$stock;
						$upt_chk=1;
						
					}
					elseif($val==2)
					{
						//echo $stock=$stock-$current_stock;
						//echo " st<br>";
						$new_stock=$old_stock+$stock;
						$upt_chk=1;
						
					}
					if($upt_chk==1)
					{
						$updt=$dbobject->exe_qry("UPDATE `stock_item2` SET `qty`='".$new_stock."' WHERE `id`='".$id."'");
					}
					
		}
	
        		
	}	
	
function open_stock_det($product_id)
{
		$dbobject = new DB();
		$dbobject->getCon();
		//		$acyear=$dbobject->get_acyear();
		$acyear=$this->get_acyear();
		$sel_price=$dbobject->select("SELECT * FROM `opening_stock` where `product_id`='".$product_id."' and `acyear`='".$acyear."'");
		$row=$dbobject->fetch_array($sel_price);	
		return $row['qty'];
}	
function open_stock_det2($product_id)
{
		$dbobject = new DB();
		$dbobject->getCon();
		//		$acyear=$dbobject->get_acyear();
		$acyear=$this->get_acyear();
		$sel_price=$dbobject->select("SELECT * FROM `opening_stock2` where `product_id`='".$product_id."' and `acyear`='".$acyear."'");
		$row=$dbobject->fetch_array($sel_price);	
		return $row['qty'];
}
function total_product_stock($opening_stock,$total_stock)
{
	 if($opening_stock!=0)
	 {
		 if($opening_stock>$total_stock)
		 {
			 $stock=$opening_stock-$total_stock;
		 }
		 elseif($total_stock>$opening_stock)
		 {
			 $stock=$total_stock-$opening_stock;
		 }
		 
	 }
	 else
	 {
		 $stock=$total_stock;
	 }
	 return $stock;
}
	function Invoice_num1($acyear)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		//$acyear=$dbobject->get_acyear();
		$sel_invoice=$dbobject->select("SELECT MAX(`reciept_no`) AS invoice_id FROM `rec_series_store` where `acyear`='".$acyear."'");
		$invoice_row=$dbobject->fetch_array($sel_invoice);
		$invID=$invoice_row['invoice_id'];
		if($invID=="")
		{
			$invID=1;
		}
		else
		{
			$invID=$invID+1;
		}
		return $invID = str_pad($invID, 4, '0', STR_PAD_LEFT);
	}
	function Invoice_num2($acyear)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		//$acyear=$dbobject->get_acyear();
		$sel_invoice=$dbobject->select("SELECT MAX(`reciept_no`) AS invoice_id FROM `rec_series_store2` where `acyear`='".$acyear."'");
		$invoice_row=$dbobject->fetch_array($sel_invoice);
		$invID=$invoice_row['invoice_id'];
		if($invID=="")
		{
			$invID=1;
		}
		else
		{
			$invID=$invID+1;
		}
		return $invID = str_pad($invID, 4, '0', STR_PAD_LEFT);
	}
	function Invoice_num3($acyear)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		//$acyear=$dbobject->get_acyear();
		$sel_invoice=$dbobject->select("SELECT MAX(`reciept_no`) AS invoice_id FROM `rec_series_store3` where `acyear`='".$acyear."'");
		$invoice_row=$dbobject->fetch_array($sel_invoice);
		$invID=$invoice_row['invoice_id'];
		if($invID=="")
		{
			$invID=1;
		}
		else
		{
			$invID=$invID+1;
		}
		return $invID = str_pad($invID, 4, '0', STR_PAD_LEFT);
	}
	function sales_group($store_type)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$sel_invoice=$dbobject->select("SELECT * FROM `sale_group` where `store_type`='".$store_type."'");
		$i=0;
		while($invoice_row=$dbobject->fetch_array($sel_invoice))
		{
			$data[$i]['id']=$invoice_row['id'];
			$data[$i]['name']=$invoice_row['name'];
		 $i++;
		}
		return $data;
	}
	function sales_group_data($grpid)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$sel_invoice=$dbobject->select("SELECT * FROM `sales_group_data` where `grpid`='".$grpid."'");
		$i=0;
		while($invoice_row=$dbobject->fetch_array($sel_invoice))
		{
			$data[$i]['id']=$invoice_row['id'];
			$data[$i]['pid']=$invoice_row['pid'];
			$data[$i]['qty']=$invoice_row['qty'];
		 $i++;
		}
		return $data;
	}
	function sales_datewise($date_from,$date_to,$store_type,$productid)
	{
		$dbobject = new DB();
		$dbobject->getCon();
           $date_from= date('Y-m-d', strtotime($date_from));
		    $date_to= date('Y-m-d', strtotime($date_to)); 		
			$store_tables_By_type=$this->store_tables_By_type($store_type);
			$table=$store_tables_By_type['table'];
			$ptable=$store_tables_By_type['ptable'];	
$sql="SELECT * FROM ".$table." left join ".$ptable." on ".$ptable.".`id`=".$table.".`productid` WHERE ".$table.".`salesdate`>='".$date_from."' and ".$table.".`salesdate`<='".$date_to."' and ".$table.".`store_type`='".$store_type."'";
if($productid!="-1")
{
$sql.=" and ".$ptable.".`id`='".$productid."'";	
}
$sql.=" order by FIELD (".$ptable.".`cat`,'NOTEBOOKS (DB TEX)','SCHOOL UNIFORM (PRASHANTHI)','SCHOOL BELT (DB TEX)','SCHOOL SOCKS (DB TEX)','SCHOOL ID CARD','BIG SHOPPER','CRAYONS','SCHOOL EMBLEM','TEXTBOOKS','TEXTBOOKHSS','OTHER')";	
		$sel_invoice=$dbobject->select($sql);
		$i=0;
		while($invoice_row=$dbobject->fetch_array($sel_invoice))
		{
			$data[$i]['sid']=$invoice_row['sid'];
			$data[$i]['studentid']=$invoice_row['studentid'];
			$data[$i]['transaction_id']=$invoice_row['transaction_id'];
			$data[$i]['qty']=$invoice_row['qty'];
			$data[$i]['price']=$invoice_row['price'];
			$data[$i]['productid']=$invoice_row['productid'];
			$data[$i]['total_amount']=$invoice_row['total_amount'];
			$data[$i]['paid_amount']=$invoice_row['paid_amount'];
			$data[$i]['salesdate']=$invoice_row['salesdate'];
			$data[$i]['store_type']=$invoice_row['store_type'];
			$data[$i]['discount']=$invoice_row['discount'];
			$data[$i]['gst_amt']=$invoice_row['gst_amt'];
			$data[$i]['store2_productid']=$invoice_row['store2_productid'];
		 $i++;
		}
		return $data;
	}
	function sales_datewise_By_usertype($date_from,$date_to,$store_type,$productid,$usertype)
	{
		$dbobject = new DB();
		$dbobject->getCon();
           $date_from= date('Y-m-d', strtotime($date_from));
		    $date_to= date('Y-m-d', strtotime($date_to)); 		
			$store_tables_By_type=$this->store_tables_By_type($store_type);
			$table=$store_tables_By_type['table'];
			$ptable=$store_tables_By_type['ptable'];		
$sql="SELECT * FROM ".$table." left join ".$ptable." on ".$ptable.".`id`=".$table.".`productid` WHERE ".$table.".`salesdate`>='".$date_from."' and ".$table.".`salesdate`<='".$date_to."' and ".$table.".`store_type`='".$store_type."'";
if($productid!="-1")
{
$sql.=" and ".$ptable.".`id`='".$productid."'";	
}
if($usertype!="" && $usertype!="-1")
{
$sql.=" and ".$table.".`usertype`='".$usertype."'";	
}	
$sql.=" order by FIELD (".$ptable.".`cat`,'NOTEBOOKS (DB TEX)','SCHOOL UNIFORM (PRASHANTHI)','SCHOOL BELT (DB TEX)','SCHOOL SOCKS (DB TEX)','SCHOOL ID CARD','BIG SHOPPER','CRAYONS','SCHOOL EMBLEM','TEXTBOOKS','TEXTBOOKHSS','OTHER')";	
		$sel_invoice=$dbobject->select($sql);
		$i=0;
		while($invoice_row=$dbobject->fetch_array($sel_invoice))
		{
			$data[$i]['sid']=$invoice_row['sid'];
			$data[$i]['studentid']=$invoice_row['studentid'];
			$data[$i]['transaction_id']=$invoice_row['transaction_id'];
			$data[$i]['qty']=$invoice_row['qty'];
			$data[$i]['price']=$invoice_row['price'];
			$data[$i]['productid']=$invoice_row['productid'];
			$data[$i]['total_amount']=$invoice_row['total_amount'];
			$data[$i]['paid_amount']=$invoice_row['paid_amount'];
			$data[$i]['salesdate']=$invoice_row['salesdate'];
			$data[$i]['store_type']=$invoice_row['store_type'];
			$data[$i]['discount']=$invoice_row['discount'];
			$data[$i]['gst_amt']=$invoice_row['gst_amt'];
			$data[$i]['store2_productid']=$invoice_row['store2_productid'];
		 $i++;
		}
		return $data;
	}	
function store_table($store_type)
{
  $store_tables_By_type=$this->store_tables_By_type($store_type);
  $table=$store_tables_By_type['table'];
  $ptable=$store_tables_By_type['ptable'];
return 	$table;
}	
function sales_det($store_type,$transaction_id)
{
	$dbobject = new DB();
	$dbobject->getCon();
	$table=$this->store_table($store_type);
	//echo "select * from ".$table." where `transaction_id`='".$transaction_id."'";
	$sales_details=$dbobject->select("select * from ".$table." where `transaction_id`='".$transaction_id."'");
	$row=$dbobject->fetch_array($sales_details);
	return $row;
}	
function sales_transaction_det($store_type,$transaction_id)
{
	$dbobject = new DB();
	$dbobject->getCon();
	$table=$this->store_table($store_type);
	$sales_details=$dbobject->select("select * from ".$table." where `transaction_id`='".$transaction_id."'");
	$i=0;
	while($row=$dbobject->fetch_array($sales_details))
	{
		$data[$i]['sid']=$row['sid'];
		$data[$i]['productid']=$row['productid'];
		$data[$i]['qty']=$row['qty'];
		$data[$i]['price']=$row['price'];
		$data[$i]['discount']=$row['discount'];
		$data[$i]['studentid']=$row['studentid'];
		$data[$i]['total_amount']=$row['total_amount'];
		$data[$i]['paid_amount']=$row['paid_amount'];
		$data[$i]['salesdate']=$row['salesdate'];
		$data[$i]['gst_amt']=$row['gst_amt'];
		$data[$i]['usertype']=$row['usertype'];
		$data[$i]['entry_by']=$row['entry_by'];
		$i++;
	}
	return $data;
}
function store_page($store_type)
{
	
	if($store_type=="st1")
	{
		$table['page']="store.php";
		$table['name']="store 1";
	}
	elseif($store_type=="st2")
	{
		$table['page']="store2.php";
		$table['name']="store 2";
	}
	elseif($store_type=="st3")
	{
		$table['page']="store3.php";
		$table['name']="store 3";
	}
return 	$table;
}
function productBYstoretype($store_type,$pid)
{
		$dbobject = new DB();
	$dbobject->getCon();
	if($store_type=="st1")
	{
   $prod_det=$dbobject->selectall("products",array("id"=>$pid));
	}
	elseif($store_type=="st2")
	{
   $prod_det=$dbobject->selectall("products2",array("id"=>$pid));
	}
	elseif($store_type=="st3")
	{
  $prod_det=$dbobject->selectall("products3",array("id"=>$pid));
	}
return $prod_det;	
}
function productstockBYstoretype($store_type,$pid,$acyear)
{
		$dbobject = new DB();
	$dbobject->getCon();
	if($store_type=="st1")
	{
   $stock_item=$dbobject->selectall("stock_item",array("product_id"=>$pid,"acyear"=>$acyear));
	}
	elseif($store_type=="st2")
	{
   $stock_item=$dbobject->selectall("stock_item2",array("product_id"=>$pid,"acyear"=>$acyear));
	}

return $stock_item;	
}
function productstock_updateBYstoretype($store_type,$pid,$acyear,$qty)
{
		$dbobject = new DB();
	$dbobject->getCon();
	if($store_type=="st1")
	{
     $chk_qry=$dbobject->select("select * from `stock_item` where `product_id`='".$pid."' and `acyear`='".$acyear."'");
	 $chk=$dbobject->mysql_num_row($chk_qry);
	 if($chk==0)
	 {
	  $stock_item=$dbobject->exe_qry("INSERT INTO `stock_item`( `product_id`, `qty`, `acyear`) VALUES ('".$pid."','".$qty."','".$acyear."')"); 
	 }
	 else
	 {
	 $stock_item=$dbobject->exe_qry("UPDATE `stock_item` SET `qty`='".$qty."' where `product_id`='".$pid."' and `acyear`='".$acyear."'");		 
	 }

	}
	elseif($store_type=="st2")
	{
     $chk_qry=$dbobject->select("select * from `stock_item2` where `product_id`='".$pid."' and `acyear`='".$acyear."'");
	 $chk=$dbobject->mysql_num_row($chk_qry);
	 if($chk==0)
	 {
	  $stock_item=$dbobject->exe_qry("INSERT INTO `stock_item2`( `product_id`, `qty`, `acyear`) VALUES ('".$pid."','".$qty."','".$acyear."')"); 
	 }
	 else
	 {
	 $stock_item=$dbobject->exe_qry("UPDATE `stock_item2` SET `qty`='".$qty."' where `product_id`='".$pid."' and `acyear`='".$acyear."'");		 
	 }
	}


}
function store_studcheck($store_type,$acyear,$sid)
{
		$dbobject = new DB();
	$dbobject->getCon();
	if($store_type=="st1")
	{
   $sql="SELECT * FROM `sales` WHERE `sid`='".$sid."'";
	}
	elseif($store_type=="st2")
	{
   $sql="SELECT * FROM `sales2` WHERE `sid`='".$sid."'";
	}
	elseif($store_type=="st3")
	{
   $sql="SELECT * FROM `sales3` WHERE `sid`='".$sid."'";
	}
	 $chk_qry=$dbobject->select($sql);
	 $chk=$dbobject->mysql_num_row($chk_qry);
	 return $chk;
}
function sales_ByStud($store_type,$sid,$acyear)
{
	$dbobject = new DB();
	$dbobject->getCon();
	$table=$this->store_table($store_type);
	$sql="SELECT * FROM ".$table." WHERE `sid`='".$sid."'";
	$chk_qry=$dbobject->select($sql);
	//$chk=$dbobject->mysql_num_row($chk_qry);
	 $i=0;
	 while($row=$dbobject->fetch_array($chk_qry))
	 {
		 //$transaction_id= $row['transaction_id'];
		  $data[$i]['id']=$row['id']; 		 
		  $data[$i]['sid']=$row['sid']; 		 
		  $data[$i]['studentid']=$row['studentid']; 		 
		  $data[$i]['transaction_id']=$row['transaction_id']; 		 
		  $data[$i]['productid']=$row['productid']; 		 
		  $data[$i]['qty']=$row['qty']; 	
		  $data[$i]['price']=$row['price']; 	
		  $data[$i]['discount']=$row['discount']; 	
		  $data[$i]['fine']=$row['fine']; 	
		  $data[$i]['total_amount']=$row['total_amount']; 	
		  $data[$i]['paid_amount']=$row['paid_amount']; 	
		  $data[$i]['salesdate']=$row['salesdate']; 	
		  $data[$i]['ref_number']=$row['ref_number']; 	
		  $data[$i]['pay_type']=$row['pay_type']; 	
		  $data[$i]['cheque_num']=$row['cheque_num']; 	
		  $data[$i]['cdate']=$row['cdate']; 	
		  $data[$i]['bank']=$row['bank']; 	
		  $data[$i]['store_type']=$row['store_type']; 	
          $i++;		  
     }
	 return $data;
}
function product_detBYstoretype($store_type,$pid)
{
	if($store_type=="st1")
	{
		$Product_Byid=$this->Product_Byid($pid);
	}
	if($store_type=="st2")
	{
		$Product_Byid=$this->Product_Byid2($pid);
	}
	if($store_type=="st3")
	{
		$Product_Byid=$this->Product_Byid3($pid);
	}	
	return $Product_Byid;
}
function sales_transaction_detBYId($store_type,$id)
{
	$dbobject = new DB();
	$dbobject->getCon();
	$table=$this->store_table($store_type);
	//echo "select * from ".$table." where `transaction_id`='".$transaction_id."'";
	$sales_details=$dbobject->select("select * from ".$table." where `id`='".$id."'");
	$i=0;
	while($row=$dbobject->fetch_array($sales_details))
	{
		$data[$i]['sid']=$row['sid'];
		$data[$i]['productid']=$row['productid'];
		$data[$i]['qty']=$row['qty'];
		$data[$i]['price']=$row['price'];
		$data[$i]['discount']=$row['discount'];
		$data[$i]['studentid']=$row['studentid'];
		$data[$i]['total_amount']=$row['total_amount'];
		$data[$i]['paid_amount']=$row['paid_amount'];
		$data[$i]['transaction_id']=$row['transaction_id'];
		$data[$i]['salesdate']=$row['salesdate'];
		$data[$i]['gst_amt']=$row['gst_amt'];
		$i++;
	}
	return $data;
}
	function salesreturn_datewise($date_from,$date_to,$store_type,$productid)
	{
		$dbobject = new DB();
		$dbobject->getCon();
        $date_from= date('Y-m-d', strtotime($date_from));
		$date_to= date('Y-m-d', strtotime($date_to));	
		$sql="SELECT * FROM `sales_return` WHERE `cancelled_date`>='".$date_from."' and `cancelled_date`<='".$date_to."' and `store_type`='".$store_type."'";
		if($productid!="-1")
		{
		$sql.=" and `productid`='".$productid."'";	
		}	
		$sel_invoice=$dbobject->select($sql);
		$i=0;
		while($invoice_row=$dbobject->fetch_array($sel_invoice))
		{
			$data[$i]['sid']=$invoice_row['sid'];
			$data[$i]['studentid']=$invoice_row['studentid'];
			$data[$i]['transaction_id']=$invoice_row['transaction_id'];
			$data[$i]['qty']=$invoice_row['qty'];
			$data[$i]['price']=$invoice_row['price'];
			$data[$i]['productid']=$invoice_row['productid'];
			$data[$i]['total_amount']=$invoice_row['total_amount'];
			$data[$i]['paid_amount']=$invoice_row['paid_amount'];
			$data[$i]['salesdate']=$invoice_row['salesdate'];
			$data[$i]['salesid']=$invoice_row['salesid'];
			$data[$i]['store_type']=$invoice_row['store_type'];
			$data[$i]['discount']=$invoice_row['discount'];
			$data[$i]['gst_amt']=$invoice_row['gst_amt'];
		 $i++;
		}
		return $data;
	}
function store_type_val()
{

	$store_type[0]="st1";

	$store_type[1]="st2";

	$store_type[2]="st3";

	 return $store_type;
}
function distinct_product_datawise($date_from,$date_to,$store_type,$productid)
{
	$dbobject = new DB();
	$dbobject->getCon();	
           $date_from= date('Y-m-d', strtotime($date_from));
		    $date_to= date('Y-m-d', strtotime($date_to)); 		
			$store_tables_By_type=$this->store_tables_By_type($store_type);
			$table=$store_tables_By_type['table'];
			$ptable=$store_tables_By_type['ptable'];		
	        
			$sql=" select distinct ".$ptable.".`id` from ".$ptable." left join  ".$table." on ".$table.".`productid`=".$ptable.".`id` WHERE ".$table.".`salesdate`>='".$date_from."' and ".$table.".`salesdate`<='".$date_to."' and ".$table.".`store_type`='".$store_type."'";
  			if($productid!="-1")
			{
			$sql.=" and ".$ptable.".`productid`='".$productid."'";	
			}
 			$sql.=" order by FIELD (".$ptable.".`cat`,'NOTEBOOKS (DB TEX)','SCHOOL UNIFORM (PRASHANTHI)','SCHOOL BELT (DB TEX)','SCHOOL SOCKS (DB TEX)','SCHOOL ID CARD','BIG SHOPPER','CRAYONS','SCHOOL EMBLEM','TEXTBOOKS','TEXTBOOKHSS','OTHER')";            			
			$sel_invoice=$dbobject->select($sql);
			$i=0;			
			while($row=$dbobject->fetch_array($sel_invoice))
			{
				$i++;
				$data[$i]['id']=$row['id'];
				$data[$i]['store_type']=$store_type;
				
			}
			return $data;
}
function store3_qty()
{
	$store_details[1][0]=166;
	$store_details[1][1]=91;
	$store_details[1][2]=51;
	$store_details[1][3]=53;
	$store_details[1][4]=172;	
	$store_details[2][0]=165;	
	$store_details[3][0]=52;	
	$store_details[4][0]=54;
	$store_details[4][1]=55;	
	$store_details[5][0]=52;	
	$store_details[6][0]=52;	
	$store_details[7][0]=91;
	$store_details[7][1]=165;
	$store_details[7][2]=166;	
	$store_details[8][0]=52;
	$store_details[9][0]=52;
	return $store_details;
}
function store2_3($date_from,$date_to,$store_type,$productid,$sid,$store3_pid)
{
	$dbobject = new DB();
	$dbobject->getCon();
   $date_from= date('Y-m-d', strtotime($date_from));
		    $date_to= date('Y-m-d', strtotime($date_to)); 		
			$store_tables_By_type=$this->store_tables_By_type($store_type);
			$table=$store_tables_By_type['table'];
			$ptable=$store_tables_By_type['ptable'];		
$sql="SELECT * FROM ".$table."  WHERE ".$table.".`salesdate`>='".$date_from."' and ".$table.".`salesdate`<='".$date_to."' and ".$table.".`store_type`='".$store_type."'  and ".$table.".`sid`='".$sid."'";
if($store3_pid=="1")
{
$sql.=" and ".$table.".`productid` IN ('166','91','51','53','172')";	
}
elseif($store3_pid=="2")
{
$sql.=" and ".$table.".`productid` IN ('165','52')";	
}
elseif($store3_pid=="3")
{
$sql.=" and ".$table.".`productid` IN ('52')";	
}
elseif($store3_pid=="4")
{
$sql.=" and ".$table.".`productid` IN ('54','55','51')";	
}
elseif($store3_pid=="5")
{
$sql.=" and ".$table.".`productid` IN ('52')";	
}
elseif($store3_pid=="6")
{
$sql.=" and ".$table.".`productid` IN ('52')";	
}
elseif($store3_pid=="7")
{
$sql.=" and ".$table.".`productid` IN ('91','165','166')";	
}
elseif($store3_pid=="8")
{
$sql.=" and ".$table.".`productid` IN ('52')";	
}
elseif($store3_pid=="9")
{
$sql.=" and ".$table.".`productid` IN ('52')";	
}
else
{
//$sql.=" and ".$table.".`productid`='".$productid."'";	
}

//echo $sql;
		$sel_invoice=$dbobject->select($sql);
		$i=0;
		while($invoice_row=$dbobject->fetch_array($sel_invoice))
		{
			$data=$data+$invoice_row['qty'];

		 $i++;
		}
		return $data;
	
}
function All_productBYstoretype($store_type)
{
	if($store_type=="st1")
	{
		$Product_data=$this->get_allProducts();
	}
	if($store_type=="st2")
	{
		$Product_data=$this->get_allProducts2();
	}
	if($store_type=="st3")
	{
		$Product_data=$this->get_allProducts3();
	}	
	return $Product_data;
}
function purchase_retun_action($pid,$store_type,$acyear,$cerdit_no,$supplier_name,$qty,$remark,$entry_by,$cerdit_date,$entry_date)
{
	$dbobject = new DB();
	$dbobject->getCon();
	$insert=$dbobject->exe_qry("INSERT INTO `store_purchase_return`( `cerdit_date`, `qty`, `entry_date`, `entry_by`, `pid`, `store_type`, `supplier_name`, `cerdit_no`, `remark`,`acyear`) VALUES ('".$cerdit_date."','".$qty."','".$entry_date."','".$entry_by."','".$pid."','".$store_type."','".$supplier_name."','".$cerdit_no."','".$remark."','".$acyear."')");
	$stock_upt=$this->update_stock($pid,$store_type,$acyear,$qty);
}
function update_stock($pid,$store_type,$acyear,$qty)
{
	$dbobject = new DB();
	$dbobject->getCon();
	$productstockBYstoretype=$this->productstockBYstoretype($store_type,$pid,$acyear);
	$current_stock=$productstockBYstoretype['qty'];
	//echo $qty;
	$stock=$current_stock-$qty;
	if($store_type=="st1")
	{
		//echo "UPDATE `stock_item` SET `qty`='".$stock."' WHERE `product_id`='".$pid."'";
	$updt=$dbobject->exe_qry("UPDATE `stock_item` SET `qty`='".$stock."' WHERE `product_id`='".$pid."'");	
	}
	elseif($store_type=="st2")
	{
	$updt=$dbobject->exe_qry("UPDATE `stock_item2` SET `qty`='".$stock."' WHERE `product_id`='".$pid."'");	
	}	
	
}
function store_database($store_type)
{
	if($store_type=="st1")
	{
		$Product_data['product']='products';
		$Product_data['sale']='sales';
		$Product_data['stock_item']='stock_item';
		$Product_data['store']='Store';
	}
	if($store_type=="st2")
	{
		$Product_data['product']='products2';
		$Product_data['sale']='sales2';		
		$Product_data['stock_item']='stock_item2';	
		$Product_data['store']='Store 2';
	}
	if($store_type=="st3")
	{
		$Product_data['product']='products3';
		$Product_data['sale']='sales3';		
		$Product_data['store']='Store 3';
	}	
	return $Product_data;
}
function product_Bycatogery_subcatogery($store_type,$category,$sub_category)
{
	$dbobject = new DB();
	$dbobject->getCon();
    $store_database=$this->store_database($store_type);	
	$sql="SELECT * FROM `".$store_database['product']."` WHERE `cat`='".$category."'";
	if($sub_category!="" && $sub_category!="-1")
	{
		$sql.=" and `sub_cat`='".$sub_category."'";
	}
	$qry=$dbobject->select($sql);
	$i=0;
	while($row=$dbobject->fetch_array($qry))
	{
		$data[$i]['id']=$row['id'];
		$data[$i]['pname']=$row['pname'];
		$data[$i]['sale_price']=$row['sale_price'];
		$data[$i]['manufacture']=$row['manufacture'];
		$data[$i]['cat']=$row['cat'];
		$data[$i]['sub_cat']=$row['sub_cat'];
		$i++;
	}
	return $data;
}
function get_distinct_catogery_byStore($store_type)
{
	$dbobject = new DB();
	$dbobject->getCon();
	$store_database=$this->store_database($store_type);
	$sql="SELECT distinct `cat` FROM `".$store_database['product']."` WHERE `cat` NOT IN ('','-1')";
	$qry=$dbobject->select($sql);
	while($row=$dbobject->fetch_array($qry))
	{
		$data[$row['cat']]=$row['cat'];
	}
	return $data;	
}
function get_distinct_subcatogery_byStore($store_type,$catogery)
{
	$dbobject = new DB();
	$dbobject->getCon();
	$store_database=$this->store_database($store_type);
	$sql="SELECT distinct `sub_cat` FROM `".$store_database['product']."` WHERE `cat`  IN ('".$catogery."') and `sub_cat` NOT IN ('','-1')";
	$qry=$dbobject->select($sql);
	while($row=$dbobject->fetch_array($qry))
	{
		$data[$row['sub_cat']]=$row['sub_cat'];
	}
	return $data;	
}
function get_acyear()
{
	$dbobject = new DB();
	$dbobject->getCon();
	$acyear=$dbobject->get_acyear();
	return  $acyear;
}
function get_product_order($product,$store_type)
{
    if($product=='School Hand Book')
	{ $order=2;	}
	elseif($product=='Time Management Record')
	{ $order=3;	}
	elseif($product=='School Magazine')
	{ $order=4;	}	
    elseif($product=='Text Books')
	{	$order=1; } 	
    elseif($product=='Vacation Diary 1')
    { $order=5;	}
    elseif($product=='Vacation Diary 2')
    { $order=6;	}  
    elseif($product=='Vacation Diary 3')
    { $order=7;	} 
    elseif($product=='Vacation Diary 4')
    { $order=8;	} 
    elseif($product=='Vacation Diary 5')
    { $order=9;	} 
    elseif($product=='Vacation Diary 6')
    { $order=10;	} 
    elseif($product=='Vacation Diary 7')
    { $order=11;	} 
    elseif($product=='Vacation Diary 8')
    { $order=12;	} 
    elseif($product=='Vacation Diary 9')
    { $order=13;	} 
    else
    { $order=0;	} 	
 return $order;
}
    function store_cancellecd_invoice_Datewise($date_from,$date_to,$store_type)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		
		$date_from= date('Y-m-d', strtotime($date_from));
		$date_to= date('Y-m-d', strtotime($date_to));
		//echo "SELECT * FROM `store_purchase_details` WHERE `date`>='".$date_from."' and `date`<='".$date_to."'";
		$i=0;
		//echo "SELECT * FROM `store_purchase_return` WHERE `cerdit_date`>='".$date_from."' and `cerdit_date`<='".$date_to."' and `store_type`='".$store_type."'";
		$sel=$dbobject->select("SELECT * FROM `sales_cancelled` WHERE `salesdate`>='".$date_from."' and `salesdate`<='".$date_to."' and `store_type`='".$store_type."'");
		while($row=$dbobject->fetch_array($sel))
		{
			$store[$i]['id']=$row['id'];
			$store[$i]['sid']=$row['sid'];
			$store[$i]['studentid']=$row['studentid'];
			$store[$i]['transaction_id']=$row['transaction_id'];
			$store[$i]['productid']=$row['productid'];
			$store[$i]['store_type']=$row['store_type'];
			$store[$i]['qty']=$row['qty'];			
			$store[$i]['price']=$row['price'];			
			$store[$i]['discount']=$row['discount'];			
			$store[$i]['fine']=$row['fine'];			
			$store[$i]['total_amount']=$row['total_amount'];			
			$store[$i]['paid_amount']=$row['paid_amount'];			
			$store[$i]['salesdate']=$row['salesdate'];			
			$store[$i]['ref_number']=$row['ref_number'];			
			$store[$i]['pay_type']=$row['pay_type'];			
			$store[$i]['cheque_num']=$row['cheque_num'];			
			$store[$i]['cdate']=$row['cdate'];			
			$store[$i]['bank']=$row['bank'];			
			$store[$i]['gst_amt']=$row['gst_amt'];			
			$store[$i]['cancelled_date']=$row['cancelled_date'];			
			$store[$i]['userid']=$row['userid'];			
			$store[$i]['reason']=$row['reason'];			
			$store[$i]['usertype']=$row['usertype'];			
			$store[$i]['entry_by']=$row['entry_by'];			
			$store[$i]['acyear']=$row['acyear'];			
					
			$i++;
		}
		return $store;
	}
	function sales_datewise_By_usertype_batch($date_from,$date_to,$store_type,$productid,$usertype,$batch)
	{
		$dbobject = new DB();
		$dbobject->getCon();
           $date_from= date('Y-m-d', strtotime($date_from));
		    $date_to= date('Y-m-d', strtotime($date_to)); 		
			$store_tables_By_type=$this->store_tables_By_type($store_type);
			$table=$store_tables_By_type['table'];
			$ptable=$store_tables_By_type['ptable'];			
	
$sql="SELECT * FROM ".$table." left join ".$ptable." on ".$ptable.".`id`=".$table.".`productid` WHERE ".$table.".`salesdate`>='".$date_from."' and ".$table.".`salesdate`<='".$date_to."' and ".$table.".`store_type`='".$store_type."'";
if($productid!="-1")
{
$sql.=" and ".$ptable.".`id`='".$productid."'";	
}
if($usertype!="" && $usertype!="-1")
{
$sql.=" and ".$table.".`usertype`='".$usertype."'";	
}
if($store_type=="st2")
{
 $sql.=" and ".$table.".`batch`='".$batch."'";   
}
	
$sql.=" order by FIELD (".$ptable.".`cat`,'NOTEBOOKS (DB TEX)','SCHOOL UNIFORM (PRASHANTHI)','SCHOOL BELT (DB TEX)','SCHOOL SOCKS (DB TEX)','SCHOOL ID CARD','BIG SHOPPER','CRAYONS','SCHOOL EMBLEM','TEXTBOOKS','TEXTBOOKHSS','OTHER')";	

		$sel_invoice=$dbobject->select($sql);
		$i=0;
		while($invoice_row=$dbobject->fetch_array($sel_invoice))
		{
			$data[$i]['sid']=$invoice_row['sid'];
			$data[$i]['studentid']=$invoice_row['studentid'];
			$data[$i]['transaction_id']=$invoice_row['transaction_id'];
			$data[$i]['qty']=$invoice_row['qty'];
			$data[$i]['price']=$invoice_row['price'];
			$data[$i]['productid']=$invoice_row['productid'];
			$data[$i]['total_amount']=$invoice_row['total_amount'];
			$data[$i]['paid_amount']=$invoice_row['paid_amount'];
			$data[$i]['salesdate']=$invoice_row['salesdate'];
			$data[$i]['store_type']=$invoice_row['store_type'];
			$data[$i]['discount']=$invoice_row['discount'];
			$data[$i]['gst_amt']=$invoice_row['gst_amt'];
			$data[$i]['store2_productid']=$invoice_row['store2_productid'];
		 $i++;
		}
		return $data;
	}
function store_tables_By_type($store_type)
{
	    if($store_type=="st1")
			{
				$table="`sales`";
				$ptable="`products`";
			}
			elseif($store_type=="st2")
			{
				$table="`sales2`";
				$ptable="`products2`";
			}
			elseif($store_type=="st3")
			{
				$table="`sales3`";
				$ptable="`products3`";
			}	
			$data['table']=$table;
			$data['ptable']=$ptable;
			return $data;
}
function store_tranx_det($store_type,$transaction_id)
{
		$dbobject = new DB();
		$dbobject->getCon();
			$store_tables_By_type=$this->store_tables_By_type($store_type);
			$table=$store_tables_By_type['table'];
			$ptable=$store_tables_By_type['ptable'];
			$qry=$dbobject->select("SELECT ".$table.".`salesdate`,".$table.".`transaction_id`,".$table.".`sid`,".$table.".`paid_amount` FROM  ".$table." WHERE `transaction_id`='".$transaction_id."'");
			while($row=$dbobject->fetch_array($qry))
			{
							$pay_date=date('d-m-Y', strtotime($row['salesdate']));
							$inv_det=explode("/", $row['transaction_id']);		
							$transaction_id=$row['transaction_id'];	
							$sid=$row['sid'];	
							$amount[$transaction_id][$pay_date][$sid]=$amount[$transaction_id][$pay_date][$sid]+$row['paid_amount'];			

			}
			return $amount;
}	
function invoice_by_datewise_store_type($store_type,$date_from,$date_to)
{
		$dbobject = new DB();
		$dbobject->getCon();
        $store_tables_By_type=$this->store_tables_By_type($store_type);
		$table=$store_tables_By_type['table'];
        $date_from= date('Y-m-d', strtotime($date_from));
        $date_to= date('Y-m-d', strtotime($date_to));			
        $qry="select distinct `transaction_id` from ".$table." where  `salesdate`>='".$date_from."' and `salesdate`<='".$date_to."'";
	    $qry1.=$qry; 
        $qry1.=" ORDER BY ".$table.".`id` "; 
		$sql=$dbobject->select($qry1);
	   while($row =  $dbobject->fetch_array($sql))
	   {
		$transaction_id=$row['transaction_id'];
		$transaction_id_array[$transaction_id]=$transaction_id;
	  }		
				//print_R($transaction_id_array);
				$i=0;
				if(!empty($transaction_id_array))
				{
					foreach($transaction_id_array as $transaction_id)
					{
					$amount=$this->store_tranx_det($store_type,$transaction_id);
					 	 foreach($amount as $trsid=>$pay_det)
				           {
							   $inv_det=explode("/", $trsid);
							   $invoice_temp=$inv_det[1]."/".$inv_det[0];	
							   foreach($pay_det as $paydate=>$studet)
							   {
								   foreach($studet as $stuid=>$tamount)
								    {
										$student_qry=$dbobject->select("select `student`.`sname`,`student`.`studentid` from `student` where `sid`='".$stuid."'");
									    $student_row=$dbobject->fetch_array($student_qry);
										$name=$student_row['sname'];
										$studentid=$student_row['studentid'];
                                        $data[$i]['name']=$name;										
                                        $data[$i]['userid']=$studentid;										
                                        $data[$i]['invoice_temp']=$invoice_temp;										
                                        $data[$i]['transaction_id']=$transaction_id;										
                                        $data[$i]['salesdate']=$paydate;										
                                        $data[$i]['tamount']=$tamount;	
                                        $i++;										
									}
							   }
						   }
					}						
				}
			
    return  $data;   		
}
}
?>