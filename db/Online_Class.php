<?php
include_once('../../db/createdb.php');
include_once('../../db/learn_module_class.php');
$dbobject = new DB();
if (!$dbobject->getCon())
{
echo "Connection Failed";
die(mysql_error());
}
else
{
    $Learn = new Learn();
    $acyear=$_GET['acyear'];
    $type=$_GET['type'];
    $subid=$_GET['subid'];
    $classno=$_GET['classno'];
    $module_data=$Learn->module_dataByClassno($subid,$classno,$type,$acyear);
    $classes=$dbobject->get_DivisionByAcyear($classno,$acyear);
}
?>
<div class="col-sm-12 class-w-box-resources">
                <ul class="nav nav-tabs custom-nav-tabs last-btn-list" style="margin-bottom: 15px;margin-top: 0px;">
                  <li class="active"><a data-toggle="tab" href="#resourcesclass1">Recorded class</a></li>
                  <li><a data-toggle="tab" href="#resourcesclass2">Teams</a></li>
                  <li><a data-toggle="tab" href="#resourcesclass2">Zoom</a></li>
                  <li style="float: right;"><a data-toggle="tab" href="#resourcesclass4" style="border: none;">Add Class</a></li>
                </ul>
            <div class="tab-content">
                <div id="resourcesclass1" class="tab-pane fade in active">
                    <div class="l-m-s-right-body">
                        <ul class="list-group-sortable-handles">
                            <li class="list-group-item">
                                <div class="body">
                                    <ul class="body-list">
                                        <?php
                                        if(!empty($module_data)){
                                            foreach($module_data as $md){
                                                echo "<li>";
                                                echo "<a class='answer_box_resources_btn' href='#' onClick='ShowResourceContent(".$md['id'].");'>".$md['title']."</a>
                                                <span class='d-t' style='margin-right: 15px;'><b style='margin-right: 5px;'>Date:</b>".$md['date']." | ".$md['start_time']."</span>
                                                <div class='answer_box_resources' style='margin-top: 10px;' id='Resource_".$md['id']."'>
                                              
                                                </div>";
                                               
                                                echo "</li>";
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
              <div id="resourcesclass2" class="tab-pane fade">
                  <div class="l-m-s-right-body">
                        <ul class="list-group-sortable-handles">
                            <li class="list-group-item">
                                <div class="body">
                                    <ul class="body-list">
                                        <li>
                                            <a class="answer_box_resources_btn" href="#">Class One.</a>
                                            <span class="d-t" style="margin-right: 15px;"><b style="margin-right: 5px;">Date:</b>10-11-2021 | 10:00am</span>
                                            
                                            <div class="answer_box_resources" style="display: none;margin-top: 10px;">
                                                <div class="row l-r-10">
                                                    <div class="col-sm-2">
                                                        <div class="class-room-list" style="margin: 0px;">
                                          <div class="btn-group" role="group" style="margin-bottom: 10px;">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                             Class1, A (3)
                                              <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                              <li><a href="#"><input type="checkbox" checked> Student1</a></li>
                                              <li><a href="#"><input type="checkbox" checked> Student2</a></li>
                                              <li><a href="#"><input type="checkbox" checked> Student3</a></li>
                                              <li><a href="#"><input type="checkbox"> Student4</a></li>
                                              <li><a href="#"><input type="checkbox"> Student5</a></li>
                                              <li><a href="#" class="add-std-note">Add New</a></li>
                                            </ul>
                                          </div>
                                          <div class="btn-group" role="group" style="margin-bottom: 10px;">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                              Class2, B (30)
                                              <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                              
                                            </ul>
                                          </div>
                                          <div class="btn-group" role="group" style="margin-bottom: 10px;">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                             Class3, C (30)
                                              <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                              
                                            </ul>
                                          </div>
                                    </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <form class="design-form-custom">
                                                    <div class="answer-box">
                                                        <iframe height="315" src="https://www.youtube.com/embed/ezbJwaLmOeM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                    </div>
                                                    <div>
                                                        <a href="#" class="btn download-btn" style="border: 1px solid;float: right;"><i class="fa fa-download" aria-hidden="true"></i> Download Resource</a>
                                                    </div>
                                                </form>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="inline-radio" style="margin-bottom: 10px;">
                                                            <label class="switch">
                                                                <input type="checkbox" id="enable-disable-new1">
                                                                <div class="slider round"></div>
                                                            </label>
                                                        </div>
                                                        <form class="design-form-custom">
                                                            <div class="row l-r-10">
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>Class</label>
                                                                        <select class="form-control">
                                                                            <option></option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>Homeroom / Division</label>
                                                                        <select class="form-control">
                                                                            <option></option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row l-r-10">
                                                                <div class="col-sm-12">
                                                                     <div class="form-group">
                                                                        <label>Student</label>
                                                                        <select class="form-control">
                                                                            <option></option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="btn_group">
                                                                <button class="btn btn-success Resources-assign-btn1">Assign</button>
                                                                <button class="btn btn-danger Resources-cancel-btn1" style="display:none;">Cancel</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <a href="#">Class Two.</a>
                                            <span class="d-t" style="margin-right: 15px;"><b style="margin-right: 5px;">Date:</b>10-11-2021 | 10:00am</span>
                                        </li>
                                        <li>
                                            <a href="#">Class Three.</a>
                                            <span class="d-t" style="margin-right: 15px;"><b style="margin-right: 5px;">Date:</b>10-11-2021 | 10:00am</span>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
              </div>
              <div id="resourcesclass4" class="tab-pane fade">
                  <div class="row l-r-10">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form class="design-form-custom" name='CreateClass' id='CreateClass' class='design-form-custom' action='CreateClassAction.php' enctype="multipart/form-data" method="POST">
                                      <input type='hidden' id='type' name='type' value='<?php echo $type;?>'/>
                                      <input type='hidden'  name='acyear' value='<?php echo $acyear;?>'/>
                                      <input type='hidden' id='subid' name='subid' value='<?php echo $subid;?>'/>
                                      <input type='hidden' id='classno' name='classno' value='<?php echo $classno;?>'/>
                                    <div class="row l-r-10">
                                    <div class="form-group">
                                        <label>Select Student</label>
                                        
                                        <?php
                                        if(!empty($classes)){
                                            foreach($classes as $cl){
                                                $student_list=$dbobject->get_studentdetByClassAndAcyear($cl['classid'],$acyear);
                                                echo "<div class='btn-group create-btn-group' role='group' style='margin-right: 5px;'><button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                                                ".$cl['division']." (".count($student_list).")
                                                <span class='caret'></span>
                                              </button>
                                              <ul class='dropdown-menu'>
                                              <li><a href='#'><label><input type='checkbox' id='check-All' checked> Select All</label></a></li>";
                                              if(!empty($student_list)){
                                                foreach($student_list as $list){
                                                    echo "
                                                <li><a href='#'><label style='margin: 0px;'><input type='checkbox' class='indi-check-box' name='sid[]' value='".$list['sid']."' checked>".$list['sname']." ".$list['s_sur_name']."</label></a></li>";
                                                }
                                              }
                                              echo "</ul></div>";
                                            }
                                        }
                                        ?>
                                        
                                    </div>
       
                                        </div>
                                    <div class="row l-r-10">
                   
             
                                            <div class="form-group">
                                                <label>Title</label>
                                                <input type="text" class="form-control" id='class_topic' name='class_topic' required>
                                            </div>
                                    </div>
                                    <div class="row l-r-10">
             
                                        <div class="form-group">
                                            <label>Class URL</label>
                                            <input type="text" class="form-control" id='class_url' name='class_url' required>
                                        </div>
                                </div>
                                    <div class="btn_group">
                                        <button type='submit' class="btn btn-success">Create</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
                        </div>
                        <script>
    $(".create-btn-group ul").click(function(e){
       e.stopPropagation();
    });
    $("#check-All").click(function(){
    $('input.indi-check-box:checkbox').not(this).prop('checked', this.checked);
});
ClassicEditor.create( document.querySelector( '#editor-create-resource' ), {
		//	toolbar: [ 'heading', '|', 'bold', 'italic', 'link' ]
		} )
		.then( editor => {
			window.editor = editor;
		} )
		.catch( err => {
			console.error( err.stack );
		} );
$("#resource-file").fileinput({
        uploadUrl: "learning/UploadFile.php",
        enableResumableUpload: true,
        resumableUploadOptions: {
           // uncomment below if you wish to test the file for previous partial uploaded chunks
           // to the server and resume uploads from that point afterwards
           // testUrl: "http://localhost/test-upload.php"
        },
        uploadExtraData: {
            'uploadToken': 'SOME-TOKEN', // for access control / security 
        },
        maxFileCount: 5,
        allowedFileTypes: ['image','pdf'],    // allow only images
        showCancel: true,
        initialPreviewAsData: true,
        overwriteInitial: false,
        // initialPreview: [],          // if you have previously uploaded preview files
        // initialPreviewConfig: [],    // if you have previously uploaded preview files
        theme: 'fas',
        deleteUrl: "http://localhost/file-delete.php"
    }).on('fileuploaded', function(event, previewId, index, fileId) {
        alert("adssad");
        console.log('File Uploaded', 'ID: ' + fileId + ', Thumb ID: ' + previewId);
    }).on('fileuploaderror', function(event, data, msg) {
        alert("Error");
        console.log('File Upload Error', 'ID: ' + data.fileId + ', Thumb ID: ' + data.previewId);
    }).on('filebatchuploadcomplete', function(event, preview, config, tags, extraData) {
        console.log('File Batch Uploaded', preview, config, tags, extraData);
    });
$('#CreateClass').on('submit', function (e) {
    var type=$("#type").val();
    var subid=$("#subid").val();
    var acyear=$("#acyear").val();
    var classno=$("#classno").val();
    e.preventDefault();
    $.ajax({
            type: 'POST',
            url: '../admin/learning/CreateClassAction.php',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            dataType:'json',
            success: function (response) {
                LoadData(type,subid,classno);
            }
          });
});
function ShowResourceContent(val){
    var acyear=$("#acyear").val();
    var classno=$("#classno").val();
    if ($("#rc_"+val).length > 0) {
     $("#Resource_"+val).html('');
    }else{
    $.ajax({
            type: 'GET',
            url: '../admin/learning/ShowResourceContent.php',
            data: {recid:val,classno:classno,acyear:acyear},
            dataType:'json',
            success: function (response) {
                $("#Resource_"+val).html(response.html);
            }
          });
    }
}
</script>