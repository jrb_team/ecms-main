<?php
include_once('../db/createdb.php');
include_once('../db/admin_class.php');
class Subject
{
	public function __construct($subject)
	{
		$this->subject=$subject;
		
	}
	function sub_table()
	{
		
		$subject=$this->subject;
		if($subject=="MORAL SCIENCE")
		{
			$sub="MSC";
		}
		elseif($subject=="ENGLISH")
		{
			$sub="ENG";
		}
		elseif($subject=="ENGLISH GRAMMAR")
		{
			$sub="ENG GR";
		}
		elseif($subject=="MALAYALAM")
		{
			$sub="MAL";
		}
		elseif($subject=="MALAYALAM I")
		{
			$sub="MAL I";
		}
		elseif($subject=="MALAYALAM II")
		{
			$sub="MAL II";
		}		
		elseif($subject=="ENVIRONMENTAL SCIENCE")
		{
			$sub="EVS";
		}
		elseif($subject=="COMPUTER" || $subject=="Computer Appln.")
		{
			$sub="COM";
		}
		elseif($subject=="GK" || $subject=="GENERAL KNOWLEDGE")
		{
			$sub="GK";
		}
		elseif($subject=="ENG GRAMMAR")
		{
			$sub="ENG GR";
		}		
		elseif($subject=="HINDI")
		{
			$sub="HIN";
		}
		elseif($subject=="SOCIAL SCIENCE" || $subject=="S.Studies")
		{
			$sub="SS";
		}
		elseif($subject=="MATHS" ||$subject=="MATHEMATICS")
		{
			$sub="MATH";
		}
		elseif($subject=="SCIENCE" || $subject=="Science")
		{
			$sub="SCI";
		}
		elseif($subject=="PHYSICS")
		{
			$sub="PHY";
		}
		elseif($subject=="CHEMISTRY")
		{
			$sub="CHE";
		}
		elseif($subject=="BIOLOGY")
		{
			$sub="BIO";
		}
		elseif($subject=="EVS")
		{
			$sub="EVS";
		}		
		elseif($subject=="IT")
		{
			$sub="IT";
		}

		elseif($subject=="BASIC SCIENCE")
		{
			$sub="BS";
		}
		elseif($subject=="ART")
		{
			$sub="ART";
		}
		elseif($subject=="WORK EXPERIENCE")
		{
			$sub="WE";
		}
		elseif($subject=="HEALTH & PHYSICAL EDUCATION" || $subject=="HEALTH & PE" ||$subject=="HEALTH & P E d" || $subject=="HEALTH & PHY.EDN")
		{
			$sub="HP";
		}
		elseif($subject=="PSYCHOLOGY")
		{
			$sub="PSY";
		}
		elseif($subject=="ZOOLOGY")
		{
			$sub="ZOO";
		}
		elseif($subject=="English Lab")
		{
			$sub="ENG.L";
		}
		elseif($subject=="History" || $subject=="History & Civics")
		{
			$sub="HIS";
		}
		elseif($subject=="BOTANY")
		{
			$sub="BOT";
		}
		elseif($subject=="Physics Lab")
		{
			$sub="PHY.L";
		}
		elseif($subject=="Chemistry Lab")
		{
			$sub="CHE.L";
		}		
		elseif($subject=="Botany Lab")
		{
			$sub="BOT.L";
		}
		elseif($subject=="Zoology Lab")
		{
			$sub="ZOO.L";
		}
		elseif($subject=="Sahithya Samajam")
		{
			$sub="SSM";
		}
		elseif($subject=="ART EDUCATION")
		{
			$sub="ART.ED";
		}		
		elseif($subject=="SUPW")
		{
			$sub="SUPW";
		}
                elseif($subject=="Psycho Edu")
                {
                       $sub="Psycho Edu"; 
                }
				elseif($subject=="ARABIC")
				{
					$sub="ARB"; 
				}
				elseif($subject=="I.S")
				{
					$sub="IS"; 
				}
				elseif($subject=="M.E")
				{
					$sub="ME"; 
				}
				else
				{
				    $sub=$subject; 
				}

		
		return $sub;
    }
    
    function mainSubject_classno($classno){
        $dbobj = new DB();
		$dbobj->getCon();
        $admin=new Admin($classno);
       $subject_table=$admin->subject_table();
       $sel=$dbobj->select("SELECT * FROM `".$subject_table."`");
       $i=0;
       while($row=$dbobj->fetch_array($sel)){
           $subject[$i]['sub_id']=$row['sub_id'];
           $subject[$i]['subject']=$row['subject'];
           $subject[$i]['sub_head']=$row['sub_head'];
           $subject[$i]['input_type']=$row['input_type'];
        $i++;
       }
       return $subject;
    }
    function optionalSubject_classno($classno){
        $dbobj = new DB();
        $dbobj->getCon();
        $i=0;
        $sel=$dbobj->select("SELECT * FROM opn_subjects where classno='".$classno."'");
        while($row=$dbobj->fetch_array($sel)){
            $subject[$i]['sub_id']=$row['id'];
            $subject[$i]['subject']=$row['opn_sub'];
            $i++;
        }
        return $subject;
    }
}
?>