<?php
include_once('createdb.php');
include_once('admin_class.php');
include_once('academic_class.php');
class Attendance
{
	public function __construct()
	{

	}
function month_list()
	{
  
		$month[0]="January";
		$month[1]="February";
		$month[2]="March";
		$month[3]="April";
		$month[4]="May";
		$month[5]="June";
		$month[6]="July";
		$month[7]="August";
		$month[8]="September";
		$month[9]="October";
		$month[10]="November";
		$month[11]="December";
	return $month;
	} 
	// Add All students to the attendence table
	function getAttendancePeriod($classno){
		$dbobject = new DB();
		$dbobject->getCon();
		$schoolinfo=$dbobject->selectall("schoolinfo",array("id"=>1));
		$data=array();
		if(!empty($schoolinfo)){
			if($schoolinfo['attendance_period']=="AM"){
				$data[0]="AM";
			}else if($schoolinfo['attendance_period']=="PM"){
				$data[0]="AM";
			}else if($schoolinfo['attendance_period']=="AM_PM"){
				$data[0]="AM";
				$data[1]="PM";
			}
			else if($schoolinfo['attendance_period']=="subject_wise"){
					$admin = new Admin($classno);
					$academic = new Academic();
					$tablename=$admin->subject_table();
					$subjects=$academic->get_subject($tablename);
					if(!empty($subjects)){
						$i=0;
						foreach($subjects as $s){
							$data[$i]=$s['subject'];
							$i++;
						}
					}

					
			}
		}
		return $data;
	}
	function Create_Attendence($classid,$acyear,$dat,$type,$taken_date,$taken_by,$time)
	{
	$dbobject = new DB();
	$dbobject->getCon();
	$attendance=array();
	if($type=="student")
	{
	$students=$dbobject->get_studentdetByClassAndAcyear($classid,$acyear);
	$i=0;
	$sl=1;
	if(!empty($students))
	{
		foreach($students as $row)
		{
			$sql=$dbobject->select("select * from `attendence` where `studentid`='".$row['studentid']."' and `classid`='".$classid."' and `date`='".$dat."' and `acyear`='".$acyear."' and `time_interval`='".$time."'");
			$att_row=$dbobject->fetch_array($sql);
			$attendance[$i]['sl']=$sl;
			$attendance[$i]['sid']=$row['sid'];
			$attendance[$i]['studentid']=$row['studentid'];
			$attendance[$i]['sname']=$row['s_sur_name']." ".$row['sname'];
			$attendance[$i]['time_interval']=$time;
			$attendance[$i]['leave']=$this->checkLeaveStudent($row['sid'],$row['dob']);
			if($att_row['status']==""){
			    if($attendance[$i]['leave']!=0){
			        $attendance[$i]['status']="Absent";  
			    }
			    else{
			        $attendance[$i]['status']="Present";     
			    }
			}
			else{
			    $attendance[$i]['status']=$att_row['status'];   
			}
			if($att_row['type']==""){
			  	 $attendance[$i]['type']="S";  
			}
			else{
			    $attendance[$i]['type']=$att_row['type'];   
			}
			$attendance[$i]['gen_remark']=$att_row['gen_remark'];
			$attendance[$i]['parent_remark']=$att_row['parent_remark'];
			$attendance[$i]['bday']=$this->checkBday($row['dob']);
			$sl++;
			$i++;
		}
	}
	}
	else
	{
	$qry="insert into teacher_attendence (`tid`,`date`,`status`,`place`,`time`,`taken_by`,`acyear`) VALUES ('".$row['id']."','".$dat."','Present','".$place."','".$taken_date."','".$taken_by."','".$acyear."')";
	mysql_query($qry) or die(mysql_error());
	}
	$qry="";
	return $attendance;
	}
	//-------------------------------------------------------------------------------
	function checkBday($dob){
	    if(date('m-d') == substr($dob,5,5)){
	        return 1;
	    }
	    else{
	        return 0;
	    }
	}
	function CheckAttendance($classid,$date,$acyear,$time){
	    $dbobject = new DB();
	    $dbobject->getCon();
	    //echo "select * from `attendence` where `classid`='".$classid."' and `date`='".$date."' and `acyear`='".$acyear."' and `time_interval`='".$time."'";
	    $sql=$dbobject->select("select * from `attendence` where `classid`='".$classid."' and `date`='".$date."' and `acyear`='".$acyear."' and `time_interval`='".$time."'");
	    $num_rows=$dbobject->mysql_num_row($sql);
	    return $num_rows;
	}
	function CheckAttendanceStatus($classid,$date,$acyear,$time){
	    $dbobject = new DB();
	    $dbobject->getCon();
	    //echo "select * from `attendence` where `classid`='".$classid."' and `date`='".$date."' and `acyear`='".$acyear."' and `time_interval`='".$time."'";
	    $sql=$dbobject->select("select * from `attendence` where `classid`='".$classid."' and `date`='".$date."' and `acyear`='".$acyear."' and `time_interval`='".$time."' and `submit_status`='1'");
	    $num_rows=$dbobject->mysql_num_row($sql);
	    return $num_rows;
	}
function checkLeaveStudent($sid,$date){
     $dbobject = new DB();
	 $dbobject->getCon();
     $check_leave=$dbobject->select("select * from leave_request where sid='".$sid."' and `approve_status`='1' and '".$date."' between from_date and to_date");
     $num=$dbobject->mysql_num_row($check_leave);
     if($num>0){
         $row=$dbobject->fetch_array($check_leave);
         return $row['id'];
     }
     else{
         return 0;
     }
}
function ShowTable($classid,$date)
{
	$dbobject = new DB();
	$dbobject->getCon();
	$qry="select * from attendence where classid='".$classid."' and date = '".$date."'" ;
	$rslt=mysql_query($qry);
	$i=1;
	$tab = $tab."<table class='table table-striped table-bordered bootstrap-datatable'><tr><th>Roll NO</th><th>ID</th><th>Name</th><th>Leave Application</th><th>Status</th><th>Remark</th></tr>";
	while($row=mysql_fetch_array($rslt))
	{
		$id=$row['id'];
		$stud_id=$row['studentid'];
		$sql007 = mysql_query("select sname,s_status from student where studentid = '".$stud_id."'");
		$row007 = mysql_fetch_array($sql007);
		$stud_name = $row007['sname'];
		if($row007['s_status']=='active')
		{
		$tab=$tab."<tr><td>".$i."</td><td>".$row['studentid']."</td><td>".$stud_name."</td><td>";
		$check_leave=mysql_query("select * from leave_appl where usertype='student' and userid='".$stud_id."'");
		while($leave_row=mysql_fetch_array($check_leave))
		{
			$fromdate=date($leave_row['fromdate']);
			if($leave_row['tod']=="0000-00-00")
			{
				$todate=$fromdate;
			}
			else
			{
				$todate=date($leave_row['todate']);
			}
			if($fromdate<=$date)
			{
			  if($todate>=$date)
			   { 
					$var="<a href=\"javascript:void(0)\" onClick=\"popup('".$row['studentid']."','".$row['date']."');\"><font color=red>Leave Applied</font></a>";
			   }
			   else
			   {
					$var="-";
			   }
			}
		}
		$id=$row[0];
		$tab=$tab.$var."</td><td><select id='att".$id."' onChange=Update(this.value,".$id.",1,rem".$id.".value)><option Value=Present>Present</option><option value=Absent>Absent</option><option value=Halfday>Halfday</option><option value=Late>Late</option><option value=Leave>Leave</option><option value=OnDuty>OnDuty</option></select></td><td><textarea  id='rem".$id."' onkeyUp='Update(att".$id.".value,".$id.",1,this.value)'>".$row['remark']."</textarea></td></tr>";
		$var="-";
		}
		$i++;
	}
	$tab = $tab."</table>";
return $tab;
}
//----------------------------
function ShowTableforEdit($classid,$date)
{
	$dbobject = new DB();
	$dbobject->getCon();
 $sel_student=mysql_query("select * from attendence where classid='".$classid."' and date = '".$date."'");
 //echo "select * from attendence where classid='".$classid."' and date = '".$date."' and `place`='".$place."'";
 $i=1;
 $tab=$tab."<table class='table table-striped table-bordered '><tr><th>Roll NO</th><th>ID</th><th>Name</th><th>Status</th><th>Remark</th><th></th></tr>";
 while($stud_row=mysql_fetch_array($sel_student))
	{
		$id=$stud_row['id'];
		$stud_id=$stud_row['studentid'];
		$sql007 = mysql_query("select sname,s_status from student where studentid = '".$stud_id."'");
		$row007 = mysql_fetch_array($sql007);
		if($row007['s_status']=='active')
		{
		$stud_name = $row007['sname'];
		$tab=$tab."<tr><td>".$i."</td><td>".$stud_id."</td><td>".$stud_name."</td>";

		$status=$stud_row['status'];
		$check_leave=mysql_query("select * from leave_appl where usertype='student' and userid='".$stud_id."'");
		while($leave_row=mysql_fetch_array($check_leave))
		{
			$fromdate=date($leave_row['fromdate']);
			if($leave_row['tod']=="0000-00-00")
			{
				$todate=$fromdate;
			}
			else
			{
				$todate=date($leave_row['todate']);
			}
			if($fromdate<=$date)
			{
			  if($todate>=$date)
			   { 
					$var="<a href=\"javascript:void(0)\" onClick=\"popup('".$row['studentid']."','".date('d-m-Y', strtotime($row['date']))."');\"><font color=red>Leave Applied</font></a>";
			   }
			   else
			   {
					$var="-";
			   }
			}
		}
		$tab=$tab."<td><select name='".$id."' id='att".$id."' onChange=Update(this.value,".$id.",1,rem".$id.".value)><option Value='".$status."'>".$status."</option><option Value='Present'>Present</option><option value='Absent'>Absent</option><option value='Halfday'>Halfday</option><option value=Late>Late</option><option value='Leave'>Leave</option><option value='OnDuty'>OnDuty</option><option value='LeaveDuringClass'>Leave During School Hours</option></select></td><td ><textarea  id='rem".$id."' name='".$id."' onkeyUp='Update(att".$id.".value,".$id.",1,this.value)'>".$stud_row['remark']."</textarea></td><td><a href='#openModal1' class='btn btn-small btn-primary' type='button' name='send_sms' `name`='".$id."' onClick='send_alert_ind(this.name)' >Send Sms</a></td>";
		$var="-";
		}
		$i++;
	}
	 $tab=$tab."</table>";
	return $tab;

}
//----------------------------
function ShowTableTeacher($date,$place)
{
	$dbobject = new DB();
	$dbobject->getCon();

	$qry="select * from teacher_attendence where date = '".$date."'" ;
	$rslt=mysql_query($qry);
	$tab = $tab."<table class='table table-striped table-bordered'><tr><th>ID</th><th>Name</th><th>Leave Application</th><th>Status</th></tr>";
	while($row=mysql_fetch_array($rslt))
	{
		$tid=$row['tid'];
		$sql007 = mysql_query("select name,lname from teacher where id = '".$tid."'");
		$row007 = mysql_fetch_array($sql007);
		$t_name = $row007['name']." ".$row007['lname'];
		$tab=$tab."<tr><td>".$row['tid']."</td><td>".$t_name."</td><td>";
		$check_leave=mysql_query("select * from leave_appl where usertype='teacher' and userid='".$tid."'");
		while($leave_row=mysql_fetch_array($check_leave))
		{
			$fromdate=date($leave_row['fromdate']);
			if($leave_row['tod']=="0000-00-00")
			{
				$todate=$fromdate;
			}
			else
			{
				$todate=date($leave_row['todate']);
			}
			if($fromdate<=$date)
			{
			  if($todate>=$date)
			   { 
					$var="<a href=\"javascript:void(0)\" onClick=\"popup('".$row['studentid']."','".$row['date']."');\"><font color=red>Leave Applied</font></a>";
			   }
			   else
			   {
					$var="-";
			   }
			}
		}
	$tab=$tab."</td><td><select onChange=Update(this.value,".$row[0].",2)><option value='".$row['status']."'>".$row['status']."</option><option Value=Present>Present</option><option value=Absent>Absent</option><option value=Halfday>Halfday</option><option value=Late>Late</option><option value=Leave>Leave</option><option value=OnDuty>On Duty</option><option value=ldc>Leave During School Hours</option></select>";
		$var="-";
	}
	$tab = $tab."</table>";
return $tab;
}
//------------------------------------------------------------------------------------------------
function ShowTableforEdit_teacher($date,$place)
{
	$dbobject = new DB();
	$dbobject->getCon();
 $tab=$tab."<table class='table table-striped table-bordered'><tr><th>ID</th><th>Name</th><th>Date</th><th>Status</th><th>Remark</th></tr>";
 $sel_teacher=mysql_query("select * from teacher_attendence where date = '".$date."' and `place`='".$place."'");
 $i=1;
 while($teacher_row=mysql_fetch_array($sel_teacher))
	{
		$tid=$teacher_row['tid'];
		
		$sql007 = mysql_query("select name,lname from teacher where id = '".$tid."'");
		$row007 = mysql_fetch_array($sql007);
		$t_name = $row007['name']." ".$row007['lname'];
		$tab=$tab."<tr><td>".$i."</td><td>".$t_name."</td><td>".$date."</td>";

		$tab=$tab."<td><select onChange=Update(this.value,".$teacher_row[0].",2)><option Value='".$teacher_row['status']."'>".$teacher_row['status']."</option><option Value=Present>Present</option><option value=Absent>Absent</option><option value=Halfday>Halfday</option><option value=Late>Late</option><option value=Leave>Leave</option><option value=OnDuty>On Duty</option></select></td>";
		$check_leave=mysql_query("select * from leave_appl where usertype='teacher' and userid='".$tid."'");
		while($leave_row=mysql_fetch_array($check_leave))
		{
			$fromdate=date($leave_row['fromdate']);
			if($leave_row['tod']=="0000-00-00")
			{
				$todate=$fromdate;
			}
			else
			{
				$todate=date($leave_row['todate']);
			}
			if($fromdate<=$date)
			{
			  if($todate>=$date)
			   { 
					$var="<a href=\"javascript:void(0)\" onClick=\"popup('".$row['teacherid']."','".$row['date']."');\"><font color=red>Leave Applied</font></a>";
			   }
			   else
			   {
					$var="-";
			   }
			}
		}
		$tab=$tab."<td>".$var."</td></tr>";
		$var="-";
		$i++;
	}
	 $tab=$tab."</table>";
	return $tab;
}
//----------------------------------------------------------------------------------------------
	function getTotalAttendace($date,$status)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$sql=$dbobject->select("SELECT count(*) as total from `attendence` where `status`='".$status."' and `date`='".$date."'");
		$data=$dbobject->fetch_array($sql);
		return $data['total'];
	}
	function getClassAttendaceBydate($date,$classid,$status)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$sql=$dbobject->select("SELECT count(*) as total from `attendence` where `status`='".$status."' and `classid`='".$classid."' and `date`='".$date."'");
		$data=$dbobject->fetch_array($sql);
		return $data['total'];
	}
	function getClassAttendaceBydateByPeriod($date,$classid,$status,$period)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$sql=$dbobject->select("SELECT count(*) as total from `attendence` where `status`='".$status."' and `classid`='".$classid."' and `date`='".$date."' and `time_interval`='".$period."'");
		$data=$dbobject->fetch_array($sql);
		return $data['total'];
	}
	function getAttendace($acyear,$classid,$date,$status)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$qry="select t1.*,t2.sname,sclass.classname,sclass.division from `attendence` as t1 left join `student` as t2 on `t2`.`studentid`=`t1`.`studentid` left join `sclass` on `t2`.`classid`=`sclass`.`classid` where `t2`.`acyear`='".$acyear."' and `t2`.`classid`='".$classid."' and `t1`.`status`='".$status."'";
		if($date!="")
		{
			$qry.=" and `t1`.`date`='".$date."'";
		}
		$qry.=" order by `t2`.`sname`";
		//echo $qry;
		$sql=$dbobject->select($qry);
		$i=0;
		while($row=$dbobject->fetch_array($sql))
		{
			$data[$i]['studentid']=$row['studentid'];
			$data[$i]['sname']=$row['sname'];
			$data[$i]['classname']=$row['classname'];
			$data[$i]['division']=$row['division'];
			$data[$i]['status']=$row['status'];
			$data[$i]['time']=$row['time'];
			$data[$i]['taken_by']=$row['taken_by'];
			$data[$i]['remark']=$row['remark'];
			$i++;
		}
		return $data;
	}
	function getAttendaceAll($acyear,$place,$classid,$date)
	{		
	$dbobject = new DB();
	$dbobject->getCon();
	$qry="select t1.*,t2.sname,sclass.classname,sclass.division  from `attendence` as t1  left join `student` as t2 on t2.`studentid`=t1.`studentid` left join `sclass` on t2.`classid`=`sclass`.`classid` where t2.`place`='".$place."'
		          and t2.`acyear`='".$acyear."' and t2.`classid`='".$classid."'";
		if($date!="")
		{
			$qry.=" and t1.`date`='".$date."'";
		}
		$qry.=" order by t2.`sname`";
		$sql=$dbobject->select($qry);
		//$num=mysql_num_rows($sql);
		$i=0;
		while($row=$dbobject->fetch_array($sql))
		{
			$data[$i]['studentid']=$row['studentid'];
			$data[$i]['classname']=$row['classname'];
			$data[$i]['division']=$row['division'];
			$data[$i]['sname']=$row['sname'];
			$data[$i]['status']=$row['status'];
			$data[$i]['time']=$row['time'];
			$data[$i]['taken_by']=$row['taken_by'];
			$data[$i]['remark']=$row['remark'];
			$i++;
		}
		return $data;
	}
	function getAttendace_teacher($place,$date,$status)
	{
			$dbobject = new DB();
			$dbobject->getCon();
		 $qry="select t1.*,t2.name,t2.lname  from `teacher_attendence` as t1  left join `teacher` as t2 on t2.`id`=t1.`tid`  where t2.`place`='".$place."' and `date`='".$date."' and t1.`status`='".$status."' order by t2.`name`";
		 $sql=$dbobject->select($qry);
		 $i=0;
		 while($row=$dbobject->fetch_array($sql))
			{
				$data[$i]['id']=$row['id'];
				$data[$i]['name']=$row['name'];
				$data[$i]['lname']=$row['lname'];
				$data[$i]['sname']=$row['sname'];
				$data[$i]['status']=$row['status'];
				$data[$i]['time']=$row['time'];
				$data[$i]['taken_by']=$row['taken_by'];
				$data[$i]['remark']=$row['remark'];
				$i++;
			}
			return $data;
	}
		function getTotalStudentAttendace($classid,$studentid,$acyear,$status)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$sql=$dbobject->select("SELECT DISTINCT `date` from `attendence` where `status`='".$status."' and `classid`='".$classid."' and `studentid`='".$studentid."' and `acyear`='".$acyear."'");
		//
		return $dbobject->mysql_num_row($sql);
	}
function getAttendace_status($acyear,$classid,$date)
{		
	$dbobject = new DB();
	$dbobject->getCon();
		$qry="select distinct `date` from `attendence` where  `classid`='".$classid."'  and `date`='".$date."'";
		$sql=$dbobject->select($qry);
		$num=$dbobject->mysql_num_row($sql);
		return $num;
}
function get_attendance_indStaff($staffid,$acyear,$status)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$re=array();
		$staff_det=$dbobject->selectall("teacher",array("userid"=>$staffid));
		$sql=$dbobject->select("SELECT  * from `teacher_attendence` where `status`='".$status."'  and `tid`='".$staff_det['id']."' and `acyear`='".$acyear."'");
		$i=0;
		while($data=$dbobject->fetch_array($sql))
		{	
			$re[$i]['date']=$data['date'];
			$re[$i]['remark']=$data['remark'];
			$i++;
		}
		return $re;
	}
function ShowAttendance($classid,$date,$acyear)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$sql=$dbobject->select("select * from `attendence` where `classid`='".$classid."' and `date`='".$date."' and `acyear`='".$acyear."'");
		$i=0;
		while($row=$dbobject->fetch_array($sql))
		{
			$student_det=$dbobject->selectall("student",array("studentid"=>$row['studentid']));
			$attendance[$i]['sid']=$student_det['sid'];
			$attendance[$i]['studentid']=$student_det['studentid'];
			$attendance[$i]['sname']=$student_det['sname'];
			$attendance[$i]['status']=$row['status'];
			$attendance[$i]['remark']=$row['remark'];
			$i++;
		}
		return $attendance;
	}	
		function get_attendance_ind($classid,$studentid,$acyear,$status)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$sql=$dbobject->select("SELECT  * from `attendence` where `status`='".$status."' and `classid`='".$classid."' and `studentid`='".$studentid."' and `acyear`='".$acyear."'");
		$i=0;
		while($data=$dbobject->fetch_array($sql))
		{
			
			$re[$i]['date']=date('d-m-Y',strtotime($data['date']));
			$re[$i]['remark']=$data['remark'];
			$i++;
		}
		return $re;
		
	}
	function get_attendance_indWithTimeIntervall($classid,$studentid,$acyear,$status,$time_interval)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$sql=$dbobject->select("SELECT  * from `attendence` where `status`='".$status."' and `classid`='".$classid."' and `studentid`='".$studentid."' and `acyear`='".$acyear."' and `time_interval`='".$time_interval."'");
		$i=0;
		while($data=$dbobject->fetch_array($sql))
		{
			
			$re[$i]['date']=date('d-m-Y',strtotime($data['date']));
			$re[$i]['remark']=$data['remark'];
			$i++;
		}
		return $re;
		
	}
	function get_attendance_Details($classid,$studentid,$acyear,$status)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$sql=$dbobject->select("SELECT  distinct `date` from `attendence` where `status`='".$status."' and `classid`='".$classid."' and `studentid`='".$studentid."' and `acyear`='".$acyear."'");
		$i=0;
		while($data=$dbobject->fetch_array($sql))
		{
			$re[$i]['date']=date('d-m-Y',strtotime($data['date']));
			$morning_att_details=$dbobject->selectall("attendence",array("date"=>$data['date'],"time_interval"=>"morning","studentid"=>$studentid,"status"=>$status));
			$re[$i]['morning_id']=$morning_att_details['id'];
			$re[$i]['morning']=$morning_att_details['remark'];
			$re[$i]['morning_parent']=$morning_att_details['parent_remark'];
			$afternoon_att_details=$dbobject->selectall("attendence",array("date"=>$data['date'],"time_interval"=>"afternoon","studentid"=>$studentid,"status"=>$status));
			$re[$i]['afternoon_id']=$afternoon_att_details['id'];
			$re[$i]['afternoon']=$afternoon_att_details['remark'];
			$re[$i]['afternoon_parent']=$afternoon_att_details['parent_remark'];
			$i++;
		}
		return $re;
		
	}
function devicelog_data_by_userid($date,$year,$month,$UserId)
{
		$dbobject = new DB();
		$dbobject->getCon();
		 $date=date('Y-m-d',strtotime($date));
		 $teacher_det=$dbobject->selectall("teacher",array("userid"=>$UserId));
		    $qry="select * from `devicelogs` where    `LogDate`  LIKE '%".$date."%'   and `Userid`='".$teacher_det['deviceid']."' order by `id`";
			$sql=$dbobject->select($qry);
			while($row=$dbobject->fetch_array($sql))
			{	
				$Direction=$row['Direction'];
				$LogDate=$row['LogDate'];
				//$data[$LogDate][$Direction][]=$row['LogDate'];
				$LogDate=explode(" ",$row['LogDate']);
				$data[$Direction][]=$LogDate[1];				
				

			}			
			return $data;
}
	//-------------------------------------------------------------------------------
function ShowTable_new($classid,$date,$time_interval)
{
	$dbobject = new DB();
	$dbobject->getCon();
	$qry="select * from attendence where classid='".$classid."' and date = '".$date."' and `time_interval`='".$time_interval."'" ;
	$rslt=mysql_query($qry);
	$i=1;
	$tab = $tab."<table class='table table-striped table-bordered bootstrap-datatable'><tr><th>Roll NO</th><th>ID</th><th>Name</th><th>Leave Application</th><th>Status</th><th></th><th>Remark</th></tr>";
	while($row=mysql_fetch_array($rslt))
	{
		$id=$row['id'];
		$stud_id=$row['studentid'];
		$type=$row['type'];
		$sql007 = mysql_query("select sname,s_status from student where studentid = '".$stud_id."'");
		$row007 = mysql_fetch_array($sql007);
		$stud_name = $row007['sname'];
		if($row007['s_status']=='active')
		{
		$tab=$tab."<tr><td>".$i."</td><td>".$row['studentid']."</td><td>".$stud_name."</td><td>";
		$check_leave=mysql_query("select * from leave_appl where usertype='student' and userid='".$stud_id."'");
		while($leave_row=mysql_fetch_array($check_leave))
		{
			$fromdate=date($leave_row['fromdate']);
			if($leave_row['tod']=="0000-00-00")
			{
				$todate=$fromdate;
			}
			else
			{
				$todate=date($leave_row['todate']);
			}
			if($fromdate<=$date)
			{
			  if($todate>=$date)
			   { 
					$var="<a href=\"javascript:void(0)\" onClick=\"popup('".$row['studentid']."','".$row['date']."');\"><font color=red>Leave Applied</font></a>";
			   }
			   else
			   {
					$var="-";
			   }
			}
		}
		$id=$row[0];
		$tab=$tab.$var."</td><td><select id='att".$id."' onChange=Update(this.value,".$id.",1,rem".$id.".value)><option Value=Present>Present</option><option value=Absent>Absent</option><option value=Halfday>Halfday</option><option value=Late>Late</option><option value=Leave>Leave</option><option value=OnDuty>OnDuty</option></select></td><td>";
$tab=$tab."<input type='checkbox' id='".$id."*S' onclick='change_mode(this.id)'";
		if($type=="S")
		{
		$tab=$tab."checked";	
		}
		$tab=$tab."/>S&nbsp;&nbsp;";
		$tab=$tab."<input type='checkbox'  id='".$id."*H' onclick='change_mode(this.id)'";
		if($type=="H")
		{
		$tab=$tab."checked";	
		}
		$tab=$tab."/>H";
		$tab=$tab."</td><td><textarea  id='rem".$id."' onkeyUp='Update(att".$id.".value,".$id.",1,this.value)'>".$row['remark']."</textarea></td></tr>";
		$var="-";
		}
		$i++;
	}
	$tab = $tab."</table>";
return $tab;
}
//----------------------------
function ShowTableforEdit_new($classid,$date,$time_interval)
{
	$dbobject = new DB();
	$dbobject->getCon();
 $sel_student=mysql_query("select * from attendence where classid='".$classid."' and date = '".$date."' and `time_interval`='".$time_interval."'");
 //echo "select * from attendence where classid='".$classid."' and date = '".$date."' and `place`='".$place."'";
 $i=1;
 $tab=$tab."<table class='table table-striped table-bordered '><tr><th>Roll NO</th><th>ID</th><th>Name</th><th>Status</th><th></th><th>Remark</th><th></th></tr>";
 while($stud_row=mysql_fetch_array($sel_student))
	{
		$id=$stud_row['id'];
		$stud_id=$stud_row['studentid'];
		$type=$stud_row['type'];
		$sql007 = mysql_query("select sname,s_status from student where studentid = '".$stud_id."'");
		$row007 = mysql_fetch_array($sql007);
		if($row007['s_status']=='active')
		{
		$stud_name = $row007['sname'];
		$tab=$tab."<tr><td>".$i."</td><td>".$stud_id."</td><td>".$stud_name."</td>";

		$status=$stud_row['status'];
		$check_leave=mysql_query("select * from leave_appl where usertype='student' and userid='".$stud_id."'");
		while($leave_row=mysql_fetch_array($check_leave))
		{
			$fromdate=date($leave_row['fromdate']);
			if($leave_row['tod']=="0000-00-00")
			{
				$todate=$fromdate;
			}
			else
			{
				$todate=date($leave_row['todate']);
			}
			if($fromdate<=$date)
			{
			  if($todate>=$date)
			   { 
					$var="<a href=\"javascript:void(0)\" onClick=\"popup('".$row['studentid']."','".date('d-m-Y', strtotime($row['date']))."');\"><font color=red>Leave Applied</font></a>";
			   }
			   else
			   {
					$var="-";
			   }
			}
		}
		$tab=$tab."<td><select name='".$id."' id='att".$id."' onChange=Update(this.value,".$id.",1,rem".$id.".value)><option Value='".$status."'>".$status."</option><option Value='Present'>Present</option><option value='Absent'>Absent</option><option value='Halfday'>Halfday</option><option value=Late>Late</option><option value='Leave'>Leave</option><option value='OnDuty'>OnDuty</option><option value='LeaveDuringClass'>Leave During School Hours</option></select></td><td>";
$tab=$tab."<input type='checkbox' id='".$id."*S' onclick='change_mode(this.id)'";
		if($type=="S")
		{
		$tab=$tab."checked";	
		}
		$tab=$tab."/>S&nbsp;&nbsp;";
		$tab=$tab."<input type='checkbox'  id='".$id."*H' onclick='change_mode(this.id)'";
		if($type=="H")
		{
		$tab=$tab."checked";	
		}
		$tab=$tab."/>H";
		$tab=$tab."</td><td ><textarea  id='rem".$id."' name='".$id."' onkeyUp='Update(att".$id.".value,".$id.",1,this.value)'>".$stud_row['remark']."</textarea></td><td><a href='#openModal1' class='btn btn-small btn-primary' type='button' name='send_sms' `name`='".$id."' onClick='send_alert_ind(this.name)' >Send Sms</a></td>";
		$var="-";
		}
		$i++;
	}
	 $tab=$tab."</table>";
	return $tab;

}
	function Create_Attendence_new($classid,$acyear,$dat,$type,$taken_date,$taken_by,$time_interval)
	{
	$dbobject = new DB();
	$dbobject->getCon();
	if($type=="student")
	{
	$students=$dbobject->get_studentdetByClassAndAcyear($classid,$acyear);
	if(!empty($students))
	{
		foreach($students as $row)
		{
			$sql=$dbobject->select("select * from `attendence` where `studentid`='".$row['studentid']."' and `classid`='".$classid."' and `date`='".$dat."' and `acyear`='".$acyear."' and `time_interval`='".$time_interval."'");
			$num_rows=$dbobject->mysql_num_row($sql);
			if($num_rows==0){
			$qry="insert into attendence (`studentid`,`classid`,`date`,`status`,`time`,`taken_by`,`acyear`,`time_interval`,`type`) VALUES ('".$row['studentid']."','".$classid."','".$dat."','Present','".$taken_date."','".$taken_by."','".$acyear."','".$time_interval."','S')";
			mysql_query($qry) or die(mysql_error());
			}
			
		}
	}
	}
	else
	{
	$qry="insert into teacher_attendence (`tid`,`date`,`status`,`place`,`time`,`taken_by`,`acyear`) VALUES ('".$row['id']."','".$dat."','Present','".$place."','".$taken_date."','".$taken_by."','".$acyear."')";
	mysql_query($qry) or die(mysql_error());
	}
	$qry="";
	}
function working_days_attendance_table($val,$date_from,$date_to,$acyear)
{
	$dbobject = new DB();
	$dbobject->getCon();
    $sql="SELECT distinct `date` from `attendence` WHERE `acyear`='".$acyear."'";	
	if($val=="date")
	{
	$sql.=" and `date`='".$date_from."'";	
	}
	if($val=="date_range")
	{
	$sql.=" and `date`>='".$date_from."' and `date`<='".$date_to."'";	
	}
	if($val=="class"){
		$sql.=" and `classid`='".$date_from."'";
	}
    $qry=$dbobject->select($sql);
    $num=$dbobject->mysql_num_row($qry);	
	return $num;
}
function working_dates_attendance_table($val,$date_from,$date_to,$acyear)
{
	$dbobject = new DB();
	$dbobject->getCon();
	$dates=array();
	$i=0;
    $sql="SELECT distinct `date` from `attendence` WHERE `acyear`='".$acyear."'";	
	if($val=="date")
	{
	$sql.=" and `date`='".$date_from."'";	
	}
	if($val=="date_range")
	{
	$sql.=" and `date`>='".$date_from."' and `date`<='".$date_to."'";	
	}
	if($val=="class"){
		$sql.=" and `classid`='".$date_from."'";
	}
    $qry=$dbobject->select($sql);
	while($row=$dbobject->fetch_array($qry)){
		$dates[$i]=$row['date'];
		$i++;
	}
	return $dates;
}	
function working_days_attendance_table_classid($val,$date_from,$date_to,$acyear,$classid)
{
	$dbobject = new DB();
	$dbobject->getCon();
    $sql="SELECT distinct `date` from `attendence` WHERE `acyear`='".$acyear."'";	
	if($val=="date")
	{
	$sql.=" and `date`='".$date_from."'";	
	}
	if($val=="date_range")
	{
	$sql.=" and `date`>='".$date_from."' and `date`<='".$date_to."'";	
	}
		 $sql.=" and `classid`='".$classid."'";
    $qry=$dbobject->select($sql);
    $num=$dbobject->mysql_num_row($qry);	
	return $num;
}
function last_attendanceDetails($studentid){
    $dbobject = new DB();
	$dbobject->getCon();
    $sql=$dbobject->select("SELECT * from `attendence` WHERE `studentid`='".$studentid."' ORDER BY `id` DESC LIMIT 1");
    $row=$dbobject->fetch_array($sql);
    return $row;
}
function WorkingDays($acyear,$classid){
	return 0;
}
function SchoolDays(){
	$dbobject = new DB();
	$dbobject->getCon();
	$i=0;
	$data=array();
    $sql=$dbobject->select("SELECT `day` from `workingdays`");
    while($row=$dbobject->fetch_array($sql)){
		$data[$i]=$row['day'];
		$i++;
	}
    return $data;
}
function getAttendanceSummerybySid($studentid,$acyear,$period){
	$data['present']=0;
	$data['absent']=0;
	$data['late']=0;
    $dbobject = new DB();
	$dbobject->getCon();
    $sel=$dbobject->select("SELECT count(case when status ='Absent'  then 1 end) as absent_count,count(case when status IN ('Present','OnDuty') then 1 end) as present_count,count(case when status ='Late' then 1 end) as late_count FROM `attendence` WHERE `studentid`='".$studentid."' AND `acyear`='".$acyear."' AND `time_interval`='".$period."'");
    $row=$dbobject->fetch_array($sel);
	$data['present']=$row['present_count'];
	$data['absent']=$row['absent_count'];
	$data['late']=$row['late_count'];
	return $data;
}
function getAttendanceSummerybyClass($classid,$acyear,$period){
	$data['present']=0;
	$data['absent']=0;
	$data['late']=0;
    $dbobject = new DB();
	$dbobject->getCon();
    $sel=$dbobject->select("SELECT count(case when status ='Absent'  then 1 end) as absent_count,count(case when status IN ('Present','OnDuty') then 1 end) as present_count,count(case when status ='Late' then 1 end) as late_count FROM `attendence` WHERE `classid`='".$classid."' AND `acyear`='".$acyear."' AND `time_interval`='".$period."'");
    $row=$dbobject->fetch_array($sel);
	$data['present']=$row['present_count'];
	$data['absent']=$row['absent_count'];
	$data['late']=$row['late_count'];
	return $data;
}
function GetDatesByStatus($studentid,$status,$acyear,$period){
    $dbobject = new DB();
	$dbobject->getCon();
    $dates=array();
    $sql="SELECT `date`,`late_time` FROM `attendence` WHERE `acyear`='".$acyear."' AND `studentid`='".$studentid."' AND `time_interval`='".urldecode($period)."'";
    if($status=="Present"){
        $sql.=" AND `status` IN ('Present','onDuty')";
    }else{
        $sql.=" AND `status`='".$status."'";
    }
    $sql.="order by `date`";
    $sel=$dbobject->select($sql);
    $i=0;
    while($row=$dbobject->fetch_array($sel)){
        $dates[$i]['date']=$row['date'];
		$dates[$i]['late_time']=$row['late_time'];
        $i++;
    }
    return $dates;
}
function Get_AttendanceData($acyear,$seltype,$query_format,$period,$date_from,$date_to){
    $dbobject = new DB();
	$dbobject->getCon();
    $data=array();

    $data['Absent']=0;
    $data['Late']=0;
    $data['Present']=0;
	$data_date_array=array();
    $sql="SELECT `attendence`.`date`,`attendence`.`time_interval`,`attendence`.`status`,`attendence`.`type`,`student`.`sid`,`student`.`gender`,`student`.`sname`,`student`.`studentid`,`student`.`s_sur_name`,`student`.`dob`,`student`.`family_code`,`student`.`parent_id`,`student`.`classid`,`student`.`vsn`,`classname`,`division` FROM `attendence` LEFT JOIN `student` ON `attendence`.`studentid`=`student`.`studentid` INNER JOIN `sclass` ON `attendence`.`classid`=`sclass`.`classid`  WHERE `attendence`.`acyear`='".$acyear."'";
    if($seltype!="School" && $seltype!='School_wise'){
      $sql.="AND `attendence`.`classid` IN (".$query_format.")";
    }
    if($period=="Today"){
        $sql.="AND `attendence`.`date`='".$date_from."'";
    }elseif($period=="date"){
         $sql.="AND `attendence`.`date`='".$date_from."'";
    }
    else{
        $sql.="AND `attendence`.`date`>='".$date_from."' AND `attendence`.`date`<='".$date_to."'";
    }
    $qry=$dbobject->select($sql);
    while($row2=$dbobject->fetch_array($qry))
    {
        $gender=$row2['gender'];
        $time_interval=$row2['time_interval'];
		$sid=$row2['sid'];
		$date=$row2['date'];
		$data['data'][$sid]=strtoupper($row2['s_sur_name']);
		$data['data_other'][$sid]['sname']=strtoupper($row2['sname']);
		$data['data_other'][$sid]['studentid']=$row2['studentid'];
		$data['data_other'][$sid]['s_sur_name']=strtoupper($row2['s_sur_name']);
		$data['data_other'][$sid]['studentid']=$row2['studentid'];
		$data['data_other'][$sid]['parent_id']=$row2['parent_id'];
		$data['data_other'][$sid]['family_code']=strtoupper($row2['family_code']);
		$data['data_other'][$sid]['gender']=$row2['gender'];
		$data['data_other'][$sid]['classid']=$row2['classid'];
		$data['data_other'][$sid]['classname']=$row2['classname'];
		$data['data_other'][$sid]['division']=$row2['division'];
		$data['data_other'][$sid]['dob']=$row2['dob'];
		$data['data_other'][$sid]['vsn']=$row2['vsn'];
		$data['data_other'][$sid]['date']=$row2['date'];
		$data['data_other'][$sid][$date][$time_interval]['status']=$row2['status'];
		$data['data_other'][$sid][$date][$time_interval]['type']=$row2['type'];
		$data['data_date_array'][$date]=$date;
		if($row2['dob']!='0000-000-00'){
			$data_other[$sid]['dob'] = date('d-m-Y', strtotime($row2['dob']));
		  }
		  else{
			$data_other[$sid][$sid]['dob'] ="";
		  }
        //echo $row2['status'];
                      if($row2['status']=="Absent"  || $row2['status']=="Leave" || $row2['status']=="Leave")
                      {
                          $data['Absent']++;
                          if(!isset($data['Absent_M_time'][$gender][$time_interval])){
                              $data['Absent_M_time'][$gender][$time_interval]=1;
                          }else{
                              $data['Absent_M_time'][$gender][$time_interval]++;	
                          }
                                              
                      }
                      else if($row2['status']=="Late")
                      {
                          $data['Late']++;
                          if(!isset($data['Late_M_time'][$gender][$time_interval])){
                              $data['Late_M_time'][$gender][$time_interval]=1;
                          }else{
                              $data['Late_M_time'][$gender][$time_interval]++;
                          }			
                      }else{
                          $data['Present']++;
                          if(!isset($data['Present_M_time'][$gender][$time_interval])){
                              $data['Present_M_time'][$gender][$time_interval]=1;
                          }else{
                              $data['Present_M_time'][$gender][$time_interval]++;
                          }
                      }						
    }
    return $data;	
}
function getStudentSummery($studentid,$acyear){
	$dbobject = new DB();
	$dbobject->getCon();
	$sel=$dbobject->select("SELECT SUM(`attendence`.`status` = 'Present' || `attendence`.`status` = 'onDuty') AS `present_count`,SUM(`attendence`.`status` = 'Absent') AS `absent_count`,SUM(`attendence`.`status` = 'Late') AS `late_count` FROM `attendence` WHERE `studentid`='".$studentid."' AND `acyear`='".$acyear."'");
	$row=$dbobject->fetch_array($sel);
	return $row;
}
function getAttendanceTaken($date,$acyear,$time_interval){
    $dbobject = new DB();
	$dbobject->getCon();
    $ATTENDANCE_TAKEN_qry=$dbobject->select("SELECT distinct`classid` FROM `attendence` WHERE `date`='".$date."' and `acyear`='".$acyear."' and `time_interval`='".$time_interval."'");
    return $count_taken=$dbobject->mysql_num_row($ATTENDANCE_TAKEN_qry);
}
function getAttendanceNotTaken($date,$acyear,$time_interval){
    $dbobject = new DB();
	$dbobject->getCon();
    $ATTENDANCE_TAKEN_qry=$dbobject->select("SELECT distinct`classid` FROM `attendence` WHERE `date`='".$date."' and `acyear`='".$acyear."' and `time_interval`='".$time_interval."'");
    return $count_taken=$dbobject->mysql_num_row($ATTENDANCE_TAKEN_qry);
}
function getAttendanceNotTakenClasses($date,$acyear,$time_interval){
    $dbobject = new DB();
	$dbobject->getCon();
	$i=0;
	$classes=array();
    $ATTENDANCE_TAKEN_qry=$dbobject->select("SELECT distinct `attendence`.`classid`,`attendence`.`submit_status`,`attendence`.`submit_date`,`attendence`.`time`,`classname`,`division` FROM `attendence` LEFT JOIN `sclass` ON `attendence`.`classid`=`sclass`.`classid` WHERE `date`='".$date."' and `acyear`='".$acyear."' and `time_interval`='".$time_interval."' order by `classno`,`division`");
	while($row=	$dbobject->fetch_array($ATTENDANCE_TAKEN_qry)){
		$classes[$i]['classid']=$row['classid'];
		$classes[$i]['classname']=$row['classname'];
		$classes[$i]['division']=$row['division'];
		$classes[$i]['submit_status']=$row['submit_status'];
		$classes[$i]['submit_date']=$row['submit_date'];
		$classes[$i]['time']=$row['time'];
		$i++;
	}
	return $classes;
}
function getAttendanceNotTakenClassesRange($date,$acyear,$time_interval,$query_format){
    $dbobject = new DB();
	$dbobject->getCon();
	$i=0;
	$classes=array();
	$sql="SELECT distinct `attendence`.`classid`,`classname`,`division` FROM `attendence` LEFT JOIN `sclass` ON `attendence`.`classid`=`sclass`.`classid` WHERE `date`='".$date."' and `acyear`='".$acyear."' and `time_interval`='".$time_interval."'";
    if($query_format!=""){
		$sql.="AND `attendence`.`classid` in(".$query_format.")";
	}
	$ATTENDANCE_TAKEN_qry=$dbobject->select($sql);
	while($row=	$dbobject->fetch_array($ATTENDANCE_TAKEN_qry)){
		$classes[$i]['classid']=$row['classid'];	
		$classes[$i]['classname']=$row['classname'];
		$classes[$i]['division']=$row['division'];
		$i++;
	}
	return $classes;
}
function getAttendanceNotTakenClassesRangeDateDange($date_from,$date_to,$acyear,$query_format){
    $dbobject = new DB();
	$dbobject->getCon();
	$i=0;
	$classes=array();
	$sql="SELECT distinct `classid`,`date`,`time_interval` FROM `attendence`  WHERE `date`>='".$date_from."'  and `date`<='".$date_to."' and `acyear`='".$acyear."'";
    if($query_format!=""){
		$sql.="AND `attendence`.`classid` in(".$query_format.")";
	}
	$ATTENDANCE_TAKEN_qry=$dbobject->select($sql);
	while($row=	$dbobject->fetch_array($ATTENDANCE_TAKEN_qry)){
		$classes[$i]['classid']=$row['classid'];
		$classes[$i]['date']=$row['date'];
		$classes[$i]['time_interval']=$row['time_interval'];
		$i++;
	}
	return $classes;
}
function getAttendanceNotTakenClassesRangeDateDangenew($date_from,$date_to,$acyear,$str_classid,$attendancePeriod){
	$dbobject = new DB();
	$dbobject->getCon();
	$count=0;
	$dates=$this->working_dates_attendance_table("date_range",$date_from,$date_to,$acyear);
	if(!empty($dates)){
		foreach($dates as $d){
			if(!empty($attendancePeriod)){
				foreach($attendancePeriod as $ap){
					if(!empty($str_classid)){
						foreach($str_classid as $cl){
							//echo "SELECT `date` FROM `attendence` WHERE `classid`='".$cl."'";
							$sel=$dbobject->select("SELECT `date` FROM `attendence` WHERE `classid`='".$cl."' AND `time_interval`='".$ap."' AND `date`='".$d."' AND `acyear`='".$acyear."'");
							$num_rows=$dbobject->mysql_num_row($sel);
							if($num_rows==0){
								$count++;
							}
						}
					}
				}
			}
		}
	}
	return $count;
    	
}
function SchoolHolidays($params){
	$dbobject = new DB();
	$dbobject->getCon();
	$acyear=$dbobject->get_acyear();
	$i=0;
	$holidays=array();
	$sql=$dbobject->select("SELECT * FROM `calendar_events` WHERE `acyear`='".$acyear."' AND `even_holiday`='Holiday'");
	while($row=$dbobject->fetch_array($sql)){
		$dates=$dbobject->createDateRangeArray1($row['st_date'],$row['end_date']);
		if(!empty($dates)){
			foreach($dates as $d){
				$holidays[$i]=$d;
				$i++;
			}
		}
		
	}
	$sql=$dbobject->select("SELECT MIN(smonth) as `smonth`, MAX(emonth) as `emonth` FROM `terms` WHERE `acyear`='".$acyear."'");
	$row=$dbobject->fetch_array($sql);
	if($row['smonth']!='' && $row['emonth']!=""){
		$date_range=$dbobject->createDateRangeArray1($row['smonth'],$row['emonth']);
		if(!empty($date_range)){
			foreach($date_range as $d){
				$check_term=$dbobject->select("SELECT `id` FROM `terms` WHERE '".$d."' between `smonth` AND `emonth` AND `acyear`='".$acyear."'");
				$num_rows=$dbobject->mysql_num_row($check_term);
				if($num_rows==0){
					$holidays[$i]=$d;
					$i++;
				}
			}
		}
	}
	return $holidays;
}
function GetStudentInd($studentid){
	$dbobject = new DB();
	$dbobject->getCon();
	$acyear=$dbobject->get_acyear();
	$sql=$dbobject->select("select studentid,count(case when status ='Absent'  then 1 end) as absent_count,count(case when status ='Present' then 1 end) as present_count,count(case when status ='Late' then 1 end) as late_count,count(distinct date) as Tot_count from `attendence` where `studentid`='".$studentid."' AND `acyear`='".$acyear."'");
	$row=$dbobject->fetch_array($sql);
	$data[0]['colorByPoint']=true;
	$data[0]['data'][0]['name']="Present";
	$data[0]['data'][0]['y']=(float)$row['present_count'];
	$data[0]['data'][1]['name']="Late";
	$data[0]['data'][1]['y']=(float)$row['late_count'];
	$data[0]['data'][2]['name']="Absent";
	$data[0]['data'][2]['y']=(float)$row['absent_count'];
	return $data;
}
function GetStudentIndByStatus($studentid,$status){
	$dbobject = new DB();
	$dbobject->getCon();
	$acyear=$dbobject->get_acyear();
	$sql=$dbobject->select("select `studentid`,`date`,`time_interval`,`status` from `attendence` where `studentid`='".$studentid."' AND `acyear`='".$acyear."' and `status`='".$status."'");
	$data=array();
	$i=0;
	while($row=$dbobject->fetch_array($sql)){
		$data[$row['date']][$row['time_interval']]=$row['status'];
		$i++;
	}

	return $data;
}
function get_ipfcount($sid,$acyear,$date_from,$date_to){
	$dbobject = new DB();
	$dbobject->getCon();
	$sql=$dbobject->select("select count(id) as `count` from `ifp_attendance` where `sid`='".$sid."' AND `date`>='".$date_from."' AND `date`<='".$date_to."'");
	$row=$dbobject->fetch_array($sql);
	if($date_from==$date_to){
		$sql="SELECT `id`  FROM `leave_request` WHERE `sid`='".$sid."' AND '".$date_from."'  BETWEEN `from_date` AND `to_date`";
	}else{
		$sql="SELECT `id` FROM `leave_request` WHERE `sid`='".$sid."' AND `from_date`>='".$date_from."' AND `to_date`<='".$date_to."'";
	}
	$leaveApp=$dbobject->select($sql);
	$leave_count=0;
	while($row1=$dbobject->fetch_array($leaveApp)){
	$sel_not=$dbobject->select("SELECT `nid` FROM `notifications` WHERE `sid`='".$sid."' and `type_id`='".$row1['id']."' AND `date_issued`='".$date."'");
	$num_rows=$dbobject->mysql_num_row($sel_not);
	if($num_rows=="0"){
		$leave_count++;
	}
	}
	return $row['count']+$leave_count;
}
function getAbsentData_datewise($acyear,$first_second,$last_second,$classno)
{		
$dbobject = new DB();
$dbobject->getCon();
$qry="SELECT `attendence`.`id`,`attendence`.`status`,`attendence`.`time_interval`,`attendence`.`parent_remark`,`attendence`.`remark`,`attendence`.`remrecp`,`attendence`.`gen_remark`,`student`.`sid`,`student`.`studentid`,`student`.`parent_id`,`student`.`sname`,`student`.`gender`,`student`.`family_code`,`student`.`parent_id`,`student`.`s_sur_name`,`student`.`PREF_NAME`,`sclass`.`classname`,`sclass`.`division` FROM `attendence` LEFT JOIN `sclass` ON `attendence`.`classid` = `sclass`.`classid` INNER JOIN `student` on `attendence`.`studentid`=`student`.`studentid` WHERE `attendence`.`acyear`='".$acyear."' and `attendence`.`status` IN ('Absent','Leave') and  `attendence`.`date`>='".$first_second."' and `attendence`.`date`<='".$last_second."'";
if($classno!=""){
	$qry.=" AND `sclass`.`classno`='".$classno."'";
}
	$sql=$dbobject->select($qry);
	//$num=mysql_num_rows($sql);
	$i=0;
	while($row=$dbobject->fetch_array($sql))
	{
		$data[$i]['id']=$row['id'];
		$data[$i]['sid']=$row['sid'];
		$data[$i]['studentid']=$row['studentid'];
		$data[$i]['sname']=$row['sname'];
		$data[$i]['family_code']=$row['family_code'];
		$data[$i]['parent_id']=$row['parent_id'];
		$data[$i]['s_sur_name']=$row['s_sur_name'];
		$data[$i]['gender']=$row['gender'];
		$data[$i]['PREF_NAME']=$row['PREF_NAME'];
		$data[$i]['parent_remark']=$row['parent_remark'];
		$data[$i]['remark']=$row['remark'];
		$data[$i]['remrecp']=$row['remrecp'];
		$data[$i]['gen_remark']=$row['gen_remark'];
		$data[$i]['time_interval']=$row['time_interval'];
		$data[$i]['status']=$row['status'];
		$data[$i]['classname']=$row['classname'];
		$data[$i]['division']=$row['division'];
		$i++;
	}
	return $data;
}
function GetLeaveStudent($sid,$date){
    $dbobject = new DB();
    $dbobject->getCon();
    $data=array();
    $check_leave=$dbobject->select("select `from_date`,`to_date`,`reason`,`approve_status` from leave_request where sid='".$sid."' and `approve_status`!='2' and '".$date."' between from_date and to_date");
    $num=$dbobject->mysql_num_row($check_leave);
    if($num>0){
        $row=$dbobject->fetch_array($check_leave);
        $data['from_date']=$row['from_date'];
        $data['to_date']=$row['to_date'];
        $data['reason']=$row['reason'];
        $data['approve_status']=$row['approve_status'];
        $data['checknum']=$num;
    }
 return $data;
}
function student_leave_data($studentid,$parent_id,$dat)
{
    $dbobject = new DB();
	$dbobject->getCon();
$leave_qry=$dbobject->select("SELECT *  FROM leave_request WHERE   `from_id` ='".$parent_id."'");
$chk_lv=$dbobject->mysql_num_row($leave_qry);
if($chk_lv!=0)
{
	while($row=$row_leave=$dbobject->fetch_array($leave_qry))
		{
			$from_date=$row_leave['from_date'];
			$to_date=$row_leave['to_date'];
			$reason=$row_leave['reason'];
            $createDateRangeArray1=$dbobject->createDateRangeArray1($from_date,$to_date);
                 if(!empty($createDateRangeArray1))
					{
							foreach($createDateRangeArray1 as $ldate)
								{
									$leave_date_array[$studentid][$ldate]=$ldate;
								}
					}																				
		}
	if (in_array($dat, $leave_date_array[$studentid]))
	{
		$data['reason']=$reason;
		$data['from_date']=$from_date;
		$data['to_date']=$to_date;
	}
return $data;	
}
}
function get_attendanceNotification($userid,$sid,$date){
    $dbobject = new DB();
	$dbobject->getCon();
	$date=date('Y-m-d',strtotime($date));
	$sel=$dbobject->select("SELECT count(`nid`) as `not_count` FROM `notifications` WHERE `userid_to` ='".$userid."' and `sid`='".$sid."' and `type`='attendance' and `date_issued`='".$date."' and `read_status`='0'");
	$row=$dbobject->fetch_array($sel);
	$leave_count=0;
	$leaveApp=$dbobject->select("SELECT id  FROM `leave_request` WHERE `sid`='".$sid."' AND '".$date."' between `from_date` AND `to_date`");
	while($row1=$dbobject->fetch_array($leaveApp)){
		$sel_not=$dbobject->select("SELECT `nid` FROM `notifications` WHERE `sid`='".$sid."' and `type_id`='".$row1['id']."' AND `date_issued`='".$date."'");
		$num_rows=$dbobject->mysql_num_row($sel_not);
		if($num_rows=="0"){
			$leave_count++;
		}
	}
	return $row['not_count']+$leave_count;
	//return $row['not_count'];

}
function update_notifications_statusIFP($userid,$sid)
{
 $dbobj = new DB();
 $dbobj->getCon();	
 $current_date=date('Y-m-d');
 $time_read=date('h:i:sa');
 //echo "update `notifications` set `read_status`='1',`date_read`='".$current_date."',`time_read`='".$time_read."' where `userid_to`='".$userid."' AND `sid`='".$sid."'and `type`='attendance'";
$update=$dbobj->exe_qry("update `notifications` set `read_status`='1',`date_read`='".$current_date."',`time_read`='".$time_read."' where `userid_to`='".$userid."' AND `sid`='".$sid."'and `type`='attendance'");
return $update;
}
function SectionFordept($class){
$section=array();
if($class=="PREP " || $class=="GRADE 1" || $class=="GRADE 2" || $class=="GRADE 3" || $class=="GRADE 4" || $class=="GRADE 5" || $class=="GRADE 6"){
	$section['sectionid']=0;
	$section['section_name']="Primary";
}
elseif($class=="YEAR 7" || $class=="YEAR 8" || $class=="YEAR 9" || $class=="YEAR 10" || $class=="YEAR 11" || $class=="YEAR 12"){
	$section['sectionid']=1;
	$section['section_name']="Secondary";
}else{
	echo $class;
}
return $section;
}
function SectionFordeptAll(){
	$section=array();
	$section[0]['id']=0;
	$section[0]['section_name']="Primary";
	$section[1]['id']=1;
	$section[1]['section_name']="Secondary";
	return $section;
}
function getIFPdataBysid($sid,$date_from,$date_to,$at_date){
	$dbobject = new DB();
	$dbobject->getCon();
	$date_from=date('Y-m-d',strtotime($date_from));
	$date_to=date('Y-m-d',strtotime($date_to));
	$i=0;
	$data=array();
	$sel_ifp=$dbobject->select("SELECT * FROM `ifp_attendance` WHERE `sid`='".$sid."' AND `date`>='".$date_from."' AND `date`<='".$date_to."'");
	while($row=$dbobject->fetch_array($sel_ifp)){
		 if($row['user_type']!='parent'){
				$teacher_det=$dbobject->selectall("teacher",array("id"=>$row['userid']));
				$data[$i]['from']=strtoupper(substr($teacher_det['name'], 0, 1)).strtoupper(substr($teacher_det['lname'], 0, 1));
				$data[$i]['msg']=$row['msg'];
				$data[$i]['entry_date']=date('d-m-Y h:i:s a',strtotime($row['entry_date']));
			}else{
				$parent_det=$dbobject->selectall("parent",array("id"=>$row['userid']));
				$data[$i]['from']=strtoupper(substr($parent_det['f_given_name'], 0, 1)).strtoupper(substr($parent_det['f_sur_name'], 0, 1));
				$data[$i]['msg']=$row['msg'];	
				$data[$i]['entry_date']=date('d-m-Y h:i:s a',strtotime($row['entry_date']));
			}
			$i++;
		}
		$sel_leave=$dbobject->select("SELECT * FROM `leave_request` WHERE '".$at_date."' between `from_date` AND `to_date` AND `sid`='".$sid."'");																	
		while($leave_row=$dbobject->fetch_array($sel_leave)){
			$data[$i]['from']="PT";
			$data[$i]['msg']="Leave applied from ".date('d-m-Y',strtotime($leave_row['from_date']))." to ".date('d-m-Y',strtotime($leave_row['to_date']))." </br>Reason: ".$leave_row['reason']."";	
			$data[$i]['entry_date']=date('d-m-Y h:i:sa',strtotime($leave_row['date_of_req']));
			$i++;
		}
		return $data;
}
function getLastWorkingday($params){
	$dbobject = new DB();
	$dbobject->getCon();
	//echo "SELECT `date` FROM `attendence` WHERE date = ( SELECT MAX(date) FROM `attendence` WHERE `date` < ( SELECT MAX(date) FROM `attendence`)) LIMIT 1;";
	$sel_date=$dbobject->select("SELECT `date` FROM `attendence` WHERE date = ( SELECT MAX(date) FROM `attendence` WHERE `date` < ( SELECT MAX(date) FROM `attendence`)) LIMIT 1;");
	$row=$dbobject->fetch_array($sel_date);
	return date('d-m-Y',strtotime($row['date']));
}
function weekly_attendance($studentid,$date){
  	$dbobject = new DB();
	$dbobject->getCon();
	$data=array();
	$SchoolDays=$this->SchoolDays();
    if(!empty($SchoolDays)){
        foreach($SchoolDays as $sc){
           if($sc=="Monday"){
               $monday = date( 'Y-m-d', strtotime( 'monday this week' ) );
               $sel_status=$dbobject->select("SELECT `status` FROM `attendence` WHERE `studentid`='".$studentid."' AND `date`='".$monday."'");
               $row=$dbobject->fetch_array($sel_status);
               $data['Mon']=$row['status'];
           }
           if($sc=="Tuesday"){
               $tuesday = date( 'Y-m-d', strtotime( 'tuesday this week' ) );
         
                $sel_status=$dbobject->select("SELECT `status` FROM `attendence` WHERE `studentid`='".$studentid."' AND `date`='".$tuesday."'");
               $row=$dbobject->fetch_array($sel_status);
               $data['Tue']=$row['status'];
           }
           if($sc=="Wednesday"){
               $wednesday = date( 'Y-m-d', strtotime( 'wednesday this week' ) );
             //  echo "SELECT `status` FROM `attendence` WHERE `studentid`='".$studentid."' AND `date`='".$wednesday."'";
               $sel_status=$dbobject->select("SELECT `status` FROM `attendence` WHERE `studentid`='".$studentid."' AND `date`='".$wednesday."'");
               $row=$dbobject->fetch_array($sel_status);
               $data['Wed']=$row['status'];
           }
           if($sc=="Thursday"){
               $thursday = date( 'Y-m-d', strtotime( 'thursday this week' ) );
               $sel_status=$dbobject->select("SELECT `status` FROM `attendence` WHERE `studentid`='".$studentid."' AND `date`='".$thursday."'");
               $row=$dbobject->fetch_array($sel_status);
               $data['Thu']=$row['status'];
           }
           if($sc=="Friday"){
               $friday = date( 'Y-m-d', strtotime( 'friday this week' ) );
               $sel_status=$dbobject->select("SELECT `status` FROM `attendence` WHERE `studentid`='".$studentid."' AND `date`='".$friday."'");
               $row=$dbobject->fetch_array($sel_status);
               $data['Fri']=$row['status'];
           }
          if($sc=="Saturday"){
               $saturday = date( 'Y-m-d', strtotime( 'saturday this week' ) );
               $sel_status=$dbobject->select("SELECT `status` FROM `attendence` WHERE `studentid`='".$studentid."' AND `date`='".$saturday."'");
               $row=$dbobject->fetch_array($sel_status);
               $data['Sat']="Present";
           }
         if($sc=="Sunday"){
               $sunday = date( 'Y-m-d', strtotime( 'sunday this week' ) );
               $sel_status=$dbobject->select("SELECT `status` FROM `attendence` WHERE `studentid`='".$studentid."' AND `date`='".$sunday."'");
               $row=$dbobject->fetch_array($sel_status);
               $data['Sun']="Present";
           }
        }
    }
    return $data;
}
}
?>