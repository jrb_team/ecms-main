<?php
include_once('createdb.php');
class Appoinment
{
  function get_available_days($tid,$parentid)
  {
	 $dbobj = new DB();
	 $dbobj->getCon();
	 $sql="select distinct `date` from `appoinment_timings` where `tid`='".$tid."'";
	  if($parentid!="")
	  {
		 $sql.=" and `book_parentid` IN ('0','".$parentid."') ";  
	  }	 
	  $sql.="order by `date`";
          $sel=$dbobj->select($sql);
	 $i=0;
	while($row=$dbobj->fetch_array($sel))
		{
			$date[$i]=$row['date'];
			$i++;
		}
	 return $date;
  }
    function parent_booked_details($parentid)
  {
	 $dbobj = new DB();
	 $dbobj->getCon();
	 $sel=$dbobj->select("select * from `appoinment_booking` where `parent_id`='".$parentid."' and `status`='active'");
	 $i=0;
	while($row=$dbobj->fetch_array($sel))
		{
			$details[$i]['id']=$row['id'];
			$details[$i]['tid']=$row['tid'];
			$details[$i]['slot_id']=$row['slot_id'];
			$details[$i]['booking_date']=$row['booking_date'];
			$details[$i]['booked_date']=$row['booked_date'];
			$details[$i]['booked_by']=$row['booked_by'];
			$details[$i]['edit_by']=$row['edit_by'];
			$i++;
		}
	 return $details;
  }
      function teacher_booked_details($tid)
  {
	 $dbobj = new DB();
	 $dbobj->getCon();
	 $sel=$dbobj->select("select * from `appoinment_booking` where `tid`='".$tid."' and `status`='active'");
	 $i=0;
	while($row=$dbobj->fetch_array($sel))
		{
			$details[$i]['id']=$row['id'];
			$details[$i]['parent_id']=$row['parent_id'];
			$details[$i]['slot_id']=$row['slot_id'];
			$details[$i]['booking_date']=$row['booking_date'];
			$details[$i]['booked_date']=$row['booked_date'];
			$details[$i]['booked_by']=$row['booked_by'];
			$details[$i]['edit_by']=$row['edit_by'];
			$i++;
		}
	 return $details;
  }
  function get_timings($tid,$date)
  {
	  	$dbobj = new DB();
		$dbobj->getCon();
		$sel=$dbobj->select("select * from `appoinment_timings` where `tid`='".$tid."' and `date`='".$date."' order by `from_time`");
		$i=0;
		while($row=$dbobj->fetch_array($sel))
		{
			$data[$i]['id']=$row['id'];
			$data[$i]['date']=$row['date'];
			$data[$i]['from_time']=$row['from_time'];
			$data[$i]['to_time']=$row['to_time'];
			$data[$i]['status']=$row['status'];
			$i++;
		}
		return $data;
  }
  function get_timingsBySubid($tid,$date,$subid,$classid)
  {
	  	$dbobj = new DB();
		$dbobj->getCon();
		$i=0;
		$data=array();
		$sel=$dbobj->select("select * from `appoinment_duration` where `tid`='".$tid."' and `date`='".$date."' AND `subid`='".$subid."' AND `classid`='".$classid."'");
		while($row=$dbobj->fetch_array($sel))
		{
			$start_time = strtotime($row['duration_from']);
			$end_time = strtotime($row['duration_to']);
			$slot_duration = $row['slot_timing'];
			$slot_type = $row['slot_timigType'];
			$gap_duration = $row['slot_break'];
			$gap_type=$row['gap_type'];
			// Initialize an empty array to store the slots

			// Loop through the time range and create slots
			$current_time = $start_time;
			while ($current_time < $end_time) {
				// Format the current time
				$data[$i]['date']=$row['date'];
				$data[$i]['enable_parent']=$row['enable_parent'];
				$slot_start = date('h:i A', $current_time);
				
				// Calculate the end time of the slot
				$slot_end = strtotime('+' . $slot_duration . ' '.$slot_type.'', $current_time);
    			$slot_end_formatted = date('h:i A', $slot_end);
				
				
				
				// Add the slot to the array
				$data[$i]['from_time']=$slot_start;
				$data[$i]['to_time']= $slot_end_formatted ;
				
				// Move to the next time slot
				$current_time = strtotime('+' . ($gap_duration) . ' '.$gap_type.'', $slot_end);
				$i++;
			}
			
			
			
		
		}

		/*$sel=$dbobj->select("select * from `appoinment_timings` where `tid`='".$tid."' and `date`='".$date."' AND `subid`='".$subid."' order by `from_time`");
		$i=0;
		while($row=$dbobj->fetch_array($sel))
		{
			$data[$i]['id']=$row['id'];
			$data[$i]['date']=$row['date'];
			$data[$i]['from_time']=$row['from_time'];
			$data[$i]['to_time']=$row['to_time'];
			$data[$i]['status']=$row['status'];
			$i++;
		}*/
		return $data;
  }
  function get_timingsBySubidAndDate($tid,$date_from,$date_to)
  {
	$dbobj = new DB();
	$dbobj->getCon();
	$i=1;
	$sel=$dbobj->select("select `appoinment_duration`.*,`teacher`.`name`,`teacher`.`lname` from `appoinment_duration` LEFT JOIN `teacher` ON `appoinment_duration`.`tid`=`teacher`.`id` where `tid`='".$tid."' and `date`>='".$date_from."'");
	while($row=$dbobj->fetch_array($sel))
	{
		$start_time = strtotime($row['duration_from']);
		$end_time = strtotime($row['duration_to']);
		$slot_duration = $row['slot_timing'];
		$slot_type = $row['slot_timigType'];
		$gap_duration = $row['slot_break'];
		$gap_type=$row['gap_type'];
		$Location=$row['Location'];
		// Initialize an empty array to store the slots
	
		// Loop through the time range and create slots
		$current_time = $start_time;
		while ($current_time < $end_time) {
			// Format the current time
			$data[$i]['id']=$i;
			$data[$i]['teacher_name']=$row['name']." ".$row['lname'];
			$data[$i]['date']=$row['date'];
	
			$slot_start = date('H:i:s', $current_time);
			
			// Calculate the end time of the slot
			$slot_end = strtotime('+' . $slot_duration . ' '.$slot_type.'', $current_time);
			$slot_end_formatted = date('H:i:s', $slot_end);
			
			
			
			// Add the slot to the array
			$data[$i]['Location']=$Location;
			$data[$i]['from_time']=$slot_start;
			$data[$i]['to_time']= $slot_end_formatted ;
			$data[$i]['subid']= $row['subid'] ;
			$data[$i]['classid']= $row['classid'] ;
			$data[$i]['status']= 0 ;
			
			// Move to the next time slot
			$current_time = strtotime('+' . ($gap_duration) . ' '.$gap_type.'', $slot_end);
			$i++;
		}
		
		
		
	
	}
		return $data;
  }
  function check_slot($id,$date)
  {
	  	$dbobj = new DB();
		$dbobj->getCon();
		$slot_det=$dbobj->selectall("appoinment_timings",array('id'=>$id));
		$check=$dbobj->select("select * from `appoinment_booking` where `slot_id`='".$id."' and `booking_date`='".$date."' and `status`!='Cancelled'");
		$num_rows=mysql_num_rows($check);
		if($num_rows==0)
		{
			return $num_rows;
		}
		else
		{
			$row=$dbobj->fetch_array($check);
			return $row['parent_id'];
		}
  }
  function get_myAppoinmentDates($tid)
  {
	  	$dbobj = new DB();
		$dbobj->getCon();
		$select=$dbobj->select("select distinct `date`  from `appoinment_timings` where `tid`='".$tid."' and `status`='open'");
		$i=0;
		while($row=$dbobj->fetch_array($select))
		{
			$dates[$i]=$row['date'];
			$i++;
		}
		return $dates;
  }
  function booking_det($slot_id,$booked_by,$date)
  {
	  $dbobj = new DB();
	  $dbobj->getCon();
	  $select=$dbobj->select("select * from `appoinment_booking` where `slot_id`='".$slot_id."' and `parent_id`='".$booked_by."' and `booking_date`='".$date."' and `status`!='cancelled'");
	  $row=$dbobj->fetch_array($select);
	  return $row;
  }
  function prev_appnmt_by_parent_teacher($parentid,$tid,$date)
  {
	  $dbobj = new DB();
	  $dbobj->getCon();	 
	  $sql=$dbobj->select("select * from `appoinment_booking` where `parent_id`='".$parentid."'  and `tid`='".$tid."' and  `status`='active' and `booking_date`='".$date."' ");
	  $chk=mysql_num_rows($sql);
	  if($chk!=0)
	  {
		$row=$dbobj->fetch_array($sql);
        $appont_det['slot_id']=$row['slot_id'];		
        $appont_det['check']=$chk;		
        $appont_det['id']=$row['id'];		
	  }
	  else
	  {
        $appont_det['slot_id']=0;		
        $appont_det['check']=0;		  
        $appont_det['check']=0;		  
	  }
	  return $appont_det;	  
      	  
  }
 function prev_appnmt_by_time($parentid,$from_time,$to_time,$date) 
 {
	  $dbobj = new DB();
	  $dbobj->getCon();	 
	  $sql=$dbobj->select("select `appoinment_booking`.*,`appoinment_timings`.`from_time`,`appoinment_timings`.`to_time` from `appoinment_booking` left join `appoinment_timings` on `appoinment_booking`.`slot_id`=`appoinment_timings`.`id` where `appoinment_booking`.`parent_id`='".$parentid."'   and  `appoinment_booking`.`status`='active' and `appoinment_timings`.`from_time`='".$from_time."' and `appoinment_timings`.`to_time`='".$to_time."' and `appoinment_timings`.`date`='".$date."'");
	  $chk=mysql_num_rows($sql);
	  if($chk!=0)
	  {
		$row=$dbobj->fetch_array($sql);
        $appont_det['slot_id']=$row['slot_id'];		
        $appont_det['check']=$chk;		
        $appont_det['id']=$row['id'];		
	  }
	  else
	  {
        $appont_det['slot_id']=0;		
        $appont_det['check']=0;		  
        $appont_det['id']=0;		  
	  }
	  return $appont_det; 	 
 }
 function apponmnt_chk($parentid,$tid,$from_time,$to_time,$date)
 {
	 $prev_appnmt_by_parent_teacher=$this->prev_appnmt_by_parent_teacher($parentid,$tid,$date);
	 $prev_appnmt_by_time=$this->prev_appnmt_by_time($parentid,$from_time,$to_time,$date);
	 $chk1=$prev_appnmt_by_parent_teacher['check'];
	 $chk2=$prev_appnmt_by_time['check'];
	if($chk1!=0 && $chk2==0)
		{

			$appo_det['val']=1;
		    $appo_det['slot_id']=$prev_appnmt_by_parent_teacher['slot_id'];
		    $appo_det['id']=$prev_appnmt_by_parent_teacher['id'];
            $appo_det['test']="test1";			


			
		}
		elseif($chk2!=0 && $chk1==0)
		{

			$appo_det['val']=2;
		    $appo_det['slot_id']=$prev_appnmt_by_time['slot_id'];
		    $appo_det['id']=$prev_appnmt_by_time['id'];
            $appo_det['test']="test2";					
		}
		elseif($chk2!=0 && $chk1!=0)
		{
			$appo_det['val']=3;
		    $appo_det['slot_id']=$prev_appnmt_by_time['slot_id'];
		    $appo_det['id']=$prev_appnmt_by_time['id'];
            $appo_det['test']="test3";				
		}

	 
    return $appo_det; 
	 
 }
function request_apponmt($parentid,$tid,$status)
 {
	  $dbobj = new DB();
	  $dbobj->getCon();	 
	  $sql="select * from `appoinment_request` where ";
  	  if($parentid!="-1")
	  {
	  $sql.="`parent_id`='".$parentid."'";	 
	  }	
	  
	  if($tid!="-1" && $parentid!="-1")
	  {
	  $sql.=" and `tid`='".$tid."'";		  
	  }
	  elseif($tid!="-1" && $parentid=="-1")
	  {
		 $sql.=" `tid`='".$tid."'"; 
	  }
	  
	  if($tid=="-1" && $parentid=="-1" && $status!=3)
	  {
		$sql.="`status`='".$status."'";  
	  }
	  elseif($tid=="-1" && $parentid=="-1" && $status==3)
	  {
		$sql.="`status`!='2'";  
	  }	
	  elseif($status==3)
	  {
		$sql.=" and `status`!='2'";  
	  }	  
	  else
	  {
		$sql.=" and `status`='".$status."'";  
	  }	
      //echo $sql;	  
	  $qry=$dbobj->select($sql);
	  $i=0;
	  while($row=$dbobj->fetch_array($qry))
	  {
		  $appo_det[$i]['reason']=$row['reason'];
		  $appo_det[$i]['date_of_req']=$row['date_of_req'];
		  $appo_det[$i]['status']=$row['status'];  
		  $appo_det[$i]['tid']=$row['tid'];  
		  $appo_det[$i]['parent_id']=$row['parent_id'];  
		  $appo_det[$i]['id']=$row['id'];
          $appo_det[$i]['appoinment_status']=$row['appoinment_status'];
			if($row['status']==1)
			{
					  $appo_det[$i]['from_time']=$row['from_time'];  
					  $appo_det[$i]['to_time']=$row['to_time'];  
					  $appo_det[$i]['Location']=$row['Location'];  
					  $appo_det[$i]['date']=$row['date'];	
			} 
		  $i++;
	  }
	  return $appo_det; 
 }
function appmnt_duration($tid,$date)
 {
	  $dbobj = new DB();
	  $dbobj->getCon();	 
	  $sql="select * from `appoinment_duration` where `tid`='".$tid."' and `date`='".$date."'";
      $qry=$dbobj->select($sql);
	  $chk=mysql_num_rows($qry);
	  $row=$dbobj->fetch_array($qry);
	  $duration['check']=$chk;
	  $duration['id']=$row['id'];
	  $duration['duration_from']=$row['duration_from'];
	  $duration['duration_to']=$row['duration_to'];
	  $duration['Location']=$row['Location'];
	  return $duration;
       	  
 }
  function appmnt_duration_timesplit($time,$duration_from,$duration_to)
 {
	 
	    $duration=( strtotime($duration_from) - strtotime($duration_to) ) / 60;
        $duration=abs($duration);
		if($time=="5mnts")
		{
			$slot_loop=round(abs($duration/5));
			//echo $slot_loop."<br>";
			$slot_loop=$slot_loop-1;
			$endTime=$duration_from;
			//$time_det[-1]=$duration_from;
			for($i=0;$i<=$slot_loop;$i++)
			{

				 $time = strtotime($endTime);
				 $startime = date("H:i:s",$time);
				// echo "-";
				 $endTime = date("H:i:s", strtotime('+5 minutes', $time));
				 $time_det[$i]['starttime']=$startime;
				 $time_det[$i]['endtime']=$endTime;
				 //echo "<br>";
				 
				
				
			}
			//echo $i."<br>";

			
		}
		elseif($time=="7mnts")
		{
			$slot_loop=round(abs($duration/7));
			//echo $slot_loop."<br>";
			$slot_loop=$slot_loop-2;	
			$endTime=$duration_from;
			//$time_det[-1]=$duration_from;
			for($i=0;$i<=$slot_loop;$i++)
			{

				 $time = strtotime($endTime);
				 $startime = date("H:i:s",$time);
				// echo "-";
				 $endTime = date("H:i:s", strtotime('+7 minutes', $time));
				 $time_det[$i]['starttime']=$startime;
				 $time_det[$i]['endtime']=$endTime;
				 //echo "<br>";
				
				
			}
			//echo $i."<br>";

			
		}
	  return $time_det;
       	  
 }
function appoinment_status_link($appoinment_status,$date,$id)
 {
	 $current_date=date('Y-m-d');
	 	if($date!=0000-000-00)
		{

		 if($date==$current_date)
		  {

			  if($appoinment_status==1)
			  {
				$appoinment_status_link="ATTENDED";
				  
			  }
              else
			  {
			 // if($appoinment_status==0)
			  //{
				$appoinment_status_link="<a class='btn btn-success' href='appmnt_status.php?id=".$id."&status=1' onclick='return check_apponmt_status()'>CHECKED IN</a>";
				 $appoinment_status_link.="&nbsp;&nbsp;"; 
			  //}
			  //elseif($appoinment_status==1)
			 // {
				 // $appoinment_status_link.="<a class='btn btn-danger' href='appmnt_status.php?id=".$id."&status=0' onclick='return check_apponmt_status()'>NOT ATTEND</a>";
			  //}				  
			  }			  
			  		
		  }
		  elseif($date>$current_date)
		  {
			 $appoinment_status_link=""; 
		  }
		  else
		  {
			  if($appoinment_status==1)
			  {
				$appoinment_status_link="ATTENDED";
				  
			  }
			  elseif($appoinment_status==0)
			  {
				  $appoinment_status_link="NOT ATTEND";
			  }		  
		  }
          		  
		}
		return $appoinment_status_link;
 }
 function parent_apponmt_action($id,$date,$requested_status)
 {
	 $current_date=date('Y-m-d');
	 if($date!=0000-000-00)
		{
         // echo $date."<=".$current_date;	  
		 if($date>=$current_date)
		  {
			   $action="<a class='btn btn-danger' href='del_req_apponmt.php?id=".$id."' onclick='return del_apponmt()'>CANCEL</a>";
		  }
		}
		if($requested_status==0)
		{
			$action="<a class='btn btn-danger' href='del_req_apponmt.php?id=".$id."' onclick='return del_apponmt()'>CANCEL</a>";
		}
		return $action;
 }
 function check_meeting($tid,$date,$from_time,$to_time,$parentid)
 {
	  $dbobj = new DB();
	  $dbobj->getCon();	 
	  $modifyed_time1=date("H:i:s", strtotime('-5 minutes', strtotime($from_time)));
	  $modifyed_time2=date("H:i:s", strtotime('+5 minutes', strtotime($to_time)));
      $sql1="SELECT * FROM `appoinment_timings` WHERE `date`='".$date."' and `tid`!='".$tid."' and `from_time`>='".$modifyed_time1."' and `to_time`<='".$from_time."' and `book_parentid`='".$parentid."'";
	  $qry1=$dbobj->select($sql1);
      $sql2="SELECT * FROM `appoinment_timings` WHERE `date`='".$date."' and `tid`!='".$tid."' and `from_time`>='".$to_time."' and `to_time`<='".$modifyed_time2."' and `book_parentid`='".$parentid."'";
	  $qry2=$dbobj->select($sql2);	  
      $num_rows1=mysql_num_rows($qry1);
      $num_rows2=mysql_num_rows($qry2);
	  if($num_rows1!=0 || $num_rows2!=0)
	  {
		  $val=1;
	  }
	  else
	  {
		  $val=0;
	  }
	  
	  return $val;	  
 }
function request_apponmt_teacher($parentid,$tid,$status)
 {
	  $dbobj = new DB();
	  $dbobj->getCon();	 
	  $sql="select * from `appoinment_request` where ";
  	  if($parentid!="-1")
	  {
	  $sql.="`parent_id`='".$parentid."'";	 
	  }	
	  
	  if($tid!="-1" && $parentid!="-1")
	  {
	  $sql.=" and `tid`='".$tid."'";		  
	  }
	  elseif($tid!="-1" && $parentid=="-1")
	  {
		 $sql.=" `tid`='".$tid."'"; 
	  }
	  
	  if($tid=="-1" && $parentid=="-1" && $status!=3)
	  {
		$sql.="`status`='".$status."'";  
	  }
	  elseif($tid=="-1" && $parentid=="-1" && $status==3)
	  {
		$sql.="`status`!='2'";  
	  }	
	  elseif($status==3)
	  {
		$sql.=" and `status`!='2'";  
	  }  
	  else
	  {
		$sql.=" and `status`='".$status."'";  
	  }	
      $sql.=" order by `id`  ASC";	  
	  $qry=$dbobj->select($sql);
	  $i=0;
	  $current_date=date('Y-m-d');
	  while($row=$dbobj->fetch_array($qry))
	  {		  
		  $parent_det=$dbobj->selectall("parent",array("id"=>$row['parent_id']));
		  $student_det=$dbobj->selectall("student",array("parent_id"=>$row['parent_id']));
		  $appo_det[$i]['id']=$row['id'];
		  $appo_det[$i]['tid']=$row['tid'];		  
		  $appo_det[$i]['parent_name']=$parent_det['fname'];  
		  $appo_det[$i]['student_name']=$student_det['sname'];
		  $appo_det[$i]['parent_id']=$row['parent_id']; 
		  $appo_det[$i]['reason']=$row['reason'];
		  $appo_det[$i]['date_of_req']=date('d-m-Y',strtotime($row['date_of_req']));
		  $appo_det[$i]['status']=$row['status'];		  
          $appo_det[$i]['appointment status']=$row['appoinment_status'];
		  if($row['date']!=0000-000-00)
		  {
			   		 if($row['date']==$current_date)
					 {
						 if($row['appoinment_status']==1)
						 {
							$appo_det[$i]['status_view']="ATTENDED"; 
							 $appo_det[$i]['status_button']=0; 
						 }
						  else
						  {
							  $appo_det[$i]['status_view']="ATTEND"; 
							  $appo_det[$i]['status_button']=1; 
							  
						  }
					 }
					 elseif($row['date']>$current_date)
					 {
						 $appo_det[$i]['status_view']=""; 
						 $appo_det[$i]['status_button']=0; 
					 }
					 else
					 {
						  if($row['appoinment_status']==1)
						  {
							$appo_det[$i]['status_view']="ATTENDED";
							$appo_det[$i]['status_button']=0; 
							  
						  }
						  elseif($row['appoinment_status']==0)
						  {
							  $appo_det[$i]['status_view']="NOT ATTEND";
							  $appo_det[$i]['status_button']=0; 
						  }							 
					 }
		  }
		  else
		  {
			  $appo_det[$i]['status_view']="";
			  $appo_det[$i]['status_button']=0; 
		  }
		  
			if($row['status']==0)
			{
				$appo_det[$i]['approval_status']="Not Approved";
							$appo_det[$i]['from_time']="";  
		   $appo_det[$i]['to_time']="";  
		   $appo_det[$i]['Location']="";  
		   $appo_det[$i]['date']=""; 
				
			}
			elseif($row['status']==1)
			{
				$appo_det[$i]['approval_status']="Approved";
						   $appo_det[$i]['from_time']=$row['from_time'];  
		   $appo_det[$i]['to_time']=$row['to_time'];  
		   $appo_det[$i]['Location']=$row['Location'];  
		   $appo_det[$i]['date']=date('d-m-Y',strtotime($row['date']));
			}		  
			 
		  $i++;
	  }
	  return $appo_det; 
 } 
  function insert_parent_appoinment($parent_id,$tid,$reason,$usertype,$userid)
 {
	  $dbobject = new DB();
	  $dbobject->getCon();	 
	  $date_of_req=date('Y-m-d');
	  $status_date=date('Y-m-d h:i:sa');
      $ins=$dbobject->exe_qry("insert into `appoinment_request` (`parent_id`,`tid`,`date_of_req`,`reason`,`status_date`,`usertype`,`status_userid`)
      values('".$parent_id."','".$tid."','".$date_of_req."','".$reason."','".$status_date."','".$usertype."','".$userid."')");
      return mysql_insert_id();	  
      
 }
function update_appointment_Byteacher($from_time,$to_time,$location,$date,$id,$status_id)
{
	  $dbobject = new DB();
	  $dbobject->getCon();
	  $status=1;
	  $status_date=date('Y-m-d h:i:sa');
		if($date!=000-00-00)
		{
			$date=date('Y-m-d',strtotime($date));
		}
		else
		{
			$date="";
		}		  
$updt = $dbobject->exe_qry("UPDATE `appoinment_request` SET `from_time`='".$from_time."',`to_time`='".$to_time."',`Location`='".$location."',`status`='".$status."',`status_date`='".$status_date."',`usertype`='teacher',`status_userid`='".$status_id."',`date`='".$date."' WHERE `id`='".$id."'");	
}
function MeetingAssignedStudent($subid,$tid,$date){
	$dbobject = new DB();
	$dbobject->getCon();
	$i=0;
	$data=array();
	$sel_student=$dbobject->select("SELECT `classid`,`sid` FROM `assigned_student_meeting` WHERE `tid`='".$tid."' AND `subid`='".$subid."' AND `date`='".$date."'");
	while($row=$dbobject->fetch_array($sel_student)){
		$data['classid'][$i]=$row['classid'];
		$data['sid'][$i]=$row['sid'];
		$i++;
	}
	return $data;
}
function Get_Meeting($params){
	$dbobject = new DB();
	$dbobject->getCon();
	$event=array();
	$i=0;
	$sql="SELECT * FROM `meeting_request` WHERE `acyear`='".$params['acyear']."' AND `tid`='".$params['staffid']."'";
	$sel=$dbobject->select($sql);
	while($row=$dbobject->fetch_array($sel)){
		$event[$i]['id']=$row['id'];
		$event[$i]['parent_id']=$row['parent_id'];
		$event[$i]['tid']=$row['tid'];
		$event[$i]['reason']=$row['reason'];
		$event[$i]['date_of_req']=$row['date_of_req'];
		$event[$i]['status']=$row['status'];
		$event[$i]['status_date']=$row['status_date'];
		$event[$i]['usertype']=$row['usertype'];
		$event[$i]['status_userid']=$row['status_userid'];
		$event[$i]['from_time']=$row['from_time'];
		$event[$i]['to_time']=$row['to_time'];
		$event[$i]['Location']=$row['Location'];
		$event[$i]['date']=$row['date'];
		$event[$i]['appoinment_status']=$row['appoinment_status'];
		$event[$i]['acyear']=$row['acyear'];
		$i++;
	}
	return $event;

}
}
?>