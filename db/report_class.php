<?php
include_once('createdb.php');
class report
{
 	function nonsubmitted_report_list_family_code($type)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		if($type=="BUSCREDIT")
		{
		$query = "SELECT credit_card_bus.*,parent.f_sur_name,parent.m_sur_name,student.studentid,student.parent_id,student.family_code FROM parent  left join credit_card_bus on parent.id=credit_card_bus.parent_id left join student on student.parent_id=parent.id WHERE credit_card_bus.parent_id IS NULL";			
		}
        elseif($type=="BUSDEBIT")
		{
		$query = "SELECT student_bus_form.*,parent.f_sur_name,parent.m_sur_name,student.studentid,student.parent_id,student.family_code FROM parent  left join student_bus_form on parent.id=student_bus_form.parent_id left join student on student.parent_id=parent.id WHERE student_bus_form.parent_id IS NULL";			
		}
        elseif($type=="FEECREDIT")
		{
		$query = "SELECT credit_card_authorisation.*,parent.f_sur_name,parent.m_sur_name,student.studentid,student.parent_id,student.family_code FROM parent  left join credit_card_authorisation on parent.id=credit_card_authorisation.parent_id left join student on student.parent_id=parent.id WHERE credit_card_authorisation.parent_id IS NULL";				
		}		
        elseif($type=="FEEDEBIT")
		{
		$query = "SELECT direct_debit_request.*,parent.f_sur_name,parent.m_sur_name,student.studentid,student.parent_id,student.family_code FROM parent  left join direct_debit_request on parent.id=direct_debit_request.parent_id left join student on student.parent_id=parent.id WHERE direct_debit_request.parent_id IS NULL";				
		}		
        elseif($type=="CSEF")
		{
		$query = "SELECT submitted_form.*,parent.f_sur_name,parent.m_sur_name,student.studentid,student.parent_id,student.family_code FROM parent 
		left join submitted_form on parent.id=submitted_form.parent_id
		left join student on student.parent_id=parent.id 
		WHERE submitted_form.parent_id IS NULL and submitted_form.type='center_pay'" ;			
		}
        elseif($type=="CENTREPAY")
		{
		$query = "SELECT submitted_form.*,parent.f_sur_name,parent.m_sur_name,student.studentid,student.parent_id,student.family_code FROM parent 
		left join submitted_form on parent.id=submitted_form.parent_id
		left join student on student.parent_id=parent.id 
		WHERE submitted_form.parent_id IS NULL  and submitted_form.type='csef_application'" ;			
		}
        elseif($type=="FAMILY")
		{
		 $query = "SELECT famliy_info.*,parent.f_sur_name,student.studentid,student.parent_id,student.family_code FROM parent 
		 left join famliy_info on parent.id=famliy_info.parent_id
		 left join student on student.parent_id=parent.id 
		 WHERE famliy_info.parent_id IS NULL" ;			
		}
        elseif($type=="WORKINGBEE")
		{
        $query = "SELECT working_bee.*,parent.f_sur_name,parent.m_sur_name,student.studentid,student.family_code FROM parent left join working_bee on parent.id=working_bee.student_id left join student on student.parent_id=parent.id WHERE working_bee.student_id IS NULL" ;			
		}		
		$select=$dbobj->select($query);
		$i=1;
		while($row=$dbobject->fetch_array($select))
	    {
			$data[$i][$row['family_code']]=$row['family_code'];
			$i++;
		}
		return $data;
	}
	function submitted_report_list_family_code($family_code,$user_type)
	{
		$dbobject = new DB();
		$dbobject->getCon();	     
		$reminder_types=$dbobject->reminder_types();
		$parent_qry=$dbobject->select("SELECT * FROM `parent` WHERE `family_code`='".$family_code."'");
		$parent_row=$dbobject->fetch_array($parent_qry);
		$parent_id=$parent_row['id'];
		if(!empty($reminder_types))
		{
			foreach($reminder_types as $type)
			{
				if($type=="BUSCREDIT")
				{
				$query = "SELECT credit_card_bus.*,parent.f_sur_name,parent.m_sur_name,student.studentid,student.parent_id,student.family_code FROM parent  left join credit_card_bus on parent.id=credit_card_bus.parent_id left join student on student.parent_id=parent.id WHERE credit_card_bus.parent_id='".$parent_id."'";		
				$select=$dbobject->select($query);
				$check=$dbobject->mysql_num_row($select);
		        $row=$dbobject->fetch_array($select);
				$data[$type]['check']=$check;
				$data[$type]['type']=$type;
				$data[$type]['pdf_link']="credit_card_bus_form_pdf.php";
				$data[$type]['pdf_link_type']=0;	
				$data[$type]['parent_id']=$parent_id;	
				}
				elseif($type=="BUSDEBIT")
				{
				$query = "SELECT student_bus_form.*,parent.f_sur_name,parent.m_sur_name,student.studentid,student.parent_id,student.family_code FROM parent  left join student_bus_form on parent.id=student_bus_form.parent_id left join student on student.parent_id=parent.id WHERE student_bus_form.parent_id='".$parent_id."'";		
				$select=$dbobject->select($query);
				$check=$dbobject->mysql_num_row($select);
		        $row=$dbobject->fetch_array($select);
				$data[$type]['check']=$check;	
				$data[$type]['type']=$type;	
               $data[$type]['pdf_link']="bus_from_pdf.php";	
                $data[$type]['pdf_link_type']=0;
                $data[$type]['parent_id']=$parent_id;				
				}	
				elseif($type=="FEECREDIT")
				{
				$query = "SELECT credit_card_authorisation.*,parent.f_sur_name,parent.m_sur_name,student.studentid,student.parent_id,student.family_code FROM parent  left join credit_card_authorisation on parent.id=credit_card_authorisation.parent_id left join student on student.parent_id=parent.id WHERE credit_card_authorisation.parent_id='".$parent_id."'";			
				$select=$dbobject->select($query);
				$check=$dbobject->mysql_num_row($select);
		        $row=$dbobject->fetch_array($select);
				$data[$type]['check']=$check;	
				$data[$type]['type']=$type;		
                $data[$type]['pdf_link']="credit_card_authorisation_request_pdf.php";					
                $data[$type]['pdf_link_type']=0;
                $data[$type]['parent_id']=$parent_id;				
				}		
				elseif($type=="FEEDEBIT")
				{
				$query = "SELECT direct_debit_request.*,parent.f_sur_name,parent.m_sur_name,student.studentid,student.parent_id,student.family_code FROM parent  left join direct_debit_request on parent.id=direct_debit_request.parent_id left join student on student.parent_id=parent.id WHERE direct_debit_request.parent_id='".$parent_id."'";			
				$select=$dbobject->select($query);
				$check=$dbobject->mysql_num_row($select);
		        $row=$dbobject->fetch_array($select);
				$data[$type]['check']=$check;
				$data[$type]['type']=$type;			
                $data[$type]['pdf_link']="direct_debit_request_pdf.php";	
                $data[$type]['pdf_link_type']=0;
                $data[$type]['parent_id']=$parent_id;				
				}		
				elseif($type=="CSEF")
				{
				$query = "SELECT submitted_form.*,parent.f_sur_name,parent.m_sur_name,student.studentid,student.parent_id,student.family_code FROM parent 
				left join submitted_form on parent.id=submitted_form.parent_id
				left join student on student.parent_id=parent.id 
				WHERE submitted_form.parent_id='".$parent_id."' and submitted_form.type='center_pay'" ;			
				$select=$dbobject->select($query);
				$check=$dbobject->mysql_num_row($select);
		        $row=$dbobject->fetch_array($select);
				if($row['file']!="")
				{
				$data[$type]['check']=$check;		
				$data[$type]['type']=$type;	
                $data[$type]['pdf_link']="../forms/".$row['file'];
                $data[$type]['pdf_link_type']=2;
                $data[$type]['parent_id']=$parent_id;					
				}			
				}
				elseif($type=="CENTREPAY")
				{
				$query = "SELECT submitted_form.*,parent.f_sur_name,parent.m_sur_name,student.studentid,student.parent_id,student.family_code FROM parent 
				left join submitted_form on parent.id=submitted_form.parent_id
				left join student on student.parent_id=parent.id 
				WHERE submitted_form.parent_id='".$parent_id."'  and submitted_form.type='csef_application'" ;		
				$select=$dbobject->select($query);
				$check=$dbobject->mysql_num_row($select);
		        $row=$dbobject->fetch_array($select);
				if($row['file']!="")
				{
				$data[$type]['check']=$check;		
				$data[$type]['type']=$type;	
                $data[$type]['pdf_link']="../forms/".$row['file'];
                $data[$type]['pdf_link_type']=2;
                $data[$type]['parent_id']=$parent_id;					
				}
				
				}
				elseif($type=="FAMILY")
				{
				 $query = "SELECT famliy_info.*,parent.f_sur_name,student.studentid,student.parent_id,student.family_code FROM parent 
				 left join famliy_info on parent.id=famliy_info.parent_id
				 left join student on student.parent_id=parent.id 
				 WHERE famliy_info.parent_id='".$parent_id."'";	
				$select=$dbobject->select($query);
				$check=$dbobject->mysql_num_row($select);
		        $row=$dbobject->fetch_array($select);
				$data[$type]['check']=$check;	
				$data[$type]['type']=$type;	
                $data[$type]['pdf_link']="famliy_info_form_pdf.php?studentid=".$row['studentid'];
                $data[$type]['pdf_link_type']=1;
                $data[$type]['parent_id']=$parent_id;				
				}
				elseif($type=="WORKINGBEE")
				{
				$query = "SELECT working_bee.*,parent.f_sur_name,parent.m_sur_name,student.studentid,student.family_code FROM parent left join working_bee on parent.id=working_bee.student_id left join student on student.parent_id=parent.id WHERE working_bee.student_id='".$parent_id."'";			
				$select=$dbobject->select($query);
				$check=$dbobject->mysql_num_row($select);
		        $row=$dbobject->fetch_array($select);
				$data[$type]['check']=$check;	
				$data[$type]['type']=$type;                		
                $data[$type]['pdf_link_type']=2;
                $data[$type]['parent_id']=$parent_id;				
				if($user_type=="admin")
				{
				$data[$type]['pdf_link']="print_wb_form.php?id=".$parent_id;	
				}
				else
				{
				$data[$type]['pdf_link']="print_wb_form.php";	
				}
				
				}				

                				
			}
		}
		return $data;
	}
function parent_id_by_family_code($family_code)
{
		$dbobject = new DB();
		$dbobject->getCon();
		$cacyear=$dbobject->get_acyear();
        $i=0;		
		if($family_code!="")
		{
		$parent_qry=$dbobject->select("SELECT * FROM `parent` WHERE `family_code`='".$family_code."' and `parent_status`='1'");
		$parent_row=$dbobject->fetch_array($parent_qry);
        $data[$i]['id']=$parent_row['id'];
        $data[$i]['m_sur_name']=$parent_row['m_sur_name'];
        $data[$i]['m_given_name']=$parent_row['m_given_name'];
        $data[$i]['f_sur_name']=$parent_row['f_sur_name'];
         $data[$i]['f_given_name']=$parent_row['f_given_name'];
        $data[$i]['family_code']=$parent_row['family_code'];
        $data[$i]['studentid']=$parent_row['studentid'];
        $data[$i]['HOUSE_NO']=$parent_row['HOUSE_NO'];
        $data[$i]['STREET']=$parent_row['STREET'];
        $data[$i]['HM_ADDRESS02']=$parent_row['HM_ADDRESS02'];
        $data[$i]['HM_ADDRESS03']=$parent_row['HM_ADDRESS03'];
        $data[$i]['HM_STATE']=$parent_row['HM_STATE'];
        $data[$i]['HM_POSTCODE']=$parent_row['HM_POSTCODE'];        
		$i++;
		}
		else
		{
		//echo "SELECT distinct  `family_code` FROM `parent` order by `famliy_info`.`updated_date` DESC";
    $sql="select distinct `student`.`family_code` from `student_class` LEFT JOIN `student` on `student`.`sid`=`student_class`.`sid` where  `student_class`.`acyear`='".$cacyear."' ";
   	$sql.=" and `s_status`='active'";
   	$sql.=" order by FIELD(gender,'F','M'),`s_sur_name`,`sname`";
		//$parent_qry=$dbobject->select("SELECT distinct  `family_code` FROM `parent` where `family_code`!='' AND `parent_status`='1'");
		$parent_qry=$dbobject->select($sql);
			 while($parent_row=$dbobject->fetch_array($parent_qry))
			 {
				 $family_code[$parent_row['family_code']]=$parent_row['family_code'];
			 }	
           foreach($family_code as $fmc)
		   {
		   // echo "SELECT * FROM `parent` WHERE `family_code`='".$fmc."'";
			$parent_qry=$dbobject->select("SELECT * FROM `parent` WHERE `family_code`='".$fmc."'");
			$parent_row=$dbobject->fetch_array($parent_qry);
			$data[$i]['id']=$parent_row['id'];
        $data[$i]['m_sur_name']=$parent_row['m_sur_name'];
        $data[$i]['m_given_name']=$parent_row['m_given_name'];
        $data[$i]['f_sur_name']=$parent_row['f_sur_name'];
         $data[$i]['f_given_name']=$parent_row['f_given_name'];
			$data[$i]['family_code']=$parent_row['family_code'];
			$data[$i]['studentid']=$parent_row['studentid'];
        $data[$i]['HOUSE_NO']=$parent_row['HOUSE_NO'];
        $data[$i]['STREET']=$parent_row['STREET'];
        $data[$i]['HM_ADDRESS02']=$parent_row['HM_ADDRESS02'];
        $data[$i]['HM_ADDRESS03']=$parent_row['HM_ADDRESS03'];
        $data[$i]['HM_STATE']=$parent_row['HM_STATE'];
        $data[$i]['HM_POSTCODE']=$parent_row['HM_POSTCODE'];			
			$i++;		
		   }			 
		}
	return $data;
}
function submitted_form_data_by_typewise($typewise)
{
	$dbobject = new DB();
	$dbobject->getCon();
    $parent_id_by_family_code=$this->parent_id_by_family_code("");
	if($typewise=="bus_report")
	{
		$type_data[0]="bus_direct_debit";
		$type_data[1]="bus_credit_card";
	}
	elseif($typewise=="fee_report")
	{
		$type_data[0]="fees_credit_card";
		$type_data[1]="fees_direct_debit";	
	}
	else
	{
		$type_data[0]=$typewise;
	}
    if(!empty($parent_id_by_family_code))
		{ 
		 foreach($type_data as $type)
		 { 
		   $i=0;
		   foreach($parent_id_by_family_code as $row)
			  {
				  if($row['studentid']!="")
				  {
 
					  if($type=='bus_direct_debit')
					  {
						$sql=$dbobject->select("SELECT * FROM `student_bus_form` WHERE `parent_id`='".$row['id']."'"); 
                        $chk=$dbobject->mysql_num_row($sql);						
					  }
					  elseif($type=='bus_credit_card')
					  {
						$sql=$dbobject->select("SELECT * FROM `credit_card_bus` WHERE `parent_id`='".$row['id']."'"); 
                        $chk=$dbobject->mysql_num_row($sql);							  
					  }
					  elseif($type=='fees_credit_card')
					  {
						$sql=$dbobject->select("SELECT * FROM `credit_card_authorisation` WHERE `parent_id`='".$row['id']."'"); 
                        $chk=$dbobject->mysql_num_row($sql);							  
					  }
					  elseif($type=='fees_direct_debit')
					  {
						$sql=$dbobject->select("SELECT * FROM `direct_debit_request` WHERE `parent_id`='".$row['id']."'"); 
                        $chk=$dbobject->mysql_num_row($sql);							  
					  }
                      if($chk!=0)
					  {
						  $row2=$dbobject->fetch_array($sql);
						  $data[$type][$i]['f_sur_name']=$row['f_sur_name'];
						  $data[$type][$i]['m_sur_name']=$row['m_sur_name'];
						  $data[$type][$i]['family_code']=$row['family_code'];
						  $data[$type][$i]['updated_date']=$row2['updated_date'];
						  $data[$type][$i]['status']=$row2['status'];
						  $data[$type][$i]['studentid']=$row['studentid'];
						  $data[$type][$i]['parent_id']=$row['id'];
						  $data[$type][$i]['c_expdatem']=$row2['c_expdatem'];
						  $data[$type][$i]['c_expdatey']=$row2['c_expdatey'];
						  $data[$type][$i]['bsb_number1']=$row2['bsb_number1'];
						  $data[$type][$i]['bsb_number2']=$row2['bsb_number2'];
						  $data[$type][$i]['bsb_number3']=$row2['bsb_number3'];
						  $data[$type][$i]['bsb_number4']=$row2['bsb_number4'];
						  $data[$type][$i]['bsb_number5']=$row2['bsb_number5'];
						  $data[$type][$i]['bsb_number6']=$row2['bsb_number6'];
						  $data[$type][$i]['account_number1']=$row2['account_number1'];
						  $data[$type][$i]['account_number2']=$row2['account_number2'];
						  $data[$type][$i]['account_number3']=$row2['account_number3'];
						  $data[$type][$i]['account_number4']=$row2['account_number4'];
						  $data[$type][$i]['account_number5']=$row2['account_number5'];
						  $data[$type][$i]['account_number6']=$row2['account_number6'];
						  $data[$type][$i]['account_number7']=$row2['account_number7'];
						  $data[$type][$i]['account_number8']=$row2['account_number8'];
						  $data[$type][$i]['account_number9']=$row2['account_number9'];
						  $data[$type][$i]['account_number10']=$row2['account_number10'];
						  $i++;
					  }
				    					  
				  }
			  }
		   }
		}			  
	return $data;
}
function submitted_form_data_by_type($type,$parent_id,$acyear)
{
	$dbobject = new DB();
	$dbobject->getCon();
		if($type=="bus_direct_debit")
		{
		$sql=$dbobject->select("SELECT * FROM `student_bus_form` WHERE `parent_id`='".$parent_id."' and `acyear`='".$acyear."'"); 
        $chk=$dbobject->mysql_num_row($sql);	
        $row2=$dbobject->fetch_array($sql);	
        $data['bsb_number']= $row2['bsb_number1']."".$row2['bsb_number2']."".$row2['bsb_number3']."".$row2['bsb_number4']."".$row2['bsb_number5']."".$row2['bsb_number6'];				
        $data['account_number']= $row2['account_number1']."".$row2['account_number2']."".$row2['account_number3']."".$row2['account_number4']."".$row2['account_number5']."".$row2['account_number6']."".$row2['account_number7']."".$row2['account_number8']."".$row2['account_number9']."".$row2['account_number10'];				
        $data['f_sur_name']= $row2['surname'];
        $data['f_given_name']= $row2['given_name'];
        $data['account_name']= $row2['account_name'];
        if($row2['term1']=="term1")
		{
			        $wk_data= "TERM 1, TERM 2, TERM 3 and TERM 4";	
		}
		else
		{
		        $wk_data= $row2['term1'];	
		}
		$data['term1']= $wk_data;		
        $data['day']= $row2['day'];	
        $data['Other_arrangement']= $row2['Other_arrangement'];			
        $data['status']= $row2['status'];
		$data['chk']= $chk;				
		}
		if($type=="bus_credit_card")
		{
		$sql=$dbobject->select("SELECT * FROM `credit_card_bus` WHERE `parent_id`='".$parent_id."' and `acyear`='".$acyear."'"); 
        $chk=$dbobject->mysql_num_row($sql);
        $row2=$dbobject->fetch_array($sql);		
        $data['credit_number']= $row2['account_number1'];				
        $data['exp']= $row2['c_expdatem']."-".$row2['c_expdatey'];	
        $data['f_sur_name']= $row2['surname'];
        $data['f_given_name']= $row2['given_name'];		
        $data['name_on_card']= $row2['name_on_card'];	
        if($row2['term1']=="term1")
		{
			        $wk_data= "TERM 1, TERM 2, TERM 3 and TERM 4";	
		}
		else
		{
		        $wk_data= $row2['term1'];	
		}
		$data['term1']= $wk_data;		
        $data['day']= $row2['day'];	
        $data['Other_arrangement']= $row2['Other_arrangement'];			
        $data['status']= $row2['status'];		
        $data['chk']= $chk;			
		}
		if($type=="fees_credit_card")
		{
		$sql=$dbobject->select("SELECT * FROM `credit_card_authorisation` WHERE `parent_id`='".$parent_id."' and `acyear`='".$acyear."'"); 
         $chk=$dbobject->mysql_num_row($sql);	
        $row2=$dbobject->fetch_array($sql);	
        $data['credit_number']= $row2['account_number1'];				
        $data['exp']= $row2['exp_card'];	
        $data['f_sur_name']= $row2['surname'];
        $data['f_given_name']= $row2['given_name'];
        $data['name_of_bank']= $row2['name_of_bank'];	
        $data['status']= $row2['status'];	
        $data['amount']= $row2['bsb_number4']."".$row2['bsb_number1']."".$row2['bsb_number2']."".$row2['bsb_number3']." . ".$row2['bsb_number5']."".$row2['bsb_number9'];	
        $data['amount_words']= $row2['bsb_number7'];	
        $data['withdrawal']= $row2['bsb_number6']." ".$row2['bsb_number8']." ".$row2['bsb_number10'];	
        if($row2['wk']==1)
		{
			$wk_data="Weekly";
		}
		if($row2['wk']==2)
		{
			$wk_data="Fortnightly";
		}
		$data['wk']= $wk_data;			
		$data['cmnts']= $row2['adnt_cmnts'];			
        $data['chk']= $chk;			
		}
		if($type=="fees_direct_debit")
		{
		$sql=$dbobject->select("SELECT * FROM `direct_debit_request` WHERE `parent_id`='".$parent_id."' and `acyear`='".$acyear."'"); 
         $chk=$dbobject->mysql_num_row($sql);
        $row2=$dbobject->fetch_array($sql);	
        $data['bsb_number']= $row2['bsb_number1']."".$row2['bsb_number2']."".$row2['bsb_number3']."".$row2['bsb_number4']."".$row2['bsb_number5']."".$row2['bsb_number6'];				
        $data['account_number']= $row2['account_number1']."".$row2['account_number2']."".$row2['account_number3']."".$row2['account_number4']."".$row2['account_number5']."".$row2['account_number6']."".$row2['account_number7']."".$row2['account_number8']."".$row2['account_number9']."".$row2['account_number10'];				
        $data['f_sur_name']= $row2['surname'];
        $data['f_given_name']= $row2['given_name'];  
        $data['account_name']= $row2['account_name'];	
        $data['status']= $row2['status'];
		if($row2['cash3']!="")
		{
		$data['amount']= "$".$row2['cash4']."".$row2['cash1']."".$row2['cash2']."".$row2['cash3']." . ".$row2['ps1']."".$row2['ps2'];	
		}
        else
		{
			$data['amount']= $row2['cash4']."".$row2['cash1']."".$row2['cash2']."".$row2['cash3']." . ".$row2['ps1']."".$row2['ps2'];
		}	
        $data['amount_words']= $row2['amount_in_words'];	
        if($row2['fw_date']!="" && $row2['fw_date']!="1970-00-00")
		{
		$data['withdrawal']=date('d-m-Y',strtotime($row2['fw_date']));	
		}
		else
		{
			$data['withdrawal']= $row2['fw_date'];
		}	
        if($row2['pfreq']==1)
		{
			$wk_data="Weekly";
		}
		if($row2['pfreq']==2)
		{
			$wk_data="Fortnightly";
		}
		$data['wk']= $wk_data;			
		$data['cmnts']= $row2['adcomments'];		
		$data['chk']= $chk;			
		}
return $data;
}
function working_bee_data_by_type($id,$type)
{
	$dbobject = new DB();
	$dbobject->getCon();
	$i=0;
     if($type=="saturday_working_bees")
	 {
		// echo "SELECT * FROM `saturday_working_bees` WHERE `wb_id` ='".$id."' and `availability`='1'";
		 $qry=$dbobject->select("SELECT * FROM `saturday_working_bees` WHERE `wb_id` ='".$id."' and `availability`='1'");
		 while($row=$dbobject->fetch_array($qry))
		 {
			 $data[$i]['date']=$row['date'];
			 $data[$i]['total_hrs']=$row['total_hrs'];
			 $i++;
		 }
	 }
     if($type=="canteen_wb")
	 {
		 $qry=$dbobject->select("SELECT * FROM `canteen_wb` WHERE `wb_id` ='".$id."' and `avaliability`='1'");
		 while($row=$dbobject->fetch_array($qry))
		 {
			 $data[$i]['date']=$row['date'];
			  $i++;
		 }
	 }
return $data;	 
}
function parent_id_by_family_code_acyear($family_code,$acyear)
{
		$dbobject = new DB();
		$dbobject->getCon();
		if($acyear=="")
		{
				$acyear=$dbobject->get_acyear();	
		}
		else
		{
					$acyear=$acyear;
		}

        $i=0;		
		if($family_code!="")
		{
		$parent_qry=$dbobject->select("SELECT * FROM `parent` WHERE `family_code`='".$family_code."' and `parent_status`='1'");
		$parent_row=$dbobject->fetch_array($parent_qry);
        $data[$i]['id']=$parent_row['id'];
        $data[$i]['m_sur_name']=$parent_row['m_sur_name'];
        $data[$i]['m_given_name']=$parent_row['m_given_name'];
        $data[$i]['f_sur_name']=$parent_row['f_sur_name'];
         $data[$i]['f_given_name']=$parent_row['f_given_name'];
        $data[$i]['family_code']=$parent_row['family_code'];
        $data[$i]['studentid']=$parent_row['studentid'];
        $data[$i]['HOUSE_NO']=$parent_row['HOUSE_NO'];
        $data[$i]['STREET']=$parent_row['STREET'];
        $data[$i]['HM_ADDRESS02']=$parent_row['HM_ADDRESS02'];
        $data[$i]['HM_ADDRESS03']=$parent_row['HM_ADDRESS03'];
        $data[$i]['HM_STATE']=$parent_row['HM_STATE'];
        $data[$i]['HM_POSTCODE']=$parent_row['HM_POSTCODE'];        
		$i++;
		}
		else
		{
		//echo "SELECT distinct  `family_code` FROM `parent` order by `famliy_info`.`updated_date` DESC";
    $sql="select distinct `student`.`family_code` from `student_class` LEFT JOIN `student` on `student`.`sid`=`student_class`.`sid` where  `student_class`.`acyear`='".$acyear."' ";
   	$sql.=" and `s_status`='active'";
   	$sql.=" order by FIELD(gender,'F','M'),`s_sur_name`,`sname`";
		//$parent_qry=$dbobject->select("SELECT distinct  `family_code` FROM `parent` where `family_code`!='' AND `parent_status`='1'");
		$parent_qry=$dbobject->select($sql);
			 while($parent_row=$dbobject->fetch_array($parent_qry))
			 {
				 if($parent_row['family_code']!="")
				 {
					$family_code[$parent_row['family_code']]=$parent_row['family_code']; 
				 }
				 
			 }	
           foreach($family_code as $fmc)
		   {
		   // echo "SELECT * FROM `parent` WHERE `family_code`='".$fmc."'";
			$parent_qry=$dbobject->select("SELECT * FROM `parent` WHERE `family_code`='".$fmc."'");
			$parent_row=$dbobject->fetch_array($parent_qry);
			$data[$i]['id']=$parent_row['id'];
        $data[$i]['m_sur_name']=$parent_row['m_sur_name'];
        $data[$i]['m_given_name']=$parent_row['m_given_name'];
        $data[$i]['f_sur_name']=$parent_row['f_sur_name'];
         $data[$i]['f_given_name']=$parent_row['f_given_name'];
			$data[$i]['family_code']=$parent_row['family_code'];
			$data[$i]['studentid']=$parent_row['studentid'];
        $data[$i]['HOUSE_NO']=$parent_row['HOUSE_NO'];
        $data[$i]['STREET']=$parent_row['STREET'];
        $data[$i]['HM_ADDRESS02']=$parent_row['HM_ADDRESS02'];
        $data[$i]['HM_ADDRESS03']=$parent_row['HM_ADDRESS03'];
        $data[$i]['HM_STATE']=$parent_row['HM_STATE'];
        $data[$i]['HM_POSTCODE']=$parent_row['HM_POSTCODE'];			
			$i++;		
		   }

    $sql2="select distinct `student`.`m_family_code` from `student_class` LEFT JOIN `student` on `student`.`sid`=`student_class`.`sid` where  `student_class`.`acyear`='".$acyear."' and `student`.`family_code`=''";
   	$sql2.=" and `s_status`='active'";
   	$sql2.=" order by FIELD(gender,'F','M'),`s_sur_name`,`sname`";
		//$parent_qry=$dbobject->select("SELECT distinct  `family_code` FROM `parent` where `family_code`!='' AND `parent_status`='1'");
		$parent_qry2=$dbobject->select($sql2);
			 while($parent_row2=$dbobject->fetch_array($parent_qry2))
			 {
				 if($parent_row2['m_family_code']!="")
				 {
					$family_code2[$parent_row2['m_family_code']]=$parent_row2['m_family_code']; 
				 }
				 
			 }	
           foreach($family_code2 as $fmc)
		   {
		   // echo "SELECT * FROM `parent` WHERE `family_code`='".$fmc."'";
			$parent_qry2=$dbobject->select("SELECT * FROM `parent` WHERE `m_family_code`='".$fmc."'");
			$parent_row2=$dbobject->fetch_array($parent_qry2);
			$data[$i]['id']=$parent_row2['id'];
        $data[$i]['m_sur_name']=$parent_row2['m_sur_name'];
        $data[$i]['m_given_name']=$parent_row2['m_given_name'];
        $data[$i]['f_sur_name']=$parent_row2['f_sur_name'];
         $data[$i]['f_given_name']=$parent_row2['f_given_name'];
			$data[$i]['family_code']=$parent_row2['family_code'];
			$data[$i]['studentid']=$parent_row2['studentid'];
        $data[$i]['HOUSE_NO']=$parent_row2['HOUSE_NO'];
        $data[$i]['STREET']=$parent_row2['STREET'];
        $data[$i]['HM_ADDRESS02']=$parent_row2['HM_ADDRESS02'];
        $data[$i]['HM_ADDRESS03']=$parent_row2['HM_ADDRESS03'];
        $data[$i]['HM_STATE']=$parent_row2['HM_STATE'];
        $data[$i]['HM_POSTCODE']=$parent_row2['HM_POSTCODE'];			
			$i++;		
		   }			 
		}
	return $data;
}
}
?>