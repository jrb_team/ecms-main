<?php
include_once('admin_class.php');


$class = $_GET['sele_class'];
$sid = $_GET['sel_student'];
$exam = $_GET['sel_exam'];

//#############retrieving student,s and school's information for report header details###########
$selschool = "select name,mob,email,affiliation_no,academic_year from schoolinfo;";
$selrs = $dbobj->select($selschool);
$selrow = $dbobj->fetch_array($selrs);
$school = $selrow['name'];
$contact = $selrow['mob'];
$academic_year=$selrow['academic_year'];
$aff_no=$selrow['affiliation_no'];
$email = $selrow['email'];

$selname = "select * from student where sid = ".$sid.";";
$result = $dbobj->select($selname);
$row = $dbobj->fetch_array($result);
$stname = $row['sname'];
$initial = $row['initial'];
$studentid = $row['studentid'];
$stud_photoid=$row['simgid'];
$parent_id=$row['parent_id'];
$dob=$row['dob'];
$adrs=$row['paddr1'];

$parent=$dbobj->selectall("parent",array("id"=>$parent_id));
$query = "select classname,division,classno from sclass where classid = ".$class.";";
$rs = mysql_query($query);
$row = mysql_fetch_array($rs);
$classname = $row['classname'];
$division = $row['division'];
$classno = $row['classno'];

$house = "select house from house INNER JOIN student_house ON student_house.house_id=house.id WHERE student_house.sid=".$sid.";";
$result1 = $dbobj->select($house);
$row1 = $dbobj->fetch_array($result1);
$huse=$row1['house'];

$club="select club_name from club INNER JOIN student_club ON student_club.club_id=club.id WHERE student_club.sid=".$sid.";";
$result2=$dbobj->select($club);
$row2=$dbobj->fetch_array($result2);
$clb=$row2['club_name'];

$admin = new Admin($classno);
$tablename=$admin->subject_table();

$form = "<tr><th style='width:200px;text-align:left'>SUBJECTS</th>";
$selsub=$dbobj->select("select * from ".$tablename." order by `sub_id` ");
$s=1;
while($sub_row=mysql_fetch_array($selsub))
{
	$sub_id[$s]=$sub_row['sub_id'];
	$sub_name[$s]=$sub_row['subject'];
	$s++;
}

$sel_exm=$dbobj->select("select * from terms where classno='".$classno."' and `exam_type`!='attendance_only'");
$t=1;
while($exm_row=$dbobj->fetch_array($sel_exm))
{
	$term_id[$t]=$exm_row['id'];
	$sub_type=$exm_row['sub_type'];
	if($sub_type=='subject_only')
	{
		$colspan="2";
	}
	elseif($sub_type=='subject_or_subhead')
	{	
	$colspan="4";
	}
$form.= "<th colspan='".$colspan."' style='text-align:center'>".strtoupper($exm_row['term_name'])."</th>";		
$t++;
}
$form.= "<th>FINAL RESULT</th></tr><tr><th style='text-align:left'>MAX.MARK</th>";
		foreach($term_id as $tid)
		{
			$max_mark=0;
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			$examtype=$term_row['exam_type'];
			if($examtype=="term")
			{
				$form.="<th style='text-align:center'>80</th><th style='text-align:center'>20</th><th style='text-align:center'>100</th>";
			}
			elseif($examtype=="mid_term")
			{
				$form.="<th style='text-align:center'>50</th>";
			}
			
			$form.="<th style='text-align:center' rowspan='2'>Grade</th>";
			
		}

$form.= "<th style='text-align=centre' rowspan='2'>PERCENTAGE</th></tr>";
$form.= "<tr><th style='text-align:left'>PASS MARK</th>";
		foreach($term_id as $tid)
		{
			$min_mark=0;
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			$examtype=$term_row['exam_type'];
			$sel_examdet=$dbobj->select("select distinct minmark from `examschedule` where `examid`='".$tid."'");
			if($examtype=="term")
			{
				$form.="<th style='text-align:center'>32</th><th style='text-align:center'>8</th><th style='text-align:center'>40</th>";
			}
			elseif($examtype=="mid_term")
			{
				$form.="<th style='text-align:center'>20</th>";
			}
		}
$form.= "</tr>";

	$form.= "";
	$form.= "";
	$i=1;
	$grant_mark=0;
	$grant_max=0;
	$grant_per=0;
	foreach($sub_id as $sub)
	{
		$g_tot=0;
		$g_max=0;
		$form.= "<tr><th>".$sub_name[$i]."</th>";	
		foreach($term_id as $tid)
		{
		$tot=0;
		$max_tot=0;
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			$exam_type=$term_row['exam_type'];
			if($sub_type=="subject_only")
			{	
				$sel_examdet=$dbobj->select("select * from `examschedule` where `examid`='".$tid."' and `classno`='".$classno."' and `sub_id`='".$sub."' and `acc_year`='".$acyear."'");
				$examdet_row=$dbobj->fetch_array($sel_examdet);
				$max_mark=$examdet_row['maxmark'];
				$min_mark=$examdet_row['minmark'];
				$sel_mark=$dbobj->select("select * from `report_card` where `sid`='".$sid."' and `classid`='".$class."' and `examid`='".$tid."' and `sub_id`='".$sub."' and `assessment`='".$exam_type."' and `ac_year`='".$acyear."'");
				$mark_row=$dbobj->fetch_array($sel_mark);
				if($mark_row['mark']<$min_mark)
				{
					$form.= "<td style='color:red;text-align:center'>".$mark_row['mark']."</td>";
				}
				else
				{
					$form.= "<td style='text-align:center'>".$mark_row['mark']."</td>";
				}
				$m_tot[$tid]=$m_tot[$tid]+$mark_row['mark'];
				$maximum_tot[$tid]=$maximum_tot[$tid]+$max_mark;
				$g_tot=$g_tot+$mark_row['mark'];
				$g_max=$g_max+$max_mark;
				$form.= "<td style='text-align:center'>".grade_calculate($mark_row['mark'],$max_mark,$classno)."</td>";
			}
			elseif($sub_type=="subject_or_subhead")
			{
				$subject=$dbobj->selectall($tablename,array("sub_id"=>$sub));
				if($subject['sub_head']=="1")
				{
					$sel_subhd=$dbobj->select("select * from `sub_heads` where `subj_id`='".$sub."' and `classno`='".$classno."'");
					$total=0;
					$max_total=0;
					while($subhd_row=$dbobj->fetch_array($sel_subhd))
					{
					
						$subhd_id=$subhd_row['id'];
						$sel_examdet=$dbobj->select("select * from `examschedule` where `examid`='".$tid."' and `classno`='".$classno."' and `sub_id`='".$sub."' and `subhead_id`='".$subhd_id."' and `acc_year`='".$acyear."'");
						$examdet_row=$dbobj->fetch_array($sel_examdet);
						$max_mark=$examdet_row['maxmark'];
						$min_mark=$examdet_row['minmark'];
						$sel_mark=$dbobj->select("select * from `report_card` where `sid`='".$sid."' and `classid`='".$class."' and `examid`='".$tid."' and `sub_id`='".$sub."' and `subhead_id`='".$subhd_id."' and `assessment`='".$exam_type."' and `ac_year`='".$acyear."'");
						$mark_row=$dbobj->fetch_array($sel_mark);
						if($mark_row['mark']<$min_mark)
						{
							$form.= "<td style='color:red;text-align:center'>".$mark_row['mark']."</td>";
						}
						else
						{
							$form.= "<td style='text-align:center'>".$mark_row['mark']."</td>";
						}
						$total=$total+$mark_row['mark'];
						$max_total=$max_total+$max_mark;
						$m_tot[$tid]=$m_tot[$tid]+$mark_row['mark'];
						$g_tot=$g_tot+$mark_row['mark'];
					}
					$g_max=$g_max+$max_total;
					$maximum_tot[$tid]=$maximum_tot[$tid]+$max_total;
				}
				else
				{
					$sel_examdet=$dbobj->select("select * from `examschedule` where `examid`='".$tid."' and `classno`='".$classno."' and `sub_id`='".$sub."' and `acc_year`='".$acyear."'");
					$examdet_row=$dbobj->fetch_array($sel_examdet);
					$max_mark=$examdet_row['maxmark'];
					$min_mark=$examdet_row['minmark'];
					$sel_mark=$dbobj->select("select * from `report_card` where `sid`='".$sid."' and `classid`='".$class."' and `examid`='".$tid."' and `sub_id`='".$sub."' and `assessment`='".$exam_type."' and `ac_year`='".$acyear."'");
					$mark_row=$dbobj->fetch_array($sel_mark);
					if($mark_row['mark']<$min_mark)
					{
						$form.= "<td style='color:red;text-align:center'>".$mark_row['mark']."</td>";
					}
					else
					{
						$form.= "<td style='text-align:center'>".$mark_row['mark']."</td>";
					}
					$form.= "<td style='text-align:center'>--</td>";
					$total=$mark_row['mark'];
					$max_total=$max_mark;
					$m_tot[$tid]=$m_tot[$tid]+$mark_row['mark'];
					$maximum_tot[$tid]=$maximum_tot[$tid]+$max_mark;
					$g_tot=$g_tot+$mark_row['mark'];
					$g_max=$g_max+$max_mark;
				}
				$form.= "<td style='text-align:center'>".$total."</td><td style='text-align:center'>".grade_calculate($total,$max_total,$classno)."</td>";
			}
					
						
		}
		
	//$form.= "<th></th></tr>";
	$round=round(($g_tot/$g_max)*100,2);
	$form.= "<th>".$round."</th></tr>";
	$grant_mark=$grant_mark+$g_tot;
	$grant_max=$grant_max+$g_max;
	$grant_per=$grant_per+$round;
	$i++;
	}


				
$form.= "<tr><th style='text-align:left'>TOTAL</th>";
		foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_only")
			{
				$form.="<td colspan='2' align='center'><b>".$m_tot[$tid]."</b></td>";
			}
			elseif($sub_type=="subject_or_subhead")
			{
				
				$sel_subhd=$dbobj->select("select distinct `head_name` from `sub_heads` where  `classno`='".$classno."'");
				$num_rows=mysql_num_rows($sel_subhd);
				$form.="<td colspan='4' align='center'><b>".$m_tot[$tid]."</b></td>";
			}
		}
$form.= "<th></th></tr>";
$form.= "<tr><th style='text-align:left'>PERCENTAGE</th>";
		foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_only")
			{
				$form.="<td colspan='2' align='center'><b>";
				if($maximum_tot[$tid]!=0)
				{
				$form.=round(($m_tot[$tid]/$maximum_tot[$tid])*100,2);
				}
				$form.="</b></td>";
			}
			elseif($sub_type=="subject_or_subhead")
			{
				
				$sel_subhd=$dbobj->select("select distinct `head_name` from `sub_heads` where  `classno`='".$classno."'");
				$num_rows=mysql_num_rows($sel_subhd);
				$form.="<td colspan='4' align='center'><b>";
				if($maximum_tot[$tid]!=0)
				{
					//$form.=$maximum_tot[$tid];
					$form.=round(($m_tot[$tid]/$maximum_tot[$tid])*100,2);
				}
				$form.="</b></td>";
			}
		}
$form.= "<th></th></tr>";
$form.= "<tr><th style='text-align:left'>GRADE</th>";
foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_only")
			{
				$form.="<td colspan='2' align='center'><b>".grade_calculate($m_tot[$tid],$maximum_tot[$tid],$classno)."</b></td>";
			}
			elseif($sub_type=="subject_or_subhead")
			{
				
				$sel_subhd=$dbobj->select("select distinct `head_name` from `sub_heads` where  `classno`='".$classno."'");
				$num_rows=mysql_num_rows($sel_subhd);
				$form.="<td colspan='4' align='center'><b>".grade_calculate($m_tot[$tid],$maximum_tot[$tid],$classno)."</b></td>";
			}
		}
$form.= "<th></th></tr>";
$form.= "<tr><th style='text-align:left'>DATE OF REPORT</th>";
foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_only")
			{
				$form.="<td colspan='2'></td>";
			}
			elseif($sub_type=="subject_or_subhead")
			{
				
				$sel_subhd=$dbobj->select("select distinct `head_name` from `sub_heads` where  `classno`='".$classno."'");
				$num_rows=mysql_num_rows($sel_subhd);
				$form.="<td colspan='4'></td>";
			}
		}
$form.= "<th></th></tr>";
$form.= "<tr><th style='text-align:left'>CLASS TEACHER</th>";
foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_only")
			{
				$form.="<td colspan='2'></td>";
			}
			elseif($sub_type=="subject_or_subhead")
			{
				
				$sel_subhd=$dbobj->select("select distinct `head_name` from `sub_heads` where  `classno`='".$classno."'");
				$num_rows=mysql_num_rows($sel_subhd);
				$form.="<td colspan='4'></td>";
			}
		}
$form.= "<th></th></tr>";
$form.= "<tr><th style='text-align:left'>PARENT</th>";
foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_only")
			{
				$form.="<td colspan='2'></td>";
			}
			elseif($sub_type=="subject_or_subhead")
			{
				
				$sel_subhd=$dbobj->select("select distinct `head_name` from `sub_heads` where  `classno`='".$classno."'");
				$num_rows=mysql_num_rows($sel_subhd);
				$form.="<td colspan='4'></td>";
			}
		}
$form.= "<th></th></tr>";

$form.= "<tr><th style='text-align:left'>HEADMASTER</th>";
foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_only")
			{
				$form.="<td colspan='2'></td>";
			}
			elseif($sub_type=="subject_or_subhead")
			{
				
				$sel_subhd=$dbobj->select("select distinct `head_name` from `sub_heads` where  `classno`='".$classno."'");
				$num_rows=mysql_num_rows($sel_subhd);
				$form.="<td colspan='4'></td>";
			}
		}
$form.= "<th></th></tr>";
//-----------------------------------------------------------------------------------------------------------------------------

$sel_term_only=$dbobj->select("select *  from `terms` where `classno`='".$classno."' and `exam_type`='term' "); 
$i=1;
while($term_only_row=$dbobj->fetch_array($sel_term_only))
{
	$term_only_id[$i]=$term_only_row['id'];
	$term_only_name[$i]=$term_only_row['term_name'];
	$i++;
}
//--------------------------------Remark-----------------------------------------------------------------------------------
$j=1;
$rem.="<table align='center' border='1' style='width:1200px;font-size:13px;border-collapse:collapse'><tr><th colspan='2' style='color:#B71D6F;'><b>REMARKS</b></th></tr>";
foreach ($term_only_id as $tid)
   {
	$rem.="<tr><td style='width:200px'><b>".strtoupper($term_only_name[$j])."</b></td>";
	$sel_remark=$dbobj->select("select * from remark where `sid`='".$sid."' and `term`='".$tid."' and `cls`='".$class."'");
	$rem_arry=$dbobj->fetch_array($sel_remark);
	$rem.="<td>".$rem_arry['remrk']."</td></tr>";
	$j++;
   }
   $rem.="</table>";
//-------------------------NON-ACADEMIC------------------------------------------------------
if($classno<=6)
	{
			$section="LP";
	}
	else
	{
			$section="HS";
	}
$sel_non_acc=$dbobj->select("select * from `performance_area` where `section`='".$section."' order by `id` ");
		while($acc_row=$dbobj->fetch_array($sel_non_acc))
		{
			$head=$acc_row['areaname'];
			foreach($term_only_id as $tid)
			{
				$term_det=$dbobj->selectall("terms",array("id"=>$tid));
				$sel_non_grade=$dbobj->select("select `grade` from `performance_evaluation` where sid='".$sid."' and  `headid`='".$acc_row['id']."' and`term`='".$tid."' and `classid`='".$class."' and `acc_year`='".$academic_year."'");
				$grade_row=$dbobj->fetch_array($sel_non_grade);
				$grade=$grade_row['grade'];
				$nonacc[$head][$term_det['term_name']]=$grade;
			}
			
		}
//------------------------------EXTRA-NON-ACADEMIC------------------------------------------------

$extra_non_acc=$dbobj->select("select * from `extra_non` where `classno`='".$classno."' order by `id` ");
		while($non_row=$dbobj->fetch_array($extra_non_acc))
		{
			$head=$non_row['head'];
			foreach($term_only_id as $tid)
			{
				$term_det=$dbobj->selectall("terms",array("id"=>$tid));
				$sel_extra_grade=$dbobj->select("select `grade` from `extra_non_grades` where sid='".$sid."' and  `headid`='".$non_row['id']."' and`term`='".$tid."' and `classid`='".$class."' and `acc_year`='".$academic_year."'");
				$grade_row=$dbobj->fetch_array($sel_extra_grade);
				$grade=$grade_row['grade'];
				$enonacc[$head][$term_det['term_name']]=$grade;
			}
		}

	  /*--------------------------------Last Page---------------------*/

				  
				 
		 
//---------------------------------------------Attendance-----------------------------------
if($classno=="12" || $classno=="14")
{

	$sel_term_for_attendance=$dbobj->select("select *  from `terms` where `classno`='".$classno."' and `term_name` IN ('I Term','II Term','III Term') "); 
}
else
{
	$sel_term_for_attendance=$dbobj->select("select *  from `terms` where `classno`='".$classno."' and `exam_type`='term' "); 
}
$i=0;
while($att_term_row=$dbobj->fetch_array($sel_term_for_attendance))
   {
	   $termid[$i]=$att_term_row['id'];
	   $t=$att_term_row['id'];
	   $t_name.="<td style='text-align:center'><b>".strtoupper($att_term_row['term_name'])."</b></td>";
	   $p=0;
	   $sel_attandance=$dbobj->select("select *  from `temp_attendace` where `sid`='".$sid."' and `term`='".$t."' and `classid`='".$class."'");
       $attandance_row=$dbobj->fetch_array($sel_attandance);
		$working_days[$t]="<td>".$attandance_row['working_days']."</td>";
		$attendance[$t]="<td>".$attandance_row['attendance']."</td>";
		if($attandance_row['working_days']!="0" && $attandance_row['working_days']!="")
		{
			$p=round(($attandance_row['attendance']/$attandance_row['working_days'])*100,2);
		}
		$percentage[$t]="<td style='text-align:center'>".$p."</td>";
		$total_attendance=$total_attendance+$attandance_row['attendance'];
		$total_workingdays=$total_workingdays+$attandance_row['working_days'];
		$i++;
   }
   //$att.="<table align='center' style='border-collapse:collapse' border='1'><tr><th colspan='4'>Attendance</th></tr><tr><td></td>".$t_name."</tr>";
   $att.="<table align='center' style='border-collapse:collapse' border='1' width='600px'><tr><td><b>COMPONENTS</b></td>".$t_name."<td><b>TOTAL</b></td></tr>";
   if($total_workingdays!="0" && $total_workingdays!="")
   {
	$total_per=round(($total_attendance/$total_workingdays)*100,2);
   }
$att.="<tr><td><b>WORKING DAYS</b></td>";
foreach ($termid as $tid)
   {
	  $att.=$working_days[$tid];
   }
$att.="<td>".$total_workingdays."</td></tr>";
$att.="<tr><td><b>ATTENDED DAYS</b></td>";
foreach ($termid as $tid)
   {
	  $att.=$attendance[$tid];
   }

$att.="<td>".$total_attendance."</td></tr>";
$att.="<tr><td><b>PERCENTAGE</b></td>";
foreach ($termid as $tid)
   {
	  $att.=$percentage[$tid];
   }
	$att.="<td>".$total_per."</td></tr>";
		
		
$att.="<tr><td style='height:16px'></td><td></td><td></td><td></td><td></td></tr>
<tr><td style='height:18px'></td><td></td><td></td><td></td><td></td></tr>
</table>";
//----------------------------------Grade Calculate------------------------------------------
function grade_calculate($val,$val2,$classno)
{
	if($val!="" && $val2!="" && $val!="0" && $val2!="0")
	{
	$ma=($val/$val2)*100;
	$sel = mysql_query("select * from grades where classno = '$classno'");
	while($rowsel = mysql_fetch_array($sel))
		{
			$min_mark = $rowsel['min_mark'];
			$min_mark = $min_mark.".00";
			$max_mark = $rowsel['max_mark'];
			$max_mark = $max_mark."00";
			$gradename = $rowsel['gradename'];
			if($ma>=$min_mark && $ma<=$max_mark )
			{
				return $gradename;
			}
		}
	}
	
}
?>
<?php
$main="<table style='border:3px solid #B71D6F'  width='100%'>
<tr><td class='ftd' width='50%' style='border:1px solid #B71D6F'>
			<table style='font-size:12px'><tr><td>
			<table border='1' class='grade' align='center' style='text-align:left;'>
				<tr><td colspan='3' align='center' style='width:300px;color:#B71D6F'><b>ACADEMIC ASSESSMENT</b></td><td colspan='2' align='center' style='color:#B71D6F'><b>NON-ACADEMIC ASSESSMENT</b></td></tr>
				<tr><td  style='width:100px'><b>MARKS</b></td><td align='left'><b>GRADE</b></td><td><b>REMARK</b></td><td><b>GRADE</b></td><td ><b>REMARK</b></td></tr>
				<tr><td  style='border-bottom-style:none;'><b>90% AND ABOVE<b></td><td  style='border-bottom-style:none;text-align:left' ><b>A+</b></td><td  style='border-bottom-style:none;'><b>EXCELLENT</b></td><td style='border-bottom-style:none;'><b>A</b></td><td  style='border-bottom-style:none;'><b>VERY GOOD</b></td></tr>
				<tr><td  style='border-top-style:none;border-bottom-style:none;'><b>80 TO 89%<b></td><td  style='border-top-style:none;border-bottom-style:none;text-align:left'><b>A<b></td><td  style='border-top-style:none;border-bottom-style:none;'><b>V.GOOD</b></td><td style='border-bottom-style:none;border-top-style:none;'><b>B</b></td><td  style='border-top-style:none;border-bottom-style:none;'><b>GOOD</b></td></tr>
				<tr><td style='border-top-style:none;border-bottom-style:none;'><b>70 TO 79%<b></td><td  style='border-top-style:none;border-bottom-style:none;text-align:left'><b>B+</b></td><td style='border-top-style:none;border-bottom-style:none;'><b>GOOD</b></td><td style='border-bottom-style:none;border-top-style:none;'><b>C</b></td><td style='border-top-style:none;border-bottom-style:none;'><b>AVERAGE</b></td></tr>
				<tr><td  style='border-top-style:none;border-bottom-style:none;'><b>60 TO 69%<b></td><td  style='border-top-style:none;border-bottom-style:none;text-align:left'><b>B</b></td><td  style='border-top-style:none;border-bottom-style:none;'><b>FAIR</b></td><td style='border-bottom-style:none;border-top-style:none;'><b>D</b></td><td  style='border-top-style:none;border-bottom-style:none;'><b>POOR</b></td></tr>
				<tr><td  style='border-top-style:none;border-bottom-style:none;'><b>50 TO 59%<b></td><td  style='border-top-style:none;border-bottom-style:none;text-align:left'><b>C+</b></td><td  style='border-top-style:none;border-bottom-style:none;'><b>AVERAGE</b></td><td style='border-bottom-style:none;border-top-style:none;'></td><td  style='border-top-style:none;border-bottom-style:none;'></td></tr>
				<tr><td  style='border-top-style:none;border-bottom-style:none;'><b>40 TO 49%<b></td><td  style='border-top-style:none;border-bottom-style:none;text-align:left'><b>C</b></td><td  style='border-top-style:none;border-bottom-style:none;'><b>SATISFACTORY</b></td><td style='border-bottom-style:none;border-top-style:none;'></td><td  style='border-top-style:none;border-bottom-style:none;'></td></tr>
				<tr><td  style='border-top-style:none;'><b>BELOW 40%<b></td><td style='border-top-style:none;'><b>D</b></td><td style='border-top-style:none;text-align:left'><b>FAILED</b></td><td align='center' style='border-top-style:none;'></td><td align='center' style='border-top-style:none;'></td></tr>
			</table>
			<tr><td>
			<table border='1' class='grade' align='center'>
					<tr><td align='center' style='font-size:15px;border-bottom-style:none;color:#B71D6F' colspan='2'><b><u>CRITERIA FOR PROMOTION</u></b></td></tr>
					 <tr><td align='left' style='border-bottom-style:none;border-right-style:none;border-top-style:none;' valign='top'><b>01.</td><td style='border-bottom-style:none;border-left-style:none;border-top-style:none;'><b>THE ACADEMIC YEAR IS SEGMENTED AS TERM I, TERM II AND TERM III.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;' valign='top'><b>02.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>ALL SUBJECTS HAVE COMPONENTS OF INTERNAL ASSESSMENT ON THE BASIS OF ASSIGNMENTS/PROJECTS/CLASS TESTS/PRACTICAL WORKS.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;' valign='top'><b>03.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>NORMALLY A STUDENT SHOULD GAIN SEPARATE MINIMUM PASS MARK IN EACH SUBJECT. HE/SHE MAY BE CONSIDERED FOR PROMOTION EVEN IF HE/SHE FAILS IN ONE SUBJECT OTHER THAN MORAL SCIENCE, ENGLISH AND A POOR GRADE IN SUPW & C S.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;' valign='top'><b>04.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>PROMOTION TO A HIGHER CLASS IS BASED ON ATTENDANCE, CUMULATIVE PASS PERCENTAGE IN ALL MID TERM, TERMINAL EXAMINATIONS AND A GOOD GRADE IN NON ACADEMICS. </b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;' valign='top'><b>05.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>PUPILS WHO ABSENT THEMSELVES FROM EXAMINATIONS WITHOUT GRAVE REASONS WILL BE CONSIDERED AS FAILED. NO RE TEST WILL BE CONDUCTED FOR THE ABSENTEES.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;' valign='top'><b>06.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>SERIOUS ACTION INCLUDING SUSPENSION AND DISMISSAL WILL BE TAKEN AGAINST PUPILS WHO RESORT TO UNFAIR MEANS DURING EXAMINATIONS.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;' valign='top'><b>07.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>PARENTS/GUARDIANS SHOULD FOLLOW UP THE WARD’S PROGRESS IN ACADEMICS AND NON ACADEMICS AND TAKE NECESSARY STEPS IN ORDER TO ACHIEVE FURTHER IMPROVEMENT.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;' valign='top'><b>08.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>THE PROGRESS REPORT CARD SHOULD BE COLLECTED, ACKNOWLEDGED AND RETURNED ON THE STIPULATED DATES.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;'valign='top'><b>09.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>THE SCHOOL RESERVES THE RIGHT TO COMPEL PARENTS/GUARDIANS TO WITHDRAW THEIR WARDS IF THE PROGRESS IN ACADEMICS IS UNSATISFACTORY, ATTENDANCE IS IRREGULAR OR THE CONDUCT IS HARMFUL TO OTHER STUDENTS. NO STUDENT CAN REPEAT THE YEAR MORE THAN TWO YEARS.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-right-style:none;' valign='top'><b>10.</td><td style='border-top-style:none;'><b>THE DECISION OF THE PRINCIPAL IN ALL MATTERS IS FINAL AND BINDING.</b></td></tr>
			</table>
			</td></tr>
			<tr><td>
		<div style='text-align:center;font-size:11px;' width='100px'><b>I, the undersigned agree to abide by these norms and other rules with regard to examinations and promotion procedures.</b></div>

		</td></tr>
		<tr>
		<td>
		<table class='grade' style='border:none;'>
		<tr><td width='50%' style='text-align:left;'><table><tr><td style='width:220px;height:30px;border-style: solid;border-width: 1px;'></td></tr></table></td><td style='text-align:right;'><table><tr><td style='width:220px;height:30px;border-style: solid;border-width: 1px;'></td></tr></table></td></tr>
		<tr><td width='50%' style='text-align:left;'><b>PLACE</b></td><td style='text-align:right;'><b>NAME OF PARENT/GUARDIAN</b></td></tr>
		<tr><td width='50%' style='text-align:left;'><table><tr><td style='width:220px;height:30px;border-style: solid;border-width: 1px;'></td></tr></table></td><td style='text-align:right;'><table><tr><td style='width:220px;height:30px;border-style: solid;border-width: 1px;'></td></tr></table></td></tr>
		<tr><td width='50%' style='text-align:left;'><b>DATE</b></td><td style='text-align:right;'><b>SIGNATURE OF PARENT/GUARDIAN</b></td></tr>
		</table>
		</td>
		</tr>
			</table>
</td><td class='ftd' valign='top' width='50%' style='border:1px solid #B71D6F'>
<table  style='margin-top:5px auto;'>
	 <tr>
		<td align='center' colspan='2'><img src='../img/donbosco.jpg' style='width:120px' /></td>
	 </tr>
	 <tr>
		<td colspan='2' align='center' style='font-size:21px; color:#B71D6F;'><b>DON BOSCO SENIOR SECONDARY SCHOOL</b></td>
	</tr>
	<tr>
		<td align='center' colspan='2' align='center' style='font-size:16px;'><b>(I.C.S.E & I.S.C.KE-051/2000)<b></td>
	</tr>	
	<tr>
		<td align='center' colspan='2' align='center' style='font-size:16px;'><b>VADUTHALA, KOCHI-682 023</b></td>
	</tr>
	<tr>
		<td align='center' colspan='2' align='center' style='font-size:16px;'><b>PHONE:H.S:2435308, L.P: 2436738, PRINCIPAL:2436602</b></td>
	</tr>
	<tr>
		<td align='center' colspan='2' align='center' style='font-size:15px;'><b>E-MAIL: sdbschool@gmail.com, info@donboscoschoolvaduthala.com</b></td>
	</tr>
	<tr>
		<td align='center' colspan='2' align='center'  style='font-size:16px;'><b>WEBSITE : www.donboscoschoolvaduthala.com</b></td>
	</tr>
	<tr><td colspan='2'></td></tr>
	<tr><td align='center' colspan='2' style='font-size:21px; color:#B71D6F;' align='center'><b>PROGRESS REPORT</b></td></tr>
	<tr><td align='center' colspan='2' align='center' style='font-size:21px;color:#B71D6F;'><b>".$academic_year."</b></td></tr>
	<tr><td align='center' colspan='2' ><div class='photo'>";
	if($stud_photoid!="0")
		{
		$sel_photo=$dbobj->select("select * from `student_img` where `id`='".$stud_photoid."'");
		$photo_row=$dbobj->fetch_array($sel_photo);
		$imagebytes=$photo_row['image'];
		$type=$photo_row['type'];
		if($type=='')
		{
		$image= "<img  boreder='0' src='../imagetmp/default.jpg' width='130' height='130' alt='Photo' />";
		}
		else
		{
			$fh=fopen("../imagetmp/tmp".$type."","w");
			fwrite($fh,$imagebytes,strlen($imagebytes));
			$image="<img  border='0' src='../imagetmp/tmp".$type."' width='100' height='100' alt='Photo' />";
		}
		}
		else
		{
			$image= "<img  boreder='0' src='../imagetmp/default.jpg' width='100' height='100' alt='Photo' />";
		}
$main.=$image."</div></td></tr>
<tr><td colspan='2' align='center' style='font-size:20px;color:#B71D6F;'><b>".strtoupper($stname)." ".strtoupper($initial)."</b></td></tr>
<tr><td align='center'>
<table  width='500px' border='0' style='margin-top:40px;'>
		<tr><td ><b>CLASS: ".$classname."</b></td><td width='150px'><b>DIVISION: ".$division."</b></td><td width='150px'><b>ROLL NUMBER:  </b></td><td style='text-align:right'><b>ID.NO : ".$studentid."</b></td></tr>	
		</table>
		<table width='500px'>
		<tr><td><b>DATE OF BIRTH: ".$dob."</b></td></tr>	
		</table >
		<table width='500px'>
		<tr><td width='90px'><b>HOUSE: ".$huse."</b></td><td width='200px'><b>CLUB: ".$clb."</b></td><td width='200px'><b>SUPW: </b></td></tr>	
		</table>
		<table width='500px'>
		<tr><td ><b>FATHER’S  NAME: ".strtoupper($parent['fname'])."<b></td><td style='text-align:right'><b>MOTHER'S NAME: ".strtoupper($parent['mname'])."<b></td></tr>	
		<tr><td colspan='2'><b>ADDRESS: ".$adrs."</b></td></tr>
		</table>
		<table width='500px'>
		<tr><td colspan='2' width='350px'></td><td><b>PIN: </b></td></tr>
		</table>
		<table width='500px' border='0'>
		<tr><td width='350px'><b>PHONE NUMBER LAND: ".$parent['phone']."</b></td><td style='text-align:right'><b>MOBILE: ".$parent['fmob']."</b></td></tr>	
		</table>
		<table width='500px' style='text-align:center'>
		<tr><td></td><td></td><td></td></tr>
		<tr><td></td><td></td><td></td></tr>
		<tr><td></td><td></td><td></td></tr>
		<tr><td></td><td></td><td></td></tr>
		<tr><td style='text-align:left'><table><tr><td style='width:150px;height:30px;border-style: solid;border-width: 1px;'></td></tr></table></td><td><table><tr><td style='width:150px;height:30px;border-style: solid;border-width: 1px;'></td></tr></table></td><td style='text-align:right'><table><tr><td style='width:150px;height:30px;border-style: solid;border-width: 1px;'></td></tr></table></td></tr>
		 <tr><td><b>FATHER'S SIGN</b></td><td><b>MOTHER'S SIGN</b></td><td><b>GUARDIAN'S SIGN</b></td></tr>
		</table>
</td></tr></table>
</td></tr>
</table>
</td></tr>
</table>
<table width='100%' style='margin-top:5px;border-collapse:collapse'>
	<tr>
		<td style='text-align:left;color:#B71D6F' width='25%' bgcolor='#E9BBD4'>DBSSS VADUTHALA</td>
		<td style='text-align:right;color:#B71D6F;border-right: solid #FFFFFF;' width='25%' bgcolor='#E9BBD4'>PROGRESS REPORT - ".$classname."</td>
		<td style='text-align:left;color:#B71D6F;' width='25%' bgcolor='#E9BBD4'>DBSSS VADUTHALA</td>
		<td style='text-align:right;color:#B71D6F' width='25%' bgcolor='#E9BBD4'>PROGRESS REPORT</td>
		</tr>
</table>";
$second="<table style='border:3px solid #B71D6F;border-collapse:collapse'><tr><td><table align='center' border='1' style='border-collapse:collapse;width:100%;font-size:12px'>
<tr><th style='text-align:center;font-size:14px;color:#B71D6F;' colspan='18'><b>ACADEMIC PERFORMANCE</b> </th></tr>".$form."
<tr><th style='width:50%;font-size:14px;color:#B71D6F;' colspan='9'><b>NON-ACADEMIC PERFORMANCE</b></th><th style='width:50%;font-size:14px;color:#B71D6F;' colspan='9'><b>NON ACADEMIC PERFORMANCE</b></th></tr>
<tr ><td valign='top' colspan='9'><table border='1' style='border-collapse:collapse;' width='700px'><tr><th style='text-align:left;'>COMPONENTS</th><th>I TERM</th><th>II TERM</th><th>III TERM</th><th style='text-align:left;'>COMPONENTS</th><th>I TERM</th><th>II TERM</th><th>III TERM</th></tr>
																									<tr><td><b>CONDUCT</b></td><td>".$nonacc['CONDUCT']['I Term']."</td><td>".$nonacc['CONDUCT']['II Term']."</td><td>".$nonacc['CONDUCT']['III Term']."</td><td><b>GROUPS&MOVEMENTS</b></td><td>".$nonacc['GROUPS & MOVEMENTS']['I Term']."</td><td>".$nonacc['GROUPS & MOVEMENTS']['II Term']."</td><td>".$nonacc['GROUPS & MOVEMENTS']['III Term']."</td></tr>
																									<tr><td><b>RESPONSIBILITY</b></td><td>".$nonacc['RESPONSIBILITY']['I Term']."</td><td>".$nonacc['RESPONSIBILITY']['II Term']."</td><td>".$nonacc['RESPONSIBILITY']['III Term']."</td><td><b>LISTENING ENGLISH</b></td><td>".$enonacc['LISTENING ENGLISH']['I Term']."</td><td>".$enonacc['LISTENING ENGLISH']['II Term']."</td><td>".$enonacc['LISTENING ENGLISH']['III Term']."</td></tr>
																									<tr><td><b>PUNCTUALITY</b></td><td>".$nonacc['PUNCTUALITY']['I Term']."</td><td>".$nonacc['PUNCTUALITY']['II Term']."</td><td>".$nonacc['PUNCTUALITY']['III Term']."</td><td><b>SPEAKING ENGLISH</b></td><td>".$enonacc['SPEAKING ENGLISH']['I Term']."</td><td>".$enonacc['SPEAKING ENGLISH']['II Term']."</td><td>".$enonacc['SPEAKING ENGLISH']['III Term']."</td></tr>
																									<tr><td><b>DISCIPLINE</b></td><td>".$nonacc['DISCIPLINE']['I Term']."</td><td>".$nonacc['DISCIPLINE']['II Term']."</td><td>".$nonacc['DISCIPLINE']['III Term']."</td><td><b>READING ENGLISH</b></td><td>".$enonacc['READING ENGLISH']['I Term']."</td><td>".$enonacc['READING ENGLISH']['II Term']."</td><td>".$enonacc['READING ENGLISH']['III Term']."</td></tr>
																									<tr><td><b>LEADERSHIP</b></td><td>".$nonacc['LEADERSHIP']['I Term']."</td><td>".$nonacc['LEADERSHIP']['II Term']."</td><td>".$nonacc['LEADERSHIP']['III Term']."</td><td><b>LISTENING MALAYALAM</b></td><td>".$enonacc['LISTENING MALAYALAM']['I Term']."</td><td>".$enonacc['LISTENING MALAYALAM']['II Term']."</td><td>".$enonacc['LISTENING MALAYALAM']['III Term']."</td></tr>
																									<tr><td><b>E C A</b></td><td>".$nonacc['ECA']['I Term']."</td><td>".$nonacc['ECA']['II Term']."</td><td>".$nonacc['ECA']['III Term']."</td><td><b>SPEAKING MALAYALAM</b></td><td>".$enonacc['SPEAKING MALAYALAM']['I Term']."</td><td>".$enonacc['SPEAKING MALAYALAM']['II Term']."</td><td>".$enonacc['SPEAKING MALAYALAM']['III Term']."</td></tr>
																									<tr><td><b>AQUATICS</b></td><td>".$nonacc['AQUATICS']['I Term']."</td><td>".$nonacc['AQUATICS']['II Term']."</td><td>".$nonacc['AQUATICS']['III Term']."</td><td><b>READING  MALAYALAM</b></td><td>".$enonacc['READING  MALAYALAM']['I Term']."</td><td>".$enonacc['READING  MALAYALAM']['II Term']."</td><td>".$enonacc['READING  MALAYALAM']['III Term']."</td></tr>
																									<tr><td><b>SUPW & C.SERVICE</b></td><td>".$nonacc['SUPW & C.SERVICE']['I Term']."</td><td>".$nonacc['SUPW & C.SERVICE']['II Term']."</td><td>".$nonacc['SUPW & C.SERVICE']['III Term']."</td><td><b>LISTENING HINDI</b></td><td>".$enonacc['LISTENING HINDI']['I Term']."</td><td>".$enonacc['LISTENING HINDI']['II Term']."</td><td>".$enonacc['LISTENING HINDI']['III Term']."</td></tr>
																																										
																									</table>";

$second.="</td><td colspan='9'><table border='1' style='border-collapse:collapse;' width='650px'>
<tr><th style='text-align:left;'>COMPONENTS</th><th>I TERM</th><th>II TERM</th><th>III TERM</th></tr>
<tr><td><b>SPEAKING HINDI</b></td><td>".$enonacc['SPEAKING HINDI']['I Term']."</td><td>".$enonacc['SPEAKING HINDI']['II Term']."</td><td>".$enonacc['SPEAKING HINDI']['III Term']."</td></tr>
<tr><td><b>READING HINDI</b></td><td>".$enonacc['READING HINDI']['I Term']."</td><td>".$enonacc['READING HINDI']['II Term']."</td><td>".$enonacc['READING HINDI']['III Term']."</td></tr>
</table>
<table  style='border-collapse:collapse;' width='650px' border='1'>
<tr><th colspan='5' style='font-size:14px;height:33px'>ATTENDANCE</th></tr>
<tr><th style='text-align:left;'>COMPONENTS</th><th style='width:70px'>I TERM</th><th style='width:70px'>II TERM</th><th style='width:70px'>III TERM</th><th style='width:70px'>TOTAL</th></tr>
<tr><td><b>WORKING DAYS</b></td><td></td><td></td><td></td><td></td></tr>
<tr><td><b>ATTENDED DAYS</b></td><td></td><td></td><td></td><td></td></tr>
<tr><td><b>PERCENTAGE</b></td><td></td><td></td><td></td><td></td></tr>
</table>
</td></tr>";
$second.="<tr><td colspan='18' valign='top' style='border-bottom-style:none;'>".$rem."</td></tr><br><br>
<tr><th colspan='18' style='border-top-style:none;border-bottom-style:none;'></th></tr>
<tr><th colspan='18' style='border-top-style:none;border-bottom-style:none;'></th></tr>
<tr><th colspan='18' style='border-top-style:none;border-bottom-style:none;'></th></tr>
<tr><th colspan='18' style='border-top-style:none;border-bottom-style:none;'></th></tr>
<tr><th colspan='18' style='border-top-style:none;border-bottom-style:none;'></th></tr>
<tr><th colspan='18' style='border-top-style:none;'><b>SIGNATURE OF CLASS TEACHER &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SCHOOL SEAL &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SIGNATURE OF HEADMASTER</b></th></tr>
</table></td></tr></table>
<table width='100%' bgcolor='#E9BBD4' style='margin-top:5px'><tr><td width='50%' style='text-align:left;border-right: solid #E9BBD4; color:#B71D6F'>DBSSS VADUTHALA</td><td width='50%' style='text-align:right;color:#B71D6F'>PROGRESS REPORT</td></tr></table>";
?>
<?php
include("../MPDF/mpdf.php");
 
$mpdf=new mPDF(); 

$mpdf->SetDisplayMode('fullpage','two');

//$mpdf->mirrorMargins = 1;
// LOAD a stylesheet
$stylesheet = file_get_contents('../css/progress_final1.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->setHeader();	// Clear headers before adding page
$mpdf->AddPage('A4','','','','',3,3,2,2,3,3);
$mpdf->WriteHTML($main);
$mpdf->AddPage('A4','','','','',3,3,10,3,3,3);
$mpdf->WriteHTML($second);
$mpdf->setHeader();	// Clear headers before adding page

$mpdf->Output('mpdf.pdf','I');
exit;
?>