<?php
include_once('createdb.php');
include_once('admin_class.php');
class Academic
{
	function get_subject($table)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$select=$dbobj->select("select * from `".$table."` order by `sub_id`");
		$i=1;
		while($row=$dbobj->fetch_array($select))
		{
			$subject[$i]['id']=$row['sub_id'];
			$subject[$i]['subject']=$row['subject'];
			$subject[$i]['sub_head']=$row['sub_head'];
			$subject[$i]['input_type']=$row['input_type'];
			$subject[$i]['opn_subject']=$row['opn_subject'];
			$i++;
		}
		return $subject;
	}
	function get_subhead($classno,$subid)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$subhd=$dbobj->select("select * from sub_heads where subj_id='".$subid."' and classno='".$classno."'");
		$i=0;
		while($subhd_row = mysql_fetch_array($subhd))
		{
		$subhead[$i]['id']=$subhd_row['id'];
		$subhead[$i]['head_name']=$subhd_row['head_name'];
		$i++;
		}
		return $subhead;
	}
	function get_subject_and_subhead($subject_table,$classno)
	{
		$subjects=$this->get_subject($subject_table);
		$i=1;
		foreach($subjects as $sub)
		{
			if($sub['sub_head']==1)
			{
			$subhead=$this->get_subhead($classno,$sub['id']);
			foreach($subhead as $subh)
			{
				$subject[$i]['id']=$subh['id'];
				$subject[$i]['subject']=$subh['head_name'];
				$i++;
			}
			}
			else
			{
			$subject[$i]['id']=$sub['id'];
			$subject[$i]['subject']=$sub['subject'];
			$i++;
			}
			
		}
		return $subject;
	}
	function get_opnsub_stud($classid,$sid,$table)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$i=0;
		
		$sql=$dbobj->select("select opn_subject_stud.sub_id,".$table.".subject,".$table.".sub_head from opn_subject_stud left join ".$table." on opn_subject_stud.sub_id=".$table.".sub_id  where opn_subject_stud.class_id='$classid' and opn_subject_stud.stud_id='$sid' order by `sub_id`");
		while($row=$dbobj->fetch_array($sql))
		{
			$subject[$i]['id']=$row['sub_id'];
			$subject[$i]['subject']=$row['subject'];
			$subject[$i]['sub_head']=$row['sub_head'];
			$i++;
		}
		return $subject;
	}
	function get_exam($classno)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$select=$dbobj->select("select * from `terms` where `classno`='".$classno."' and `exam_type`='mid_term' order by `term_name`");
		$i=1;
		while($row=$dbobj->fetch_array($select))
		{
			$check=$dbobj->select("select * from `par_progress` where `classno`='".$classno."' and `term_id`='".$row['id']."'");
			$numrows=mysql_num_rows($check);
			//if($numrows==1)
			//{
				$terms[$i]['id']=$row['id'];
				$terms[$i]['classno']=$row['classno'];
				$terms[$i]['term_name']=$row['term_name'];
				$terms[$i]['class_test']=$row['class_test'];
				$i++;
			//}
		}
		return $terms;
	}
	function get_examtype($exam)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$select=$dbobj->select("select * from `terms` where `id`='".$exam."' order by `id`");
		$row=$dbobj->fetch_array($select);
		return $row['class_test'];
		
	}
	function get_assm($classno,$exam,$sub)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$i=1;
		//echo "select `fa`,sum(max_mark) as max_mark from `assessment` where `classno`='".$classno."' and `term_id`='".$exam."' and `sub_id`='".$sub."' group by `fa`";
		$select=$dbobj->select("select `fa`,`assesment`,sum(max_mark) as max_mark from `assessment` where `classno`='".$classno."' and `term_id`='".$exam."' and `sub_id`='".$sub."' group by `fa`");
		while($row=$dbobj->fetch_array($select))
		{
		$assm[$i]['fa']=$row['fa'];
		$assm[$i]['assesment']=$row['assesment'];
		$assm[$i]['max_mark']=$row['max_mark'];
		$i++;
		}
		return $assm;
				
	}
	function get_assm_ind($classno,$exam,$sub)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$i=1;
		$select=$dbobj->select("select * from `assessment` where `classno`='".$classno."' and `term_id`='".$exam."' and `sub_id`='".$sub."'");
		while($row=$dbobj->fetch_array($select))
		{
		$assm[$i]['id']=$row['id'];
		$assm[$i]['fa']=$row['fa'];
		$assm[$i]['max_mark']=$row['max_mark'];
		$i++;
		}
		return $assm;
	}
	function get_fa_by_term($classno,$term)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$i=1;
		$select=$dbobj->select("select distinct `fa` from `assessment` where `classno`='".$classno."' and `term_id`='".$term."' group by `fa`");
		while($row=$dbobj->fetch_array($select))
		{
		$fa[$i]['fa']=$row['fa'];
		$i++;
		}
		return $fa;
	}
	function get_maxmark($classno,$subid,$acyear,$examid)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$sql = $dbobj->select("select * from examschedule where examid = '$examid' and classno = '$classno' and sub_id = '$subid'");
		$row=$dbobj->fetch_array($sql);
		return $max_mark=$row['maxmark'];
	}
	function get_minmark($classno,$subid,$acyear,$examid)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$sql = $dbobj->select("select * from examschedule where examid = '$examid' and classno = '$classno' and sub_id = '$subid'");
		$row=$dbobj->fetch_array($sql);
		return $min_mark=$row['minmark'];
	}
	function get_mark_subject($sid,$classid,$classno,$acyear,$sub,$assm,$term,$marktable)
	{
	$dbobj = new DB();
	$dbobj->getCon();	
   $sql_clnm=$dbobj->select("select * from sclass where classid ='".$classid."'");
   $row_clnm=$dbobj->fetch_array($sql_clnm);
   $classno=$row_clnm['classno'];
   $sel_term=$dbobj->select("select * from terms where classno='".$classno."' and id='".$term."'");
   $term_row=$dbobj->fetch_array($sel_term);
   $assm=$term_row['exam_type'];
   //$admin=new Admin($classno);
   //$subject_table=$admin->subject_table();
  // $mark_table=$admin->mark_table()	;
	$sel_mark=$dbobj->select("select * from report_card where `sid`='".$sid."' and `examid`='".$term."' and `sub_id`='".$sub."' and `classid`='".$classid."' and `assessment`='".$assm."' and `ac_year`='".$acyear."'");
    $mrk_row=$dbobj->fetch_array($sel_mark);
    return $mrk_row['mark'];
	}
	function get_mark_subject_classtest($sid,$classid,$classno,$acyear,$sub,$term,$marktable)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$mark=0;
		//echo "select * from ".$marktable." where classid = '$classid' and examid = '$term' and sid = '$sid' and sub_id = '$sub'";
		$selmark = $dbobj->select("select * from ".$marktable." where classid = '$classid' and examid = '$term' and sid = '$sid' and sub_id = '$sub'");
		while($row=$dbobj->fetch_array($selmark))
		{
		$mark=$mark+$row['mark'];
		}
		return $mark;
	}
	function get_mark_subject_910($sid,$classid,$classno,$acyear,$sub,$assm,$term,$marktable)
	{
		
		$dbobj = new DB();
		$dbobj->getCon();
		$admin = new Admin($classno);
		$i=1;
		$tasm=0;
		foreach($assm as $ass)
		{
		$fa=$ass['fa'];
		if($ass['fa']=="FA 1" || $ass['fa']=="FA 2")
		{
			$asm=$ass['max_mark'];	
			if($asm=="10")
			{
			$selmark =$dbobj->select("select * from ".$marktable." where sid='".$sid."' and sub_id = '".$sub."' and assessment = '".$ass['fa']."' and assessment_id = '".$ass['id']."' and classid = '".$classid."'");
			while($row=$dbobj->fetch_array($selmark))
			{
				$i++;
				$m=$row['mark'];
				$tmark[$fa][$i]=$m;
				//echo $tmark[$fa][$i]=$m."-".$fa."</br>";
				$i++;
				
			}
			}
			if($asm=="5")
			{
			//$tm=0;
			 $selmark =$dbobj->select("select * from ".$marktable." where sid='".$sid."' and sub_id = '".$sub."' and assessment = '".$ass['fa']."' and assessment_id = '".$ass['id']."' and classid = '".$classid."'");
			 while($row=$dbobj->fetch_array($selmark))
			{
			 $ti=$i;
				$m=$row['mark'];
				$tmark[$fa][$ti]=$tmark[$fa][$ti]+$m;
			}
			}
			if(!empty($tmark[$fa]))
			{
			$mark[$fa]=max($tmark[$fa]);
			}
			else
			{
			$mark[$fa]=0;
			}
			}
			elseif($ass['fa']=="SA 1" || $ass['fa']=="SA 2")
			{
			 $tasm=$tasm+$ass['max_mark'];	
			 $selmark =$dbobj->select("select * from ".$marktable." where sid='".$sid."' and sub_id = '".$sub."' and assessment = '".$ass['fa']."' and assessment_id = '".$ass['id']."' and classid = '".$classid."'");
			 while($row=$dbobj->fetch_array($selmark))
			 {
			
			 $mark[$fa]=$mark[$fa]+$row['mark'];
			 }
			}

		}
		
		return $mark;
		}
	function get_mark_subject_classtest_hss($sid,$classid,$classno,$acyear,$sub,$term,$marktable)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$mark=0;
		//echo "select * from ".$marktable." where term = '$term' and stud_id = '$sid' and sub_id = '$sub'";
		$selmark = $dbobj->select("select * from ".$marktable." where term = '$term' and stud_id = '$sid' and sub_id = '$sub'");
		while($row=$dbobj->fetch_array($selmark))
		{
		$mark=$mark+$row['mark'];
		}
		return $mark;
	}
		function get_percentage($total,$mtotal)
		{
			$percent = 0;
			$percent = ($total/$mtotal)*100;
			$percent = sprintf ("%.2f", $percent);
			return $percent;
		}
		function get_grade($classno,$total_mark)
		{
		$sel = mysql_query("select * from grades where classno = '$classno'");
		while($rowsel = mysql_fetch_array($sel))
		{
			$min_mark = $rowsel['min_mark'];
			$min_mark = $min_mark.".00";
			$max_mark = $rowsel['max_mark'];
			$max_mark = $max_mark."00";
			$gradename = $rowsel['gradename'];
			
			if($total_mark>=$min_mark && $total_mark<=$max_mark )
			{
				return $gradename;
			}
		}
		}
		function select_Grade($tablemark,$classid,$classno,$term,$stud_id,$ss,$opn_sub)
		{
			$tab = mysql_query("select * from ".$tablemark." where `term` = '".$_GET['sel_exam']."' and `stud_id` = '$stud_id' and sub_id = '$ss'");

			$rowtab = mysql_fetch_array($tab);
			return $rowtab['mark'];
		}
		function get_studentdetFromReportCard($classid,$place,$acyear,$marktable)
		{
			$sql="select distinct `student`.`sid`,`student`.`sname` from `$marktable` LEFT JOIN `student` ON `student`.`sid`=`$marktable`.`sid` where `student`.`classid`='".$classid."'  and `student`.`place`='".$place."' and `$marktable`.`acyear`='".$acyear."' order by `student`.`sname`";
			$result = mysql_query($sql);
			$i=0;
			while($row=mysql_fetch_array($result))
			{
			$student[$i]['sid']=$row['sid'];
			$student[$i]['sname']=$row['sname'];
			$i++;
			}
			return $student;
		}
	function get_mark_subject_classtest_with_maxmark($sid,$classid,$classno,$acyear,$sub,$term,$marktable)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$mark['mark']=0;
		$mark['maxmark']=0;
		//echo "select * from ".$marktable." where classid = '$classid' and examid = '$term' and sid = '$sid' and sub_id = '$sub'";
		$selmark = $dbobj->select("select * from ".$marktable." where classid = '$classid' and examid = '$term' and sid = '$sid' and sub_id = '$sub'");
		while($row=$dbobj->fetch_array($selmark))
		{
		$mark['mark']=$mark['mark']+$row['mark'];
		$mark['maxmark']=$mark['maxmark']+$row['max_mark'];
		}
		return $mark;
	}
	function GetMarkTerm6_9_with_maxmark($sid,$subid,$classid,$exam,$acyear,$marktable)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$sel_mark=$dbobj->select("select * from ".$marktable." where sid='".$sid."' and sub_id = '".$subid."' and classid = '".$classid."' and `term`='".$exam."' and `acyear`='".$acyear."'");
		//$selmark =$dbobj->select("select * from ".$marktable." where sid='".$sid."' and sub_id = '".$sub."' and assessment = '".$ass['fa']."' and assessment_id = '".$ass['id']."' and classid = '".$classid."'");
		while($row=$dbobj->fetch_array($sel_mark))
		{
			$mark['mark']=$mark['mark']+$row['mark'];
			$mark['maxmark']=$mark['maxmark']+$row['max_mark'];
		}
		return $mark;
	}
	function get_studentdetFromReportCard_hss($classid,$place,$acyear,$marktable)
		{

			$sql="select distinct `student`.`sid`,`student`.`sname` from `$marktable` LEFT JOIN `student` ON `student`.`sid`=`$marktable`.`stud_id` where `student`.`classid`='".$classid."'  and `student`.`place`='".$place."' and `$marktable`.`acyear`='".$acyear."' order by `student`.`sname`;";
			$result = mysql_query($sql);
			$i=0;
			while($row=mysql_fetch_array($result))
			{
			$student[$i]['sid']=$row['sid'];
			$student[$i]['sname']=$row['sname'];
			$i++;
			}
			return $student;
		}
	function termFromReportCard($acyear,$sid,$marktable)
	{
		$sql="select distinct `examid` from ".$marktable." where `acyear`='".$acyear."' and sid='".$sid."'";
		$result = mysql_query($sql);
		$i=0;
		while($row=mysql_fetch_array($result))
		{
			$sel_term=mysql_query("select * from `terms` where `id`='".$row['examid']."'");
			$term_row=mysql_fetch_array($sel_term);
			$term[$i]['id']=$term_row['id'];
			$term[$i]['term_name']=$term_row['term_name'];
			$term[$i]['class_test']=$term_row['class_test'];
		 $i++;	
		}
		return $term;
	}
	function termFromReportCardHSS($acyear,$sid,$marktable)
	{
		$sql="select distinct `term` from ".$marktable." where `acyear`='".$acyear."' and stud_id='".$sid."'";
		$result = mysql_query($sql);
		$i=0;
		while($row=mysql_fetch_array($result))
		{
			$sel_term=mysql_query("select * from `terms` where `id`='".$row['term']."'");
			$term_row=mysql_fetch_array($sel_term);
			$term[$i]['id']=$term_row['id'];
			$term[$i]['term_name']=$term_row['term_name'];
			$term[$i]['class_test']=$term_row['class_test'];
		 $i++;	
		}
		return $term;
	}
	function subjectFromReportCard($acyear,$sid,$marktable,$term)
	{
		$sql="select distinct `subname` from ".$marktable." where `acyear`='".$acyear."' and `examid`='".$term."' and sid='".$sid."' order by `sub_id`";
		$result = mysql_query($sql);
		
		while($row=mysql_fetch_array($result))
		{
			$subid=$row['sub_id'];
			$subname=$row['subname'];
		}
	}
	function get_ClasstestAcademicPerformance($acyear,$sid,$marktable,$term)
	{
		$sql="select * from ".$marktable." where `acyear`='".$acyear."' and `examid`='".$term."' and sid='".$sid."' order by `sub_id`";
		$result = mysql_query($sql);
		$i=0;
		while($row=mysql_fetch_array($result))
		{
			$mark[$i]['sub_id']=$row['sub_id'];
			$mark[$i]['subname']=$row['subname'];
			$mark[$i]['subhd_id']=$row['subhd_id'];
			$mark[$i]['subhd_name']=$row['subhd_name'];
			$mark[$i]['mark']=$row['mark'];
			$mark[$i]['max_mark']=$row['max_mark'];
			$i++;
		}
		return $mark;
	}
	function get_TermAcademicPerformance($acyear,$sid,$marktable,$term)
	{
		$sql="select * from ".$marktable." where `acyear`='".$acyear."' and `examid`='".$term."' and sid='".$sid."' order by `assessment` ASC";
		$result = mysql_query($sql);
		$i=0;
		while($row=mysql_fetch_array($result))
		{
			$subname=$row['subname'];
			$assm=$row['assessment'];
			$mark[$subname][$assm]['mark']=$mark[$subname][$assm]['mark']+$row['mark'];
			$mark[$subname][$assm]['max_mark']=$mark[$subname][$assm]['max_mark']+$row['max_mark'];
		}
		return $mark;
	}
	function get_ClasstestAcademicPerformanceHSS($acyear,$sid,$marktable,$term,$subject_table,$classno)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$sql="select * from ".$marktable." where `acyear`='".$acyear."' and `term`='".$term."' and stud_id='".$sid."' order by `sub_id`";
		$result = mysql_query($sql);
		$i=0;
		while($row=mysql_fetch_array($result))
		{
			$mark[$i]['sub_id']=$row['sub_id'];
			$subject_det=$dbobj->selectall($subject_table,array("sub_id"=>$mark[$i]['sub_id']));
			$mark[$i]['subname']=$subject_det['subject'];
			$mark[$i]['mark']=$row['mark'];
			$mark[$i]['max_mark']=$this->get_maxmark($classno,$mark[$i]['sub_id'],$acyear,$term);
			$i++;
		}
		return $mark;
	}
function get_TermAcademicPerformanceHSS($acyear,$sid,$marktable,$term,$subject_table,$classno)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$sql="select * from ".$marktable." where `acyear`='".$acyear."' and `term`='".$term."' and stud_id='".$sid."' order by `sub_id`,`subheadid`";
		$result = mysql_query($sql);
		$i=0;
		while($row=mysql_fetch_array($result))
		{
			$mark[$i]['sub_id']=$row['sub_id'];
			$mark[$i]['subname']=$row['subname'];
			$mark[$i]['subheadid']=$row['subheadid'];
			$mark[$i]['subheadname']=$row['subheadname'];
			$mark[$i]['mark']=$row['mark'];
			if($mark[$i]['subheadid']==0)
			{
				$mark[$i]['max_mark']=$this->get_maxmark($classno,$mark[$i]['sub_id'],$acyear,$term);
			}
			else
			{
				$mark[$i]['max_mark']=$this->get_maxmark($classno,$mark[$i]['subheadid'],$acyear,$term);
			}
			$i++;
		}
		return $mark;
	}
	
function group_details($groupid)
{
	  $dbobj = new DB();
	  $dbobj->getCon();
	  $sql_group=$dbobj->select("select * from `group_member` where `group_id`='".$groupid."'");
      $i=0;	 
	 while($row_group=$dbobj->fetch_array($sql_group))
	  {
		  if($row_group['selectiontype']=="INDIVIDUAL")
	      { 
			  $group_det[$i]['name']=$this->group_individual($row_group['type'],$row_group['userid']);
	          $group_det[$i]['groupid']=$groupid;
			  $group_det[$i]['selectiontype']=$row_group['selectiontype'];
			  $group_det[$i]['type']=$row_group['type'];
			  $group_det[$i]['userid']=$row_group['userid'];
			  $group_det[$i]['admin']=$row_group['admin'];
			  $group_det[$i]['id']=$row_group['id'];
			  
		  }
		  else
		  {
			  $group_det[$i]['name']=$this->group_non_individual($row_group['type'],$row_group['userid'],$row_group['selectiontype']);
	          $group_det[$i]['groupid']=$groupid;
			  $group_det[$i]['selectiontype']=$row_group['selectiontype'];
			  $group_det[$i]['type']=$row_group['type'];
			  $group_det[$i]['userid']=$row_group['userid'];
			  $group_det[$i]['admin']=$row_group['admin'];
			  $group_det[$i]['id']=$row_group['id'];
			  
		  }
		$i++;  
	  }
return $group_det; 
}
function group_individual($type,$userid)
{
	  $dbobj = new DB();
	  $dbobj->getCon();
	if($type=="student" || $type=="parent")
			  {
				  $sql=$dbobj->select("select * from student where sid='".$userid."'");
			      $row=$dbobj->fetch_array($sql);
				  $name=$row['sname'];
			  }
	if($type=="teacher")
			  {
				  $sql=$dbobj->select("select * from teacher where id='".$userid."'");
			      $row=$dbobj->fetch_array($sql);
				  $name1=$row['name'];
				  $name2=$row['lname'];
				  $name=$name1."".$name2;
			  }

 return $name;			  
}
function group_non_individual($type,$userid,$selectiontype)
{
	  $dbobj = new DB();
	  $dbobj->getCon();
	  if($selectiontype=="classno")
	  {  
        		  $sql=$dbobj->select("select distinct classname from sclass where classno='".$userid."'");
			      $row=$dbobj->fetch_array($sql);
				  $name=$row['classname'];
	  }
	  else
	  {
		          $sql=$dbobj->select("select *  from sclass where classid='".$userid."'");
			      $row=$dbobj->fetch_array($sql);
				  $name_temp1=$row['classname'];
				  $name_temp2=$row['division'];
				  $name=$name_temp1." ".$name_temp2;
	  }
	  return $name;
}

function group_email($type,$userid)
{
	  $dbobj = new DB();
	  $dbobj->getCon();
	         if($type=="student" )
			  {
				  $sql=$dbobj->select("select * from student where sid='".$userid."'");
			      $row=$dbobj->fetch_array($sql);
				  $email=$row['email'];
			  }
			 if($type=="parent")
			  {
				  $sql=$dbobj->select("select * from student where sid='".$userid."'");
			      $row=$dbobj->fetch_array($sql);
				  $sql2=$dbobj->select("select * from parent where id='".$row['parent_id']."'");
			      $row2=$dbobj->fetch_array($sql2);
				  $email=$row2['femail'];
			  }
	        if($type=="teacher")
			  {
				  $sql=$dbobj->select("select * from teacher where id='".$userid."'");
			      $row=$dbobj->fetch_array($sql);
				  $email=$row['email'];
			  }
	return $email;		  
}
function group_number($type,$userid)
{
	  $dbobj = new DB();
	  $dbobj->getCon();
	         if($type=="student" )
			  {
				  $sql=$dbobj->select("select * from student where sid='".$userid."'");
			      $row=$dbobj->fetch_array($sql);
				  $phone=$row['phone'];
			  }
			 if($type=="parent")
			  {
				  //echo "select * from student where sid='".$userid."'";
				  $sql=$dbobj->select("select * from student where sid='".$userid."'");
			      $row=$dbobj->fetch_array($sql);
				  $sql2=$dbobj->select("select * from parent where id='".$row['parent_id']."'");
			      $row2=$dbobj->fetch_array($sql2);
				  $phone=$row2['sms_number'];
			  }
	        if($type=="teacher")
			  {
				  $sql=$dbobj->select("select * from teacher where id='".$userid."'");
			      $row=$dbobj->fetch_array($sql);
				  $phone=$row['mob'];
			  }
	return $phone;		  
}	
	 function rollnumber($sid,$classid,$acyear)
 {
			$dbobj = new DB();
			$dbobj->getCon();
			$student=$dbobj->get_studentdetByClassAndAcyear($classid,$acyear);
			if(!empty($student))
			{   $i=0;
				foreach($student as $stud)
				{
					$i++;
					$sid2=$stud['sid'];
					if($sid==$sid2)
					{
						$roll=$i;
					}
					
					
				}
			}
	return $roll;		
            			
 } 
function get_all_subject($table,$classno)
{
		$dbobj = new DB();
		$dbobj->getCon();

		  $select=$dbobj->select("select * from `".$table."` order by `sub_id`");
			$i=1;
			while($row=$dbobj->fetch_array($select))
			{
				$subject[$i]['id']="main-".$row['sub_id'];
				$subject[$i]['subject']=$row['subject'];
                                $subject[$i]['part']="PART I";
				$i++;	
			}
			//echo "select * from `opn_subjects` where `classno`='".$classno."' order by `id` ";
			$opn_sub=$dbobj->select("select * from `opn_subjects` where `classno`='".$classno."' order by `id` ");
				while($row=$dbobj->fetch_array($opn_sub))
			{
				$subject[$i]['id']="opn-".$row['id'];
				$subject[$i]['subject']=$row['opn_sub'];
				$subject[$i]['part']=$row['part'];
				$i++;	
			}
			return $subject;
}
 function mark_chk($term,$subid,$sub_type,$mark_table,$classid,$stud_id)
 {
	$dbobj = new DB();
	$dbobj->getCon();
	$sql="select * from $mark_table where classid = '$classid' and examid = '$term' and sid = '$stud_id'";
	if($sub_type=="main")
	{
		$sql.=" and sub_id='".$subid."'";
	}
	elseif($sub_type=="opn")
	{
		$sql.="and opn_sub='".$subid."'";
	}
	$qry=$dbobj->exe_qry($sql);
	$check=mysql_num_rows($qry);
	return $check;
 }
 function grade_calculate($mm,$min,$maxtotal,$classno,$sub)
{
	
}
function num_of_failed_subject($mm,$min)
{
$no_fail=0;
  if(!empty($mm))
  {
	foreach($mm as $k=>$m)
	{

		
         foreach($min as $l=>$mi)
		 {
				if($k==$l)
				{

						if($m<$mi)
						{
						$no_fail++;
						}
						
					
				}

		 }

	}
  }	
	return $no_fail;
}
function student_work($sid,$id,$acyear)
{
	$dbobj = new DB();
	$dbobj->getCon();
	//echo "select * from `studentwork` where `sid`='".$sid."' and `learn_id`='".$id."'";
    $qry=$dbobj->select("select * from `studentwork` where `sid`='".$sid."' and `learn_id`='".$id."' and `acyear`='".$acyear."'");
    $row=$dbobj->fetch_array($qry);	
	return $row;
}
function legend_valBydate($submit_date,$due_date)
{
	if($submit_date!=0000-00-00 && $due_date!=0000-00-00)
	{
		$date = str_replace('/', '-', $submit_date);
	    $submit_date=date('Y-m-d', strtotime($date));
		$date = str_replace('/', '-', $due_date);
	    $due_date=date('Y-m-d', strtotime($date));
		if($submit_date==$due_date)
		{
			$val=1; // on time
		}
		if($submit_date<$due_date)
		{
			$val=1; // on time
		}
		if($submit_date>$due_date)
		{
			$val=2; // recived late
		}		
		
	}
    if($submit_date==0000-00-00 && $due_date!=0000-00-00)
	{
		$date = str_replace('/', '-', $due_date);
	    $due_date=date('Y-m-d', strtotime($date));
		$current_date=date('Y-m-d');
		if($current_date<$due_date)
		{
			$val=3; // pending
		}
		if($current_date==$due_date)
		{
			$val=3; // pending
		}
		if($current_date>$due_date)
		{
			$val=4; // overdue
		}		
		
	}
    return $val;	
}
function legend_imageByval($val)
{
	if($val==1)
	{
		$legend="<img src='../legend/ontime.png'  height='25' width='25'>";
	}
	if($val==2)
	{
		$legend="<img src='../legend/recivedlate.png'  height='25' width='25'>";
	}
	if($val==3)
	{
		$legend="<img src='../legend/pending.png'  height='25' width='25'>";
	}
	if($val==4)
	{
		$legend="<img src='../legend/overdue.jpg'  height='25' width='25'>";
	}
     return	$legend;
}
function legend_det($submit_date,$due_date)
{
    $val=$this->legend_valBydate($submit_date,$due_date);
	// legend image
    $legend=$this->legend_imageByval($val);	
    return $legend;	
}
function student_workBysubject($sid,$type,$acyear,$subid,$subtype)
{
	$dbobj = new DB();
	$dbobj->getCon();
	$sid;
    $sql="select `studentwork`.*,`learning_mod`.`subid`,`learning_mod`.`subtype`,`learning_mod`.`due_date`,`learning_mod`.`title`,`learning_mod`.`refcode`,`learning_mod`.`date` from `studentwork` left join `learning_mod` on `learning_mod`.`id`=`studentwork`.`learn_id` where  `studentwork`.`acyear`='".$acyear."' ";
	if($sid!="-1")
	{	
	$sql.=" and `studentwork`.`sid`='".$sid."'";
	}
	if($type!="-1")
	{
		$sql.=" and `learning_mod`.`type`='".$type."'";
	}	
	if($subid!="" && $subid!="all")
	{
	$sql.=" and `learning_mod`.`subid`='".$subid."' and `learning_mod`.`subtype`='".$subtype."'";
	}
	//echo $sql;
	$sel=$dbobj->select($sql);
	$i=0;
    while($row=$dbobj->fetch_array($sel))
	{
		$i++;
		$learn_det[$i]['id']=$row['learn_id'];
		$learn_det[$i]['workid']=$row['id'];
		$learn_det[$i]['submit_date']=$row['submit_date'];
		$learn_det[$i]['subid']=$row['subid'];
		$learn_det[$i]['subtype']=$row['subtype'];
		$learn_det[$i]['due_date']=$row['due_date'];
		$learn_det[$i]['title']=$row['title'];
		$learn_det[$i]['refcode']=$row['refcode'];
		$learn_det[$i]['date']=$row['date'];
		$learn_det[$i]['teacher_remark']=$row['teacher_remark'];
                $learn_det[$i]['teacher_comments']=$row['teacher_comments'];
		
	}
return $learn_det;	
}
function termOnly($classno,$acyear)
{
	$dbobj = new DB();
	$dbobj->getCon();
	$sql1 = $dbobj->select("select * from `terms` where `classno` = '$classno' and `exam_type`='term'");
	$i=0;
	while($row = $dbobj->fetch_array($sql1))
	{
			$term[$i]['id']=$row['id'];;
			$term[$i]['name']=$row['term_name'];
			$i++;	   
	
	}
	return $term;	
}
  function followup_type($acyear,$classno,$termid)
 {
	 $dbobj = new DB();
	 $dbobj->getCon();
	// $term_det=$dbobj->select("SELECT distinct `type` FROM `internal_exam_settings` WHERE `classno`='".$classno."' and `exam`='".$termid."' and `acyear`='".$acyear."'");
	//"SELECT distinct `type` FROM `internal_exam_settings` WHERE `acyear`='".$acyear."'"
    $term_det=$dbobj->select("SELECT distinct `type` FROM `internal_exam_settings`");	 
	 $i=0;
	 while($row=$dbobj->fetch_array($term_det))
	 {
	   $i++;		 
	   $type[$i]=$row['type'];   
	 } 
	 return $type;	 
     	 
 }
  function followup_head($acyear,$classno,$termid,$type)
 {
	 $dbobj = new DB();
	 $dbobj->getCon();
	 $term_det =$dbobj->selectall("terms",array("id" => $termid));
	 $term_name=$term_det['term_name'];
	// $term_det=$dbobj->select("SELECT *  FROM `internal_exam_settings` WHERE `classno`='".$classno."' and `exam`='".$termid."' and `acyear`='".$acyear."' and `type`='".$type."'");
	//echo "SELECT *  FROM `internal_exam_settings` WHERE `acyear`='".$acyear."' and `type`='".$type."' and `term_name`='".$term_name."'";
	 $term_det=$dbobj->select("SELECT *  FROM `internal_exam_settings` WHERE  `type`='".$type."' and `term_name`='".$term_name."'");	 
	 $i=0;
	 while($row=$dbobj->fetch_array($term_det))
	 {
	   $i++;		 
	   $head[$i]['head']=$row['head'];  
	   $head[$i]['id']=$row['id']; 	   
	   $head[$i]['max_mark']=$row['max_mark'];   	
	   $head[$i]['type']=$row['type'];
	   $head[$i]['input_type']=$row['input_type'];  	   
	 } 
	 return $head;		 
     	 
 } 
  function followup_mark($acyear,$classid,$termid,$subid,$subtype,$typeid,$sid)
 {
	 $dbobj = new DB();
	 $dbobj->getCon();
	 $mark_det=$dbobj->select("SELECT *  FROM `student_followup_record` WHERE `classid`='".$classid."' and `term`='".$termid."' and `acyear`='".$acyear."' and `type_id`='".$typeid."' and `sid`='".$sid."' and `subid`='".$subid."' and `subtype`='".$subtype."'");
	 $row=$dbobj->fetch_array($mark_det);   	   
	 return $row['mark'];	 
     	 
 } 
  function followup_remark($acyear,$classid,$termid,$subid,$subtype)
 {
	 $dbobj = new DB();
	 $dbobj->getCon();
	 $remark_det=$dbobj->select("SELECT *  FROM `student_followup_remark` WHERE `classid`='".$classid."' and `term`='".$termid."' and `acyear`='".$acyear."' and `subid`='".$subid."' and `subtype`='".$subtype."'");
	 $row=$dbobj->fetch_array($remark_det);   	   
	 return $row['remark'];	 
     	 
 }
   function subject_det($subid,$subtype,$classno)
 {
	 $dbobj = new DB();
	 $dbobj->getCon();
  	 if($subtype=="main")
	{
		$admin = new Admin($classno);
       $tablename=$admin->subject_table();
		$row =$dbobj->selectall("$tablename",array("sub_id"=>$subid));
		$subject['subject']=$row['subject'];		
	 }
	 else
	{
		$row =$dbobj->selectall("opn_subjects",array("id"=>$subid));
		$subject['subject']=$row['opn_sub'];
	}	 
    return  $subject;	 
 }
  function exam_name($classno,$acyear)
{
	$dbobj = new DB();
	$dbobj->getCon();
	$sql1 = $dbobj->select("select * from `examname` where classno = '$classno' and `acyear`='".$acyear."' order by FIELD (`name`,'I MID TERM EXAM','I TERMINAL EXAM','II MID TERM EXAM','II TERMINAL EXAM','III TERMINAL EXAM','PRE MODEL','MODEL')");
	$i=0;
	while($row1 = $dbobj->fetch_array($sql1))
	{
		
		$term_name = $row1['name'];
		$term[$i]['id']=$row1['termid'];;
		$term[$i]['name']=$row1['name'];
		$i++;
		
	}
	return $term;
}
	function get_exam_month($classno,$acyear)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$select=$dbobj->select("select * from `term_month` where `classno`='".$classno."' order by `id`");
		$i=1;
		while($row=$dbobj->fetch_array($select))
		{
			    $terms[$i]['termid']=$row['termid'];
			    $terms[$i]['id']=$row['id'];
				$terms[$i]['term_name']=$row['term_name'];
				$terms[$i]['smonth']=$row['smonth'];
				$terms[$i]['emonth']=$row['emonth'];

				$i++;
		
		}
		return $terms;
	}
function question_paper_year($termid,$acyear)
{
		$dbobject = new DB();
		$dbobject->getCon();	
		$terms_det=$dbobject->selectall("terms",array("id"=>$termid));
        $term_name=$terms_det['term_name'];		
	$year_det=explode('-',$acyear);
	$year=$year_det[0];
   /* if($term_name=="III TERMINAL EXAM" || $term_name=="PRE MODEL" || $term_name=="MODEL")
	{
		$year++;
	}
	return $year;*/

		$year2=$year+1;
	
	return $year."-".$year2;

}
function timeToSeconds($time)
{
     $timeExploded = explode(':', $time);
     if (isset($timeExploded[2])) {
         return $timeExploded[0] * 3600 + $timeExploded[1] * 60 + $timeExploded[2];
     }
     return $timeExploded[0] * 3600 + $timeExploded[1] * 60;
}
function time_elapsed($secs)
{
    $bit = array(
        ' Hour' => $secs / 3600 % 24,
        ' Mts' => $secs / 60 % 60
        );
        
    foreach($bit as $k => $v)
        if($v > 0)$ret[] = $v . $k;
        
        if(!empty($ret))
		{
    return join(' ', $ret);			
		}
}
   function termname($termname)
 {
	 if($termname=="I MID TERM EXAM")
	 {
		 $term_name="FIRST MID TERM EXAMINATION";
	 }
	 elseif($termname=="I TERMINAL EXAM")
	 {
		$term_name="FIRST  TERMINAL EXAMINATION"; 
	 }
	 elseif($termname=="II MID TERM EXAM")
	 {
		$term_name="SECOND MID TERM EXAMINATION"; 
	 }
	 elseif($termname=="II TERMINAL EXAM")
	 {
		$term_name="SECOND  TERMINAL EXAMINATION"; 		 
	 }
	 elseif($termname=="III TERMINAL EXAM")
	 {
		//$term_name="ANNUAL EXAMINATION"; 	
		$term_name="ANNUAL EVALUATION"; 
	 }
	 elseif($termname=="PRE MODEL")
	 {
		$term_name="PRE MODEL EXAMINATION"; 				 
	 }
	 elseif($termname=="MODEL")
	 {
		$term_name="MODEL EXAMINATION"; 				 
	 }		 
     else
	 {
		 $term_name=$termname; 
	 }		 
    return $term_name;
 }
   function hss_biology_id($classno,$subid,$sub_type)
 {
	 $dbobj = new DB();
	 $dbobj->getCon();
	 if($sub_type=="opn")
	 {
		 $subject_det=$dbobj->selectall("opn_subjects",array("id"=>$subid));
		 if($subject_det['opn_sub']=="BOTANY" || $subject_det['opn_sub']=="ZOOLOGY")
		 {
				 $sub_det=$dbobj->select("SELECT * FROM `opn_subjects` WHERE `classno`='".$classno."' and `opn_sub` IN ('BOTANY','ZOOLOGY')");       
				 $i=0;
		         while($row=$dbobj->fetch_array($sub_det))
				 {
					 $i++;
					 $subid_det[$i]=$sub_type."-".$row['id'];
			 
				 }			 
		 }
		 else
		 {
			 $subid_det[0]=$sub_type."-".$subid;
		 }

		 		 
	 }
	 else
	 {
		 $subid_det[0]=$sub_type."-".$subid;
	 }
	  return $subid_det;
 }
 function roman_letter($no)
{
    if($no==1)
	{
		$letter="I";
	}
	elseif($no==2)
	{
		$letter="II";
	}
	elseif($no==3)
	{
		$letter="III";
	}
	elseif($no==4)
	{
		$letter="IV";
	}
	elseif($no==5)
	{
       $letter="V";
	}
	elseif($no==6)
	{
       $letter="VI";
	}
	elseif($no==7)
	{
       $letter="VII";
	}
	elseif($no==8)
	{
       $letter="VIII";
	}
	elseif($no==9)
	{
       $letter="IX";
	}
	elseif($no==10)
	{
       $letter="X";
	}
	elseif($no==11)
	{
       $letter="XI";
	}
	elseif($no==12)
	{
       $letter="XII";
	}
	elseif($no==13)
	{
       $letter="XIII";
	}
	elseif($no==14)
	{
       $letter="XIV,";
	}
	elseif($no==15)
	{
       $letter="XV";
	}	
    else
	{
		$letter=$no;
	}	
	return $letter;	
}
function question_data($question_data)
{
	   $dbobj = new DB();
	   $dbobj->getCon(); 
       $findme="<img";	
	   $question_data =   preg_replace( '/style=(["\'])[^\1]*?\1/i', '', $question_data, -1 );
       $pos = strpos($question_data, $findme);		  
		if($pos== true)
        {
         // $question_data=str_replace("<img","<img style='image-rendering: pixelated;image-rendering: crisp-edges;display: inline-block;'",$question_data);
        // $question_data=str_replace("<img","<img style='filter:invert(100%);'",$question_data);
         // $question_data=str_replace("<img","<img style='width:18px;height:18px'",$question_data);
          $question_data=str_replace("<img","<img style='filter:invert(100%);-webkit-filter: invert(1); -webkit-filter: grayscale(1);display: inline-block;image-rendering: pixelated;image-
           rendering: crisp-edges;display: inline-block;'",$question_data);
			$question_data= preg_replace( '/%5Cdpi%7B[0-9][0-9]%7D/', '', $question_data, -1 );
        }
		else
		{
            $question_data=str_replace("<table","<table style='font-family: arial;font-size:16px;!important' ",$question_data);		   
		}
		 return $question_data;
}
function subject_name($subject_name)
{
    if($subject_name=="IT")
	{
		$subname="INFORMATION TECHNOLOGY";
	}
	elseif($subject_name=="GK")
	{
		$subname="GENERAL KNOWLEDGE";
	}
	elseif($subject_name=="ENG GRAMMAR")
	{
		$subname="ENGLISH GRAMMAR";
	}
	else
	{
		$subname=$subject_name;
	}	
	return $subname;	
} 
function question_part_num($classno,$exam,$subid,$sub_type,$acyear)
{
	    $dbobject = new DB();
	   $dbobject->getCon();
	  $sel_max=$dbobject->select("select * from `question_part` where `classno`='".$classno."' and `exam`='".$exam."' and `subid`='".$subid."' and `sub_type`='".$sub_type."' and `acyear`='".$acyear."'");
	  $i=0;
	  while($row=$dbobject->fetch_array($sel_max))
	  {
		  $i++;
		  $part_num[$i]=$row['part_num'];
		  
	  }	
	  $part_num_max= max($part_num);
	  return $part_num_max;
}
function ucfirstSentence($str){
     $str = ucfirst(strtolower($str));
     $str = preg_replace_callback('/([.!?])\s*(\w)/', 
       create_function('$matches', 'return strtoupper($matches[0]);'), $str);
     return $str;
}
function get_hss_subject($table,$classid,$acyear,$sid)
{
   $subject=$this->get_hss_subject_data($table,$classid,$acyear,$sid);
   return $subject;
}
function get_hss_subject_data($table,$classid,$acyear,$sid)
{
		$dbobj = new DB();
		$dbobj->getCon();
		  $select=$dbobj->select("select * from `".$table."` order by `sub_id`");
			$i=0;
			while($row=$dbobj->fetch_array($select))
			{
				$subject[$i]['id']="main-".$row['sub_id'];
				$subject[$i]['sub_type']="main";
				$subject[$i]['sub_id']=$row['sub_id'];
				$subject[$i]['subject']=$row['subject'];
                $subject[$i]['part']="PART I";
				$i++;	
			}
			//echo "select * from `opn_subjects` where `classno`='".$classno."' order by `id` ";
			$opn_sub=$dbobj->select("select `opn_subjects`.*,`opn_subject_stud`.`sub_id` from `opn_subjects` left join `opn_subject_stud` on `opn_subjects`.`id`=`opn_subject_stud`.`sub_id` where `opn_subject_stud`.`stud_id`='".$sid."'  and `opn_subject_stud`.`class_id`='".$classid."' and `opn_subject_stud`.`academic_year`='".$acyear."'  ORDER BY `opn_subjects`.`id`");
				while($row=$dbobj->fetch_array($opn_sub))
			{
				$subject[$i]['id']="opn-".$row['id'];
				$subject[$i]['sub_type']="opn";
				$subject[$i]['sub_id']=$row['id'];				
				$subject[$i]['subject']=$row['opn_sub'];
				$subject[$i]['part']=$row['part'];
				$i++;	
			}
			return $subject;	
}
function get_hss_subject_sheduled($table,$classid,$acyear,$sid,$classno,$termid)
{
   $get_hss_subject_data=$subject=$this->get_hss_subject_data($table,$classid,$acyear,$sid);
   if(!empty($get_hss_subject_data))
   {
	   $i=0;
	   foreach($get_hss_subject_data as $sub)
	   {
				$check_subject_sheduled=$this->check_subject_sheduled($sub['sub_id'],$sub['sub_type'],$termid,$acyear,$classno);
				if($check_subject_sheduled!=0){
				$subject[$i]['id']=$sub['id'];
				$subject[$i]['sub_type']=$sub['sub_type'];
				$subject[$i]['sub_id']=$sub['sub_id'];				
				$subject[$i]['subject']=$sub['subject'];
				$subject[$i]['part']=$sub['part'];
				$i++;}		   
	   }
   }
   return $subject;
}
function check_subject_sheduled($sub_id,$sub_type,$termid,$acyear,$classno)
{
    $dbobj = new DB();
	$dbobj->getCon();
    $sql= "SELECT * FROM `examschedule` WHERE `examid`='".$termid."' and `classno`='".$classno."'  and `acc_year`='".$acyear."'";
	if($sub_type=="main")
	{
		$sql.= " and`sub_id`='".$sub_id."' ";
	}
	elseif($sub_type=="opn")
	{
	$sql.= "and `opn_sub`='".$sub_id."'";		
	}
	//echo $sub_type."-".$sql;
	$qry=$dbobj->select($sql);
    $check=mysql_num_rows($qry);	
	return $check;
}
function get_Exam_subject($classno,$exam,$acyear)
{
		$dbobj = new DB();
		$dbobj->getCon();
		$admin=new Admin($classno);
		$tablename=$admin->subject_table();
		$sql1 = $dbobj->select("select distinct `sub_id` from examschedule where examid = '".$exam."'    order by `sub_id`");
		$i=0;
		while($row_2=mysql_fetch_array($sql1))
		{
		$subject[$i]['sub_id']=$row_2['sub_id'];
		$sql_11 = $dbobj->select("select * from ".$tablename."  where sub_id='".$row_2['sub_id']."' ");
		$row3=$dbobj->fetch_array($sql_11);
		$subject[$i]['subject'] = $row3['subject'];
		$subject[$i]['sub_type'] ="main";
		$i++;
		}
		//echo "select distinct `opn_sub` from examschedule where examid = '".$exam."' and `acc_year`='".$acyear."' order by `opn_sub`";
		$sql_opn = $dbobj->select("select distinct `opn_sub` from examschedule where examid = '".$exam."' and `sub_id`='0' order by `opn_sub`");
		while($row_opn=mysql_fetch_array($sql_opn))
		{
		$i++;
		$subject[$i]['sub_id']=$row_opn['opn_sub'];
		$sql_12opn = $dbobj->select("select * from `opn_subjects`  where `id`='".$row_opn['opn_sub']."'");
		$row_opn1=$dbobj->fetch_array($sql_12opn);
		$subject[$i]['subject'] = $row_opn1['opn_sub'];
		$subject[$i]['sub_type'] ="opn";
		
		}		
		return $subject;
}
 function get_mark($term,$classid,$sub,$sid,$acyear,$subhdid)
 {
	$dbobj = new DB();
	$dbobj->getCon();	
   $sql_clnm=$dbobj->select("select * from sclass where classid ='".$classid."'");
   $row_clnm=$dbobj->fetch_array($sql_clnm);
   $classno=$row_clnm['classno'];
   $sel_term=$dbobj->select("select * from terms where classno='".$classno."' and id='".$term."'");
   $term_row=$dbobj->fetch_array($sel_term);
   $assm=$term_row['exam_type'];
   //$admin=new Admin($classno);
   //$subject_table=$admin->subject_table();
  // $mark_table=$admin->mark_table()	;
	$sel_mark=$dbobj->select("select * from report_card where `sid`='".$sid."' and `examid`='".$term."' and `sub_id`='".$sub."' and `classid`='".$classid."' and `assessment`='".$assm."' and `ac_year`='".$acyear."'");
    $mrk_row=$dbobj->fetch_array($sel_mark);
    return $mrk_row['mark'];	
 }
 function get_mark_opn($term,$classid,$opnsub,$sid,$acyear,$subhdid)
 {
	$dbobj = new DB();
	$dbobj->getCon();	
   $sql_clnm=$dbobj->select("select * from sclass where classid ='".$classid."'");
   $row_clnm=$dbobj->fetch_array($sql_clnm);
   $classno=$row_clnm['classno'];
   $sel_term=$dbobj->select("select * from terms where classno='".$classno."' and id='".$term."'");
   $term_row=$dbobj->fetch_array($sel_term);
   $assm=$term_row['exam_type'];
   //$admin=new Admin($classno);
   //$subject_table=$admin->subject_table();
  // $mark_table=$admin->mark_table()	;
	$sel_mark=$dbobj->select("select * from report_card where `sid`='".$sid."' and `examid`='".$term."' and `opn_sub`='".$opnsub."' and `classid`='".$classid."' and `assessment`='".$assm."' and `ac_year`='".$acyear."'");
    $mrk_row=$dbobj->fetch_array($sel_mark);
    return $mrk_row['mark'];	
 }
   function get_max_min($term,$classno,$sub,$acyear,$subhdid)
 {
	$dbobj = new DB();
	$dbobj->getCon();
	 
    $trm_examdet=$dbobj->select("select * from `examschedule` where `examid`='".$term."' and `classno`='".$classno."' and `sub_id`='".$sub."'");	
    $temdet_row=$dbobj->fetch_array($trm_examdet);
    return $temdet_row;	
 } 
   function get_max_min_opn($term,$classno,$opnsub,$acyear,$subhdid)
 {
	$dbobj = new DB();
	$dbobj->getCon();
	 
    $trm_examdet=$dbobj->select("select * from `examschedule` where `examid`='".$term."' and `classno`='".$classno."' and `opn_sub`='".$opnsub."'");	
    $temdet_row=$dbobj->fetch_array($trm_examdet);
    return $temdet_row;	
 } 
 function distinct_acyear_by_sid($sid)
{
		$dbobject = new DB();
		$dbobject->getCon();	
	    $sql="SELECT distinct `acyear` FROM `student_class` WHERE `sid`='".$sid."'";
		$qry=$dbobject->select($sql);
		while($row=$dbobject->fetch_array($qry))
		{
			$data[]=$row['acyear'];
		}
		return $data;
}
function academic_summery_student_new($sid,$classid,$acyear)
{
		$dbobj = new DB();
		$dbobj->getCon();
		$class_det=$dbobj->selectall("sclass",array("classid"=>$classid));
        //echo $acyear;  		
		$admin=new Admin($class_det['classno']);
		$marktable=$admin->mark_table();
		$subject=$this->sheduled_exmaClasswise($class_det['classno'],$acyear);
		$classno=$class_det['classno'];
		$term_det=$this->term_open($classno,$acyear);
		
		if(!empty($subject))
		{
	        $assm=0;
			foreach($subject as $sub)
			{
					if(!empty($term_det))
					{
						$sub_type=$sub['sub_type'];
						$subject_name=new Subject($sub['subject']);
						$sub_name=$subject_name->sub_table();							
							foreach($term_det as $trm)
							{		
							        $termid=$trm['term_id'];
									if($sub_type=="main")
									{
										//$divmak[$termid]=$this->get_mark_subject($sid,$classid,$classno,$acyear,$sub['sub_id'],$assm,$termid,$marktable);
										$divmak[$termid]=$this->dumy_data_individual($sub['subject'],$sid,$termid);
										$max_temp[$termid]=$this->dumy_data_max();
										//$max_temp[$termid]=$this->max_mark_subhead_withsubject($classno,$sub['sub_id'],$acyear,$termid);
									}
									else
									{
										//$divmak[$termid]=$this->get_mark_subject_opn($sid,$classid,$classno,$acyear,$sub['sub_id'],$assm,$termid,$marktable);
										$divmak[$termid]=$this->dumy_data_individual($sub['subject'],$sid,$termid);
										$max_temp[$termid]=$this->dumy_data_max();							
										//$max_temp[$termid]=$this->max_mark_subhead_withopn($classno,$sub['sub_id'],$acyear,$termid);							
									}
                                    if($max_temp[$termid]!=0)
									{
									$res[$trm['term_name']][$sub_name]['mark']=round(($divmak[$termid]/$max_temp[$termid])*100,2);	
									}
									else
									{
										$res[$trm['term_name']][$sub_name]['mark']=0;
									}
														
						   }
					}
			
			}
			
		}
			return $res;
 }
 	function max_mark_subhead_withsubject($classno,$subid,$acyear,$examid)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$sql = $dbobj->select("select * from examschedule where examid = '$examid' and classno = '$classno' and sub_id = '$subid'");
		while($row=$dbobj->fetch_array($sql))
		{
				$max_mark=$max_mark+$row['maxmark'];	
		}

		return $max_mark;
	}
	function max_mark_subhead_withopn($classno,$opn,$acyear,$examid)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		//echo "select * from examschedule where examid = '$examid' and classno = '$classno' and sub_id = '0' and  `opn_sub`='".$opn."' and `acc_year`='".$ac_year."'";
		$sql = $dbobj->select("select * from examschedule where examid = '$examid' and classno = '$classno' and sub_id = '0' and  `opn_sub`='".$opn."'");
		while($row=$dbobj->fetch_array($sql))
		{
				$max_mark=$max_mark+$row['maxmark'];	
		}

		return $max_mark;
	}
 function academic_performance_table_new($sid,$classid,$termid,$acyear,$i)
 {
		$dbobj = new DB();
		$dbobj->getCon();
		$class_det=$dbobj->selectall("sclass",array("classid"=>$classid));
		$admin=new Admin($class_det['classno']);
		//$marktable="report_card";
		$marktable=$admin->mark_table();
		$subject_table=$admin->subject_table();
		$classno=$class_det['classno'];
		if($classno>=13 && $classno<=14)
		{	
		$subject=$this->get_hss_subject_sheduled($subject_table,$classid,$acyear,$sid,$classno,$termid);
			
		}
		else
		{
		$subject=$this->get_Exam_subject($classno,$termid,$acyear);			
		}
		$i=0;
	    $assm=0;
		if(!empty($subject))
		{   
			foreach($subject as $sub)
			{
				
				$sub_type=$sub['sub_type'];
				if($sub_type=="main")
				{				
				   //$smark=$this->get_mark_subject($sid,$classid,$classno,$acyear,$sub['sub_id'],$assm,$termid,$marktable);
				   $smark=$this->dumy_data_individual($sub['subject'],$sid,$termid);
				}
				else
				{
					//$smark=$this->get_mark_subject_opn($sid,$classid,$classno,$acyear,$sub['sub_id'],$assm,$termid,$marktable);
					$smark=$this->dumy_data_individual($sub['subject'],$sid,$termid);
				}
			//	$student=$dbobj->get_studentdetByClassAndAcyear($classid,$acyear);
				
				$clsmak=0;
				$str=0;
			//$student="";
				if(!empty($student))
				{
					foreach($student as $studs)
					{
						$ssid=$studs['sid'];						
				    if($sub_type=="main")
			     	{						
						//$clsmak=$clsmak+$this->get_mark_subject($ssid,$classid,$classno,$acyear,$sub['sub_id'],$assm,$termid,$marktable);
						$clsmak=$clsmak+$this->dumy_data_individual($sub['subject'],$ssid,$termid);
				    }
					else
					{
						//$clsmak=$clsmak+$this->get_mark_subject_opn($ssid,$classid,$classno,$acyear,$sub['sub_id'],$assm,$termid,$marktable);						
						$clsmak=$clsmak+$this->dumy_data_individual($sub['subject'],$ssid,$termid);						
					}					

						
						$str++;
					}
				}
				$subject_name=new Subject($sub['subject']);
				$sub_name=$subject_name->sub_table();
				$res[$sub_name]['mark']=$smark;
				$res[$sub_name]['class_avg']=round($clsmak/$str,2);
				$i++;
			}
			
		}
			return $res;
 }	
  function sheduled_exmaClasswise($classno,$acyear)
{
		$dbobj = new DB();
		$dbobj->getCon();
		$admin=new Admin($classno);
		$tablename=$admin->subject_table();
		$sql1 = $dbobj->select("select distinct `sub_id` from examschedule where `classno` = '".$classno."'   and `sub_id`!='0' order by `sub_id`");
		$i=0;
		while($row_2=$dbobj->fetch_array($sql1))
		{
		$subject[$i]['sub_id']=$row_2['sub_id'];
		$sql_11 = $dbobj->select("select * from ".$tablename."  where sub_id='".$row_2['sub_id']."' ");
		$row3=$dbobj->fetch_array($sql_11);
		$subject[$i]['subject'] = $row3['subject'];
		$subject[$i]['sub_type'] ="main";
		$i++;
		}
		$sql_opn = $dbobj->select("select distinct `opn_sub` from examschedule where `classno` = '".$classno."'   and `sub_id`='0' order by `opn_sub`");
		while($row_opn=$dbobj->fetch_array($sql_opn))
		{
		$i++;
		$subject[$i]['sub_id']=$row_opn['opn_sub'];
		$sql_12opn = $dbobj->select("select * from `opn_subjects`  where id='".$row_opn['opn_sub']."' ");
		$row_opn1=$dbobj->fetch_array($sql_12opn);
		$subject[$i]['subject'] = $row_opn1['opn_sub'];
		$subject[$i]['sub_type'] ="opn";

		}		
		return $subject;
}
function term_open($classno,$acyear)
{
	$dbobj = new DB();
	$dbobj->getCon();
	$term_det=$dbobj->select("select * from par_progress  LEFT JOIN `terms` on `par_progress`.`term_id`=`terms`.`id` where `terms`.`classno`='".$classno."'   and `par_progress`.`acyear`='".$acyear."' order by `term_name`");
    $i=0;   
   while($row=$dbobj->fetch_array($term_det))
	{
		$term[$i]['term_id']=$row['term_id'];
		$term[$i]['term_name']=$row['term_name'];
		$term[$i]['exam_type']=$row['exam_type'];
		$term[$i]['sub_type']=$row['sub_type'];
		$term[$i]['date']=$row['date'];
		$i++;
	}
	
return $term;
}
function academic_summery_class_data($classid,$acyear)
{
		$dbobj = new DB();
		$dbobj->getCon();
		$class_det=$dbobj->selectall("sclass",array("classid"=>$classid));
		$admin=new Admin($class_det['classno']);
		$marktable=$admin->mark_table();
		$term_det=$this->term_open($class_det['classno'],$acyear);
		$subject=$this->sheduled_exmaClasswise($class_det['classno'],$acyear);
		$student=$dbobj->get_studentdetByClassAndAcyear($classid,$acyear);
		$classno=$class_det['classno'];
		if(!empty($subject))
		{
	        $assm=0;
			foreach($subject as $sub)
			{
			   
				if(!empty($student))
				{
					if(!empty($term_det))
					{
						$subject_name=new Subject($sub['subject']);
						$sub_name=$subject_name->sub_table();						
							foreach($term_det as $trm)
							{		
							   $termid=$trm['term_id'];
                               $divmak[$termid]=0;	
                                $str=0;							   
								foreach($student as $studs)
								{
									$ssid=$studs['sid'];
									$inc_val=substr($ssid, -1);
									$inc_val2=substr($termid, -1);
									//$divmak[$termid]=$divmak[$termid]+$this->get_mark_subject($ssid,$classid,$classno,$acyear,$sub['sub_id'],$assm,$termid,$marktable);
									if($sub['sub_type']=="main")
									{				
									  // $divmak[$termid]=$divmak[$termid]+$this->get_mark_subject($ssid,$classid,$classno,$acyear,$sub['sub_id'],$assm,$termid,$marktable);
									   $divmak[$termid]=$divmak[$termid]+$this->dumy_data_individual($sub['subject'],$ssid,$termid);
									}
									else
									{
										//$divmak[$termid]=$divmak[$termid]+$this->get_mark_subject_opn($ssid,$classid,$classno,$acyear,$sub['sub_id'],$assm,$termid,$marktable);				
										$divmak[$termid]=$divmak[$termid]+$this->dumy_data_individual($sub['subject'],$ssid,$termid);			
									}									
									$str++;
								}

								$res[$trm['term_name']][$sub_name]['mark']=round($divmak[$termid]/$str,2);						
						   }
 
					}

				}

			}
			
		}
      return $res;
 }
 function academic_Performance_class_data($classid,$termid,$acyear)
 {
		$dbobj = new DB();
		$dbobj->getCon();
		$class_det=$dbobj->selectall("sclass",array("classid"=>$classid));
		$term_open=$this->term_open_check($class_det['classno'],$acyear,$termid);
		$admin=new Admin($class_det['classno']);
		$marktable=$admin->mark_table();
		$subject=$this->get_Exam_subject($class_det['classno'],$termid,$acyear);
		$classlist=$dbobj->get_classByClassno($class_det['classno']);
		$tclassid=$classid;
		$tclassno=$class_det['classno'];
		if(!empty($subject))
		{
	        $assm=0;
			foreach($subject as $sub)
			{
				$subject_name=$sub['subject'];
				$student=$dbobj->get_studentdetByClassAndAcyear($tclassid,$acyear);
				$divmak=0;
				$str=0;
				if(!empty($student))
				{
					foreach($student as $studs)
					{
						$ssid=$studs['sid'];
						if($sub['sub_type']=="main")
						{				
						   //$divmak=$divmak+$this->get_mark_subject($ssid,$tclassid,$tclassno,$acyear,$sub['sub_id'],$assm,$termid,$marktable);
						   $divmak=$divmak+$this->dumy_data_individual($subject_name,$ssid,$termid);
						}
						else
						{
							//$divmak=$divmak+$this->get_mark_subject_opn($ssid,$tclassid,$tclassno,$acyear,$sub['sub_id'],$assm,$termid,$marktable);				
							$divmak=$divmak+$this->dumy_data_individual($subject_name,$ssid,$termid);				
						}		
						$str++;
					}
					}
				
				
				$str2=0;
				$clsmak=0;
				foreach($classlist as $class)
				{
					
					$classid=$class['classid'];
					$classno=$class['classno'];
					$student_class=$dbobj->get_studentdetByClassAndAcyear($classid,$acyear);
					if(!empty($student_class))
					{

						foreach($student_class as $studs_class)
						{
							$ssid_class=$studs_class['sid'];
							//$clsmak=$clsmak+$this->get_mark_subject($ssid_class,$classid,$classno,$acyear,$sub['sub_id'],$assm,$termid,$marktable);
							if($sub['sub_type']=="main")
							{				
							   //$clsmak=$clsmak+$this->get_mark_subject($ssid_class,$classid,$classno,$acyear,$sub['sub_id'],$assm,$termid,$marktable);
							   $clsmak=$clsmak+$this->dumy_data_class($subject_name,$ssid_class,$termid);
							}
							else
							{
								$clsmak=$clsmak+$this->dumy_data_class($subject_name,$ssid_class,$termid);			
							}							
							$str2++;
						}
					}

					
				}

				$subject_name=new Subject($subject_name);
			    $sub_name=$subject_name->sub_table();
				$res[$sub_name]['divmark']=round($divmak/$str,2);
				$res[$sub_name]['clsmak']=round($clsmak/$str2,2);
				}
               // $table.="<tr><th>".$sub_name."</th><th>".round($divmak/$str,2)."</th><th>".round($clsmak/$str2,2)."</th></tr>";
			}			
		
			return $res;
 } 
function dumy_data_individual($sub_name,$sid,$termid)
{
		$inc_val=substr($sid, -1);
		$inc_val2=substr($termid, -1);
		if($sub_name=="English")
		{
			$mark=27+$inc_val+$inc_val2;
		}
		elseif($sub_name=="Malayalam")
		{
			$mark=23+$inc_val+$inc_val2;
		}
		elseif($sub_name=="Hindi")
		{
			$mark=28+$inc_val+$inc_val2;
		}
		elseif($sub_name=="EVS")
		{
			$mark=17+$inc_val+$inc_val2;
		}
		elseif($sub_name=="Social Studies")
		{
			$mark=25+$inc_val+$inc_val2;
		}	
		elseif($sub_name=="Mathematics")
		{
			$mark=18+$inc_val+$inc_val2;
		}
		elseif($sub_name=="IT")
		{
		$mark=24+$inc_val+$inc_val2;
		}	
		elseif($sub_name=="GK")
		{
		$mark=22+$inc_val+$inc_val2;	
		}
        else
		{
		$mark=12+$inc_val+$inc_val2;	
		}
   return $mark;		
} 
function dumy_data_class($sub_name,$inc_val,$inc_val2)
{
   $mark=$this->dumy_data_individual($sub_name,$inc_val,$inc_val2);
   return $mark;		
}
function dumy_data_max()
{
   $max=50;
   return $max;		
}
function dumy_data_min()
{
   $max=20;
   return $max;		
}
function term_open_check($classno,$acyear,$termid)
{
	$dbobj = new DB();
	$dbobj->getCon();
$term_det=$dbobj->select("SELECT * FROM `par_progress` WHERE `classno`='".$classno."' and `term_id`='".$termid."' and `acyear`='".$acyear."'");
    $chk=mysql_num_rows($term_det);  	
return $chk;
} 
function part_area($section)
{
	$dbobj = new DB();
	$dbobj->getCon();
	$sql="select * from `performance_head` where `section`='".$section."'";
	$performance_head= $dbobj->select($sql);
	$i=0;
	while($p_row=$dbobj->fetch_array($performance_head))
		{
		 $headname[$i]['headname']=$p_row['headname'];
		 $headname[$i]['id']=$p_row['id'];
		 $headname[$i]['section']=$p_row['section'];
         $headname[$i]['part']=$p_row['part'];		 
         $i++;		 
		}
   return  $headname;
	
}
function class_topper($class_percent)
{
	if(!empty($class_percent))
	{
		$max_perc= max($class_percent);
		$i=0;
		foreach($class_percent as $sid=>$perc)
		{
			if($max_perc==$perc)
			{
				$i++;
				$topper[$i]=$sid;
				//$topper[$i]['sid']=$sid;
				//$topper[$i]['perc']=$perc;
				
			}
		}
		
	}
	
	return $topper;
}
function class_topper_90_per($class_percent,$wrk_days)
{
	if(!empty($class_percent))
	{
		$max_perc= max($class_percent);
		$i=0;
		foreach($class_percent as $sid=>$perc)
		{
			$per_90=($perc/$wrk_days)*100;
			if($per_90>=90)
			{
				$i++;
				$topper[$i]=$sid;
				//$topper[$i]['sid']=$sid;
				//$topper[$i]['perc']=$perc;
				
			}
		}
		
	}
	
	return $topper;
}
	function get_scheduledSubjects($classno,$examid,$acyear)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$sql = $dbobj->select("select distinct `sub_id` from examschedule where examid = '$examid' and classno = '$classno'  and `acc_year`='".$acyear."'");
		$i=0;
		while($row=$dbobj->fetch_array($sql))
		{
			$subject[$i]['id']="main-".$row['sub_id'];
			$i++;
		}
		return $subject;
	}
function get_max_mark($term,$classno,$sub,$acyear,$subhdid)
 {
	$dbobj = new DB();
	$dbobj->getCon();
	 
    $trm_examdet=$dbobj->select("select * from `examschedule` where `examid`='".$term."' and `classno`='".$classno."' and `sub_id`='".$sub."' and subhead_id='".$subhdid."' and acc_year='".$acyear."'");	
    $temdet_row=$dbobj->fetch_array($trm_examdet);
    return $temdet_row['maxmark'];	
 }
function progress_card_link($sid,$acyear,$examid,$classno,$classid){
    $data['status']="Success";
    $data['link']="http://jrboonsolutions.com/ecms-erp/Exam/student_mark_arabic_mark_formindweb.php?sid=".$sid."&acyear=".$acyear."&classno=".$classno."&classid=".$classid."&examid=".$examid;

    return $data;
}
}
?>