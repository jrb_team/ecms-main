<?php
include_once ('createdb.php');
include_once ('transport_class.php');
class Fee
{
    function currency(){
        $currency="$";
        return $currency;
    }
    function get_FeeUnpaidList($term, $acyear)
    {
        $dbobj = new DB();
        $dbobj->getCon();
        $sql = "select * from `student_academic_fee` left join `student` on `student_academic_fee`.`sid`=`student`.`sid` where `ac_year`='" . $acyear . "' and `fee_name`='" . $term . "' and `flag`='false' and `s_status`='active'";
        $i = 0;
        $exe_qry = $row = $dbobj->select($sql);
        while ($row = $dbobj->fetch_array($exe_qry))
        {
            $student[$i]['sname'] = $row['sname'];
            $student[$i]['classid'] = $row['classid'];
            $i++;
        }
        return $student;

    }
    function get_paidList()
    {
        $dbobj = new DB();
        $dbobj->getCon();
        $sql = "select * from `student_academic_fee` left join `student` on `student_academic_fee`.`sid`=`student`.`sid` where `ac_year`='" . $acyear . "' and `fee_name`='" . $term . "' and `flag`='true' and `s_status`='active'";
        $i = 0;
        $exe_qry = $row = $dbobj->select($sql);
        while ($row = $dbobj->fetch_array($exe_qry))
        {
            $student[$i]['sname'] = $row['sname'];
            $student[$i]['classid'] = $row['classid'];
            $i++;
        }
        return $student;
    }
    function get_GroupForFee()
    {
        $group[0] = "none";
        $group[1] = "biology";
        $group[2] = "computer";
        $group[3] = "commerce";
        return $group;

    }
    function feeType()
    {
        $fee_type['db_student'] = "Enrolled/Attending";
        $fee_type['other'] = "New/Future";
        return $fee_type;
    }

    function get_TermForfees()
    {

        $term[0] = "I Term";
        $term[1] = "II Term";
        $term[2] = "III Term";

        return $term;
    }
    function get_FeePaidList($term, $acyear)
    {
        $dbobj = new DB();
        $dbobj->getCon();
        $sql = "select * from `student_academic_fee` left join `student` on `student_academic_fee`.`sid`=`student`.`sid` where `ac_year`='" . $acyear . "' and `fee_name`='" . $term . "' and `flag`='true' and `s_status`='active'";
        $i = 0;
        $exe_qry = $row = $dbobj->select($sql);
        while ($row = $dbobj->fetch_array($exe_qry))
        {
            $student[$i]['sname'] = $row['sname'];
            $student[$i]['classid'] = $row['classid'];
            $i++;
        }
        return $student;

    }
    function get_academicFee($sid, $acyear)
    {
        $dbobj = new DB();
        $dbobj->getCon();
        $sql = "select * from `student_academic_fee` where `sid`='" . $sid . "' and `ac_year`='" . $acyear . "'";
        $i = 0;
        $exe_qry = $dbobj->select($sql);
        while ($row = $dbobj->fetch_array($exe_qry))
        {
            $fee_det[$i]['feeid'] = $row['id'];
            $fee_det[$i]['sid'] = $row['sid'];
            $fee_det[$i]['fee_name'] = $row['fee_name'];
            $fee_det[$i]['amount'] = $row['amount'];
            $fee_det[$i]['due_date'] = $row['due_date'];
            $fee_det[$i]['classid'] = $row['classid'];
            $fee_det[$i]['discount'] = $row['discount'];
            $fee_det[$i]['reason'] = $row['reason'];
            $fee_det[$i]['paid_date'] = $row['paid_date'];
            $fee_det[$i]['ac_year'] = $row['ac_year'];
            $fee_det[$i]['flag'] = $row['flag'];
            $fee_det[$i]['feeppaymentmode'] = $row['feeppaymentmode'];
            $i++;
        }
        return $fee_det;
    }

    function get_AcademicAmountByFeename($sid, $feename,$fee_type,$parentid, $classno, $acyear)
    {
        $dbobj = new DB();
        $dbobj->getCon();
        $res=array();
        $student_det = $dbobj->selectall("student", array(
            "sid" => $sid
        ));
        $sel_fee = $dbobj->select("select * from `student_academic_fee` where `sid`='".$sid."' and  `fee_type`='".$fee_type."' and `parent_id`='".$parentid."' and `fee_name`='".$feename."' and `ac_year`='".$acyear."'");
        $fee_row = $dbobj->fetch_array($sel_fee);
        $res['fee_name']=$fee_row['fee_name'];
        $res['dis_type']=$fee_row['dis_type'];
        $res['amount']=$fee_row['amount'];
        $res['dis_type']=$fee_row['dis_type'];
        $res['ac_year']=$fee_row['ac_year'];
        $res['discount']=$fee_row['discount'];
        $res['due_date']=$fee_row['due_date'];
        $res['duration']=$fee_row['duration'];
        return $res;
    }
    function get_AcademicAmountByIndFeename($sid, $feename,$fee_type,$parentid, $classno,$headid, $acyear)
    {
        $dbobj = new DB();
        $dbobj->getCon();
        $res=array();
        $ins_studentFee=array();
        $student_det = $dbobj->selectall("student", array(
            "sid" => $sid
        ));
        $sql_sellFee="select * from `custom_academic_fee` where `parent_id`='".$parentid."' AND `sid`='".$sid."' and  `fee_type`='".$fee_type."' and `parent_id`='".$parentid."' and `fee_name`='".$feename."' and `ac_year`='".$acyear."'";
        if($headid!=''){
            $sql_sellFee.=" AND `feehead_id`='".$headid."'";
        }
        $sel_fee = $dbobj->select($sql_sellFee);
        $fee_row = $dbobj->fetch_array($sel_fee);
        $res['id']=$fee_row['id'];
        $res['fee_name']=$fee_row['fee_name'];
        $res['dis_type']=$fee_row['dis_type'];
        $res['amount']=$fee_row['amount'];
        $res['dis_type']=$fee_row['dis_type'];
        $res['ac_year']=$fee_row['ac_year'];
        $res['discount']=$fee_row['discount'];
        $res['due_date']=$fee_row['due_date'];
        $res['duration']=$fee_row['duration'];
        $res['ins_studentFee']=array();
        $i=0;
        if($sid!='0'){
        $sql_feeind="select * from `customInstallmentStudent` where `sid`='".$sid."' and  `feeid`='".$res['id']."'  and `acyear`='".$acyear."'";
        if($headid!=''){
            $sql_feeind.=" AND `headid`='".$headid."'";
        }
        $sel_feeind = $dbobj->select($sql_feeind);
        while($indfeerow=$dbobj->fetch_array($sel_feeind)){
            $res['ins_studentFee']['sid']=$indfeerow['sid'];
            $res['ins_studentFee']['feeid']=$indfeerow['feeid'];
            $res['ins_studentFee']['amount']=$indfeerow['amount'];
            $i++;
        }
        }else{
            $sql_feeFamily="select * from `customInstallmentStudent` where `sid`='0' and  `feeid`='".$res['id']."'  and `acyear`='".$acyear."'";
            if($headid!=''){
                $sql_feeFamily.=" AND `headid`='".$headid."'";
            }
            $sel_feeFamily = $dbobj->select($sql_feeFamily);
            while($indfeerow=$dbobj->fetch_array($sel_feeFamily)){
                $res['ins_studentFee']['sid']=$indfeerow['sid'];
                $res['ins_studentFee']['feeid']=$indfeerow['feeid'];
                $res['ins_studentFee']['amount']=$indfeerow['amount'];
                $i++;
            }
        }
        return $res;
    }
    function get_academicFeeHead($acyear)
    {
        $dbobject = new DB();
        $dbobject->getCon();
        $sel_genfee = $dbobject->select("select distinct `feename` from `generalfee` where `acyear`='" . $acyear . "' order by `feename`");
        $i = 0;
        while ($fee_row = $dbobject->fetch_array($sel_genfee))
        {
            $headname[$i] = $fee_row["feename"];
            $i++;
        }
        return $headname;
    }
    function get_fine($gtot,$duedate,$fine_type,$rate,$fine_duration)
    {
        $dbobj = new DB();
        $dbobj->getCon();
        $date = date("Y-m-d");  
        $fine=0;
        $duration=0;
        if($date>$duedate){
        if($fine_duration=="Monthly"){
            $ts1 = strtotime($duedate);
            $ts2 = strtotime($date);
            $year1 = date('Y', $ts1);
            $year2 = date('Y', $ts2);
            $month1 = date('m', $ts1);
            $month2 = date('m', $ts2);

            $duration = (($year2 - $year1) * 12) + ($month2 - $month1);
        }elseif($fine_duration=="Weekly"){;
            $duration=$this->week_between_two_dates($date,$duedate);
        }
        if($fine_type=="Percentage"){
            $fine=($gtot/100)*$rate;
            $fine=round(($fine*$duration),2);
        }else{
            $fine=$rate*$duration; 
        }
    }
        return $fine;
    }
    function week_between_two_dates($datefrom, $dateto)
    {
        $HowManyWeeks = date( 'W', strtotime($datefrom) ) - date( 'W', strtotime($dateto) );
        return $HowManyWeeks;
    }
    function get_studentDiscount($sid, $fid)
    {
        $dbobject = new DB();
        $dbobject->getCon();
        $sel_dis = $dbobject->select("select * from `discount_tbl` where `sid`='" . $sid . "' and `feeid`='" . $fid . "'");
        $disamnt = 0;
        while ($dis_row = $dbobject->fetch_array($sel_dis))
        {
            $disamnt = $disamnt + $dis_row['amount'];
        }
        return $disamnt;
    }
    function getIndividualDiscount($sid, $feeid, $head)
    {
        $dbobject = new DB();
        $dbobject->getCon();
        $discount = 0;
        $sel_dis = $dbobject->select("select * from `discount_tbl` where `feeid`='" . $feeid . "' and `sid`='" . $sid . "' and `feehead`='" . $head . "'");
        $row = $dbobject->fetch_array($sel_dis);
        $discount = $row['amount'];
        return $discount;
    }
    function getIndividualrefund($sid, $feeid, $head)
    {
        $dbobject = new DB();
        $dbobject->getCon();
        $refund = 0;
        $sel_dis = $dbobject->select("select * from `refund` where `feeid`='" . $feeid . "' and `sid`='" . $sid . "' and `feehead`='" . $head . "'");
        $row = $dbobject->fetch_array($sel_dis);
        $refund = $row['amount'];
        return $refund;
    }
    function general_fee($fee_type, $acyear, $classno, $term, $group)
    {
        $dbobject = new DB();
        $dbobject->getCon();
        $qry = $dbobject->select("SELECT * FROM `generalfee` WHERE `classno`='" . $classno . "' and `fee_type`='" . $fee_type . "' and `acyear`='" . $acyear . "' and `term`='" . $term . "' and `group`='" . $group . "'");
        $i = 0;
        while ($row = $dbobject->fetch_array($qry))
        {
            $fee[$i]['id'] = $row['id'];
            $fee[$i]['feename'] = $row['feename'];
            $fee[$i]['amount'] = $row['amount'];
            $fee[$i]['fee_type'] = $row['fee_type'];
            $fee[$i]['duedate'] = $row['duedate'];
            $i++;

        }
        return $fee;
    }
    function Invoice_num($student_acyear)
    {
        $dbobject = new DB();
        $dbobject->getCon();
        //$acyear=$dbobject->get_acyear();
        $sel_invoice = $dbobject->select("SELECT MAX(`reciept_no`) AS invoice_id FROM `rec_series` where `acyear`='" . $student_acyear . "'");
        $invoice_row = $dbobject->fetch_array($sel_invoice);
        $invID = $invoice_row['invoice_id'];
        if ($invID == "")
        {
            $invID = 1;
        }
        else
        {
            $invID = $invID + 1;
        }
        return $invID = str_pad($invID, 4, '0', STR_PAD_LEFT);
    }
    function general_fee_term($fee_type, $acyear, $classno, $term, $group)
    {
        $dbobject = new DB();
        $dbobject->getCon();
        $qry = $dbobject->select("SELECT * FROM `generalfee` WHERE `classno`='" . $classno . "' and `fee_type`='" . $fee_type . "' and `acyear`='" . $acyear . "' and `term`='" . $term . "' and `group`='" . $group . "'");
        $term_tot = 0;
        while ($row = $dbobject->fetch_array($qry))
        {
            $term_tot = $term_tot + $row['amount'];
            if ($row['duedate'] != "0000-00-00")
            {
                $d_date = date("d-m-Y", strtotime($row['duedate']));
            }
        }
        $term_dt['amount'] = $term_tot;
        $term_dt['duedate'] = $d_date;
        return $term_dt;
    }
    function paymentBydate($date_from, $date_to, $type)
    {
        $dbobject = new DB();
        $dbobject->getCon();
        $date_from = date('Y-m-d', strtotime($date_from));
        $date_to = date('Y-m-d', strtotime($date_to));
        $sql = "SELECT * FROM `payment` WHERE `pay_date`>='" . $date_from . "' and `pay_date`<='" . $date_to . "'";
        if ($type != "all")
        {
            $sql .= " and `feetype`='" . $type . "'";
        }
        $sql .= "order by `feetype`,`pay_date`";
        $qry = $dbobject->select($sql);
        $i = 0;
        while ($row = $dbobject->fetch_array($qry))
        {
            $pay[$i]['parent_id'] = $row['parent_id'];
            $pay[$i]['studentid'] = $row['studentid'];
            $pay[$i]['feeid'] = $row['feeid'];
            $pay[$i]['total_amount'] = $row['total_amount'];
            $pay[$i]['fine'] = $row['fine'];
            $pay[$i]['discount'] = $row['discount'];
            $pay[$i]['paid_amount'] = $row['paid_amount'];
            $pay[$i]['pay_date'] = $row['pay_date'];
            $pay[$i]['transaction_id'] = $row['transaction_id'];
            $pay[$i]['ref_number'] = $row['ref_number'];
            $pay[$i]['status_code'] = $row['status_code'];
            $pay[$i]['pay_type'] = $row['pay_type'];
            $pay[$i]['print'] = $row['print'];
            $pay[$i]['cheque_num'] = $row['cheque_num'];
            $pay[$i]['bank'] = $row['bank'];
            $pay[$i]['feetype'] = $row['feetype'];
            $pay[$i]['refund'] = $row['refund'];
            $i++;

        }
        return $pay;
    }
    function distinctTransationid()
    {
        $dbobject = new DB();
        $dbobject->getCon();
        $qry = $dbobject->select("SELECT * FROM `rec_series`");
        $i = 0;
        while ($row = $dbobject->fetch_array($qry))
        {

            $transactionid[$i] = $row['acyear'] . "/" . $row['reciept_no'];
            $i++;
        }
        return $transactionid;
    }
    function TransportfeeByTerm($term, $side, $val)
    {
        $dbobject = new DB();
        $dbobject->getCon();
        $month1 = array(
            "june",
            "july",
            "august",
            "september"
        );
        $month2 = array(
            "october",
            "november",
            "december",
            "january"
        );
        $month3 = array(
            "february",
            "march"
        );
        if ($term == "I Term")
        {
            foreach ($month1 as $mnth)
            {
                $month_det = $dbobject->selectall("`transport_fee`", array(
                    "root" => $val,
                    "month" => $mnth
                ));
                $term_double = $term_double + $month_det['single_amt'];
            }

        }
        if ($term == "II Term")
        {
            foreach ($month2 as $mnth)
            {
                $month_det = $dbobject->selectall("`transport_fee`", array(
                    "root" => $val,
                    "month" => $mnth
                ));
                $term_double = $term_double + $month_det['single_amt'];
            }

        }
        if ($term == "III Term")
        {
            foreach ($month3 as $mnth)
            {
                $month_det = $dbobject->selectall("`transport_fee`", array(
                    "root" => $val,
                    "month" => $mnth
                ));
                $term_double = $term_double + $month_det['single_amt'];
            }

        }
        if ($term_double != 0)
        {
            $single = $term_double / 2;
        }

        if ($side == "double")
        {
            $amount = $term_double;
        }
        elseif ($side == "single")
        {
            $amount = $single;
        }

        return $amount;
    }
    function TransportfeeBymonth($term, $side, $val)
    {
        $dbobject = new DB();
        $dbobject->getCon();
        $month_amount = $dbobject->selectall("`transport_fee`", array(
            "root" => $val,
            "month" => $term
        ));
        if ($side == "double")
        {
            $amount = $month_amount['single_amt'];
        }
        elseif ($side == "single")
        {
            $single = $month_amount['single_amt'] / 2;
            $amount = $single;
        }

        return $amount;
    }
    function insert_academic_fee($sid, $acyear, $classno, $fee_type, $group)
    {
        $dbobject = new DB();
        $dbobject->getCon();
        $terms = $this->get_TermForfees();
        foreach ($terms as $row)
        {
            $fee_name = $row;
            $insert = $dbobject->exe_qry("INSERT INTO `student_academic_fee`(`sid`, `fee_name`, `feeppaymentmode`, `flag`, `classid`, `ac_year`) VALUES ('" . $sid . "','" . $fee_name . "','" . $fee_type . "','false','" . $classno . "','" . $acyear . "')");
        }
    }
    function unpaid_reporthead($type, $feetype, $ptype, $term, $acyear, $classno)
    {
        $dbobject = new DB();
        $dbobject->getCon();
        $head = "FEE " . strtoupper($ptype) . " REPORT " . $acyear . "  ";
        if ($type == "section_wise")
        {
            if ($classno == - 1)
            {
                $val = "KG,LP,UP,HS,HSS";
            }
            else
            {
                $val = $classno;
            }

        }
        elseif ($type == "class_wise")
        {
            $class_det = $dbobject->selectall("sclass", array(
                "classno" => $classno
            ));
            $val = " CLASS " . $class_det['classname'];
        }

        $head .= "( " . strtoupper($term);
        if ($feetype == - 1)
        {
            $feetype = "COMBINED";
        }
        $head .= " " . strtoupper($feetype);
        $head .= " " . strtoupper($val) . " )";
        return $head;
    }
    function get_academicFeeBYid($sid, $acyear, $id, $classno)
    {
        $dbobj = new DB();
        $dbobj->getCon();
        $sql = "select * from `student_academic_fee` where `sid`='" . $sid . "' and `ac_year`='" . $acyear . "' and `id`='" . $id . "'";
        $qry = $dbobj->select($sql);
        $row = $dbobj->fetch_array($qry);
        $amount = $this->general_fee_term($row['feeppaymentmode'], $acyear, $classno, $row['fee_name'], "none");
        $fee_det = $amount['amount'];

        return $amount['amount'];
    }
    function Student_transport_feeBYid($sid, $acyear, $id)
    {
        $dbobject = new DB();
        $dbobject->getCon();
        $transport = new Transport();
        $qry = $dbobject->select("SELECT * FROM `student_bus_fee` WHERE `sid`='" . $sid . "' and `ac_year`='" . $acyear . "' and `id`='" . $id . "'");
        $row = $dbobject->fetch_array($qry);
        $feetype = $row['feetype'];
        $route_id = $row['route_id'];
        $side = $row['side'];
        $term = $row['term'];
        if ($feetype == "term")
        {
            $amount = $transport->TransportfeeByTerm($term, $side, $route_id);
        }
        else
        {
            $amount = $transport->TransportfeeBymonth($term, $side, $val);
        }

        return $amount;
    }
    function get_academicFee_unpaid($sid, $acyear)
    {
        $dbobj = new DB();
        $dbobj->getCon();
        $sql = "select * from `student_academic_fee` where `sid`='" . $sid . "' and `ac_year`='" . $acyear . "' and `flag`='false'";
        $i = 0;
        $exe_qry = $dbobj->select($sql);
        while ($row = $dbobj->fetch_array($exe_qry))
        {
            $fee_det[$i]['feeid'] = $row['id'];
            $fee_det[$i]['sid'] = $row['sid'];
            $fee_det[$i]['fee_name'] = $row['fee_name'];
            $fee_det[$i]['amount'] = $row['amount'];
            $fee_det[$i]['due_date'] = $row['due_date'];
            $fee_det[$i]['classid'] = $row['classid'];
            $fee_det[$i]['discount'] = $row['discount'];
            $fee_det[$i]['reason'] = $row['reason'];
            $fee_det[$i]['paid_date'] = $row['paid_date'];
            $fee_det[$i]['ac_year'] = $row['ac_year'];
            $fee_det[$i]['flag'] = $row['flag'];
            $fee_det[$i]['feeppaymentmode'] = $row['feeppaymentmode'];
            $i++;
        }
        return $fee_det;
    }
    function academic_feehead_amount($classno, $feetype, $group, $term, $acyear)
    {
        $dbobj = new DB();
        $dbobj->getCon();
        $sql = "SELECT * FROM `generalfee` WHERE `classno`='" . $classno . "' and `fee_type`='" . $feetype . "' and `group`='" . $group . "' and `term`='" . $term . "' and `acyear`='" . $acyear . "' ORDER BY FIELD(`feename`,'ADMISSION FEE','TUITION FEE','SPECIAL FEES','ESTABLISHMENT FEE','STAFF WELFARE FUND ','DIGITAL CLASS FEE ','EXAMINATION FEE','LIBRARY FEE','LABORATORY FEE','SPORTS FEE','CELEBRATION FEE','CULTURAL ACTIVITY FEE','STAMP FEE','EXTRA CURRICULAR ACTIVITY FEE ','PTA FUND','MISCELLANEOUS FEE')";
        $qry = $dbobj->select($sql);
        $i = 0;
        while ($row = $dbobj->fetch_array($qry))
        {
            $i++;
            $fee_det[$i]['feename'] = $row['feename'];
            $fee_det[$i]['amount'] = $row['amount'];
            $fee_det[$i]['duedate'] = $row['duedate'];
        }

        return $fee_det;
    }
    function unpaid_academic_fee($studentid)
    {
        $dbobj = new DB();
        $dbobj->getCon();
        $student_det = $dbobj->selectall("student", array(
            "studentid" => $studentid
        ));
        $sid = $student_det['sid'];
        $sql = "SELECT * FROM `student_academic_fee` WHERE `sid`='" . $sid . "' and `flag`='false'";
        $qry = $dbobj->select($sql);
        $i = 0;
        while ($row = $dbobj->fetch_array($qry))
        {

            $amount = $this->general_fee_term($row['feeppaymentmode'], $row['ac_year'], $row['classid'], $row['fee_name'], "none");
            $discount = $this->get_studentDiscount($sid, $row['id']);
            $student_class = $dbobj->selectall("student_class", array(
                "sid" => $sid,
                "acyear" => $row['ac_year']
            ));
            $class_det = $dbobj->selectall("sclass", array(
                "classid" => $student_class['classid']
            ));
            $fee_det[$i]['id'] = $row['id'];
            $fee_det[$i]['fee_name'] = $row['fee_name'];
            $fee_det[$i]['amount'] = $amount['amount'] - $discount;
            $fee_det[$i]['acyear'] = $row['ac_year'];
            $fee_det[$i]['class'] = $class_det['classname'] . " " . $class_det['division'];
            $i++;

        }

        return $fee_det;
    }
    function fee_report_instalment_mode()
    {
        $dbobject = new DB();
        $dbobject->getCon();
        $sql = "SELECT distinct `type` FROM `fee_term` ";
        $qry = $dbobject->select($sql);
        while ($row = $dbobject->fetch_array($qry))
        {
			$data[$row['type']] = $row['type'];			
        }
		$data['FIXED']='CUSTOM';
        //$data['ONETIME']="ONETIME";
        //$data['QUARTERLY']="QUARTERLY";
        //$data['MONTHLY']="MONTHLY";
        //$data['FORTNIGHTLY']="FORTNIGHTLY";
        //$data['WEEKLY']="WEEKLY";
        return $data;
    }
    function get_fee_type()
    {
        $data['academic'] = 'Academic';
        $data['misc'] = 'Miscellaneous';
        return $data;
    }
    function get_fee_type_head_name($acyear, $feehead_type, $fee_type)
    {
        $dbobject = new DB();
        $dbobject->getCon();
        $sql = "SELECT distinct `head_name` FROM `fee_head` WHERE `acyear`='" . $acyear . "'";
        if ($feehead_type != "" && $feehead_type != "-1")
        {
            $sql .= " and `feehead_type`='" . $feehead_type . "' ";
        }
        if ($fee_type != "" && $fee_type != "-1")
        {
            $sql .= "and `fee_type`='" . $fee_type . "'";
        }
         $sql.=" and `duedate`='on'";
        //echo $sql.="order by `head_name`";
        $qry = $dbobject->select($sql);
        while ($row = $dbobject->fetch_array($qry))
        {
            $data[$row['head_name']] = $row['head_name'];
        }
        return $data;
    }
    
    function GetClassFee($params){
        $dbobject = new DB();
        $dbobject->getCon();
        $sel_amount = $dbobject->select("select * from `fee_head_det` where `classno`='" . $params['classno'] . "'  order by `id`");
        $total = 0;
        $disamnt = 0;
        while ($amount_row = $dbobject->fetch_array($sel_amount))
        {

            $total = $total + $amount_row['amount'];

        }
        return $total;
    }
    function GetClassFeeWithhead($params){
        $dbobject = new DB();
        $dbobject->getCon();
        $sel_amount = $dbobject->select("select * from `fee_head_det`  where `classno`='" . $params['classno'] . "'  order by `id`");
        $total = 0;
        $disamnt = 0;
        $fee_head=array();
        $i=0;
        while ($amount_row = $dbobject->fetch_array($sel_amount))
        {
            $fee_head[$i]['id']=$amount_row['id'];
            $fee_head[$i]['head_id']=$amount_row['head_id'];
            $fee_head[$i]['head_type']=$amount_row['head_type'];
            $fee_head[$i]['classno']=$amount_row['classno'];
            $fee_head[$i]['amount']=$amount_row['amount'];
            $i++;
        }
        return $fee_head;
    }
    function getFeeHeadByparent($parentid,$acyear,$students,$duedate){
        $dbobject = new DB();
        $dbobject->getCon();
        $fee_head=array();
        $count=count($students);
        $j=0;
        $sql_class="select distinct `head_id`,`head_name`,`fee_type`,`class_type`,`feemod`,`start_date`,`end_date`,`distype`,`rate`,`duration` from `fee_head_class` INNER JOIN `fee_head` on `fee_head_class`.`head_id`=`fee_head`.`id` where";
        $i=1;
        if(!empty($students)){
            $sql_class.=" `classid` IN (";
        foreach($students as $s){
            $sql_class.="'".$s['current_class']."'";
            if($i<$count){
                $sql_class.=",";
            }
            $i++;
        }
        $sql_class.=") AND ";
    }
    $a=1;
    $b=1;
    if(!empty($students)){
        $sql_class.=" `fee_type` IN (";
        foreach($students as $s){
            if($s['student_type']=='other'){
                if($b>1){
                    $sql_class.=",";
                }
                $sql_class.="'db_student'";
            }else{
                if($b>1){
                    $sql_class.=","; 
                }
                $sql_class.="'db_student'";
            }
            $b++;
        }
        $sql_class.=") AND ";
       }
        $sql_class.="`acyear`='".$acyear."'";
        if($duedate=="on"){
            $sql_class.=" AND `fee_head`.`duedate`='on'";
        }elseif($duedate=="off"){
            $sql_class.=" AND `fee_head`.`duedate`=''";
        }
		$sel_class=$dbobject->select($sql_class);
        while ($class_row = $dbobject->fetch_array($sel_class)){
            $fee_head[$j]['id']=$class_row['head_id'];
            $fee_head[$j]['head_name']=$class_row['head_name'];
            $fee_head[$j]['fee_type']=$class_row['fee_type'];
            $fee_head[$j]['class_type']=$class_row['class_type'];
            $fee_head[$j]['feemod']=$class_row['feemod'];
            $fee_head[$j]['start_date']=$class_row['start_date'];
            $fee_head[$j]['end_date']=$class_row['end_date'];
            $fee_head[$j]['distype']=$class_row['distype'];
            $fee_head[$j]['rate']=$class_row['rate'];
            $fee_head[$j]['duration']=$class_row['duration'];
            $j++;
         }
         $sql_family_model="select distinct `headid`,`head_name`,`fee_type`,`class_type`,`feemod`,`start_date`,`end_date`,`distype`,`rate`,`duration` from `family_model_fee` INNER JOIN `fee_head` on `family_model_fee`.`headid`=`fee_head`.`id` where `fee_head`.`acyear`='".$acyear."' AND `feemod`!='individual'";
         $b=1;
         if(!empty($students)){
             $sql_family_model.="AND `fee_type` IN (";
             foreach($students as $s){
                if($s['student_type']=='other'){
                    if($b>1){
                        $sql_family_model.=",";
                    }
                    $sql_family_model.="'db_student'";
                }else{
                    if($b>1){
                        $sql_family_model.=",";
                    }
                    $sql_family_model.="'db_student'";
                }
                /* $sql_family_model.="'".$s['student_type']."'";*/
    
                 $b++;
             }
             $sql_family_model.=") AND";
            }
            $a=1;
            if(!empty($students)){
                $sql_family_model.=" `feehead_mode` IN (";
                foreach($students as $s){
                    $sql_family_model.="'".$s['visa_type']."'";
                    if($a<$count){
                        $sql_family_model.=",";
                    }
                    $a++;
                }
                $sql_family_model.=") ";
            }
         if($duedate=="on"){
            $sql_family_model.="AND `fee_head`.`duedate`='on'";
        }else{
            $sql_family_model.="AND `fee_head`.`duedate`=''";
        }
         $sql_family_model=$dbobject->select($sql_family_model);
         while ($family_model_row = $dbobject->fetch_array($sql_family_model)){
            $fee_head[$j]['id']=$family_model_row['headid'];
            $fee_head[$j]['head_name']=$family_model_row['head_name'];
            $fee_head[$j]['fee_type']=$family_model_row['fee_type'];
            $fee_head[$j]['class_type']=$family_model_row['class_type'];
            $fee_head[$j]['feemod']=$family_model_row['feemod'];
            $fee_head[$j]['start_date']=$family_model_row['start_date'];
            $fee_head[$j]['end_date']=$family_model_row['end_date'];
            $fee_head[$j]['distype']=$family_model_row['distype'];
            $fee_head[$j]['rate']=$family_model_row['rate'];
            $fee_head[$j]['duration']=$family_model_row['duration'];
            $j++;
         }
         $sql_family_model2="select distinct `headid`,`head_name`,`fee_type`,`class_type`,`feemod`,`start_date`,`end_date`,`distype`,`rate`,`duration` from `family_model_fee2` INNER JOIN `fee_head` on `family_model_fee2`.`headid`=`fee_head`.`id` where `fee_head`.`acyear`='".$acyear."'";
         $b=1;
         if(!empty($students)){
             $sql_family_model2.="AND `fee_type` IN (";
             foreach($students as $s){
                if($s['student_type']=='other'){
                    if($b>1){
                        $sql_family_model2.=",";
                    }
                    $sql_family_model2.="'db_student'";
                }else{
                    if($b>1){
                        $sql_family_model2.=",";
                    }
                    $sql_family_model2.="'db_student'";
                }
                /* $sql_family_model2.="'".$s['student_type']."'";
                 if($b<$count){
                     $sql_family_model2.=",";
                 }*/
                 $b++;
             }
             $sql_family_model2.=") ";
            }
         if($duedate=="on"){
            $sql_family_model2.="AND `fee_head`.`duedate`='on'";
        }else{
            $sql_family_model2.="AND `fee_head`.`duedate`=''";
        }
         $sql_family_model2=$dbobject->select($sql_family_model2);
         while ($family_model_row2 = $dbobject->fetch_array($sql_family_model2)){
            $fee_head[$j]['id']=$family_model_row2['headid'];
            $fee_head[$j]['head_name']=$family_model_row2['head_name'];
            $fee_head[$j]['fee_type']=$family_model_row2['fee_type'];
            $fee_head[$j]['class_type']=$family_model_row2['class_type'];
            $fee_head[$j]['feemod']=$family_model_row2['feemod'];
            $fee_head[$j]['start_date']=$family_model_row2['start_date'];
            $fee_head[$j]['end_date']=$family_model_row2['end_date'];
            $fee_head[$j]['distype']=$family_model_row2['distype'];
            $fee_head[$j]['rate']=$family_model_row2['rate'];
            $fee_head[$j]['duration']=$family_model_row2['duration'];
            $j++;
         }
         $sql_IndiFee="select distinct `fee_head_id`,`head_name`,`fee_type`,`class_type`,`feemod`,`start_date`,`end_date`,`distype`,`rate`,`duration` from `fee_head_student` INNER JOIN `fee_head` on `fee_head_student`.`fee_head_id`=`fee_head`.`id` where `fee_head`.`acyear`='".$acyear."'";

         $mj=1;
         if(!empty($students)){
            $sql_IndiFee.=" AND `sid` IN (";
         foreach($students as $s){
             $sql_IndiFee.="'".$s['sid']."'";
             if($mj<$count){
                 $sql_IndiFee.=",";
             }
             $mj++;
         }
         $sql_IndiFee.=")";
        }
         if($duedate=="on"){
            $sql_IndiFee.="AND `fee_head`.`duedate`='on'";
        }else{
            $sql_IndiFee.="AND `fee_head`.`duedate`=''";
        }
         $sql_family_model2=$dbobject->select($sql_IndiFee);
         while ($family_model_row2 = $dbobject->fetch_array($sql_family_model2)){
            $fee_head[$j]['id']=$family_model_row2['fee_head_id'];
            $fee_head[$j]['head_name']=$family_model_row2['head_name'];
            $fee_head[$j]['fee_type']=$family_model_row2['fee_type'];
            $fee_head[$j]['class_type']=$family_model_row2['class_type'];
            $fee_head[$j]['feemod']=$family_model_row2['feemod'];
            $fee_head[$j]['start_date']=$family_model_row2['start_date'];
            $fee_head[$j]['end_date']=$family_model_row2['end_date'];
            $fee_head[$j]['distype']=$family_model_row2['distype'];
            $fee_head[$j]['rate']=$family_model_row2['rate'];
            $fee_head[$j]['duration']=$family_model_row2['duration'];
            $j++;
         }
         $fee_head = array_map("unserialize", array_unique(array_map("serialize", $fee_head)));
         usort($fee_head, function($a, $b) {
           return $a['id'] - $b['id'];
        });
        return $fee_head;
        //`classno`='" . $params['classno'] . "'  order by `id`";
    }
    function getFeeHeadByparent_new($parentid,$acyear,$students,$duedate){
        $dbobject = new DB();
        $dbobject->getCon();
        $fee_head=array();
        if(!empty($students)){
            $count=count($students);
        }else{
            $count=0;
        }
        
        $j=0;
        $sql_class="select distinct `head_id`,`head_name`,`fee_type`,`class_type`,`feemod`,`start_date`,`end_date`,`distype`,`rate`,`duration` from `fee_head_class` INNER JOIN `fee_head` on `fee_head_class`.`head_id`=`fee_head`.`id` where";
        $i=1;
        if(!empty($students)){
            $sql_class.=" `classid` IN (";
        foreach($students as $s){
            $sql_class.="'".$s['current_class']."'";
            if($i<$count){
                $sql_class.=",";
            }
            $i++;
        }
        $sql_class.=") AND ";
    }
    $a=1;
    $b=1;
    if(!empty($students)){
        $sql_class.=" `fee_type` IN (";
        foreach($students as $s){
            if($s['student_type']=='other'){
                if($b>1){
                    $sql_class.=",";
                }
                $sql_class.="'other'";
            }else{
                if($b>1){
                    $sql_class.=","; 
                }
                $sql_class.="'other'";
            }
            $b++;
        }
        $sql_class.=") AND ";
       }
        $sql_class.="`acyear`='".$acyear."'";
        if($duedate=="on"){
            $sql_class.=" AND `fee_head`.`duedate`='on'";
        }elseif($duedate=="off"){
            $sql_class.=" AND `fee_head`.`duedate`=''";
        }
		$sel_class=$dbobject->select($sql_class);
        while ($class_row = $dbobject->fetch_array($sel_class)){
            $fee_head[$j]['id']=$class_row['head_id'];
            $fee_head[$j]['head_name']=$class_row['head_name'];
            $fee_head[$j]['fee_type']=$class_row['fee_type'];
            $fee_head[$j]['class_type']=$class_row['class_type'];
            $fee_head[$j]['feemod']=$class_row['feemod'];
            $fee_head[$j]['start_date']=$class_row['start_date'];
            $fee_head[$j]['end_date']=$class_row['end_date'];
            $fee_head[$j]['distype']=$class_row['distype'];
            $fee_head[$j]['rate']=$class_row['rate'];
            $fee_head[$j]['duration']=$class_row['duration'];
            $j++;
         }
         $sql_family_model="select distinct `headid`,`head_name`,`fee_type`,`class_type`,`feemod`,`start_date`,`end_date`,`distype`,`rate`,`duration` from `family_model_fee` INNER JOIN `fee_head` on `family_model_fee`.`headid`=`fee_head`.`id` where `fee_head`.`acyear`='".$acyear."' AND `feemod`!='individual'";
         $b=1;
         if(!empty($students)){
             $sql_family_model.="AND `fee_type` IN (";
             foreach($students as $s){
                if($s['student_type']=='other'){
                    if($b>1){
                        $sql_family_model.=",";
                    }
                    $sql_family_model.="'other'";
                }else{
                    if($b>1){
                        $sql_family_model.=",";
                    }
                    $sql_family_model.="'other'";
                }
                /* $sql_family_model.="'".$s['student_type']."'";*/
    
                 $b++;
             }
             $sql_family_model.=") AND";
            }
            $a=1;
            if(!empty($students)){
                $sql_family_model.=" `feehead_mode` IN (";
                foreach($students as $s){
                    $sql_family_model.="'".$s['visa_type']."'";
                    if($a<$count){
                        $sql_family_model.=",";
                    }
                    $a++;
                }
                $sql_family_model.=") ";
            }
         if($duedate=="on"){
            $sql_family_model.="AND `fee_head`.`duedate`='on'";
        }else{
            $sql_family_model.="AND `fee_head`.`duedate`=''";
        }
         $sql_family_model=$dbobject->select($sql_family_model);
         while ($family_model_row = $dbobject->fetch_array($sql_family_model)){
            $fee_head[$j]['id']=$family_model_row['headid'];
            $fee_head[$j]['head_name']=$family_model_row['head_name'];
            $fee_head[$j]['fee_type']=$family_model_row['fee_type'];
            $fee_head[$j]['class_type']=$family_model_row['class_type'];
            $fee_head[$j]['feemod']=$family_model_row['feemod'];
            $fee_head[$j]['start_date']=$family_model_row['start_date'];
            $fee_head[$j]['end_date']=$family_model_row['end_date'];
            $fee_head[$j]['distype']=$family_model_row['distype'];
            $fee_head[$j]['rate']=$family_model_row['rate'];
            $fee_head[$j]['duration']=$family_model_row['duration'];
            $j++;
         }
         $sql_family_model2="select distinct `headid`,`head_name`,`fee_type`,`class_type`,`feemod`,`start_date`,`end_date`,`distype`,`rate`,`duration` from `family_model_fee2` INNER JOIN `fee_head` on `family_model_fee2`.`headid`=`fee_head`.`id` where `fee_head`.`acyear`='".$acyear."'";
         $b=1;
         if(!empty($students)){
             $sql_family_model2.="AND `fee_type` IN (";
             foreach($students as $s){
                if($s['student_type']=='other'){
                    if($b>1){
                        $sql_family_model2.=",";
                    }
                    $sql_family_model2.="'other'";
                }else{
                    if($b>1){
                        $sql_family_model2.=",";
                    }
                    $sql_family_model2.="'other'";
                }
                /* $sql_family_model2.="'".$s['student_type']."'";
                 if($b<$count){
                     $sql_family_model2.=",";
                 }*/
                 $b++;
             }
             $sql_family_model2.=") ";
            }
         if($duedate=="on"){
            $sql_family_model2.="AND `fee_head`.`duedate`='on'";
        }else{
            $sql_family_model2.="AND `fee_head`.`duedate`=''";
        }
         $sql_family_model2=$dbobject->select($sql_family_model2);
         while ($family_model_row2 = $dbobject->fetch_array($sql_family_model2)){
            $fee_head[$j]['id']=$family_model_row2['headid'];
            $fee_head[$j]['head_name']=$family_model_row2['head_name'];
            $fee_head[$j]['fee_type']=$family_model_row2['fee_type'];
            $fee_head[$j]['class_type']=$family_model_row2['class_type'];
            $fee_head[$j]['feemod']=$family_model_row2['feemod'];
            $fee_head[$j]['start_date']=$family_model_row2['start_date'];
            $fee_head[$j]['end_date']=$family_model_row2['end_date'];
            $fee_head[$j]['distype']=$family_model_row2['distype'];
            $fee_head[$j]['rate']=$family_model_row2['rate'];
            $fee_head[$j]['duration']=$family_model_row2['duration'];
            $j++;
         }
         $sql_IndiFee="select distinct `fee_head_id`,`head_name`,`fee_type`,`class_type`,`feemod`,`start_date`,`end_date`,`distype`,`rate`,`duration` from `fee_head_student` INNER JOIN `fee_head` on `fee_head_student`.`fee_head_id`=`fee_head`.`id` where `fee_head`.`acyear`='".$acyear."'";

         $mj=1;
         if(!empty($students)){
            $sql_IndiFee.=" AND `sid` IN (";
         foreach($students as $s){
             $sql_IndiFee.="'".$s['sid']."'";
             if($mj<$count){
                 $sql_IndiFee.=",";
             }
             $mj++;
         }
         $sql_IndiFee.=")";
        }
         if($duedate=="on"){
            $sql_IndiFee.="AND `fee_head`.`duedate`='on'";
        }else{
            $sql_IndiFee.="AND `fee_head`.`duedate`=''";
        }
         $sql_family_model2=$dbobject->select($sql_IndiFee);
         while ($family_model_row2 = $dbobject->fetch_array($sql_family_model2)){
            $fee_head[$j]['id']=$family_model_row2['fee_head_id'];
            $fee_head[$j]['head_name']=$family_model_row2['head_name'];
            $fee_head[$j]['fee_type']=$family_model_row2['fee_type'];
            $fee_head[$j]['class_type']=$family_model_row2['class_type'];
            $fee_head[$j]['feemod']=$family_model_row2['feemod'];
            $fee_head[$j]['start_date']=$family_model_row2['start_date'];
            $fee_head[$j]['end_date']=$family_model_row2['end_date'];
            $fee_head[$j]['distype']=$family_model_row2['distype'];
            $fee_head[$j]['rate']=$family_model_row2['rate'];
            $fee_head[$j]['duration']=$family_model_row2['duration'];
            $j++;
         }
         $fee_head = array_map("unserialize", array_unique(array_map("serialize", $fee_head)));
         usort($fee_head, function($a, $b) {
           return $a['id'] - $b['id'];
        });
        return $fee_head;
        //`classno`='" . $params['classno'] . "'  order by `id`";
    }
    function getHeadAmountIndividual($sid,$classid,$head_id,$acyear){
        $dbobject = new DB();
        $dbobject->getCon();
        $total=0;
        $sql_class=$dbobject->select("SELECT `amount` FROM `fee_head_class` WHERE `classid`='".$classid."' AND `head_id`='".$head_id."'");
        $class_row=$dbobject->fetch_array($sql_class);
        if(!empty($class_row)){
            $total=$total+$class_row['amount'];
        }else{
            $sql_ind=$dbobject->select("SELECT `amount` FROM `fee_head_student` WHERE `sid`='".$sid."' AND `fee_head_id`='".$head_id."'");
            $ind_row=$dbobject->fetch_array($sql_ind);
            $total=$total+$ind_row['amount'];
        }
       return $total;
    }
    function getHeadDiscount($sid,$classid,$head_id,$acyear){
        $dbobject = new DB();
        $dbobject->getCon();
        $total=0;
        $sql_class=$dbobject->select("SELECT `discount_tbl`.`discount_amount` FROM `discount_tbl`  WHERE  `sid`='".$sid."' AND `feetype`='individual' AND `headid`='".$head_id."'");
        $class_row=$dbobject->fetch_array($sql_class);
        if(!empty($class_row)){
            $val=0;
            $total=$total+$class_row['discount_amount'];
        }
       return $total;
    }
    function getHeadAmountIndividualWithDiscount($sid,$classid,$head_id,$acyear){
        $dbobject = new DB();
        $dbobject->getCon();
        $total=0;
        $sql_ind=$dbobject->select("SELECT `amount` FROM `student_academic_fee`  WHERE `sid`='".$sid."' AND `feehead_id`='".$head_id."' AND `ac_year`='".$acyear."'");
        $ind_row=$dbobject->fetch_array($sql_ind);
        if(!empty($ind_row)){
            //$total=$total+$ind_row['amount'];
        }
        $sql_class=$dbobject->select("SELECT `discount_tbl`.`discount_amount` AS `discount_rate` FROM `discount_tbl`  WHERE  `sid`='".$sid."' AND `feetype`='individual' AND `headid`='0'");
        $class_row=$dbobject->fetch_array($sql_class);
        if(!empty($class_row)){
            $val=0;
            $total=$total+$class_row['discount_rate'];
        }
       return $total;
    }
    function getFamilyTreeFee($sid,$i,$classid,$head_id,$class_type,$acyear){
        $dbobject = new DB();
        $dbobject->getCon();
        $fee=0;
        if($class_type=="Section"){
     //   $student_det=$dbobject->selectall("student",array("sid"=>$sid));
        $student_class=$dbobject->selectall("student_class",array("sid"=>$sid,"acyear"=>$acyear));
        $sel_classDet=$dbobject->select("SELECT `classid`,`sectionid`,`section` FROM `sclass` LEFT JOIN `sclass_section` on `sclass`.`sectionid`=`sclass_section`.`id` WHERE `classid`='".$student_class['classid']."'");
        $classDet_row=$dbobject->fetch_array($sel_classDet);
        $sql="SELECT `first`,`second`,`third`,`fourth`,`five`,`six` FROM `family_model_fee` WHERE `headid`='".$head_id."' AND `acyear`='".$acyear."' AND `section_no`='".$classDet_row['section']."'";
        }elseif($class_type=="Class"){
           // $student_det=$dbobject->selectall("student",array("sid"=>$sid));
           $student_class=$dbobject->selectall("student_class",array("sid"=>$sid,"acyear"=>$acyear));
            $sel_classDet=$dbobject->select("SELECT `classno` FROM `sclass`  WHERE `classid`='".$student_class['classid']."'");
            $classDet_row=$dbobject->fetch_array($sel_classDet);
            $sql="SELECT `first`,`second`,`third`,`fourth`,`five`,`six` FROM `family_model_fee` WHERE `headid`='".$head_id."' AND `acyear`='".$acyear."' AND `section_no`='".$classDet_row['classno']."'";
        }elseif($class_type=="Homeroom"){
           // $student_det=$dbobject->selectall("student",array("sid"=>$sid));
           $student_class=$dbobject->selectall("student_class",array("sid"=>$sid,"acyear"=>$acyear));
            $sql="SELECT `first`,`second`,`third`,`fourth`,`five`,`six` FROM `family_model_fee` WHERE `headid`='".$head_id."' AND `acyear`='".$acyear."' AND `section_no`='".$student_class['classid']."'";
        }
        else{
            $sql="SELECT `first`,`second`,`third`,`fourth`,`five`,`six` FROM `family_model_fee` WHERE `headid`='".$head_id."' AND `acyear`='".$acyear."'";
        }
        $sql_head=$dbobject->select($sql);
        $head_row=$dbobject->fetch_array($sql_head);
        if($i==0){
            $fee=$head_row['first'];
        }
        else if($i==1){
            $fee=$head_row['second'];
        }
        else if($i==2){
            $fee=$head_row['third'];
        }
        else if($i==3){
            $fee=$head_row['fourth'];
        }
        else if($i==4){
            $fee=$head_row['five'];
        }
        else if($i==5){
            $fee=$head_row['six'];
        }
        return $fee;
    }
   function getFamilyFee($sid,$i,$classid,$head_id,$acyear){
    $dbobject = new DB();
    $dbobject->getCon();
    $fee=0;
    $sql_head=$dbobject->select("SELECT `amount` FROM `family_model_fee2` WHERE `headid`='".$head_id."' AND `amount`!=''");
    $head_row=$dbobject->fetch_array($sql_head);
    if(!empty($head_row)){
        $fee=$head_row['amount'];
    }
    return $fee;
   }
    function getFamilyDiscountFee($family_id,$acyear){
        $dbobject = new DB();
        $dbobject->getCon();
        $fee=0;
        $sql_head=$dbobject->select("SELECT `amount` FROM `family_model_fee2`");
        while($head_row=$dbobject->fetch_array($sql_head)){
            $fee=$head_row['amount'];
         }
        $sql_head=$dbobject->select("SELECT `discount_tbl`.`discount_amount` AS `discount_rate` FROM `discount_tbl`  WHERE  `family_id`='".$family_id."' AND `feetype`='family' AND `headid`='0'");
        $head_row=$dbobject->fetch_array($sql_head);
        if(!empty($head_row)){
          $fee=$fee-$head_row['discount_rate'];
        }
        return $fee;
    }
    function getInstallmentByparent($parentid,$acyear){
        $dbobject = new DB();
        $dbobject->getCon();
        $sel= $dbobject->select("SELECT distinct `fee_name` FROM  `custom_academic_fee` WHERE `parent_id`='".$parentid."' AND `feehead_id`='' AND `ac_year`='".$acyear."' order by `id`");
        $i=0;
        $res=array();
        while($row=$dbobject->fetch_array($sel)){
            $res[$i]=$row['fee_name'];
            $i++;
        }
        return $res;
    }
    function getInstallmentByparentForHead($parentid,$headid,$acyear){
        $dbobject = new DB();
        $dbobject->getCon();
        $sel= $dbobject->select("SELECT distinct `fee_name` FROM  `custom_academic_fee` WHERE `parent_id`='".$parentid."' AND `feehead_id`='".$headid."' AND `ac_year`='".$acyear."' order by `id`");
        $i=0;
        $res=array();
        while($row=$dbobject->fetch_array($sel)){
            $res[$i]=$row['fee_name'];
            $i++;
        }
        return $res;
    }
    function getInstallmentByparentWithFee($parentid,$acyear,$headid){
        $dbobject = new DB();
        $dbobject->getCon();
        $sql="SELECT * FROM  `student_academic_fee` WHERE `parent_id`='".$parentid."' AND `ac_year`='".$acyear."'";
            $sql.=" AND `feehead_id`='".$headid."'";
        $sql.=" order by `id`";
        $sel= $dbobject->select($sql);
        $i=0;
        $res=array();
        while($row=$dbobject->fetch_array($sel)){
            $res[$i]['id']=$row['id'];
            $res[$i]['fee_name']=$row['fee_name'];
            $res[$i]['amount']=$row['amount'];
            $res[$i]['instalment_mode']=$row['instalment_mode'];
            $res[$i]['feehead_id']=$row['feehead_id'];
            $res[$i]['due_date']=$row['due_date'];
            $res[$i]['instid']=$row['instid'];
            $res[$i]['flag']=$row['flag'];
            $i++;
        }
        return $res;
    }
    function getInstallmentByparentWithFeeGroup($parentid,$acyear,$headid){
        $dbobject = new DB();
        $dbobject->getCon();
        $sql="SELECT * FROM  `student_academic_fee` WHERE `parent_id`='".$parentid."' AND `ac_year`='".$acyear."'";
            $sql.=" AND `feehead_id`='".$headid."'";
        $sql.=" order by `id`";
        $sel= $dbobject->select($sql);
        $i=0;
        $j=0;
        $res=array();
        while($row=$dbobject->fetch_array($sel)){
            if($row['flag']=='true'){
                $res['paid'][$j]['id']=$row['id'];
                $res['paid'][$j]['fee_name']=$row['fee_name'];
                $res['paid'][$j]['amount']=$row['amount'];
                $res['paid'][$j]['instalment_mode']=$row['instalment_mode'];
                $res['paid'][$j]['feehead_id']=$row['feehead_id'];
                $res['paid'][$j]['due_date']=$row['due_date'];
                $res['paid'][$j]['instid']=$row['instid'];
                $res['paid'][$j]['dis_type']=$row['dis_type'];
                $res['paid'][$j]['dis_value']=$row['dis_value'];
                $res['paid'][$j]['discount']=$row['discount'];
                $res['paid'][$j]['reason']=$row['reason'];
                $res['paid'][$j]['flag']=$row['flag'];
                $j++;
            }else{
                $res['unpaid'][$i]['id']=$row['id'];
                $res['unpaid'][$i]['fee_name']=$row['fee_name'];
                $res['unpaid'][$i]['amount']=$row['amount'];
                $res['unpaid'][$i]['instalment_mode']=$row['instalment_mode'];
                $res['unpaid'][$i]['feehead_id']=$row['feehead_id'];
                $res['unpaid'][$i]['due_date']=$row['due_date'];
                $res['unpaid'][$i]['instid']=$row['instid'];
                $res['unpaid'][$i]['dis_type']=$row['dis_type'];
                $res['unpaid'][$i]['dis_value']=$row['dis_value'];
                $res['unpaid'][$i]['discount']=$row['discount'];
                $res['unpaid'][$i]['reason']=$row['reason'];
                $res['unpaid'][$i]['flag']=$row['flag'];
                $i++;
            }
        }
        return $res;
    }
	  function fee_report_instalment_mode_name($instalment_mode)
	  {
		  if($instalment_mode=="FIXED")
		  {
			  $name="CUSTOM";
		  }
		  else
		  {
			$name=$instalment_mode;  
		  }
		  return $name;
	  }    
      function termly_fee_instalment()
	  {
        $dbobject = new DB();
        $dbobject->getCon();		  
		$sql="SELECT distinct `term_name` FROM `terms` order by `term_name`";
		$qry=$dbobject->select($sql);
		while($row=$dbobject->fetch_array($qry))
		{
			$data[$row['term_name']]=$row['term_name'];
		}
		return $data;
	  }	  
      function termly_fee_instalment_date($term_name)
	  {
        $dbobject = new DB();
        $dbobject->getCon();		  
		$sql="SELECT  * FROM `terms` where `term_name`='".$term_name."' order by `smonth`";
		$qry=$dbobject->select($sql);
		$row=$dbobject->fetch_array($qry);
		$data['smonth']=$row['smonth'];
		$data['emonth']=$row['emonth'];
		return $data;
	  }	
      function monthly_fee_instalment()
	  {
 
        $instalment_array=$this->termly_fee_instalment();
		if(!empty($instalment_array))
		{
			foreach($instalment_array as $key=>$instalment)
			{	
		$termly_fee_instalment_date=$this->termly_fee_instalment_date($key);
		$start_date=$termly_fee_instalment_date['smonth'];
		$end_date=$termly_fee_instalment_date['emonth'];
		if($start_date!="" && $start_date!="0000-00-00" && $end_date!="" && $end_date!="0000-00-00" )
		{
			$start   = new DateTime($start_date);
			$start->modify('first day of this month');
			$end      = new DateTime($end_date);
			$end->modify('first day of next month');
			$interval = DateInterval::createFromDateString('1 month');
			$period   = new DatePeriod($start, $interval, $end);
			foreach ($period as $dt)
			 {
				$month=$dt->format("F");
				$data[$month]=$month;
			 }
			 
		}
			}
		}		
		return $data;
	  }
      function getTotalDiscount($studentList,$heads,$family_id,$acyear){
        $dbobject = new DB();
        $dbobject->getCon();
        $total_discount=array();
          if(!empty($studentList)){
              foreach($studentList as $s){
                 $sel=$dbobject->select("SELECT * FROM `discount_tbl`  WHERE `sid`='".$s['sid']."' AND `headid`='0' AND `feetype`='individual' AND `acyear`='".$acyear."'");
                 $row=$dbobject->fetch_array($sel);
                  if(!empty($row)){  
                        $total_discount['indivitual'][$s['sid']]['feetype']=$row['feetype'];
                        $total_discount['indivitual'][$s['sid']]['type']=$row['type'];
                        $total_discount['indivitual'][$s['sid']]['headid']=$row['headid'];
                        $total_discount['indivitual'][$s['sid']]['amount']=$row['amount'];
                        $total_discount['indivitual'][$s['sid']]['ptype']=$row['ptype'];
                        $total_discount['indivitual'][$s['sid']]['discount_amount']=$row['discount_amount'];
                        $total_discount['indivitual'][$s['sid']]['reason']=$row['reason'];
                  }
                  else{
                    $total_discount['indivitual'][$s['sid']]['feetype']="";
                    $total_discount['indivitual'][$s['sid']]['type']="";
                    $total_discount['indivitual'][$s['sid']]['headid']="";
                    $total_discount['indivitual'][$s['sid']]['amount']="";
                    $total_discount['indivitual'][$s['sid']]['ptype']="";
                    $total_discount['indivitual'][$s['sid']]['reason']="";
                    $total_discount['indivitual'][$s['sid']]['discount_amount']="";
                  }
                  if(!empty($heads)){
                      foreach($heads as $h){
                        $sel_head=$dbobject->select("SELECT * FROM `discount_tbl` WHERE `sid`='".$s['sid']."' AND `feetype`='individual' AND `headid`='".$h['id']."' AND `acyear`='".$acyear."'");
                        $head_row=$dbobject->fetch_array($sel_head);
                        if(!empty($head_row)){
                            $total_discount['head'][$s['sid']][$h['id']]['feetype']=$head_row['feetype'];
                            $total_discount['head'][$s['sid']][$h['id']]['type']=$head_row['type'];
                            $total_discount['head'][$s['sid']][$h['id']]['headid']=$head_row['headid'];
                            $total_discount['head'][$s['sid']][$h['id']]['amount']=$head_row['amount'];
                            $total_discount['head'][$s['sid']][$h['id']]['ptype']=$head_row['ptype'];
                            $total_discount['head'][$s['sid']][$h['id']]['reason']=$head_row['reason'];
                            $total_discount['head'][$s['sid']][$h['id']]['discount_amount']=$head_row['discount_amount'];
                        }else{
                            $total_discount['head'][$s['sid']][$h['id']]['feetype']="";
                            $total_discount['head'][$s['sid']][$h['id']]['type']="";
                            $total_discount['head'][$s['sid']][$h['id']]['headid']="";
                            $total_discount['head'][$s['sid']][$h['id']]['amount']="";
                            $total_discount['head'][$s['sid']][$h['id']]['ptype']="";
                            $total_discount['head'][$s['sid']][$h['id']]['reason']="";
                            $total_discount['head'][$s['sid']][$h['id']]['discount_amount']="";
                        }
                      }
                  }
              }
          }
          $sel_family=$dbobject->select("SELECT * FROM `discount_tbl` WHERE `family_id`='".$family_id."' and `headid`='0' and `feetype`='family' AND `acyear`='".$acyear."'");
          $family_row=$dbobject->fetch_array($sel_family);
          if(!empty($family_row)){
            $total_discount['family']['feetype']=$family_row['feetype'];
            $total_discount['family']['type']=$family_row['type'];
            $total_discount['family']['headid']=$family_row['headid'];
            $total_discount['family']['amount']=$family_row['amount'];
            $total_discount['family']['ptype']=$family_row['ptype'];
            $total_discount['family']['reason']=$family_row['reason'];
            $total_discount['family']['discount_amount']=$family_row['discount_amount'];
          }
          else{
            $total_discount['family']['feetype']="";
            $total_discount['family']['type']="";
            $total_discount['family']['feeid']="";
            $total_discount['family']['amount']="";
            $total_discount['family']['ptype']="";
            $total_discount['family']['reason']="";
            $total_discount['family']['discount_amount']="";
          }
          if(!empty($heads)){
            foreach($heads as $h){
                $family_sel_head=$dbobject->select("SELECT * FROM `discount_tbl` WHERE `feetype`='family' AND `headid`='".$h['id']."' AND `acyear`='".$acyear."'");
                $family_head_row=$dbobject->fetch_array($family_sel_head);
                if(!empty($family_head_row)){
                    $total_discount['family_head'][$h['id']]['feetype']=$family_head_row['feetype'];
                    $total_discount['family_head'][$h['id']]['type']=$family_head_row['type'];
                    $total_discount['family_head'][$h['id']]['headid']=$family_head_row['headid'];
                    $total_discount['family_head'][$h['id']]['amount']=$family_head_row['amount'];
                    $total_discount['family_head'][$h['id']]['ptype']=$family_head_row['ptype'];
                    $total_discount['family_head'][$h['id']]['reason']=$family_head_row['reason'];
                    $total_discount['family_head'][$h['id']]['discount_amount']=$family_head_row['discount_amount'];
                }
                else{
                    $total_discount['family_head'][$h['id']]['feetype']="";
                    $total_discount['family_head'][$h['id']]['type']="";
                    $total_discount['family_head'][$h['id']]['headid']="";
                    $total_discount['family_head'][$h['id']]['amount']="";
                    $total_discount['family_head'][$h['id']]['ptype']="";
                    $total_discount['family_head'][$h['id']]['reason']="";
                    $total_discount['family_head'][$h['id']]['discount_amount']="";
                    
                }
            }
        }
          return $total_discount;
      }
      function getHeadAmountIndividualDiscount($sid,$headid,$family_id,$acyear,$feemod){
        $dbobject = new DB();
        $dbobject->getCon();
        $total_discount=array();
        $sel_head=$dbobject->select("SELECT * FROM `discount_tbl` WHERE `sid`='".$sid."' AND `feetype`='".$feemod."' AND `headid`='".$headid."' AND `acyear`='".$acyear."'");
        $head_row=$dbobject->fetch_array($sel_head);
        if(!empty($head_row)){
            $total_discount['feetype']=$head_row['feetype'];
            $total_discount['type']=$head_row['type'];
            $total_discount['headid']=$head_row['headid'];
            $total_discount['amount']=$head_row['amount'];
            $total_discount['ptype']=$head_row['ptype'];
            $total_discount['reason']=$head_row['reason'];
            $total_discount['discount_amount']=$head_row['discount_amount'];
        }else{
            $total_discount['feetype']="";
            $total_discount['type']="";
            $total_discount['headid']="";
            $total_discount['amount']="";
            $total_discount['ptype']="";
            $total_discount['reason']="";
            $total_discount['discount_amount']="";
        }
        return $total_discount;
      }
      function due_date_by_instalment($install_mode,$installment,$cacyear)
	  {
        $dbobject = new DB();
        $dbobject->getCon();	
		$sql="SELECT * FROM `fee_term_due_date` WHERE `install_mode`='".$install_mode."' and `installment`='".$installment."' and `acyear`='".$cacyear."'";		
		$qry=$dbobject->select($sql);
        $row=$dbobject->fetch_array($qry);
        return $row['due_date'];		
	  }		  
      function bus_due_date_by_instalment($install_mode,$installment,$cacyear)
	  {
        $dbobject = new DB();
        $dbobject->getCon();	
	    $sql="SELECT * FROM `bus_duedate_setting` WHERE `install_mode`='".$install_mode."' and `installment`='".$installment."' and `acyear`='".$cacyear."'";		
		$qry=$dbobject->select($sql);
        $row=$dbobject->fetch_array($qry);
        return $row['due_date'];		
	  }
	  function create_fee_sction()
	  {
		  $data['School']="school";
		  $data['Section']="section";
		  $data['Class']="classno";
		  $data['Homeroom']="classid";
		 return $data;
	  }	 
 function fee_head_student_classno($fee_head_id,$acyear)
    {
        $dbobj = new DB();
        $dbobj->getCon();
        $sql = "SELECT distinct `fee_head_student`.`classid` FROM `fee_head_student` 
		LEFT JOIN `sclass` on `sclass`.`classid`=`fee_head_student`.`classid`
		WHERE `fee_head_id`='".$fee_head_id."' and `acyear`='".$acyear."'
		order by `sclass`.`classno`,`sclass`.`division`
		";
	//	echo $sql;
        $qry = $dbobj->select($sql);
        $i = 0;
        while ($row = $dbobj->fetch_array($qry))
        {
            $sclass_det=$dbobj->selectall("sclass",array("classid"=>$row['classid']));
			$fee_det[$sclass_det['classno']]['classno'] = $sclass_det['classno'];
			$fee_det[$sclass_det['classno']]['classname'] = $sclass_det['classname'];

        }

        return $fee_det;
    }
    function fee_head_student_classid_BYclassno($fee_head_id,$acyear,$classno)
    {
        $dbobj = new DB();
        $dbobj->getCon();
        $sql = "SELECT distinct `fee_head_student`.`classid` FROM `fee_head_student` 
		LEFT JOIN `sclass` on `sclass`.`classid`=`fee_head_student`.`classid`
		WHERE `fee_head_student`.`fee_head_id`='".$fee_head_id."' and `fee_head_student`.`acyear`='".$acyear."' and `sclass`.`classno`='".$classno."'
		order by `sclass`.`classno`,`sclass`.`division`
		";
        $qry = $dbobj->select($sql);
        $i = 0;
        while ($row = $dbobj->fetch_array($qry))
        {
            $sclass_det=$dbobj->selectall("sclass",array("classid"=>$row['classid']));
			$fee_det[$sclass_det['classno']]['classno'] = $sclass_det['classno'];
			$fee_det[$sclass_det['classno']]['classname'] = $sclass_det['classname'];

        }

        return $fee_det;
    }	
    function fee_head_student_classid($fee_head_id,$acyear)
    {
        $dbobj = new DB();
        $dbobj->getCon();
        $sql = "SELECT distinct `fee_head_student`.`classid` FROM `fee_head_student` 
		LEFT JOIN `sclass` on `sclass`.`classid`=`fee_head_student`.`classid`
		WHERE `fee_head_id`='".$fee_head_id."' and `acyear`='".$acyear."'
		order by `sclass`.`classno`,`sclass`.`division`
		";
        $qry = $dbobj->select($sql);
        $i = 0;
        while ($row = $dbobj->fetch_array($qry))
        {
            $sclass_det=$dbobj->selectall("sclass",array("classid"=>$row['classid']));
			$fee_det[$i]['classid'] = $sclass_det['classid'];
			$fee_det[$i]['classno'] = $sclass_det['classno'];
			$fee_det[$i]['classname'] = $sclass_det['classname'];
			$fee_det[$i]['division'] = $sclass_det['division'];
            $i++;

        }

        return $fee_det;
    }
function fee_termMonth(){
    $fee_name_array[0]="January";
    $fee_name_array[1]="February";
    $fee_name_array[2]="March";
    $fee_name_array[3]="April";
    $fee_name_array[4]="May";
    $fee_name_array[5]="June";
    $fee_name_array[6]="July";
    $fee_name_array[7]="August";
    $fee_name_array[8]="September";
    $fee_name_array[9]="October";
    $fee_name_array[10]="November";
    $fee_name_array[11]="December";
    return $fee_name_array;
}
function CalcTotalfee($totalfee,$distype,$ptype,$rate){
    $val=0;
    $totalfee=(float)$totalfee;
   if($totalfee=="0"){
       $totalfee=0;
   }
   if($totalfee!=0){
        if($ptype=="Percentage"){
            $val=($totalfee/100)*$rate;
        }else{
            $val=$rate;
        }
        if($distype=="Addition"){
            $totalfee=$totalfee+$val;
        }else{
            $totalfee=$totalfee-$val;
        }
   }
   return $totalfee;
}
function CalcTotalfeeHead($totalfee,$distype,$ptype,$rate){
    $val=0;
   if($totalfee!=0){
        if($ptype=="Percentage"){
            $val=($totalfee/100)*$rate;
        }else{
            $val=$rate;
        }
        if($distype=="Addition"){
            $totalfee=$totalfee+$val;
        }else{
            $totalfee=$totalfee-$val;
        }
   }
   return $totalfee;
}
function get_FeesplitOptions(){
    $dbobject = new DB();
    $dbobject->getCon();
    $i=0;
    $options=array();
    $qry=$dbobject->select("SELECT DISTINCT `type` FROM `fee_head_split_option` order by `id`");
	while($row=$dbobject->fetch_array($qry)){
        $options[$i]=$row['type'];
        $i++;
    }
    return $options;
}
function getFullfee($acyear,$class_list){
    /*$dbobject = new DB();
    $dbobject->getCon();
    $amount=0;
    if(!empty($class_list)){
        foreach($class_list as $cls){
            $qry=$dbobject->select("SELECT `fee_head_class`.`amount` FROM `fee_head_class`  LEFT JOIN `fee_head` ON `fee_head_class`.`head_id`=`fee_head`.`id` WHERE `acyear`='".$acyear."' AND `classid`='".$cls['classid']."'");
            while($qry_row=$dbobject->fetch_array($qry)){
                $ss=$dbobject->retrieve_strength2($cls['classid'],$acyear);
                $srt=$ss['strength'];
                $amount=($amount+($qry_row['amount'])*$srt);
            }
        }
     }
     $qry=$dbobject->select("SELECT `fee_head_student`.`amount` FROM `fee_head_student`  LEFT JOIN `fee_head` ON `fee_head_student`.`fee_head_id`=`fee_head`.`id` WHERE `fee_head`.`acyear`='".$acyear."'");
     while($ind_row=$dbobject->fetch_array($qry)){
         $amount=$amount+$ind_row['amount'];
     }
     return $amount;*/
     return 402335.04;
}
function todayCollection($acyear){
    $dbobject = new DB();
    $dbobject->getCon();
    $amount=0;
    $date=date('Y-m-d');
    $qry=$dbobject->select("SELECT `payment`.`paid_amount` FROM `payment`  WHERE `pay_date`='".$date."'");
    while($qry_row=$dbobject->fetch_array($qry)){
        $amount=$amount+$qry_row['paid_amount'];
    }
    return $amount;
}
function TotalDiscount($acyear){
    $dbobject = new DB();
    $dbobject->getCon();
    $fee=new Fee();
    $amount=0;
    $total_amount=0;
    $date=date('Y-m-d');
    $family_codes=array();
    $discount_coupons=$fee->DiscountCoupons($acyear);
        if(!empty($discount_coupons)){
            foreach($discount_coupons as $ds){
                $sel_coupon_data=$dbobject->select("SELECT `coupon_mode`,`coupon_data` FROM `coupon_data` WHERE `couponid`='".$ds['id']."'");
                while($c_row=$dbobject->fetch_array($sel_coupon_data)){
                    if($c_row['coupon_mode']!='2'){
                        $parent_det=$dbobject->selectall("parent",array("id"=>$c_row['coupon_data']));
                        $parentids[$parent_det['family_id']]=$parent_det['id'];
                        if($parent_det['family_code']!=""){
                        if (!in_array($family_codes, $parent_det['family_code'])) {
                            array_push($family_codes,$parent_det['family_code']);
                        }
                    }
                        $coupons_array[$ds['coupon_name']][$parent_det['family_code']]['val']=$ds['discount_value'];
                        $coupons_array[$ds['coupon_name']][$parent_det['family_code']]['type']=$ds['discount_mode'];
                    }
                    
                }
                $coupons_total[$ds['coupon_name']]=0;
            }
        }
        $custom=array();
        $sel_discount=$dbobject->select("SELECT `family_id`,`ptype`,sum(dis_amount) as dis_amount,`discount_amount` FROM `discount_tbl`   WHERE `type`='Discount' AND `acyear`='".$acyear."' group by `family_id`");
        while($discount_row=$dbobject->fetch_array($sel_discount)){
            $parent_det=$fee->getParentbyFamily_Code($discount_row['family_id']);
            $parentids[$discount_row['family_id']]=$parent_det['id'];
            if (!in_array($family_codes, $parent_det['family_code'])) {
                array_push($family_codes,$parent_det['family_code']);
            }
        
            $disc=round($discount_row['dis_amount'],2);
            $custom[$parent_det['family_code']]['val']=$disc;
            $custom[$parent_det['family_code']]['discount_amount']=$discount_row['discount_amount'];
            $custom[$parent_det['family_code']]['type']=$discount_row['ptype'];
        }
        $i=1;
        $customtotal=0;
        if(!empty($family_codes)){
            foreach($family_codes as $fmy){
              $studentList=$dbobject->student_data_by_family_codeAcyear($fmy,$acyear);
              $pid=$parentids[$fmy];
              $fee_head=$fee->getFeeHeadByparent($pid,$acyear,$studentList,"off");
              $i=0;
              if(!empty($studentList)){
                $j=1;
                foreach($studentList as $sl){
                    $amount=$fee->GetoneTimeFee($fee_head,$j,$sl['sid'],$sl['current_class'],$acyear);
                    $amount=$amount-$custom[$fmy]['discount_amount'];
                    if(isset($custom[$fmy])){
                        $customtotal=$customtotal+$amount;
                    }
                    if(!empty($coupons_array)){
                        foreach($coupons_array as $key=>$cp){
                            if(isset($coupons_array[$key][$fmy])){
                                $amount=$fee->GetoneTimeFee($fee_head,$j,$sl['sid'],$sl['current_class'],$acyear);
                                if($coupons_array[$key][$fmy]['val']!=""){
                                $disc=$fee->CalcTotalfee($amount,$coupons_array[$key][$fmy]['type'],"Discount",$coupons_array[$key][$fmy]['val']);
                                $amount=$amount-$disc;
                                $coupons_total[$key]=$coupons_total[$key]+$amount;
                                }                                
                                
                            }

                        }
                    }

                    }
                    $j++;
                    $i++;
                }
            }
        }
    $total_amount=$total_amount+$customtotal;
    if(!empty($coupons_array)){
        foreach($coupons_array as $key=>$cp){
            $total_amount=$total_amount+$coupons_total[$key];
        }
    }
    return $total_amount;
}
function TotalRefund($acyear){
    $dbobject = new DB();
    $dbobject->getCon();
    $amount=0;
    $date=date('Y-m-d');
    $qry=$dbobject->select("SELECT SUM(refund) AS `refund` FROM `payment`   WHERE `acyear`='".$acyear."' AND `ref_type`='refund'");
    while($qry_row=$dbobject->fetch_array($qry)){
        $amount=$amount+$qry_row['refund'];
    }
    return $amount;
}
function getFeeCredit($pid){
    $dbobject = new DB();
    $dbobject->getCon();
    $amount=0;
    $deduct=0;
    $qry=$dbobject->select("SELECT SUM(amount) AS `amount`,sum(deduct) as `deduct` FROM `wallet` WHERE `pid`='".$pid."'");
    while($qry_row=$dbobject->fetch_array($qry)){
        $amount=$amount+$qry_row['amount'];
        $deduct=$deduct+$qry_row['deduct'];
    }
    return $amount-$deduct;
}
function TotalCollection($acyear){
    $dbobject = new DB();
    $dbobject->getCon();
    $amount=0;
    $qry=$dbobject->select("SELECT SUM(paid_amount) AS `amount` FROM `payment` WHERE `acyear`='".$acyear."'");
    $qry_row=$dbobject->fetch_array($qry);
    return $amount=$qry_row['amount'];
}
function TransactionId(){
    return substr(hash('sha256', mt_rand() . microtime()), 0, 20);
}
function getOnetimeFeeByParent($pid,$acyear){
    $dbobject = new DB();
    $dbobject->getCon();
    $total_amount=0;
    $parent_det=$dbobject->selectall("parent",array("id"=>$pid));
    $studentList=$dbobject->student_data_by_family_codeAcyear($parent_det['family_code'],$acyear);
    $fee_head=$this->getFeeHeadByparent($parent_det['id'],$acyear,$studentList,"off");
   
    if(!empty($fee_head)){
        foreach($fee_head as $h){
            
            if(!empty($studentList)){
                $i=0;
                $j=0;
                
                foreach($studentList as $s){
                  
                    if($h['feemod']=="individual"){
                      $amount=$this->getHeadAmountIndividual($s['sid'],$s['current_class'],$h['id'],$acyear);
                       $total_amount=$total_amount+$amount;
                    }
                    elseif($h['feemod']=="family_tree"){
                       $amount=$this->getFamilyTreeFee($s['sid'],$j,$s['current_class'],$h['id'],$h['class_type'],$acyear);
                       if($amount==""){
                           $amount=0;
                       }
                       $total_amount=$total_amount+$amount;
                    }
                    elseif($h['feemod']=='familyfee'){
                       
                       $family_head=$this->getFamilyFee($s['sid'],$j,$s['current_class'],$h['id'],$acyear);
                       $total_amount=$total_amount+$family_head;
                     
                    }
                    $j++;
                    $i++;
            }
            }
        }
    }
    return $total_amount;
}
function getOnetimeFeeByParent1($pid,$acyear){
    $dbobject = new DB();
    $dbobject->getCon();
    $total_amount=0;
    $parent_det=$dbobject->selectall("parent",array("id"=>$pid));
    $studentList=$dbobject->student_data_by_family_codeAcyear($parent_det['family_code'],$acyear);
    $fee_head=$this->getFeeHeadByparent($parent_det['id'],$acyear,$studentList,"off");
    if(!empty($fee_head)){
        foreach($fee_head as $h){
            if(!empty($studentList)){
                $i=0;
                $j=0;
                foreach($studentList as $s){
                    if($h['feemod']=="individual"){
                       $amount=$this->getHeadAmountIndividual($s['sid'],$s['current_class'],$h['id'],$acyear);
                       $total_amount=$total_amount+$amount;
                    }
                    elseif($h['feemod']=="family_tree"){
                       $amount=$this->getFamilyTreeFee($s['sid'],$j,$s['current_class'],$h['id'],$h['class_type'],$acyear);
                       if($amount==""){
                           $amount=0;
                       }
                       $total_amount=$total_amount+$amount;
                    }
                    elseif($h['feemod']=='familyfee'){
                       $family_head=$this->getFamilyFee($s['sid'],$j,$s['current_class'],$h['id'],$acyear);
                       $total_amount=$total_amount+$family_head;
                     
                    }
                    $j++;
                    $i++;
            }
            }
        }
    }
    return $total_amount;
}
function getDueByParent($parent_id,$acyear,$instalment_mode){
    $dbobject = new DB();
    $dbobject->getCon();
    $total_amount=0;
    $today=Date('Y-m-d');
    $date=Date('Y-m-d', strtotime('+3 days'));
    $date_array = $this->displayDates($today,$date);
    $sql="SELECT SUM(amount) as `total_amount` FROM `student_academic_fee` WHERE `parent_id`='".$parent_id."' AND `ac_year`='".$acyear."' and `instalment_mode`='".$instalment_mode."'";
    if(!empty($date_array)){
        $k=1;
        $count=count($date_array);
        $sql.=" and `due_date` IN ( ";
        foreach($date_array as $d){
            $sql.="'".$d."'";
            if($k<$count){
                $sql.=",";
            }
            $k++;
        }
        $sql.=")";
        
    }
    $sql.="and `flag`='false'";
	$getFee=$dbobject->select($sql);
	$row=$dbobject->fetch_array($getFee);
	if($row['total_amount']!=''){
		$total_amount=$row['total_amount'];
	}
	return round($total_amount,2);
}
function getOverDueByParent($parent_id,$acyear,$instalment_mode){
    $dbobject = new DB();
    $dbobject->getCon();
	$fee=new Fee();
	$date=Date('Y-m-d');
	$total_amount=0;
    $getFee=$dbobject->select("SELECT SUM(amount) as `total_amount` FROM `student_academic_fee` WHERE `parent_id`='".$parent_id."' AND `ac_year`='".$acyear."' and `instalment_mode`='".$instalment_mode."' and `due_date`<'".$date."' and `flag`='false'");
	$row=$dbobject->fetch_array($getFee);
	if($row['total_amount']!=''){
		$total_amount=$row['total_amount'];
	}
	return round($total_amount,2);
}
function getTotalByParent($parent_id,$acyear,$instalment_mode){
    $dbobject = new DB();
    $dbobject->getCon();
	$fee=new Fee();
	$date=Date('Y-m-d');
	$total_amount=0;
    $getFee=$dbobject->select("SELECT SUM(amount) as `total_amount` FROM `student_academic_fee` WHERE `parent_id`='".$parent_id."' AND `ac_year`='".$acyear."' and `instalment_mode`='".$instalment_mode."'");
	$row=$dbobject->fetch_array($getFee);
	if($row['total_amount']!=''){
		$total_amount=$row['total_amount'];
	}
	return round($total_amount,2);
}
function getTotalByParent_periodwise($parent_id,$acyear,$instalment_mode,$period,$period_type,$period_type2){
    $dbobject = new DB();
    $dbobject->getCon();
	$fee=new Fee();
	$date=Date('Y-m-d');
        if ($period == "month")
        {
            $year = date("Y");
            $rmonth = ucfirst($period_type);
            $timestamp = strtotime('' . $rmonth . ' ' . $year . '');
            $first_second = date('Y-m-01', $timestamp);
            $last_second = date('Y-m-t', $timestamp); // A leap year!
            
        }
        if ($period == "date")
        {
            $first_second = date('Y-m-d', strtotime($period_type));
            $last_second = date('Y-m-d', strtotime($period_type)); // A leap year!
            
        }
        if ($period == "Custom")
        {
            $first_second = date('Y-m-d', strtotime($period_type));
            $last_second = date('Y-m-d', strtotime($period_type2)); // A leap year!
            
        }	
	$total_amount=0;
     if($period == "Full_year")
        {
        $sql="SELECT SUM(amount) as `total_amount` FROM `student_academic_fee` WHERE `parent_id`='".$parent_id."' AND `ac_year`='".$acyear."' and `instalment_mode`='".$instalment_mode."'"; 
        }
        else
		{
		$sql="SELECT SUM(amount) as `total_amount` FROM `student_academic_fee` WHERE `parent_id`='".$parent_id."' AND `ac_year`='".$acyear."' and `instalment_mode`='".$instalment_mode."' and `paid_date`>='" . $first_second . "' and `paid_date`<='" . $last_second . "'"; 		
		}
//echo $sql;		
    $getFee=$dbobject->select($sql);
	$row=$dbobject->fetch_array($getFee);
	if($row['total_amount']!=''){
		$total_amount=$row['total_amount'];
	}
	return round($total_amount,2);
}
    function get_fee_type_head_name_new($acyear, $feehead_type, $fee_type,$head_name)
    {
        $dbobject = new DB();
        $dbobject->getCon();
        $sql = "SELECT distinct `head_name` FROM `fee_head` WHERE `acyear`='" . $acyear . "'";
        if ($feehead_type != "" && $feehead_type != "-1")
        {
            $sql .= " and `feehead_type`='" . $feehead_type . "' ";
        }
        if ($fee_type != "" && $fee_type != "-1")
        {
            $sql .= "and `fee_type` IN ('other','db_student')";
        }
        if ($head_name != "" && $head_name != "-1")
        {
            $sql .= "and `head_name`='" . $head_name . "'";
        }		
         $sql.=" and `duedate`='on'";
        $sql.="order by `head_name`";
        $qry = $dbobject->select($sql);
        while ($row = $dbobject->fetch_array($qry))
        {
            $data[$row['head_name']] = $row['head_name'];
        }
        return $data;
    }
function get_fee_type_head($acyear, $feehead_type, $fee_type,$head_name)
    {
        $dbobject = new DB();
        $dbobject->getCon();
        $i=0;
        $sql = "SELECT `id`,`head_name`,`end_date` FROM `fee_head` WHERE `acyear`='" . $acyear . "'";
        if ($feehead_type != "" && $feehead_type != "-1")
        {
            $sql .= " and `feehead_type`='" . $feehead_type . "' ";
        }
        if ($fee_type != "" && $fee_type != "-1")
        {
            $sql .= "and `fee_type` IN ('other','db_student')";
        }
        if ($head_name != "" && $head_name != "-1")
        {
            $sql .= "and `head_name`='" . $head_name . "'";
        }		
         $sql.=" and `duedate`='on'";
        $sql.="order by `head_name`";
        $qry = $dbobject->select($sql);
        while ($row = $dbobject->fetch_array($qry))
        {
            $data[$i]['id'] = $row['id'];
            $data[$i]['head_name'] = $row['head_name'];
            $data[$i]['due_date'] = $row['end_date'];
            $i++;
        }
        return $data;
    }
function getDueDateFeeByParent($fid,$fee_type_head_id,$acyear,$h){
$dbobject = new DB();
$dbobject->getCon();
$date=Date('Y-m-d');
$total_amount=0;
        $studentList=$dbobject->student_data_by_family_codeAcyear($fid,$acyear);
        if(!empty($studentList)){
            $i=0;
            $j=0;
            foreach($studentList as $s){
                if($h['feemod']=="individual"){
                    $amount=$this->getHeadAmountIndividual($s['sid'],$s['current_class'],$h['id'],$acyear);
                    $total_amount=$total_amount+$amount;
                 }
                 elseif($h['feemod']=="family_tree"){
                    $amount=$this->getFamilyTreeFee($s['sid'],$j,$s['current_class'],$h['id'],$h['class_type'],$acyear);
                    if($amount==""){
                        $amount=0;
                    }
                    $total_amount=$total_amount+$amount;
                 }
                 elseif($h['feemod']=='familyfee'){
                   
                    $family_head=$this->getFamilyFee($s['sid'],$j,$s['current_class'],$h['id'],$acyear);
                    $total_amount=$total_amount+$family_head;
                  
                 }
                 $j++;
                 $i++;
            }
        }
        return round($total_amount,2);
 }
 function getDueDateFeeBySid($sid,$student_class,$j,$fee_type_head_id,$acyear,$h){
    $dbobject = new DB();
    $dbobject->getCon();
    $date=Date('Y-m-d');
    $total_amount=0;
                $i=0;
                $j=0;
                    if($h['feemod']=="individual"){
                        $amount=$this->getHeadAmountIndividual($sid,$student_class,$h['id'],$acyear);
                        $total_amount=$total_amount+$amount;
                     }
                     elseif($h['feemod']=="family_tree"){
                        $amount=$this->getFamilyTreeFee($sid,$j,$student_class,$h['id'],$h['class_type'],$acyear);
                        if($amount==""){
                            $amount=0;
                        }
                        $total_amount=$total_amount+$amount;
                     }
                     elseif($h['feemod']=='familyfee'){
                       
                        $family_head=$this->getFamilyFee($sid,$j,$student_class,$h['id'],$acyear);
                        $total_amount=$total_amount+$family_head;
                      
                     }
                     $j++;
                     $i++;
            return round($total_amount,2);
}
 function displayDates($startDate, $endDate, $format = "Y-m-d")
 {
     $begin = new DateTime($startDate);
     $end = new DateTime($endDate);
  
     $interval = new DateInterval('P1D'); // 1 Day
     $dateRange = new DatePeriod($begin, $interval, $end);
  
     $range = [];
     foreach ($dateRange as $date) {
         $range[] = $date->format($format);
     }
  
     return $range;
 }
 function UpdateFeeFromCart($transactionid){
    $dbobject = new DB();
    $dbobject->getCon();
    $tr_det=$dbobject->selectall("transaction_det",array("trid"=>$transactionid));
    $student_det=$dbobject->selectall("student",array("parent_id"=>$tr_det['pid']));
     $invoice=$this->Invoice_num($tr_det['acyear']);
    $pay_date=date('Y-m-d');
    $sel_cart=$dbobject->select("SELECT * FROM `fee_cart` WHERE `transactionid`='".$transactionid."'");
    while($cart_row=$dbobject->fetch_array($sel_cart)){
        $parent_id=$cart_row['parent_id'];
        $acyear=$cart_row['acyear'];
        $pay_mode=$cart_row['pay_mode'];
        $surcharge=$this->surcharge($total,$mode);
       if($cart_row['instalment_mode']=='ONETIME'){
            $ins_ac=$dbobject->exe_qry("INSERT INTO `student_academic_fee` (`fee_name`,`instalment_mode`,`fee_type`,`parent_id`,`amount`,`fine`,`due_date`,`flag`,`ac_year`,`paid_date`,`feeppaymentmode`)
            VALUES('ONETIME','ONETIME','indivitual','".$cart_row['parent_id']."','".$cart_row['amount']."','".$cart_row['fine']."','".$cart_row['duedate']."','true','".$cart_row['acyear']."','".$pay_date."','".$student_det['feeppaymentmode']."')");
            $feeid=$dbobject->mysql_insert_id();
            $ins=$dbobject->exe_qry("INSERT INTO `payment` (`parent_id`,`feetype`,`feeid`,`total_amount`,`fine`,`paid_amount`,`pay_date`,`transaction_id`,`payee`,`pay_type`,`surcharge`)
            VALUES('".$cart_row['parent_id']."','academic','".$feeid."','".$cart_row['amount']."','".$cart_row['fine']."','".$cart_row['total']."','".$pay_date."','".$invoice."','".$cart_row['payee']."','".$cart_row['pay_mode']."','".$surcharge."')");
        }elseif($cart_row['instalment_mode']=='fee_head'){

            $ins_ac=$dbobject->exe_qry("INSERT INTO `student_academic_fee` (`fee_name`,`instalment_mode`,`fee_type`,`feehead_id`,`parent_id`,`amount`,`fine`,`due_date`,`flag`,`ac_year`,`paid_date`,`feeppaymentmode`)
            VALUES('".$cart_row['fee_name']."','fee_head','indivitual','".$cart_row['fee_id']."','".$cart_row['parent_id']."','".$cart_row['amount']."','".$cart_row['fine']."','".$cart_row['duedate']."','true','".$cart_row['acyear']."','".$pay_date."','".$student_det['feeppaymentmode']."')");
             $feeid=$dbobject->mysql_insert_id();

            $ins=$dbobject->exe_qry("INSERT INTO `payment` (`parent_id`,`feetype`,`feeid`,`total_amount`,`fine`,`paid_amount`,`pay_date`,`transaction_id`,`payee`,`pay_type`,`surcharge`)
                               VALUES('".$cart_row['parent_id']."','academic','".$feeid."','".$cart_row['amount']."','".$cart_row['fine']."','".$cart_row['total']."','".$pay_date."','".$invoice."','".$cart_row['payee']."','".$cart_row['pay_mode']."','".$surcharge."')");
        }else{
            $upd_ac=$dbobject->exe_qry("UPDATE `student_academic_fee` SET `flag`='true',`paid_date`='".$pay_date."' WHERE `id`='".$cart_row['fee_id']."'");
            $ins=$dbobject->exe_qry("INSERT INTO `payment` (`parent_id`,`feetype`,`feeid`,`total_amount`,`fine`,`paid_amount`,`pay_date`,`transaction_id`,`payee`,`pay_type`,`surcharge`)
            VALUES('".$cart_row['parent_id']."','academic','".$cart_row['fee_id']."','".$cart_row['amount']."','".$cart_row['fine']."','".$cart_row['total']."','".$pay_date."','".$invoice."','".$cart_row['payee']."','".$cart_row['pay_mode']."','".$surcharge."')");
        }
    }

    $ins_rec=$dbobject->exe_qry("INSERT INTO `rec_series` (`pid`,`reciept_no`,`acyear`,`date`,`type`,`pay_mode`) VALUES ('".$parent_id."','".$invoice."','".$acyear."','".$pay_date."','academic','".$pay_mode."')");
    return $invoice;
 }
 function surcharge($total,$mode){
    $surcharge=0;
   // $surcharge=($total/100)*1.5;
    return $surcharge;
 }
 function DiscountCoupons($acyear){
    $dbobject = new DB();
    $dbobject->getCon();
    $i=0;
    $data=array();
    $sel_coupons=$dbobject->select("SELECT * FROM `discount_coupons` WHERE `acyear`='".$acyear."' and `status`='0'");
    while($coupon_row=$dbobject->fetch_array($sel_coupons)){
        $data[$i]['id']=$coupon_row['id'];
        $data[$i]['coupon_name']=$coupon_row['coupon_name'];
        $data[$i]['acyear']=$coupon_row['acyear'];
        $data[$i]['discount_mode']=$coupon_row['discount_mode'];
        $data[$i]['discount_value']=$coupon_row['discount_value'];
        $data[$i]['discount_status']=$coupon_row['discount_status'];
        $data[$i]['coupon_mode']=$coupon_row['coupon_mode'];
        $i++;
    }
    return $data;
}
function DiscountCouponsActive($acyear){
    $dbobject = new DB();
    $dbobject->getCon();
    $i=0;
    $data=array();
    $sel_coupons=$dbobject->select("SELECT * FROM `discount_coupons` WHERE `acyear`='".$acyear."' and `status`='0'  and `discount_status`='1'");
    while($coupon_row=$dbobject->fetch_array($sel_coupons)){
        $data[$i]['id']=$coupon_row['id'];
        $data[$i]['coupon_name']=$coupon_row['coupon_name'];
        $data[$i]['acyear']=$coupon_row['acyear'];
        $data[$i]['discount_mode']=$coupon_row['discount_mode'];
        $data[$i]['discount_value']=$coupon_row['discount_value'];
        $data[$i]['discount_status']=$coupon_row['discount_status'];
        $data[$i]['coupon_mode']=$coupon_row['coupon_mode'];
        $i++;
    }
    return $data;
}
function checkFeestatusByStudent($params){
    $dbobject = new DB();
    $dbobject->getCon();
    $sql="SELECT `flag`,`paid_date`,`pay_mode`,`invoice` FROM `student_academic_fee` WHERE";
    if($params['sid']!=''){
       $sql.="`sid`='".$params['sid']."'"; 
    }
    if($params['acyear']!=''){
        $sql.="AND `ac_year`='".$params['acyear']."'"; 
    }
    if($params['instalment_mode']!=''){
        $sql.="AND `instalment_mode`='".$params['instalment_mode']."'"; 
    }
    if($params['feehead_id']!=''){
        $sql.="AND `feehead_id`='".$params['feehead_id']."'"; 
    }
    if($params['fee_name']!=''){
        $sql.="AND `fee_name`='".$params['fee_name']."'";  
    }
    $sel_flag=$dbobject->select($sql);
    $flag_row=$dbobject->fetch_array($sel_flag);
    return $flag_row;
}
function get_paidStudents($invoice){
    $dbobject = new DB();
    $dbobject->getCon();
    $i=0;
    $data=array();
    $sql="SELECT DISTINCT `student_academic_fee`.`sid`,`sname`,`s_sur_name`,`ac_year` FROM `student_academic_fee` LEFT JOIN `student` on `student_academic_fee`.`sid`=`student`.`sid`  WHERE `invoice`='".$invoice."'";
    $query=$dbobject->select($sql);
    while($row=$dbobject->fetch_array($query)){
        $data[$i]['sid']=$row['sid'];
        $data[$i]['sname']=$row['sname'];
        $data[$i]['s_sur_name']=$row['s_sur_name'];
        $student_class=$dbobject->selectall("student_class",array("sid"=>$data[$i]['sid'],"acyear"=>$row['ac_year']));
        $class_det=$dbobject->selectall("sclass",array("classid"=>$student_class['classid']));
        $data[$i]['classname']=$class_det['classname'];
        $data[$i]['division']=$class_det['division'];
        $i++;
    }
    return $data;
}
function getFeeConversion($params){
    $dbobject = new DB();
    $dbobject->getCon();
    $data=array();
    $i=0;
    $sql="SELECT DISTINCT `fee_name`,`instalment_mode`,`due_date` FROM `student_fee_spit` ";
    if(!empty($params)){
        $sql.="WHERE ";
    }
    if($params['acyear']!=''){
        $sql.="`acyear`='".$params['acyear']."'";
    }
    if($params['fee_mode']!=''){
        $sql.=" AND `fee_mode`='".$params['fee_mode']."'";
    }
    if(!empty($params['studentlist'])){
        $count=count($params['studentlist']);
        $i=1;
            $sql.="AND `sid` IN (";
        foreach($params['studentlist'] as $s){
            $sql.="'".$s['sid']."'";
            if($i<$count){
                $sql.=",";
            }
            $i++;
        }
        $sql.=")";
    
    }
    if($params['headid']!=''){
        $sql.="AND `head_id`='".$params['headid']."'";
    }
    $query=$dbobject->select($sql);
    $j=0;
    while($row=$dbobject->fetch_array($query)){
        $data[$j]['fee_name']=$row['fee_name'];
        $data[$j]['instalment_mode']=$row['instalment_mode'];
        $data[$j]['due_date']=$row['due_date'];
        $j++;
    }
    return $data;
}
function get_installment_fee($params){
    $dbobject = new DB();
    $dbobject->getCon();
    $data=array();
    $i=0;
    $sql="SELECT  SUM(amount) AS `amount` FROM `student_fee_spit` ";
    if(!empty($params)){
        $sql.="WHERE ";
    }
    if($params['acyear']!=''){
        $sql.="`acyear`='".$params['acyear']."'";
    }
    if($params['sid']!=''){
        $sql.="AND `sid`='".$params['sid']."'";
    }
    if($params['fee_name']!=''){
        $sql.="AND `fee_name`='".$params['fee_name']."'";
    }
    if($params['instalment_mode']!=''){
        $sql.="AND `instalment_mode`='".$params['instalment_mode']."'";
    }
    if($params['headid']!=''){
        $sql.="AND `head_id`='".$params['headid']."'";
    }
    $query=$dbobject->select($sql);
    while($row=$dbobject->fetch_array($query)){
        $data['amount']=$row['amount'];
        $i++;
    }
    return $data;
}
function CheckForDiscountReward($params){
    $dbobject = new DB();
    $dbobject->getCon();
    $sql="SELECT  * FROM `applied_discount` ";
    if(!empty($params)){
        $sql.=" WHERE";
        if($params['parent_id']!=""){
            $sql.=" `parent_id`='".$params['parent_id']."'";
        }
        if($params['invoice']!=""){
            $sql.="AND `invoice`='".$params['invoice']."'";
        }
        if($params['couponid']!=""){
            $sql.="AND `couponid`='".$params['couponid']."'";
        }
    }
   $query=$dbobject->select($sql);
   return $num_rows=$dbobject->mysql_num_row($query);

}
function searchForStudentnumber($id, $array) {
    foreach ($array as $key => $val) {
        if ($val['sid'] === $id) {
            return $key;
        }
    }
    return null;
 }
function GetoneTimeFee($fee_head,$j,$sid,$classid,$acyear){
    $total_amount=0;
    if(!empty($fee_head)){
        foreach($fee_head as $h){
            if($h['feemod']=="individual"){
                $amount=$this->getHeadAmountIndividual($sid,$classid,$h['id'],$acyear);
                $total_amount=$total_amount+$amount;
             }
             elseif($h['feemod']=="family_tree"){
                $amount=$this->getFamilyTreeFee($sid,$j,$classid,$h['id'],$h['class_type'],$acyear);
                if($amount==""){
                    $amount=0;
                }
                $total_amount=$total_amount+$amount;
             }
             elseif($h['feemod']=='familyfee'){
               
                $family_head=$this->getFamilyFee($sid,$j,$classid,$h['id'],$acyear);
                $total_amount=$total_amount+$family_head;
              
             }
        }
    }
    return $total_amount;
}
function getParentbyFamily_Code($family_code){
    $dbobject = new DB();
    $dbobject->getCon();
    $parent_det=$dbobject->selectall("parent",array("family_code"=>$family_code));
    return $parent_det;
}
function getInstallmentByPlan($instalment_mode,$acyear){
    $dbobject = new DB();
    $dbobject->getCon();
    $i=0;
    $data=array();
   $sel=$dbobject->select("SELECT `type`,`head`,`duedate` FROM `fee_head_split_option` WHERE `type`='".$instalment_mode."' AND `acyear`='".$acyear."'");
   while($row=$dbobject->fetch_array($sel)){
    $data[$i]['type']=$row['type'];
    $data[$i]['head']=$row['head'];
    $data[$i]['duedate']=$row['duedate'];
    $i++;
   }
   return $data;
}
function get_feename($type,$head){
    if($type=="QUARTERLY"){
        $feename="QUARTER ".$head;
    }elseif($type=="FORTNIGHTLY"){
        $feename="FORTNIGHT ".$head;
    }elseif($type=="WEEKLY"){
        $feename="WEEK ".$head;
    }
    else{
        $feename=$head;
    }
return $feename;
}
function student_data_by_family_codeAcyearForFee($family_code,$acyear,$type,$seltype,$seltype2)
{
    $dbobject = new DB();
    $dbobject->getCon();
	$sql="select `student`.*,`sclass`.`classno`,`classname`,`division`,`sclass`.`status`,`sclass`.`classid` AS `current_class`,`student_type` from `student`";
	$sql.="INNER JOIN `student_class` ON `student`.`sid`=`student_class`.`sid` LEFT JOIN `sclass`  on `student_class`.`classid`=`sclass`.`classid` where `family_code`='".$family_code."'  and `sclass`.`status`='1' and `student_class`.`acyear`='".$acyear."' and `student`.`s_status`='active'";
    if($type=="other"){
        $sql.="AND `student_class`.`student_type`='".$type."'";
    }else{
        $sql.="AND `student_class`.`student_type` IN ('other','db_student')";
    }
    if($seltype=="homeroom"){
        $sql.="AND `student_class`.`classid`='".$seltype2."'";
    }elseif($seltype=="classwise"){
        $sql.="AND `sclass`.`classno`='".$seltype2."'";
    }elseif($seltype=="section"){
        $sql.="AND `sclass`.`sectionid`='".$seltype2."'";
    }
    $sql.="order by `classno` DESC";
    $qry=$dbobject->select($sql);
	$i=0;
	while($row=$dbobject->fetch_array($qry))
	{
	  $data[$i]=$row;
	  $i++;	
	}

	return $data;
}
function student_data_by_family_codeAcyearForFeeByApplication($params)
{
    $dbobject = new DB();
    $dbobject->getCon();
	$sql="select `student_enroll`.*,`student`.`sid` as `ssid`  from `student_enroll` INNER JOIN `student` on `student_enroll`.`sid`=`student`.`enrol_id`";
    $sql.="where `appl_no`='".$params['app_id']."' AND `student_enroll`.`acyear`='".$params['acyear']."'";
    $qry=$dbobject->select($sql);
	$i=0;
	while($row=$dbobject->fetch_array($qry))
	{
      $class_det=$dbobject->select("SELECT `classid` FROM `sclass` LEFT JOIN `sclass_section` ON `sclass`.`sectionid`=`sclass_section`.`id` WHERE `sclass_section`.`acyear`='".$params['acyear']."' and `sclass`.`classno`='".$row['classno']."'");
      $cls_row=$dbobject->fetch_array($class_det);
      $data[$i]['sid']=$row['ssid'];
      $data[$i]['sname']=$row['first_name'];
      $data[$i]['s_sur_name']=$row['family_name'];
      $data[$i]['current_class']=$cls_row['classid'];
      $data[$i]['acyear']=$params['acyear'];
      $data[$i]['student_type']="other";
      $data[$i]['parent_id']="1";
	  $i++;	
	}

	return $data;
}
function checkFeestatusStudent($sid,$acyear){
    $dbobject = new DB();
    $dbobject->getCon();
    $data=array();
    $i=0;
    $check_custom=$dbobject->select("SELECT * FROM `custom_academic_fee` WHERE `sid`='".$sid."' AND `ac_year`='".$acyear."'");
    $custom_num=$dbobject->mysql_num_row($check_custom);
    if($custom_num=="0"){
        $check_feesplit=$dbobject->select("SELECT DISTINCT `fee_name`,`due_date` FROM `student_fee_spit` WHERE `sid`='".$sid."' AND `acyear`='".$acyear."'");
        $split_num=$dbobject->mysql_num_row($check_feesplit);
        if($split_num=="0"){
            $onetime_fee=$dbobject->select("SELECT * FROM `student_academic_fee` WHERE `sid`='".$sid."' AND `ac_year`='".$acyear."'");
            $onetime_num=$dbobject->mysql_num_row($onetime_fee);
            if($onetime_num=="0"){
                $onetime_det=$dbobject->selectall("fee_head_split_option",array("type"=>"ONETIME","acyear"=>$acyear,"fee_type"=>"db_student"));
                $data['ONETIME'][$i]['fee_name']="ONETIME";
                $data['ONETIME'][$i]['due_date']=$onetime_det['duedate'];
            }

        }else{
            while($spit_row=$dbobject->fetch_array($check_feesplit)){
                $data['ONETIME'][$i]['fee_name']=$spit_row['fee_name'];
                $data['ONETIME'][$i]['due_date']=$spit_row['due_date'];
                $i++;
            }
        }
    }
    $fee_head=$this->getFeeheads($acyear,"on");
    if(!empty($fee_head)){
        foreach($fee_head as $fh){
            $sel_feehead=$dbobject->select("SELECT * FROM `student_academic_fee` WHERE `sid`='".$sid."' AND `feehead_id`='".$fh['id']."' AND `ac_year`='".$acyear."'");
            $num_feehead=$dbobject->mysql_num_row($sel_feehead);
            if($num_feehead=="0"){
                $data['fee_head'][$i]['fee_name']=$fh['head_name'];
                $data['fee_head'][$i]['due_date']=$fh['end_date'];
                $i++;
            }
        }
    }
    return $data;
}
function checkFeestatusStudentNew($sid,$acyear){
    $dbobject = new DB();
    $dbobject->getCon();
    $data=array();
    $i=0;
    $check_custom=$dbobject->select("SELECT * FROM `custom_academic_fee` WHERE `sid`='".$sid."' AND `ac_year`='".$acyear."'");
    $custom_num=$dbobject->mysql_num_row($check_custom);
    if($custom_num=="0"){
        $check_feesplit=$dbobject->select("SELECT DISTINCT `fee_name`,`due_date` FROM `student_fee_spit` WHERE `sid`='".$sid."' AND `acyear`='".$acyear."'");
        $split_num=$dbobject->mysql_num_row($check_feesplit);
        if($split_num=="0"){
            $onetime_fee=$dbobject->select("SELECT * FROM `student_academic_fee` WHERE `sid`='".$sid."' AND `ac_year`='".$acyear."'");
            $onetime_num=$dbobject->mysql_num_row($onetime_fee);
            if($onetime_num=="0"){
                $onetime_det=$dbobject->selectall("fee_head_split_option",array("type"=>"ONETIME","fee_type"=>"other","acyear"=>$acyear));
                $data['ONETIME'][$i]['fee_name']="ONETIME";
                $data['ONETIME'][$i]['due_date']=$onetime_det['duedate'];
            }

        }else{
            while($spit_row=$dbobject->fetch_array($check_feesplit)){
                $data['ONETIME'][$i]['fee_name']=$spit_row['fee_name'];
                $data['ONETIME'][$i]['due_date']=$spit_row['due_date'];
                $i++;
            }
        }
    }
    $fee_head_params=array("acyear"=>$acyear,"fee_due"=>"on","type"=>"other");
    $fee_head=$this->getFeeheadsNew($fee_head_params);
    if(!empty($fee_head)){
        foreach($fee_head as $fh){
            $sel_feehead=$dbobject->select("SELECT * FROM `student_academic_fee` WHERE `sid`='".$sid."' AND `feehead_id`='".$fh['id']."' AND `ac_year`='".$acyear."'");
            $num_feehead=$dbobject->mysql_num_row($sel_feehead);
            if($num_feehead=="0"){
                $data['fee_head'][$i]['fee_name']=$fh['head_name'];
                $data['fee_head'][$i]['due_date']=$fh['end_date'];
                $i++;
            }
        }
    }
    return $data;
}
function getFeeheads($acyear,$fee_due){
    $dbobject = new DB();
    $dbobject->getCon();
    $i=0;
    $data=array();
    $sel=$dbobject->select("SELECT `id`,`head_name`,`end_date` FROM `fee_head` WHERE `acyear`='".$acyear."' AND `duedate`='".$fee_due."'");
    while($row=$dbobject->fetch_array($sel)){
        $data[$i]['id']=$row['id'];
        $data[$i]['head_name']=$row['head_name'];
        $data[$i]['end_date']=$row['end_date'];
        $i++;
    }
    return $data;
}
function getFeeheadsNew($params){
    $dbobject = new DB();
    $dbobject->getCon();
    $i=0;
    $data=array();
    $sel=$dbobject->select("SELECT `id`,`head_name`,`end_date` FROM `fee_head` WHERE `acyear`='".$params['acyear']."' AND `duedate`='".$params['fee_due']."' AND `fee_type`='".$params['type']."'");
    while($row=$dbobject->fetch_array($sel)){
        $data[$i]['id']=$row['id'];
        $data[$i]['head_name']=$row['head_name'];
        $data[$i]['end_date']=$row['end_date'];
        $i++;
    }
    return $data;
}
}
?>