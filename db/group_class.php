<?php
include_once('createdb.php');
include_once('notification.php');
include_once('teacher_class.php');
include_once('admin_class.php');
include_once('academic_class.php');
class Group
{
    function all_group_members($groupid,$acyear)
	{
		
		 $dbobject = new DB();
	    $dbobject->getCon();
		$academic=new Academic;
		$group_details=$academic->group_details($groupid);
		if(!empty($group_details))
		{
			$i=0;
			foreach($group_details as $grpdt)
			{
				if($grpdt['selectiontype']=="INDIVIDUAL")
				{
					$to_type[$i]=$grpdt['type'];
					if($grpdt['type']=="teacher")
					{
						$teacher_det=$dbobject->selectall("teacher",array("id"=>$grpdt['userid']));
						$to[$i]=$teacher_det['userid'];
					}
					else
					{
						$to[$i]=$grpdt['userid'];
					}
					$i++;			
				}
				elseif($grpdt['selectiontype']=="classno")
				{
					if($grpdt['type']=="parent")
					{
						$classno_det=$dbobject->get_classlistByclassno($grpdt['userid']);
						if(!empty($classno_det))
						{
							foreach($classno_det as $classid)
							{
								$studentdet=$dbobject->get_studentdetByClassAndAcyear($classid['classid'],$acyear);
								if(!empty($studentdet))
								{
									foreach($studentdet as $stud)
									{
										$to_type[$i]=$grpdt['type'];
										$to[$i]=$stud['parent_id'];
										$i++;
									}
								}
							}
						}
						
					}
					else
					{
						$classno_det=$dbobject->get_classlistByclassno($grpdt['userid']);
						if(!empty($classno_det))
						{
							foreach($classno_det as $classid)
							{
								$studentdet=$dbobject->get_studentdetByClassAndAcyear($classid['classid'],$acyear);
								if(!empty($studentdet))
								{
									foreach($studentdet as $stud)
									{
										$to_type[$i]=$grpdt['type'];
										$to[$i]=$stud['sid'];
										$i++;
									}
								}
							}
						}
					}			
				}
				elseif($grpdt['selectiontype']=="classid")
				{
					if($grpdt['type']=="parent")
					{
								$studentdet=$dbobject->get_studentdetByClassAndAcyear($grpdt['userid'],$acyear);
								if(!empty($studentdet))
								{
									foreach($studentdet as $stud)
									{
										$to_type[$i]=$grpdt['type'];
										$to[$i]=$stud['parent_id'];
										$i++;
									}
								}
							
						
						
					}
					else
					{
								$studentdet=$dbobject->get_studentdetByClassAndAcyear($grpdt['userid'],$acyear);
								if(!empty($studentdet))
								{
									foreach($studentdet as $stud)
									{
										$to_type[$i]=$grpdt['type'];
										$to[$i]=$stud['sid'];
										$i++;
									}
								}
					}				
				}
				

			}
		}		
		 $data['to_type']=$to_type;
		 $data['to']=$to;
		 return $data;
	}
    function GetGroupmembers($gid){
        $dbobject = new DB();
	    $dbobject->getCon();
        $i=0;
        $data=array();
        $sel_members=$dbobject->select("SELECT * FROM `group_member` WHERE `group_id`='".$gid."'");
        while($row=$dbobject->fetch_array($sel_members)){
            $data[$i]['userid']=$row['userid'];
            $data[$i]['type']=$row['type'];
            $i++;
        }
        return $data;
    }
	
}
?>