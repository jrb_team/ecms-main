<?php
include_once('admin_class.php');


$class = $_GET['sele_class'];
$sid = $_GET['sel_student'];
$exam = $_GET['sel_exam'];

//#############retrieving student,s and school's information for report header details###########
$selschool = "select name,mob,email,affiliation_no,academic_year from schoolinfo;";
$selrs = $dbobj->select($selschool);
$selrow = $dbobj->fetch_array($selrs);
$school = $selrow['name'];
$contact = $selrow['mob'];
$academic_year=$selrow['academic_year'];
$aff_no=$selrow['affiliation_no'];
$email = $selrow['email'];

$selname = "select * from student where sid = ".$sid.";";
$result = $dbobj->select($selname);
$row = $dbobj->fetch_array($result);
$stname = $row['sname'];
$initial = $row['initial'];
$studentid = $row['studentid'];
$stud_photoid=$row['simgid'];
$parent_id=$row['parent_id'];
$dob=$row['dob'];
$adrs=$row['paddr1'];

$parent=$dbobj->selectall("parent",array("id"=>$parent_id));

$query = "select classname,division,classno from sclass where classid = ".$class.";";
$rs = mysql_query($query);
$row = mysql_fetch_array($rs);
$classname = $row['classname'];
$division = $row['division'];
$classno = $row['classno'];

$house = "select house from house INNER JOIN student_house ON student_house.house_id=house.id WHERE student_house.sid=".$sid.";";
$result1 = $dbobj->select($house);
$row1 = $dbobj->fetch_array($result1);
$huse=$row1['house'];

$club="select club_name from club INNER JOIN student_club ON student_club.club_id=club.id WHERE student_club.sid=".$sid.";";
$result2=$dbobj->select($club);
$row2=$dbobj->fetch_array($result2);
$clb=$row2['club_name'];

$admin = new Admin($classno);
$tablename=$admin->subject_table();

$form = "<tr><th style='width:110px;'>SUBJECTS</th>";
$selsub=$dbobj->select("select * from ".$tablename." order by `sub_id` ");
$s=1;
while($sub_row=mysql_fetch_array($selsub))
{
	$sub_id[$s]=$sub_row['sub_id'];
	$sub_name[$s]=$sub_row['subject'];
	$s++;
}

$selopn_sub=$dbobj->select("select distinct `sub_id` from `opn_subject_stud` where `stud_id`='".$sid."' and `class_id`='".$class."' ");
$opn_num=mysql_num_rows($selopn_sub);
if($opn_num>0)
{
	$o=1;
	while($opn_row=$dbobj->fetch_array($selopn_sub))
	{
		$opn_id[$o]=$opn_row['sub_id'];
		$o++;
	}
	
}
$sel_exm=$dbobj->select("select * from terms where classno='".$classno."' and `exam_type`!='attendance_only'");
$t=1;
while($exm_row=$dbobj->fetch_array($sel_exm))
{
	$term_id[$t]=$exm_row['id'];
	$sub_type=$exm_row['sub_type'];
	if($sub_type=='subject_and_opn')
	{
		$colspan="2";
	}

$form.= "<th colspan='".$colspan."' style='text-align:center;'>".strtoupper($exm_row['term_name'])."</th>";	
$t++;
}
$form.= "<th colspan='2' style='width:130px'>FINAL RESULT</th></tr><tr><th style='text-align:left'>MAX.MARKS</th>";
		foreach($term_id as $tid)
		{
			$max_mark=0;
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			$examtype=$term_row['exam_type'];
			$sel_examdet=$dbobj->select("select distinct maxmark from `examschedule` where `examid`='".$tid."'");
			while($examdet_row=$dbobj->fetch_array($sel_examdet))
			{
				$max_mark=$max_mark+$examdet_row['maxmark'];
			}
			if($examtype=="term")
			{
				
				$form.="<th style='text-align:center'>100</th><th rowspan='2' style='text-align:center;width:30px;'>GRADE</th>";
			}
			elseif($examtype=="mid_term")
			{
				$form.="<th style='text-align:center'>50</th><th rowspan='2' style='text-align:center;width:30px;'>GRADE</th>";
			}
			
		}

$form.= "<th style='text-align:center'>400</th><th style='text-align:center;width:30px' rowspan='2'>%</th></tr>";
$form.= "<tr><th style='text-align:left'>PASS MARK</th>";
foreach($term_id as $tid)
		{
			$max_mark=0;
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			$examtype=$term_row['exam_type'];
			$sel_examdet=$dbobj->select("select distinct maxmark from `examschedule` where `examid`='".$tid."'");
			while($examdet_row=$dbobj->fetch_array($sel_examdet))
			{
				$max_mark=$max_mark+$examdet_row['maxmark'];
			}
			if($examtype=="term")
			{
				
				$form.="<th style='text-align:center'>40</th>";
			}
			elseif($examtype=="mid_term")
			{
				$form.="<th style='text-align:center'>20</th>";
			}
			
		}

$form.= "<th style='text-align:center'>160</th></tr>";
	$i=1;
	foreach($sub_id as $sub)
	{
		$g_tot=0;
		$g_max=0;
		$a= "<tr><th style='text-align:left'>".strtoupper($sub_name[$i])."</th>";
		foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			$exam_type=$term_row['exam_type'];
			
				$sel_examdet=$dbobj->select("select * from `examschedule` where `examid`='".$tid."' and `classno`='".$classno."' and `sub_id`='".$sub."' ");
				$examdet_row=$dbobj->fetch_array($sel_examdet);
				$max_mark=$examdet_row['maxmark'];
				$min_mark=$examdet_row['minmark'];
				$max[$tid]=$max[$tid]+$max_mark;
				$sel_mark=$dbobj->select("select * from `report_card` where `sid`='".$sid."' and `classid`='".$class."' and `examid`='".$tid."' and `sub_id`='".$sub."' and `assessment`='".$exam_type."'");
				$mark_row=$dbobj->fetch_array($sel_mark);
				if($mark_row['mark']=="Absent" )
				{
					$a.= "<td style='color:red;text-align:center' >Ab</td>";
					$flag[$tid]="1";
					$a.= "<td></td>";
					
				}
				else
				{
					if($mark_row['mark']<$min_mark)
					{
						$a.= "<td style='color:red;text-align:center'>".$mark_row['mark']."</td>";
					}
					else
					{
						$a.= "<td style='text-align:center'>".$mark_row['mark']."</td>";
					}
					$m_tot[$tid]=$m_tot[$tid]+$mark_row['mark'];
					$g_tot=$g_tot+$mark_row['mark'];
					$g_max=$g_max+$max_mark;
					$a.= "<td style='text-align:center'>".grade_calculate($mark_row['mark'],$max_mark,$classno)."</td>";
				}

		}
		$a.= "<td></td><td></td></tr>";
	//$form.= "<th>".round(($g_tot/$g_max)*100,2)."</th></tr>";
	if($sub_name[$i]=="Moral Science")
	{
		$b=$a;
	}
	else
	{
		$form.=$a;
	}
	$i++;
	}
	foreach($opn_id as $opn)
	{
		$g_tot=0;
		$g_max=0;
		$opn_subject=$dbobj->selectall("opn_subjects",array("id"=>$opn));
		$form.= "<tr><th style='text-align:left'>".strtoupper($opn_subject['opn_sub'])."</th>";
		foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			$exam_type=$term_row['exam_type'];
			
			$sel_examdet=$dbobj->select("select * from `examschedule` where `examid`='".$tid."' and `classno`='".$classno."' and `sub_id`='0' and `opn_sub`='".$opn."'");
			$examdet_row=$dbobj->fetch_array($sel_examdet);
			$max_mark=$examdet_row['maxmark'];
			$min_mark=$examdet_row['minmark'];
			$max[$tid]=$max[$tid]+$max_mark;
			$sel_mark=$dbobj->select("select * from `report_card` where `sid`='".$sid."' and `classid`='".$class."' and `examid`='".$tid."' and `sub_id`='0' and `opn_sub`='".$opn."' and `assessment`='".$exam_type."'");
			 $mark_row=$dbobj->fetch_array($sel_mark);

			if($mark_row['mark']=="Absent" )
				{
					$form.= "<td style='color:red;text-align:center'>Ab</td>";
					$flag[$tid]="1";
					$form.= "<td></td>";
					
				}
			else
			{
			if($mark_row['mark']<$min_mark)
			{
					$form.= "<td style='color:red;text-align:center'>".$mark_row['mark']."</td>";
			}
			else
			{
				$form.= "<td style='text-align:center'>".$mark_row['mark']."</td>";
			}
			$m_tot[$tid]=$m_tot[$tid]+$mark_row['mark'];
			$g_tot=$g_tot+$mark_row['mark'];
			$g_max=$g_max+$max_mark;
			$form.= "<td style='text-align:center'>".grade_calculate($mark_row['mark'],$max_mark,$classno)."</td>";
			}
			
		}	
	
	$form.= "<td></td><td></td></tr>";
	//$form.= "<th>".round(($g_tot/$g_max)*100,2)."</th></tr>";
	}
	$form.=$b;			
$form.= "<tr><th style='text-align:left'>TOTAL</th>";
		foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_and_opn")
			{
				if($flag[$tid]!="1")
				{
					$form.="<td colspan='2' align='center'><b>".$m_tot[$tid]."</b></td>";
				}
				else
				{
					$form.="<td colspan='2' align='center'></td>";
				}
			}
		}
$form.= "<td colspan='2'></td></tr>";
$form.= "<tr><th style='text-align:left'>PERCENTAGE</th>";
		foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_and_opn")
			{
				if($flag[$tid]!="1")
				{
			
					$form.="<td colspan='2' align='center'><b>";
					if($max[$tid]!="0")
					{
					$form.=round(($m_tot[$tid]*100)/$max[$tid],2);
					}
					$form.="</b></td>";
				}
				else
				{
					$form.="<td colspan='2' align='center'></td>";
				}
			}
		}
$form.= "<td colspan='2'></td></tr>";
$form.= "<tr><th style='text-align:left'>GRADE</th>";
foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_and_opn")
			{
				if($flag[$tid]!="1")
				{
					$form.="<td colspan='2' align='center'><b>".grade_calculate($m_tot[$tid],$max[$tid],$classno)."</b></td>";
				}
				else
				{
					$form.="<td colspan='2' align='center'></td>";
				}
			}
		}
$form.= "<td colspan='2'></td></tr>";
$form.= "<tr><th style='text-align:left'>DATE OF REPORT</th>";
foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_and_opn")
			{
				$form.="<td colspan='2'></td>";
			}
		}
$form.= "<td colspan='2'></td></tr>";
$form.= "<tr><th style='text-align:left'>CLASS TEACHER</th>";
foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_and_opn")
			{
				$form.="<td colspan='2'></td>";
			}
		}
$form.= "<td colspan='2'></td></tr>";
$form.= "<tr><th style='text-align:left'>PARENT</th>";
foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_and_opn")
			{
				$form.="<td colspan='2'></td>";
			}
		}
$form.= "<td colspan='2'></td></tr>";
$form.= "<tr><th style='text-align:left'>PRINCIPAL</th>";
foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_and_opn")
			{
				$form.="<td colspan='2'></td>";
			}
		}
$form.= "<td colspan='2'></td></tr>";
//-----------------------------------------------------------------------------------------------------------------------------

$sel_term_only=$dbobj->select("select *  from `terms` where `classno`='".$classno."' and `exam_type`='term' "); 
$i=1;
while($term_only_row=$dbobj->fetch_array($sel_term_only))
{
	$term_only_id[$i]=$term_only_row['id'];
	$term_only_name[$i]=$term_only_row['term_name'];
	$i++;
}
//--------------------------------Remark-----------------------------------------------------------------------------------
$j=1;
$rem.="<table align='center' border='1' style='width:1200px;font-size:13px;border-collapse:collapse'><tr><th colspan='2' style='color:#B71D6F;'><b>REMARKS</b></th></tr>";
foreach ($term_only_id as $tid)
   {
	$rem.="<tr><td style='width:110px'><b>".strtoupper($term_only_name[$j])."</b></td>";
	$sel_remark=$dbobj->select("select * from remark where `sid`='".$sid."' and `term`='".$tid."' and `cls`='".$class."'");
	$rem_arry=$dbobj->fetch_array($sel_remark);
	$rem.="<td>".$rem_arry['remrk']."</td></tr>";
	$j++;
   }
   $rem.="</table>";
//-------------------------NON-ACADEMIC------------------------------------------------------
					if($classno<=6)
					{
						$section="LP";
					}
					else
					{
						$section="HS";
					}
$sel_non_acc=$dbobj->select("select * from `performance_area` where `section`='".$section."' order by `id` ");
		while($acc_row=$dbobj->fetch_array($sel_non_acc))
		{
			$head=$acc_row['areaname'];
			foreach($term_only_id as $tid)
			{
				$term_det=$dbobj->selectall("terms",array("id"=>$tid));
				$sel_non_grade=$dbobj->select("select `grade` from `performance_evaluation` where sid='".$sid."' and  `headid`='".$acc_row['id']."' and`term`='".$tid."' and `classid`='".$class."' and `acc_year`='".$academic_year."'");
				$grade_row=$dbobj->fetch_array($sel_non_grade);
				$grade=$grade_row['grade'];
				$nonacc[$head][$term_det['term_name']]=$grade;
			}
			
		}

	  /*--------------------------------Last Page---------------------*/

				  
				 
		 
//---------------------------------------------Attendance-----------------------------------

	$sel_term_for_attendance=$dbobj->select("select *  from `terms` where `classno`='".$classno."' and `exam_type`='term' "); 
$i=0;
while($att_term_row=$dbobj->fetch_array($sel_term_for_attendance))
   {
	   $termid[$i]=$att_term_row['id'];
	   $t=$att_term_row['id'];
	   if($att_term_row['term_name']=="Pre-Borad I")
	   {
	   $att_term_row['term_name']="II Term";
	   }
	   elseif($att_term_row['term_name']=="Pre-Board II")
	   {
	    $att_term_row['term_name']="III Term";
	   }
	   $t_name.="<td style='text-align:center;width:60px'><b>".$att_term_row['term_name']."</b></td>";
	   $p=0;
	   $sel_attandance=$dbobj->select("select *  from `temp_attendace` where `sid`='".$sid."' and `term`='".$t."' and `classid`='".$class."'");
       $attandance_row=$dbobj->fetch_array($sel_attandance);
		$working_days[$t]="<td>".$attandance_row['working_days']."</td>";
		$attendance[$t]="<td>".$attandance_row['attendance']."</td>";
		if($attandance_row['working_days']!="0" && $attandance_row['working_days']!="")
		{
			$p=round(($attandance_row['attendance']/$attandance_row['working_days'])*100,2);
		}
		$percentage[$t]="<td style='text-align:center'>".$p."</td>";
		$total_attendance=$total_attendance+$attandance_row['attendance'];
		$total_workingdays=$total_workingdays+$attandance_row['working_days'];
		$i++;
   }
   
$sel_at_term=$dbobj->selectall("terms",array("term_name"=>"I TERM","classno"=>$classno));
$smonth="2015-06-01";
$emonth="2015-08-01";
$sel_working=$dbobj->select("select distinct `date` from `attendence` where `date` >= '".$smonth."' AND `date` <= '".$emonth."'");
$work_days=mysql_num_rows($sel_working);

$sel_att=$dbobj->select("select * from `attendence` where `date` >= '".$smonth."' AND `date` <= '".$emonth."' and `status`='Present' and `studentid`='".$studentid."' and `classid`='".$class."'");
$attended_days=mysql_num_rows($sel_att);

   //$att.="<table align='center' style='border-collapse:collapse' border='1'><tr><th colspan='4'>Attendance</th></tr><tr><td></td>".$t_name."</tr>";
   $att.="<table align='center' style='border-collapse:collapse' border='1' width='450px'><tr><td><b>COMPONENTS</b></td>".strtoupper($t_name)."<td><b>TOTAL</b></td></tr>";
$att.="<tr><td style='height:30px'><b>WORKING DAYS</b></td>";
$att.="<td align='center'>".$work_days."</td></tr>";
$att.="<td align='center'></td></tr>";
$att.="<td align='center'></td></tr>";
$att.="<td align='center'></td></tr>";
$att.="<tr><td style='height:30px'><b>ATTENDED DAYS</b></td>";
$att.="<td align='center'>".$attended_days."</td></tr>";
$att.="<td align='center'></td></tr>";
$att.="<td align='center'></td></tr>";
$att.="<td align='center'></td></tr>";
$att.="<tr><td style='height:30px'><b>PERCENTAGE</b></td>";
$att.="<td align='center'>".round(($attended_days/$work_days)*100,2)."</td></tr>";
$att.="<td align='center'></td></tr>";
$att.="<td align='center'></td></tr>";
$att.="<td align='center'></td></tr>";
	$att.="</table>";
//----------------------------------Grade Calculate------------------------------------------
function grade_calculate($val,$val2,$classno)
{
	if($val!="" && $val2!="" && $val!="0" && $val2!="0")
	{
	$ma=($val/$val2)*100;
	$sel = mysql_query("select * from grades where classno = '$classno'");
	while($rowsel = mysql_fetch_array($sel))
		{
			$min_mark = $rowsel['min_mark'];
			$min_mark = $min_mark.".00";
			$max_mark = $rowsel['max_mark'];
			$max_mark = $max_mark."00";
			$gradename = $rowsel['gradename'];
			if($ma>=$min_mark && $ma<=$max_mark )
			{
				return $gradename;
			}
		}
	}
	
}
?>
<?php
$main="<table style='border:3px solid #B71D6F'  width='100%'>
<tr><td class='ftd' width='50%' style='border:1px solid #B71D6F'>
			<table style='font-size:12px'><tr><td>
			<table border='1' class='grade' align='center' style='text-align:left;'>
				<tr><td colspan='3' align='center' style='width:300px;color:#B71D6F'><b>ACADEMIC ASSESSMENT</b></td><td colspan='2' align='center' style='color:#B71D6F'><b>NON-ACADEMIC ASSESSMENT</b></td></tr>
				<tr><td  style='width:100px'><b>MARKS</b></td><td align='left'><b>GRADE</b></td><td><b>REMARK</b></td><td><b>GRADE</b></td><td ><b>REMARK</b></td></tr>
				<tr><td  style='border-bottom-style:none;'><b>90% AND ABOVE<b></td><td  style='border-bottom-style:none;text-align:left' ><b>A+</b></td><td  style='border-bottom-style:none;'><b>EXCELLENT</b></td><td style='border-bottom-style:none;'><b>A</b></td><td  style='border-bottom-style:none;'><b>VERY GOOD</b></td></tr>
				<tr><td  style='border-top-style:none;border-bottom-style:none;'><b>80 TO 89%<b></td><td  style='border-top-style:none;border-bottom-style:none;text-align:left'><b>A<b></td><td  style='border-top-style:none;border-bottom-style:none;'><b>V.GOOD</b></td><td style='border-bottom-style:none;border-top-style:none;'><b>B</b></td><td  style='border-top-style:none;border-bottom-style:none;'><b>GOOD</b></td></tr>
				<tr><td style='border-top-style:none;border-bottom-style:none;'><b>70 TO 79%<b></td><td  style='border-top-style:none;border-bottom-style:none;text-align:left'><b>B+</b></td><td style='border-top-style:none;border-bottom-style:none;'><b>GOOD</b></td><td style='border-bottom-style:none;border-top-style:none;'><b>C</b></td><td style='border-top-style:none;border-bottom-style:none;'><b>AVERAGE</b></td></tr>
				<tr><td  style='border-top-style:none;border-bottom-style:none;'><b>60 TO 69%<b></td><td  style='border-top-style:none;border-bottom-style:none;text-align:left'><b>B</b></td><td  style='border-top-style:none;border-bottom-style:none;'><b>FAIR</b></td><td style='border-bottom-style:none;border-top-style:none;'><b>D</b></td><td  style='border-top-style:none;border-bottom-style:none;'><b>POOR</b></td></tr>
				<tr><td  style='border-top-style:none;border-bottom-style:none;'><b>50 TO 59%<b></td><td  style='border-top-style:none;border-bottom-style:none;text-align:left'><b>C+</b></td><td  style='border-top-style:none;border-bottom-style:none;'><b>AVERAGE</b></td><td style='border-bottom-style:none;border-top-style:none;'></td><td  style='border-top-style:none;border-bottom-style:none;'></td></tr>
				<tr><td  style='border-top-style:none;border-bottom-style:none;'><b>40 TO 49%<b></td><td  style='border-top-style:none;border-bottom-style:none;text-align:left'><b>C</b></td><td  style='border-top-style:none;border-bottom-style:none;'><b>SATISFACTORY</b></td><td style='border-bottom-style:none;border-top-style:none;'></td><td  style='border-top-style:none;border-bottom-style:none;'></td></tr>
				<tr><td  style='border-top-style:none;'><b>BELOW 40%<b></td><td style='border-top-style:none;'><b>D</b></td><td style='border-top-style:none;text-align:left'><b>FAILED</b></td><td align='center' style='border-top-style:none;'></td><td align='center' style='border-top-style:none;'></td></tr>
			</table>
			<tr><td>
			<table border='1' class='grade' align='center'>
					<tr><td align='center' style='font-size:15px;border-bottom-style:none;color:#B71D6F' colspan='2'><b><u>CRITERIA FOR PROMOTION</u></b></td></tr>
					 <tr><td align='left' style='border-bottom-style:none;border-right-style:none;border-top-style:none;' valign='top'><b>01.</td><td style='border-bottom-style:none;border-left-style:none;border-top-style:none;'><b>THE ACADEMIC YEAR IS SEGMENTED AS TERM I, TERM II AND TERM III.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;' valign='top'><b>02.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>ALL SUBJECTS HAVE COMPONENTS OF INTERNAL ASSESSMENT ON THE BASIS OF ASSIGNMENTS/PROJECTS/CLASS TESTS/PRACTICAL WORKS.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;' valign='top'><b>03.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>NORMALLY A STUDENT SHOULD GAIN SEPARATE MINIMUM PASS MARK IN EACH SUBJECT. HE/SHE MAY BE CONSIDERED FOR PROMOTION EVEN IF HE/SHE FAILS IN ONE SUBJECT OTHER THAN MORAL SCIENCE, ENGLISH AND A POOR GRADE IN SUPW & C S.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;' valign='top'><b>04.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>PROMOTION TO A HIGHER CLASS IS BASED ON ATTENDANCE, CUMULATIVE PASS PERCENTAGE IN ALL MID TERM, TERMINAL EXAMINATIONS AND A GOOD GRADE IN NON ACADEMICS. </b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;' valign='top'><b>05.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>PUPILS WHO ABSENT THEMSELVES FROM EXAMINATIONS WITHOUT GRAVE REASONS WILL BE CONSIDERED AS FAILED. NO RE TEST WILL BE CONDUCTED FOR THE ABSENTEES.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;' valign='top'><b>06.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>SERIOUS ACTION INCLUDING SUSPENSION AND DISMISSAL WILL BE TAKEN AGAINST PUPILS WHO RESORT TO UNFAIR MEANS DURING EXAMINATIONS.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;' valign='top'><b>07.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>PARENTS/GUARDIANS SHOULD FOLLOW UP THE WARD’S PROGRESS IN ACADEMICS AND NON ACADEMICS AND TAKE NECESSARY STEPS IN ORDER TO ACHIEVE FURTHER IMPROVEMENT.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;' valign='top'><b>08.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>THE PROGRESS REPORT CARD SHOULD BE COLLECTED, ACKNOWLEDGED AND RETURNED ON THE STIPULATED DATES.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-bottom-style:none;border-right-style:none;'valign='top'><b>09.</td><td style='border-top-style:none;border-bottom-style:none;border-left-style:none;'><b>THE SCHOOL RESERVES THE RIGHT TO COMPEL PARENTS/GUARDIANS TO WITHDRAW THEIR WARDS IF THE PROGRESS IN ACADEMICS IS UNSATISFACTORY, ATTENDANCE IS IRREGULAR OR THE CONDUCT IS HARMFUL TO OTHER STUDENTS. NO STUDENT CAN REPEAT THE YEAR MORE THAN TWO YEARS.</b></td></tr>
					 <tr><td align='left' style='border-top-style:none;border-right-style:none;' valign='top'><b>10.</td><td style='border-top-style:none;'><b>THE DECISION OF THE PRINCIPAL IN ALL MATTERS IS FINAL AND BINDING.</b></td></tr>
			</table>
			</td></tr>
			<tr><td>
		<div style='text-align:center;font-size:11px;' width='100px'><b>I, the undersigned agree to abide by these norms and other rules with regard to examinations and promotion procedures.</b></div>

		</td></tr>
		<tr>
		<td>
		<table class='grade' style='border:none;'>
		<tr><td width='50%' style='text-align:left;'><table><tr><td style='width:220px;height:30px;border-style: solid;border-width: 1px;'></td></tr></table></td><td style='text-align:right;'><table><tr><td style='width:220px;height:30px;border-style: solid;border-width: 1px;'></td></tr></table></td></tr>
		<tr><td width='50%' style='text-align:left;'><b>PLACE</b></td><td style='text-align:right;'><b>NAME OF PARENT/GUARDIAN</b></td></tr>
		<tr><td width='50%' style='text-align:left;'><table><tr><td style='width:220px;height:30px;border-style: solid;border-width: 1px;'></td></tr></table></td><td style='text-align:right;'><table><tr><td style='width:220px;height:30px;border-style: solid;border-width: 1px;'></td></tr></table></td></tr>
		<tr><td width='50%' style='text-align:left;'><b>DATE</b></td><td style='text-align:right;'><b>SIGNATURE OF PARENT/GUARDIAN</b></td></tr>
		</table>
		</td>
		</tr>
			</table>
</td><td class='ftd' valign='top' width='50%' style='border:1px solid #B71D6F'>
<table  style='margin-top:5px auto;'>
	 <tr>
		<td align='center' colspan='2'><img src='../img/donbosco.jpg' style='width:120px' /></td>
	 </tr>
	 <tr>
		<td colspan='2' align='center' style='font-size:21px; color:#B71D6F;'><b>DON BOSCO SENIOR SECONDARY SCHOOL</b></td>
	</tr>
	<tr>
		<td align='center' colspan='2' align='center' style='font-size:16px;'><b>(I.C.S.E & I.S.C.KE-051/2000)<b></td>
	</tr>	
	<tr>
		<td align='center' colspan='2' align='center' style='font-size:16px;'><b>VADUTHALA, KOCHI-682 023</b></td>
	</tr>
	<tr>
		<td align='center' colspan='2' align='center' style='font-size:16px;'><b>PHONE:H.S:2435308, L.P: 2436738, PRINCIPAL:2436602</b></td>
	</tr>
	<tr>
		<td align='center' colspan='2' align='center' style='font-size:15px;'><b>E-MAIL: sdbschool@gmail.com, info@donboscoschoolvaduthala.com</b></td>
	</tr>
	<tr>
		<td align='center' colspan='2' align='center'  style='font-size:16px;'><b>WEBSITE : www.donboscoschoolvaduthala.com</b></td>
	</tr>
	<tr><td colspan='2'></td></tr>
	<tr><td align='center' colspan='2' style='font-size:21px; color:#B71D6F;' align='center'><b>PROGRESS REPORT</b></td></tr>
	<tr><td align='center' colspan='2' align='center' style='font-size:21px;color:#B71D6F;'><b>".$academic_year."</b></td></tr>
	<tr><td align='center' colspan='2' ><div class='photo'>";
	if($stud_photoid!="0")
		{
		$sel_photo=$dbobj->select("select * from `student_img` where `id`='".$stud_photoid."'");
		$photo_row=$dbobj->fetch_array($sel_photo);
		$imagebytes=$photo_row['image'];
		$type=$photo_row['type'];
		if($type=='')
		{
		$image= "<img  boreder='0' src='../imagetmp/default.jpg' width='130' height='130' alt='Photo' />";
		}
		else
		{
			$fh=fopen("../imagetmp/tmp".$type."","w");
			fwrite($fh,$imagebytes,strlen($imagebytes));
			$image="<img  border='0' src='../imagetmp/tmp".$type."' width='100' height='100' alt='Photo' />";
		}
		}
		else
		{
			$image= "<img  boreder='0' src='../imagetmp/default.jpg' width='100' height='100' alt='Photo' />";
		}
$main.=$image."</div></td></tr>
<tr><td colspan='2' align='center' style='font-size:20px;color:#B71D6F;'><b>".strtoupper($stname)." ".strtoupper($initial)."</b></td></tr>
<tr><td align='center'>
<table  width='500px' border='0' style='margin-top:35px;'>
		<tr><td ><b>CLASS: ".$classname."</b></td><td width='150px'><b>DIVISION: ".$division."</b></td><td width='150px'><b>ROLL NUMBER:  </b></td><td style='text-align:right'><b>ID.NO : ".$studentid."</b></td></tr>	
		</table>
		<table width='500px'>
		<tr><td><b>DATE OF BIRTH: ".$dob."</b></td></tr>	
		</table >
		<table width='500px'>
		<tr><td width='90px'><b>HOUSE: ".$huse."</b></td><td width='200px'><b>CLUB: ".$clb."</b></td><td width='200px'><b>SUPW: </b></td></tr>	
		</table>
		<table width='500px'>
		<tr><td ><b>FATHER’S  NAME: ".strtoupper($parent['fname'])."<b></td><td style='text-align:right'><b>MOTHER'S NAME: ".strtoupper($parent['mname'])."<b></td></tr>	
		<tr><td colspan='2'><b>ADDRESS: ".$adrs."</b></td></tr>
		</table>
		<table width='500px'>
		<tr><td colspan='2' width='350px'></td><td><b>PIN: </b></td></tr>
		</table>
		<table width='500px' border='0'>
		<tr><td width='350px'><b>PHONE NUMBER LAND: ".$parent['phone']."</b></td><td style='text-align:right'><b>MOBILE: ".$parent['fmob']."</b></td></tr>	
		</table>
		<table width='500px' style='text-align:center'>
		<tr><td></td><td></td><td></td></tr>
		<tr><td style='text-align:left'><table><tr><td style='width:150px;height:30px;border-style: solid;border-width: 1px;'></td></tr></table></td><td><table><tr><td style='width:150px;height:30px;border-style: solid;border-width: 1px;'></td></tr></table></td><td style='text-align:right'><table><tr><td style='width:150px;height:30px;border-style: solid;border-width: 1px;'></td></tr></table></td></tr>
		 <tr><td><b>FATHER'S SIGN</b></td><td><b>MOTHER'S SIGN</b></td><td><b>GUARDIAN'S SIGN</b></td></tr>
		</table>
</td></tr></table>
</td></tr>
</table>
</td></tr>
</table>
<table width='100%' style='margin-top:5px;border-collapse:collapse'>
	<tr>
		<td style='text-align:left;color:#B71D6F' width='25%' bgcolor='#E9BBD4'>DBSSS VADUTHALA</td>
		<td style='text-align:right;color:#B71D6F;border-right: solid #FFFFFF;' width='25%' bgcolor='#E9BBD4'>PROGRESS REPORT - ".$classname."</td>
		<td style='text-align:left;color:#B71D6F;' width='25%' bgcolor='#E9BBD4'>DBSSS VADUTHALA</td>
		<td style='text-align:right;color:#B71D6F' width='25%' bgcolor='#E9BBD4'>PROGRESS REPORT</td>
		</tr>
</table>";
$second="<table style='border:3px solid #B71D6F;border-collapse:collapse'><tr><td><table align='center' border='1' style='border-collapse:collapse;width:100%;font-size:12px'>
<tr><th style='text-align:center;font-size:14px;color:#B71D6F;' colspan='".$c."'><b>ACADEMIC PERFORMANCE</b> </th></tr>".$form."
<tr><th style='width:50%;font-size:14px;color:#B71D6F;' colspan='11'><b>NON-ACADEMIC PERFORMANCE</b></th><th style='width:50%;font-size:14px;color:#B71D6F;' colspan='8'><b>ATTENDANCE</b></th></tr>
<tr ><td valign='top' colspan='11'><table border='1' style='border-collapse:collapse;' width='900px'><tr><th style='text-align:left'>COMPONENTS</th><th style='width:70px'>I TERM</th><th style='width:70px'>II TERM</th><th style='width:70px'>III TERM</th><th width='170px' style='text-align:left'>COMPONENTS</th><th style='width:70px'>I TERM</th><th style='width:70px'>II TERM</th><th style='width:70px'>III TERM</th></tr>
																									<tr><td><b>CONDUCT</b></td><td>".$nonacc['CONDUCT']['I Term']."</td><td>".$nonacc['CONDUCT']['II Term']."</td><td>".$nonacc['CONDUCT']['III Term']."</td><td><b>SPEAKING ENGLISH<b></td><td>".$nonacc['SPEAKING ENGLISH']['I Term']."</td><td>".$nonacc['SPEAKING ENGLISH']['II Term']."</td><td>".$nonacc['SPEAKING ENGLISH']['III Term']."</td></tr>
																									<tr><td><b>RESPONSIBILITY</b></td><td>".$nonacc['RESPONSIBILITY']['I Term']."</td><td>".$nonacc['RESPONSIBILITY']['II Term']."</td><td>".$nonacc['RESPONSIBILITY']['III Term']."</td><td><b>ECA</b></td><td>".$nonacc['ECA']['I Term']."</td><td>".$nonacc['ECA']['II Term']."</td><td>".$nonacc['ECA']['III Term']."</td></tr>
																									<tr><td><b>PUNCTUALITY</b></td><td>".$nonacc['PUNCTUALITY']['I Term']."</td><td>".$nonacc['PUNCTUALITY']['II Term']."</td><td>".$nonacc['PUNCTUALITY']['III Term']."</td><td><b>AQUATICS</b></td><td>".$nonacc['AQUATICS']['I Term']."</td><td>".$nonacc['AQUATICS']['II Term']."</td><td>".$nonacc['AQUATICS']['III Term']."</td></tr>
																									<tr><td><b>DISCIPLINE</b></td><td>".$nonacc['DISCIPLINE']['I Term']."</td><td>".$nonacc['DISCIPLINE']['II Term']."</td><td>".$nonacc['DISCIPLINE']['III Term']."</td><td><b>SUPW & C.SERVICE</b></td><td>".$nonacc['SUPW & C.SERVICE']['I Term']."</td><td>".$nonacc['SUPW & C.SERVICE']['II Term']."</td><td>".$nonacc['SUPW & C.SERVICE']['III Term']."</td></tr>
																									<tr><td><b>LEADERSHIP</b></td><td>".$nonacc['LEADERSHIP']['I Term']."</td><td>".$nonacc['LEADERSHIP']['II Term']."</td><td>".$nonacc['LEADERSHIP']['III Term']."</td><td><b>GROUPS & MOVEMENTS</b></td><td>".$nonacc['LEADERSHIP']['I Term']."</td><td>".$nonacc['LEADERSHIP']['II Term']."</td><td>".$nonacc['LEADERSHIP']['III Term']."</td></tr>
																									</table>";

$second.="</td><td valign='top' colspan='6'>".$att."</td></tr><tr><td colspan='13' valign='top' style='border-bottom-style:none;'>".$rem."</td></tr><br><br>
<tr><th colspan='13' style='border-top-style:none;border-bottom-style:none;'></th></tr>
<tr><th colspan='13' style='border-top-style:none;border-bottom-style:none;'></th></tr>
<tr><th colspan='13' style='border-top-style:none;border-bottom-style:none;'></th></tr>
<tr><th colspan='13' style='border-top-style:none;border-bottom-style:none;'></th></tr>
<tr><th colspan='13' style='border-top-style:none;border-bottom-style:none;'></th></tr>
<tr><th colspan='13' style='border-top-style:none;'><b>SIGNATURE OF CLASS TEACHER &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SCHOOL SEAL &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SIGNATURE OF PRINCIPAL </b></th></tr>
</table></td></tr></table>
<table width='100%' bgcolor='#E9BBD4' style='margin-top:5px'><tr><td width='50%' style='text-align:left;border-right: solid #E9BBD4; color:#B71D6F'>DBSSS VADUTHALA</td><td width='50%' style='text-align:right;color:#B71D6F'>PROGRESS REPORT</td></tr></table>";
?>
<?php
include("../MPDF/mpdf.php");
 
$mpdf=new mPDF(); 

$mpdf->SetDisplayMode('fullpage','two');

//$mpdf->mirrorMargins = 1;
// LOAD a stylesheet
$stylesheet = file_get_contents('../css/progress_final1.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->setHeader();	// Clear headers before adding page
$mpdf->AddPage('A4','','','','',3,3,2,2,3,3);
$mpdf->WriteHTML($main);
$mpdf->AddPage('A4','','','','',8,8,25,3,3,3);
$mpdf->WriteHTML($second);
$mpdf->setHeader();	// Clear headers before adding page

$mpdf->Output('mpdf.pdf','I');
exit;
?>