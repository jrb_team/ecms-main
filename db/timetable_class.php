<?php
include_once('createdb.php');
include_once('../db/admin_class.php');
include_once('../db/sub_name.php');
class timetable
{
function Teacherdep($val)
{

			if($val=="1")
			{
				$sec="KINDERGARTEN";
			}
			if($val=="2")
			{
				$sec="L P SECTION";
			}				
			if($val=="3")
			{
				$sec="U P SECTION";
			}
			if($val=="4")
			{
				$sec="H S SECTION";
			}
			if($val=="5")
			{
				$sec="HSS SECTION";
			}
			if($val=="6")
			{
				$sec="LP";
			}	
			if($val=="7")
			{
				$sec="UP";
			}		
	return $sec;
}
function day($day)
{

			if($day=="Mon")
			{
				$name="Monday";
			}
			if($day=="Tue")
			{
				$name="Tuesday";
			}				
			if($day=="Wed")
			{
				$name="Wednesday";
			}
			if($day=="Thu")
			{
				$name="Thursday";
			}
			if($day=="Fri")
			{
				$name="Friday";
			}
	return $name;
}	
function teacherBYsection($val)
{

		$dbobject = new DB();
		$dbobject->getCon();  
  		$dep=$this->Teacherdep($val);
         if($dep=="KINDERGARTEN" || $dep=="H S SECTION")
			{
				$sql="select * from `teacher` where `status` IN ('active') and `group` IN ('TEACHER','HEADMISTRESS','SCHOOL-COORDINATOR') and `dep`='".$dep."' order by `name`";
			 $qry=$dbobject->select($sql);
			 $i=0;
			 while($row=$dbobject->fetch_array($qry))
			   	  {
					$teacher_details[$i]['name']=$row['name'];	
					$teacher_details[$i]['userid']=$row['userid'];	
					$i++;
				  } 
                  $teacher_det['misc']=$teacher_details;				  
			}	 
			if($dep=="LP")
			{

				$sql_lp1="select * from `teacher` where `status` IN ('active','1') and `group` IN ('TEACHER','HEADMISTRESS','SCHOOL-COORDINATOR') and `dep` IN ('L P SECTION') order by `dep`,`name`";  
				 $qry=$dbobject->select($sql_lp1);
				 $i=0;
				 while($row=$dbobject->fetch_array($qry))
					  {
						$teacher_det_lp[$i]['name']=$row['name'];	
						$teacher_det_lp[$i]['userid']=$row['userid'];	
						$teacher_det_lp[$i]['dep']=$row['dep'];	
						$i++;
					  }	
				$sql_lp2="select * from `teacher` where `status` IN ('active','1') and `group` IN ('TEACHER','HEADMISTRESS','SCHOOL-COORDINATOR') and `dep` IN ('KINDERGARTEN') order by `dep`,`name`";  
				 $qry2=$dbobject->select($sql_lp2);
				 $i=0;
				 while($row2=$dbobject->fetch_array($qry2))
					  {
						$teacher_det_kg[$i]['name']=$row2['name'];	
						$teacher_det_kg[$i]['userid']=$row2['userid'];	
						$teacher_det_kg[$i]['dep']=$row2['dep'];	
						$i++;
					  }	
				$sql_lp3="select * from `teacher` where `status` IN ('active','1') and `group` IN ('TEACHER','HEADMISTRESS','SCHOOL-COORDINATOR') and `dep` IN ('CONSOLIDATED') order by `dep`,`name`";  
				 $qry3=$dbobject->select($sql_lp3);
				 $i=0;
				 while($row3=$dbobject->fetch_array($qry3))
					  {
						$teacher_det_consol[$i]['name']=$row3['name'];	
						$teacher_det_consol[$i]['userid']=$row3['userid'];	
						$teacher_det_consol[$i]['dep']=$row3['dep'];	
						$i++;
					  }						  
				$sql_lp4="select * from `teacher` where  `userid` IN('DBHSSP230','DBHSSP113')";  
				 $qry4=$dbobject->select($sql_lp4);
				 $i=0;
				 while($row4=$dbobject->fetch_array($qry4))
					  {
						$teacher_det_misc[$i]['name']=$row4['name'];	
						$teacher_det_misc[$i]['userid']=$row4['userid'];	
						$teacher_det_misc[$i]['dep']=$row4['dep'];	
						$i++;
					  }
			   $teacher_det['kg']=$teacher_det_kg;
               $teacher_det['lp']=$teacher_det_lp;              					  
               $teacher_det['consol']=$teacher_det_consol;					  
               $teacher_det['misc']=$teacher_det_misc;					  
			}
			if($dep=="UP")
			{

				$sql_lp1="select * from `teacher` where `status` IN ('active','1') and `group` IN ('TEACHER','HEADMISTRESS','SCHOOL-COORDINATOR','Pincipal','ADMINISTRATOR','Librarian') and `dep` IN ('HSS SECTION') and `userid` NOT IN('DBHSSP230','DBHSSP113','DBHSSP131') order by `dep`,`name`" ;  
				 $qry=$dbobject->select($sql_lp1);
				 $i=0;
				 while($row=$dbobject->fetch_array($qry))
					  {
						$teacher_det_hss[$i]['name']=$row['name'];	
						$teacher_det_hss[$i]['userid']=$row['userid'];	
						$teacher_det_hss[$i]['dep']=$row['dep'];	
						$i++;
					  }	

				$sql_lp2="select * from `teacher` where `status` IN ('active','1') and `group` IN ('TEACHER','HEADMISTRESS','SCHOOL-COORDINATOR','Pincipal','ADMINISTRATOR','Librarian') and `dep` IN ('U P SECTION') and `userid` NOT IN('DBHSSP230','DBHSSP113','DBHSSP131') order by `dep`,`name`" ;  
				 $qry2=$dbobject->select($sql_lp2);
				 $i=0;
				 while($row2=$dbobject->fetch_array($qry2))
					  {
						$teacher_det_up[$i]['name']=$row2['name'];	
						$teacher_det_up[$i]['userid']=$row2['userid'];	
						$teacher_det_up[$i]['dep']=$row2['dep'];	
						$i++;
					  }	
				$sql_lp3="select * from `teacher` where `status` IN ('active','1') and `group` IN ('TEACHER','HEADMISTRESS','SCHOOL-COORDINATOR','Pincipal','ADMINISTRATOR','Librarian') and `dep` IN ('H S SECTION') and `userid` NOT IN('DBHSSP230','DBHSSP113','DBHSSP131') order by `dep`,`name`" ;  
				 $qry3=$dbobject->select($sql_lp3);
				 $i=0;
				 while($row3=$dbobject->fetch_array($qry3))
					  {
						$teacher_det_hs[$i]['name']=$row3['name'];	
						$teacher_det_hs[$i]['userid']=$row3['userid'];	
						$teacher_det_hs[$i]['dep']=$row3['dep'];	
						$i++;
					  }	
				$sql_lp4="select * from `teacher` where `status` IN ('active','1') and `group` IN ('TEACHER','HEADMISTRESS','SCHOOL-COORDINATOR','Pincipal','ADMINISTRATOR','Librarian') and `dep` IN ('LAB ASSISTANT HSS SCALE','ADMINISTRATION','LIBRARIAN UP SCALE') and `userid` NOT IN('DBHSSP230','DBHSSP113','DBHSSP131') order by `dep`,`name`" ;  
				 $qry4=$dbobject->select($sql_lp4);
				 $i=0;
				 while($row4=$dbobject->fetch_array($qry4))
					  {
						$teacher_det_misc[$i]['name']=$row4['name'];	
						$teacher_det_misc[$i]['userid']=$row4['userid'];	
						$teacher_det_misc[$i]['dep']=$row4['dep'];	
						$i++;
					  }
					  
               $teacher_det['up']=$teacher_det_up;	
               $teacher_det['hs']=$teacher_det_hs;	
               $teacher_det['hss']=$teacher_det_hss;				   
               $teacher_det['misc']=$teacher_det_misc;						  
			}			
	 return $teacher_det;
}
function teacher_period_class($classid,$day,$pid,$acyear)
{
	$dbobject = new DB();
	$dbobject->getCon(); 
	$classno_dt=$dbobject->selectall("sclass",array("classid"=>$classid)); 
	$sel=$dbobject->select("select * from `timetable` where `classid`='".$classid."' and `day`='".$day."' and `period`='".$pid."' and `acyear`='".$acyear."'");
	$i=0;
		while($row=$dbobject->fetch_array($sel)){
			if($row['subject_type']=='extrasubject')
				{
					$subject_det=$dbobject->selectall("extra_subject",array("id"=>$row['subject']));
					$subname=$subject_det['subject'];
				}
			elseif($row['subject_type']=='subject')
				{
					$admin=new Admin($classno_dt['classno']);
					$subject_table=$admin->subject_table();
					$subject_det=$dbobject->selectall($subject_table,array("sub_id"=>$row['subject']));
					$subname=$subject_det['subject'];
				}
			elseif($row['subject_type']=="addtional")
				{
					$subject_det=$dbobject->selectall("opn_subjects",array("id"=>$row['subject']));
					$subname=$subject_det['opn_sub'];
				}													
			elseif($row['subject_type']=="subhead")
				{
					$subject_det=$dbobject->selectall("sub_heads",array("id"=>$row['subject']));
					$subname=$subject_det['head_name'];
				}
													
			$subject_name=new Subject($subname);
			$sub_name=$subject_name->sub_table();
            $periode[$i]=$sub_name;;
			$i++;
		}
		return $periode;
}	
function getSubjectClasslist($acyear,$tid){
	$dbobject = new DB();
	$dbobject->getCon(); 
	$sel=$dbobject->select("select DISTINCT `timetable`.`classid`,`classname`,`classno`,`division` from `timetable` LEFT JOIN `sclass` on `timetable`.`classid`=`sclass`.`classid` where `subject_teacher`='".$tid."' and `acyear`='".$acyear."'");
	$i=0;
	$data=array();
	while($row=$dbobject->fetch_array($sel)){
		$data[$i]['classid']=$row['classid'];
		$data[$i]['classno']=$row['classno'];
		$data[$i]['classname']=$row['classname'];
		$data[$i]['division']=$row['division'];
		$i++;
	}
	return $data;
}
function getsubject_listByClassno($classno,$tid,$acyear){
	$dbobject = new DB();
	$dbobject->getCon(); 
	$division=$dbobject->get_DivisionByAcyear($classno,$acyear);
	$d_count=count($division);
	$data=array();
	if($d_count>0){
	$sql="select DISTINCT `timetable`.`subject` from `timetable`  where `subject_teacher`='".$tid."' and `acyear`='".$acyear."'";
	$sql.="AND `classid` IN (";
	$j=1;
	if(!empty($division)){
		foreach($division as $div){
			$sql.="'".$div['classid']."'";
			if($j<$d_count){
				$sql.=",";
			}
			$j++;
		}
	}
	$sql.=")";
	$sel=$dbobject->select($sql);
	$num_rows=$dbobject->mysql_num_row($sel);
	$i=0;
	while($row=$dbobject->fetch_array($sel)){
		$data[$i]['subid']=$row['subject'];
		$admin=new Admin($classno);
		$subject_table=$admin->subject_table();
		$subject_det=$dbobject->selectall($subject_table,array("sub_id"=>$row['subject']));
		$subname=$subject_det['subject'];
		$data[$i]['subject']=$subname;
		$i++;
	}
	}
	return $data;
}
function colorBYsec($sec)
{

			if($sec==1)
			{
				$color="color:#93C572";
			}
			if($sec==6)
			{
				$color="color:#483d8b";
			}
			if($sec==7)
			{
				$color="color:#c57fc5";
			}			
	return $color;
}
		function get_periods_kg($acyear,$classid)
	{
	    				$dbobject = new DB();
		$dbobject->getCon(); 
		$peroid=$dbobject->select("select * from `kg_period_settings`");
                 $i=0;
		while($period_row=$dbobject->fetch_array($peroid))
		{
			$periods[$i]=$period_row['period_name'];
			$i++;
		}
	  return $periods;
	}
      function chk_subject_per_period_day($tid,$acyear,$period_id,$subject,$day)
	  {
		$dbobject = new DB();
		$dbobject->getCon();
		//echo "select * from `timetable` where `acyear`='".$acyear."' and `subject_teacher`='".$userid."' and `period`='".$period."' and `subject_type`='".$subject_type."' and `subject`='".$subject."' and `classid`='".$classid."'";
		//echo "select * from `timetable` where `acyear`='".$acyear."'  and `period`='".$period_id."' and `subject_teacher`='".$tid."' and `day`='".$day."'";
		//echo "select * from `timetable` where `acyear`='".$acyear."'  and `period`='".$period_id."' and `subject_teacher`='".$tid."' and `day`='".$day."'";
		$qry=$dbobject->select("select * from `timetable` where `acyear`='".$acyear."'  and `period`='".$period_id."' and `subject_teacher`='".$tid."' and `day`='".$day."'");
		return $chk=mysql_num_rows($qry);	  
	  }	
function no_of_period_inserted($classid,$subject,$subject_type,$acyear,$tid)
{
				$dbobject = new DB();
				//echo "SELECT * FROM `teacher_subject` WHERE `classid`='".$classid."' and `subject`='".$subject."' and `type`='".$type."' and `acyear`='".$acyear."' limit 1";
				$qry=$dbobject->select("SELECT * FROM `timetable` WHERE `classid`='".$classid."' and `subject`='".$subject."' and `subject_type`='".$subject_type."' and `acyear`='".$acyear."' and `subject_teacher`='".$tid."'");
				$chk_num=mysql_num_rows($qry);
				return $chk_num;	
}	
function no_of_period_alloted($classid,$subject,$type,$acyear,$teacherid)
{
	$dbobject = new DB();
	//echo "SELECT * FROM `teacher_subject` WHERE `classid`='".$classid."' and `subject`='".$subject."' and `type`='".$type."' and `acyear`='".$acyear."' limit 1";
	$qry=$dbobject->select("SELECT * FROM `teacher_subject` WHERE `classid`='".$classid."' and `subject`='".$subject."' and `type`='".$type."' and `acyear`='".$acyear."'and `teacherid`='".$teacherid."'");
    while($row=$dbobject->fetch_array($qry))
 		 {
		  $no_of_period=$no_of_period+$row['no_of_period'];
		 }
	return $no_of_period;
}
function lab_subjects()
{
	$data['id']=array(50,57,64,71,78,85,92,93,94,95,96,103,104,105,106,107,112,113,141,142,143,144,145);
    $data['qry']="'50','57','64','71','78','85','92','93','94','95','96','103','104','105','106','107','112','113','141','142','143','144','145'";
	return $data;
}
function distinct_lab_subjects()
{
	$dbobject = new DB();
	//echo "SELECT * FROM `teacher_subject` WHERE `classid`='".$classid."' and `subject`='".$subject."' and `type`='".$type."' and `acyear`='".$acyear."' limit 1";
	$lab_subjects=$this->lab_subjects();
	$qry=$dbobject->select("SELECT distinct `subject` FROM `extra_subject` WHERE `id` IN (".$lab_subjects['qry'].")");
    while($row=$dbobject->fetch_array($qry))
 		 {
		  $data[$row['subject']]=$row['subject'];
		 }
	return $data;
}
function lab_timetable_daywise($subject,$acyear)
{
	$dbobject = new DB();
	$qry=$dbobject->select("SELECT * FROM `extra_subject` WHERE `subject` IN ('".$subject."')");
    while($row=$dbobject->fetch_array($qry))
 		 {
		  $data[$row['id']]=$row['id'];
		 }
		 $query_format=$dbobject->query_format($data);
		$peroid=$dbobject->select("select * from `period_settings`");
		$i=1;
		while($period_row=$dbobject->fetch_array($peroid))
		{
			$periods[$i]=$period_row['period_name'];
			$i++;
		}	
		$working_days=$dbobject->select("select * from `workingdays`");
		$wrk_num=mysql_num_rows($working_days);
		$z=1;
		while($wrk_row=$dbobject->fetch_array($working_days))
		{
			$day[$z]=$wrk_row['day'];
			$z++;
		}
        foreach($day as $d)
		{
			
			foreach($periods as $p)
			{
				
				$p_det=$dbobject->selectall("period_settings",array("period_name"=>$p));
				$sel=$dbobject->select("select * from `timetable` where `day`='".$d."' and `period`='".$p_det['id']."' and `subject` IN (".$query_format.") and `subject_type`='extrasubject' and`acyear`='".$acyear."'");
			    while($sel_row=$dbobject->fetch_array($sel))
				{
					$subject_data=$sel_row['subject_type']."-".$sel_row['subject'];
					
					$classid=$sel_row['classid'];
					$subject_teacher=$sel_row['subject_teacher'];
					$data2[$p][$d][]=$subject_data."*".$classid."*".$subject_teacher;
				}
			}
		}
		$data_temp['timetable']=$data2;
		$data_temp['periods']=$periods;
		$data_temp['day']=$day;
return $data_temp;		
}
function lab_timetable_daywise2($subject,$acyear)
{
	$dbobject = new DB();
	if($subject=="COMP LAB1")
	{
	$sql="SELECT * FROM `extra_subject` WHERE `subject` IN ('IT LAB','COMP LAB','MATH LAB') and `classno`>='7' and `classno`<='12'";		
	}
	elseif($subject=="COMP LAB2")
	{
	$sql="SELECT * FROM `extra_subject` WHERE `subject` IN ('IT LAB') and `classno`>='3' and `classno`<='6'";		
	}
	elseif($subject=="ECA")
	{
	$sql="SELECT * FROM `extra_subject` WHERE `subject` IN ('ECA')";		
	}	
	else
	{
	$sql="SELECT * FROM `extra_subject` WHERE `subject` IN ('".$subject."')";	
	}
	$qry=$dbobject->select($sql);
    while($row=$dbobject->fetch_array($qry))
 		 {
		  $data[$row['id']]=$row['id'];
		 }
		 $query_format=$dbobject->query_format($data);
		$peroid=$dbobject->select("select * from `period_settings`");
		$i=1;
		while($period_row=$dbobject->fetch_array($peroid))
		{
			$periods[$i]=$period_row['period_name'];
			$i++;
		}	
		$working_days=$dbobject->select("select * from `workingdays`");
		$wrk_num=mysql_num_rows($working_days);
		$z=1;
		while($wrk_row=$dbobject->fetch_array($working_days))
		{
			$day[$z]=$wrk_row['day'];
			$z++;
		}
        foreach($day as $d)
		{
			
			foreach($periods as $p)
			{
				
				$p_det=$dbobject->selectall("period_settings",array("period_name"=>$p));
				if($subject=="ECA")
				{
				$sql2="select * from `timetable` where `day`='".$d."' and `period`='".$p_det['id']."' and `subject` IN (".$query_format.",'ECA') and `subject_type`IN ('extrasubject','custom') and`acyear`='".$acyear."'";	
				}
				else
				{
				$sql2="select * from `timetable` where `day`='".$d."' and `period`='".$p_det['id']."' and `subject` IN (".$query_format.") and `subject_type`='extrasubject' and`acyear`='".$acyear."'";	
				}
				$sel=$dbobject->select($sql2);
			    while($sel_row=$dbobject->fetch_array($sel))
				{
					$subject_data=$sel_row['subject_type']."-".$sel_row['subject'];
					
					$classid=$sel_row['classid'];
					$subject_teacher=$sel_row['subject_teacher'];
					$data2[$p][$d][]=$subject_data."*".$classid."*".$subject_teacher;
				}
			}
		}
		$data_temp['timetable']=$data2;
		$data_temp['periods']=$periods;
		$data_temp['day']=$day;
return $data_temp;		
}
}
?>