<?php
class Admin
{

	public function __construct($classno)
	{
		$this->classno=$classno;
		
	}
	function subject_table()
	{
		$classno= $this->classno;
		if($classno == '0')
		{
			$tablename = "subject_lkg";
			
		}
		elseif($classno == '1')
		{
			$tablename = "subject_ukg";
			
		}
		elseif($classno == '2')
		{
			$tablename = "subject_1";
		
		}
		elseif($classno=='3')
		{
			$tablename = "subject_2";

		}
		elseif($classno=='4')
		{
			$tablename = "subject_3";

		}
		elseif($classno=='5')
		{
			$tablename = "subject_4";
		}
		elseif($classno=='6')
		{
			$tablename = "subject_5";

		}
		elseif($classno=='7')
		{
			$tablename = "subject_6";

		}
		elseif($classno=='8')
		{
			$tablename = "subject_7";

		}
		elseif($classno=='9')
		{
			$tablename = "subject_8";

		}
		elseif($classno=='10')
		{
			$tablename = "subject_9";

		}
		elseif($classno=='11')
		{
			$tablename = "subject_10";

		}
		elseif($classno=='12')
		{
			$tablename = "subject_11";

		}
		elseif($classno=='13')
		{
			$tablename = "subject_12";

		}
		elseif($classno=='14')
		{
			$tablename = "subject_13";

		}
		else
		{
		  $tablename = "subject_table_main";  
		}
		return $tablename;
	}
	function mark_table()
	{
		$classno= $this->classno;
		if($classno=='1')
		{
			$mark_table = "mark_lkg";

		}
		if($classno=='2')
		{
			$mark_table = "mark_lkg";

		}
		if($classno=='3')
		{
			$mark_table = "mark_1";

		}
		if($classno=='4')
		{
			$mark_table = "mark_2";

		}
		if($classno=='5')
		{
			$mark_table = "mark_3";

		}
		if($classno=='6')
		{
			$mark_table = "mark_4";

		}
		if($classno=='7')
		{
			$mark_table= "mark_6";

		}
		if($classno=='8')
		{
			$mark_table = "mark_6";

		}
		if($classno=='9')
		{
			$mark_table = "mark_6";

		}
		if($classno=='10')
		{
			$mark_table= "mark_6";

		}
		if($classno=='11')
		{
			$mark_table = "mark_6";

		}
		if($classno=='12')
		{
			$mark_table= "mark_6";

		}
		if($classno=='13')
		{
			$mark_table= "mark_11";

		}
		if($classno=='14')
		{
			$mark_table = "mark_12";

		}
		return $mark_table;
	}
	function assmdiv($assm)
	{
		$classno= $this->classno;
		if($classno>=8 && $classno<=12)
		{
			if($assm=="FA 1" || $assm=="FA 2" || $assm=="FA 3" || $assm=="FA 4")
			{
				$div=10;
			}
			elseif($assm=="SA 1" || $assm=="SA 2")
			{
				$div=30;
			}
		}
		return $div;
	}
	function section()
	{
		$classno= $this->classno;
	if($classno=="0")

		{
			$section="ARK";
		}
		if($classno >= '1' && $classno <= '2')
		{
			$section="KG";
		}
		if($classno >= '3' && $classno <= '6')
		{
		$section="LP";
		}
		if($classno >= '7' && $classno <= '10')
		{
		$section="UP";
		}
		if($classno >= '11' && $classno <= '12')
		{
		 $section ="HS";
		}
		elseif($classno >= '13' && $classno <= '14')
		{
		 $section="HSS";
		}
		return $section;
	}
	function classnoBysection($section)
	{
		if($section=="KG")
		{
			$classno[0]=1;
			$classno[1]=2;
		}
		if($section=="LP")
		{
			$classno[0]=3;
			$classno[1]=4;
			$classno[2]=5;
			$classno[3]=6;			
		}
		if($section=="UP")
		{
			$classno[0]=7;
			$classno[1]=8;
			$classno[2]=9;
			$classno[3]=10;		
				
		}		
		if($section=="HS")
		{
			$classno[4]=11;
			$classno[5]=12;				
		}			
		if($section=="HSS")
		{
			$classno[0]=13;
			$classno[1]=14;
		}
        if($section=="-1")
		{
			$classno[1]=1;
			$classno[2]=2;	
			$classno[3]=3;
			$classno[4]=4;
			$classno[5]=5;
			$classno[6]=6;	
			$classno[7]=7;
			$classno[8]=8;
			$classno[9]=9;
			$classno[10]=10;		
			$classno[11]=11;
			$classno[12]=12;	
			$classno[13]=13;
			$classno[14]=14;			
		} 		
		
		return $classno;
	}	
	function section_new()
	{
		$classno= $this->classno;
	  if($classno=="0")

		{
			$section="ARK";
		}
		elseif($classno >= '1' && $classno <= '2')
		{
			$section="KG";
		}
		elseif($classno >= '3' && $classno <= '6')
		{
		$section="LP";
		}
		elseif($classno >= '7' && $classno <= '10')
		{
		$section="UP";
		}
		elseif($classno >= '11' && $classno <= '12')
		{
		 $section ="HS";
		}
		elseif($classno >= '13' && $classno <= '14')
		{
		 $section="HSS";
		}
		return $section;		
	}
}
?>