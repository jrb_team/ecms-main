<?php
include_once('createdb.php');
class VSR
{
    function AccountDetails(){
		$dbobject = new DB();
        $dbobject->getCon();
      /*  $data['username']="eduweb\VSR02024141";
        $data['password']='rJ2-bJ3%';
        $data['ProviderID']='02024141';*/
		$vsr_settings=$dbobject->selectall("vsr_settings",array("id"=>"1"));
		$data['username']=$vsr_settings['username'];
        $data['password']=$vsr_settings['password'];
        $data['ProviderID']="02024144";
		$data['next_batch']=$vsr_settings['next_batch'];
        $data['vendor']='JRBoon';
        $data['software']='eCMS';
        $data['version']='1.2';
        $data['providerVSNContactPersonName']=$vsr_settings['contact_person'];
        $data['providerContactPhoneNumber']=$vsr_settings['contact_number'];
		$data['batchserviceHost']="https://wwwstg.eduweb.vic.gov.au/VSRAssuranceWebservice/BatchService.asmx";
        return $data;
    }
    function getBatchCount(){
        $dbobject = new DB();
        $dbobject->getCon();
        $sel=$dbobject->select("SELECT * FROM `vsr_batch`");
        $num_rows=mysql_num_rows($sel);
        return $num_rows;
    }
	function vsr_status_array()
	{
		$seltype2_opn_array=array("4"=>"Enrol","1"=>"Exit","2"=>"Cancel/Delete","3"=>"Student Details Change","0"=>"All");
		return $seltype2_opn_array;
	} 
	function vsr_report_sql_count_by_status($acyear,$period,$period_type,$period_type2,$seltype2,$batchid)
	{
		$sql="SELECT `status`,`studentid` FROM `vsr_batch_details` WHERE `acyear`='".$acyear."'";
		if($seltype2!="" && $seltype2!="-1")
		{
		    $sql.=" and `status`='".$seltype2."'";	
		}		
		if($batchid!="" && $batchid!="-1")
		{
		    $sql.=" and `batchid`='".$batchid."'";	
		}
		if($period=="today")
		{
		    $sql.=" and `date`='".date('Y-m-d')."'";	
		}
		if($period=="month")
		{
			$year = date("Y"); 	
			$rmonth = ucfirst($period_type); 	
			$timestamp    = strtotime(''.$rmonth.' '.$year.'');
			$first_second = date('Y-m-01', $timestamp);
			$last_second  = date('Y-m-t', $timestamp); // A leap year!			
			$sql.=" and `date`>='".$first_second."' and `date`<='".$last_second."'";	
		}		
		if($period=="date")
		{
		    $sql.=" and `date`='".date('Y-m-d',strtotime($period_type))."'";	
		}
		if($period=="Custom")
		{

			$first_second = date('Y-m-d',strtotime($period_type));
			$last_second  =date('Y-m-d',strtotime($period_type2)); // A leap year!			
			$sql.=" and `date`>='".$first_second."' and `date`<='".$last_second."'";	
		}
        $sql.="   and `entry_status`='1'";		
        $sql.=" order by `status`";		
        return 	$sql;	
	}
	function vsr_report_count_by_status($acyear,$period,$period_type,$period_type2,$seltype2,$batchid)
	{
        $dbobject = new DB();
        $dbobject->getCon();
		$vsr_status_array=$this->vsr_status_array();
		$vsr_report_sql=$this->vsr_report_sql_count_by_status($acyear,$period,$period_type,$period_type2,$seltype2,$batchid);
		$vsr_report_qry=$dbobject->select($vsr_report_sql);		
        while($row=$dbobject->fetch_array($vsr_report_qry))
		{
			$data[$row['status']]++;
		}
        return $data;		
	}
	function vsr_report_sql_data_by_status($acyear,$period,$period_type,$period_type2,$seltype2,$batchid)
	{
        $dbobject = new DB();
        $dbobject->getCon();		
		$sql="SELECT `status`,`studentid`,`date` FROM `vsr_batch_details` WHERE `acyear`='".$acyear."'";
		if($seltype2!="" && $seltype2!="-1")
		{
		    $sql.=" and `status`='".$seltype2."'";	
		}		
		if($batchid!="" && $batchid!="-1")
		{
		    $sql.=" and `batchid`='".$batchid."'";	
		}
		if($period=="today")
		{
		    $sql.=" and `date`='".date('Y-m-d')."'";	
		}
		if($period=="month")
		{
			$year = date("Y"); 	
			$rmonth = ucfirst($period_type); 	
			$timestamp    = strtotime(''.$rmonth.' '.$year.'');
			$first_second = date('Y-m-01', $timestamp);
			$last_second  = date('Y-m-t', $timestamp); // A leap year!			
			$sql.=" and `date`>='".$first_second."' and `date`<='".$last_second."'";	
		}		
		if($period=="date")
		{
		    $sql.=" and `date`='".date('Y-m-d',strtotime($period_type))."'";	
		}
		if($period=="Custom")
		{

			$first_second = date('Y-m-d',strtotime($period_type));
			$last_second  =date('Y-m-d',strtotime($period_type2)); // A leap year!			
			$sql.=" and `date`>='".$first_second."' and `date`<='".$last_second."'";	
		}
       // $sql.=" order by `status`";	
         $sql.="   and `entry_status`='1'";		   
		$vsr_report_qry=$dbobject->select($sql);		
        while($row=$dbobject->fetch_array($vsr_report_qry))
		{
			$data[$row['studentid']]['studentid']=$row['studentid'];
			$data[$row['studentid']]['status']=$row['status'];
			$data[$row['studentid']]['date']=$row['date'];
		}
        return $data;		
	}
function title_by_status_vsr($status)
{
	if($status==4)
	{
	    $array=array("type"=>"Type","VSN"=>"VSN","ProviderStudentID"=>"Provider Student ID","PreviousProviderID"=>"Previous Provider ID","StudentFirstName"=>"First Name","StudentMiddleName"=>"Middle Name","StudentLastName"=>"Last Name","StudentDateOfBirth"=>"Date Of Birth","StudentGender"=>"Gender","EnrolmentDate"=>"Enrolment Date","EnrolledBefore"=>"Enrolled Before Indicator","AccessRestriction"=>"Access Restriction","TimeFraction"=>"Time Fraction","ProviderUserID"=>"Provider Userid","s_status"=>"Student Status");
	}
	if($status==1)
	{
		$array=array("type"=>"Type","VSN"=>"VSN","ProviderStudentID"=>"Provider StudentID","StudentFirstName"=>"Student First Name","StudentLastName"=>"Student Last Name","StudentMiddleName"=>"Student Middle Name","StudentDateOfBirth"=>"Student Date Of Birth","StudentGender"=>"Student Gender","EnrolmentDate"=>"Enrolment Date","LastAttendanceDate"=>"Last Attendance Date","ExitDate"=>"Exit Date","Deceased"=>"Deceased Flag","ProviderUserID"=>"Provider Userid","s_status"=>"Student Status");	
	}
	if($status==2)
	{
		$array=array("type"=>"Type","VSN"=>"VSN","ProviderStudentID"=>"Provider StudentID","StudentFirstName"=>"Student First Name","StudentLastName"=>"Student Last Name","StudentMiddleName"=>"Student Middle Name","StudentDateOfBirth"=>"Student Date Of Birth","StudentGender"=>"Student Gender","EnrolmentDate"=>"Enrolment Date","LastAttendanceDate"=>"Last Attendance Date","CancelDate"=>"Cancel Date","ProviderUserID"=>"Provider Userid","s_status"=>"Student Status");	
	}
	if($status==3)
	{
		$array=array("type"=>"Type","VSN"=>"VSN","ProviderStudentID"=>"Provider Student ID","StudentFirstName"=>"Current First Name","StudentLastName"=>"Current Last Name","StudentMiddleName"=>"Current Middle Name","StudentDateOfBirth"=>"Current Date Of Birth","StudentGender"=>"Current Gender","EnrolmentDate"=>"Current Enrolment Date","AccessRestriction"=>"Current Access Restriction","NewFirstName"=>"New First Name","NewLastName"=>"New Last Name","NewMiddleName"=>"New Middle Name","NewDateOfBirth"=>"New Date Of Birth","NewGender"=>"New Gender","NewProviderStudentID"=>"New Provider Student ID","NewEnrolmentDate"=>"New Enrolment Date","NewAccessRestriction"=>"New Access Restriction","ProviderUserID"=>"Provider User ID","s_status"=>"Student Status");	
	}
    if($status==5)
	{
	    $array=array("type"=>"Type","StudentFirstName"=>"First Name","s_sur_name"=>"Middle Name","StudentLastName"=>"Last Name","VSN"=>"VSN","StudentDateOfBirth"=>"Date Of Birth","StudentGender"=>"Gender","EnrolmentDate"=>"Enrolment Date","EnrolledBefore"=>"Enrolled Before Indicator","AccessRestriction"=>"Access Restriction","ProviderUserID"=>"Provider Userid","status"=>"Status");
	}
	if($status==0)
	{
		$array=array("type"=>"Type","VSN"=>"VSN","ProviderStudentID"=>"Provider Student ID","StudentFirstName"=>"Current First Name","StudentMiddleName"=>"Current Middle Name","StudentLastName"=>"Current Last Name","StudentDateOfBirth"=>"Current Date Of Birth","StudentGender"=>"Current Gender","EnrolmentDate"=>"Enrolment Date","EnrolledBefore"=>"Enrolled Before Indicator","AccessRestriction"=>"Access Restriction","TimeFraction"=>"Time Fraction","NewFirstName"=>"New First Name","NewMiddleName"=>"New Middle Name","NewLastName"=>"New Last Name","NewDateOfBirth"=>"New Date Of Birth","NewGender"=>"New Gender","NewProviderStudentID"=>"New Provider Student ID","NewEnrolmentDate"=>"New Enrolment Date","NewAccessRestriction"=>"New Access Restriction","ProviderUserID"=>"Provider Userid","LastAttendanceDate"=>"Last Attendance Date","ExitDate"=>"Exit Date","Deceased"=>"Deceased Flag","CancelDate"=>"Cancel Date","ProviderUserID"=>"Provider User ID","s_status"=>"Student Status");	
	}
    return 	$array;
}
function title_by_status_vsr_batch($status)
{
	if($status==4)
	{
	     $array=array("type"=>"Type","VSN"=>"VSN","ProviderStudentID"=>"Provider Student ID","PreviousProviderID"=>"Previous Provider ID","StudentFirstName"=>"First Name","StudentMiddleName"=>"Middle Name","StudentLastName"=>"Last Name","StudentDateOfBirth"=>"Date Of Birth","StudentGender"=>"Gender","EnrolmentDate"=>"Enrolment Date","EnrolledBefore"=>"Enrolled Before Indicator","AccessRestriction"=>"Access Restriction","TimeFraction"=>"Time Fraction","ProviderUserID"=>"Provider Userid","s_status"=>"Student Status");
	}
	if($status==1)
	{
		$array=array("StudentFirstName"=>"Student First Name","StudentLastName"=>"Student Last Name","StudentDateOfBirth"=>"Student Date Of Birth","StudentGender"=>"Student Gender","ExitDate"=>"Exit Date","Deceased"=>"Deceased","ProviderUserID"=>"Provider Student Id","status"=>"Status");	
	}
	if($status==2)
	{
		$array=array("vsn"=>"VSN","StudentFirstName"=>"Student First Name","StudentLastName"=>"Student Last Name","StudentGender"=>"Student Gender","StudentDateOfBirth"=>"Student Date Of Birth","ProviderUserID"=>"Provider User Id");	
	}
	if($status==3)
	{
		$array=array("VSN"=>"VSN","StudentFirstName"=>"Current First Name","StudentLastName"=>"Current Last Name","StudentMiddleName"=>"Current Middle Name","StudentDateOfBirth"=>"Current Date Of Birth","StudentGender"=>"Current Gender","NewFirstName"=>"New First Name","NewLastName"=>"New Last Name	","new_middle_name"=>"New Middle Name","NewDateOfBirth"=>"New Date Of Birth","NewGender"=>"New Gender","status"=>"Status");	
	}
	if($status==0)
	{
		//$array=array("sname"=>"CURRENT FIRST NAME","current_last_name"=>"CURRENT LAST NAME","s_sur_name"=>"CURRENT MIDDLE NAME","dob"=>"CURRENT DATE OF BIRTH","gender"=>"CURRENT GENDER","new_first_name"=>"NEW FIRST NAME","new_last_name"=>"NEW LAST NAME","new_middle_name"=>"NEW MIDDLE NAME","new_bob"=>"NEW DATE OF BIRTH","new_gender"=>"NEW GENDER","batch"=>"BATCH","status"=>"STATUS","date"=>"SUBMITTED DATE");
		//$array=array("StudentFirstName"=>"CURRENT FIRST NAME","StudentLastName"=>"CURRENT LAST NAME","s_sur_name"=>"CURRENT MIDDLE NAME","StudentDateOfBirth"=>"CURRENT DATE OF BIRTH","StudentGender"=>"CURRENT GENDER","NewFirstName"=>"NEW FIRST NAME","NewLastName"=>"NEW LAST NAME","new_middle_name"=>"NEW MIDDLE NAME","NewDateOfBirth"=>"NEW DATE OF BIRTH","NewGender"=>"NEW GENDER","status"=>"STATUS");
		$array=array("VSN"=>"VSN","type"=>"Type","ProviderStudentID"=>"Provider Student ID","StudentFirstName"=>"Current First Name","StudentMiddleName"=>"Current Middle Name","StudentLastName"=>"Current Last Name","StudentDateOfBirth"=>"Current Date Of Birth","StudentGender"=>"Current Gender","EnrolmentDate"=>"Enrolment Date","EnrolledBefore"=>"Enrolled Before Indicator","AccessRestriction"=>"Access Restriction","TimeFraction"=>"Time Fraction","NewFirstName"=>"New First Name","NewLastName"=>"New Last Name","NewMiddleName"=>"New Middle Name","NewDateOfBirth"=>"New Date Of Birth","NewGender"=>"New Gender","NewProviderStudentID"=>"New Provider Student ID","NewEnrolmentDate"=>"New Enrolment Date","NewAccessRestriction"=>"New Access Restriction","ProviderUserID"=>"Provider Userid","LastAttendanceDate"=>"Last Attendance Date","ExitDate"=>"Exit Date","CancelDate"=>"Cancel Date","s_status"=>"Student Status");
	}
    return 	$array;
}
function title_by_status_vsr_Returnbatch($status)
{
	if($status==4)
	{
		//$array=array("sname"=>"FIRST NAME","s_sur_name"=>"MIDDLE NAME","last_name"=>"LAST NAME","dob"=>"DATE OF BIRTH","gender"=>"GENDER","enrol_date"=>"ENROLMENT DATE","existing"=>"EXISTING","new"=>"NEW","vsn"=>"VSN","access_restiction"=>"ACCESS RESTRICTION","access_restiction"=>"ACCESS RESTRICTION","provider_userid"=>"PROVIDER USERID","batch"=>"BATCH","status"=>"STATUS","date"=>"SUBMITTED DATE");
		//$array=array("StudentFirstName"=>"FIRST NAME","s_sur_name"=>"MIDDLE NAME","StudentLastName"=>"LAST NAME","StudentDateOfBirth"=>"DATE OF BIRTH","StudentGender"=>"GENDER","EnrolmentDate"=>"ENROLMENT DATE","EnrolledBefore"=>"VSR ENROLL","vsn"=>"VSN","AccessRestriction"=>"ACCESS RESTRICTION","ProviderUserID"=>"PROVIDER USERID","status"=>"STATUS");
	    $array=array("vsn"=>"VSN","ProviderStudentID"=>"Provider Student ID","StudentFirstName"=>"First Name","StudentLastName"=>"Last Name","StudentMiddleName"=>"Middle Name","StudentDateOfBirth"=>"Date Of Birth","StudentGender"=>"Gender","EnrolmentDate"=>"Enrolment Date","EnrolledBefore"=>"Enrolled Before Indicator","AccessRestriction"=>"Access Restriction","TimeFraction"=>"Time Fraction","ProviderUserID"=>"Provider Userid","PreviousProviderID"=>"Previous Provider ID","status"=>"VSR Status","ExceptionID"=>"Exception ID","ExceptionMessage"=>"Exception Message","s_status"=>"Student Status");
	}
	if($status==1)
	{
		$array=array("VSN"=>"VSN","ProviderStudentID"=>"Provider StudentID","StudentFirstName"=>"Student First Name","StudentLastName"=>"Student Last Name","StudentDateOfBirth"=>"Student Date Of Birth","StudentGender"=>"Student Gender","EnrolmentDate"=>"Enrolment Date","LastAttendanceDate"=>"Last Attendance Date","ExitDate"=>"Exit Date","Deceased"=>"Deceased Flag","ProviderUserID"=>"Provider Student Id","ProviderUserID"=>"Provider Userid","status"=>"VSR Status","ExceptionID"=>"Exception ID","ExceptionMessage"=>"Exception Message","s_status"=>"Student Status");	
	}
	if($status==2)
	{
		//$array=array("sname"=>"STUDENT FIRST NAME","last_name"=>"STUDENT LAST NAME","gender"=>"STUDENT GENDER","dob"=>"STUDENT DATE OF BIRTH","vsn"=>"VSN","provider_userid"=>"PROVIDER USER ID","batch_no"=>"BATCHNO","status"=>"STATUS","date"=>"SUBMITTED DATE");
		//$array=array("StudentFirstName"=>"STUDENT FIRST NAME","StudentLastName"=>"STUDENT LAST NAME","StudentGender"=>"STUDENT GENDER","StudentDateOfBirth"=>"STUDENT DATE OF BIRTH","vsn"=>"VSN","ProviderUserID"=>"PROVIDER USER ID","status"=>"STATUS");
		$array=array("vsn"=>"VSN","ProviderStudentID"=>"Provider Student ID","StudentFirstName"=>"Student First Name","StudentLastName"=>"Student Last Name","StudentMiddleName"=>"Student Middle Name","StudentGender"=>"Student Gender","StudentDateOfBirth"=>"Student Date Of Birth","EnrolmentDate"=>"Enrolment Date","LastAttendanceDate"=>"Last Attendance Date","CancelDate"=>"Cancel Date","ProviderStudentID"=>"Provider Student ID","ProviderUserID"=>"Provider User Id","ExceptionID"=>"Exception ID","ExceptionMessage"=>"Exception Message","s_status"=>"Student Status");	
	}
	if($status==3)
	{
		$array=array("VSN"=>"VSN","ProviderStudentID"=>"Provider Student ID","StudentFirstName"=>"Current First Name","StudentLastName"=>"Current Last Name","StudentMiddletName"=>"Current Middle Name","StudentDateOfBirth"=>"Current Date Of Birth","StudentGender"=>"Current Gender","EnrolmentDate"=>"Current Enrolment Date","NewFirstName"=>"New First Name","NewLastName"=>"New Last Name","NewMiddleName"=>"New Middle Name","NewDateOfBirth"=>"New Date Of Birth","NewGender"=>"New Gender","NewProviderStudentID"=>"New Provider Student ID","NewEnrolmentDate"=>"New Enrolment Date","NewAccessRestriction"=>"New Access Restriction","ProviderUserID"=>"Provider User ID","status"=>"VSR Status","ExceptionID"=>"Exception ID","ExceptionMessage"=>"Exception Message","s_status"=>"Student Status");	
		//$array=array("VSN"=>"VSN","StudentFirstName"=>"Current First Name","StudentLastName"=>"Current Last Name","s_sur_name"=>"Current Middle Name","StudentDateOfBirth"=>"Current Date Of Birth","StudentGender"=>"Current Gender","NewFirstName"=>"New First Name","NewLastName"=>"New Last Name	","new_middle_name"=>"New Middle Name","NewDateOfBirth"=>"New Date Of Birth","NewGender"=>"New Gender","NewProviderStudentID"=>"New Provider Student ID",);	
	}
	if($status==0)
	{
		//$array=array("sname"=>"CURRENT FIRST NAME","current_last_name"=>"CURRENT LAST NAME","s_sur_name"=>"CURRENT MIDDLE NAME","dob"=>"CURRENT DATE OF BIRTH","gender"=>"CURRENT GENDER","new_first_name"=>"NEW FIRST NAME","new_last_name"=>"NEW LAST NAME","new_middle_name"=>"NEW MIDDLE NAME","new_bob"=>"NEW DATE OF BIRTH","new_gender"=>"NEW GENDER","batch"=>"BATCH","status"=>"STATUS","date"=>"SUBMITTED DATE");
		//$array=array("StudentFirstName"=>"CURRENT FIRST NAME","StudentLastName"=>"CURRENT LAST NAME","s_sur_name"=>"CURRENT MIDDLE NAME","StudentDateOfBirth"=>"CURRENT DATE OF BIRTH","StudentGender"=>"CURRENT GENDER","NewFirstName"=>"NEW FIRST NAME","NewLastName"=>"NEW LAST NAME","new_middle_name"=>"NEW MIDDLE NAME","NewDateOfBirth"=>"NEW DATE OF BIRTH","NewGender"=>"NEW GENDER","status"=>"STATUS");
		$array=array("VSN"=>"VSN","type"=>"Type","ProviderStudentID"=>"Provider Student ID","StudentFirstName"=>"Current First Name","StudentMiddleName"=>"Current Middle Name","StudentLastName"=>"Current Last Name","StudentDateOfBirth"=>"Current Date Of Birth","StudentGender"=>"Current Gender","EnrolmentDate"=>"Enrolment Date","EnrolledBefore"=>"Enrolled Before Indicator","AccessRestriction"=>"Access Restriction","TimeFraction"=>"Time Fraction","NewFirstName"=>"New First Name","NewLastName"=>"New Last Name","NewMiddleName"=>"New Middle Name","NewDateOfBirth"=>"New Date Of Birth","NewGender"=>"New Gender","NewProviderStudentID"=>"New Provider Student ID","NewEnrolmentDate"=>"New Enrolment Date","NewAccessRestriction"=>"New Access Restriction","ProviderUserID"=>"Provider Userid","LastAttendanceDate"=>"Last Attendance Date","ExitDate"=>"Exit Date","CancelDate"=>"Cancel Date","SuccessMessage"=>"VSR Status","ExceptionID"=>"Exception ID","ExceptionMessage"=>"Exception Message","s_status"=>"Student Status");
	}
    return 	$array;
}
function data_by_status_vsr($status)
{

        $dbobject = new DB();
        $dbobject->getCon();
		$title_by_status_vsr=$this->title_by_status_vsr($status);		
		$sql="SELECT `status`,`studentid`,`date`,`vsr_status` FROM `vsr_batch_details` ";
	    $sql.=" WHERE `status`='".$status."'";	
		$vsr_report_qry=$dbobject->select($sql);		
        while($row=$dbobject->fetch_array($vsr_report_qry))
		{
			$data[$row['studentid']]['studentid']=$row['studentid'];
			$data[$row['studentid']]['status']=$row['status'];
			$data[$row['studentid']]['date']=$row['date'];
			echo $data[$row['studentid']]['vsr_status']=$row['vsr_status'];
		}
    return 	$data;
					
}
function data_by_status_vsr_entryByid($ids)
{

        $dbobject = new DB();
        $dbobject->getCon();
		$data=array();
		if(!empty($ids)){
		foreach($ids as $i){
		$sql="SELECT `vsr_batch_details`.*,`student`.`vsn` FROM `vsr_batch_details` LEFT JOIN `student` ON  `vsr_batch_details`.`sid`=`student`.`sid`";
	    $sql.=" WHERE ";
			$sql.="`vsr_batch_details`.`id`='".$i."'";
		$vsr_report_qry=$dbobject->select($sql);		
        while($row=$dbobject->fetch_array($vsr_report_qry))
		{
			$data[$row['studentid']]['id']=$row['id'];
			$data[$row['studentid']]['studentid']=$row['studentid'];
			$data[$row['studentid']]['status']=$row['status'];
			$data[$row['studentid']]['VSN']=$row['VSN'];
			$data[$row['studentid']]['date']=$row['date'];
			$data[$row['studentid']]['EnrolledBefore']=$row['EnrolledBefore'];
			$data[$row['studentid']]['vsr_enroll']=$row['vsr_enroll'];
			$data[$row['studentid']]['StudentFirstName']=$row['StudentFirstName'];
			$data[$row['studentid']]['StudentMiddleName']=$row['StudentMiddleName'];
			$data[$row['studentid']]['StudentLastName']=$row['StudentLastName'];
			$data[$row['studentid']]['StudentDateOfBirth']=$row['StudentDateOfBirth'];
			$data[$row['studentid']]['StudentGender']=$row['StudentGender'];
			$data[$row['studentid']]['EnrolmentDate']=$row['EnrolmentDate'];
			$data[$row['studentid']]['TimeFraction']=$row['TimeFraction'];
			$data[$row['studentid']]['AccessRestriction']=$row['AccessRestriction'];
			$data[$row['studentid']]['ProviderUserID']=$row['ProviderUserID'];
			$data[$row['studentid']]['ProviderStudentID']=$row['ProviderStudentID'];
			$data[$row['studentid']]['PreviousProviderID']=$row['PreviousProviderID'];
			$data[$row['studentid']]['NewFirstName']=$row['NewFirstName'];
			$data[$row['studentid']]['NewMiddleName']=$row['NewMiddleName'];
			$data[$row['studentid']]['NewLastName']=$row['NewLastName'];
			$data[$row['studentid']]['NewDateOfBirth']=$row['NewDateOfBirth'];
			$data[$row['studentid']]['NewGender']=$row['NewGender'];
			$data[$row['studentid']]['NewProviderStudentID']=$row['NewProviderStudentID'];
			$data[$row['studentid']]['NewEnrolmentDate']=$row['NewEnrolmentDate'];
			$data[$row['studentid']]['NewAccessRestriction']=$row['NewAccessRestriction'];
			$data[$row['studentid']]['ExitDate']=$row['ExitDate'];
			if($row['LastAttendanceDate']!='0000-00-00'){
				$data[$row['studentid']]['LastAttendanceDate']=date('d-m-Y',strtotime($row['LastAttendanceDate']));
			}
			else{
			$data[$row['studentid']]['LastAttendanceDate']="";	
			}
			$data[$row['studentid']]['Deceased']=$row['Deceased'];
			if($row['status']=="4"){
				$data[$row['studentid']]['type']="Enrol";
			}elseif($row['status']=="3"){
				$data[$row['studentid']]['type']="Change Student Details";
			}elseif($row['status']=="1"){
				$data[$row['studentid']]['type']="Exit";
			}elseif($row['status']=="2"){
				$data[$row['studentid']]['type']="Cancel/Delete";
			}
			
		}
		}
		}
    return 	$data;
					
}
function data_by_status_vsr_entry($status)
{

        $dbobject = new DB();
        $dbobject->getCon();
		$date=date('Y-m-d');
		$title_by_status_vsr=$this->title_by_status_vsr($status);		
		$sql="SELECT `vsr_batch_details`.*,`student`.`vsn`,`student`.`status` as `s_status` FROM `vsr_batch_details` LEFT JOIN `student` ON  `vsr_batch_details`.`sid`=`student`.`sid`";
	    $sql.=" WHERE ";
		if($status!=0){
			$sql.="`vsr_batch_details`.`status`='".$status."' and";
		}
		//if($status==4 || $status=0){
			$sql.="`vsr_batch_details`.`EnrolmentDate`<='".$date."' AND";
	//	}
		$sql.="`entry_status`='0' order by field(`vsr_batch_details`.`status`,'4',3,1,2),`vsr_batch_details`.`id`";
		//echo $sql;
		$vsr_report_qry=$dbobject->select($sql);		
        while($row=$dbobject->fetch_array($vsr_report_qry))
		{
			$data[$row['studentid']]['id']=$row['id'];
			$data[$row['studentid']]['studentid']=$row['studentid'];
			$data[$row['studentid']]['status']=$row['status'];
			$data[$row['studentid']]['VSN']=$row['VSN'];
			$data[$row['studentid']]['date']=$row['date'];
			$data[$row['studentid']]['EnrolledBefore']=$row['EnrolledBefore'];
			$data[$row['studentid']]['vsr_enroll']=$row['vsr_enroll'];
			$data[$row['studentid']]['StudentFirstName']=$row['StudentFirstName'];
			$data[$row['studentid']]['StudentMiddleName']=$row['StudentMiddleName'];
			$data[$row['studentid']]['StudentLastName']=$row['StudentLastName'];
			$data[$row['studentid']]['StudentDateOfBirth']=$row['StudentDateOfBirth'];
			$data[$row['studentid']]['StudentGender']=$row['StudentGender'];
			$data[$row['studentid']]['EnrolmentDate']=$row['EnrolmentDate'];
			$data[$row['studentid']]['TimeFraction']=$row['TimeFraction'];
			$data[$row['studentid']]['AccessRestriction']=$row['AccessRestriction'];
			$data[$row['studentid']]['ProviderUserID']=$row['ProviderUserID'];
			$data[$row['studentid']]['ProviderStudentID']=$row['ProviderStudentID'];
			$data[$row['studentid']]['PreviousProviderID']=$row['PreviousProviderID'];
			$data[$row['studentid']]['NewFirstName']=$row['NewFirstName'];
			$data[$row['studentid']]['NewMiddleName']=$row['NewMiddleName'];
			$data[$row['studentid']]['NewLastName']=$row['NewLastName'];
			$data[$row['studentid']]['NewDateOfBirth']=$row['NewDateOfBirth'];
			$data[$row['studentid']]['NewGender']=$row['NewGender'];
			$data[$row['studentid']]['NewProviderStudentID']=$row['NewProviderStudentID'];
			$data[$row['studentid']]['NewEnrolmentDate']=$row['NewEnrolmentDate'];
			$data[$row['studentid']]['NewAccessRestriction']=$row['NewAccessRestriction'];
			$data[$row['studentid']]['ExitDate']=$row['ExitDate'];
			if($row['CancelDate']!='0000-00-00'){
				$data[$row['studentid']]['CancelDate']=date('d-m-Y',strtotime($row['CancelDate']));
			}else{
				$data[$row['studentid']]['CancelDate']="";
			}
			if($row['LastAttendanceDate']!='0000-00-00'){
				$data[$row['studentid']]['LastAttendanceDate']=date('d-m-Y',strtotime($row['LastAttendanceDate']));
			}
			else{
			$data[$row['studentid']]['LastAttendanceDate']="";	
			}
			$data[$row['studentid']]['Deceased']=$row['Deceased'];
			if($row['status']=="4"){
				$data[$row['studentid']]['type']="Enrol";
				if($row['s_status']=="APPROVED"){
					$data[$row['studentid']]['s_status']="FULL";
				}
				else{
						$data[$row['studentid']]['s_status']=$row['s_status'];
				}
			}elseif($row['status']=="3"){
				$data[$row['studentid']]['type']="Change Student Details";
				$data[$row['studentid']]['s_status']="FULL";
			}elseif($row['status']=="1"){
				$data[$row['studentid']]['type']="Exit";
				$data[$row['studentid']]['s_status']="Exit";
			}elseif($row['status']=="2"){
				$data[$row['studentid']]['type']="Cancel/Delete";
				$data[$row['studentid']]['s_status']="Cancel";
			}
			
		}
    return 	$data;
					
}
	function vsr_status_array_name()
	{
		$seltype2_opn_array=array("0"=>"All","4"=>"Student Enrolment","1"=>"Student Exit","2"=>"Student Cancel/Delete","3"=>"Student Details Change");
		return $seltype2_opn_array;
	}
function data_by_status_vsr_entry_batchid($status,$BatchID)
{

        $dbobject = new DB();
        $dbobject->getCon();
		$title_by_status_vsr=$this->title_by_status_vsr($status);		
		$sql="SELECT `vsr_batch_details`.*,`student`.`status` as `student_status` FROM `vsr_batch_details` INNER JOIN `student` on `vsr_batch_details`.`sid`=`student`.`sid` WHERE";
		if($status!="0"){
			$sql.="`vsr_batch_details`.`status`='".$status."' and";
		}
	    $sql.="  `batchid`='".$BatchID."' and `entry_status`='1'";	
		$vsr_report_qry=$dbobject->select($sql);		
        while($row=$dbobject->fetch_array($vsr_report_qry))
		{
			$student_det=$dbobject->selectall("student",array("sid"=>$row['sid']));
			$data[$row['studentid']]['studentid']=$row['studentid'];
			$data[$row['studentid']]['status']=$row['status'];
			if($row['student_status']=="APPROVED"){
			 $data[$row['studentid']]['s_status']="FULL";
			}
			elseif($row['student_status']=="TRANSFER CERTIFICATE"){
			 $data[$row['studentid']]['s_status']="EXIT";
			}
			else{
			$data[$row['studentid']]['s_status']=$row['student_status'];
			}
			$data[$row['studentid']]['date']=$row['date'];
			$data[$row['studentid']]['VSN']=$row['VSN'];
			$data[$row['studentid']]['EnrolledBefore']=$row['EnrolledBefore'];
			$data[$row['studentid']]['vsr_enroll']=$row['vsr_enroll'];
			$data[$row['studentid']]['StudentFirstName']=$row['StudentFirstName'];
			$data[$row['studentid']]['StudentLastName']=$row['StudentLastName'];
			$data[$row['studentid']]['StudentMiddleName']=$row['StudentMiddleName'];
			$data[$row['studentid']]['StudentDateOfBirth']=$row['StudentDateOfBirth'];
			$data[$row['studentid']]['StudentGender']=$row['StudentGender'];
			if($row['EnrolmentDate']!='0000-00-00'){
				$data[$row['studentid']]['EnrolmentDate']=$row['EnrolmentDate'];
			}
			else{
				$data[$row['studentid']]['EnrolmentDate']="";
			}
			$data[$row['studentid']]['TimeFraction']=$row['TimeFraction'];
			$data[$row['studentid']]['AccessRestriction']=$row['AccessRestriction'];
			$data[$row['studentid']]['ProviderUserID']=$row['ProviderUserID'];
			$data[$row['studentid']]['PreviousProviderID']=$row['PreviousProviderID'];
			$data[$row['studentid']]['ProviderStudentID']=$row['ProviderStudentID'];
			$data[$row['studentid']]['NewFirstName']=$row['NewFirstName'];
			$data[$row['studentid']]['NewLastName']=$row['NewLastName'];
			$data[$row['studentid']]['NewMiddleName']=$row['NewMiddleName'];
			$data[$row['studentid']]['NewDateOfBirth']=$row['NewDateOfBirth'];
			$data[$row['studentid']]['NewGender']=$row['NewGender'];
			$data[$row['studentid']]['ExitDate']=$row['ExitDate'];
			if($row['CancelDate']!='0000-00-00'){
				$data[$row['studentid']]['CancelDate']=date('d-m-Y',strtotime($row['CancelDate']));
			}else{
				$data[$row['studentid']]['CancelDate']="";
			}
			if($row['LastAttendanceDate']!='0000-00-00'){
				$data[$row['studentid']]['LastAttendanceDate']=date('d-m-Y',strtotime($row['LastAttendanceDate']));
			}
			else{
			$data[$row['studentid']]['LastAttendanceDate']="";	
			}
			$data[$row['studentid']]['Deceased']=$row['Deceased'];
			$data[$row['studentid']]['NewProviderStudentID']=$row['NewProviderStudentID'];
			if($row['NewEnrolmentDate']!='0000-00-00'){
			$data[$row['studentid']]['NewEnrolmentDate']=date('d-m-Y',strtotime($row['NewEnrolmentDate']));
			}
			else{
				$data[$row['studentid']]['NewEnrolmentDate']="";
			}
			$data[$row['studentid']]['NewAccessRestriction']=$row['NewAccessRestriction'];
			$data[$row['studentid']]['SuccessMessage']=$row['SuccessMessage'];
			$data[$row['studentid']]['ExceptionID']=$row['ExceptionID'];
			$data[$row['studentid']]['ExceptionMessage']=$row['ExceptionMessage'];
			if($row['status']=="4"){
				$data[$row['studentid']]['type']="Enrol";
			}elseif($row['status']=="3"){
				$data[$row['studentid']]['type']="Change Student Details";
			}elseif($row['status']=="1"){
				$data[$row['studentid']]['type']="Exit";
			}elseif($row['status']=="2"){
				$data[$row['studentid']]['type']="Cancel/Delete";
			}
		}
    return 	$data;
					
}
function data_by_status_vsr_entry_batchidres($status,$BatchID)
{

        $dbobject = new DB();
        $dbobject->getCon();
		$title_by_status_vsr=$this->title_by_status_vsr($status);		
		$sql="SELECT `vsr_batch_details`.*,`student`.`status` as `student_status` FROM `vsr_batch_details` INNER JOIN `student` on `vsr_batch_details`.`sid`=`student`.`sid` WHERE";
		if($status!="0"){
			$sql.="`vsr_batch_details`.`status`='".$status."' and";
		}
	    $sql.="  `batchid`='".$BatchID."' and `entry_status`='1' AND `ExceptionID`!=''";	
		$vsr_report_qry=$dbobject->select($sql);		
        while($row=$dbobject->fetch_array($vsr_report_qry))
		{
			$student_det=$dbobject->selectall("student",array("sid"=>$row['sid']));
			$data[$row['studentid']]['reconciliation']=$row['reconciliation'];
			$data[$row['studentid']]['studentid']=$row['studentid'];
			$data[$row['studentid']]['status']=$row['status'];
			if($row['student_status']=="APPROVED"){
			 $data[$row['studentid']]['s_status']="FULL";
			}
			elseif($row['student_status']=="TRANSFER CERTIFICATE"){
			 $data[$row['studentid']]['s_status']="FULL";
			}
			else{
			$data[$row['studentid']]['s_status']=$row['student_status'];
			}
			$data[$row['studentid']]['date']=$row['date'];
			$data[$row['studentid']]['VSN']=$row['VSN'];
			$data[$row['studentid']]['EnrolledBefore']=$row['EnrolledBefore'];
			$data[$row['studentid']]['vsr_enroll']=$row['vsr_enroll'];
			$data[$row['studentid']]['StudentFirstName']=$row['StudentFirstName'];
			$data[$row['studentid']]['StudentLastName']=$row['StudentLastName'];
			$data[$row['studentid']]['StudentMiddleName']=$row['StudentMiddleName'];
			$data[$row['studentid']]['StudentDateOfBirth']=$row['StudentDateOfBirth'];
			$data[$row['studentid']]['StudentGender']=$row['StudentGender'];
			if($row['EnrolmentDate']!='0000-00-00'){
				$data[$row['studentid']]['EnrolmentDate']=$row['EnrolmentDate'];
			}
			else{
				$data[$row['studentid']]['EnrolmentDate']="";
			}
			$data[$row['studentid']]['TimeFraction']=$row['TimeFraction'];
			$data[$row['studentid']]['AccessRestriction']=$row['AccessRestriction'];
			$data[$row['studentid']]['ProviderUserID']=$row['ProviderUserID'];
			$data[$row['studentid']]['PreviousProviderID']=$row['PreviousProviderID'];
			$data[$row['studentid']]['ProviderStudentID']=$row['ProviderStudentID'];
			$data[$row['studentid']]['NewFirstName']=$row['NewFirstName'];
			$data[$row['studentid']]['NewLastName']=$row['NewLastName'];
			$data[$row['studentid']]['NewMiddleName']=$row['NewMiddleName'];
			$data[$row['studentid']]['NewDateOfBirth']=$row['NewDateOfBirth'];
			$data[$row['studentid']]['NewGender']=$row['NewGender'];
			$data[$row['studentid']]['ExitDate']=$row['ExitDate'];
			if($row['CancelDate']!='0000-00-00'){
				$data[$row['studentid']]['CancelDate']=date('d-m-Y',strtotime($row['CancelDate']));
			}else{
				$data[$row['studentid']]['CancelDate']="";
			}
			if($row['LastAttendanceDate']!='0000-00-00'){
				$data[$row['studentid']]['LastAttendanceDate']=date('d-m-Y',strtotime($row['LastAttendanceDate']));
			}
			else{
			$data[$row['studentid']]['LastAttendanceDate']="";	
			}
			$data[$row['studentid']]['Deceased']=$row['Deceased'];
			$data[$row['studentid']]['NewProviderStudentID']=$row['NewProviderStudentID'];
			if($row['NewEnrolmentDate']!='0000-00-00'){
			$data[$row['studentid']]['NewEnrolmentDate']=date('d-m-Y',strtotime($row['NewEnrolmentDate']));
			}
			else{
				$data[$row['studentid']]['NewEnrolmentDate']="";
			}
			$data[$row['studentid']]['NewAccessRestriction']=$row['NewAccessRestriction'];
			$data[$row['studentid']]['SuccessMessage']=$row['SuccessMessage'];
			$data[$row['studentid']]['ExceptionID']=$row['ExceptionID'];
			$data[$row['studentid']]['ExceptionMessage']=$row['ExceptionMessage'];
			if($row['status']=="4"){
				$data[$row['studentid']]['type']="Enrol";
			}elseif($row['status']=="3"){
				$data[$row['studentid']]['type']="Change Student Details";
			}elseif($row['status']=="1"){
				$data[$row['studentid']]['type']="Exit";
			}elseif($row['status']=="2"){
				$data[$row['studentid']]['type']="Cancel/Delete";
			}
		}
    return 	$data;
					
}
function button_class_status($status)
{
	if($status==0)
	{
		$container[0]='btn btn-info';
	}
    else
	{
									
	    $container[0]='btn btn-outline-secondary';	
	}	
	if($status==4)
	{
		$container[4]='btn btn-info';
	}
    else
	{
									
	    $container[4]='btn btn-outline-secondary';	
	}	
	if($status==1)
	{
		$container[1]='btn btn-info';		
	}
    else
	{
									
	    $container[1]='btn btn-outline-secondary';	
	}	
	if($status==2)
	{
	    $container[2]='btn btn-info';	
	}
    else
	{
									
	    $container[2]='btn btn-outline-secondary';	
	}	
	if($status==3)
	{
		$container[3]='btn btn-info';		
	}	
    else
	{
									
	    $container[3]='btn btn-outline-secondary';	
	}
return 	$container;
}
function GetStudentBySid($sid,$userid){
    $dbobject = new DB();
    $dbobject->getCon();
	$student_det=$dbobject->selectall("student",array("sid"=>$sid));
	$this->Enrol($student_det['vsn'],$student_det['sid'],$student_det['sname'],$student_det['middle_name'],$student_det['s_sur_name'],$student_det['dob'],$student_det['gender'],$student_det['enroll_date'],"",$student_det['studentid'],$student_det['acyear'],$userid);
}
function Enrol($vsn,$sid,$sname,$middle_name,$s_sur_name,$dob,$gender,$enroll_date,$time,$studentid,$acyear,$userid){
    $dbobject = new DB();
    $dbobject->getCon();
    $date=date('Y-m-d');
	$time_fraction="1.0";
	$AccessRestriction='false';
    if($vsn=="" || $vsn=="N"){
        $vsn_enrolBefore='false';
    }
    else{
        $vsn_enrolBefore='true';
    }
    if($time==""){
        $time=date('Y-m-d H:S');
    }

   return $dbobject->exe_qry("INSERT INTO `vsr_batch_details` (`studentid`,`sid`,`status`,`acyear`,`date`,`date_time`,`entry_status`,`vsn`,`EnrolledBefore`,`EnrolmentDate`,`ProviderStudentID`,`StudentFirstName`,`StudentMiddleName`,`StudentLastName`,`StudentDateOfBirth`,`StudentGender`,`TimeFraction`,`AccessRestriction`,`ProviderUserID`)
   VALUES ('".$studentid."','".$sid."','4','".$acyear."','".$date."','".$time."','0','".$vsn."','".$vsn_enrolBefore."','".$enroll_date."','".$studentid."','".$sname."','".$middle_name."','".$s_sur_name."','".$dob."','".$gender."','".$time_fraction."','".$AccessRestriction."','".$userid."')");
                                                    
}
function Edit_details($sid,$studentid,$sname,$secondname,$s_sur_name,$dob,$gender,$vsn,$student_det,$AccessRestriction,$enroll_date,$userid){
	$dbobject = new DB();
    $dbobject->getCon();
	$vsr_batch_details_student_qry=$dbobject->exe_qry("SELECT * FROM `vsr_batch_details` WHERE `sid`='".$sid."' and `entry_status`='0' and `batchid`=''");
	$chk_vsr=$dbobject->mysql_num_row($vsr_batch_details_student_qry);
	if($chk_vsr==0)
	{
		$vsr_ins_sql="INSERT INTO `vsr_batch_details`
		(`vsn`,`studentid`,`sid`,`name`, `status`, `acyear`, `date`,`EnrolledBefore`, `StudentFirstName`,`StudentMiddleName`,`StudentLastName`, `StudentDateOfBirth`, `StudentGender`, `EnrolmentDate`,`AccessRestriction`, `ProviderUserID`, `ProviderStudentID`, `NewFirstName`,`NewMiddleName`, `NewLastName`, `NewDateOfBirth`, `NewGender`,`NewAccessRestriction`,`NewProviderStudentID`,`NewEnrolmentDate`)
		 VALUES ('".$vsn."','".$studentid."','".$sid."','".addslashes($student_det['sname'])."','3','".$student_det['acyear']."','".date('Y-m-d')."','".$student_det['vsn_enrolled']."','".addslashes($student_det['sname'])."','".addslashes($student_det['SECOND_NAME'])."','".addslashes($student_det['s_sur_name'])."','".$student_det['dob']."','".$student_det['gender']."','".$student_det['enroll_date']."','".$student_det['AccessRestriction']."','".$userid."','".$student_det['studentid']."','".$sname."','".$secondname."','".$s_sur_name."','".$dob."','".$gender."','".$AccessRestriction."','".$studentid."','".$enroll_date."')";
	   $vsr_ins=$dbobject->exe_qry($vsr_ins_sql);
	}else{
		$vsr_batch_details_student_row=$dbobject->fetch_array($vsr_batch_details_student_qry);
		if($vsr_batch_details_student_row['status']=="4"){
			$vsr_upt_sql="UPDATE `vsr_batch_details` SET `studentid`='".$studentid."',`name`='".$sname."',`StudentFirstName`='".$sname."', `StudentLastName`='".$s_sur_name."',
			`StudentDateOfBirth`='".$dob."', `StudentGender`='".$gender."',`AccessRestriction`='".$AccessRestriction."',`EnrolmentDate`='".$enroll_date."' WHERE `id`='".$vsr_batch_details_student_row['id']."'";
		   $vsr_upt=$dbobject->exe_qry($vsr_upt_sql);
		}
		else{
			$vsr_upt_sql="UPDATE `vsr_batch_details` SET `NewFirstName`='".$sname."', `NewLastName`='".$s_sur_name."',
			`NewDateOfBirth`='".$dob."', `NewGender`='".$gender."',`NewAccessRestriction`='".$AccessRestriction."',`NewProviderStudentID`='".$studentid."',`NewEnrolmentDate`='".$enroll_date."' WHERE `id`='".$vsr_batch_details_student_row['id']."'";
		   $vsr_upt=$dbobject->exe_qry($vsr_upt_sql);
		}
	}
}
function ExitStudent($studentid,$sid,$student_det,$dot,$Deceased,$userid){
	$dbobject = new DB();
    $dbobject->getCon();
	$vsr_ins_sql="INSERT INTO `vsr_batch_details`
	(`vsn`,`studentid`,`sid`,`name`, `status`, `acyear`, `date`,`EnrolledBefore`, `StudentFirstName`,`StudentMiddleName`, `StudentLastName`, `StudentDateOfBirth`, `StudentGender`, `EnrolmentDate`,`AccessRestriction`, `ProviderUserID`, `ProviderStudentID`,`ExitDate`,`Deceased`)
	 VALUES ('".$student_det['vsn']."','".$studentid."','".$sid."','".$student_det['sname']."','1','".$student_det['acyear']."','".date('Y-m-d')."','".$student_det['vsn_enrolled']."','".$student_det['sname']."','".$student_det['SECOND_NAME']."','".$student_det['s_sur_name']."','".$student_det['dob']."','".$student_det['gender']."','".$student_det['enroll_date']."','".$student_det['AccessRestriction']."','".$userid."','".$student_det['studentid']."','".$dot."','".$Deceased."')";
	$vsr_ins=$dbobject->exe_qry($vsr_ins_sql);	
}
function CancelStudent($studentid,$sid,$student_det,$doc,$userid){
	$dbobject = new DB();
    $dbobject->getCon();
	$vsr_ins_sql="INSERT INTO `vsr_batch_details`
	(`vsn`,`studentid`,`sid`,`name`, `status`, `acyear`, `date`,`EnrolledBefore`, `StudentFirstName`,`StudentMiddleName`, `StudentLastName`, `StudentDateOfBirth`, `StudentGender`, `EnrolmentDate`,`AccessRestriction`, `ProviderUserID`, `ProviderStudentID`,`CancelDate`,`Deceased`)
	 VALUES ('".$student_det['vsn']."','".$studentid."','".$sid."','".$student_det['sname']."','2','".$student_det['acyear']."','".date('Y-m-d')."','".$student_det['vsn_enrolled']."','".$student_det['sname']."','".$student_det['SECOND_NAME']."','".$student_det['s_sur_name']."','".$student_det['dob']."','".$student_det['gender']."','".$student_det['enroll_date']."','".$student_det['AccessRestriction']."','".$userid."','".$student_det['studentid']."','".$doc."','".$Deceased."')";
	$vsr_ins=$dbobject->exe_qry($vsr_ins_sql);	
}
function Get_nextBatch(){
	$dbobject = new DB();
    $dbobject->getCon();
	$account=$this->AccountDetails();
	$sel=$dbobject->select("SELECT `BatchID` FROM `vsr_batch` ORDER BY `id` DESC");
	$row=$dbobject->fetch_array($sel);
	if(!empty($row)){
		$barray=explode("-",$row['BatchID']);
		$bno=$barray[1]+1;
		//return $account['ProviderID']."-".$bno;
	}
	else{
		$bno=1;
		//return $account['ProviderID']."-1";
	}
	$upd=$dbobject->exe_qry("UPDATE `vsr_settings` set `next_batch`='".$bno."'");
	return true;
	
}

}
?>