<?php
class Cellcast
{
function SMSurl()
{
    $url="https://www.cellcast.com.au/api/v3/";
    return $url;
}
function auth(){
    $auth="CELLCAST18916fedc269661b585e60672e40db9d";
    return $auth;
}
function httpUrl(){
    $url=$this->SMSurl()."send-sms";
    return $url;
}
function Senderid(){
    $dbobj = new DB();
    $dbobj->getCon();
    $gateway_det=$dbobj->selectall("sms_gateway",array("sms_gateway"=>"cellcast"));
    return $gateway_det['sender_id'];
}
function Send_smsInd($num,$msg,$type,$sms_tocken,$sms_date)
{
    $dbobj = new DB();
    $dbobj->getCon();
    if(!empty($num))
    {
        $status="";
        $url=$this->httpUrl($num['num'],$msg);	
        $c = new curl($url);
        $c->setopt(CURLOPT_FOLLOWLOCATION, true);
        $re = $c->exec();
        $res_res=json_decode($re);
        if($res_res->messages[0]->accepted=="1"){
            $status="Submit";
        }
        $ins=$dbobj->exe_qry("insert into `sms` (`user`,`mob_no`,`message`,`sms_date`,`report`,`type`,`delivery_report`,`sms_tocken`) values ('".$num['userid']."','".$num['num']."','".addslashes($msg)."','".$sms_date."','".$res_res->messages[0]->apiMessageId."','".$type."','Submit','".$sms_tocken."')");
        return $num['num']."-Sent Successfully";
    }
    else{
    return "Invalid Number";
    }
    
}
function Send_smsArray($num,$msg,$type,$sms_tocken,$sms_date)
{
    $dbobj = new DB();
    $dbobj->getCon();
    $sender_id=$this->Senderid();
    $retun_data=array();
    $header = array(
        "Content-Type: application/json", // example header
        "APPKEY: ".$this->auth()."" // example header
    );

    $body = array(
        "sms_text" => $msg,
        "numbers" => array()
    );
    $i=0;
    if(!empty($num)){
     foreach($num as $n){
        $body['numbers'][$i]=$this->FormatNumber($n['num']);
        $i++;
     }
    }
    if($sender_id!=""){
        $body['from']=$sender_id;
    }

   $json_body = json_encode($body);
    if(!empty($num))
    {
        $status="";
        $url=$this->httpUrl();	
        $c = new curl($url);
        $c->setopt(CURLOPT_FOLLOWLOCATION, true);
        $c->setopt(CURLOPT_HTTPHEADER, $header);
        $c->setopt(CURLOPT_POSTFIELDS, $json_body);
        $re = $c->exec();
        $res_res=json_decode($re, true);
        if($res_res['meta']['code']=="200"){
            if($res_res['meta']['status']=="SUCCESS"){
                $retun_data['status']="Success";
                $data=$res_res['data']['messages'];
                if(!empty($num)){
                    foreach($num as $n){
                    $valueToSearch = $n['num'];
                    $founded=$this->searchValueInArray($data,$valueToSearch);
                    if(!empty($founded)){
                        $ins=$dbobj->exe_qry("insert into `sms` (`user`,`mob_no`,`message`,`sms_date`,`report`,`type`,`delivery_report`,`sms_tocken`) values ('".$n['userid']."','".$n['num']."','".addslashes($msg)."','".$sms_date."','".$founded['message_id']."','".$type."','Submit','".$sms_tocken."')");
                        $retun_data['data'].=$n['num']."-Sent Successfully </br>";
                    }else{
                        $retun_data['data'].=$n['num']."-Invalid Number </br>"; 
                    }
                    }
                }
            }
           
        }else{
            $retun_data['status']="Error";
            $retun_data['data']="Invalid Numbers";
        }
        return $retun_data;
    }
    else{
        $retun_data['status']="Error";
        $retun_data['data']="Invalid Numbers";
        return $retun_data;
    }
    
}
function searchValueInArray($array, $value) {
    $found = array();
    foreach ($array as $item) {
        if ($item['to'] === $value) {
            $found = $item;
            return $found;
            break;
        }
    }
}
function FormatNumber($num){
    $num = str_replace("+", "", $num);
    return $num;
}
}