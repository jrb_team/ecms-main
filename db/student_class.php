<?php
require_once('../db/createdb.php');
class Student
{
	function NewStudentID($params){
		$dbobj = new DB();
		$dbobj->getCon();
        $sel_studentCount=$dbobj->select("SELECT MAX(`studentid`) as `studentid` FROM `student`  LIMIT 1");
        $studentid_row=$dbobj->fetch_array($sel_studentCount);
        $letters = preg_replace('/[^a-zA-Z]/', '', $studentid_row['studentid']);
        $numbers = preg_replace('/[^0-9]/', '', $studentid_row['studentid']);                                                              
        $nex_number=$numbers+1;
        return $letters.$nex_number;
    }
    function NewFamilyID($params){
		$dbobj = new DB();
		$dbobj->getCon();
		$letters=strtoupper(substr($params['f_surname'], 0, 3));
		$numbers = str_replace(' ', '', $params['f_phone']);
		$numbers=substr($numbers, -3);                                                             
        return $letters.$numbers;
    }
    function enrolStudent($enrolid,$studentid,$familyCode,$parent_data,$student_data,$family_info){
		$dbobj = new DB();
		$dbobj->getCon();
		$sel_parent=$dbobj->select("SELECT * FROM `parent` WHERE `family_code`='".$familyCode."'");
		$num_rows=$dbobj->mysql_num_row($sel_parent);
			if($num_rows=="0"){
			$dbobj->exe_qry("insert into  `parent` (`family_code`,`studentid`,`f_given_name`,`f_sur_name`,`f_r_status`,`fmob`,`f_work_num`,`femail`,`HM_ADDRESS02`,`HM_STATE`,`HM_POSTCODE`) 
			values ('".$familyCode."','".$studentid."','".$parent_data['f_given_name']."','".$parent_data['f_sur_name']."','".$parent_data['f_r_status']."','".$parent_data['fmob']."','".$parent_data['f_work_num']."','".$parent_data['femail']."','".$parent_data['HM_ADDRESS02']."','".$parent_data['HM_STATE']."','".$parent_data['HM_POSTCODE']."')");
			$parent_id=$dbobj->mysql_insert_id();
			}else{
				$parent_det=$dbobj->selectall("parent",array("family_code"=>$familyCode));
				$parent_id=$parent_det['id'];
				$dbobj->exe_qry("UPDATE `parent` SET `f_r_status`='".$parent_data['f_r_status']."',`fmob`='".$parent_data['fmob']."',`f_work_num`='".$parent_data['f_work_num']."',`femail`='".$parent_data['femail']."',`HM_ADDRESS02`='".$parent_data['HM_ADDRESS02']."',`HM_STATE`='".$parent_data['HM_STATE']."',`HM_POSTCODE`='".$parent_data['HM_POSTCODE']."' WHERE `id`='".$parent_id."'");
				
			}
		
		$sel_family=$dbobj->select("SELECT * FROM `famliy_info` WHERE `parent_id`='".$parent_id."'");
		$fam_num=$dbobj->mysql_num_row($sel_family);
		if($fam_num=='0'){
			$dbobj->exe_qry("INSERT INTO `famliy_info` (`emrgy1_SURNAME`,`emrgy_contact1`,`emrgy1_DESCRIPTION`,`parent_id`) VALUES ('".$family_info['emrgy1_SURNAME']."','".$family_info['emrgy_contact1_mob']."','".$family_info['emrgy1_DESCRIPTION']."','".$parent_id."')");
		}else{
			$dbobj->exe_qry("UPDATE `famliy_info` SET `emrgy1_SURNAME`='".$family_info['emrgy1_SURNAME']."',`emrgy_contact1`='".$family_info['emrgy_contact1_mob']."',`emrgy1_DESCRIPTION`='".$family_info['emrgy1_DESCRIPTION']."' WHERE `parent_id`='".$parent_id."'");
		}
		$dbobj->exe_qry("insert into `student` (`studentid`,`parent_id`,`sname`,`dob`,`gender`,`acyear`,`status`,`s_sur_name`,`family_code`,`SECOND_NAME`,`s_status`,`admission_date`,`classid`,`feeppaymentmode`)
	  					 values ('".$studentid."','".$parent_id."','".$student_data['sname']."','".$student_data['dob']."','".$student_data['gender']."','".$student_data['acyear']."','APPROVED','".$student_data['s_sur_name']."','".$familyCode."','".$student_data['SECOND_NAME']."','active','".$student_data['admission_date']."','".$student_data['classid']."','other')");
		$sid=$dbobj->mysql_insert_id();
	   $dbobj->exe_qry("INSERT INTO `student_class`(`ac_year`, `sid`, `classid`, `acyear`, `student_type`) VALUES ('".$student_data['acyear']."','".$sid."','".$student_data['classid']."','".$student_data['acyear']."','other')");
	   $dbobj->exe_qry("UPDATE `student_enroll` SET `status`='5' WHERE `sid`='".$enrolid."'");
	   return true;
	}
	function Student_details($studentid)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$student=$dbobj->selectall_assoc("student",array("studentid"=>$studentid));
		$class_det=$dbobj->selectall("sclass",array("classid"=>$student['classid']));
		$acyear=$student['acyear'];
		$house_det=$dbobj->selectall("student_house",array("sid"=>$student['sid'],"acyear"=>$acyear));
		$student['house']=$house_det['house'];
		$eca_det=$dbobj->selectall("eca_stud",array("sid"=>$student['sid'],"acyear"=>$acyear));
		$student['eca']=$eca_det['eca'];
		$club_det=$dbobj->selectall("student_club",array("sid"=>$student['sid'],"acyear"=>$acyear));
		$student['parent_id']=$student['parent_id'];
		$student_profile_Section=$dbobj->student_profile_Section($class_det['classno']);
		if($student_profile_Section=="1"){
			$student['section']="Primary";
		}
		elseif($student_profile_Section=="2"){
			$student['section']="Middle_School";
		}
		elseif($student_profile_Section=="3"){
			$student['section']="Senior";
		}
		elseif($student_profile_Section=="4"){
			$student['section']="VCE";
		}
		$student['club']=$eca_det['club'];
		$student['classname']=$class_det['classname'];
		$student['division']=$class_det['division'];
		$student['sid']=$student['sid'];
		$student['parent_id']=$student['parent_id'];
		$student['studentid']=$student['studentid'];
		$student['simgid']=$student['simgid'];
		$student['sname']=$student['sname'];
		$student['gender']=$student['gender'];
		$student['classname']=$student['classname'];
		$student['division']=$student['division'];
		$student['doj']=$student['doj'];
		$student['placeofbirth']=$student['placeofbirth'];
		$student['mother_tongue']=$student['mother_tongue'];
		$student['religion']=$student['religion'];
		$student['stype']=$student['stype'];
		$student['dob']=$student['dob'];
		$student['paddr1']=$student['paddr1'];
		$student['group']=$student['group'];
		$student['classid']=$student['classid'];
		$student['cat']=$student['cat'];
		$student['adhar_no']=$student['adhar_no'];
		return $student;
	}
	function Parent_details($studentid)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$parent=$dbobj->selectall_assoc("parent",array("studentid"=>$studentid));
		return $parent;
	}
	function Parent_detailsByid($id)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$parent=$dbobj->selectall_assoc("parent",array("id"=>$id));
		return $parent;
	}
	function Document_details($sid)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$sel=$dbobj->select("select * from `document_det` where `sid`='".$sid."'");
		$i=0;
		while($row=$dbobj->fetch_array_assoc($sel))
		{
			$document[$i]['did']=$row['did'];
			$document[$i]['dname']=$row['dname'];
			$document[$i]['dremark']=$row['dremark'];
			$i++;
		}
		return $document;
	}
	function More_info($sid)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$info=$dbobj->selectall_assoc("more_info",array("sid"=>$sid));
		return $info;		
	}
	function Family_info($parent_id){
	    $dbobj = new DB();
		$dbobj->getCon();
		$i=0;
		$info=array();
		$sel=$dbobj->select("select `id`, `parent_id`, `emrgy1_SURNAME`,`emrgy_contact1`,`emrgy1_DESCRIPTION`,`emrgy1_HOME_PHONE`,`emrgy_contact1_mob`,`emrgy_contact1_wrk`,`emrgy2_SURNAME`,`emrgy_contact2`,`emrgy2_DESCRIPTION`,`emrgy2_HOME_PHONE`,`emrgy_contact2_mob`    from `famliy_info` where `parent_id`='".$parent_id."'");
		while($row=$dbobj->fetch_array($sel)){
		    $info[$i]=$row;
		}
	   return $info;	
	}
	function Family_ChurchInfo($parent_id){
	    $dbobj = new DB();
		$dbobj->getCon();
		$i=0;
		$info=array();
	    $sel=$dbobj->select("select `id`, `parent_id`, `ch_name`,`ch_pastor_name`,`ch_address`,`ch_subrub`,`ch_contact`,`ch_STATE`,`ch_POST` from `famliy_info` where `parent_id`='".$parent_id."'");
	   	while($row=$dbobj->fetch_array($sel)){
		    $info[$i]=$row;
		}
	   return $info;
	}
	function Family_MedicalInfo($parent_id){
	    $dbobj = new DB();
		$dbobj->getCon();
		$i=0;
		$info=array();
	    $sel=$dbobj->select("select `id`, `parent_id`, `med_cen_name`,`med_cen_HOUSE_NO`,`med_cen_TITLE`,`med_cen_addrs`,`med_cen_contact`,`med_cen_addrs2`,`med_cen_SUBURB`,`med_cen_post`,`med_cen_num`,`med_cen_STATE`,`med_cen_ab_cvr_num` from `famliy_info` where `parent_id`='".$parent_id."'");
	   	while($row=$dbobj->fetch_array($sel)){
		    $info[$i]=$row;
		}
	   return $info;
	}
	function Passport_details($sid)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$passport_details=$dbobj->selectall_assoc("passport_details",array("sid"=>$sid));
		return $passport_details;	
	}
	function health_details($student_data_array)
	{

		$health_details['height']=$student_data_array['height'];
		$health_details['weight']=$student_data_array['weight'];
		$health_details['bloodgroup']=$student_data_array['bloodgroup'];
		$health_details['bmi']=$student_data_array['bmi'];
		$health_details['allergies']=$student_data_array['allergies'];
		$health_details['family_doctor']=$student_data_array['family_doctor'];
		$health_details['other_disease']=$student_data_array['other_disease'];
		return $health_details;	
	}	
	function Student_place($sid,$acyear)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$student_det=$dbobject->selectall("student",array("sid"=>$sid));
		return $student_det['place'];
	}
	function Student_Classid($sid,$acyear)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$student_det=$dbobject->selectall("student",array("sid"=>$sid));
		return $student_det['classid'];
	}
	function Student_photo($studentid)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$student_det=$dbobject->selectall("student",array("studentid"=>$studentid));
		$stud_photoid=$student_det['simgid'];
		if($stud_photoid!="0")
					{
					$sel_photo=$dbobject->select("select * from `student_img` where `id`='".$stud_photoid."'");
					$photo_row=$dbobject->fetch_array($sel_photo);
					$imagebytes=$photo_row['image'];
					$type=$photo_row['type'];
					if($type=='')
					{
						$src="http://www.ecms-erp.com/ecms/img/people-img1.png";
					}
					else
					{
						//$src="'data:image/jpeg;base64,".base64_encode($imagebytes)."'";
						//$src="'www.donboscoschoolputhuppally.org/ecms/student_photo/".$studentid.".jpg'";
						$src="http://www.ecms-erp.com/ecms/img/people-img1.png";
					}
					}
					else
					{
						$src="http://www.ecms-erp.com/ecms/img/people-img1.png";
					}
		return $src;
		
	}
	function Student_photo_src($stud_photoid,$studentid)
	{
		$dbobject = new DB();
		$dbobject->getCon();
	    if($stud_photoid!="0")
		{
		$sel_photo=$dbobject->select("select * from `student_img` where `id`='".$stud_photoid."'");
		$photo_row=$dbobject->fetch_array($sel_photo);
		$imagebytes=$photo_row['image'];
		$type=$photo_row['type'];
		if($type=='')
		{
											//$image[$student_sid]="<img  border='0' src='data:image/jpg;base64,".base64_encode($imagebytes)."' width='96' height='96' alt='Photo' />";
											$src="../student_photo/".$studentid.".jpg";
		}
		else
		{
			$fh=fopen("../imagetmp/tmp".$type."","w");
			fwrite($fh,$imagebytes,strlen($imagebytes));
											//$image[$student_sid]="<img  border='0' src='data:image/jpg;base64,".base64_encode($imagebytes)."' width='96' height='96' alt='Photo' />";
											$src="../student_photo/".$studentid.".jpg";
		}
		}
		else
		{
			//$image[$student_sid]="<img  border='0' src='../imagetmp/default.jpg' width='96' height='96' alt='Photo' />";
			$src= "../imagetmp/default.jpg";
		}
		return $src;
		
	}	
	function Student_report($acyear,$classno,$classid)
	{
			$dbobject = new DB();
			$dbobject->getCon();
			$qry="select * from `student` LEFT JOIN `student_class`
			 ON `student`.`sid`=`student_class`.`sid` ";
			 $qry.=" RIGHT OUTER JOIN `sclass` ON `student`.`classid`=`sclass`.`classid` where  `student`.`sid`!='' ";
			if($classno!="-1")
			{
			$qry.="and `sclass`.`classno`='".$classno."'";
			}
			if($classid!="-1")
			{
				$qry.=" and `student_class`.`classid`='".$classid."' ";
			}
			$qry.="and  `student_class`.`acyear`='".$acyear."'";
			$sel_student=$dbobject->select($qry);
			$i=0;
			while($row=$dbobject->fetch_array($sel_student))
			{
				$stud_photoid=0;
				$data[$i]['studentid']=$row['studentid'];
				$data[$i]['parent_id']=$row['parent_id'];
				$data[$i]['sname']=$row['sname'];
				$data[$i]['simgid']=$row['simgid'];
				$data[$i]['bloodgroup']=$row['bloodgroup'];
				$class_det=$dbobject->selectall('sclass',array("classid"=>$row['classid']));
				$data[$i]['classname']=$class_det['classname'];
				$data[$i]['division']=$class_det['division'];
				if($row['parent_id']==0)
				{
					$parent_det=$dbobject->selectall("parent",array("studentid"=>$row['studentid']));
				}
				else{
					$parent_det=$dbobject->selectall("parent",array("id"=>$row['parent_id']));
				}
				$data[$i]['fname']=$parent_det['fname'];
				$data[$i]['fmob']=$parent_det['fmob'];
				$data[$i]['femail']=$parent_det['femail'];
				$data[$i]['mname']=$parent_det['mname'];
				$data[$i]['mmob']=$parent_det['mmob'];
				$data[$i]['memail']=$parent_det['memail'];
				$data[$i]['memail']=$parent_det['memail'];
				$data[$i]['paddr1']=$parent_det['paddr1'];
				$data[$i]['sms_number']=$parent_det['sms_number'];
				$data[$i]['default_email']=$parent_det['default_email'];
				$i++;	
			}
			return $data;
	}
	function Get_StudentByAcyear($acyear)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$sql="select * from `student` LEFT JOIN `sclass` on `student`.`classid`=`sclass`.`classid` where `acyear`='".$acyear."' and `student`.`status`='APPROVED'";
		$str=$dbobject->Checknum($sql);
		if($str>0)
		{
			$i=0;
			$sel=$dbobject->select($sql);
			while($row=$dbobject->fetch_array($sel))
			{
				$student[$i]['sid']=$row['sid'];
				$student[$i]['parent_id']=$row['parent_id'];
				$student[$i]['studentid']=$row['studentid'];
				$student[$i]['simgid']=$row['simgid'];
				$student[$i]['sname']=$row['sname'];
				$student[$i]['classname']=$row['classname'];
				$student[$i]['division']=$row['division'];
				$i++;
			}
		}
			return $student;
	}
    function StudentAdmissionClass($sid){
        $dbobject = new DB();
		$dbobject->getCon();
        $sel=$dbobject->select("SELECT * FROM student_class LEFT JOIN `sclass` on `student_class`.`classid`=`sclass`.`classid` WHERE `sid` = '".$sid."' ORDER BY `student_class`.`id` ASC LIMIT 1");
        $row=$dbobject->fetch_array($sel);
        return $row;
    }
	function getAllActiveFamily_code(){
        $dbobject = new DB();
		$dbobject->getCon();
        $sel=$dbobject->select("SELECT * FROM `parent`");
		$data=array();
		$i=0;
        while($row=$dbobject->fetch_array($sel)){
			$data[$i]['parent_id']=$row['id'];
			$data[$i]['family_code']=$row['family_code'];
			$data[$i]['f_sur_name']=$row['f_sur_name'];
			$data[$i]['f_given_name']=$row['f_given_name'];
			$data[$i]['m_sur_name']=$row['m_sur_name'];
			$data[$i]['m_given_name']=$row['m_given_name'];
			$i++;
		}
        return $data;
	}
	function GetStudentGovt($section,$acyear){
		$dbobject = new DB();
		$dbobject->getCon();
		$data=array();
		$i=0;
	
		if($section=="Primary"){
			$section_det=$dbobject->selectall("sclass_section",array("section"=>"Primary","acyear"=>$acyear));
			$sql="SELECT `sclass`.`classname`,`sclass`.`division`,`student`.`sid`,`student`.`s_sur_name`,`student`.`sname`,`student`.`family_code` FROM student_class LEFT JOIN `sclass` on `student_class`.`classid`=`sclass`.`classid` LEFT JOIN `student` ON `student_class`.`sid`=`student`.`sid` LEFT JOIN `famliy_info` on `student`.`parent_id`=`famliy_info`.`parent_id` WHERE `sclass`.`sectionid`='".$section_det['id']."' AND `famliy_info`.`hcardok`='health_care'";
			$sel=$dbobject->select($sql);
			while($row=$dbobject->fetch_array($sel)){
				$data[$i]['classname']=$row['classname'];
				$data[$i]['family_code']=$row['family_code'];
				$data[$i]['division']=$row['division'];
				$data[$i]['sid']=$row['sid'];
				$data[$i]['s_sur_name']=$row['s_sur_name'];
				$data[$i]['sname']=$row['sname'];
				$i++;
			}
		}
		if($section=="Middle-School"){
			$section_det=$dbobject->selectall("sclass_section",array("section"=>"Middle-School","acyear"=>$acyear));
			$sql="SELECT `sclass`.`classname`,`sclass`.`division`,`student`.`sid`,`student`.`s_sur_name`,`student`.`sname`,`student`.`family_code` FROM student_class LEFT JOIN `sclass` on `student_class`.`classid`=`sclass`.`classid` LEFT JOIN `student` ON `student_class`.`sid`=`student`.`sid` LEFT JOIN `famliy_info` on `student`.`parent_id`=`famliy_info`.`parent_id` WHERE `sclass`.`sectionid`='".$section_det['id']."' AND `famliy_info`.`hcardok`='health_care'";
			$sel=$dbobject->select($sql);
			while($row=$dbobject->fetch_array($sel)){
				$data[$i]['classname']=$row['classname'];
				$data[$i]['family_code']=$row['family_code'];
				$data[$i]['division']=$row['division'];
				$data[$i]['sid']=$row['sid'];
				$data[$i]['s_sur_name']=$row['s_sur_name'];
				$data[$i]['sname']=$row['sname'];
				$i++;
			}
		}
		if($section=="Senior School"){
			$section_det=$dbobject->selectall("sclass_section",array("section"=>"Senior School","acyear"=>$acyear));
			$sql="SELECT `sclass`.`classname`,`sclass`.`division`,`student`.`sid`,`student`.`s_sur_name`,`student`.`sname`,`student`.`family_code` FROM student_class LEFT JOIN `sclass` on `student_class`.`classid`=`sclass`.`classid` LEFT JOIN `student` ON `student_class`.`sid`=`student`.`sid` LEFT JOIN `famliy_info` on `student`.`parent_id`=`famliy_info`.`parent_id` WHERE `sclass`.`sectionid`='".$section_det['id']."' AND `famliy_info`.`hcardok`='health_care'";
			$sel=$dbobject->select($sql);
			while($row=$dbobject->fetch_array($sel)){
				$data[$i]['classname']=$row['classname'];
				$data[$i]['family_code']=$row['family_code'];
				$data[$i]['division']=$row['division'];
				$data[$i]['sid']=$row['sid'];
				$data[$i]['s_sur_name']=$row['s_sur_name'];
				$data[$i]['sname']=$row['sname'];
				$i++;
			}
		}
		if($section=="VCE"){
			$section_det=$dbobject->selectall("sclass_section",array("section"=>"VCE","acyear"=>$acyear));
			$sql="SELECT `sclass`.`classname`,`sclass`.`division`,`student`.`sid`,`student`.`s_sur_name`,`student`.`sname`,`student`.`family_code` FROM student_class LEFT JOIN `sclass` on `student_class`.`classid`=`sclass`.`classid` LEFT JOIN `student` ON `student_class`.`sid`=`student`.`sid` LEFT JOIN `famliy_info` on `student`.`parent_id`=`famliy_info`.`parent_id` WHERE `sclass`.`sectionid`='".$section_det['id']."' AND `famliy_info`.`hcardok`='health_care'";
			$sel=$dbobject->select($sql);
			while($row=$dbobject->fetch_array($sel)){
				$data[$i]['classname']=$row['classname'];
				$data[$i]['family_code']=$row['family_code'];
				$data[$i]['division']=$row['division'];
				$data[$i]['sid']=$row['sid'];
				$data[$i]['s_sur_name']=$row['s_sur_name'];
				$data[$i]['sname']=$row['sname'];
				$i++;
			}
		}
		return $data;

	}
function govt_supportCount($acyear){

	$dbobject = new DB();
	$dbobject->getCon();
	$sql="SELECT `famliy_info`.`parent_id`,`family_code` FROM `famliy_info`  LEFT JOIN `student` ON `student`.`parent_id`=`famliy_info`.`parent_id`  WHERE  `famliy_info`.`hcardok`='health_care' and `student`.`acyear`='".$acyear."'";
	$sel=$dbobject->select($sql);
	$num_rows=$dbobject->mysql_num_row($sel);
	return $num_rows;
}
function Get_SidByAcyear($acyear){
	$dbobject = new DB();
	$dbobject->getCon();
	$data=array();
	$sql="select `student_class`.`sid` from `student_class` LEFT JOIN `student` on `student`.`sid`=`student_class`.`sid` where `student_class`.`acyear`='".$acyear."' and `status`='APPROVED'";
		$i=0;
		$sel=$dbobject->select($sql);
		while($row=$dbobject->fetch_array($sel))
		{
			$data[$i]=$row['sid'];
			$i++;
		}
		return $data;
}
}
?>
