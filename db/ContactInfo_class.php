<?php
include_once('../db/createdb.php');
class ContactClass
{
    function getStudent(){
        $dbobject = new DB();
        $dbobject->getCon();
        $sql="SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='student';";
        $rs=$dbobject->select($sql);
        $i=0;
        $data['primary_info']=array();
        $data['student_info']=array();
        while($row=$dbobject->fetch_array($rs)){
            if($row['COLUMN_NAME']=='studentid'){
                $data['primary_info']['Student ID']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='vsn'){
                $data['primary_info']['VSN']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='s_sur_name'){
                $data['primary_info']['Surname']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='sname'){
                $data['primary_info']['Given Name']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='SECOND_NAME'){
                $data['primary_info']['Second Name']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='PREF_NAME'){
                $data['primary_info']['Pref Name']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='dob'){
                $data['primary_info']['Date Of Birth']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='gender'){
                $data['primary_info']['Gender']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='reg_date'){
                $data['primary_info']['Application Date']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='classid'){
                $data['primary_info']['Class']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='admission_date'){
                $data['primary_info']['Admission Date']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='acyear'){
                $data['primary_info']['Academic Year']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='enroll_date'){
                $data['primary_info']['Enrollment Date']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='ExitDate'){
                $data['primary_info']['ExitDate']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='placeofbirth'){
                $data['student_info']['Place Of Birth']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='country_of_birth'){
                $data['student_info']['Contry Of Birth']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='nationality'){
                $data['student_info']['Nationality']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='mother_tongue'){
                $data['student_info']['LOTE 1']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='lang_know'){
                $data['student_info']['LOTE 2']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='religion'){
                $data['student_info']['Religion']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='Describes_child'){
                $data['student_info']['Describes this child?']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='previous_school'){
                $data['student_info']['Previous School']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='Language_1'){
                $data['student_info']['Born in Australia ?']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='ingenious_status'){
                $data['student_info']['Indigenous status']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='caregiver'){
                $data['student_info']['Care Giver']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='caregiver_remark'){
                $data['student_info']['Care Giver Remark']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='S_HOUSE_NO'){
                $data['student_info']['Flat No. OR Unit No. / Street No.']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='S_STREET_NAME'){
                $data['student_info']['Street Name']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='S_HM_ADDRESS03'){
                $data['student_info']['City/Suburb']=$row['COLUMN_NAME'];
            } 
            else if($row['COLUMN_NAME']=='S_HM_STATE'){
                $data['student_info']['State']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='S_HM_POSTCODE'){
                $data['student_info']['Postcode']=$row['COLUMN_NAME'];
            }
        }
        return $data;

    }
    function getParentDetails(){
        $dbobject = new DB();
        $dbobject->getCon();
        $data['father']=array();
        $data['mother']=array();
        $data['other']=array();
        $sql="SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='parent';";
        $rs=$dbobject->select($sql);
        while($row=$dbobject->fetch_array($rs)){
            if($row['COLUMN_NAME']=='f_sur_name'){
                $data['father']['Surname']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='family_code'){
                $data['father']['Family Code']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='f_given_name'){
                $data['father']['Given Name']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='fnationality'){
                $data['father']['Nationality']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='f_r_status'){
                $data['father']['Relationship']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='flangknown'){
                $data['father']['Languages Known']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='FSE_STATUS'){
                $data['father']['School Education']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='FNSE_STATUS'){
                $data['father']['Non School Education']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='focc'){
                $data['father']['Occupation']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='FATHER_OCCU_GROUP'){
                $data['father']['Occupation Group']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='father_office_address'){
                $data['father']['Office Address']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='fmob'){
                $data['father']['Mobile']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='f_work_num'){
                $data['father']['Work Number']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='femail'){
                $data['father']['Email']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='sms_number'){
                $data['father']['SMS Number']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='F_Country_of_Birth'){
                $data['father']['Country of Birth']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='fdob'){
                $data['father']['Date Of Birth']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='HM_ADDRESS02'){
                $data['father']['Flat No. OR Unit No. / Street No.']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='F_STREETNAME'){
                $data['father']['Street Name']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='HM_ADDRESS03'){
                $data['father']['City/Suburb']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='HM_STATE'){
                $data['father']['State']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='HM_POSTCODE'){
                $data['father']['Postcode']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='m_family_code'){
                $data['mother']['Family Code']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='m_sur_name'){
                $data['mother']['Surname']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='m_given_name'){
                $data['mother']['Given Name']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='mnationality'){
                $data['mother']['Nationality']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='m_r_status'){
                $data['mother']['Relationship']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='mlangknown'){
                $data['mother']['Languages Known']=$row['lang'];
            }
            else if($row['COLUMN_NAME']=='MSE_STATUS'){
                $data['mother']['School Education']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='MNSE_STATUS'){
                $data['mother']['Non School Education']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='mocc'){
                $data['mother']['Occupation']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='MOTHER_OCCU_GROUP'){
                $data['mother']['Occupation Group']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='mmob'){
                $data['mother']['Mobile']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='m_work_num'){
                $data['mother']['Work Number']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='memail'){
                $data['mother']['Email']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='fmob'){
                $data['mother']['SMS Number']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='M_Country_of_Birth'){
                $data['mother']['Country of Birth']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='mdob'){
                $data['mother']['Date Of Birth']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='M_HM_ADDRESS02'){
                $data['mother']['Flat No. OR Unit No. / Street No.']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='M_STREETNAME'){
                $data['mother']['Street Name']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='M_HM_ADDRESS03'){
                $data['mother']['City/Suburb']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='M_HM_STATE'){
                $data['mother']['State']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='M_HM_POSTCODE'){
                $data['mother']['Postcode']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='o_family_code'){
                $data['other']['Family Code']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='gsurname'){
                $data['other']['Surname']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='gname'){
                $data['other']['Given Name']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='gnationality'){
                $data['other']['Relationship']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='grelation'){
                $data['other']['Relationship']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='language_known'){
                $data['other']['Languages Known']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='GSE_STATUS'){
                $data['other']['School Education']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='GNSE_STATUS'){
                $data['other']['Non School Education']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='gocc'){
                $data['other']['Occupation']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='gOccupationGrup'){
                $data['other']['Occupation Group']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='g_office_address'){
                $data['other']['Office Address']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='g_work_num'){
                $data['other']['Work Number']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='gemail'){
                $data['other']['Email']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='sms_number'){
                $data['other']['SMS Number']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='G_Country_of_Birth'){
                $data['other']['Country of Birth']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='gdob'){
                $data['other']['Date Of Birth']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='g_visa_status'){
                $data['other']['Residential Status']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='g_visa_subclass'){
                $data['other']['Visa Subclass']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='HG_ADDRESS02'){
                $data['other']['Flat No. OR  Unit No. / Street No.']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='G_STREETNAME'){
                $data['other']['Street Name']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='HG_ADDRESS03'){
                $data['other']['City/Suburb']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='HG_STATE'){
                $data['other']['State']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='HG_POSTCODE'){
                $data['other']['Postcode']=$row['COLUMN_NAME'];
            }
        }
        $data['father']['Residential Status']=1;
        $data['father']['Visa Subclass']=1;
        $data['mother']['Residential Status']=1;
        $data['mother']['Visa Subclass']=1;
        return $data;
    }
    function getFamilyInfo(){
        $dbobject = new DB();
        $dbobject->getCon();
        $sql="SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='famliy_info';";
        $rs=$dbobject->select($sql);
        $data['emergency_contact1']=array();
        $data['emergency_contact2']=array();
        $data['emergency_contact3']=array();
        $data['church_info']=array();
        $data['medical_info']=array();
        while($row=$dbobject->fetch_array($rs)){
            if($row['COLUMN_NAME']=='emrgy1_SURNAME'){
                $data['emergency_contact1']['Surname']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='emrgy_contact1'){
                $data['emergency_contact1']['First Name']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='emrgy1_DESCRIPTION'){
                $data['emergency_contact1']['Relationship to Students']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='emrgy1_HOME_PHONE'){
                $data['emergency_contact1']['Home Number']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='emrgy_contact1_mob'){
                $data['emergency_contact1']['Mobile']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='emrgy_contact1_wrk'){
                $data['emergency_contact1']['Work']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='emrgy2_SURNAME'){
                $data['emergency_contact2']['Surname']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='emrgy_contact2'){
                $data['emergency_contact2']['First Name']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='emrgy2_DESCRIPTION'){
                $data['emergency_contact2']['Relationship to Students']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='emrgy2_HOME_PHONE'){
                $data['emergency_contact2']['Home Number']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='emrgy_contact2_mob'){
                $data['emergency_contact2']['Mobile']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='emrgy_contact2_wrk'){
                $data['emergency_contact2']['Work']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='emrgy3_SURNAME'){
                $data['emergency_contact3']['Surname']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='emrgy_contact3'){
                $data['emergency_contact3']['First Name']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='emrgy3_DESCRIPTION'){
                $data['emergency_contact3']['Relationship to Students']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='emrgy3_HOME_PHONE'){
                $data['emergency_contact3']['Home Number']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='emrgy_contact3_mob'){
                $data['emergency_contact3']['Mobile']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='emrgy_contact3_wrk'){
                $data['emergency_contact3']['Work']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='ch_name'){
                $data['church_info']['Church Name']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='Church_religion'){
                $data['church_info']['Religion']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='ch_pastor_name'){
                $data['church_info']['Pastors Name']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='ch_contact'){
                $data['church_info']['Contact number']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='ch_address'){
                $data['church_info']['Church Address']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='ch_subrub'){
                $data['church_info']['Suburb']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='ch_STATE'){
                $data['church_info']['State']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='ch_POST'){
                $data['church_info']['Postcode']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='ch_POST'){
                $data['church_info']['Postcode']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='med_cen_contact'){
                $data['medical_info']['Contact Number']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='med_cen_contact'){
                $data['medical_info']['Contact Number']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='med_cen_name'){
                $data['medical_info']['Medical Center Name']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='med_cen_addrs'){
                $data['medical_info']['Address']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='med_cen_SUBURB'){
                $data['medical_info']['City/Suburb']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='med_cen_STATE'){
                $data['medical_info']['State']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='med_cen_post'){
                $data['medical_info']['Medicare Number']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='med_cen_exp'){
                $data['medical_info']['Medicare Expiry']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='med_cen_ab_cvr_num'){
                $data['medical_info']['Ambulance Number']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='ambulance_exp'){
                $data['medical_info']['Ambulance Expiry']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='hcover'){
                $data['medical_info']['Health Cover']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='provider'){
                $data['medical_info']['Provider']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='hcardok'){
                $data['medical_info']['Health Care']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='hc_pc_name'){
                $data['medical_info']['Name on card']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='crnexpiry'){
                $data['medical_info']['Expiry']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='height'){
                $data['medical_info']['Height (In cm)']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='weight'){
                $data['medical_info']['Weight (In kg)']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='bloodgroup'){
                $data['medical_info']['Blood Group']=$row['COLUMN_NAME'];
            }
            $data['medical_info']['Family Doctor']=$row['COLUMN_NAME'];
            $data['medical_info']['Tetanus Booster']=$row['COLUMN_NAME'];
        }
        $data['document_info']['Document Details']=1;
        $data['more_info']['More info']=1;
        $data['Passport_info']['Passport Details']=1;
        $data['submitted_form']['Submitted Forms']=1;
        return $data;
    }
    function getStaff(){
        $dbobject = new DB();
        $dbobject->getCon();
        $sql="SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='teacher';";
        $rs=$dbobject->select($sql);
        $data=array();
        while($row=$dbobject->fetch_array($rs)){
            if($row['COLUMN_NAME']=='userid'){
                $data['primary_info']['Staff ID']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='name'){
                $data['primary_info']['First Name']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='middle_name'){
                $data['primary_info']['Middle Name']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='lname'){
                $data['primary_info']['Surname']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='pref_name'){
                $data['primary_info']['Pref Name']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='dob'){
                $data['primary_info']['Date of birth']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='age'){
                $data['primary_info']['Age']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='sex'){
                $data['primary_info']['Gender']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='email'){
                $data['primary_info']['Email']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='phone'){
                $data['primary_info']['Contact No']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='mob'){
                $data['primary_info']['Mobile No']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='designation'){
                $data['more_info']['Designation']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='job_status'){
                $data['more_info']['Job Status']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='nationalilty'){
                $data['more_info']['Nationality']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='res_status'){
                $data['more_info']['Residential Status']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='visa_subclass'){
                $data['more_info']['VISA Subclass']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='address'){
                $data['more_info']['Res address']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='suburb'){
                $data['more_info']['Suburb']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='state'){
                $data['more_info']['State']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='doj'){
                $data['more_info']['Date of joining']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='experience'){
                $data['more_info']['Years of experience']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='exp_in_this_school'){
                $data['more_info']['No.Of Years Served in this school']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='edu'){
                $data['more_info']['Highest Degree']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='Tax_File_No'){
                $data['more_info']['Tax File No']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='VIT_Reg'){
                $data['more_info']['VIT Reg']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='Expiry'){
                $data['more_info']['Expiry']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='WWC'){
                $data['more_info']['WWC']=$row['COLUMN_NAME'];
            }
            else if($row['COLUMN_NAME']=='Expiry2'){
                $data['more_info']['Expiry']=$row['COLUMN_NAME'];
            }
        }
       
        return $data;
    }
    function get_level($type){
        $dbobject = new DB();
        $dbobject->getCon();
        if($type=="staff"){
        $main_menu=$dbobject->selectall("menu_main_items",array("item"=>"Staff Management"));
        $submenu=$dbobject->selectall("menu_sub_items",array("main_id"=>$main_menu['id'],"submenu"=>"Staff Info Access Level"));
        }elseif($type=="student"){
            $main_menu=$dbobject->selectall("menu_main_items",array("item"=>"Student Management"));
            $submenu=$dbobject->selectall("menu_sub_items",array("main_id"=>$main_menu['id'],"submenu"=>"Student Info Access Level"));
        }
        $sql="SELECT `id`,`submenu2` FROM `menu_sub_items2` WHERE `sub_menu_id`='".$submenu['id']."' AND `type`='".$type."'";
        $rs=$dbobject->select($sql);
        $i=0;
        $data=array();
        while($row=$dbobject->fetch_array($rs)){
            $data[$row['id']]=$row['submenu2'];
            $i++;
        }
        return $data;
    }
    function Classlist($classlist){
        $class=array();
        $i=0;
        if(!empty($classlist)){
            foreach($classlist as $c){
               $class[$i]=array($c['classno'],$c['classname']);
               $i++;
            }
        }
       return $class;
    }

}