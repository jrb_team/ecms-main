<?php
class myrootonline
{
    function myroot_url(){
        $url="https://planner.myrouteonline.com/ws_api/?m=routePlanStart";
        return $url;
    }
    function Url_routePlanCheck(){
        $url="https://planner.myrouteonline.com/ws_api/?m=routePlanCheck";
        return $url;
    }
    function myroot_tocken(){
        $tocken="johns-672e32b6-dcf1-4f95-9525-d2f067d38c50";
        return  $tocken;      
    }
    function Curl_Action($type,$post_data_string){
        if($type==1){
            $myroot_url=$this->myroot_url();
        }else{
            $myroot_url=$this->Url_routePlanCheck();
        }
        
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $myroot_url);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data_string);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }
    function has_duplicate_address($array,$address) {
        $length = count($array);
      
        for ($i = 0; $i < $length; $i++) {
            if ($array[$i]['address'] === $address) {
              return true;
          }
        }
      
        return false;
      }
}
?>