<?php
date_default_timezone_set('Asia/Kolkata');
include_once('createdb.php');
include_once('attendance_class.php');
class tAttendance
{
	
     function select_teacher()
	 {
		$dbobject = new DB();
		$dbobject->getCon();
		$sql=$dbobject->select("select * from `teacher` where `status`='1' and `name`!='ADMIN' order by `name`");
		$i=0;
		while($row=$dbobject->fetch_array($sql))
		{
			$tids[$i]=$row['id'];
			$i++;
		}
		return $tids;
	 }
	 function sel_data($date1,$date2,$deviceid)
	 {
		$dbobject = new DB();
		$dbobject->getCon();
		//echo "select * from `teacher_attendence` where `date`>='".$date1."' and `date`<'".$date2."' and `tid`='".$deviceid."' order by `id`";
		$sql=$dbobject->select("select * from `teacher_attendence` where `date`>='".$date1."' and `date`<'".$date2."' and `tid`='".$deviceid."' order by `id`");
		return $sql;
	 }
	 function calc_time($login,$logout)
	 {
		if($login!="" && $logout!="")
		{
		$login=new DateTime($login);
		$logout=new DateTime($logout);
		$hours= date_diff($login,$logout);
		return $hours->format('%h:%i:%s');
		}
		else
		{
		return $hours=0;
		}
	 }
     function remark($giventime,$dep,$type)
     {
        $dbobject = new DB();
        $dbobject->getCon();
		$giventime_temp=$giventime;
        $sql=$dbobject->select("select * from `staff_wok_hours`");
        $row=$dbobject->fetch_array($sql);
        if($type=="login")
        {
        $giventime=date('H:i:s',strtotime($giventime));
        $giventime = strtotime($giventime);
        $start_time=strtotime($row['start_hour']);
        $timediff=round(abs($giventime - $start_time) / 60,2). " minute";
		if($giventime_temp!="")
		{
			
				if($giventime > $start_time)
				{
				$remark="late";
				}
				else{
					 $remark="Present";
				}
		}
        else
		{
			$remark="Absent";
		}		
         return $remark;
        }
     }
function teacher_attendence_dateRange($data_array,$staffid)
	 {
		$dbobject = new DB();
		$dbobject->getCon();
		$first_date=array_values($data_array)[0];
		$last_date=end($data_array);
		$qry="select * from `teacher_attendence` where `date`>='".$first_date."' and `date`<='".$last_date."' ";
		if($staffid!=""){
			$qry.=" AND `tid`='".$staffid."'";
		}
		$qry.="order by `date`,`tid`,`checkin`";
		$sql=$dbobject->select($qry);
		$data=array();
		$i=0;
		while($row=$dbobject->fetch_array($sql))
		{
			$data[$row['date']][$row['tid']][$i]['checkin']=$row['checkin'];
			$data[$row['date']][$row['tid']][$i]['latitute']=$row['latitute'];
			$data[$row['date']][$row['tid']][$i]['longitute']=$row['longitute'];
			$i++;
		}
		return $data;
	 }
 function teacher_attendence_bydate($date1)
	 {
		$dbobject = new DB();
		$dbobject->getCon();		 
		 $teacher=$this->select_teacher();
		 $s_absent=0;
		 $s_late=0;
		 $s_present=0;
		 $date2=$next_date = date('Y-m-d', strtotime($date1 .' +1 day'))." 00:00:00";
		 if(!empty($teacher))
		 {
			 foreach($teacher as $tid)
			 {
					$login="";
					$logout="";
					$logintime="";
					$logouttime="";
					$remark="";
					$teacher_det=$dbobject->selectall("teacher",array("id"=>$tid));
					$deviceid=$teacher_det['deviceid'];
					
					$set_data=$this->sel_data($date1,$date2,$tid);
				
					$i=1;
					
					while($row=$dbobject->fetch_array($set_data))
					{
						if($i % 2==0)
						{
						$logout=$row['checkin'];
						
						}
						else
						{
						$login=$row['checkin'];
						}
						
						$i++;
					}
					$hours=$this->calc_time($login,$logout);
					$remark=$this->remark($login,$teacher_det['tdep'],"login");
					if($login!="")
					{
					$logintime=date('h:i:s a',strtotime($login));
					}
					if($logout!="")
					{
					$logouttime=date('h:i:s a',strtotime($logout));
					}

							if($remark=="Absent")
							{
							$s_absent++;
							}
							elseif($remark=="late")
							{
							$s_late++;
							}						
							elseif($remark=="Present")
							{
							$s_present++;
							}
			 }
		 }
		    $data['Present']=$s_present;
			$data['Absent']=$s_absent;
			$data['Late']=$s_late;
			return $data;
	 }
 function teacher_attendence_bydate_list($date1)
	 {
		$dbobject = new DB();
		$dbobject->getCon();
        $Attendance=new Attendance();		
		 $teacher=$this->select_teacher();
		 $s_absent=0;
		 $s_late=0;
		 $s_present=0;
		 $date2=$next_date = date('Y-m-d', strtotime($date1 .' +1 day'))." 00:00:00";
		 if(!empty($teacher))
		 {
					$present_list=0;
					$absent_list=0;
					$late_list=0;			 
			 foreach($teacher as $tid)
			 {
					$login="";
					$logout="";
					$logintime="";
					$logouttime="";
					$remark="";

					$teacher_det=$dbobject->selectall("teacher",array("id"=>$tid));
					$deviceid=$teacher_det['deviceid'];
				 $devicelog_data_by_userid=$Attendance->devicelog_data_by_userid($date1,$year,$month,$teacher_det['userid']);			
					$set_data=$this->sel_data($date1,$date2,$tid);
				
					$i=1;
					
					while($row=$dbobject->fetch_array($set_data))
					{
						if($i % 2==0)
						{
						$logout=$row['checkin'];
						
						}
						else
						{
						$login=$row['checkin'];
						}
						
						$i++;
					}
					$hours=$this->calc_time($login,$logout);
					$remark=$this->remark($login,$teacher_det['tdep'],"login");
					if($login!="")
					{
					$logintime=date('h:i:s a',strtotime($login));
					}
					if($logout!="")
					{
					$logouttime=date('h:i:s a',strtotime($logout));
					}

							$data_absent[$absent_list]['id']=$tid;
							$data_absent[$absent_list]['fname']=$teacher_det['name'];
							$data_absent[$absent_list]['middle_name']=$teacher_det['middle_name'];
							$data_absent[$absent_list]['lname']=$teacher_det['lname'];
							$data_absent[$absent_list]['pref_name']=$teacher_det['pref_name'];
							$data_absent[$absent_list]['slno']=$absent_list_sl;
							$data_absent[$absent_list]['IN']=$logintime;
							$data_absent[$absent_list]['OUT']=$logouttime;
							$data_absent[$absent_list]['hours']=$hours;	
							$absent_list++;

							
			 }
		 }
	
			return $data_absent;
	 }	 
}
?>