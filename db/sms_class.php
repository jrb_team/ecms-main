<?php
include_once('createdb.php');
//include_once("class.curl.php") ;
class Sms
{

	function Get_SMSGateway()
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$content=$dbobj->selectall("sms_gateway",array("id"=>1));
		return $content;
	}
	function Get_number_ind($studentid)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$sel_stud =$dbobj->selectall("student",array("studentid"=>$studentid));
	    $parent_id=$sel_stud['parent_id'];
		$parent = $dbobj->selectall("parent",array("id"=>$parent_id));
		$num=preg_replace('/[^A-Za-z0-9\-]/', '', $parent['sms_number']);
		$num=substr($num, -10);
		return $num;
	}
	function Get_number_indWithpid($sid)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$number=array();
		$sel_stud =$dbobj->selectall("student",array("sid"=>$sid));
	    $parent_id=$sel_stud['parent_id'];
		$parent = $dbobj->selectall("parent",array("id"=>$parent_id));
		if($parent['sms_number']=='fmob'){
			$num=$parent['fmob'];
		}elseif($parent['sms_number']=='mmob'){
			$num=$parent['mmob'];
		}
		if($num!=""){
		$number['num']=$this->formatNumber($num);
		$number['userid']=$parent['id'];
		}
		return $number;
	}
	function formatNumber($num){
		$num=preg_replace('/[^A-Za-z0-9\-]/', '', $num);
		$num=substr($num, -9);
		return $num="61".$num;
	}
	function Get_number_section(){
		
	}
	function Get_SMSBalance($params)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$balance=0;
		$sql="SELECT SUM(sms_balance) as sms_balance FROM `sms_purchase`";
		if(!empty($params)){
			$sql.="WHERE `tr_status`='1'";
		}
		$qry=$dbobj->select($sql);
		while($row=$dbobj->fetch_array($qry)){
			$balance=$balance+$row['sms_balance'];
		}
		$sql_sent="SELECT COUNT(id) as sms_sent FROM `sms`";
		$qry_sent=$dbobj->select($sql_sent);
		while($row_sent=$dbobj->fetch_array($qry_sent)){
			$balance=$balance-$row_sent['sms_sent'];
		}
		return $balance;
	}
	function Send_sms($num,$msg)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$num=implode(',', $num);
		if(!empty($num))
		{
			$url=$this->url($num,$msg);	
			$c = new curl($url);
			$c->setopt(CURLOPT_FOLLOWLOCATION, true);
			$re = $c->exec();
			return $re;
		}
		else{
		return "Invalid Number";
		}
		
	}
	function Send_sms_ind($studid,$num,$msg,$type)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$sms_date = date('Y-m-d H:i:s a');
		if($num!="")
		{
			//$url=$this->url($num,$msg);	
			//$c = new curl($url);
			//$c->setopt(CURLOPT_FOLLOWLOCATION, true);
			//$re = $c->exec();
			//$re=substr($re, 0, 36);
			$ins=$dbobj->exe_qry("insert into `sms` (`user`,`mob_no`,`message`,`sms_date`,`report`,`type`,`delivery_report`) values ('".$studid."','".$num."','".addslashes($msg)."','".$sms_date."','".$re."','".$type."','Submit')");
			//$status=$this->Sms_status($re);
			if($type=="parent")
			{
				$student_det=$dbobj->selectall("student",array("studentid"=>$studid));
				$name=strtoupper($student_det['sname']);
			}
			elseif($type=="teacher")
			{
				$teacher_det=$dbobj->selectall("teacher",array("id"=>$studid));
				$name=strtoupper($teacher_det['name'])." ".strtoupper($teacher_det['lname']);
			}
			elseif($type=="office_staff")
			{
				$staff_det=$dbobj->selectall("office_staffs",array("id"=>$studid));
				$name=strtoupper($staff_det['cfname'])." ".strtoupper($staff_det['clname']);
			}
			elseif($type=="school_heads")
			{
				$staff_det=$dbobj->selectall("school_heads",array("id"=>$studid));
				$name=strtoupper($staff_det['name']);
			}

			return $studid."-".$name."-".$num."-".$status;
		}
		else{
		return "Invalid Number";
		}
	}
	function url($num,$msg)
	{
	//$url="http://sms.jrboonsolutions.com/api/smsapi.aspx?username=donboscoputhupally&password=donboscoputhupallypwd&to=".$num."&from=DBHSSP&message=".$msg."";
	return $url;
	}
	function Sms_status($re)
	{
		$re=substr($re, 0, 36);
		if (preg_match("/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i", $re)) {
		  $result="SMS Sent Successfully";
		} else {
		  $result=$re;
		}
	 return $result;
	}
	function Sms_Delivery($re)
	{ 
			$url=$this->Status_url($re);	
			$c = new curl($url);
			$c->setopt(CURLOPT_FOLLOWLOCATION, true);
			$re = $c->exec();
			return $re;
		
	}
	
	function Status_url($re)
	{
	//$re = "4aa9384c-c131-44bd-8117-fb0ef172e9ec ";
	// $url="http://sms.jrboonsolutions.com/api/smsstatus.aspx?username=donboscoputhupally&password=donboscoputhupallypwd&messageid=$re";
	return $url;
	}
function student_individual_list($acyear)
{
  $dbobject = new DB();
  $dbobject->getCon();	
	
	$classno_det=$dbobject->get_classlist("");
	$i=0;
	if(!empty($classno_det))
	{
		foreach($classno_det as $classno_dt)
		{
			$classno=$classno_dt['classno'];
			$classid_det=$dbobject->get_classByClassno($classno);
			if(!empty($classid_det))
			{
				foreach($classid_det as $classid_dt)
				{
					
					$data[$i]['class_id']=$classid_dt['classid'];
					$data[$i]['classname']=$classid_dt['classname'];
					$data[$i]['division']=$classid_dt['division'];
					$data[$i]['studentList']=$dbobject->get_studentdetByClassAndAcyear($data[$i]['class_id'],$acyear);
				$i++;						
				}
			}
		}
	}
	return $data;
}
function Other_Staff()
{
  $dbobject = new DB();
  $dbobject->getCon();	
  $qry="select id,phone,name,lname,mob from teacher  where `status`='active' and `group` NOT IN ('TEACHER','HEADMISTRESS','SCHOOL-COORDINATOR','Admin') order by name ASC"; 	
  $rslt=$dbobject->select($qry);
  $i=0;
  while($row=$dbobject->fetch_array($rslt))
		{
			$data[$i]['id']=$row['id'];
			$data[$i]['name']=$row['name'];
			$data[$i]['num']=$row['mob'];
			$data[$i]['type']="office_staff";
			$i++;
		}  
return $data;		
}
function school_heads($acyear)
{
  $dbobject = new DB();
  $dbobject->getCon();	
  $qry="select * from school_heads order by name ASC;";	
  $rslt=$dbobject->select($qry);
  $i=0;
  while($row=$dbobject->fetch_array($rslt))
		{
			$data[$i]['id']=$row['id'];
			$data[$i]['name']=$row['name'];
			$data[$i]['num']=$row['num'];
			$data[$i]['type']="school_heads";
			$i++;
		} 
return $data;		
}
function school_pta($acyear)
{
  $dbobject = new DB();
  $dbobject->getCon();	
  $qry="select * from `school_pta`  where `acyear`='".$acyear."' order by name ASC";
  $rslt=$dbobject->select($qry);
  $i=0;
  while($row=$dbobject->fetch_array($rslt))
		{
			$data[$i]['id']=$row['id'];
			$data[$i]['name']=$row['name'];
			$data[$i]['num']=$row['num'];
			$data[$i]['type']="school_pta";
			$i++;
		}  
return $data;		
}
function classlist_BYclassno()
{
  $dbobject = new DB();
  $dbobject->getCon();
  $i=0;
  $qry="select distinct classname,classno from sclass where `classid`!='-1' and `classno`<='14' order by classno,division;"; 
  $rslt=$dbobject->select($qry);
  while( $row=$dbobject->fetch_array($rslt))
		{
			$data[$i]['classname']=$row['classname'];
			$data[$i]['classno']=$row['classno'];
			$i++;
        }	
	return $data;
}
function teacher()
{
  $dbobject = new DB();
  $dbobject->getCon();	
  $qry="select id,phone,name,lname,mob from teacher  where `status`='active' and `group` IN ('TEACHER','HEADMISTRESS','SCHOOL-COORDINATOR','Admin') order by name ASC"; 	
  $rslt=$dbobject->select($qry);
  $i=0;
  while($row=$dbobject->fetch_array($rslt))
		{
			$data[$i]['id']=$row['id'];
			$data[$i]['name']=$row['name'];
			$data[$i]['num']=$row['mob'];
			$data[$i]['type']="teacher";
			$i++;
		}  
return $data;		
}
}
?>
