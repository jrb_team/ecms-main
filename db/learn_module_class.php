<?php
include_once('createdb.php');
include_once('admin_class.php');
include_once('academic_class.php');
class Learn
{

	function LearnDataByclassAndSub($subid,$classid,$classno){
		$date=array();
		$data[0]['type']="Notes";
		$data[0]['class']="btn-seven notes-btn";
		$data[0]['name']="Notes";
		$data[0]['count']=0;
		$data[1]['type']="Assignment";
		$data[1]['class']="btn-two assignment-btn";
		$data[1]['name']="Assignment";
		$data[1]['count']=0;
		$data[2]['type']="Homework";
		$data[2]['class']="btn-three Homework-btn";
		$data[2]['name']="Homework";
		$data[2]['count']=0;
		$data[3]['type']="Resources";
		$data[3]['class']="btn-four Resources-btn";
		$data[3]['name']="Resources";
		$data[3]['count']=0;
		$data[4]['type']="Online_Class";
		$data[4]['class']="btn-five";
		$data[4]['name']="Online Class";
		$data[4]['count']=0;
		$data[5]['type']="Lesson_Plan";
		$data[5]['class']="btn-six Lesson-Plan-btn";
		$data[5]['name']="Lesson Plan";
		$data[5]['count']=0;
		/*$data[6]['type']="Exam";

		$data[6]['class']="btn-one";

		$data[6]['name']="Exam";

		$data[6]['count']=0;*/

		return $data;

	}

	function module_data($subid,$classno,$classid,$type,$acyear)

	 {

		$dbobject = new DB();

		$dbobject->getCon(); 

		$Academic = new Academic();

		$date_from= date('Y-m-d', strtotime($date_from));

		$date_to= date('Y-m-d', strtotime($date_to));

		$qry="select `learning_mod`.*,`sclass`.`classname`,`sclass`.`division`,`sclass`.`classno` from `learning_mod` left join `sclass` on `sclass`.`classid`=`learning_mod`.`classid` where `learning_mod`.`acyear`='".$acyear."'";

		if($classid!="-1")

		{
		$qry.=" and `sclass`.`classid`='".$classid."'";			
		}
		if($classno!="-1")
		{
		$qry.=" and `sclass`.`classno`='".$classno."'";			
		}
		$qry.=" and `learning_mod`.`subid`='".$subid."'";			
		if($type!="-1")
		{
		$qry.=" and `learning_mod`.`type`='".$type."'";	
		}	
        $qry.=" order by  `learning_mod`.`type`";
		$sql=$dbobject->select($qry);
		$i=0;
		while($row=$dbobject->fetch_array($sql))
		{
			$data[$i]['id']=$row['id'];
			$data[$i]['title']=$row['title'];
			$data[$i]['refcode']=$row['refcode'];
			$data[$i]['due_date']=$row['due_date'];			
			$data[$i]['atta_path']=$row['atta_path'];
		    $data[$i]['descr']=$row['descr'];
			$data[$i]['reflink']=$row['reflink'];
			$data[$i]['entry_by']=$row['entry_by'];
		    $data[$i]['date']=$row['date'];
			$data[$i]['classname']=$row['classname'];
			$data[$i]['division']=$row['division'];
			$data[$i]['classno']=$row['classno'];
			$data[$i]['classid']=$row['classid'];
			$data[$i]['type']=$row['type'];
			$data[$i]['subid']=$row['subid'];			
			$data[$i]['subtype']=$row['subtype'];
			$data[$i]['start_time']=$row['start_time'];	
			 $admin=new Admin($row['classno']);
	         $subject_table=$admin->subject_table();
			 $subject_data=$row['subtype']."-".$row['subid'];			                     
	         $data[$i]['subname']=$dbobject->sel_subject($subject_data,$subject_table);
			$i++;
		}
		return $data;
	 }
function getLearnAttachment($id){
	$dbobject = new DB();
	$dbobject->getCon();
	$i=0;
	$data=array();
	$sel=$dbobject->select("SELECT * FROM `attachments` WHERE `learn_id`='".$id."'");
	while($row=$dbobject->fetch_array($sel)){
		$data[$i]['id']=$row['id'];
		$data[$i]['learn_id']=$row['learn_id'];
		$data[$i]['filetype']=$row['filetype'];
		$data[$i]['size']=$row['size'];
		$data[$i]['extension']=$row['extension'];
		$data[$i]['document_name']=$row['document_name'];
		$data[$i]['original_filename']=$row['original_filename'];
		$i++;
	}
	return $data;
}

function getStudentAttachment($workid){
	$dbobject = new DB();
	$dbobject->getCon();
	$i=0;
	$data=array();
	$sel=$dbobject->select("SELECT * FROM `studentwork_atttachment` WHERE `studentworkid`='".$workid."'");
	while($row=$dbobject->fetch_array($sel)){
		$data[$i]['id']=$row['id'];
		$data[$i]['studentworkid']=$row['studentworkid'];
		$data[$i]['filename']=$row['filename'];
		$i++;
	}
	return $data;
}

function module_dataByClassno($subid,$classno,$type,$acyear)
	 {
		$dbobject = new DB();
		$dbobject->getCon(); 
		$Academic = new Academic();
		$date_from= date('Y-m-d', strtotime($date_from));
		$date_to= date('Y-m-d', strtotime($date_to));
		$qry="select `learning_mod`.*,`sclass`.`classname`,`sclass`.`division`,`sclass`.`classno`,`learning_mod`.`download_restriction`,`learning_mod`.`status` from `learning_mod` left join `sclass` on `sclass`.`classid`=`learning_mod`.`classid` where `learning_mod`.`acyear`='".$acyear."' and `draft`='0'";
		if($classno!="-1")
		{
		$qry.=" and `learning_mod`.`classno`='".$classno."'";
		}		
		$qry.=" and `learning_mod`.`subid`='".$subid."'";			
		if($type!="-1")
		{
		$qry.=" and `learning_mod`.`type`='".$type."'";
		}	
        $qry.=" order by  `learning_mod`.`type`";
		$sql=$dbobject->select($qry);
		$i=0;
		while($row=$dbobject->fetch_array($sql))
		{
			$data[$i]['id']=$row['id'];
			$data[$i]['title']=$row['title'];
			$data[$i]['refcode']=$row['refcode'];
			$data[$i]['due_date']=$row['due_date'];			
			$data[$i]['atta_path']=$row['atta_path'];
		    $data[$i]['descr']=$row['descr'];
			$data[$i]['reflink']=$row['reflink'];
			$data[$i]['entry_by']=$row['entry_by'];
		    $data[$i]['date']=$row['date'];
			$data[$i]['classname']=$row['classname'];
			$data[$i]['division']=$row['division'];
			$data[$i]['classno']=$row['classno'];
			$data[$i]['type']=$row['type'];
			$data[$i]['subid']=$row['subid'];			
			$data[$i]['subtype']=$row['subtype'];
			$data[$i]['start_time']=$row['start_time'];
			$data[$i]['download_restriction']=$row['download_restriction'];
			$data[$i]['status']=$row['status'];	
			 $admin=new Admin($row['classno']);
	         $subject_table=$admin->subject_table();
			 $subject_data=$row['subtype']."-".$row['subid'];			                     
	         $data[$i]['subname']=$dbobject->sel_subject($subject_data,$subject_table);
			$i++;
		}
		return $data;
	 }
	 function module_dataByClassnoSid($subid,$classno,$type,$acyear,$sid)
	 {
		$dbobject = new DB();
		$dbobject->getCon(); 
		$Academic = new Academic();
		$date_from= date('Y-m-d', strtotime($date_from));
		$date_to= date('Y-m-d', strtotime($date_to));
		$qry="select `studentwork`.*,`learning_mod`.`title`,`learning_mod`.`refcode`,`learning_mod`.`date`,`learning_mod`.`due_date`,`learning_mod`.`start_time`,`learning_mod`.`atta_path`,`learning_mod`.`descr`,`learning_mod`.`reflink`,`learning_mod`.`download_restriction`,`learning_mod`.`status` from `studentwork` left join `learning_mod` on `studentwork`.`learn_id`=`learning_mod`.`id` where `learning_mod`.`acyear`='".$acyear."' and `draft`='0'";
		if($classno!="-1")
		{
		$qry.=" and `learning_mod`.`classno`='".$classno."'";
		}		
		$qry.=" and `learning_mod`.`subid`='".$subid."'";			
		if($type!="-1")
		{
		$qry.=" and `learning_mod`.`type`='".$type."'";
		}	
        $qry.=" and `studentwork`.`sid`='".$sid."' order by  `learning_mod`.`type`";
		$sql=$dbobject->select($qry);
		$i=0;
		while($row=$dbobject->fetch_array($sql))
		{
			$data[$i]['id']=$row['learn_id'];
			$data[$i]['title']=$row['title'];
			$data[$i]['refcode']=$row['refcode'];
			$data[$i]['due_date']=$row['due_date'];			
			$data[$i]['atta_path']=$row['atta_path'];
		    $data[$i]['descr']=$row['descr'];
			$data[$i]['reflink']=$row['reflink'];
			$data[$i]['entry_by']=$row['entry_by'];
		    $data[$i]['date']=$row['date'];
			$data[$i]['type']=$row['type'];
			$data[$i]['subid']=$row['subid'];			
			$data[$i]['subtype']=$row['subtype'];
			$data[$i]['start_time']=$row['start_time'];
			$data[$i]['login_time']=$row['login_time'];
			$data[$i]['submit_date']=$row['submit_date'];
			$data[$i]['submition_status']=$row['submition_status'];
			$data[$i]['download_restriction']=$row['download_restriction'];
			$data[$i]['status']=$row['status'];	
			 $admin=new Admin($row['classno']);
	         $subject_table=$admin->subject_table();
			 $subject_data=$row['subtype']."-".$row['subid'];			                     
	         $data[$i]['subname']=$dbobject->sel_subject($subject_data,$subject_table);
			$i++;
		}
		return $data;
	 }
function module_DraftByClassno($subid,$classno,$type,$acyear)
	 {
		$dbobject = new DB();
		$dbobject->getCon(); 
		$Academic = new Academic();
		$qry="select `learning_mod`.*,`sclass`.`classname`,`sclass`.`division`,`sclass`.`classno`,`learning_mod`.`download_restriction`,`learning_mod`.`status` from `learning_mod` left join `sclass` on `sclass`.`classid`=`learning_mod`.`classid` where `learning_mod`.`acyear`='".$acyear."' and `draft`='1'";
		if($classno!="-1")
		{
		$qry.=" and `learning_mod`.`classno`='".$classno."'";
		}		
		$qry.=" and `learning_mod`.`subid`='".$subid."'";			
		if($type!="-1")
		{
		$qry.=" and `learning_mod`.`type`='".$type."'";
		}	
        $qry.=" order by  `learning_mod`.`type`";
		$sql=$dbobject->select($qry);
		$i=0;
		while($row=$dbobject->fetch_array($sql))
		{
			$data[$i]['id']=$row['id'];
			$data[$i]['title']=$row['title'];
			$data[$i]['refcode']=$row['refcode'];
			$data[$i]['due_date']=$row['due_date'];			
			$data[$i]['atta_path']=$row['atta_path'];
		    $data[$i]['descr']=$row['descr'];
			$data[$i]['reflink']=$row['reflink'];
			$data[$i]['entry_by']=$row['entry_by'];
		    $data[$i]['date']=$row['date'];
			$data[$i]['classname']=$row['classname'];
			$data[$i]['division']=$row['division'];
			$data[$i]['classno']=$row['classno'];
			$data[$i]['type']=$row['type'];
			$data[$i]['subid']=$row['subid'];			
			$data[$i]['subtype']=$row['subtype'];
			$data[$i]['start_time']=$row['start_time'];
			$data[$i]['download_restriction']=$row['download_restriction'];
			$data[$i]['status']=$row['status'];	
			 $admin=new Admin($row['classno']);
	         $subject_table=$admin->subject_table();
			 $subject_data=$row['subtype']."-".$row['subid'];			                     
	         $data[$i]['subname']=$dbobject->sel_subject($subject_data,$subject_table);
			$i++;
		}
		return $data;
}
	 function module_dataByClassnoPublished($subid,$classno,$type,$acyear)

	 {

		$dbobject = new DB();

		$dbobject->getCon(); 

		$Academic = new Academic();

		$date_from= date('Y-m-d', strtotime($date_from));

		$date_to= date('Y-m-d', strtotime($date_to));

		$qry="select `learning_mod`.*,`sclass`.`classname`,`sclass`.`division`,`sclass`.`classno`,`learning_mod`.`download_restriction`,`learning_mod`.`status` from `learning_mod` left join `sclass` on `sclass`.`classid`=`learning_mod`.`classid` where `learning_mod`.`acyear`='".$acyear."' and `learning_mod`.`status`='1'";

		if($classno!="-1")

		{

		$qry.=" and `learning_mod`.`classno`='".$classno."'";			

		}		

		$qry.=" and `learning_mod`.`subid`='".$subid."'";			

		if($type!="-1")

		{

		$qry.=" and `learning_mod`.`type`='".$type."'";			

		}	

        $qry.=" order by  `learning_mod`.`type`";

		$sql=$dbobject->select($qry);

		$i=0;

		while($row=$dbobject->fetch_array($sql))

		{

			$data[$i]['id']=$row['id'];

			$data[$i]['title']=$row['title'];

			$data[$i]['refcode']=$row['refcode'];

			$data[$i]['due_date']=$row['due_date'];			

			$data[$i]['atta_path']=$row['atta_path'];

		    $data[$i]['descr']=$row['descr'];

			$data[$i]['reflink']=$row['reflink'];

			$data[$i]['entry_by']=$row['entry_by'];

		    $data[$i]['date']=$row['date'];

			$data[$i]['classname']=$row['classname'];

			$data[$i]['division']=$row['division'];

			$data[$i]['classno']=$row['classno'];

			$data[$i]['type']=$row['type'];

			$data[$i]['subid']=$row['subid'];			

			$data[$i]['subtype']=$row['subtype'];

			$data[$i]['start_time']=$row['start_time'];

			$data[$i]['download_restriction']=$row['download_restriction'];

			$data[$i]['status']=$row['status'];	

			 $admin=new Admin($row['classno']);

	         $subject_table=$admin->subject_table();

			 $subject_data=$row['subtype']."-".$row['subid'];			                     

	         $data[$i]['subname']=$dbobject->sel_subject($subject_data,$subject_table);			

			

			



			$i++;

		}

		return $data;

	 }

function GetSubmittedStudent($id){
	$dbobject = new DB();
	$dbobject->getCon(); 
	$sel=$dbobject->select("SELECT DISTINCT `studentwork`.`sid`,`teacher_chk_status`,`login_time`,`submit_date`,`toughness`,`time_taken`,`sname`,`s_sur_name` FROM `studentwork` INNER JOIN `student` ON `studentwork`.`sid`=`student`.`sid`  WHERE `learn_id`='".$id."' AND `studentwork`.`status`='1'");
	$i=0;
	$data=array();
	while($row=$dbobject->fetch_array($sel)){
		$data[$i]['sid']=$row['sid'];
		$data[$i]['sname']=$row['sname']." ".$row['s_sur_name'];
		$data[$i]['sname']=$row['sname']." ".$row['s_sur_name'];
		$data[$i]['login_time']=$row['login_time'];
		$data[$i]['submit_date']=$row['submit_date'];
		$data[$i]['toughness']=$row['toughness'];
		$data[$i]['time_taken']=$row['time_taken'];
		$i++;
	}
	return $data;
}

function GetNonSubmittedStudent($id){

	$dbobject = new DB();

	$dbobject->getCon(); 

	$sel=$dbobject->select("SELECT DISTINCT `studentwork`.`sid`,`teacher_chk_status`,`sname`,`s_sur_name` FROM `studentwork` INNER JOIN `student` ON `studentwork`.`sid`=`student`.`sid`  WHERE `learn_id`='".$id."' AND `studentwork`.`status`='0'");

	$i=0;

	$data=array();

	while($row=$dbobject->fetch_array($sel)){

		$data[$i]['sid']=$row['sid'];

		$data[$i]['sname']=$row['sname']." ".$row['s_sur_name'];

		$data[$i]['teacher_chk_status']=$row['teacher_chk_status'];

		$i++;

	}

	return $data;

}

function submitted_count($learnid,$classid){

	$dbobject = new DB();

	$dbobject->getCon(); 

	$sel=$dbobject->select("SELECT  COUNT(`studentwork`.`sid`) AS submited_count FROM `studentwork` INNER JOIN `student` ON `studentwork`.`sid`=`student`.`sid` WHERE `learn_id`='".$learnid."' AND `studentwork`.`status`='1' AND `student`.`classid`='".$classid."'");

	$row=$dbobject->fetch_array($sel);

	return $row['submited_count'];

}	

function check_module_refno($refcode)

{

	$dbobject = new DB();

	$dbobject->getCon(); 	

	$sel=$dbobject->exe_qry("select * from learning_mod where refcode='".$refcode."'");

	return $num=mysql_num_rows($sel);	

}	 
function module_data_insert($title,$refcode,$due_date,$desc,$reflink,$newfilename,$subid,$classid,$type,$acyear,$entry_by,$portion_completed,$remark)
{
    $action=$this->module_data_insert_action($title,$refcode,$due_date,$desc,$reflink,$newfilename,$subid,$classid,$type,$acyear,$entry_by,$portion_completed,$remark);		
	return $action['mess'];
}
function module_data_insert_action($params,$sids){
		$field="";
		$values="";
		$dbobject = new DB();
		$dbobject->getCon(); 
		$i=1;
		$count=count($params);
		if($params['due_date']!="" && $params['due_date']!="0000-00-00")
		{
		$params['due_date']= date('Y-m-d', strtotime($params['due_date']));	
		}
        else
		{
		$params['due_date']="0000-00-00";	
		}
		$date= date('Y-m-d');
		if(!empty($params)){
			foreach($params as $key=>$p){
				$field.="`".$key."`";
				if($count>$i){
					$field.=",";
				}
				$values.="'".$p."'";
				if($count>$i){
					$values.=",";
				}
				$i++;
			}
		}
		 $sql="INSERT INTO `learning_mod` (".$field.") VALUES(".$values.")";
		 $ins=$dbobject->exe_qry($sql);	
		 $learn_id=$dbobject->mysql_insert_id();
		 if(!empty($sids)){
			 foreach($sids as $s){
				 $student=$dbobject->exe_qry("INSERT INTO `studentwork` (`learn_id`,`sid`,`acyear`) VALUES ('".$learn_id."','".$s."','".$params['acyear']."')");
			 }
		 }
		 if($ins)
		 {
          $mess=1;			 
		 }
		 else
		 {
          $mess=0;			 
		 }
        $data['mess']=$mess;		
        $data['id']=$learn_id;		
		return $data;
}
function module_data_Update_action($type_id,$params,$sids){
	$field="";
	$values="";
	$dbobject = new DB();
	$dbobject->getCon(); 
	$i=1;
	$count=count($params);
	if($params['due_date']!="" && $params['due_date']!="0000-00-00")
	{
	$params['due_date']= date('Y-m-d', strtotime($params['due_date']));	
	}
	else
	{
	$params['due_date']="0000-00-00";	
	}
	$date= date('Y-m-d');
	$sql="UPDATE `learning_mod` SET ";
	if(!empty($params)){
		foreach($params as $key=>$p){
			$sql.="`".$key."`='".$p."'";
			if($count>$i){
				$sql.=",";
			}
			$i++;
		}
	}
	$sql.=",`draft`='0' WHERE `id`='".$type_id."'";
	$ins=$dbobject->exe_qry($sql);
	$del_student=$dbobject->exe_qry("DELETE FROM `studentwork` WHERE `learn_id`='".$type_id."'");
	if(!empty($sids)){
		foreach($sids as $s){
			$student=$dbobject->exe_qry("INSERT INTO `studentwork` (`learn_id`,`sid`,`acyear`) VALUES ('".$type_id."','".$s."','".$params['acyear']."')");
		}
	}
	if($ins)
	{
	 $mess=1;			 
	}
	else
	{
	 $mess=0;			 
	}
   $data['mess']=$mess;		
   $data['id']=$type_id;		
   return $data;
}
	function insert_attachment($tmp_name,$image_name,$directory)

	 {

		$dbobject = new DB();

		$dbobject->getCon(); 

        $newfilename=$dbobject->insert_attachment_by_directory($tmp_name,$image_name,$directory);

		return $newfilename;

	 }

	function delete_attachment($filename,$directory)

	 {

		$dbobject = new DB();

		$dbobject->getCon(); 

        $delete=$dbobject->delete_attachment_by_directory($filename,$directory);

	 }	 

function attacment_directory()

{

	$dbobject = new DB();

	$dbobject->getCon(); 	

	$upload_directory="../../upload";

//	$upload_directory=$dbobject->upload_directory();

	return $upload_directory;

}	

function legend_valBydate($submit_date,$due_date)

{

	if($submit_date!=0000-00-00 && $due_date!=0000-00-00)

	{

		$date = str_replace('/', '-', $submit_date);

	    $submit_date=date('Y-m-d', strtotime($date));

		$date = str_replace('/', '-', $due_date);

	    $due_date=date('Y-m-d', strtotime($date));

		if($submit_date==$due_date)

		{

			$val=1; // on time

		}

		if($submit_date<$due_date)

		{

			$val=1; // on time

		}

		if($submit_date>$due_date)

		{

			$val=2; // recived late

		}		

		

	}

    if($submit_date==0000-00-00 && $due_date!=0000-00-00)

	{

		$date = str_replace('/', '-', $due_date);

	    $due_date=date('Y-m-d', strtotime($date));

		$current_date=date('Y-m-d');

		if($current_date<$due_date)

		{

			$val=3; // pending

		}

		if($current_date==$due_date)

		{

			$val=3; // pending

		}

		if($current_date>$due_date)

		{

			$val=4; // overdue

		}		

		

	}

    return $val;	

}

function legend_imageByval($val)

{

	if($val==1)

	{

		$legend="<img src='../legend/ontime.png'  height='25' width='25'>";

	}

	if($val==2)

	{

		$legend="<img src='../legend/recivedlate.png'  height='25' width='25'>";

	}

	if($val==3)

	{

		$legend="<img src='../legend/pending.png'  height='25' width='25'>";

	}

	if($val==4)

	{

		$legend="<img src='../legend/overdue.jpg'  height='25' width='25'>";

	}

     return	$legend;

} 

function legend_det($submit_date,$due_date)

{

    $val=$this->legend_valBydate($submit_date,$due_date);

	// legend image

    $legend=$this->legend_imageByval($val);	

    return $legend;	

}

function student_work($sid,$learn_id)

{

	$dbobj = new DB();

	$dbobj->getCon();

	$check_data=$dbobj->select("select * from `studentwork` where `sid`='".$sid."' and `learn_id`='".$learn_id."'");

    $check=mysql_num_rows($check_data);

	$sql="select * from `learning_mod` where `id`='".$learn_id."'";

    $qry=$dbobj->select($sql);

	$row=$dbobj->fetch_array($qry);

	$sql="select * from `studentwork` where `studentwork`.`sid`='".$sid."' and `studentwork`.`learn_id`='".$learn_id."'";		

    $qry2=$dbobj->select($sql);

	$row2=$dbobj->fetch_array($qry2);	

	// student work 

	$data['submit_date']=$row2['submit_date'];

	$data['work']=$row2['work'];

	$data['filename']=$row2['filename'];

	$data['teacher_remark']=$row2['teacher_remark'];

	$data['teacher_comments']=$row2['teacher_comments'];

	$data['teacher_chk_status']=$row2['teacher_chk_status'];

	$data['checked_date']=$row2['checked_date'];

	$data['checked_by']=$row2['checked_by'];

	$data['id']=$row2['id'];

	// learning_mod 

	$data['subtype']=$row['subtype'];

	$data['subid']=$row['subid'];

	$data['classid']=$row['classid'];

	$data['title']=$row['title'];

	$data['due_date']=$row['due_date'];

	$data['atta_path']=$row['atta_path'];

	$data['descr']=$row['descr'];

	$data['refcode']=$row['refcode'];

	$data['reflink']=$row['reflink'];	

	$data['portion_completed']=$row['portion_completed'];	

	$data['type']=$row['type'];	

	$data['date']=$row['date'];	

	$data['entry_by']=$row['entry_by'];	

	$data['acyear']=$row['acyear'];

	return $data;

}

function student_workBysubject($sid,$type,$acyear,$subid,$subtype)

{

	$dbobj = new DB();

	$dbobj->getCon();

	$sid;

    $sql="select `studentwork`.*,`learning_mod`.`subid`,`learning_mod`.`subtype`,`learning_mod`.`due_date`,`learning_mod`.`title`,`learning_mod`.`refcode`,`learning_mod`.`date` from `studentwork` left join `learning_mod` on `learning_mod`.`id`=`studentwork`.`learn_id` where  `studentwork`.`acyear`='".$acyear."' ";

	if($sid!="-1")

	{	

	$sql.=" and `studentwork`.`sid`='".$sid."'";

	}

	if($type!="-1")

	{

		$sql.=" and `learning_mod`.`type`='".$type."'";

	}	

	if($subid!="" && $subid!="all")

	{

	$sql.=" and `learning_mod`.`subid`='".$subid."' and `learning_mod`.`subtype`='".$subtype."'";

	}

	//echo $sql;

	$sel=$dbobj->select($sql);

	$i=0;

    while($row=$dbobj->fetch_array($sel))

	{

		$i++;

		$learn_det[$i]['id']=$row['learn_id'];

		$learn_det[$i]['workid']=$row['id'];

		$learn_det[$i]['submit_date']=$row['submit_date'];

		$learn_det[$i]['subid']=$row['subid'];

		$learn_det[$i]['subtype']=$row['subtype'];

		$learn_det[$i]['due_date']=$row['due_date'];

		$learn_det[$i]['title']=$row['title'];

		$learn_det[$i]['refcode']=$row['refcode'];

		$learn_det[$i]['date']=$row['date'];

		$learn_det[$i]['teacher_remark']=$row['teacher_remark'];

        $learn_det[$i]['teacher_comments']=$row['teacher_comments'];

		

	}

return $learn_det;	

}

function insert_student_work($learn_id,$sid,$work,$filename,$acyear)

{

	$dbobj = new DB();

	$dbobj->getCon();

	$submit_date=date('Y-m-d');

	$check_data=$dbobj->select("select * from `studentwork` where `sid`='".$sid."' and `learn_id`='".$learn_id."'");

    $check=mysql_num_rows($check_data);	

	if($check==0)

	{

		if($filename!="")

		{

		$sql="INSERT INTO `studentwork`(`learn_id`, `sid`, `submit_date`, `work`, `filename`,`acyear`)";	

		$sql.=" VALUES ('".$learn_id."', '".$sid."', '".$submit_date."', '".$work."', '".$filename."', '".$acyear."')";			

		

		}

		else

		{

		$sql="INSERT INTO `studentwork`(`learn_id`, `sid`, `submit_date`, `work`,`acyear`)";	

		$sql.=" VALUES ('".$learn_id."', '".$sid."', '".$submit_date."', '".$work."','".$acyear."')";			

		}		

	}

	else

	{

		$stu_work_det=$dbobj->fetch_array($check_data);

		$stu_work_id=$stu_work_det['id'];

		if($filename!="")

		{

		$sql="UPDATE `studentwork` SET `work`='".$work."',`submit_date`='".$submit_date."',`filename`='".$filename."'  WHERE  `id`='".$stu_work_id."'";	

		}

		else

		{

		$sql="UPDATE `studentwork` SET `work`='".$work."',`submit_date`='".$submit_date."' WHERE  `id`='".$stu_work_id."'";			

		}

	}



	$action=$dbobj->exe_qry($sql);

	return $action;

}

function update_teacher_comments($stu_work_id,$teacher_comments)

{

	$dbobj = new DB();

	$dbobj->getCon();

    $sql="UPDATE `studentwork` SET `teacher_comments`='".$teacher_comments."' WHERE `id`='".$stu_work_id."'";	

	$upt=$dbobj->exe_qry($sql);

	return $upt;

}

function update_teacher_status($stu_work_id,$userid)

{

	$dbobj = new DB();

	$dbobj->getCon();

	$date=date('Y-m-d');
    $sql="UPDATE `studentwork` SET `teacher_chk_status`='1',`checked_date`='".$date."',`checked_by`='".$userid."' WHERE `id`='".$stu_work_id."'";	
	$upt=$dbobj->exe_qry($sql);
	return $upt;
}
function student_workBy_learn_id_and_classid($learn_id,$classid){
	$dbobj = new DB();
	$dbobj->getCon();
    $sql="select `studentwork`.`id`,`student`.* from `studentwork` left join `student` on `student`.`sid`=`studentwork`.`sid` where `student`.`classid`='$classid' and `studentwork`.`learn_id`='$learn_id'";
	$sel=$dbobj->select($sql);
	$i=0;
    while($row=$dbobj->fetch_array($sel))
	{
		$i++;
		$learn_det[$i]['studentid']=$row['studentid'];
		$learn_det[$i]['sname']=$row['sname'];
		$learn_det[$i]['sid']=$row['sid'];
	}
return $learn_det;	
}
function delete_module($id){
    $dbobj = new DB();
	$dbobj->getCon();
     $sql="SELECT * FROM `studentwork` where `learn_id`='".$id."'";
	$sel=$dbobj->select($sql);
	$i=0;
    while($row=$dbobj->fetch_array($sel))
	{
		$del=$dbobj->exe_qry("DELETE FROM `notifications` WHERE `type`='studentwork' and `type_id`='".$row['id']."'");
	}
	$delete=$dbobj->exe_qry("delete from `learning_mod` where `id`='".$id."'");
	$delete2=$dbobj->exe_qry("delete from `studentwork` where `learn_id`='".$id."'");
	$val=1;
    return $val;	
}
function update_student_work($work,$filename,$stu_work_id)
{
	$dbobj = new DB();
	$dbobj->getCon();
	$submit_date=date('Y-m-d');
		if($filename!="")
		{
		$sql="UPDATE `studentwork` SET `work`='".$work."',`submit_date`='".$submit_date."',`filename`='".$filename."'  WHERE  `id`='".$stu_work_id."'";	
		}
		else
		{
		$sql="UPDATE `studentwork` SET `work`='".$work."',`submit_date`='".$submit_date."' WHERE  `id`='".$stu_work_id."'";
		}
	$action=$dbobj->exe_qry($sql);
	return $action;
}
function get_all_subject_and_count($studentid,$acyear)
{
	$dbobject = new DB();
	$dbobject->getCon();
	$Academic = new Academic();
	$student_det=$dbobject->selectall("student",array("studentid"=>$studentid));
	$classid=$student_det['classid'];
	$sid=$student_det['sid'];
	$parent_id=$student_det['parent_id'];
 	$class_det=$dbobject->selectall("sclass",array("classid"=>$classid));
	$classno=$class_det['classno'];
	$admin=new Admin($classno);
	$subject_table=$admin->subject_table();	
	$get_all_subject=$Academic->get_all_subject($subject_table,$classno);
	if(!empty($get_all_subject))
	{
		$i=0;
		foreach($get_all_subject as $subject)
		{
			$subject_det=explode("-",$subject['id']);
			//echo "SELECT * FROM `learning_mod` WHERE `subid`='".$subject_det[1]."' and `subtype`='".$subject_det[0]."' and `acyear`='".$acyear."'";
			$qry=$dbobject->select("SELECT * FROM `learning_mod` WHERE `subid`='".$subject_det[1]."' and `subtype`='".$subject_det[0]."' and `acyear`='".$acyear."' and `classid`='".$classid."'");
			$row=$dbobject->fetch_array($qry);
			$learn_id=$row['id'];
			//echo "SELECT * FROM `studentwork` where `sid`='".$sid."' and `learn_id`='".$learn_id."' and `acyear`='".$acyear."'";
			$qry2=$dbobject->select("SELECT * FROM `studentwork` where `sid`='".$sid."' and `learn_id`='".$learn_id."' and `acyear`='".$acyear."'");
			$row2=$dbobject->fetch_array($qry2);
			$studentworkid=$row2['id'];
			//echo "SELECT * FROM `notifications` WHERE `type`='studentwork' and `type_id`='".$studentworkid."' and `user_to`='parent' and `userid_to`='".$parent_id."' and `read_status`='0'";
			$qry3=$dbobject->select("SELECT * FROM `notifications` WHERE `type`='studentwork' and `type_id`='".$studentworkid."' and `user_to`='parent' and `userid_to`='".$parent_id."' and `read_status`='0'");
			$count=mysql_num_rows($qry3);
			$data[$i]['id']=$subject['id'];
			$data[$i]['subject']=$subject['subject'];
			$data[$i]['count']=$count;
			$i++;
		}
	}
	return $data;
}

function Get_lesson_plans($start_date,$end_date,$subid,$classno){
	$dbobject = new DB();
	$dbobject->getCon();
	$sel=$dbobject->select("SELECT * FROM `lesson_plan`");
	$i=0;
	$data=array();
	while($row=$dbobject->fetch_array($sel)){
		$data[$i]=$row;
     $i++;
	}
	return $data;
}

function module_attachmentInsert($learn_id,$attachments,$type,$usertype,$userid,$acyear){
	$dbobject = new DB();
	$dbobject->getCon();
	if(!empty($attachments)){
		foreach($attachments as $att){
			$data_array=explode("-",$att);
			$dbobject->exe_qry("INSERT INTO `attachments` (`learn_id`,`type`,`usertype`,`userid`,`document_name`,`original_filename`,`extension`,`filetype`,`size`,`date`,`acyear`) VALUES ('".$learn_id."','".$type."','".$usertype."','".$userid."','".$data_array[0]."','".$data_array[1]."','".$data_array[2]."','".$data_array[3]."','".$data_array[4]."','".date('Y-m-d')."','".$acyear."')");
		}
	}
}

function Student_attachmentInsert($workid,$attachments){
	$dbobject = new DB();
	$dbobject->getCon();
	if(!empty($attachments)){
		foreach($attachments as $att){
			$dbobject->exe_qry("INSERT INTO `studentwork_atttachment` (`studentworkid`,`filename`,`date`) VALUES ('".$workid."','".$att."','".date('Y-m-d')."')");
		}
	}
}

function toughness(){
	$toughness=array("Very Easy","Easy","Moderate","Hard","Very Hard");
	return $toughness;
}

function time(){
	$time=array("00","01","02","03","04");
	return $time;
}
function uploadDirectory(){
	$protocol = ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";  
	$CurPageURL = $protocol . $_SERVER['HTTP_HOST']."/ecms-erp/admin/learning/";  
	return $CurPageURL;  

}
function getLearnindataBysid($sid,$acyear){
	$dbobject = new DB();
	$dbobject->getCon();
    $i=0;
    $data=array();
    $sql="SELECT `learning_mod`.`id`,`learning_mod`.`title`,`learning_mod`.`descr`,`learning_mod`.`date`,`learning_mod`.`start_time`,`learning_mod`.`due_date` FROM `studentwork` LEFT JOIN `learning_mod` ON `learning_mod`.`id`=`studentwork`.`learn_id` WHERE `studentwork`.`sid`='".$sid."' AND `learning_mod`.`acyear`='".$acyear."' AND `studentwork`.`status`='0' AND `learning_mod`.`status`='1'";
    $sel=$dbobject->select($sql);
    while($row=$dbobject->fetch_array($sel)){
        $data[$i]['id']=$row['id'];
        $data[$i]['date']=$row['date'];
        $data[$i]['start_time']=$row['start_time'];
        $data[$i]['title']=$row['title'];
        $data[$i]['descr']=strip_tags($row['descr']);
        $data[$i]['due_date']=strip_tags($row['due_date']);
        $i++;
    }
    return $data;
}
function getAssignedStudentCount($learn_id,$classid,$acyear){
	$dbobject = new DB();
	$dbobject->getCon();
	$sql="SELECT COUNT(`studentwork`.`sid`) AS `count` FROM `studentwork` LEFT JOIN `student_class` on `studentwork`.`sid`=`student_class`.`sid` WHERE `learn_id`='".$learn_id."' AND `studentwork`.`acyear`='".$acyear."' AND `student_class`.`acyear`='".$acyear."' AND `student_class`.`classid`='".$classid."'";
	$sel=$dbobject->select($sql);
	$row=$dbobject->fetch_array($sel);
	return $row['count'];
}
function updateSeenStatus($id){
	$dbobject = new DB();
	$dbobject->getCon();
	$date = date('Y-m-d H:i:s');
	$update=$dbobject->exe_qry("UPDATE `studentwork` SET `login_time`='".$date."' WHERE `id`='".$id."'");
	return true;
}
function get_filetype($extension){
	$filetype="";
	if($extension=="jpg" || $extension=="png"){
		$filetype='image';
	}elseif($extension=="pdf"){
		$filetype='pdf';
	}elseif($extension=="xlsx" || $extension=="docx"){
		$filetype='office';
	}elseif($extension=="mp4" || $extension=="wmv" || $extension=="avi" || $extension=="flv" || $extension=="mkv"){
		$filetype='video';
	}
	return $filetype;
}
}
?>