<?php
include_once('admin_class.php');


$class = $_GET['sele_class'];
$sid = $_GET['sel_student'];
$exam = $_GET['sel_exam'];
$acyear=$_GET['acyear'];

//#############retrieving student,s and school's information for report header details###########
$selschool = "select name,mob,email,affiliation_no,academic_year from schoolinfo;";
$selrs = $dbobj->select($selschool);
$selrow = $dbobj->fetch_array($selrs);
$school = $selrow['name'];
$contact = $selrow['mob'];
$academic_year=$selrow['academic_year'];
$aff_no=$selrow['affiliation_no'];
$email = $selrow['email'];

$selname = "select * from student where sid = ".$sid.";";
$result = $dbobj->select($selname);
$row = $dbobj->fetch_array($result);
$stname = $row['sname'];
$initial = $row['initial'];
$studentid = $row['studentid'];
$stud_photoid=$row['simgid'];
$parent_id=$row['parent_id'];

$parent=$dbobj->selectall("parent",array("id"=>$parent_id));
$school_info=$dbobj->selectall("schoolinfo",array("id"=>1));

$query = "select classname,division,classno from sclass where classid = ".$class.";";
$rs =$dbobj->select($query);
$row = $dbobj->fetch_array($rs);
$classname = $row['classname'];
$division = $row['division'];
$classno = $row['classno'];

$admin = new Admin($classno);
$tablename=$admin->subject_table();

$form = "<table style='width:100%' align='center'><tr><th>Subject</th>";
$selsub=$dbobj->select("select * from ".$tablename." order by `sub_id` ");
$s=1;
while($sub_row=$dbobj->fetch_array($selsub))
{
	$sub_id[$s]=$sub_row['sub_id'];
	$sub_name[$s]=$sub_row['subject'];
	$s++;
}

$sel_exm=$dbobj->select("select * from terms where classno='".$classno."' and `exam_type`!='attendance_only'");
$t=1;
while($exm_row=$dbobj->fetch_array($sel_exm))
{
	$term_id[$t]=$exm_row['id'];
	$sub_type=$exm_row['sub_type'];
	if($sub_type=='subject_only')
	{
		$colspan="2";
	}
	elseif($sub_type=='subject_or_subhead')
	{	
	$colspan="4";
	}
$form.= "<th colspan='".$colspan."' style='text-align:center'>".$exm_row['term_name']."</th>";		
$t++;
}
$form.= "<th colspan='2'>Final Result</th></tr><tr><th>MAX.MARKS</th>";
		foreach($term_id as $tid)
		{
			$max_mark=0;
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			$examtype=$term_row['exam_type'];
			if($examtype=="term")
			{
				if($sub_type=='subject_only')
				{
					$form.="<th style='text-align:center'>100</th>";
				}
				elseif($sub_type=='subject_or_subhead')
				{
				$form.="<th style='text-align:center'>80</th><th style='text-align:center'>20</th><th style='text-align:center'>100</th>";
				}
			}
			elseif($examtype=="mid_term")
			{
				$form.="<th style='text-align:center'>50</th>";
			}
			
			$form.="<th style='text-align:center' rowspan='2'>Grade</th>";
		}

$form.= "<th style='text-align:center'>400</th><th style='text-align:center' rowspan='2'>%</th></tr>";
$form.= "<tr><th>PASS MARK</th>";
		foreach($term_id as $tid)
		{
			$min_mark=0;
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			$examtype=$term_row['exam_type'];
			if($examtype=="term")
			{
				if($sub_type=='subject_only')
				{
					$form.="<th style='text-align:center'>40</th>";
				}
				else
				{
					$form.="<th style='text-align:center'>32</th><th style='text-align:center'>8</th><th style='text-align:center'>40</th>";
				}
			}
			elseif($examtype=="mid_term")
			{
				$form.="<th style='text-align:center'>20</th>";
			}
		}
$form.= "<th style='text-align:center'>160</th></tr>";

	$form.= "";
	$i=1;
	foreach($sub_id as $sub)
	{
		$g_tot=0;
		$g_max=0;
		$form.= "<tr><th>".$sub_name[$i]."</th>";	
		foreach($term_id as $tid)
		{
		$tot=0;
		$max_tot=0;
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			$exam_type=$term_row['exam_type'];
			if($sub_type=="subject_only")
			{	
				$sel_examdet=$dbobj->select("select * from `examschedule` where `examid`='".$tid."' and `classno`='".$classno."' and `sub_id`='".$sub."' and `acc_year`='".$acyear."'");
				$examdet_row=$dbobj->fetch_array($sel_examdet);
				$max_mark=$examdet_row['maxmark'];
				$min_mark=$examdet_row['minmark'];
				$sel_mark=$dbobj->select("select * from `report_card` where `sid`='".$sid."' and `classid`='".$class."' and `examid`='".$tid."' and `sub_id`='".$sub."' and `assessment`='".$exam_type."' and `ac_year`='".$acyear."'");
				$mark_row=$dbobj->fetch_array($sel_mark);
				if($mark_row['mark']=="Absent")
				{
					$form.= "<td style='color:red;text-align:center'>Ab</td>";
					$flag[$tid]=1;
				}
				elseif($mark_row['mark']<$min_mark)
				{
					$form.= "<td style='color:red;text-align:center'>".$mark_row['mark']."</td>";
				}
				else
				{
					$form.= "<td style='text-align:center'>".$mark_row['mark']."</td>";
				}
				$m_tot[$tid]=$m_tot[$tid]+$mark_row['mark'];
				$maximum_tot[$tid]=$maximum_tot[$tid]+$max_mark;
				$g_tot=$g_tot+$mark_row['mark'];
				$g_max=$g_max+$max_mark;
				if($mark_row['mark']!="Absent")
				{
					$form.= "<td style='text-align:center'>".grade_calculate($mark_row['mark'],$max_mark,$classno)."</td>";
				}
				else
				{
					$form.= "<td style='text-align:center'></td>";
				}
			}
			elseif($sub_type=="subject_or_subhead")
			{
				$subject=$dbobj->selectall($tablename,array("sub_id"=>$sub));
				if($subject['sub_head']=="1")
				{
					$sel_subhd=$dbobj->select("select * from `sub_heads` where `subj_id`='".$sub."' and `classno`='".$classno."'");
					$total=0;
					$max_total=0;
					while($subhd_row=$dbobj->fetch_array($sel_subhd))
					{
					
						$subhd_id=$subhd_row['id'];
						$sel_examdet=$dbobj->select("select * from `examschedule` where `examid`='".$tid."' and `classno`='".$classno."' and `sub_id`='".$sub."' and `subhead_id`='".$subhd_id."' and `acc_year`='".$acyear."'");
						$examdet_row=$dbobj->fetch_array($sel_examdet);
						$max_mark=$examdet_row['maxmark'];
						$min_mark=$examdet_row['minmark'];
						$sel_mark=$dbobj->select("select * from `report_card` where `sid`='".$sid."' and `classid`='".$class."' and `examid`='".$tid."' and `sub_id`='".$sub."' and `subhead_id`='".$subhd_id."' and `assessment`='".$exam_type."' and `ac_year`='".$acyear."'");
						$mark_row=$dbobj->fetch_array($sel_mark);
						if($mark_row['mark']=="Absent")
						{
							$form.= "<td style='color:red;text-align:center'>Ab</td>";
							$flag[$tid]=1;
						}
						elseif($mark_row['mark']<$min_mark)
						{
							$form.= "<td style='color:red;text-align:center'>".$mark_row['mark']."</td>";
						}
						else
						{
							$form.= "<td style='text-align:center'>".$mark_row['mark']."</td>";
						}
						$total=$total+$mark_row['mark'];
						$max_total=$max_total+$max_mark;
						$m_tot[$tid]=$m_tot[$tid]+$mark_row['mark'];
						$g_tot=$g_tot+$mark_row['mark'];
					}
					$g_max=$g_max+$max_total;
					$maximum_tot[$tid]=$maximum_tot[$tid]+$max_total;
				}
				else
				{
					$sel_examdet=$dbobj->select("select * from `examschedule` where `examid`='".$tid."' and `classno`='".$classno."' and `sub_id`='".$sub."' and `acc_year`='".$acyear."'");
					$examdet_row=$dbobj->fetch_array($sel_examdet);
					$max_mark=$examdet_row['maxmark'];
					$min_mark=$examdet_row['minmark'];
					$sel_mark=$dbobj->select("select * from `report_card` where `sid`='".$sid."' and `classid`='".$class."' and `examid`='".$tid."' and `sub_id`='".$sub."' and `assessment`='".$exam_type."' and `ac_year`='".$acyear."'");
					$mark_row=$dbobj->fetch_array($sel_mark);
					if($mark_row['mark']=="Absent")
					{
						$form.= "<td style='color:red;text-align:center'>Ab</td>";
						$flag[$tid]=1;
					}
					elseif($mark_row['mark']<$min_mark)
					{
						$form.= "<td style='color:red;text-align:center'>".$mark_row['mark']."</td>";
					}
					else
					{
						$form.= "<td style='text-align:center'>".$mark_row['mark']."</td>";
					}
					$form.= "<td style='text-align:center'>--</td>";
					$total=$mark_row['mark'];
					$max_total=$max_mark;
					$m_tot[$tid]=$m_tot[$tid]+$mark_row['mark'];
					$maximum_tot[$tid]=$maximum_tot[$tid]+$max_mark;
					$g_tot=$g_tot+$mark_row['mark'];
					$g_max=$g_max+$max_mark;
				}
				if($flag[$tid]!=1)
				{
				$form.= "<td style='text-align:center'>".$total."</td><td style='text-align:center'>".grade_calculate($total,$max_total,$classno)."</td>";
				}
				else
				{
					$form.= "<td style='text-align:center'></td><td style='text-align:center'></td>";
				}
			}
					
						
		}
		if($g_max!=0)
		{
		$round=round(($g_tot/$g_max)*100,2);
		}
		$form.= "<td></td><td></td></tr>";
	//$form.= "<th>".$round."</th></tr>";
	$i++;
	}

				
$form.= "<tr><th>Total</th>";
		foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_only")
			{
				if($flag[$tid]!=1)
				{
					$form.="<td colspan='2' align='center'><b>".$m_tot[$tid]."</b></td>";
				}
				else
				{
					$form.="<td colspan='2' align='center'></td>";
				}
			}
			elseif($sub_type=="subject_or_subhead")
			{
				
				$sel_subhd=$dbobj->select("select distinct `head_name` from `sub_heads` where  `classno`='".$classno."'");
				$num_rows=mysql_num_rows($sel_subhd);
				if($flag[$tid]!=1)
				{
					$form.="<td colspan='4' align='center'><b>".$m_tot[$tid]."</b></td>";
				}
				else
				{
					$form.="<td colspan='4' align='center'></td>";
				}
			}
		}
$form.= "<td colspan='2'></td></tr>";
$form.= "<tr><th>Percentage</th>";
		foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_only")
			{
				if($flag[$tid]!=1)
				{
					$form.="<td colspan='2' align='center'><b>";
					if($maximum_tot[$tid]!=0)
					{
					$form.=round(($m_tot[$tid]/$maximum_tot[$tid])*100,2);
					}
					$form.="</b></td>";
				}
				else
				{
					$form.="<td colspan='2' align='center'></td>";
				}
			}
			elseif($sub_type=="subject_or_subhead")
			{
				
				$sel_subhd=$dbobj->select("select distinct `head_name` from `sub_heads` where  `classno`='".$classno."'");
				$num_rows=mysql_num_rows($sel_subhd);
				if($flag[$tid]!=1)
				{
					$form.="<td colspan='4' align='center'><b>";
					if($maximum_tot[$tid]!=0)
					{
						//$form.=$maximum_tot[$tid];
						$form.=round(($m_tot[$tid]/$maximum_tot[$tid])*100,2);
					}
					$form.="</b></td>";
				}
				else
				{
					$form.="<td colspan='2' align='center'></td>";
				}
			}
		}
$form.= "<td colspan='2'></td></tr>";
$form.= "<tr><th>Grade</th>";
foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_only")
			{
				if($flag[$tid]!=1)
				{
					$form.="<td colspan='2' align='center'><b>".grade_calculate($m_tot[$tid],$maximum_tot[$tid],$classno)."</b></td>";
				}
				else
				{
					$form.="<td colspan='2' align='center'></td>";
				}
			}
			elseif($sub_type=="subject_or_subhead")
			{
				
				$sel_subhd=$dbobj->select("select distinct `head_name` from `sub_heads` where  `classno`='".$classno."'");
				$num_rows=mysql_num_rows($sel_subhd);
				if($flag[$tid]!=1)
				{
					$form.="<td colspan='4' align='center'><b>".grade_calculate($m_tot[$tid],$maximum_tot[$tid],$classno)."</b></td>";
				}
				else
				{
					$form.="<td colspan='2' align='center'></td>";
				}
			}
		}
$form.= "<td colspan='2'></td></tr>";
$form.= "<tr><th>Date Of Report</th>";
foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_only")
			{
				$form.="<td colspan='2'></td>";
			}
			elseif($sub_type=="subject_or_subhead")
			{
				
				$sel_subhd=$dbobj->select("select distinct `head_name` from `sub_heads` where  `classno`='".$classno."'");
				$num_rows=mysql_num_rows($sel_subhd);
				$form.="<td colspan='4'></td>";
			}
		}
$form.= "<td colspan='2'></td></tr>";
$form.= "<tr><th>Class Teacher's Signature</th>";
foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_only")
			{
				$form.="<td colspan='2'></td>";
			}
			elseif($sub_type=="subject_or_subhead")
			{
				
				$sel_subhd=$dbobj->select("select distinct `head_name` from `sub_heads` where  `classno`='".$classno."'");
				$num_rows=mysql_num_rows($sel_subhd);
				$form.="<td colspan='4'></td>";
			}
		}
$form.= "<td colspan='2'></td></tr>";
$form.= "<tr><th>Principal's Signature</th>";
foreach($term_id as $tid)
		{
			$term=$dbobj->select("select * from terms where `id`='".$tid."'");
			$term_row=$dbobj->fetch_array($term);
			$sub_type=$term_row['sub_type'];
			if($sub_type=="subject_only")
			{
				$form.="<td colspan='2'></td>";
			}
			elseif($sub_type=="subject_or_subhead")
			{
				
				$sel_subhd=$dbobj->select("select distinct `head_name` from `sub_heads` where  `classno`='".$classno."'");
				$num_rows=mysql_num_rows($sel_subhd);
				$form.="<td colspan='4'></td>";
			}
		}
$form.= "<td colspan='2'></td></tr>";
$form.= "</table>";
//-----------------------------------------------------------------------------------------------------------------------------

$sel_term_only=$dbobj->select("select *  from `terms` where `classno`='".$classno."' and `exam_type`='term' "); 
$i=1;
while($term_only_row=$dbobj->fetch_array($sel_term_only))
{
	$term_only_id[$i]=$term_only_row['id'];
	$term_only_name[$i]=$term_only_row['term_name'];
	$i++;
}
//--------------------------------Remark-----------------------------------------------------------------------------------
$j=1;
$rem.="<table align='center' style='width:100%;margin-top:25px;margin-bottom:10px;'><tr><th colspan='2' style='text-align:center'><b>Remarks</b></th></tr>";
foreach ($term_only_id as $tid)
   {
	$rem.="<tr><td><b>".$term_only_name[$j]."</b></td>";
	$sel_remark=$dbobj->select("select * from remark where `sid`='".$sid."' and `term`='".$tid."' and `cls`='".$class."'");
	$rem_arry=$dbobj->fetch_array($sel_remark);
	$rem.="<td>".$rem_arry['remrk']."</td></tr>";
	$j++;
   }
   $rem.="</table>";
//-------------------------NON-ACADEMIC------------------------------------------------------
					if($classno<=6)
					{
						$section="LP";
					}
					else
					{
						$section="HS";
					}
$sel_non_acc=$dbobj->select("select * from `performance_area` where `section`='".$section."' order by `id` ");
		while($acc_row=$dbobj->fetch_array($sel_non_acc))
		{
			$head=$acc_row['areaname'];
			foreach($term_only_id as $tid)
			{
				$term_det=$dbobj->selectall("terms",array("id"=>$tid));
				$sel_non_grade=$dbobj->select("select `grade` from `performance_evaluation` where sid='".$sid."' and  `headid`='".$acc_row['id']."' and`term`='".$tid."' and `classid`='".$class."' and `acc_year`='".$acyear."'");
				$grade_row=$dbobj->fetch_array($sel_non_grade);
				$grade=$grade_row['grade'];
				$nonacc[$head][$term_det['term_name']]=$grade;
			}
			
		}
	  /*--------------------------------Last Page---------------------*/
$lastpage="<table style='border:2;margin-top:25px;margin-bottom:10px;' align='center' >
                    <tr>
						<td colspan='4' align='center'>Students are assessed according to the following grades:</td>
					</tr>
					<tr><td align='center' ><b> GRADING SYSTEM</b></td></tr>
					 <tr><td> <table border='2' width=90% align='center'>
					   <tr><td colspan='3' align='center'><b>ACADEMIC ASSESSMENT</b></td><td colspan='2' align='center'><b>NON-ACADEMIC ASSESSMENT</b></td></tr>
					   <tr><td align='center'><b>MARKS RANGE</b></td><td align='center'><b>GRADE</b></td><td align='center'><b>Remark</b></td><td align='center'><b>GRADE</b></td><td align='center'><b>Remark</b></td></tr>
					   <tr><td align='center'>90% and above</td><td align='center'>A+</td><td align='center'>Excellent</td><td align='center'>A</td><td align='center'>Very Good</td></tr>
					   <tr><td align='center'>80 to 89%</td><td align='center'>A</td><td align='center'>V.Good</td><td align='center'>B</td><td align='center'>Good</td></tr>
					   <tr><td align='center'>70 to 79%</td><td align='center'>B+</td><td align='center'>Good</td><td align='center'>C</td><td align='center'>Average</td></tr>
					   <tr><td align='center'>60 to 69%</td><td align='center'>B</td><td align='center'>Fair</td><td align='center'>D</td><td align='center'>Should improve</td></tr>
					   <tr><td align='center'>50 to 59%</td><td align='center'>C+</td><td align='center'>Average</td><td align='center'></td><td align='center'></td></tr>
					   <tr><td align='center'>40 to 49%</td><td align='center'>C</td><td align='center'>Satisfactory</td><td align='center'></td><td align='center'></td></tr>
					   <tr><td align='center'>Below 40%</td><td align='center'>D</td><td align='center'>Failed</td><td align='center'></td><td align='center'></td></tr>
					   </table></td></tr>
					 <tr><td align='center'><div>Promotion at the end of the year will be base on the attendance and the consolidated performance of the student during the whole year.</div></td></tr>
					 <tr><td align='center'><div>The academic year is divided into three segments as Term 1, Term 2, and Term 3.</div></td></tr>
					 <tr><td align='center'><div>Normally a student should pass in all subjects. He/She may be considered for promotion even if he/she fails in one of the subjects other than Moral science, English, and a poor grade in SUPW.</div></td></tr>
					 <tr><td align='center'><div>A student's promotion will be determined by the cumulative percentage of all the Mid term and the Terminal Examinations.</div></td></tr>
					 <tr><td align='center'><div>A student should gain separate minimum pass mark in each subject.</div></td></tr>
					 <tr><td align='center'><div>No retests will be conducted for the absentees. </div></td></tr>
					 <tr><td align='center'><div>Parents are requested to go through their ward's performance carefully and take necessary steps to bring further improvement.</div></td></tr>
					 <tr><td align='center'><div>The decision of the principal in all matters is final and binding.</div></td></tr>
				
		 </table>";
		 
		 
//---------------------------------------------Attendance-----------------------------------

	$sel_term_for_attendance=$dbobj->select("select *  from `terms` where `classno`='".$classno."' and `exam_type`='term' "); 
$i=0;
while($att_term_row=$dbobj->fetch_array($sel_term_for_attendance))
   {
	   $termid[$i]=$att_term_row['id'];
	   $t=$att_term_row['id'];
	   $t_name.="<td><b>".$att_term_row['term_name']."</b></td>";
	   $p=0;
	   $sel_attandance=$dbobj->select("select *  from `temp_attendace` where `sid`='".$sid."' and `term`='".$t."' and `classid`='".$class."'");
       $attandance_row=$dbobj->fetch_array($sel_attandance);
		$working_days[$t]="<td>".$attandance_row['working_days']."</td>";
		$attendance[$t]="<td>".$attandance_row['attendance']."</td>";
		if($attandance_row['working_days']!="0" && $attandance_row['working_days']!="")
		{
			$p=round(($attandance_row['attendance']/$attandance_row['working_days'])*100,2);
		}
		$percentage[$t]="<td>".$p."</td>";
		$total_attendance=$total_attendance+$attandance_row['attendance'];
		$total_workingdays=$total_workingdays+$attandance_row['working_days'];
		$i++;
   }
   $att.="<table align='center' width='100%' height='100%'><tr><td><b>COMPONENTS</b></td>".$t_name."</tr>";
   if($total_workingdays!="0" && $total_workingdays!="")
   {
	$total_per=round(($total_attendance/$total_workingdays)*100,2);
   }
$att.="<tr><td style='height:30px'><b>WORKING DAYS</b></td>";
foreach ($termid as $tid)
   {
	  $att.=$working_days[$tid];
   }
$att.="</tr>";
$att.="<tr><td style='height:30px'><b>ATTENDED DAYS</b></td>";
foreach ($termid as $tid)
   {
	  $att.=$attendance[$tid];
   }

$att.="</tr>";
$att.="<tr><td><b>Percentage Of Attendance </b></td>";
foreach ($termid as $tid)
   {
	  $att.=$percentage[$tid];
   }
	$att.="</tr>
		<tr><td><b>Total Attendance</b></td><td colspan='3' align='center'>".$total_attendance."</td></tr>
		<tr><td><b>Percentage</b></td><td colspan='3' align='center'>".$total_per."</td></tr>";
$att.="</table>";
//----------------------------------Grade Calculate------------------------------------------
function grade_calculate($val,$val2,$classno)
{
	if($val!="" && $val2!="" && $val!="0" && $val2!="0")
	{
	$ma=($val/$val2)*100;
	$sel = mysql_query("select * from grades where classno = '$classno'");
	while($rowsel = mysql_fetch_array($sel))
		{
			$min_mark = $rowsel['min_mark'];
			$min_mark = $min_mark.".00";
			$max_mark = $rowsel['max_mark'];
			$max_mark = $max_mark."00";
			$gradename = $rowsel['gradename'];
			if($ma>=$min_mark && $ma<=$max_mark )
			{
				return $gradename;
			}
		}
	}
	
}
?>
<html>
<head>
<link rel="stylesheet" href="../css/progress_final.css" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>ecMS-Report Book</title>
<script type="text/javascript" language="javascript">

function print_page()
{
	window.print();
}

</script>
</head>
<body style="background-color:#FFFFFF">
<div align="right"><input type="button" value="Print" onclick="window.location.href='progressreport_pdf.php?sele_class=<?php echo $_GET['sele_class'];?>&sel_student=<?php echo $_GET['sel_student']; ?>&sel_exam=<?php echo $_GET['sel_exam']; ?>'" /></div>
<center>
<table><tr><td align="center" rowspan="8" ><img src='../img/donbosco.jpg' style="width:120px" /></td><td colspan="2" style="font-size:25px;"><b><?php echo strtoupper($school); ?></h></td></tr>
	<tr>
		<td align='center' colspan="2">(I.C.S.E & I.S.C-KE 051/2000)</td>
	</tr>	
	<tr>
		<td align='center' colspan="2">Vaduthala, Cochin-682 023</td>
	</tr>	
	<tr>
		<td align='center' colspan="2">Phone:H.S:2435308, L.P: 2436738, Principal:2436602</td>
	</tr>	
	<tr>
		<td align='center' colspan="2">e-mail: sdbschool@gmail.com, info@donboscoschoolvaduthala.com</td>
	</tr>
	<tr>
		<td align='center' colspan="2">web : www.donboscoschoolvaduthala.com</td>
	</tr>

<tr><td align="center" colspan="2" style="font-size:25px;><b>PROGRESS REPORT</b></td></tr>
	<tr>
		<td align='center' colspan="2"><b><?php echo $academic_year; ?></b<</td>
	</tr>
</table>
<table>
	
	<tr>
		
	</tr>
	
	
</table>
<hr style="border:dotted; border-color:#666666;width:100%">
<div>
<table width="50%" align="center" border="1">
<tr><td align="center" rowspan="7">
<?php
if($stud_photoid!="0")
{
	$sel_photo=$dbobj->select("select * from `student_img` where `id`='".$stud_photoid."'");
	$photo_row=$dbobj->fetch_array($sel_photo);
	$imagebytes=$photo_row['image'];
	$type=$photo_row['type'];
		if($type=='')
		{
		$image= "<img  boreder='0' src='../imagetmp/default.jpg' width=130 height=130 alt=Photo />";
		}
		else
		{
			$image="<img  border='0' src='data:image/jpeg;base64,".base64_encode($imagebytes)."' width='130' height='130' alt='Photo' />";
		}
}
else
{
	$image= "<img  boreder='0' src='../imagetmp/default.jpg' width='130' height='130' alt='Photo' />";
}
	echo $image;
	?>
</td></tr>
<tr><td colspan="2" ><b>Name of the Student:</b> <?php echo $stname." ".$initial; ?></td></tr>
<tr><td><b>Date of Birth:</b></td></tr>
<tr><td><b>Class:</b> <?php echo $classname."     "?><b>Division:</b>  <?php echo $division .".    "; ?></td></tr>
<tr><td><b>ID No:</b><?php echo $studentid."    "; ?> <b>House:</b> </td></tr>
<tr><td><b>Father's/Guardian's Name:</b><?php echo $parent['fname']?></td></tr>
<tr><td><b>Mothers Name:</b><?php echo $parent['mname']?></td></tr>
</table>
</div>
<!--<span><a href="home.php"><img  border="0" src="images/home.png" alt="Home" width="90" height="36" /></a></span>
--></center>

<div id="front_page">
<div align="right"></div>
<?php if($main_page=='true'){echo $front_page;} ?>
</div>
<div>
<table align="center" >
<tr><th style="text-align:center" colspan="2"><b>ACADEMIC PERFORMANCE</b> </th></tr>
<tr><td colspan="2" valign="top">
<?php
echo $form;
//echo $rem;
?>
</td><tr><th style="text-align:center"><b>NON-ACADEMIC PERFORMANCE</b> </th><th style="text-align:center"><b>NON ACADEMIC PERFORMANCE</b></th></tr>
<tr ><td valign="top" width="60%"><table width="100%">
	<tr><th style='text-align:left;'>COMPONENTS</th><th>I TERM</th><th>II TERM</th><th>III TERM</th><th style='text-align:left;'>COMPONENTS</th><th>I TERM</th><th>II TERM</th><th>III TERM</th></tr>
	<tr><td><b>CONDUCT</b></td><td><?php echo $nonacc['CONDUCT']['I Term'];?></td><td><?php echo $nonacc['CONDUCT']['II Term'];?></td><td><?php echo $nonacc['CONDUCT']['III Term'];?></td><td><b>SPEAKING ENGLISH</b></td><td><?php echo $nonacc['SPEAKING ENGLISH']['I Term'];?></td><td><?php echo $nonacc['SPEAKING ENGLISH']['II Term'];?></td><td><?php echo $nonacc['SPEAKING ENGLISH']['III Term'];?></td></tr>
																									<tr><td><b>RESPONSIBILITY</b></td><td><?php echo $nonacc['RESPONSIBILITY']['I Term'];?></td><td><?php echo $nonacc['RESPONSIBILITY']['II Term'];?></td><td><?php echo $nonacc['RESPONSIBILITY']['III Term'];?></td><td><b>ECA</b></td><td><?php echo $nonacc['ECA']['I Term'];?></td><td><?php echo $nonacc['ECA']['II Term'];?></td><td><?php echo $nonacc['ECA']['III Term'];?></td></td></tr>
																									<tr><td><b>PUNCTUALITY</b></td><td><?php echo $nonacc['PUNCTUALITY']['I Term'];?></td><td><?php echo $nonacc['PUNCTUALITY']['II Term'];?></td><td><?php echo $nonacc['PUNCTUALITY']['III Term'];?></td><td><b>AQUATICS</b></td><td><?php echo $nonacc['AQUATICS']['I Term'];?></td><td><?php echo $nonacc['AQUATICS']['II Term'];?></td><td><?php echo $nonacc['AQUATICS']['III Term'];?></td></tr>
																									<tr><td><b>DISCIPLINE</b></td><td><?php echo $nonacc['DISCIPLINE']['I Term'];?></td><td><?php echo $nonacc['DISCIPLINE']['II Term'];?></td><td><?php echo $nonacc['DISCIPLINE']['III Term'];?></td><td><b>SUPW & C.SERVICE</b></td><td><?php echo $nonacc['SUPW & C.SERVICE']['I Term'];?></td><td><?php echo $nonacc['SUPW & C.SERVICE']['II Term'];?></td><td><?php echo $nonacc['SUPW & C.SERVICE']['III Term'];?></td></tr>
																									<tr><td><b>LEADERSHIP</b></td><td><?php echo $nonacc['LEADERSHIP']['I Term'];?></td><td><?php echo $nonacc['LEADERSHIP']['II Term'];?></td><td><?php echo $nonacc['LEADERSHIP']['III Term'];?></td><td><b>GROUPS & MOVEMENTS</b></td><td><?php echo $nonacc['GROUPS & MOVEMENTS']['I Term'];?></td><td><?php echo $nonacc['GROUPS & MOVEMENTS']['II Term'];?></td><td><?php echo $nonacc['GROUPS & MOVEMENTS']['III Term'];?></td></tr>
																									</table></td><td valign="top">
<table  style='border-collapse:collapse;'  border='1' width="100%">
<tr><th colspan='5' style='font-size:14px;height:33px;text-align:center'><b>ATTENDANCE</b></th></tr>
<tr><th style='text-align:left;'>COMPONENTS</th><th>I TERM</th><th>II TERM</th><th>III TERM</th><th>TOTAL</th></tr>
<tr><td style='font-size:14px;height:30px;'><b>WORKING DAYS</b></td>
<?php
$sel_at_term=$dbobj->selectall("terms",array("term_name"=>"I TERM","classno"=>$classno));
$smonth="2015-06-01";
$emonth="2015-08-01";
$sel_working=$dbobj->select("select distinct `date` from `attendence` where `date` >= '".$smonth."' AND `date` <= '".$emonth."'");
$work_days=mysql_num_rows($sel_working);
 ?>
 <td><?php echo $work_days;?></td>
<td></td><td></td><td></td></tr>
<?php
$sel_att=$dbobj->select("select * from `attendence` where `date` >= '".$smonth."' AND `date` <= '".$emonth."' and `status`='Present' and `studentid`='".$studentid."' and `classid`='".$class."'");
$attended_days=mysql_num_rows($sel_att);
?>
<tr><td style='font-size:14px;height:30px;'><b>ATTENDED DAYS</b></td><td><?php echo $attended_days;?></td><td></td><td></td><td></td></tr>
<tr><td style='font-size:14px;height:30px;'><b>PERCENTAGE</b></td><td><?php echo round(($attended_days/$work_days)*100,2)?></td><td></td><td></td><td></td></tr>
</table>
</td></tr>
<tr>
<td colspan="2">
<?php
echo $rem;
?>
</td></tr>
<tr>
<td colspan="2">
<?php
echo $lastpage;
?>
</td></tr>
</table>
</div>
</body>

</html>
