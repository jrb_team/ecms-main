<?php
class Salary
{
	function get_basic($type,$basic)
	{
			if($type=="daily_wage")
			{
				$basic=0;
			}
			else
			{
				$basic=$basic;
			}	
			return $basic;
	}
	function get_salary($type,$basic)
	{
			if($type=="daily_wage")
			{
				$salary=($basic)*30;
			}
			else
			{
				$salary=$basic;
			}	
			return $salary;
	}
	
	function get_hra($salary,$hra_val,$type)
	{
		if($type=="daily_wage")
		{
			$hra=0;
		}
		else
		{
			$hra=round(($salary*$hra_val)/100,0);
		}
		return $hra;
	}
	function get_pf($pre_pf,$total,$pf_val)
	{
		if($pre_pf!=0)
		{
				if($total>15000)
				{
					$pf=round((15000*$pf_val)/100,0);
				}
				else
				{
					$pf=round(($total*$pf_val)/100,0);
				}
		}
		else
		{
		$pf=0;
		}
		return $pf;
	}
	function get_ipf($pre_pf,$total,$ipf_val)
	{
		if($pre_pf!=0)
		{
				if($total>15000)
				{
					$ipf=round((15000*$ipf_val)/100,0);
				}
				else
				{
					$ipf=round((15000*$ipf_val)/100,0);
				}
		}
		else
		{
		$ipf=0;
		}
		return $ipf;
	}
	function get_esi($esit,$gross,$esi_val)
	{
		if($esit=="1")
			{
			$esi=($gross*$esi_val)/100;
			$tesi=floor($esi);
			$esirem=($esi - $tesi);
			if($esirem > 0.09)
			{
				$esi=ceil($esi);
			}
			else
			{
				$esi=round($esi,0);
			}
			}
			else
			{
				$esi=0;
			}
			return $esi;
			
	}
	function get_iesi($esit,$gross,$esi,$iesi_val)
	{
		if($esit=="1")
			{
			$iesi=(round(($gross*4.75)/100,2))+$esi;
			}
			else
			{
			$iesi=0;
			}
		return $iesi;
	}
	
	 function getNumberOfWorkingYears($joindYear)
	 {
			$date=date('t-m-Y');
			$datetime1 = new DateTime($date);
			$datetime2 = new DateTime($joindYear);
			$interval = $datetime1->diff($datetime2);
			return $interval->format('%y.%m');
	 }
	 function get_fa($work_year,$month,$staff_type)
	 {
	 if($month=="December")
	 {
		if($staff_type=="regular")
		{
			if($work_year>=0 && $work_year<3)
			{
			$fa=3000;
			}
			elseif($work_year>=3 && $work_year<7)
			{
			$fa=5000;
			}
			elseif($work_year>7 && $work_year<12)
			{
			$fa=7000;
			}
			elseif($work_year>12 && $work_year<17)
			{
			$fa=9000;
			}
			elseif($work_year>=17)
			{
			$fa=11000;
			}
		}
		else
		{
		$fa=3000;
		}
	 }
	 else
	 {
	 $fa=0;
	 }
	 return $fa;
	 }
	
}
?>