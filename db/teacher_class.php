<?php
include_once('createdb.php');
class Teacher
{
	function get_photo($tid)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$src= "src='img/people-img1.png'";
		return $src;
	}
	 function get_teacher_place($userid)
	 {
		$dbobject = new DB();
		$dbobject->getCon();
		$acyear=$dbobject->get_acyear();
		$sql=$dbobject->select("select distinct `place` from `teacher_subject` where `teacherid`='".$userid."' and `acyear`='".$acyear."'");
		$i=0;
		while($row=$dbobject->fetch_array($sql))
		{
			$place[$i]=$row['place'];
			$i++;
		}
		return $place;
	 }
	 function get_teacher_incharge($classid)
	 {
		$dbobject = new DB();
		$dbobject->getCon();
		$acyear=$dbobject->get_acyear();
		$sql=$dbobject->select("select * from `teacher_subject` where `classid`='".$classid."'and `acyear`='".$acyear."'and 'Y' IN (`classteacher`,`associateteacher`)");
		$i=0;
		while($row=$dbobject->fetch_array($sql))
		{
			$teacherid[$i]=$row['teacherid'];
			$i++;
		}
		return $teacherid;
	 }
	 function get_teacher_name_by_id($tid)
	 {
		$dbobject = new DB();
		$dbobject->getCon();
		//$acyear=$dbobject->get_acyear();
		$sql=$dbobject->select("select * from `teacher` where `userid`='".$tid."'");
		$thr= $dbobject->fetch_array($sql);
	    $teacher=$thr['name'];
		return $teacher;
	 }
	 function get_teacher_inchargeclass($userid)
	 {
		$dbobject = new DB();
		$dbobject->getCon(); 
		$acyear=$dbobject->get_acyear();
		$sql=$dbobject->select("select distinct `sclass`.`classid`,`sclass`.classname,`sclass`.division,`sclass`.classno from `sclass` LEFT JOIN `teacher_subject` ON `sclass`.`classid`=`teacher_subject`.`classid` where `sclass`.`classid`!='-1' and `teacher_subject`.`teacherid`='".$userid."' and `acyear`='".$acyear."' and 'Y' IN (classteacher,associateteacher) order by `sclass`.`classno`,`sclass`.`division`");
		$i=0;
		while($row=$dbobject->fetch_array($sql)){
			$class[$i]['classid']=$row['classid'];
			$class[$i]['classname']=$row['classname'];
			$class[$i]['division']=$row['division'];
			$class[$i]['classno']=$row['classno'];
			$i++;
		}
		return $class;
	 }
	 function get_classlist($userid,$place)
	 {
		$dbobject = new DB();
		$dbobject->getCon(); 
		$acyear=$dbobject->get_acyear();
		$sql=$dbobject->select("select distinct `teacher_subject`.`classid`,`sclass`.`classname`,`sclass`.`division`,`sclass`.`classno` from `teacher_subject` LEFT JOIN `sclass` ON `teacher_subject`.`classid`=`sclass`.`classid` where `teacherid`='".$userid."' and `acyear`='".$acyear."' and `teacher_subject`.`classid`!='' order by `sclass`.`classno`,`sclass`.`division`");
		$i=0;
		while($row=$dbobject->fetch_array($sql)){
			$class[$i]['classid']=$row['classid'];
			$class[$i]['classname']=$row['classname'];
			$class[$i]['division']=$row['division'];
			$class[$i]['classno']=$row['classno'];
			$i++;
		}
		return $class;
     }
     function GetAttendanceClasses($userid,$classno,$acyear){
        $dbobject = new DB();
        $dbobject->getCon(); 
        $classes=array();
        $i=0;
        $teacher_subject_attn=$dbobject->select("select DISTINCT `classid` FROM `teacher_subject`  WHERE `teacherid`='".$userid."' and `acyear`='".$acyear."' and `classno`='".$classno."' AND `type`='0' and `subject`=''");
                while( $row=$dbobject->fetch_array($teacher_subject_attn))
                {
                    $sclass_det=$dbobject->selectall("sclass",array("classid"=>$row['classid']));
                    $classes[$i]['classid']=$sclass_det['classid'];
                    $classes[$i]['classname']=$sclass_det['classname'];
                    $classes[$i]['division']=$sclass_det['division'];
                    $i++;
                }
                return $classes;
     }
function GetAttendanceClassesByuserid($userid){
        $dbobject = new DB();
        $dbobject->getCon(); 
        $acyear=$dbobject->get_acyear(); 
        $classes=array();
        $i=0;
        $teacher_subject_attn=$dbobject->select("select * FROM `teacher_subject`  WHERE `teacherid`='".$userid."' and `acyear`='".$acyear."'  AND `type`='0' and `subject`=''");
                while( $row=$dbobject->fetch_array($teacher_subject_attn))
                {
                    $sclass_det=$dbobject->selectall("sclass",array("classid"=>$row['classid']));
                    $classes[$i]['classid']=$sclass_det['classid'];
                    $classes[$i]['classno']=$sclass_det['classno'];
                    $classes[$i]['classname']=$sclass_det['classname'];
                    $classes[$i]['division']=$sclass_det['division'];
                    $i++;
                }
                return $classes;
     }
	 function get_subjectByClassid($classid,$acyear,$place)
	 {
		$dbobject = new DB();
		$dbobject->getCon(); 
		$acyear=$dbobject->get_acyear(); 
		$sql=$dbobject->select("select * from `teacher_subject`  where  `acyear`='".$acyear."' and `place`='".$place."' and `teacher_subject`.`classid`='".$classid."' and `teacher_subject`.`subject`!='0' and `teacher_subject`.`subject`!='-1'");
		$i=0;
		while($row=$dbobject->fetch_array($sql)){
			$subject[$i]['type']=$row['type'];
			$subject[$i]['subject']=$row['subject'];
			$i++;
		}
		return $subject;
	 }
function sel_subject($sub,$tablename)
{			
				$dbobject = new DB();
				$rest = substr($sub,0, 3);
				if($rest=="sub")
				{
					$subid = explode($rest,$sub);
					$ss=$dbobject->selectall($tablename,array("sub_id"=>$subid[1]));
					$subject=$ss['subject'];
				}
				elseif($rest=="ext")
				{
					$subid = explode($rest,$sub);
					$ss=$dbobject->selectall("extra_subject",array("id"=>$subid[1]));
					$subject=$ss['subject'];
				}
				elseif($rest=="opn")
				{
					$subid = explode($rest,$sub);
					$ss=$dbobject->selectall("opn_subjects",array("id"=>$subid[1]));
					$subject=$ss['opn_sub'];
				}
				else
				{
					$subject=$sub;
				}
				return $subject;
}
function get_SubjectBy_tid($tid,$acyear)
{
	$dbobject = new DB();
	$dbobject->getCon();
	$i=0;
	$sel=$dbobject->select("select * from `teacher_subject` where `teacherid`='".$tid."' and `acyear`='".$acyear."'");
	while($row=$dbobject->fetch_array($sel))
	{
		$subject[$i]['type']=$row['type'];
		$subject[$i]['subid']=$row['subject'];
		$subject[$i]['classno']=$row['classno'];
		$i++;
	}
	return $subject;
}
	function subject_teacherByclassid($classid,$acyear)
	{
	$dbobject = new DB();
	$dbobject->getCon();		
		$sql="select distinct `teacher_subject`.`teacherid`,`teacher`.* from `teacher_subject` left join `teacher` on `teacher`.`userid`=`teacher_subject`.`teacherid` where  ";
		$sql.="`teacher_subject`.`acyear`='".$acyear."' and ";
		$sql.="`teacher_subject`.`classid`='".$classid."' and `teacher_subject`.`subject`!='0' and `teacher_subject`.`subject`!='-1'";
		$qry=$dbobject->select($sql);
	    $i=0;
		while($row=$dbobject->fetch_array($qry))
		{
			
			$teacher[$i]['tid']=$row['id'];		
			$teacher[$i]['name']=$row['name']." ".$row['lname'];
			$teacher[$i]['userid']=$row['userid'];	
			$teacher[$i]['group']=$row['group'];	
            $i++;			
		}
		return $teacher;		
    }
    function get_StaffList(){
        $dbobject = new DB();
        $dbobject->getCon();
        $sql="select * from  `teacher` WHERE `status`='1' and `group`!='admin' order by `name` ";
        $qry=$dbobject->select($sql);
        $i=0;
        $teacher=array();
		while($row=$dbobject->fetch_array($qry))
		{
            $teacher[$i]['id']=$row['id'];
            $teacher[$i]['userid']=$row['userid'];
			$teacher[$i]['imgid']=$row['imgid'];	
			$teacher[$i]['name']=$row['name'];	
			$teacher[$i]['middle_name']=$row['middle_name'];	
			$teacher[$i]['pref_name']=$row['pref_name'];
            $teacher[$i]['lname']=$row['lname'];
            $teacher[$i]['staff_name']=ucfirst($row['name']." ".$row['lname']);
             $teacher[$i]['mob']=$row['mob'];
            $teacher[$i]['email']=$row['email'];

            $i++;
        }
        return $teacher;
    }
function get_StaffListByStatus($status){
        $dbobject = new DB();
        $dbobject->getCon();
        $sql="select * from  `teacher` WHERE `status`='".$status."' and `group`!='admin' order by `name` ";
        $qry=$dbobject->select($sql);
        $i=0;
        $teacher=array();
		while($row=$dbobject->fetch_array($qry))
		{
            $teacher[$i]['id']=$row['id'];
            $teacher[$i]['userid']=$row['userid'];
			$teacher[$i]['imgid']=$row['imgid'];	
			$teacher[$i]['name']=$row['name'];	
			$teacher[$i]['middle_name']=$row['middle_name'];	
			$teacher[$i]['pref_name']=$row['pref_name'];
            $teacher[$i]['lname']=$row['lname'];
            $teacher[$i]['staff_name']=ucfirst($row['name']." ".$row['lname']);
             $teacher[$i]['mob']=$row['mob'];
            $teacher[$i]['phone']=$row['phone'];
			$teacher[$i]['email']=$row['email'];

            $i++;
        }
        return $teacher;
    }
	function staff_ListByAdmin()
	{
	$dbobject = new DB();
	$dbobject->getCon();
		$sql="select `teacher_list`.`date`,`teacher`.* from `teacher_list` left join `teacher` on `teacher`.`id`=`teacher_list`.`tid` order by `teacher_list`.`id`";
		
		$qry=$dbobject->select($sql);
	    $i=0;
		while($row=$dbobject->fetch_array($qry))
		{
			
			$teacher[$i]['tid']=$row['id'];		
			$teacher[$i]['name']=$row['name'];	
			$teacher[$i]['lname']=$row['lname'];	
			$teacher[$i]['userid']=$row['userid'];	
			$teacher[$i]['group']=$row['group'];	
            $i++;			
		}
		return $teacher;		
	}	
function teacher_subByclassid($subtype,$subid,$classid,$acyear)
	{
	    $dbobject = new DB();
	    $dbobject->getCon();
        if($subtype=="main")
		{
			$type="subject";
		}
        elseif($subtype=="opn")
		{
			$type="addtional";
		}		
	$sql="SELECT * FROM `teacher_subject` WHERE `classid`='".$classid."' and `subject`='".$subid."' and `type`='".$type."' and `acyear`='".$acyear."'";
		$qry=$dbobject->select($sql);
	   // $i=0;
		while($row=$dbobject->fetch_array($qry))
		{
			
			$teacher['teacherid']=$row['teacherid'];		
			$teacher['subject']=$row['subject'];	
			$teacher['type']=$row['type'];	
			$teacher['classteacher']=$row['classteacher'];	
            //$i++;			
		}
		return $teacher;		
	}	
function subject_time_table($subtype,$subid,$classid,$acyear)
{
	$dbobject = new DB();
	$dbobject->getCon();
        if($subtype=="main")
		{
			$type="subject";
		}
        elseif($subtype=="opn")
		{
			$type="addtional";
		}	
	$sql="SELECT * FROM `timetable` WHERE `classid`='".$classid."' and `subject`='".$subid."' and `subject_type`='".$type."' and `acyear`='".$acyear."' order by `period`";
	$qry=$dbobject->select($sql);
	$i=0;
	while($row=$dbobject->fetch_array($qry))
	{
		$i++;
		$det[$i]['day']=$row['day'];
		$det[$i]['period']=$row['period'];
		$det[$i]['subject']=$row['subject'];
		$det[$i]['subject_type']=$row['subject_type'];			
	}	
	return $det;
}	
function teacheridBysub($classid,$acyear,$subid,$subtype)
{
		$dbobject = new DB();
		$dbobject->getCon(); 
		if($subtype=="main")
		{
			$type="subject";
		}
		else
		{
			$type="addtional";
		}
		$qry=$dbobject->select("SELECT * FROM `teacher_subject` WHERE `classid`='".$classid."' and `acyear`='".$acyear."' and `subject`='".$subid."' and `type`='".$type."'");
		$row=$dbobject->fetch_array($qry);
	return 	$row['teacherid'];
}
function class_teacherbyclassid($classid)
{
$dbobject = new DB();
$acyear=$dbobject->get_acyear();
//echo "select * from `teacher_subject` where `classteacher`='Y' and `acyear`='".$acyear."' and `classid`='".$classid."'";	
$qry=$dbobject->select("select * from `teacher_subject` where `classteacher`='Y' and `acyear`='".$acyear."' and `classid`='".$classid."'");
$row=$dbobject->fetch_array($qry);
return $row['teacherid'];	
}
	function Teacher_list()
	 {
		$dbobject = new DB();
		$dbobject->getCon(); 
		$sql=$dbobject->select("select * from  `teacher` where `status`='1'  order by `name`");
		$i=0;
		while($row=$dbobject->fetch_array($sql))
		{
			$teacher[$i]['id']=$row['id'];
			$teacher[$i]['userid']=$row['userid'];
			$teacher[$i]['name']=$row['name'];
			$teacher[$i]['lname']=$row['lname'];
			$i++;
		}
		return $teacher;
	 }
function teacher_period_class($userid,$day,$pid,$acyear)
{
	$dbobject = new DB();
	$dbobject->getCon(); 
	$teacher_det=$dbobject->selectall("teacher",array("userid"=>$userid));	
	$tid=$teacher_det['id'];
	$teacher_period_class_data=$this->teacher_period_class_data($tid,$day,$pid,$acyear);
    $data=$teacher_period_class_data[0];	
	if(!empty($data))
				{
					$i=0;
					foreach($data as $classname=>$sub)
					{
						if(!empty($sub))
							{
								foreach($sub as $s)
								{
								$det[$i]['class']=$classname;
								$det[$i]['subject']=$s;
								$i++;											
								}
							}
						
					}
				}
				return $det;
}
function teacher_period_class_data($tid,$day,$pid,$acyear)
{
	$dbobject = new DB();
	$dbobject->getCon(); 
	$tid=5;
	$sel_period=$dbobject->select("select * from `timetable` where `day`='".$day."' and `period`='".$pid."' and `acyear`='".$acyear."' and `subject_teacher`='".$tid."'");
	$num_rows=mysql_num_rows($sel_period);
	$i=0;
	while($row=$dbobject->fetch_array($sel_period))
	{
		$subid=$row['subject'];
		$type=$row['subject_type'];
		$subject_data=$type."-".$subid;
		$classid=$row['classid'];
		$classdet=$dbobject->selectall("sclass",array("classid"=>$row['classid']));
		include_once('admin_class.php');
		$admin = new Admin($classdet['classno']);
		$tablename=$admin->subject_table();						 
		$subject=$dbobject->sel_subject($subject_data,$tablename);
		include_once('sub_name.php');
		$subname=new Subject($subject);
		$sub=$subname->sub_table();
	    $data_temp[$classdet['classname']." ".$classdet['division']][$sub]=$sub;
	    $data_temp2[$row['id']]['class']=$classdet['classname']." ".$classdet['division'];
	    $data_temp2[$row['id']]['subject']=$sub;
	}	
	$data[0]=$data_temp;
	$data[1]=$data_temp2;
	return $data;
}
function teacher_class_graph($userid)
{
$dbobject = new DB();
$acyear=$dbobject->get_acyear();
$qry=$dbobject->select("select * from `teacher_subject` where `classteacher`='Y' and `teacherid`='".$userid."' and `acyear`='".$acyear."'");
$row=$dbobject->fetch_array($qry);
return 4;
}
	function Staff_photo($id)
	{
		$dbobject = new DB();
		$dbobject->getCon();
		$sel_photo=$dbobject->select("select * from `staff_img` where `id`='".$id."'");
		$photo_row=$dbobject->fetch_array($sel_photo);
		$imagebytes=$photo_row['image'];
		$type=$photo_row['type'];
		if($type=='' || $imagebytes=="")
		{
			$src="http://www.ecms-erp.com/ecms/img/people-img1.png";
		}
		else
		{
			//$src="data:image/jpeg;base64,".base64_encode($imagebytes)."";
			$src="http://www.ecms-erp.com/ecms/img/people-img1.png";
		}
		return $src;
	}
	 	 function get_teacher_subject_byclass($userid,$classid,$subject_table)
	 {
		$dbobject = new DB();
		$dbobject->getCon(); 
		$acyear=$dbobject->get_acyear();
		$sql=$dbobject->select("SELECT DISTINCT subject,classid,type FROM `teacher_subject` WHERE `teacherid`='".$userid."'and `classid`='".$classid."' and `acyear`='".$acyear."'");
		$i=0;
	    while($row=$dbobject->fetch_array($sql))
	    {
			if($row['type']=="subject")
			{
				$subject[$i]['id']="main-".$row['subject'];
				$query="SELECT * FROM `".$subject_table."` WHERE `sub_id`='".$row['subject']."'";
			}
			elseif($row['type']=="addtional")
			{
				$subject[$i]['id']="opn-".$row['subject'];
				$query="SELECT * FROM `opn_subjects` WHERE `id`='".$row['subject']."'";
			}
					
			$sqll=mysql_query($query);
			$row1=$dbobject->fetch_array($sqll);
			if($row['type']=="subject")
			{
			 $subject[$i]['subject']=$row1['subject'];
			}
			elseif($row['type']=="addtional")
			{
				$subject[$i]['subject']=$row1['opn_sub'];
			}
			$i++;
	    }
		return $subject; 
      }
function get_SubjectBy_tid_classid($tid,$classid,$acyear)
{
	$dbobject = new DB();
	$dbobject->getCon();
	$i=0;
	//$sel=$dbobject->select("select * from `teacher_subject` where `teacherid`='".$tid."' and `classid`='".$classid."' and `acyear`='".$acyear."'");
	$sel=$dbobject->select("select * from `teacher_subject` where `teacherid`='".$tid."'  and `acyear`='".$acyear."'");
	while($row=$dbobject->fetch_array($sel))
	{
		$admin=new Admin($row['classno']);
		$subject_table=$admin->subject_table();
		$subject[$i]['type']=$row['type'];
		$subject[$i]['subid']=$row['subject'];
		$subject[$i]['classno']=$row['classno'];
				if($sub['type']=="subject")
				{
					$subject_det=$dbobject->selectall("$subject_table",array("sub_id"=>$row['subject']));
					$subject[$i]['subname']=$subject_det['subject'];
				}
				if($sub['type']=="addtional")
				{
					$subject_det=$dbobject->selectall("opn_subjects",array("id"=>$row['subject']));
					$subject[$i]['subname']=$subject_det['opn_sub'];
				}
				if($sub['type']=="extrasubject")
				{
					$subject_det=$dbobject->selectall("extra_subject",array("id"=>$row['subject']));
					$subject[$i]['subname']=$subject_det['subject'];
				}				
		$i++;
	}
	return $subject;
}
	 function get_subjectByClassid2($classid,$acyear)
	 {
		$dbobject = new DB();
		$dbobject->getCon(); 
		if($acyear=="" || $acyear=="-1")
		{
		$acyear=$dbobject->get_acyear();	
		}
	//	echo "select distinct `type`,`subject` from `teacher_subject`  where  `acyear`='".$acyear."' and `teacher_subject`.`classid`='".$classid."' and `teacher_subject`.`subject`!='0' and `teacher_subject`.`subject`!='-1'";
		$sql=$dbobject->select("select * from `teacher_subject`  where  `acyear`='".$acyear."' and `teacher_subject`.`classid`='".$classid."' and `teacher_subject`.`subject`!='0' and `teacher_subject`.`subject`!='-1' order by `type`,`subject`");
		$i=0;
		while($row=$dbobject->fetch_array($sql)){
			$subject[$i]['type']=$row['type'];
			$subject[$i]['subject']=$row['subject'];
			$subject[$i]['teacherid']=$row['teacherid'];
			$subject[$i]['no_of_period']=$row['no_of_period'];
			$i++;
		}
		return $subject;
	 }
function Alloted_Periode($day,$periode,$teacher,$acyear,$tablename)
{
	$dbobject = new DB();
	$dbobject->getCon();
	$i=0;
	
	//echo "select * from `timetable`  where `day`='".$day."' and `period`='".$periode."' and `subject_teacher`='".$teacher."' and `acyear`='".$acyear."'";
	//echo "select * from `timetable`  where `day`='".$day."' and `period`='".$periode."' and `subject_teacher`='".$teacher."' and `acyear`='".$acyear."'";
	$sel=$dbobject->select("select * from `timetable`  where `day`='".$day."' and `period`='".$periode."' and `subject_teacher`='".$teacher."' and `acyear`='".$acyear."'");
	while($row=$dbobject->fetch_array($sel))
	{
		$classid=$row['classid'];
		$class_det=$dbobject->selectall("sclass",array("classid"=>$classid));
		$data[$i]['class']=$class_det['classname']." ".$class_det['division'];
		$data[$i]['classid']=$class_det['classid'];
		$subject_data=$row['subject_type']."-".$row['subject'];
		$subname=$dbobject->sel_subject($subject_data,$tablename);
		$data[$i]['subject']=$subname;
		$i++;
	}
	return $data;
}
	function Teacher_attendance_hedding($seltype,$seltype2,$period,$period_type,$period_type2,$acyear)
	 {	
		if($period=="today")
		{
			$first_second = date('Y-m-d', strtotime($period_type));
			$last_second  = date('Y-m-d', strtotime($period_type)); // A leap year!	
        $headding="<center><h4>Attendance Report  ".date("j F, Y", strtotime($period_type))."</h4></center>"; 		
		}
		if($period=="Full_year")
		{
        $headding="<center><h4>Attendance Report For ".$acyear."</h4></center>"; 		
		}
		if($period=="month")
		{
			$year = date("Y"); 	
			$rmonth = ucfirst($period_type); 	
			$timestamp    = strtotime(''.$rmonth.' '.$year.'');
			$first_second = date('Y-m-01', $timestamp);
			$last_second  = date('Y-m-t', $timestamp); // A leap year!
	  $headding="<center><h4>Attendance Report From ".date("j F, Y", strtotime($first_second))." To ".date("j F, Y", strtotime($last_second))."</h4></center>";
		}
		if($period=="Custom")
		{
			$first_second = date('Y-m-d',strtotime($period_type));
			//echo "<br>";
			$last_second  = date('Y-m-d',strtotime($period_type2)); // A leap year!
	  $headding="<center><h4>Attendance Report From ".date("j F, Y", strtotime($first_second))." To ".date("j F, Y", strtotime($last_second))."</h4></center>";		
		}		
if($period=="date")
{
			$first_second = date('Y-m-d',strtotime($period_type));
			//echo "<br>";
			$last_second  = date('Y-m-d',strtotime($period_type2)); // A leap year!
	  $headding="<center><h4>Attendance Report From ".date("j F, Y", strtotime($first_second))." </h4></center>";		
}
		return $headding;
	 }
 function SubjectTeacher($classid,$subject_data,$acyear)
 {
	$dbobject = new DB();
	$dbobject->getCon(); 
	$i=0;
	$data=explode("-",$subject_data);
	$subtype=$data[0];
	$subid=$data[1];	 
	//echo "select * from `teacher_subject` where `type`='".$subtype."' and `subject`='".$subid."' and `classid`='".$classid."'";
	$sql="select * from `teacher_subject` where `type`='".$subtype."' and `subject`='".$subid."' and `classid`='".$classid."'";
	if($acyear!="" && $acyear!="-1")
	{
			$sql.=" and `acyear`='".$acyear."'";
	}
    // echo $sql;
	$sel=$dbobject->select($sql);
	while($row=$dbobject->fetch_array($sel))
	{

		$teacher_det=$dbobject->selectall("teacher",array("userid"=>$row['teacherid']));
		$teacher[$i]['id']=$teacher_det['id'];
		$teacher[$i]['name']=$teacher_det['name']." ".$teacher_det['tlname'];
		$i++;
	}
	return 	$teacher;
	
 }  
	function Teacher_list_BY_acyear($acyear)
	 {
		$dbobject = new DB();
		$dbobject->getCon(); 
		$sql=$dbobject->select("SELECT  distinct `teacherid` FROM `teacher_subject` WHERE  `acyear`='".$acyear."'");
		$i=0;
		while($row=$dbobject->fetch_array($sql))
		{
			$teacher_det=$dbobject->selectall("teacher",array("userid"=>$row['teacherid']));
			$teacher[$i]['id']=$teacher_det['id'];
			$teacher[$i]['userid']=$teacher_det['userid'];
			$teacher[$i]['name']=$teacher_det['name'];
			$i++;
		}
		return $teacher;
	 }
	 function GetDepAndProfile($staff_id){
		$dbobject = new DB();
		$dbobject->getCon(); 
		$query_portfolio= "SELECT * from `staff_department_portfolio` where ";
		$query_portfolio.="  `staff_id`='".$staff_id."' and `dept_id` NOT IN ('','0') order by `id` DESC LIMIT 1";
		
		$rs_portfolio=$dbobject->select($query_portfolio);	

		$row_portfolio=$dbobject->fetch_array($rs_portfolio);
		
		$dept_id =$row_portfolio['dept_id'];		
		$profile_id =$row_portfolio['profile_id'];
			
		$staff_profile_det=$dbobject->selectall("staff_subdivision",array("id"=>$profile_id));
		$category_det=$dbobject->selectall("staff_department",array("id"=>$staff_profile_det['dept_id']));
		$profile_data['department_name']=$category_det['department_name'];
		$profile_data['profile_name']=$staff_profile_det['subdivision'];
		return $profile_data;
	 }
	 function check_reception($tid)
	 {
		$dbobject = new DB();
		$dbobject->getCon();
		$dbobject = new DB();
		$dbobject->getCon();
		$teacher_det=$dbobject->selectall("teacher",array("userid"=>$tid));
		$group=$teacher_det['group'];
		if($group=="admin")
		{
		$chk_recp=1;
		return $chk_recp;	
		}
		else
		{
			if($tid=="LIAEIK")
			{
			$chk_recp=1;	
			return $chk_recp;
			}
			else
			{
		$chk_recption=$dbobject->select("SELECT * FROM `staff_department_portfolio` WHERE `staff_id` = '".$teacher_det['id']."' and `dept_id`='19'");
		return $chk_recp=mysql_num_rows($chk_recption);					
			}		
		
		}
	
	 }
	 function check_teacher($tid)
	 {
		$dbobject = new DB();
		$dbobject->getCon();
		$dbobject = new DB();
		$dbobject->getCon();
		$teacher_det=$dbobject->selectall("teacher",array("userid"=>$tid));
		$group=$teacher_det['group'];
		if($group=="admin")
		{
		$chk_recp=1;
		return $chk_recp;	
		}
		else
		{
			if($tid=="LIAEIK")
			{
			$chk_recp=1;
	return $chk_recp;		
			}
			else
			{
			$chk_recption=$dbobject->select("SELECT * FROM `staff_department_portfolio` WHERE `staff_id` = '".$teacher_det['id']."' and `dept_id`IN ('26','14','22','21','1')");
			$chk_recp=$dbobject->mysql_num_row($chk_recption);	
	return $chk_recp;			
			}		
		
		}
	
	 }
		function Teacher_attendance_count($seltype,$seltype2,$period,$period_type,$period_type2,$acyear)
	 {
		$dbobject = new DB();
		$dbobject->getCon(); 
		$get_StaffList=$this->get_StaffList();
		$Present=0;
		$Absent=0;
		$Late=0;		
		$str=0;	
		if($period=="today")
		{
			$first_second = date('Y-m-d', strtotime($period_type));
			$last_second  = date('Y-m-d', strtotime($period_type)); // A leap year!	
			 if(!empty($get_StaffList))
			 {

				 foreach($get_StaffList as $staff)
				 {
					 
					$tid=$staff['id'];
					$str++;
					$qry=$dbobject->select("SELECT * FROM `teacher_attendence` WHERE  `date`>='".$first_second."' and `date`<='".$last_second."' and `tid`='".$tid."' and `acyear`='".$acyear."'"); 
				    $check=$dbobject->mysql_num_row($qry);
					if($check!=0)
					{
					$Present++;	
					}
					else
					{
					$Absent++;		
					}
				 }
			 }		
		}
		if($period=="Full_year")
		{
			 if(!empty($get_StaffList))
			 {
				 foreach($get_StaffList as $staff)
				 {
					$tid=$staff['id'];
					$str++;
					$qry=$dbobject->select("SELECT * FROM `teacher_attendence` WHERE   `tid`='".$tid."' and `acyear`='".$acyear."'"); 
				    $check=$dbobject->mysql_num_row($qry);
					if($check!=0)
					{
					$Present++;	
					}
					else
					{
					$Absent++;		
					}
				 }
			 }			
		}
		if($period=="month")
		{
			$year = date("Y"); 	
			$rmonth = ucfirst($period_type); 	
			$timestamp    = strtotime(''.$rmonth.' '.$year.'');
			$first_second = date('Y-m-01', $timestamp);
			$last_second  = date('Y-m-t', $timestamp); // A leap year!
			 if(!empty($get_StaffList))
			 {
				 foreach($get_StaffList as $staff)
				 {
					$tid=$staff['id'];
					$str++;
					$qry=$dbobject->select("SELECT * FROM `teacher_attendence` WHERE  `date`>='".$first_second."' and `date`<='".$last_second."' and `tid`='".$tid."' and `acyear`='".$acyear."'"); 
				    $check=$dbobject->mysql_num_row($qry);
					if($check!=0)
					{
					$Present++;	
					}
					else
					{
					$Absent++;		
					}
				 }
			 }		
		}
		if($period=="Custom")
		{
			$first_second = date('Y-m-d',strtotime($period_type));
			//echo "<br>";
			$last_second  = date('Y-m-d',strtotime($period_type2)); // A leap year!
			 if(!empty($get_StaffList))
			 {
				 foreach($get_StaffList as $staff)
				 {
					$tid=$staff['id'];
					$str++;
					$qry=$dbobject->select("SELECT * FROM `teacher_attendence` WHERE  `date`>='".$first_second."' and `date`<='".$last_second."' and `tid`='".$tid."' and `acyear`='".$acyear."'"); 
				    $check=$dbobject->mysql_num_row($qry);
					if($check!=0)
					{
					$Present++;	
					}
					else
					{
					$Absent++;		
					}
				 }
			 }		
		}	
		if($period=="date")
		{
			$first_second = date('Y-m-d',strtotime($period_type));
			//echo "<br>";
			$last_second  = date('Y-m-d',strtotime($period_type)); // A leap year!
			 if(!empty($get_StaffList))
			 {
				 foreach($get_StaffList as $staff)
				 {
					$tid=$staff['id'];
					$str++;
					$qry=$dbobject->select("SELECT * FROM `teacher_attendence` WHERE  `date`>='".$first_second."' and `date`<='".$last_second."' and `tid`='".$tid."' and `acyear`='".$acyear."'"); 
				    $check=$dbobject->mysql_num_row($qry);
					if($check!=0)
					{
					$Present++;	
					}
					else
					{
					$Absent++;		
					}
				 }
			 }		
		}		
        $data['Present']=$Present;
        $data['Absent']=$Absent;
        $data['Late']=$Late;
        $data['str']=$str;
		return $data;
	 }
	function Teacher_attendance_count_tid_date($seltype,$seltype2,$period,$period_type,$period_type2,$acyear)
	 {
		$dbobject = new DB();
		$dbobject->getCon(); 
		$get_StaffList=$this->get_StaffList();
		$Present=0;
		$Absent=0;
		$Late=0;		
		$str=0;
		if($period=="Full_year")
		{
		

			$qry=$dbobject->select("SELECT distinct `date` FROM `teacher_attendence` WHERE     `acyear`='".$acyear."' order by `date`"); 
            while($row=$dbobject->fetch_array($qry))
			{
				$data[$row['date']]=$row['date'];
			}
				 
			 			
		}
		elseif($period=="month")
		{
			$year = date("Y"); 	
			$rmonth = ucfirst($period_type); 	
			$timestamp    = strtotime(''.$rmonth.' '.$year.'');
			$first_second = date('Y-m-01', $timestamp);
			$last_second  = date('Y-m-t', $timestamp); // A leap year!

			$qry=$dbobject->select("SELECT distinct `date` FROM `teacher_attendence` WHERE  `date`>='".$first_second."' and `date`<='".$last_second."'  and `acyear`='".$acyear."' order by `date`"); 
            while($row=$dbobject->fetch_array($qry))
			{
				$data[$row['date']]=$row['date'];
			}
				 
			 		
		}
		elseif($period=="Custom")
		{
			$first_second = date('Y-m-d',strtotime($period_type));
			//echo "<br>";
			$last_second  = date('Y-m-d',strtotime($period_type2)); // A leap year!
					$qry=$dbobject->select("SELECT distinct `date` FROM `teacher_attendence` WHERE  `date`>='".$first_second."' and `date`<='".$last_second."' and `acyear`='".$acyear."' order by `date`"); 
            while($row=$dbobject->fetch_array($qry))
			{
				$data[$row['date']]=$row['date'];
			}
				 
			 		
		}elseif($period=="today"){
			$today=date('Y-m-d');
			$data[$today]=$today;
		}elseif($period=="date"){
			$first_second = date('Y-m-d',strtotime($period_type));
			$data[$first_second]=$first_second;
		}
		return $data;	
	 }
	 function TeacherAttendanceclasses($userid,$acyear){
		$dbobject = new DB();
		$dbobject->getCon();
		$i=0;
		$class=array();
		$qry=$dbobject->select("select DISTINCT `teacher_subject`.`classno`,`classname` from `teacher_subject` LEFT JOIN `sclass` on `teacher_subject`.`classno`=`sclass`.`classno` where `type`='0' and `acyear`='".$acyear."' and `teacherid`='".$userid."' and `type`='0'");
		while($row=$dbobject->fetch_array($qry))
		{
			$class[$i]['classno']=$row['classno'];
			$class[$i]['classname']=$row['classname'];
			$i++;
		}
		return $class;
	}
	function TeacherAttendanceHomerooms($userid,$acyear){
		$dbobject = new DB();
		$dbobject->getCon();
		$i=0;
		$class=array();
		$qry=$dbobject->select("select DISTINCT `teacher_subject`.`classid`,`classname`,`division` from `teacher_subject` LEFT JOIN `sclass` on `teacher_subject`.`classid`=`sclass`.`classid` where `type`='0' and `acyear`='".$acyear."' and `teacherid`='".$userid."' and `type`='0'");
		while($row=$dbobject->fetch_array($qry))
		{
			$class[$i]['classid']=$row['classid'];
			$class[$i]['classname']=$row['classname'];
			$class[$i]['division']=$row['division'];
			$i++;
		}
		return $class;
	}
	 function TeacherSubclasses($userid,$acyear){
		$dbobject = new DB();
		$dbobject->getCon();
		$i=0;
		$class=array();
		$qry=$dbobject->select("select DISTINCT `teacher_subject`.`classno`,`classname` from `teacher_subject` LEFT JOIN `sclass` on `teacher_subject`.`classno`=`sclass`.`classno` where `type`!='0' and `acyear`='".$acyear."' and `teacherid`='".$userid."'");
		while($row=$dbobject->fetch_array($qry))
		{
			$class[$i]['classno']=$row['classno'];
			$class[$i]['classname']=$row['classname'];
			$i++;
		}
		return $class;
	}
    function Ger_TeacherListByClass($classid,$acyear){
		$dbobject = new DB();
		$dbobject->getCon();
		$i=0;
		$teacher=array();
		$qry=$dbobject->select("select DISTINCT `teacher_subject`.`teacherid`,`teacher`.`id` from `teacher_subject` LEFT JOIN `teacher` on `teacher_subject`.`teacherid`=`teacher`.`userid` WHERE `teacher_subject`.`acyear`='".$acyear."' and `teacher_subject`.`classid`='".$classid."' AND `type`='0'");
		while($row=$dbobject->fetch_array($qry))
		{
			$teacher[$i]['id']=$row['id'];
			$teacher[$i]['teacherid']=$row['teacherid'];
			$i++;
		}
		return $teacher;
	}
	function Ger_AdminList($acyear){
		$dbobject = new DB();
		$dbobject->getCon();
		$i=0;
		$teacher=array();
		$qry=$dbobject->select("select `id`,`userid` FROM `teacher` WHERE `group` IN ('admin','ADMIN')");
		while($row=$dbobject->fetch_array($qry))
		{
			$teacher[$i]['id']=$row['id'];
			$teacher[$i]['userid']=$row['userid'];
			$i++;
		}
		return $teacher;
	}
}
?>