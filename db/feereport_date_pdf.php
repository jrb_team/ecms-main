<?php
 ini_set('memory_limit', '1024M');
require_once('authentication.php');
authenticate();
include_once('../db/createdb.php');
		$dbobject = new DB();
		if (!$dbobject->getCon())
		{
		echo "Connection Failed";
		}
		else
		{
			$dt_frm=date("Y-m-d",strtotime($_GET['dt_frm']));
			$dt_to=date("Y-m-d",strtotime($_GET['dt_to']));
			$schoolinfo=$dbobject->selectall("schoolinfo",array("id"=>1));
			$section=$_GET['section'];
			$td.="<table ><tr><td colspan='4' style='border-top:none;border-bottom:none;border-left:none;border-right:none;font-size:16px;' padding='10px'><b>ECMS<b></td></tr>";
			$td.="<tr><td colspan='4' style='border-top:none;border-bottom:none;border-left:none;border-right:none;font-size:16px;'><b>KOCHI - 682023</b></td></tr>";
			if($section=="KG")
			{
				$td.="<tr><td colspan='4' style='border-top:none;border-bottom:none;border-left:none;border-right:none;font-size:16px;'><b>FEE COLLECTION REPORT-DBSSSKG</b></td></tr>";
			}
			elseif($section=="LP")
			{
				$td.="<tr><td colspan='4' style='border-top:none;border-bottom:none;border-left:none;border-right:none;font-size:16px;'><b>FEE COLLECTION REPORT-DBSSSLP</b></td></tr>";
			}
			elseif($section=="HS")
			{
				$td.="<tr><td colspan='4' style='border-top:none;border-bottom:none;border-left:none;border-right:none;font-size:16px;'><b>FEE COLLECTION REPORT-DBSSSHS</b></td></tr>";
			}
			elseif($section=="KGN")
			{
				$td.="<tr><td colspan='4' style='border-top:none;border-bottom:none;border-left:none;border-right:none;font-size:16px;'><b>FEE COLLECTION REPORT-DBSSSKG 2016-17</b></td></tr>";
			}
			else
			{
				$td.="<tr><td colspan='4' style='border-top:none;border-bottom:none;border-left:none;border-right:none;font-size:16px;'><b>FEE COLLECTION REPORT-DBSSSKG,DBSSSLP,DBSSSHS - COMBINED</b></td></tr>";
			}
			$td.="<tr><td colspan='4' style='border-top:none;border-bottom:none;border-left:none;border-right:none;font-size:16px;'><b>FOR THE PERIOD ".$_GET['dt_frm']." TO ".$_GET['dt_to']."</b></td></tr></table>";
		$td.="<table border='1' style='width:100%;border-collapse:collapse'>
				<tr><th>Sl.NO.</th><th>Date</th><th>Student Name/ID.No/Ref.No</th><th>Fee Amount</th><th>Discount</th><th>Paid Amount</th></tr>";
			$j=1;
			$total=0;
			$sel_payment=$dbobject->select("SELECT * FROM `payment` WHERE `feetype`='academic' and `pay_date` >= '".$dt_frm."' AND `pay_date` <= '".$dt_to."'");
			while($row=$dbobject->fetch_array($sel_payment))
			{
			$parent_id=$row['parent_id'];
			$studentid=$row['studentid'];
			$student_det=$dbobject->selectall("student",array("studentid"=>$studentid));
			$fee_det=$dbobject->selectall("student_academic_fee",array("id"=>$row['feeid']));
			if($section=="KG")
			{
			  if($schoolinfo['academic_year']==$fee_det['ac_year'])
			    {
			   if($fee_det['classid']>=1 &&  $fee_det['classid']<=2)
			   {
					$td.="<tr><td>".$j."</td><td align='center'>".date("d-m-Y",strtotime($row['pay_date']))."</td><td>".$student_det['sname']."/".$studentid."/".$row['ref_number']."</td><td align='center'>".$row['total_amount']."</td><td align='center'>".$row['discount']."</td><td align='center'>".$row['paid_amount']."</td></tr>";
					$total=$total+$row['paid_amount'];
					$j++;
			   }
			   }
			}
			elseif($section=="LP")
			{
			   if($schoolinfo['academic_year']==$fee_det['ac_year'])
			    {
			   if($fee_det['classid']>=3 &&  $fee_det['classid']<=6)
			   {
			   		$td.="<tr><td>".$j."</td><td align='center'>".date("d-m-Y",strtotime($row['pay_date']))."</td><td>".$student_det['sname']."/".$studentid."/".$row['ref_number']."</td><td align='center'>".$row['total_amount']."</td><td align='center'>".$row['discount']."</td><td align='center'>".$row['paid_amount']."</td></tr>";
					$total=$total+$row['paid_amount'];
					$j++;
			   }
			   }
				
			}
			elseif($section=="HS")
			{
			 if($schoolinfo['academic_year']==$fee_det['ac_year'])
			    {
				if($fee_det['classid']>=7 &&  $fee_det['classid']<=14)
			   {
			   		$td.="<tr><td>".$j."</td><td align='center'>".date("d-m-Y",strtotime($row['pay_date']))."</td><td>".$student_det['sname']."/".$studentid."/".$row['ref_number']."</td><td align='center'>".$row['total_amount']."</td><td align='center'>".$row['discount']."</td><td align='center'>".$row['paid_amount']."</td></tr>";
					$total=$total+$row['paid_amount'];
					$j++;
			   }
			   }
			}
			elseif($section=="KGN")
			{
			if($fee_det['ac_year']=="2016-17")
				{
					$td.="<tr><td>".$j."</td><td align='center'>".date("d-m-Y",strtotime($row['pay_date']))."</td><td>".$student_det['sname']."/".$studentid."/".$row['ref_number']."</td><td align='center'>".$row['total_amount']."</td><td align='center'>".$row['discount']."</td><td align='center'>".$row['paid_amount']."</td></tr>";
					$total=$total+$row['paid_amount'];
					$j++;
				}
			}
			else
			{
					$td.="<tr><td>".$j."</td><td align='center'>".date("d-m-Y",strtotime($row['pay_date']))."</td><td>".$student_det['sname']."/".$studentid."/".$row['ref_number']."</td><td align='center'>".$row['total_amount']."</td><td align='center'>".$row['discount']."</td><td align='center'>".$row['paid_amount']."</td></tr>";
					$total=$total+$row['paid_amount'];
					$j++;
			}
			
			}
			$td.="<tr><td colspan='5' align='right'>Total</td><td align='center'>".$total."</td></tr>";
			$td.="</table>";
			
			
			
							
}

include("../MPDF/mpdf.php");

$mpdf=new mPDF('', 'A4');

// LOAD a stylesheet
//$stylesheet = file_get_contents('css/challan.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
$mpdf->setHeader();	// Clear headers before adding page
$mpdf->setFooter('Page {PAGENO} of {nb}');
$mpdf->AddPage('P','','','','',10,15,10,10,10,10);
$mpdf->WriteHTML($td);

$mpdf->Output('mpdf.pdf','I');
exit;
//==============================================================
//==============================================================
//==============================================================
?>