<?php
include_once('../db/createdb.php');
include_once('../db/fee_class.php');
class Priviliage
{
    function StaffList($id){
        $dbobject = new DB();
        $dbobject->getCon();
       //$query = "SELECT * from `staff_department_portfolio` LEFT JOIN `teacher` ON `staff_department_portfolio`.`staff_id`=`teacher`.`id` WHERE `teacher`.`group`!='admin' AND `staff_department_portfolio`.`profile_id`='".$id."'";
       $query = "SELECT `teacher`.`id`,`userid`,`lname`,`name`,`submenu_id`,`submenu_id2`,`main_menu_id`,`staff_department_portfolio`.`profile_id` from `teacher` LEFT JOIN `staff_department_portfolio` ON `staff_department_portfolio`.`staff_id`=`teacher`.`id` LEFT JOIN `menu_sub_privilege2` ON `menu_sub_privilege2`.`sub_dept_id`=`teacher`.`id` WHERE `teacher`.`group`!='admin' AND `staff_department_portfolio`.`profile_id`='".$id."'";
       $query.=" order by `name`";
		$rs=$dbobject->select($query);
        $i=0;
        $j=0;
        $data=array();
        while($row=$dbobject->fetch_array($rs)){
            $i=$row['id'];
            $data[$i]['id']=$row['id'];
            $data[$i]['userid']=$row['userid'];
            $data[$i]['lname']=$row['lname'];
            $data[$i]['name']=$row['name'];
            $data[$i]['submenu_id'][$j]=$row['submenu_id'];
            $data[$i]['submenu_id2'][$j]=$row['submenu_id2'];
            $data[$i]['main_menu_id'][$j]=$row['main_menu_id'];
            $data[$i]['profile_id'][$j]=$row['profile_id'];
            $j++;
        }
        return $data;
    }   
    function CheckPriviliage_main($id,$group,$depId,$option){
        $dbobject = new DB();
        $dbobject->getCon();
       // echo "SELECT * FROM `menu_sub_privilege2` LEFT JOIN `menu_sub_items` ON `menu_sub_privilege2`.`submenu_id`=`menu_sub_items`.`id` WHERE `sub_dept_id`='".$id."' AND `submenu`='".$option."'";
        $sql=$dbobject->select("SELECT * FROM `menu_sub_privilege2` LEFT JOIN `menu_sub_items` ON `menu_sub_privilege2`.`submenu_id`=`menu_sub_items`.`id` WHERE `sub_dept_id`='".$id."' AND `submenu`='".$option."'");
        $num_row=$dbobject->mysql_num_rows($sql);
        if($num_row>0){
            return true;
        }
        else{
            return false;
        }
    }
    function CheckPriviliage_MainWithSub($id,$group,$depId,$option){
        $dbobject = new DB();
        $dbobject->getCon();
        $i=0;
        $data=array();
        if($group=="admin"){
            $sql=$dbobject->select("SELECT `submenu`,`submenu2` FROM `menu_sub_items` LEFT JOIN `menu_sub_items2` ON `menu_sub_items`.`id`=`menu_sub_items2`.`sub_menu_id` WHERE `submenu`='".$option."'");
        }else{
            $sql=$dbobject->select("SELECT `submenu`,`submenu2` FROM `menu_sub_privilege2` LEFT JOIN `menu_sub_items` ON `menu_sub_items`.`id`=`menu_sub_privilege2`.`submenu_id` LEFT JOIN `menu_sub_items2` ON `menu_sub_items2`.`id`= `menu_sub_privilege2`.`submenu_id2` WHERE `sub_dept_id`='".$id."' AND `submenu`='".$option."'");
        }
        while($row=$dbobject->fetch_array($sql)){
            $data[$i]['submenu']=$row['submenu'];
            $data[$i]['submenu2']=$row['submenu2'];
            $i++;
        }
        return $data;
    }
    function staff_department($id){
        $i=0;
        $dbobject = new DB();
        $dbobject->getCon();
        $data=array();
       // echo "SELECT * from staff_department where `staff_department_section_id`='".$id."' order by id";
        $sql=$dbobject->select("SELECT * from staff_department order by id");
        while($row=$dbobject->fetch_array($sql)){
            $data[$i]['id']=$row['id'];
            $data[$i]['department_name']=$row['department_name'];
            $i++;
        }
        return $data;
    }
    function getAttentancePriviliages($id,$group,$depId,$option){
        $dbobject = new DB();
        $dbobject->getCon();
        $data=array();
        if($group=='admin'){
            $data['section']['School_wise']="School";
            $data['section']['section']="Section";
            $data['section']['classwise']="Year Level";
            $data['section']['homeroom']="Homeroom";
            $data['section']['multiple_student']="Multiple Student";
            $data['section']['indivitual']="Individual Student";
            $data['section']['family']="Family";
            $data['section2']['Dept.Summary']="Dept.Summary";
            $data['period']['today']="Today";
            $data['period']['Full_year']="Full Year";
            $data['period']['sem']="Semester";
            $data['period']['term']="Term";
            $data['period']['month']="Month";
            $data['period']['date']="Day";
            $data['period']['Custom']="Custom";
            $data['type']['Student']="Student";
        }
        else{
        $data['type']['Student']="Student";
      //  $data['section']['School_wise']="School";
        $sql=$dbobject->select("SELECT * FROM `menu_sub_privilege2` LEFT JOIN `menu_sub_items2` ON `menu_sub_privilege2`.`submenu_id2`=`menu_sub_items2`.`id` LEFT JOIN `menu_sub_items` ON `menu_sub_privilege2`.`submenu_id`=`menu_sub_items`.`id` WHERE `sub_dept_id`='".$id."' AND `menu_sub_items`.`submenu`='".$option."' order by `menu_sub_items2`.`id`");
        while($row=$dbobject->fetch_array($sql)){
            if($row['submenu2']=="Today"){
                $data['period']['today']="Today";
               // $data['period']['Today_absent']="Today's absence report";
            }
            elseif($row['submenu2']=="Full Year"){
                $data['period']['Full_year']="Full Year";
            }
            elseif($row['submenu2']=="Semester"){
                $data['period']['sem']="Semester";
            }
            elseif($row['submenu2']=="Term"){
                $data['period']['term']="Term";
            }
            elseif($row['submenu2']=="Month"){
                $data['period']['month']="Month";
            }
            elseif($row['submenu2']=="Day"){
                $data['period']['date']="Day";
            }
            elseif($row['submenu2']=="Custom"){
                $data['period']['Custom']="Custom";
            }
            elseif($row['submenu2']=="Whole School"){
                $data['section']['School_wise']="School";
            }
            elseif($row['submenu2']=="Department wise"){
                $data['section']['section']="Section";
            }
            elseif($row['submenu2']=="Class wise"){
                $data['section']['classwise']="Year Level";
            }
            elseif($row['submenu2']=="Division/homeroom wise"){
                $data['section']['homeroom']="Homeroom";
            }
            elseif($row['submenu2']=="Select Multiple student and compare"){
                $data['section']['multiple_student']="Multiple Student";
            }
            elseif($row['submenu2']=="Individual"){
                $data['section']['indivitual']="Individual Student";
            }
            elseif($row['submenu2']=="Family wise -students"){
                $data['section']['family']="Family";
            }
            elseif($row['submenu2']=="Dept.Summary"){
                $data['section2']['Dept.Summary']="Dept.Summary";
            }
            
        }
    }
        return $data;
    }
    function StudentDetailsPriviliages($userid,$usertype,$studentid){
        $dbobject = new DB();
        $dbobject->getCon();
        $data=array();
        if($usertype=='parent'){
            $student_det=$dbobject->selectall("student",array("studentid"=>$studentid));
            $sql="select * from `parent` where '".$userid."' IN (`family_code`,`m_family_code`,`o_family_code`)";
            $result = $dbobject->select($sql);
            $row = $dbobject->fetch_array($result);
            if($student_det['HM_TPYE']==0){
                if($row['family_code']==$userid){
                    $care_giver=true;
                }       
            }
            elseif($student_det['HM_TPYE']==1){
                if($row['m_family_code']==$userid){
                    $care_giver=true;
                }
            }
            else{
                if($row['o_family_code']==$userid){
                    $care_giver=true;
                }
            }
            if(!$care_giver){
                $sql="select * from `parent_priviliage` where `userid`='".$userid."' and `menu_name`='student_details'";
                $result = $dbobject->select($sql);
                while($row = $dbobject->fetch_array($result)){
                   $data[$row['option_name']]=$row['access_value'];
                }
            }
            else{
                $data['student_info']=1;
                $data['parent_info']=1;
                $data['emergency_contact']=1;
                $data['church_info']=1;
                $data['medical_info']=1;
                $data['document_details']=1;
                $data['more_info']=1;
                $data['passport_details']=1;
                $data['submitted_form']=1;
                $data['father_familycode']=1;
                $data['mother_familycode']=1;
                $data['other_familycode']=1;
                $data['father_details']=1;
                $data['mother_details']=1;
                $data['other_details']=1;
            }
   
        }
        else{
            $data['save']=1;
            $data['search']=1;
            $data['student_info']=1;
            $data['parent_info']=1;
            $data['emergency_contact']=1;
            $data['church_info']=1;
            $data['medical_info']=1;
            $data['document_details']=1;
            $data['more_info']=1;
            $data['passport_details']=1;
            $data['submitted_form']=1;
            $data['father_familycode']=1;
            $data['mother_familycode']=1;
            $data['other_familycode']=1;
            $data['father_details']=1;
            $data['mother_details']=1;
            $data['other_details']=1;
        }
        return $data;
    }
    function getfeerport_Priviliages($id,$group,$depId,$option,$period)
	{
        $dbobject = new DB();
        $dbobject->getCon();
		$Fee = new Fee();
		$feeType=$Fee->feeType();
        $data=array();
        if($group=='admin'){
            $data['section']['School_wise']="School";
            $data['section']['section']="Section";
            $data['section']['classwise']="Year Level";
            $data['section']['homeroom']="Room/Div";
            if($period!="")
			{
				if($period!="today")
				{
                $data['section']['family']="Family";					
				}
					
			}
            $data['period']['today']="Today";
            $data['period']['Full_year']="Full Year";
            $data['period']['month']="Month";
            $data['period']['date']="Day";
            $data['period']['Custom']="Custom";
			if(!empty($feeType))
			{
				foreach($feeType as $key=>$type)
				{
					 $data['type'][$key]=$type;
				}
			}			
        }
        else{
        $sql=$dbobject->select("SELECT * FROM `menu_sub_privilege2` LEFT JOIN `menu_sub_items2` ON `menu_sub_privilege2`.`submenu_id2`=`menu_sub_items2`.`id` LEFT JOIN `menu_sub_items` ON `menu_sub_privilege2`.`submenu_id`=`menu_sub_items`.`id` WHERE `sub_dept_id`='".$id."' AND `menu_sub_items`.`submenu`='".$option."' order by `menu_sub_items2`.`id`");
        while($row=$dbobject->fetch_array($sql)){
            if($row['submenu2']=="Whole School"){
                $data['section']['School_wise']="School";
            }
            elseif($row['submenu2']=="Department wise"){
                $data['section']['section']="Section";
            }
            elseif($row['submenu2']=="Class wise"){
                $data['section']['classwise']="Year Level";
            }
            elseif($row['submenu2']=="Division/homeroom wise"){
                $data['section']['homeroom']="Homeroom";
            }
            elseif($row['submenu2']=="Select Multiple student and compare"){
                $data['section']['multiple_student']="Multiple Student";
            }
            elseif($row['submenu2']=="Individual"){
                $data['section']['indivitual']="Indivitual Student";
            }
            elseif($row['submenu2']=="Family wise -students"){
                $data['section']['family']="Family";
            
            }
            elseif($row['submenu2']=="Today's absence report"){
                $data['period']['Today']="Today";
                $data['period']['Today_absent']="Today's absence report";
            }
            $data['type']['Student']="Student";
            
        }
    }
  //  print_r($data);
        return $data;
    }
function Get_staff_subdivision(){
        $dbobject = new DB();
        $dbobject->getCon();
        $query = "SELECT `id`,`dept_id`,`subdivision` from `staff_subdivision`";
		$rs=$dbobject->select($query);
        $i=0;
        $data=array();
        while($row=$dbobject->fetch_array($rs)){
            $data[$i]['id']=$row['id'];
            $data[$i]['dept_id']=$row['dept_id'];
            $data[$i]['subdivision']=$row['subdivision'];
            $i++;
        }
         return $data;
    }
    function staff_subdivision(){
        $dbobject = new DB();
        $dbobject->getCon();
        $query = "SELECT DISTINCT `subdivision` from `staff_subdivision`";
		$rs=$dbobject->select($query);
        $i=0;
        $data=array();
        while($row=$dbobject->fetch_array($rs)){
            $data[$i]['subdivision']=$row['subdivision'];
            $i++;
        }
         return $data;
    }
    function Get_main_menu(){
        $dbobject = new DB();
        $dbobject->getCon();
        $main_menu=array();
        $query_itm = "SELECT `id`,`item`,`icon`,`url` FROM `menu_main_items` WHERE `status`='0' order by (`id` = 1) desc, `item`";
        $rs_itm=$dbobject->select($query_itm);
        $i=0;
        while($row_itm=$dbobject->fetch_array($rs_itm))
        {
            $main_menu[$i]['id']=$row_itm['id'];
            $main_menu[$i]['item']=$row_itm['item'];
            $main_menu[$i]['submenu']=$this->Get_submenu($row_itm['id']);
            $main_menu[$i]['color']=$this->random_color();
            $i++;
        }
        return $main_menu;
    }
function Get_submenu($menu_id){
        $dbobject = new DB();
        $dbobject->getCon();
        $sub_menu=array();
        $query_subitm = $dbobject->select("SELECT * FROM `menu_sub_items` WHERE `main_id`='".$menu_id."'");;
        $i=0;
        while($row_itm=$dbobject->fetch_array($query_subitm))
        {
            $sub_menu[$i]['id']=$row_itm['id'];
            $sub_menu[$i]['submenu']=$row_itm['submenu'];
           // if($row_itm['submenu']=="Student Info Access Level"){
             //   $sub_menu[$i]['submenu_level2']=$this->Get_Level("student");
            //}
            //else{
                $sub_menu[$i]['submenu_level2']=$this->Get_submenu_level2($row_itm['id']);
            //}
            
            $i++;
        }
    return $sub_menu;
    }
    function getAttentancePriviliages_staff($id,$group,$depId,$option){
        $dbobject = new DB();
        $dbobject->getCon();
        $data=array();     
            $data['section']['School_wise']="School";
            $data['section']['indivitual']="Indivitual";
            $data['period']['today']="Today";
            $data['period']['Full_year']="Full Year";
            $data['period']['month']="Month";
            $data['period']['date']="Day";
            $data['period']['Custom']="Custom";
            $data['type']['Staff']="Staff";

  //  print_r($data);
        return $data;
    }
    function Get_Level($type){
        $dbobject = new DB();
        $dbobject->getCon();
        $sub_menu=array();
        $query_subitm = $dbobject->select("SELECT * FROM `contact_level` WHERE `type`='".$type."'");;
        $i=0;
        while($row_itm=$dbobject->fetch_array($query_subitm))
        {
            $sub_menu[$i]['id']=$row_itm['id'];
            $sub_menu[$i]['submenu2']=$row_itm['Level_name'];
            $i++;
        }
    return $sub_menu;
    }
    function Get_submenu_level2($menu_id){
        $dbobject = new DB();
        $dbobject->getCon();
        $sub_menu=array();
        $query_subitm = $dbobject->select("SELECT * FROM `menu_sub_items2` WHERE `sub_menu_id`='".$menu_id."'");;
        $i=0;
        while($row_itm=$dbobject->fetch_array($query_subitm))
        {
            $sub_menu[$i]['id']=$row_itm['id'];
            $sub_menu[$i]['submenu2']=$row_itm['submenu2'];
            $i++;
        }
    return $sub_menu;
    }
	function attendence_contact_view($staff_id)
	{
        $dbobject = new DB();
        $dbobject->getCon();		
        $menu_main_privilege=$dbobject->select("SELECT DISTINCT `main_menu_id`,`item` FROM `menu_sub_privilege2` LEFT JOIN `menu_main_items` ON `menu_sub_privilege2`.`main_menu_id`=`menu_main_items`.`id`  WHERE `sub_dept_id`='".$staff_id."' and `main_menu_id`='4' and `submenu_id`='15' and `submenu_id2`='1'");		
	    $staff_det=$dbobject->selectall("teacher",array("id"=>$staff_id));
		if($staff_det['userid']=="adminlccc")
		{
		$chk=1;
		return $chk;	
		}
		else
		{
			return $chk=$dbobject->mysql_num_row($menu_main_privilege);
		}
		
	}
    function getIndPriviliage($group,$userid,$menu_id){
        $dbobject = new DB();
        $dbobject->getCon();
        $sub_menu=array();
        $i=0;
        if($group=="admin"){
            $sel=$dbobject->select("SELECT `sub_menu_id`,`submenu2` FROM `menu_main_items` INNER JOIN `menu_sub_items` ON `menu_sub_items`.`main_id`=`menu_main_items`.`id` LEFT JOIN `menu_sub_items2` on `menu_sub_items2`.`sub_menu_id`=`menu_sub_items`.`id` WHERE `menu_main_items`.`id`='".$menu_id."' and `menu_main_items`.`status`='0' AND `menu_sub_items2`.`status`='1'");
        }else if($group=="parent"){
            $sub_menu[0]="Enable Direct Debit";
        }
        else{
            $sel=$dbobject->select("SELECT `sub_menu_id`,`submenu2` FROM `menu_sub_privilege2` INNER JOIN `menu_sub_items2` ON `menu_sub_privilege2`.`submenu_id2`=`menu_sub_items2`.`id` WHERE `sub_dept_id`='".$userid."' AND `main_menu_id`='".$menu_id."'");
        }
        while($submenu_row=$dbobject->fetch_array($sel)){
            $sub_menu[$i]=$submenu_row['submenu2'];
            $i++;
        }
        return $sub_menu;
    } 
    function getpriviliage_individualOption($group,$userid,$menu){
        $dbobject = new DB();
        $dbobject->getCon();
        if($group=="admin"){
        return true;
        }else{
           $sql=$dbobject->select("SELECT `menu_sub_privilege2`.`id` FROM `menu_sub_privilege2` INNER JOIN `menu_sub_items2` ON `menu_sub_privilege2`.`submenu_id2`=`menu_sub_items2`.`id` WHERE `menu_sub_items2`.`submenu2`='".$menu."' AND `sub_dept_id`='".$userid."'");
           $num_rows=$dbobject->mysql_num_row($sql);
           if($num_rows>0){
            return true;
           }else{
            return false;
           }
           
        }
    }
    function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }
    
    function random_color() {
        return $this->random_color_part() . $this->random_color_part() . $this->random_color_part();
    }
}
?>