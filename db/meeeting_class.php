<?php
include_once('createdb.php');
require_once('teacher_class.php');
class meeting
{
  
   function get_available_days($tid,$parentid)
  {
	 $dbobj = new DB();
	 $dbobj->getCon();
	 $sql="select distinct `date` from `appoinment_timings` where `tid`='".$tid."'";
	  if($parentid!="")
	  {
		 $sql.=" and `book_parentid` IN ('0','".$parentid."') ";  
	  }	 
	  $sql.="order by `date`";
          $sel=$dbobj->select($sql);
	 $i=0;
	while($row=$dbobj->fetch_array($sel))
		{
			$date[$i]=$row['date'];
			$i++;
		}
	 return $date;
  } 
  function get_timings($tid,$date)
  {
	  	$dbobj = new DB();
		$dbobj->getCon();
		$sel=$dbobj->select("select * from `appoinment_timings` where `tid`='".$tid."' and `date`='".$date."' order by `from_time`");
		$i=0;
		while($row=$dbobj->fetch_array($sel))
		{
			$data[$i]['id']=$row['id'];
			$data[$i]['date']=$row['date'];
			$data[$i]['from_time']=$row['from_time'];
			$data[$i]['to_time']=$row['to_time'];
			$data[$i]['status']=$row['status'];
			$data[$i]['book_parentid']=$row['book_parentid'];
			$i++;
		}
		return $data;
  }
  function meeting_for_parent($tid,$parentid)
  {
	  	 $dbobj = new DB();
	     $dbobj->getCon();
		 $teacher_det=$dbobj->selectall("teacher",array("id"=>$tid));
		 $student_det=$dbobj->selectall("student",array("parent_id"=>$parentid));
		 $acyear=$student_det['acyear'];
		 $imgid=$teacher_det['imgid'];
		 $teacher=new Teacher();
		 $subjects=$teacher->get_SubjectBy_tid_classid($teacher_det['userid'],$student_det['classid'],$acyear);
		 $get_available_days=$this->get_available_days($tid,$parentid);
		if(!empty($subjects))
		{
			foreach($subjects as $sub)
			{
				$teacher_sub_det[$sub['subname']]=$sub['subname'];
			}
		}
		$tf=0;
		if(!empty($teacher_sub_det))
		{
			foreach($teacher_sub_det as $subname=>$subname1)
			{
				if($tf==0)
				{
					$data.=$subname1;
				}
				else
				{
					$data.=",".$subname1;
				}
				$tf=1;
			}
		}		
		 $j=0;
		 $output=array();
		  $output['name']=strtoupper($teacher_det['name'])." ".strtoupper($teacher_det['lname']);
		  $output['photo']=$teacher->Staff_photo($imgid);
		  $output['note']="It is recommended that you check the meeting location and keep at least 5 to 10 minutes gap between each parent-teacher interview booking for avoiding late reporting.";
		  $output['subject']=$data;
		 if(!empty($get_available_days))
		 {
			 foreach($get_available_days as $date)
			 {
				 $output['slot_details'][$j]['date']=$date;
				 $available_timings=$this->get_timings($tid,$date);
		         if(!empty($available_timings))
		        {
					$i=0;
			       foreach($available_timings as $timings)
			              {
							  $check_slot_status=$this->check_slot($timings['id'],$date);
								if($timings['status']=='open' || $check_slot_status==$parentid)
								{
										 //Available
										 if($timings['book_parentid']==0 || $timings['book_parentid']==$parentid)
										 {
                                     $output['slot_details'][$j]['slot'][$i]['slotid']=$timings['id'];										 
                                     $output['slot_details'][$j]['slot'][$i]['from_time']=$timings['from_time'];										 
                                     $output['slot_details'][$j]['slot'][$i]['to_time']=$timings['to_time'];										 									 
                                     $output['slot_details'][$j]['slot'][$i]['status_value']=$check_slot_status;	
                                        $i++;	     
										}									 
		
								}
								else
								{
									//closed
									//echo "<a href='#openModal1' class='btn btn-danger'>".$timings['from_time']."-".$timings['to_time']."</a>&nbsp;";
								}
														
				           }
				}
				 
				 $j++;
			 }
		 }
		 return $output;
	
  }
   function check_slot($id,$date)
  {
	  	$dbobj = new DB();
		$dbobj->getCon();
		$slot_det=$dbobj->selectall("appoinment_timings",array('id'=>$id));
		//echo "select * from `appoinment_booking` where `slot_id`='".$id."' and `booking_date`='".$date."' and `status`!='Cancelled'";
		$check=$dbobj->select("select * from `appoinment_booking` where `slot_id`='".$id."' and `booking_date`='".$date."' and `status`!='Cancelled'");
		$num_rows=mysql_num_rows($check);
		if($num_rows==0)
		{
			return $num_rows;
		}
		else
		{
			$row=$dbobj->fetch_array($check);
			return $row['parent_id'];
		}
  }
  function prev_appnmt_by_parent_teacher($parentid,$tid,$date)
  {
	  $dbobj = new DB();
	  $dbobj->getCon();	 
	  $sql=$dbobj->select("select * from `appoinment_booking` where `parent_id`='".$parentid."'  and `tid`='".$tid."' and  `status`='active' and `booking_date`='".$date."' ");
	  $chk=mysql_num_rows($sql);
	  if($chk!=0)
	  {
		$row=$dbobj->fetch_array($sql);
        $appont_det['slot_id']=$row['slot_id'];		
        $appont_det['check']=$chk;		
        $appont_det['id']=$row['id'];		
	  }
	  else
	  {
        $appont_det['slot_id']=0;		
        $appont_det['check']=0;		  
        $appont_det['check']=0;		  
	  }
	  return $appont_det;	  
      	  
  }  
 function prev_appnmt_by_time($parentid,$from_time,$to_time,$date) 
 {
	  $dbobj = new DB();
	  $dbobj->getCon();	 
	  $sql=$dbobj->select("select `appoinment_booking`.*,`appoinment_timings`.`from_time`,`appoinment_timings`.`to_time` from `appoinment_booking` left join `appoinment_timings` on `appoinment_booking`.`slot_id`=`appoinment_timings`.`id` where `appoinment_booking`.`parent_id`='".$parentid."'   and  `appoinment_booking`.`status`='active' and `appoinment_timings`.`from_time`='".$from_time."' and `appoinment_timings`.`to_time`='".$to_time."' and `appoinment_timings`.`date`='".$date."'");
	  $chk=mysql_num_rows($sql);
	  if($chk!=0)
	  {
		$row=$dbobj->fetch_array($sql);
        $appont_det['slot_id']=$row['slot_id'];		
        $appont_det['check']=$chk;		
        $appont_det['id']=$row['id'];		
	  }
	  else
	  {
        $appont_det['slot_id']=0;		
        $appont_det['check']=0;		  
        $appont_det['id']=0;		  
	  }
	  return $appont_det; 	 
 }  
 function apponmnt_chk($parentid,$tid,$from_time,$to_time,$date)
 {
	 $prev_appnmt_by_parent_teacher=$this->prev_appnmt_by_parent_teacher($parentid,$tid,$date);
	 $prev_appnmt_by_time=$this->prev_appnmt_by_time($parentid,$from_time,$to_time,$date);
	 $chk1=$prev_appnmt_by_parent_teacher['check'];
	 $chk2=$prev_appnmt_by_time['check'];

	if($chk1!=0 && $chk2==0)
		{

			$appo_det['val']=1;
		    $appo_det['slot_id']=$prev_appnmt_by_parent_teacher['slot_id'];
		    $appo_det['id']=$prev_appnmt_by_parent_teacher['id'];
            $appo_det['test']="test1";			


			
		}
		elseif($chk2!=0 && $chk1==0)
		{

			$appo_det['val']=2;
		    $appo_det['slot_id']=$prev_appnmt_by_time['slot_id'];
		    $appo_det['id']=$prev_appnmt_by_time['id'];
            $appo_det['test']="test2";					
		}
		elseif($chk2!=0 && $chk1!=0)
		{
			$appo_det['val']=3;
		    $appo_det['slot_id']=$prev_appnmt_by_time['slot_id'];
		    $appo_det['id']=$prev_appnmt_by_time['id'];
            $appo_det['test']="test3";				
		}

	 
    return $appo_det; 
	 
 }  
  function check_meeting($tid,$date,$from_time,$to_time,$parentid)
 {
	  $dbobj = new DB();
	  $dbobj->getCon();	 
	  $modifyed_time1=date("H:i:s", strtotime('-5 minutes', strtotime($from_time)));
	  $modifyed_time2=date("H:i:s", strtotime('+5 minutes', strtotime($to_time)));
      $sql1="SELECT * FROM `appoinment_timings` WHERE `date`='".$date."' and `tid`!='".$tid."' and `from_time`>='".$modifyed_time1."' and `to_time`<='".$from_time."' and `book_parentid`='".$parentid."'";
	  $qry1=$dbobj->select($sql1);
      $sql2="SELECT * FROM `appoinment_timings` WHERE `date`='".$date."' and `tid`!='".$tid."' and `from_time`>='".$to_time."' and `to_time`<='".$modifyed_time2."' and `book_parentid`='".$parentid."'";
	  $qry2=$dbobj->select($sql2);	  
      $num_rows1=mysql_num_rows($qry1);
      $num_rows2=mysql_num_rows($qry2);
	  if($num_rows1!=0 || $num_rows2!=0)
	  {
		  $val=1;
	  }
	  else
	  {
		  $val=0;
	  }
	  
	  return $val;	  
 }
function slot_details($slotid,$parentid)
{
   $dbobj = new DB();
   $dbobj->getCon(); 
   $slot_det=$dbobj->selectall("appoinment_timings",array("id"=>$slotid));
   $teacher_det=$dbobj->selectall("teacher",array("id"=>$slot_det['tid']));
   $tname=strtoupper($teacher_det['name'])." ".strtoupper($teacher_det['lname']);
   $check_slot_status=$this->check_slot($slotid,$slot_det['date']);  
    $prev_appnmt_det=$this->apponmnt_chk($parentid,$slot_det['tid'],$slot_det['from_time'],$slot_det['to_time'],$slot_det['date']);	  
	$check_prev_appnmt=$prev_appnmt_det['val'];
	$slotid_prev_appnmt=$prev_appnmt_det['slot_id'];
	$id_prev_appnmt=$prev_appnmt_det['id'];
	$prev_slot_det=$dbobj->selectall("appoinment_timings",array('id'=>$slotid_prev_appnmt));
	if($check_slot_status==0)
	{ 
       if($check_prev_appnmt==1)
	   {
		   //change appointment
		   $data['booking_status']="CHANGE APPOINTMENT";
		   $data['booking_value']=1;
		   $data['booking_note']="An Appointment is booked  on ".date('d-m-Y',strtotime($prev_slot_det['date']))." For Time ".$prev_slot_det['from_time']." To ".$prev_slot_det['to_time']."";		   
		   $data['id_prev_appnmt']=$id_prev_appnmt;
		   $data['slotid_prev_appnmt']=$slotid_prev_appnmt;
		   $data['slotid']=$slotid;
		   $data['from_time']=$slot_det['from_time'];
		   $data['to_time']=$slot_det['to_time'];
		   $data['location']=$slot_det['Location'];
		   $data['date']=date('d-m-Y',strtotime($slot_det['date']));
		   $data['name']=$tname;
	   }
       else
	   {
		   // book now
		    $data['booking_status']="BOOK NOW";
			$data['booking_value']=0;
			$data['booking_note']="";
		   $data['id_prev_appnmt']="";
		   $data['slotid_prev_appnmt']="";
		   $data['slotid']=$slotid;
		   $data['from_time']=$slot_det['from_time'];
		   $data['to_time']=$slot_det['to_time'];
		   $data['location']=$slot_det['Location'];	
           $data['date']=date('d-m-Y',strtotime($slot_det['date']));		   
           $data['name']=$tname;		   
	   }	   
	}
	elseif($check_slot_status==$parentid)
	{
      // cancell
	  $data['booking_status']="CANCEL APPOINTMENT";
	  $data['booking_value']=2;
	  $data['booking_note']="";
	  $data['id_prev_appnmt']="";
      $data['slotid_prev_appnmt']="";
	  $data['slotid']=$slotid;
		   $data['from_time']=$slot_det['from_time'];
		   $data['to_time']=$slot_det['to_time'];
		   $data['location']=$slot_det['Location'];	
 $data['date']=date('d-m-Y',strtotime($slot_det['date']));		   
      $data['name']=$tname;	  
		
		
	}	
	return $data;
}
function change_meeting($id_prev_appnmt,$slotid_prev_appnmt,$slotid,$parentid)
{
	  $dbobj = new DB();
	  $dbobj->getCon();	
	  $date=date("Y-m-d H:i:s");
	  $slot_det=$dbobj->selectall("appoinment_timings",array("id"=>$slotid));
	  $update1=$dbobj->exe_qry("update `appoinment_booking` set `booking_date`='".$slot_det['date']."',`booked_date`='".$date."',`slot_id`='".$slotid."' where `id`='".$id_prev_appnmt."'");
      $update2=$dbobj->exe_qry("update `appoinment_timings` set `book_parentid`='0' where `id`='".$slotid_prev_appnmt."'");
      $update3=$dbobj->exe_qry("update `appoinment_timings` set `book_parentid`='".$parentid."' where `id`='".$slotid."'");	  
}
function book_meeting($slotid,$parentid)
{
	  $dbobj = new DB();
	  $dbobj->getCon();	
	  $date=date("Y-m-d H:i:s");
	  $slot_det=$dbobj->selectall("appoinment_timings",array("id"=>$slotid));
      $ins=$dbobj->exe_qry("insert into `appoinment_booking` (`slot_id`,`tid`,`parent_id`,`booking_date`,`booked_date`,`booked_by`,`status`)
      values('".$slotid."','".$slot_det['tid']."','".$parentid."','".$slot_det['date']."','".$date."','".$booked_by."','active')");
      $update3=$dbobj->exe_qry("update `appoinment_timings` set `book_parentid`='".$parentid."' where `id`='".$slotid."'");	  
}
function cancel_meeting($slotid,$parentid)
{
	  $dbobj = new DB();
	  $dbobj->getCon();	
	  $date=date("Y-m-d H:i:s");
	  $slot_det=$dbobj->selectall("appoinment_timings",array("id"=>$slotid));
	$upd=$dbobj->exe_qry("update `appoinment_booking` set `status`='cancelled' where `parent_id`='".$parentid."' and `slot_id`='".$slotid."' and `booking_date`='".$slot_det['date']."'");
	$update=$dbobj->exe_qry("update `appoinment_timings` set `book_parentid`='0' where `id`='".$slotid."'");  
}
function meeting_status_updation($slotid,$parentid,$id_prev_appnmt,$slotid_prev_appnmt,$booking_status_value)
{
	if($booking_status_value==0)
	{
		$booking=$this->book_meeting($slotid,$parentid);
		$val=$booking_status_value;
		
	}
	elseif($booking_status_value==1)
	{
		$change_meeting=$this->change_meeting($id_prev_appnmt,$slotid_prev_appnmt,$slotid,$parentid);
		$val=$booking_status_value;
	}
	elseif($booking_status_value==2)
	{
		$cancel_meeting=$this->cancel_meeting($slotid,$parentid);
		$val=$booking_status_value;
	}
	else
	{
		$val=3;
	}
	return $val;
    	
}
}
?>