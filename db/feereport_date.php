<?php
			$dt_frm=date("Y-m-d",strtotime($_GET['dt_frm']));
			$dt_to=date("Y-m-d",strtotime($_GET['dt_to']));
			$section=$_GET['section'];
			$f_day= date('j', strtotime($dt_frm));
			$td.="<table border='1' style='width:100%'>
				<tr><th>Sl.NO.</th><th>Date</th><th>Student Name/ID.No/Ref.No</th><th>Fee Amount</th><th>Discount</th><th>Paid Amount</th></tr>";
			$j=1;
			$total=0;
			$sel_payment=$dbobject->select("SELECT * FROM `payment` WHERE `feetype`='academic' and `pay_date` >= '".$dt_frm."' AND `pay_date` <= '".$dt_to."'");
			while($row=$dbobject->fetch_array($sel_payment))
			{
			$parent_id=$row['parent_id'];
			$studentid=$row['studentid'];
			$student_det=$dbobject->selectall("student",array("studentid"=>$studentid));
			$fee_det=$dbobject->selectall("student_academic_fee",array("id"=>$row['feeid']));
			if($section=="KG")
			{
			   if($fee_det['classid']>=1 &&  $fee_det['classid']<=2)
			   {
					$td.="<tr><td>".$j."</td><td align='center'>".date("d-m-Y",strtotime($row['pay_date']))."</td><td>".$student_det['sname']."/".$studentid."/".$row['ref_number']."</td><td align='center'>".$row['total_amount']."</td><td align='center'>".$row['discount']."</td><td align='center'>".$row['paid_amount']."</td></tr>";
					$total=$total+$row['paid_amount'];
					$j++;
			   }
			}
			elseif($section=="LP")
			{
			   if($fee_det['classid']>=3 &&  $fee_det['classid']<=6)
			   {
			   		$td.="<tr><td>".$j."</td><td align='center'>".date("d-m-Y",strtotime($row['pay_date']))."</td><td>".$student_det['sname']."/".$studentid."/".$row['ref_number']."</td><td align='center'>".$row['total_amount']."</td><td align='center'>".$row['discount']."</td><td align='center'>".$row['paid_amount']."</td></tr>";
					$total=$total+$row['paid_amount'];
					$j++;
			   }
				
			}
			elseif($section=="HS")
			{
				if($fee_det['classid']>=7 &&  $fee_det['classid']<=14)
			   {
			   		$td.="<tr><td>".$j."</td><td align='center'>".date("d-m-Y",strtotime($row['pay_date']))."</td><td>".$student_det['sname']."/".$studentid."/".$row['ref_number']."</td><td align='center'>".$row['total_amount']."</td><td align='center'>".$row['discount']."</td><td align='center'>".$row['paid_amount']."</td></tr>";
					$total=$total+$row['paid_amount'];
					$j++;
			   }
			}
			else
			{
					$td.="<tr><td>".$j."</td><td align='center'>".date("d-m-Y",strtotime($row['pay_date']))."</td><td>".$student_det['sname']."/".$studentid."/".$row['ref_number']."</td><td align='center'>".$row['total_amount']."</td><td align='center'>".$row['discount']."</td><td align='center'>".$row['paid_amount']."</td></tr>";
					$total=$total+$row['paid_amount'];
					$j++;
			}
			}
			$td.="<tr><td colspan='5' align='right'>Total</td><td align='center'>".$total."</td></tr>";
			$td.="</table>";		
?>
<div>
			<?php include 'statusbar.php'; ?>
			</div>
			<div class="row-fluid sortable">
			
				<div class="box span9 center">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-th"></i>COLLECTION REPORT</h2>
						<div class="box-icon">

							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<center>
					<form action="feereport_date_pdf.php" name="" method="GET"/>
					<input type="hidden" name="dt_frm" value="<?php echo $_GET['dt_frm']?>"/>
					<input type="hidden" name="dt_to" value="<?php echo $_GET['dt_to'] ?>"/>
					<input type="hidden" name="section" value="<?php echo $_GET['section'] ?>"/>
					<div class="box-content">
                  	<div align="center" class="row-fluid">
					<input type='submit' value='EXPORT TO PDF'/>
					<div style="height:400px;overflow:scroll">
					<?php
					if($section=="KG")
					{
					?>
					<h3>FEE COLLECTION REPORT-DBSSSKG</h3>
					<?php
					}
					elseif($section=="LP")
					{
						?>
						<h3>FEE COLLECTION REPORT-DBSSSLP</h3>
						<?php
					}
					elseif($section=="HS")
					{
						?>
						<h3>FEE COLLECTION REPORT-DBSSSHS</h3>
						<?php
					}
					else
					{
						?>
						<h3>FEE COLLECTION REPORT-DBSSSKG,DBSSSLP,DBSSSHS - COMBINED</h3>
						<?php
					}
					?>
					<h3>Fee Report From <?php echo $_GET['dt_frm'];?> To <?php echo $_GET['dt_to'] ?></h3>
					<?php
						echo $td;
					?>
							</center>
						</form>
					</div>
                    
                    </div>                   
                  </div>
				</div><!--/span-->
			</div><!--/row-->