<?php
include_once('createdb.php');
class Library
{
	function get_fine()
	{
		$obj=new DB();
		$obj->getCon();
		$query="select * from `fine_tbl` where `type`='library' ";
		$rs=$obj->select($query);
		$row=$obj->fetch_array($rs);
		$fine['fine']=$row['fine'];
		$fine['percent']=$row['percent'];
		$fine['fixed']=$row['fixed'];
		return $fine;
	}
	function fine_amount($duedate,$today,$bookno)
	{
			$obj=new DB();
			$obj->getCon();
			$book_det=$obj->selectall("library",array("bookno"=>$bookno));
			$fine=$this->get_fine();
				if($today>$duedate)
				{
					$today=strtotime($today);
					$duedate=strtotime($duedate);
					$diff=abs($today-$duedate);
					$years = floor($diff / (365*60*60*24));// Find Years
					$months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
					$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/(60*60*24));
					if($fine['percent']=="1" && $fine['fixed']=="0")
					{
						$price=$book_det['price'];
						$perc=round(($price/100)*$fine['fine'],2);
						$fine=$days*$perc;    // 5 is the Fine amount
					}
					else
					{
						$fine=$days*$fine['fine'];  
					}
					
				}
				else
				{
					$fine=0;
				}
			return $fine;
	}
			function get_data_lib($type)
	{
		$obj=new DB();
		$obj->getCon();
		$sql=$obj->select("select distinct `".$type."` from  `library` order by `".$type."`");	
		$i=1;
		while($row=$obj->fetch_array($sql))
		{
		$data[$i]=$row[$type];
		$i++;
		//"$row'['".$type."]"
		}
		return $data;
	}
	
	function get_category_name()
	{
		$obj=new DB();
		$obj->getCon();
		$sql=$obj->select("select distinct `category_name`,`id` from `category`" );	
		$i=1;
		
		while($row=$obj->fetch_array($sql))
		{
		$data[$i]['category_name']=$row['category_name'];
		$data[$i]['id']=$row['id'];
		$i++;
		}
		
		return $data;
	}
	
function reserve($bookid)
 {
	    $obj=new DB();
		$obj->getCon();
	    $sql_reserve=$obj->select("select * from book_reserve where bookid='".$bookid."' and `issue_status`='0' and `reserve_status`='Y'");
		$i=0;
		while($sql_row=$obj->fetch_array($sql_reserve))
		{
			$reserve[$i]['id']=$sql_row['id'];
			$reserve[$i]['studid']=$sql_row['studid'];
			$reserve[$i]['due_date']=$sql_row['due_date'];
			$reserve[$i]['reserve_date']=$sql_row['reserve_date'];
			
			$i++;
		} 
	return $reserve;	
 }	
}

?>