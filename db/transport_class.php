<?php
include_once('createdb.php');
class Transport
{
	function currency()
	{
		$currency="$";
		return $currency;
	}
	function get_allDriver()
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$i=0;
		$sql=$dbobj->select("select `teacher`.`id`,`teacher`.`name`,`teacher`.`lname` from `staff_subdivision`  LEFT JOIN `staff_department_portfolio` on `staff_subdivision`.`id`=`staff_department_portfolio`.`profile_id` LEFT JOIN `teacher` on `staff_department_portfolio`.`staff_id`=`teacher`.`id` WHERE `subdivision` LIKE '%driver%'");
		while($driver_row=$dbobj->fetch_array($sql))
		{
			$driver[$i]['id']=$driver_row['id'];
			$driver[$i]['cfname']=strtoupper($driver_row['name']);
			$driver[$i]['clname']=strtoupper($driver_row['lname']);
			$i++;
		}
		return $driver;
	}
	function get_allAttender()
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$i=0;
		$sql=$dbobj->select("select `teacher`.`id`,`teacher`.`name`,`teacher`.`lname` from `staff_subdivision`  LEFT JOIN `staff_department_portfolio` on `staff_subdivision`.`id`=`staff_department_portfolio`.`profile_id` LEFT JOIN `teacher` on `staff_department_portfolio`.`staff_id`=`teacher`.`id` WHERE `subdivision` LIKE '%attender%'");
		while($driver_row=$dbobj->fetch_array($sql))
		{
			$attender[$i]['id']=$driver_row['id'];
			$attender[$i]['cfname']=strtoupper($driver_row['name']);
			$attender[$i]['clname']=strtoupper($driver_row['lname']);
			$i++;
		}
		return $attender;
	}
	function getAllbus(){
		$dbobj = new DB();
		$dbobj->getCon();
		$sel=$dbobj->select("select * from `bus` WHERE `status` IN ('0','2','3') order by `busname`");
		$i=0;
		while($row=$dbobj->fetch_array($sel))
		{
			$stops[$i]['id']=$row['id'];
			$stops[$i]['busname']=$row['busname'];
			$stops[$i]['root']=$row['root'];
			$stops[$i]['seats']=$row['seats'];
			$stops[$i]['locality']=$row['locality'];
			$stops[$i]['printAndDirectionsUrl']=$row['printAndDirectionsUrl'];
			$i++;
		}
		return $stops;
	}
	function get_numberOfStudent($bus,$place,$acyear)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$acyear=$dbobj->get_acyear();
		$sql=$dbobj->select("select distinct `sid` from `student_bus_fee` where `route_id`='".$bus."' and `ac_year`='".$acyear."'");
		$num=mysql_num_rows($sql);
		return $num;
	}
	function get_BusDriver($bus,$place,$acyear)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$sel=$dbobj->select("select * from `bus_driver` where `bus`='".$bus."' and `place`='".$place."'");
		$driver_row=$dbobj->fetch_array($sel);
		$staffid=$driver_row['driver'];
		$staff_det=$dbobj->selectall("teacher",array("id"=>$staffid));
		return $staff_det;
	}
	function get_BusAttender($bus,$place,$acyear)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$sel=$dbobj->select("select * from `bus_driver` where `bus`='".$bus."' and `place`='".$place."'");
		$attender_row=$dbobj->fetch_array($sel);
		$staffid=$attender_row['attender'];
		$staff_det=$dbobj->selectall("teacher",array("id"=>$staffid));
		return $staff_det;
	}		
	function get_numOfStopes($vehicle_no,$place)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$sel=$dbobj->select("select * from `bus` where `veh_no`='".$vehicle_no."' and `place`='".$place."'");
		$num=mysql_num_rows($sel);
		return $num;
	}
	function get_numberOfStudentBus($bus,$acyear,$place)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$no_students=0;
		$sel_stop=$dbobj->select("select * from bus where root='".$bus."' and `place`='".$place."' order by `id`");
			while($stop_row=$dbobj->fetch_array($sel_stop))
			{
				$no_students=$no_students+$this->get_numberOfStudent($stop_row['id'],$place,$acyear);
			}
			return $no_students;
	}
	function get_ListOfStudentStopAll($stop,$acyear,$status)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$sql=$dbobj->select("select distinct `student_bus_fee`.`sid`,`student_bus_fee`.`instalment_mode`,`sname`,`s_sur_name`,`SECOND_NAME`,`studentid`,`student`.`parent_id`,`student`.`family_code`,`classname`,`division` from `student_bus_fee` LEFT JOIN `student` on `student_bus_fee`.`sid`=`student`.`sid` LEFT JOIN `sclass` on `student`.`classid`=`sclass`.classid where `stop`='".$stop."' and `ac_year`='".$acyear."' AND `student_bus_fee`.`status`='".$status."'");
		$i=1;
		while($row=$dbobj->fetch_array($sql))
		{
			$sids[$i]['sid']=$row['sid'];
			$sids[$i]['parent_id']=$row['parent_id'];
			$sids[$i]['studentid']=$row['studentid'];
            $sids[$i]['sname']=$row['sname'];
            $sids[$i]['s_sur_name']=$row['s_sur_name'];
            $sids[$i]['SECOND_NAME']=$row['SECOND_NAME'];
            $sids[$i]['classname']=$row['classname'];
            $sids[$i]['division']=$row['division'];
			$sids[$i]['family_code']=$row['family_code'];
			$sids[$i]['instalment_mode']=$row['instalment_mode'];
			$i++;
		}
		return $sids;
	}
	function get_ListOfStudentStopModel2($stop,$acyear,$status)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		//echo "select distinct `student_bus_inquiry`.`sid`,`student_bus_inquiry`.`side`,`sname`,`s_sur_name`,`SECOND_NAME`,`studentid`,`student`.`parent_id`,`student`.`family_code`,`classname`,`division` from `student_bus_inquiry` LEFT JOIN `student` on `student_bus_inquiry`.`sid`=`student`.`sid` LEFT JOIN `sclass` on `student`.`classid`=`sclass`.classid where `stop_id`='".$stop."' and `ac_year`='".$acyear."' AND `student_bus_inquiry`.`status`='1'";
		$sql=$dbobj->select("select distinct `student_bus_inquiry`.`sid`,`student_bus_inquiry`.`side`,`sname`,`s_sur_name`,`SECOND_NAME`,`studentid`,`student`.`parent_id`,`student`.`family_code`,`classname`,`division` from `student_bus_inquiry` LEFT JOIN `student` on `student_bus_inquiry`.`sid`=`student`.`sid` LEFT JOIN `sclass` on `student`.`classid`=`sclass`.classid where `stop_id`='".$stop."' and `ac_year`='".$acyear."' AND `student_bus_inquiry`.`status`='1'");
		$i=1;
		while($row=$dbobj->fetch_array($sql))
		{
			$sids[$i]['sid']=$row['sid'];
			$sids[$i]['parent_id']=$row['parent_id'];
			$sids[$i]['studentid']=$row['studentid'];
            $sids[$i]['sname']=$row['sname'];
            $sids[$i]['s_sur_name']=$row['s_sur_name'];
            $sids[$i]['SECOND_NAME']=$row['SECOND_NAME'];
            $sids[$i]['classname']=$row['classname'];
            $sids[$i]['division']=$row['division'];
			$sids[$i]['family_code']=$row['family_code'];
			$sids[$i]['side']=$row['side'];
			$i++;
		}
		return $sids;
	}
	function get_ListOfStudentStop($stop,$side,$acyear,$status)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$sql=$dbobj->select("select distinct `student_bus_fee`.`sid`,`student_bus_fee`.`instalment_mode`,`sname`,`s_sur_name`,`SECOND_NAME`,`studentid`,`student`.`parent_id`,`family_code`,`classname`,`division` from `student_bus_fee` LEFT JOIN `student` on `student_bus_fee`.`sid`=`student`.`sid` LEFT JOIN `sclass` on `student`.`classid`=`sclass`.classid where `stop`='".$stop."' and `ac_year`='".$acyear."' AND `student_bus_fee`.`status`='".$status."' AND `side`='".$side."'");
		$i=1;
		$sids=array();
		while($row=$dbobj->fetch_array($sql))
		{
			$sids[$i]['sid']=$row['sid'];
			$sids[$i]['parent_id']=$row['parent_id'];
			$sids[$i]['studentid']=$row['studentid'];
            $sids[$i]['sname']=$row['sname'];
            $sids[$i]['s_sur_name']=$row['s_sur_name'];
            $sids[$i]['SECOND_NAME']=$row['SECOND_NAME'];
			$sids[$i]['family_code']=$row['family_code'];
            $sids[$i]['classname']=$row['classname'];
            $sids[$i]['division']=$row['division'];
			$sids[$i]['instalment_mode']=$row['instalment_mode'];
			$i++;
		}
		return $sids;
	}
	function get_PaidListByacyear($mode,$acyear,$flag)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$acyear=$dbobj->get_acyear();
		$parent_id=array();
		$sel="select distinct `parent_id` from `student_bus_fee` where `ac_year`='".$acyear."' and `amount`!='0'";
		if($flag!=""){
			$sel.=" AND `flag`='".$flag."'";	
		}
		if($mode!=""){
			$sel.=" AND `instalment_mode`='".$mode."'";	
		}
		$sql=$dbobj->select($sel);
		$i=1;
		while($row=$dbobj->fetch_array($sql))
		{
			$parent_id[$i]=$row['parent_id'];
			$i++;
		}
		return $parent_id;
	}
	function get_PaidUnpaindListByacyear($stopid,$acyear,$flag)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$acyear=$dbobj->get_acyear();
		$sql=$dbobj->select("select distinct `parent_id` from `student_bus_fee` where `ac_year`='".$acyear."' and `route_id`='".$stopid."' and `flag`='".$flag."'");
		$i=1;
		$parent_id=array();
		while($row=$dbobj->fetch_array($sql))
		{
			$parent_id[$i]=$row['parent_id'];
			$i++;
		}
		return $parent_id;
	}
	function get_StudentListStop($bus,$acyear,$place)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$i=0;
		$sel_stop=$dbobj->select("select * from bus where `stop`='".$bus."' order by `id`");
			while($stop_row=$dbobj->fetch_array($sel_stop))
			{
				$sids=$this->get_ListOfStudentBus($stop_row['id'],$place,$acyear);
				if(!empty($sids))
				{
					foreach($sids as $s)
					{
						$student=$dbobj->selectall("student",array("sid"=>$s));
						$student_list[$i]['sid']=$s;
						$student_list[$i]['studentid']=$student['studentid'];
						$student_list[$i]['s_sur_name']=$student['s_sur_name'];
						$student_list[$i]['sname']=$student['sname'];
						$i++;
					}
				}
			}
			return $student_list;
	}
	function get_stopList($root,$acyear)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$sel=$dbobj->select("SELECT * FROM `bus_stopes` WHERE `bus_id`='".$root."' order by `km`");
		$i=0;
		while($row=$dbobj->fetch_array($sel))
		{
			$stops_array[$i]['id']=$row['id'];
			$stops_array[$i]['bus_id']=$row['bus_id'];
			$stops_array[$i]['stop']=$row['stop'];
			$stops_array[$i]['stop_address']=$row['stop_address'];
			$stops_array[$i]['km']=$row['km'];
			$stops_array[$i]['pick_up']=$row['pick_up'];
			$stops_array[$i]['drop_up']=$row['drop_up'];
			$stops_array[$i]['rate']=$row['rate'];
			$i++;
		}
		return $stops_array;
	}
function get_stopListModel2($root,$acyear)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$sel=$dbobj->select("SELECT * FROM `bus_stopes` WHERE `bus_id`='".$root."' AND `stopType` NOT IN ('start','end') order by `stop` ASC");
		$i=0;
		while($row=$dbobj->fetch_array($sel))
		{
			$stops_array[$i]['id']=$row['id'];
			$stops_array[$i]['bus_id']=$row['bus_id'];
			$stops_array[$i]['stop']=$row['stop'];
			$stops_array[$i]['stop_address']=$row['stop_address'];
			$stops_array[$i]['km']=$row['km'];
			$stops_array[$i]['pick_up']=$row['pick_up'];
			$stops_array[$i]['drop_up']=$row['drop_up'];
			$stops_array[$i]['travelTimeMinutes']=$row['travelTimeMinutes'];
			$stops_array[$i]['rate']=$row['rate'];
			$i++;
		}
		return $stops_array;
	}
function get_stopByroot($root)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$sel=$dbobj->select("select * from `bus_stopes` where `bus_id`='".$root."' AND `stopType` NOT IN ('start','end')");
		$stops_array=array();
		$i=0;
		while($row=$dbobj->fetch_array($sel))
		{
			$stops_array[$i]['id']=$row['id'];
			$stops_array[$i]['stop_no']=$row['stop_no'];
			$stops_array[$i]['bus_id']=$row['bus_id'];
			$stops_array[$i]['stop']=$row['stop'];
			$stops_array[$i]['stop_address']=$row['stop_address'];
			$stops_array[$i]['km']=$row['km'];
			$stops_array[$i]['serviceTimeInMinutes']=$row['serviceTimeInMinutes'];
			$stops_array[$i]['travelTimeMinutes']=$row['travelTimeMinutes'];
			$stops_array[$i]['rate']=$row['rate'];
			$i++;
		}
		return $stops_array;
	}
    function term_det()
  {

	$dbobject = new DB();
	$dbobject->getCon();	
	$qry=$dbobject->select("SELECT distinct `term` FROM `term_month_fee_setting` order by `term`");
	$i=0;
	while($row=$dbobject->fetch_array($qry))
	{
		$terms[$i]=$row['term'];
		$i++;
	}			
		return $terms;
  }
  function month_det()
  {

	$dbobject = new DB();
	$dbobject->getCon();	
	$qry=$dbobject->select("SELECT *  FROM `term_month_fee_setting` order by `term`");
	$i=0;
	while($row=$dbobject->fetch_array($qry))
	{
		$month[$i]=$row['month'];
		$i++;
	}			
		return $month;
  } 
   function TransportfeeByTerm($term,$side,$val,$acyear)
	{
			$dbobject = new DB();
			$dbobject->getCon();
			$month_temp=$this->term_month();
			$month1=$month_temp['I Term'];
		    $month2=$month_temp['II Term'];
			$month3=$month_temp['III Term'];
			if($term=="I Term")
				{
					foreach($month1 as $mnth)
					{
						$month_det=$dbobject->selectall("transport_fee",array("root"=>$val,"month"=>$mnth,"acyear"=>$acyear));
						$term_double=$term_double+$month_det['single_amt'];
					}
					
				}
				if($term=="II Term")
				{
					foreach($month2 as $mnth)
					{
						$month_det=$dbobject->selectall("transport_fee",array("root"=>$val,"month"=>$mnth,"acyear"=>$acyear));
						$term_double=$term_double+$month_det['single_amt'];
					}
					
				}
				if($term=="III Term")
				{
					foreach($month3 as $mnth)
					{
						$month_det=$dbobject->selectall("transport_fee",array("root"=>$val,"month"=>$mnth,"acyear"=>$acyear));
						$term_double=$term_double+$month_det['single_amt'];
					}
					
				}
				if($term_double!=0)
				{
					$single=$term_double/2;
				}
				
				if($side=="double")
                 {
					 $amount=$term_double;
				 }
				 elseif($side=="single")
				 {
					 $amount=$single;
				 }

				
		return 	$amount;	
	}
	function TransportfeeBymonth($term,$side,$val,$acyear)
	{
			$dbobject = new DB();
			$dbobject->getCon();
	        $month_amount=$dbobject->selectall("transport_fee",array("root"=>$val,"month"=>$term,"acyear"=>$acyear));
				if($side=="double")
                 {
					 $amount=$month_amount['single_amt'];
				 }
				 elseif($side=="single")
				 {
					 $single=$month_amount['single_amt']/2;
					 $amount=$single;
				 }

				
		return 	$amount;	
	} 
function bus_fee_data_by_sid_acyear($sid,$acyear)
{
	$dbobject = new DB();
	$dbobject->getCon();	
	$sel_fee=$dbobject->select("select * from `student_bus_fee` where `sid`='".$sid."' and  `ac_year`='".$acyear."'");
	$i=0;
	while($sel_row=$dbobject->fetch_array($sel_fee))
	{
		$data[$i]['id']=$sel_row['id'];
		$data[$i]['flag']=$sel_row['flag'];
		$data[$i]['term']=$sel_row['term'];
		$data[$i]['route_id']=$sel_row['route_id'];
		$data[$i]['feetype']=$sel_row['feetype'];
		$data[$i]['side']=$sel_row['side'];
		$selroute=$dbobject->selectall("bus",array("id"=>$sel_row['route_id']));
		if($sel_row['amount']!=0)
		{
			$amount=$sel_row['amount'];
		}
		else
		{
			if($sel_row['feetype']=="term")
			{
				$amount=$this->TransportfeeByTerm($sel_row['term'],$sel_row['side'],$sel_row['route_id'],$acyear);
			}
			elseif($sel_row['feetype']=="month")
			{				
				$amount=$this->TransportfeeBymonth($sel_row['term'],$sel_row['side'],$sel_row['route_id'],$acyear);				
			}

		}
        $data[$i]['amount']=$amount;	
        $data[$i]['root']=$selroute['root'];	
        $data[$i]['stops']=$selroute['stops'];        	
        $i++;		
	}
return $data;	
}
	function transport_details_by_term($sid,$acyear)
	{

        $bus_fee_data_by_sid_acyear=$this->bus_fee_data_by_sid_acyear($sid,$acyear);
		if(!empty($bus_fee_data_by_sid_acyear))
		{
			foreach($bus_fee_data_by_sid_acyear as $bus_fee_data)
			{
				$data[$bus_fee_data['term']]['term']=$bus_fee_data['term'];
				$data[$bus_fee_data['term']]['amount']=$bus_fee_data['amount'];
				$data[$bus_fee_data['term']]['flag']=$bus_fee_data['flag'];
				$data[$bus_fee_data['term']]['id']=$bus_fee_data['id'];
				$data[$bus_fee_data['term']]['route_id']=$bus_fee_data['route_id'];
				$data[$bus_fee_data['term']]['side']=$bus_fee_data['side'];
				$data[$bus_fee_data['term']]['root']=$bus_fee_data['root'];
				$data[$bus_fee_data['term']]['stops']=$bus_fee_data['stops'];
				$data[$bus_fee_data['term']]['feetype']=$bus_fee_data['feetype'];
			}
		}
		return $data;	
	}
  function month_det_new()
  {

	$dbobject = new DB();
	$dbobject->getCon();	
	$qry=$dbobject->select("SELECT * FROM `term_month_fee_setting` order by `term`");
	$i=0;
	while($row=$dbobject->fetch_array($qry))
	{
		$month[$i]=$row['month'];
		$i++;
	}		
		return $month;
  } 
 function term_month()
{
	$dbobject = new DB();
	$dbobject->getCon();	
	$qry=$dbobject->select("SELECT distinct `term` FROM `term_month_fee_setting` order by `term`");  
  	while($row=$dbobject->fetch_array($qry))
	{
		$term=$row['term'];
		$qry2=$dbobject->select("SELECT * FROM `term_month_fee_setting` where `term`='".$term."' order by `term`");  
		$i=0;
		while($row2=$dbobject->fetch_array($qry2))
		{
			$month[$term][$i]=$row2['month'];
			$i++;
		}		
	}

  return $month;
}  
function term_month_det()
{
	$dbobject = new DB();
	$dbobject->getCon();	
	$qry=$dbobject->select("SELECT * FROM `term_month_fee_setting` order by `term`");
	while($row=$dbobject->fetch_array($qry))
	{
		$data[$row['month']]=$row['term'];
	}
	return $data;
}
	function distinct_Busfeetype()
	{
			$dbobject = new DB();
			$dbobject->getCon();
	        $route_det=$dbobject->select("SELECT DISTINCT  `feetype` FROM  `student_bus_fee`");
            $i=0;           
		   while($route_row=$dbobject->fetch_array($route_det))
			{
				$i++;
              	$feetype[$i]=$route_row['feetype'];		
			}	
   			return $feetype;
		    
	}
	function payment_type($val)
	{
		  if($val=="month")
		  {
			  $payment_type="Monthly";
		  }
		  elseif($val=="term")
		  {
              $payment_type="TermWise";
		  }
	
   			return $payment_type;
		    
	}
	function side()
	{
 	  $dbobject = new DB();
	  $dbobject->getCon();
	  $qry=$dbobject->select("select distinct `side` from `student_bus_fee`");
	  $i=0;
	  while($row=$dbobject->fetch_array($qry))
	  {
		  $i++;
		  $side[$i]=$row['side'];
		  
	  }
      return $side;	  
		    
	}	
	function termBymonth($month)
	{
	  $dbobject = new DB();
	  $dbobject->getCon();
	  $qry=$dbobject->select("select distinct `term` from `transport_fee` where `month`='".$month."'");
	  $row=$dbobject->fetch_array($qry);
      return $row['term']; 	  
		    
	}
	function calculateTRFee($params,$tr_settings){
		$dbobject = new DB();
		$dbobject->getCon();
		$amount=0;
		if($tr_settings['fee_type']=="Fixed-KM"){
			$sel_fee=$dbobject->select("SELECT * FROM `km_fee` WHERE `type`='Fixed-KM' AND `field`<='".$params['km']."' order by `field` DESC LIMIT 1");
			$num_rows=$dbobject->mysql_num_row($sel_fee);
			if($num_rows=="0"){
				$sel_fee=$dbobject->select("SELECT * FROM `km_fee` WHERE `type`='Fixed-KM'  order by `field` ASC LIMIT 1");
			}
			$row=$dbobject->fetch_array($sel_fee);
			if($params['student_pos']==1){
				$amount=$row['1st_child'];
			}elseif($params['student_pos']==2){
				$amount=$row['2nd_child'];
			}elseif($params['student_pos']==3){
				$amount=$row['3rd_child'];
			}elseif($params['student_pos']==4){
				$amount=$row['4th_child'];
			}elseif($params['student_pos']==5){
				$amount=$row['5th_child'];
			}elseif($params['student_pos']==6){
				$amount=$row['6th_child'];
			}
		}elseif($tr_settings['fee_type']=="Per-KM"){
			$tr_fee=$dbobject->selectall("km_fee",array("type"=>"Per-KM","acyear"=>$params['acyear']));
			if($params['student_pos']==1){
				$amount=$tr_fee['1st_child'];
			}elseif($params['student_pos']==2){
				$amount=$tr_fee['2nd_child'];
			}elseif($params['student_pos']==3){
				$amount=$tr_fee['3rd_child'];
			}elseif($params['student_pos']==4){
				$amount=$tr_fee['4th_child'];
			}elseif($params['student_pos']==5){
				$amount=$tr_fee['5th_child'];
			}elseif($params['student_pos']==6){
				$amount=$tr_fee['6th_child'];
			}
			$amount=$amount*$params['km'];
		}elseif($tr_settings['fee_type']=="Stop-Wise"){
			$tr_fee=$dbobject->selectall("km_fee",array("type"=>"Stop-Wise","field"=>$params['stopid'],"acyear"=>$params['acyear']));
			if($params['student_pos']==1){
				$amount=$tr_fee['1st_child'];
			}elseif($params['student_pos']==2){
				$amount=$tr_fee['2nd_child'];
			}elseif($params['student_pos']==3){
				$amount=$tr_fee['3rd_child'];
			}elseif($params['student_pos']==4){
				$amount=$tr_fee['4th_child'];
			}elseif($params['student_pos']==5){
				$amount=$tr_fee['5th_child'];
			}elseif($params['student_pos']==6){
				$amount=$tr_fee['6th_child'];
			}
		}
	return $amount;
	}
	function getBus(){
		$dbobject = new DB();
		$dbobject->getCon();
		$sel=$dbobject->select("select  id,root,veh_no from bus order by `root`");
		$i=0;
		$data=array();
		while($row=$dbobject->fetch_array($sel))
		{
			$data[$i]['id']=$row['id'];
			$data[$i]['root']=$row['root'];
			$data[$i]['veh_no']=$row['veh_no'];
			$i++;
		}
		return $data;
	}
	function GetTransportFee($acyear,$parentid){
		$dbobject = new DB();
		$dbobject->getCon();
		$data=array();
		$i=0;
		$sel_fee=$dbobject->select("SELECT DISTINCT `term` FROM `student_bus_fee` WHERE `parent_id`='".$parentid."' AND `ac_year`='".$acyear."'");
		while($row=$dbobject->fetch_array($sel_fee)){
			$fee_det=$dbobject->selectall("student_bus_fee",array("term"=>$row['term'],"parent_id"=>$parentid,"ac_year"=>$acyear));
			$data[$row['term']][$i]=$fee_det;
			$i++;
		}
		return $data;
	}
	function GetTransportFeeIndv($acyear,$sid){
		$dbobject = new DB();
		$dbobject->getCon();
		$data=array();
		$i=0;
		$sel_fee=$dbobject->select("SELECT * FROM `student_bus_fee` WHERE `sid`='".$sid."' AND `ac_year`='".$acyear."' order by `id`");
		while($row=$dbobject->fetch_array($sel_fee)){
			$data[$i]['term']=$row['term'];
			$data[$i]['amount']=$row['amount'];
			$data[$i]['due_date']=$row['due_date'];
			$data[$i]['instalment_mode']=$row['instalment_mode'];
			$i++;
		}
		return $data;
	}
	function ChildrenCount($pid,$acyear){
		$dbobject = new DB();
		$dbobject->getCon();
		$data=array();
		$i=0;
		$sel_student=$dbobject->select("SELECT DISTINCT `sid` FROM `student_bus_fee` WHERE `parent_id`='".$pid."' AND `ac_year`='".$acyear."'");
		while($row=$dbobject->fetch_array($sel_student)){
			$data[$i]=$row['sid'];
			$i++;
		}
		return $data;
	}
	function get_PaidListByInstallModeacyear($mode,$acyear,$flag){
		$dbobj = new DB();
		$dbobj->getCon();
		$parent_id=array();
		$sql="select distinct `parent_id` from `student_bus_fee` where `ac_year`='".$acyear."' and `flag`='".$flag."'  and `amount`!='0' and `instalment_mode`='".$mode."'";;
		$sel=$dbobj->select($sql);
		$i=1;
		while($row=$dbobj->fetch_array($sel))
		{
			$parent_id[$i]=$row['parent_id'];
			$i++;
		}
		return $parent_id;
	}
	function get_PaidListByInstallModeDate($mode,$acyear,$flag,$date){
		$dbobj = new DB();
		$dbobj->getCon();
		$parent_id=array();
		$date=date('Y-m-d',strtotime($date));
		$sql="select distinct `parent_id` from `student_bus_fee` where `ac_year`='".$acyear."' and `flag`='".$flag."' and `instalment_mode`='".$mode."' and `amount`!='0' and `paid_date`='".$date."'";;
		$sel=$dbobj->select($sql);
		$i=1;
		while($row=$dbobj->fetch_array($sel))
		{
			$parent_id[$i]=$row['parent_id'];
			$i++;
		}
		return $parent_id;
	}
	function getFeeMode($acyear,$type){
		$dbobj = new DB();
		$dbobj->getCon();
		$sel=$dbobj->select("SELECT * FROM `fee_head_split_optionTransport` WHERE `acyear`='".$acyear."' AND `type`='".$type."'");
		$i=0;
		$data=array();
		while($row=$dbobj->fetch_array($sel)){
			$data[$i]['type']=$row['type'];
			$data[$i]['head']=$row['head'];
			$data[$i]['due_date']=$row['due_date'];
			$i++;
		}
		return $data;
	}
	function FeeSplitOption($acyear){
		$dbobj = new DB();
		$dbobj->getCon();
		$sel=$dbobj->select("SELECT DISTINCT `type` FROM `fee_head_split_optionTransport` WHERE `acyear`='".$acyear."'");
		$i=0;
		$data=array();
		while($row=$dbobj->fetch_array($sel)){
			$data[$i]['type']=$row['type'];
			$i++;
		}
		return $data;
	}
	function getStudentByMode($acyear,$type,$busid){
		$dbobj = new DB();
		$dbobj->getCon();
		$sel=$dbobj->select("SELECT DISTINCT `student_bus_fee`.`sid`,`sname`,`s_sur_name` FROM `student_bus_fee` LEFT JOIN `student` on `student_bus_fee`.`sid`=`student`.`sid` WHERE `ac_year`='".$acyear."' AND `instalment_mode`='".$type."' and `root`='".$busid."'");
		$i=0;
		$data=array();
		while($row=$dbobj->fetch_array($sel)){
			$data[$i]['sid']=$row['sid'];
			$data[$i]['sname']=$row['sname']." ".$row['s_sur_name'];
			$i++;
		}
		return $data;
	}
	function getIndAmount($acyear,$option,$yearlyFee){
		$getFeeMode=$this->getFeeMode($acyear,$option);
		$feecount=count($getFeeMode);
		if($yearlyFee!="" && $yearlyFee!='0' && $feecount!='0'){
			$ind_fee=round($yearlyFee/$feecount,2);
		}else{
			$ind_fee=0;
		}
		return $ind_fee;
	}
	function CalcFine($instalment_mode,$Effective,$due_date,$amount){
		$date=date('d-m-Y');
		if($Effective=="Weekly"){
			$startDate = new DateTime($date);
			$endDate = new DateTime($due_date);
			$difference = $endDate->diff($startDate);
			$diff=(int)(($difference->days) / 7);
	
		}elseif($Effective=="Fortnightly"){
			$startDate = new DateTime($date);
			$endDate = new DateTime($due_date);
			$difference = $endDate->diff($startDate);
			$diff=(int)(($difference->days) / 14);
		}elseif($Effective=="Monthly"){
			$date1 = $due_date;
			$date2 = $date;
			
			$ts1 = strtotime($date1);
			$ts2 = strtotime($date2);
			
			$year1 = date('Y', $ts1);
			$year2 = date('Y', $ts2);
			
			$month1 = date('m', $ts1);
			$month2 = date('m', $ts2);
			
			$diff = (($year2 - $year1) * 12) + ($month2 - $month1);
		}
		$amount=$amount*$diff;
		return $amount;
	}
	function WaitingListbyNum($acyear,$root){
		$dbobject = new DB();
		$dbobject->getCon();
		$student_bus_fee_qry_chk=$dbobject->select("SELECT distinct `student_bus_fee`.`sid`,`student_bus_fee`.`status`,`stop`  FROM `student_bus_fee` WHERE `ac_year`='".$acyear."' and `root`='".$root."' and `status`='0'"); 
		$waiting=$dbobject->mysql_num_row($student_bus_fee_qry_chk);
		return $waiting;
	}
	function WaitingListbyNumModel2($acyear,$root){
		$dbobject = new DB();
		$dbobject->getCon();
		$student_bus_fee_qry_chk=$dbobject->select("SELECT distinct `student_bus_inquiry`.`sid`,`student_bus_inquiry`.`status` FROM `student_bus_inquiry` WHERE `ac_year`='".$acyear."' and `root_id`='".$root."' AND `status`='1'"); 
		$waiting=$dbobject->mysql_num_row($student_bus_fee_qry_chk);
		return $waiting;
	}
	function SideIn($acyear,$root){
		$dbobject = new DB();
		$dbobject->getCon();
		$student_bus_fee_qry_in=$dbobject->select("SELECT distinct `student_bus_fee`.`sid`,`student_bus_fee`.`status`  FROM `student_bus_fee` WHERE `ac_year`='".$acyear."' and `root`='".$root."' and `side`='IN' and `status`='1'"); 
		$side_in=$dbobject->mysql_num_row($student_bus_fee_qry_in);
		return $side_in;
	}
	function SideOut($acyear,$root){
		$dbobject = new DB();
		$dbobject->getCon();
		$student_bus_fee_qry_out=$dbobject->select("SELECT distinct `student_bus_fee`.`sid`,`student_bus_fee`.`status`  FROM `student_bus_fee` WHERE `ac_year`='".$acyear."' and `root`='".$root."' and `side`='OUT' and `status`='1'"); 
		$side_out=$dbobject->mysql_num_row($student_bus_fee_qry_out);
		return $side_out;
	}
	function get_WaitingStudentListMode2($acyear){
		$dbobj = new DB();
		$dbobj->getCon();
		$sql=$dbobj->select("select `student_bus_inquiry`.*,`sname`,`s_sur_name`,`SECOND_NAME`,`studentid`,`student`.`parent_id`,`student`.`family_code`,`classname`,`division` from `student_bus_inquiry` LEFT JOIN `student` on `student_bus_inquiry`.`sid`=`student`.`sid` LEFT JOIN `sclass` on `student`.`classid`=`sclass`.classid where  `ac_year`='".$acyear."' AND `student_bus_inquiry`.`root_id`='0' AND `student_bus_inquiry`.`status`='0' order by `family_code`");
		$i=1;
		while($row=$dbobj->fetch_array($sql))
		{
			$sids[$i]['id']=$row['id'];
			$sids[$i]['sid']=$row['sid'];
			$sids[$i]['parent_id']=$row['parent_id'];
			$sids[$i]['studentid']=$row['studentid'];
            $sids[$i]['sname']=$row['sname'];
            $sids[$i]['s_sur_name']=$row['s_sur_name'];
            $sids[$i]['SECOND_NAME']=$row['SECOND_NAME'];
            $sids[$i]['classname']=$row['classname'];
            $sids[$i]['division']=$row['division'];
			$sids[$i]['family_code']=$row['family_code'];
			$sids[$i]['pickup_busname']=$row['pickup_busname'];
			$sids[$i]['dropoff_busname']=$row['dropoff_busname'];
			$sids[$i]['pickuplocality']=$row['pickuplocality'];
			$sids[$i]['dropofflocality']=$row['dropofflocality'];
			$i++;
		}
		return $sids;
	}
	function get_WaitingStudentListByrootMode2($acyear,$rootid){
		$dbobj = new DB();
		$dbobj->getCon();
		$sql=$dbobj->select("select `student_bus_inquiry`.*,`sname`,`s_sur_name`,`SECOND_NAME`,`studentid`,`student`.`parent_id`,`student`.`family_code`,`classname`,`division` from `student_bus_inquiry` LEFT JOIN `student` on `student_bus_inquiry`.`sid`=`student`.`sid` LEFT JOIN `sclass` on `student`.`classid`=`sclass`.classid where  `ac_year`='".$acyear."' AND `student_bus_inquiry`.`status`='0' AND `root_id`='".$rootid."' order by `family_code`");
		$i=1;
		while($row=$dbobj->fetch_array($sql))
		{
			$sids[$i]['id']=$row['id'];
			$sids[$i]['sid']=$row['sid'];
			$sids[$i]['parent_id']=$row['parent_id'];
			$sids[$i]['studentid']=$row['studentid'];
            $sids[$i]['sname']=$row['sname'];
            $sids[$i]['s_sur_name']=$row['s_sur_name'];
            $sids[$i]['SECOND_NAME']=$row['SECOND_NAME'];
            $sids[$i]['classname']=$row['classname'];
            $sids[$i]['division']=$row['division'];
			$sids[$i]['family_code']=$row['family_code'];
			$sids[$i]['pickup_busname']=$row['pickup_busname'];
			$sids[$i]['dropoff_busname']=$row['dropoff_busname'];
			$sids[$i]['pickuplocality']=$row['pickuplocality'];
			$sids[$i]['dropofflocality']=$row['dropofflocality'];
			$i++;
		}
		return $sids;
	}
	function get_ListOfStudentWaitingModel2($stop,$side,$acyear,$status)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$sids=array();
		$qry="select distinct `student_bus_inquiry`.`sid`,`sname`,`s_sur_name`,`SECOND_NAME`,`studentid`,`student`.`parent_id`,`family_code`,`classname`,`division` from `student_bus_inquiry` LEFT JOIN `student` on `student_bus_inquiry`.`sid`=`student`.`sid` LEFT JOIN `sclass` on `student`.`classid`=`sclass`.classid where `stop_id`='".$stop."' and `ac_year`='".$acyear."' AND `student_bus_inquiry`.`status`='1'";
		if($side=="IN"){
			$qry.="AND `side` IN ('IN','IN_OUT')";
		}
		if($side=="OUT"){
			$qry.="AND `side` IN ('PUT','IN_OUT')";
		}
		$sql=$dbobj->select($qry);
		$i=1;
		while($row=$dbobj->fetch_array($sql))
		{
			$sids[$i]['sid']=$row['sid'];
			$sids[$i]['parent_id']=$row['parent_id'];
			$sids[$i]['studentid']=$row['studentid'];
            $sids[$i]['sname']=$row['sname'];
            $sids[$i]['s_sur_name']=$row['s_sur_name'];
            $sids[$i]['SECOND_NAME']=$row['SECOND_NAME'];
			$sids[$i]['family_code']=$row['family_code'];
            $sids[$i]['classname']=$row['classname'];
            $sids[$i]['division']=$row['division'];
			$sids[$i]['instalment_mode']=$row['instalment_mode'];
			$i++;
		}
		return $sids;
	}
	function getGmapKey(){
		$key="AIzaSyA5TuuUnm-okQwgc8P3NLurc_nLTjrQtiU";
		return $key;
	}
	function GetPickupTime($start_time,$travel_time,$stop_num,$travelTimeMinutes,$last_time){
		
		if($stop_num==0){
			return $start_time;
		}else{
		if($travelTimeMinutes!=0){
		  $time = new DateTime($last_time);
            $minute=$travelTimeMinutes;
			if($minute>0){
		   	$time->add(new DateInterval('PT'.$minute.'M'));
			}
		  // output the new time
		  return $time->format('h:i A');
		}else{
		$time = new DateTime($start_time);
		// add 245 minutes to the time
		$time->add(new DateInterval('PT'.$travel_time.'M'));
		
		// output the new time
		return $time->format('h:i A');
		}
		}
	}
	function GetDropoffTime($start_time,$travel_time){
		$time = new DateTime($start_time);

		// add 245 minutes to the time
		$time->add(new DateInterval('PT'.$travel_time.'M'));
		
		// output the new time
		return $time->format('h:i A');
	}
	function parent_detByFamilyCode($family_id){
		$dbobj = new DB();
		$dbobj->getCon();
		$parent_det=$dbobj->selectall("parent",array("family_code"=>$family_id));
		return $parent_det;
	}
}
?>