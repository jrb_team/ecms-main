<?php
include_once('createdb.php');
include_once('notification.php');
include_once('teacher_class.php');
class Message
{
	function send_message($type,$type_data,$subject,$message,$from_type,$to_type,$from_id,$to,$date,$attach)
	 {  
	     $dbobj = new DB();
         $dbobj->getCon();
		 $notifyobj = new notification();
		 $attachf=$this->upload($attach);
		 $subject=addslashes($subject);
		 $message=addslashes($message);
		 $msg_tocken=md5(time().'-'.mt_rand());
		 $ins_head=$dbobj->exe_qry("insert into `message_head` (`tocken`,`type`,`type_data`) VALUES ('".$msg_tocken."','".$type."','".$type_data."')");
		 //print_r($attachf);exit;
		 $h=0;
		 if(!empty($to))
		 {
		  foreach($to as $i)
			{
					$ins= $dbobj->exe_qry("insert into message(`subject`,`body`,`type_from`,`type_to`,`id_from`,`id_to`,`date`,`attachment`,`tocken`) values('".$subject."','".$message."','".$from_type."','".$to_type[$h]."','".$from_id."','".$i."','".$date."','".$attachf['name']."','".$msg_tocken."')");
					$msg_id=$dbobj->mysql_insert_id();
					$notify_id=$notifyobj->insert('message',$from_type,$from_id,$msg_id,$to_type[$h],$i);
					//$notify=$notifyobj->push_notification($notify_id,'message',$from_type,$from_id,$msg_id,$to_type[$h],$i);
					$h++;
			}
         }
		if($msg_id!="")
		{
         $val=1;
		}
		else
		{
		 $val=2;
		}
		return $val;
	 }
	 function send_message_ind($subject,$message,$from_type,$to_type,$from_id,$to,$date,$attach)
	 {  
	     $dbobj = new DB();
         $dbobj->getCon();
		 $msg_tocken=md5(time().'-'.mt_rand());
		 $notifyobj = new notification();
		 $attachf=$this->upload($attach);
	     $ins= $dbobj->exe_qry("insert into message(`subject`,`body`,`type_from`,`type_to`,`id_from`,`id_to`,`date`,`attachment`,`tocken`) values('".$subject."','".$message."','".$from_type."','".$to_type."','".$from_id."','".$to."','".$date."','".$attachf['name']."','".$msg_tocken."')");
		 $msg_id=$dbobj->mysql_insert_id();
	     $notify_id=$notifyobj->insert('message',$from_type,$from_id,$msg_id,$to_type,$to);
	     //$notify=$notifyobj->push_notification($notify_id,'message',$from_type,$from_id,$msg_id,$to_type,$to);
	           
		if($msg_id!="")
		{
         $val=1;
		}
		else
		{
		 $val=2;
		}
		return $val;
	 }
	function reply_message($subject,$message,$from_type,$to_type,$from_id,$to,$date,$attach,$msgid)
	 {  
	     $dbobj = new DB();
         $dbobj->getCon();
		 $notifyobj = new notification();
		 $attachf=$this->upload($attach);
		 //print_r($attachf);exit;
	     $ins= $dbobj->exe_qry("insert into reply_mail(`msgid`,`subject`,`body`,`type_from`,`type_to`,`id_from`,`id_to`,`date`,`attachment`) values('".$msgid."','".$subject."','".$message."','".$from_type."','".$to_type."','".$from_id."','".$to."','".$date."','".$attachf['name']."')");
	     $msg_id=$dbobj->mysql_insert_id();
	     $notify=$notifyobj->insert('reply message',$from_type,$from_id,$msg_id,$to_type,$to);
		 if($msg_id!="")
		    {
             $val=1;
		    }
		 else
		    {
		     $val=2;
		    }
		return $val;
	 }
	function upload($attach)
	 {
      $dbobj = new DB();
      $dbobj->getCon();	 
	  if($_FILES['attachFile']['name']!='')
        {
	      $documents_dir ="../message/";  // The  Folder
		  $size=$_FILES["attachFile"]["size"];
	      $temp = explode(".", $_FILES["attachFile"]["name"]);
	      $type= $temp[1]; // Getting the File Extension .jpg,.gif.etc..
	      $newfilename1 = round(microtime(true))."msg1".".".$type;
	      copy($_FILES['attachFile']['tmp_name'], "$documents_dir" . $newfilename1);  //Copy the file to temp folder
	      $ins=$dbobj->exe_qry("insert into `attachments` (`type`,`document_name`,`extension`,`date`,`acyear`) values ('message','".$newfilename1."','".$type."','".date('Y-m-d H:S')."','".$acyear."')");
        }
		$attachc['name']=$newfilename1;
		
		return $attachc;
	 }
	function fetch_mail($id_to,$type)
	 {
      $dbobj = new DB();
      $dbobj->getCon();	    
	  $ins= $dbobj->select("select * from message where `id_to`='".$id_to."' and `type_to`='".$type."' AND `status`='0'");
	  $g=0;
	  while($row=$dbobj->fetch_array($ins))
	    {
	        $inboxx[$g]['id']=$row['id'];
			$inboxx[$g]['subject']=$row['subject'];
			$inboxx[$g]['body']=$row['body'];
			$inboxx[$g]['type_from']=$row['type_from'];
			$inboxx[$g]['type_to']=$row['type_to'];
			$inboxx[$g]['id_from']=$row['id_from'];
			$user_det=$dbobj->user_details($row['type_from'],$row['id_from']);
			$to_userDet=$dbobj->user_details_to($row['type_to'],$row['id_to']);
			$inboxx[$g]['from_name']=$user_det['name'];
			$inboxx[$g]['to_name']=$to_userDet['name'];
			$inboxx[$g]['id_to']=$row['id_to'];
			$inboxx[$g]['date']=$row['date'];
			$inboxx[$g]['attachment']=$row['attachment'];
			$inboxx[$g]['seen']=$row['seen'];
			$g++;
			
	    }
	  return $inboxx;
	 }
	 function deleted_mail($id_to,$type)
	 {
      $dbobj = new DB();
      $dbobj->getCon();	    
	  $ins= $dbobj->select("select * from message where `id_to`='".$id_to."' and `type_to`='".$type."' AND `status`='1'");
	  $g=0;
	  while($row=$dbobj->fetch_array($ins))
	    {
	        $inboxx[$g]['id']=$row['id'];
			$inboxx[$g]['subject']=$row['subject'];
			$inboxx[$g]['body']=$row['body'];
			$inboxx[$g]['type_from']=$row['type_from'];
			$inboxx[$g]['type_to']=$row['type_to'];
			$inboxx[$g]['id_from']=$row['id_from'];
			$user_det=$dbobj->user_details($row['type_from'],$row['id_from']);
			$to_userDet=$dbobj->user_details_to($row['type_to'],$row['id_to']);
			$inboxx[$g]['from_name']=$user_det['name'];
			$inboxx[$g]['to_name']=$to_userDet['name'];
			$inboxx[$g]['id_to']=$row['id_to'];
			$inboxx[$g]['date']=$row['date'];
			$inboxx[$g]['attachment']=$row['attachment'];
			$inboxx[$g]['seen']=$row['seen'];
			$g++;
			
	    }
	  return $inboxx;
	 }
	function fetch_send_mail($id_from,$type)
	 {
      $dbobj = new DB();
      $dbobj->getCon();
	  $ins= $dbobj->select("select * from `message` where `id_from`='".$id_from."' and `type_from`='".$type."' order by `id` DESC");
	  $g=0;
	  while($row=$dbobj->fetch_array($ins))
	    {    
	        $sendmail[$g]['id']=$row['id'];
			$sendmail[$g]['subject']=$row['subject'];
			$sendmail[$g]['body']=$row['body'];
			$sendmail[$g]['type_from']=$row['type_from'];
			$sendmail[$g]['type_to']=$row['type_to'];
			$sendmail[$g]['id_from']=$row['id_from'];
			$sendmail[$g]['id_to']=$row['id_to'];
			$sendmail[$g]['date']=$row['date'];
			$sendmail[$g]['attachment']=$row['attachment'];
			$sendmail[$g]['seen']=$row['seen'];
			$user_det=$dbobj->user_details($row['type_to'],$row['id_to']);
			$to_userDet=$dbobj->user_details_to($row['type_to'],$row['id_to']);
			$sendmail[$g]['from_name']=$user_det['name'];
			$sendmail[$g]['to_name']=$to_userDet['name'];
			$g++;
	    }
	  return $sendmail;
	 } 
 function fetch_draft_message($id_from,$type)
	 {
      $dbobj = new DB();
      $dbobj->getCon();
	  $ins= $dbobj->select("select * from `message_draft` where `userid`='".$id_from."' and `draft_type`='message' order by `id` DESC");
	  $g=0;
	  while($row=$dbobj->fetch_array($ins))
	    {    
	        $sendmail[$g]['id']=$row['id'];
			$sendmail[$g]['subject']=$row['subject'];
			$sendmail[$g]['body']=$row['body'];
			$g++;
	    }
	  return $sendmail;
	 } 
	 function fetch_send_mail_group($id_from,$type){
		$dbobj = new DB();
		$dbobj->getCon();
		$ins= $dbobj->select("SELECT DISTINCT `message`.`tocken`,`subject`,`date`,`message_head`.`id`,`message_head`.`type`,`message_head`.`type_data` from `message` INNER JOIN `message_head` ON `message`.`tocken`=`message_head`.`tocken` where `id_from`='".$id_from."' and `type_from`='".$type."' and `status`='0' order by `message`.`id` DESC");
		$g=0;
		while($row=$dbobj->fetch_array($ins))
		  {  
			$sendmail[$g]['id']=$row['id'];
			  $sendmail[$g]['type']=$row['type'];
			  $sendmail[$g]['type_data']=$row['type_data'];  
			  $sendmail[$g]['subject']=$row['subject'];
			  $sendmail[$g]['tocken']=$row['tocken'];
			  $sendmail[$g]['date']=$row['date'];
			  //$sendmail[$g]['num_count']=$row['num_count'];
			  $g++;
		  }
		return $sendmail;
	 }
	 function fetch_send_mail_groupDel($id_from,$type){
		$dbobj = new DB();
		$dbobj->getCon();
		$ins= $dbobj->select("SELECT DISTINCT `tocken`,`subject`,`date` from `message` where `id_from`='".$id_from."' and `type_from`='".$type."' and `status`='1' order by `id` DESC");
		$g=0;
		while($row=$dbobj->fetch_array($ins))
		  {    
			  $sendmail[$g]['subject']=$row['subject'];
			  $sendmail[$g]['tocken']=$row['tocken'];
			  $sendmail[$g]['date']=$row['date'];
			  //$sendmail[$g]['num_count']=$row['num_count'];
			  $g++;
		  }
		return $sendmail;
	 }
	function fetch_mail_by_reply($msgid)
	 {
      $dbobj = new DB();
      $dbobj->getCon();	
	  $ins= $dbobj->select("select * from reply_mail where `msgid`='".$msgid."'");
	  $g=0;
	  while($row=$dbobj->fetch_array($ins))
	    {
	        $reply[$g]['id']=$row['id'];
			$reply[$g]['subject']=$row['subject'];
			$reply[$g]['body']=$row['body'];
			$reply[$g]['type_from']=$row['type_from'];
			$reply[$g]['type_to']=$row['type_to'];
			$reply[$g]['id_from']=$row['id_from'];
			$reply[$g]['id_to']=$row['id_to'];
			$reply[$g]['date']=$row['date'];
			$reply[$g]['attachment']=$row['attachment'];
			$reply[$g]['seen']=$row['seen'];
			if($row['type_from']=="Admin" || "Teacher")
			{ 
			$chk= $dbobj->select("select * from teacher where `userid`='".$row['id_from']."'");
			$ind= $dbobj->fetch_array($chk);
			$reply[$g]['pname']=$ind['name'];
			}
			if($row['type_from']=="parent")
			{
			$chk= $dbobj->select("select * from parent where `id`='".$row['id_from']."'");
			$ind= $dbobj->fetch_array($chk);
			$reply[$g]['pname']=$ind['fname'];
			}
			$g++;
	    }
	  return $reply;
	 } 
	 
    function fetch_mail_and_reply($id_to,$type,$msgid)
	 {
      $dbobj = new DB();
      $dbobj->getCon();
      //echo $type;  
	   $ins= $dbobj->select("select * from message where id_to='".$id_to."' and type_to='".$type."' and id='".$msgid."'");
      //echo "select * from message where id_from='".$id_to."' and type_from='".$type."' and id='".$msgid."'";echo "<br>";
	  $c=mysql_num_rows($ins);
	  $inss= $dbobj->select("select * from reply_mail where id_from='".$id_to."' and type_from='".$type."' and msgid='".$msgid."'");
	  //echo "select * from reply_mail where id_to='".$id_to."' and type_to='".$type."' and msgid='".$msgid."'";echo "<br>";
	  $co=mysql_num_rows($inss);
	  $count=$c+$co;
	  return $count;
	 }	 
	function fetch_sent_mail_and_reply($id_from,$type,$msgid)
	 {
      $dbobj = new DB();
      $dbobj->getCon();
      //echo $id_from;
      //echo $type;
      //echo $msgid;	  
	  $ins= $dbobj->select("select * from message where id_from='".$id_from."' and type_from='".$type."' and id='".$msgid."'");
      //echo "select * from message where id_from='".$id_from."' and type_from='".$type."' and id='".$msgid."'";echo "<br>";
	  $c=mysql_num_rows($ins);
	  $inss= $dbobj->select("select * from reply_mail where id_to='".$id_from."' and type_to='".$type."' and msgid='".$msgid."'");
	  //echo "select * from reply_mail where id_to='".$id_from."' and type_to='".$type."' and msgid='".$msgid."'";echo "<br>";
	  $co=mysql_num_rows($inss);
	  $count=$c+$co;
	  return $count;
	 }
	function no_of_mail($id_to,$type)
	 {
	  $dbobj = new DB();
      $dbobj->getCon();	 
	  $ins= $dbobj->select("select * from message where `seen`='' and `id_to`='".$id_to."' and `type_to`='".$type."'");
	  $g=0;
	  while($row=$dbobj->fetch_array($ins))
	    {
			$g++;
	    }
	  return $g;
	 }
	function fetch_mail_details($msg_id)
	 {
	  $dbobj = new DB();
	  $tobj = new Teacher();
      $dbobj->getCon();	 
	  $fetch= $dbobj->select("select * from message where `id`='".$msg_id."'");
	  $fth= $dbobj->fetch_array($fetch);
	  $details['id']=$fth['id'];
	  $details['subject']=$fth['subject'];
	  $details['body']=$fth['body'];
	  $details['type_from']=$fth['type_from'];
	  $details['type_to']=$fth['type_to'];
	  $details['id_from']=$fth['id_from'];
	  $details['id_to']=$fth['id_to'];
	  $details['date']=$fth['date'];
	  $details['attachment']=$fth['attachment'];
	  $details['seen']=$fth['seen'];
	  $details['tname']=$tobj->get_teacher_name_by_id($fth['id_from']);
	  $details['fname']=$this->get_parent_name_by_id($fth['id_to']);
	  return $details;
	 }
	 
	function fetch_mail_teacher_details($msg_id)
	 {
	  $dbobj = new DB();
	  $tobj = new Teacher();
      $dbobj->getCon();	 
	  $fetch= $dbobj->select("select * from message where `id`='".$msg_id."'");
	  $fth= $dbobj->fetch_array($fetch);
	  $details['id']=$fth['id'];
	  $details['subject']=$fth['subject'];
	  $details['body']=$fth['body'];
	  $details['type_from']=$fth['type_from'];
	  $details['type_to']=$fth['type_to'];
	  $details['id_from']=$fth['id_from'];
	  $details['id_to']=$fth['id_to'];
	  $details['date']=$fth['date'];
	  $details['attachment']=$fth['attachment'];
	  $details['seen']=$fth['seen'];
	  $details['tname']=$tobj->get_teacher_name_by_id($fth['id_from']);
	  $details['fname']=$this->get_parent_name_by_id($fth['id_to']);
	  return $details;
	 } 
	function fetch_mail_teacher_detailss($msg_id)
	 {
	  $dbobj = new DB();
	  $tobj = new Teacher();
      $dbobj->getCon();	 
	  $fetch= $dbobj->select("select * from message where `id`='".$msg_id."'");
	  $fth= $dbobj->fetch_array($fetch);
	  $details['id']=$fth['id'];
	  $details['subject']=$fth['subject'];
	  $details['body']=$fth['body'];
	  $details['type_from']=$fth['type_from'];
	  $details['type_to']=$fth['type_to'];
	  $details['id_from']=$fth['id_from'];
	  $details['id_to']=$fth['id_to'];
	  $details['date']=$fth['date'];
	  $details['attachment']=$fth['attachment'];
	  $details['seen']=$fth['seen'];
	  if($fth['id_from']=="admin")
	  {
	  $details['tname']=$tobj->get_teacher_name_by_id($fth['id_to']);
	  $details['fname']=$this->get_parent_name_by_id($fth['id_to']);
	  }
	  else
	  {
	  $details['tname']=$tobj->get_teacher_name_by_id($fth['id_from']);
	  $details['fname']=$this->get_parent_name_by_id($fth['id_from']);
	  }
	  return $details;
	 } 
    function get_parent_name_by_id($pid)
	 {
		$dbobject = new DB();
		$dbobject->getCon();
		//$acyear=$dbobject->get_acyear();
		$sql=$dbobject->select("select * from `parent` where `id`='".$pid."'");
		$thr= $dbobject->fetch_array($sql);
	    $parent=$thr['fname'];
		return $parent;
	 }
	function get_position_by_id($tid)
	 {
		$dbobject = new DB();
		$dbobject->getCon();
		//$acyear=$dbobject->get_acyear();
		$sql=$dbobject->select("select * from `teacher` where `userid`='".$tid."'");
		$thr= $dbobject->fetch_array($sql);
		if($thr['group']=="TEACHER"){
			$teacher="staff";
		}else{
			$teacher=$thr['group'];
		}
	    
		return $teacher;
	 }       	 
	function mark_read($msgid)
	 {
		$dbobject = new DB();
		$dbobject->getCon();
		//$acyear=$dbobject->get_acyear();
		$sql=$dbobject->exe_qry("UPDATE `message` SET `seen`='true' WHERE `id`=$msgid");
	 }
	function fetch_send_mail_by_userid_to($id_from,$type,$id_to)
	 {
      $dbobj = new DB();
      $dbobj->getCon();	
	  $ins= $dbobj->select("select * from message where `id_from`='".$id_from."' and `type_from`='".$type."' and `id_to`='".$id_to."'");
	  $g=0;
	  while($row=$dbobj->fetch_array($ins))
	    {    
	        $sendmail[$g]['id']=$row['id'];
			$sendmail[$g]['subject']=$row['subject'];
			$sendmail[$g]['body']=$row['body'];
			$sendmail[$g]['type_from']=$row['type_from'];
			$sendmail[$g]['type_to']=$row['type_to'];
			$sendmail[$g]['id_from']=$row['id_from'];
			$sendmail[$g]['id_to']=$row['id_to'];
			$sendmail[$g]['date']=$row['date'];
			$sendmail[$g]['attachment']=$row['attachment'];
			$sendmail[$g]['seen']=$row['seen'];
			$user_det=$dbobj->user_details($row['type_to'],$row['id_to']);
			$sendmail[$g]['to']=$user_det['name'];
			$g++;
	    }
	  return $sendmail;
	 } 
	function fetch_send_mail_by_distinct_subject($id_from,$type)
	 {
      $dbobj = new DB();
      $dbobj->getCon();	
	  $ins= $dbobj->select("select distinct `subject`,`date` from message where `id_from`='".$id_from."' and `type_from`='".$type."' order by `date`");
	  $g=0;
	  while($row=$dbobj->fetch_array($ins))
	    {    
	        
			$sendmail[$g]['subject']=$row['subject'];
			$sendmail[$g]['date']=$row['date'];

			$g++;
	    }
	  return $sendmail;
	 }
	function fetch_send_mail_by_distinct_subject_date($id_from,$type,$subject,$date)
	 {
      $dbobj = new DB();
      $dbobj->getCon();	
	  $ins= $dbobj->select("select * from message where `id_from`='".$id_from."' and `type_from`='".$type."' and `subject`='".$subject."' and `date`='".$date."'");
	  $g=0;
	  while($row=$dbobj->fetch_array($ins))
	    {    
	        $sendmail[$g]['id']=$row['id'];
			$sendmail[$g]['subject']=$row['subject'];
			$sendmail[$g]['body']=$row['body'];
			$sendmail[$g]['type_from']=$row['type_from'];
			$sendmail[$g]['type_to']=$row['type_to'];
			$sendmail[$g]['id_from']=$row['id_from'];
			$sendmail[$g]['id_to']=$row['id_to'];
			$sendmail[$g]['date']=$row['date'];
			$sendmail[$g]['attachment']=$row['attachment'];
			$sendmail[$g]['seen']=$row['seen'];
			$user_det=$dbobj->user_details($row['type_to'],$row['id_to']);
			$sendmail[$g]['to']=$user_det['name']." ".$user_det['lname'];
			$g++;
	    }
	  return $sendmail;
	 } 	 
	function fetch_send_mail_by_distinct_type($id_from,$type,$subject,$date)
	 {
      $dbobj = new DB();
      $dbobj->getCon();	

	  $ins= $dbobj->select("select distinct `type_to` from message where `id_from`='".$id_from."' and `type_from`='".$type."' and `subject`='".$subject."' and `date`='".$date."' order by `date`");
	  $g=0;
	  while($row=$dbobj->fetch_array($ins))
	    {    
	        
			$sendmail[$g]['type_to']=$row['type_to'];
			

			$g++;
	    }
	  return $sendmail;
	 }	
	function fetch_send_mail_by_distinct_subject_date_type_to($id_from,$type,$subject,$date,$type_to)
	 {
      $dbobj = new DB();
      $dbobj->getCon();	
	  $ins= $dbobj->select("select * from message where `id_from`='".$id_from."' and `type_from`='".$type."' and `subject`='".$subject."' and `date`='".$date."' and `type_to`='".$type_to."'");
	  $g=0;
	  while($row=$dbobj->fetch_array($ins))
	    {    
	        $sendmail[$g]['id']=$row['id'];
			$sendmail[$g]['subject']=$row['subject'];
			$sendmail[$g]['body']=$row['body'];
			$sendmail[$g]['type_from']=$row['type_from'];
			$sendmail[$g]['type_to']=$row['type_to'];
			$sendmail[$g]['id_from']=$row['id_from'];
			$sendmail[$g]['id_to']=$row['id_to'];
			$sendmail[$g]['date']=$row['date'];
			$sendmail[$g]['attachment']=$row['attachment'];
			$sendmail[$g]['seen']=$row['seen'];
			$user_det=$dbobj->user_details($row['type_to'],$row['id_to']);
			$sendmail[$g]['to']=$user_det['name']." ".$user_det['lname'];
			$g++;
	    }
	  return $sendmail;
	 }
	function fetch_send_mail_by_distinct_subject_date_body($id_from,$type,$subject,$date)
	 {
      $dbobj = new DB();
      $dbobj->getCon();	
	  $ins= $dbobj->select("select distinct body from message where `id_from`='".$id_from."' and `type_from`='".$type."' and `subject`='".$subject."' and `date`='".$date."'");
	  $g=0;
	  while($row=$dbobj->fetch_array($ins))
	    {    

			$sendmail[$g]['body']=$row['body'];
			$g++;
	    }
	  return $sendmail;
	 }  
	function fetch_send_mail_by_distinct_subject_new($id_from,$type)
	 {
      $dbobj = new DB();
      $dbobj->getCon();
//echo "select distinct `subject`,`date` from message_history where `from_id`='".$id_from."' and `from_type`='".$type."' order by `date`";	  
	  $ins= $dbobj->select("select distinct `subject`,`date` from message_history where `from_id`='".$id_from."' and `from_type`='".$type."' order by `date`");
	  $g=0;
	  while($row=$dbobj->fetch_array($ins))
	    {    
	        
			$sendmail[$g]['subject']=$row['subject'];
			$sendmail[$g]['date']=$row['date'];

			$g++;
	    }
	  return $sendmail;
	 }	 
	function fetch_send_mail_by_userid_to_new($id_from,$type,$id_to)
	 {
      $dbobj = new DB();
      $dbobj->getCon();	
	  $ins= $dbobj->select("select * from message_history where `from_id`='".$id_from."' and `from_type`='".$type."' and `to_id`='".$id_to."'");
	  $g=0;
	  while($row=$dbobj->fetch_array($ins))
	    {    
	        $sendmail[$g]['id']=$row['id'];
			$sendmail[$g]['subject']=$row['subject'];
			$sendmail[$g]['body']=$row['msg'];
			$sendmail[$g]['type_from']=$row['type_from'];
			$sendmail[$g]['type_to']=$row['to_type'];
			$sendmail[$g]['id_from']=$row['to_id'];
			$sendmail[$g]['id_to']=$row['to_id'];
			$sendmail[$g]['date']=$row['date'];
			$sendmail[$g]['attachment']=$row['attachment'];
			$sendmail[$g]['seen']=$row['seen'];
			$user_det=$dbobj->user_details($row['to_type'],$row['to_id']);
			$sendmail[$g]['to']=$user_det['name'];
			$g++;
	    }
	  return $sendmail;
	 }	 
		function fetch_send_mail_by_distinct_type_new($id_from,$type,$subject,$date)
	 {
      $dbobj = new DB();
      $dbobj->getCon();	

	  $ins= $dbobj->select("select distinct `to_type` from message_history where `from_id`='".$id_from."' and `from_type`='".$type."' and `subject`='".$subject."' and `date`='".$date."' order by `date`");
	  $g=0;
	  while($row=$dbobj->fetch_array($ins))
	    {    
	        
			$sendmail[$g]['to_type']=$row['to_type'];
			

			$g++;
	    }
	  return $sendmail;
	 }  
	function fetch_send_mail_by_userid_to_new_mail($id_from,$type,$id_to)
	 {
      $dbobj = new DB();
      $dbobj->getCon();	
	  $ins= $dbobj->select("select * from email_history where `from_id`='".$id_from."' and `from_type`='".$type."' and `to_id`='".$id_to."'");
	  $g=0;
	  while($row=$dbobj->fetch_array($ins))
	    {    
	        $sendmail[$g]['id']=$row['id'];
			$sendmail[$g]['subject']=$row['subject'];
			$sendmail[$g]['body']=$row['msg'];
			$sendmail[$g]['type_from']=$row['type_from'];
			$sendmail[$g]['type_to']=$row['to_type'];
			$sendmail[$g]['id_from']=$row['to_id'];
			$sendmail[$g]['id_to']=$row['to_id'];
			$sendmail[$g]['date']=$row['date'];
			$sendmail[$g]['attachment']=$row['attachment'];
			$sendmail[$g]['seen']=$row['seen'];
			$user_det=$dbobj->user_details($row['to_type'],$row['to_id']);
			$sendmail[$g]['to']=$user_det['name'];
			$g++;
	    }
	  return $sendmail;
	 }	 
	function no_of_mail_sent($id_from,$type)
	 {
	  $dbobj = new DB();
      $dbobj->getCon();
	  $g=0;
	  $ins= $dbobj->select("select DISTINCT `tocken`  from `message` where  `id_from`='".$id_from."' and `type_from`='".$type."' and `status`='0' group by `tocken`");
	  $g=$dbobj->mysql_num_row($ins);
	  return $g;
	 }
	 function no_of_mail_draft($id_from)
	 {
	  $dbobj = new DB();
      $dbobj->getCon();
	  $g=0;
	  $ins= $dbobj->select("select COUNT(`id`) as `num_count`  from `message_draft` where  `userid`='".$id_from."' and `draft_type`='message'");
	  $g=$dbobj->fetch_array($ins);
	  return $g['num_count'];
	 }
	 function no_of_mail_Delete($id_from,$type_to)
	 {
	  $dbobj = new DB();
      $dbobj->getCon();
	  $g=0;
	  $ins= $dbobj->select("select DISTINCT `tocken` from `message` where  `id_from`='".$id_from."' and `type_from`='".$type_to."' and `status`='1'");
	  $g=$dbobj->mysql_num_row($ins);
	  return $g;
	 }
	function fetch_message_teacher_details($msg_id)
	 {
	  $dbobj = new DB();
	  $tobj = new Teacher();
      $dbobj->getCon();	 
	  $fetch= $dbobj->select("select `tocken`,`subject`,`date`,`body`, COUNT(`tocken`) as num_count from message where `tocken`='".$msg_id."'");
	  $fth= $dbobj->fetch_array($fetch);
	  $details['id']=$fth['id'];
	  $details['subject']=$fth['subject'];
	  $details['body']=$fth['body'];
	  $details['date']=$fth['date'];
	  $details['num_count']=$fth['num_count'];
	  $details['attachment']=$fth['attachment'];
	  $user_details=$dbobj->user_details($fth['to_type'],$fth['to_id']);
	  $details['name']=$user_details['name'];
	  return $details;
	 }	 
}
?>
