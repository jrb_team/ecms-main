<?php
include_once('createdb.php');
class PoliPayment
{
	public $poli_settings=array();
	public function __construct()
	{
		$object_poli=new DB();
		$object_poli->getCon();
		$this->poli_settings=$object_poli->selectall("pgpayment",array("pg_name"=>"polipay"));		
	}
	function BaseURL()
	{
		$url="https://poliapi.apac.paywithpoli.com/api/v2/Transaction/";
		return $url;
	}
	function TransactionId(){
		return substr(hash('sha256', mt_rand() . microtime()), 0, 20);
	}
	function Initiate(){
		$url=$this->BaseURL()."Initiate";
		return $url;
	}
	function GETTransaction(){
		$url=$this->BaseURL()."GetTransaction";
		return $url;
	}
	function MerchantKey()
	{
		$key=$this->poli_settings['MerchantKey'];
		return $key;
	}
	function Salt()
	{
		$salt=$this->poli_settings['Salt'];
		return $salt;
	}
	function authorization_Header()
	{
		$auth=$this->poli_settings['auth_head'];
		return $auth;
	}
	function sURL($family_code,$group)
	{
		if($group=="admin"){
			$url=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/admin/PolyPaymentUpdation.php";
		}elseif($group=="parent"){
			$url=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/parent/PolyPaymentUpdation.php";
		}else{
			$url=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/staff/PolyPaymentUpdation.php";
		}
		return $url;
	}
	function fURL_PayCollection($family_code,$group)
	{
		if($group=="admin"){
			$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/admin/cancel_poly.php?type=family&family_code=".$family_code;
		}elseif($group=="parent"){
			$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/parent/cancel_poly.php?type=family&family_code=".$family_code;
		}else{
			$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/staff/cancel_poly.php?type=family&family_code=".$family_code;
		}
		return $url;
	}
	function fURL_Renew()
	{
		$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/admin/RenewAction.php";
		return $url;
	}
	function cURL_Renew()
	{
		$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/admin/renewAccount.php";
		return $url;
	}
	function cURL($family_code,$group)
	{
		if($group=="admin"){
			$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/admin/cancel_poly.php?type=family&family_code=".$family_code;
		}elseif($group=="parent"){
			$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/parent/cancel_poly.php?type=family&family_code=".$family_code;
		}else{
			$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/staff/cancel_poly.php?type=family&family_code=".$family_code;
		}
		return $url;
	}
	function nURL($ref)
	{
		$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/admin/Polinudge.php?ref=".urlencode($ref);
		return $url;
	}
	function ProductInfo()
	{
		$product="SchoolFee";
		return $product;
	}
	function ServiceProvider()
	{
		$service_provider="payu_paisa";
		return $service_provider;
	}
	function currency(){
		$currency="AUD";
		return $currency;
	}
	function PageURL(){
		$PageURL="https://payamdari.com/";
		return $PageURL;
	}
	
}
?>