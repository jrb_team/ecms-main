<?php
include_once('createdb.php');
class Payroll
{
function get_Month(){
    $data[0]="January";
    $data[1]="February";
    $data[2]="March";
    $data[3]="April";
    $data[4]="May";
    $data[5]="June";
    $data[6]="July";
    $data[7]="August";
    $data[8]="September";
    $data[9]="October";
    $data[10]="October";
    $data[11]="December";
    return $data;
}
public function get_Grouphead($groupid,$type){
    $dbobject = new DB();
	$dbobject->getCon();
    $sel=$dbobject->select("SELECT * FROM `payroll_settings` WHERE `head_type`='".$type."' AND `groupid`='".$groupid."' AND `status`='0'");
    $data=array();
    $i=0;
    while($row=$dbobject->fetch_array($sel)){
        $data[$i]['id']=$row['id'];
        $data[$i]['head']=$row['head'];
        $data[$i]['groupid']=$row['groupid'];
        $data[$i]['value']=$row['value'];
        $data[$i]['type']=$row['type'];
    $i++;
    }
    return $data;
}
function get_group($acyear){
    $dbobject = new DB();
	$dbobject->getCon();
    $data=array();
    $i=0;
    $sel=$dbobject->select("SELECT `id`,`head` FROM `payroll_heads` WHERE `acyear`='".$acyear."' AND `status`='0'");
    while($row=$dbobject->fetch_array($sel)){
        $data[$i]['id']=$row['id'];
        $data[$i]['head']=$row['head'];
      $i++;   
    }
    return $data;
}
function get_headsDistinct($groupid,$type){
    $dbobject = new DB();
	$dbobject->getCon();
    $sel=$dbobject->select("SELECT `head`,`value`,`type` FROM `payroll_settings` WHERE `head_type`='".$type."' AND `groupid`='".$groupid."' AND `status`='0'");
    $data=array();
    $i=0;
    while($row=$dbobject->fetch_array($sel)){
        $data[$i]['head']=$row['head'];
        $data[$i]['value']=$row['value'];
        $data[$i]['type']=$row['type'];
        $i++;
    }
    return $data;
}
function getStaffLIst($groupid){
    $dbobject = new DB();
	$dbobject->getCon();
    $i=0;
    $data=array();
    $sel=$dbobject->select("SELECT `payroll_salary`.*,`teacher`.`userid`,`teacher`.`name`,`teacher`.`middle_name`,`teacher`.`lname` FROM `payroll_salary` LEFT JOIN `teacher` ON `teacher`.`id` = `payroll_salary`.`staff_id` WHERE `groupid`='".$groupid."'");
    while($row=$dbobject->fetch_array($sel)){
        $data[$i]['tid']=$row['staff_id'];
        $data[$i]['userid']=$row['userid'];
        $data[$i]['name']=$row['name'];
        $data[$i]['middle_name']=$row['middle_name'];
        $data[$i]['lname']=$row['lname'];
        $data[$i]['salary']=$row['salary'];
        $data[$i]['groupid']=$row['groupid'];
        $i++;
    }
    return $data;
}
function calChead($salary,$head){
    $value=0;
 if($head['type']=="Fixed"){
     $value=$head['value'];
 }elseif($head['type']=="Percentage"){
     $value=($salary/100)*$head['value'];
 }
 return $value;
}
function Calc_workHours($groupid,$tid,$work_hrs,$acyear){
    return 12;
}
function CalcStaffSalary($groupid,$salary,$tid,$work_hrs,$staff_workHours,$acyear){
    $work_minute=$work_hrs*60;
    $staff_minute=$staff_workHours*60;
    $salary=$salary/60;
    $salary=round($staff_minute*$salary,2);
    return $salary;
}
}
?>