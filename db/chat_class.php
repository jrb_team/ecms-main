<?php
include_once('createdb.php');
class onlinechat
{
	function chat_by_type_and_typid($type,$type_id,$type_to,$id_to)
	{
		$dbobj = new DB();
		$dbobj->getCon();		
		$sql="SELECT * FROM `online_chat` WHERE `type`='".$type."' and `type_id`='".$type_id."' and `type_to`='".$type_to."' and `id_to`='".$id_to."'";
		$qry=$dbobj->select($sql);
		$i=0;
		while($row=$dbobj->fetch_array($qry))
		{
			$data[$i]['id']=$row['id'];
			$data[$i]['body']=$row['body'];
			$data[$i]['type_from']=$row['type_from'];
			$data[$i]['type_to']=$row['type_to'];
			$data[$i]['id_from']=$row['id_from'];
			$data[$i]['id_to']=$row['id_to'];
			$data[$i]['date']=$row['date'];
			$data[$i]['type']=$row['type'];
			$data[$i]['type_id']=$row['type_id'];
			$data[$i]['seen']=$row['seen'];
			$i++;
		}
		return $data;
	}
	function chat_reply_by_chatid($chatid)
	{
		$dbobj = new DB();
		$dbobj->getCon();		
		$sql="SELECT * FROM `online_chat_reply` WHERE `chatid`='".$chatid."'";
		$qry=$dbobj->select($sql);
		$i=0;
		while($row=$dbobj->fetch_array($qry))
		{
			$data[$i]['id']=$row['id'];
			$data[$i]['reply']=$row['reply'];
			$data[$i]['date_reply']=$row['date_reply'];
			$data[$i]['seen_flag']=$row['seen_flag'];
			$i++;
		}
		return $data;
	}
	function chat_insert_by_user($body,$type_from,$type_to,$id_from,$id_to,$type,$type_id,$entry_by)
	{
		$dbobj = new DB();
		$dbobj->getCon();
        $date=date('Y-m-d h:i:sa');		
		$sql="INSERT INTO `online_chat`(`body`, `type_from`, `type_to`, `id_from`, `id_to`, `date`, `type`, `type_id`,`entry_by`) VALUES ('".$body."', '".$type_from."', '".$type_to."', '".$id_from."', '".$id_to."', '".$date."', '".$type."', '".$type_id."','".$entry_by."')";
		$qry=$dbobj->exe_qry($sql);
        $chatid=mysql_insert_id();
		return $chatid;
	}
	function chat_conversation_type_typeid($type,$type_id,$type_to,$id_to,$type_from,$id_from)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$sql="SELECT * FROM `online_chat` WHERE `type`='".$type."' and `type_id`='".$type_id."'";
		$sql.=" and `type_from` IN ('".$type_to."','".$type_from."') and `id_to` IN ('".$id_to."','".$id_from."') and `id_from` IN ('".$id_to."','".$id_from."')";
		$sql.="ORDER BY `online_chat`.`id` ASC";
		$qry=$dbobj->select($sql);
		$i=0;
		while($row=$dbobj->fetch_array($qry))
		{
			$data[$i]['id']=$row['id'];
			$data[$i]['body']=$row['body'];
			$data[$i]['type_from']=$row['type_from'];
			$data[$i]['type_to']=$row['type_to'];
			$data[$i]['id_from']=$row['id_from'];
			$data[$i]['id_to']=$row['id_to'];
			$data[$i]['date']=$row['date'];
			$data[$i]['type']=$row['type'];
			$data[$i]['type_id']=$row['type_id'];
			$data[$i]['seen']=$row['seen'];
			$i++;
		}
		return $data;
	}
	function chat_conversation_learnid($learnid)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$sql="SELECT * FROM `online_chat` WHERE `type`='learning_mod' and `type_id`='".$learnid."'";
		//$sql.=" and `type_from` IN ('".$type_to."','".$type_from."') and `id_to` IN ('".$id_to."','".$id_from."') and `id_from` IN ('".$id_to."','".$id_from."')";
		$sql.="ORDER BY `online_chat`.`id` ASC";
		$qry=$dbobj->select($sql);
		$i=0;
		while($row=$dbobj->fetch_array($qry))
		{
			$data[$i]['id']=$row['id'];
			$data[$i]['body']=$row['body'];
			$data[$i]['type_from']=$row['type_from'];
			$data[$i]['type_to']=$row['type_to'];
			$data[$i]['id_from']=$row['id_from'];
			$data[$i]['id_to']=$row['id_to'];
			$data[$i]['date']=$row['date'];
			$data[$i]['type']=$row['type'];
			$data[$i]['type_id']=$row['type_id'];
			$data[$i]['seen']=$row['seen'];
			$i++;
		}
		return $data;
	}
function chat_username($type,$id)
{
		$dbobject = new DB();
		$dbobject->getCon();
        if($type=="teacher")
		{
			$user_det=$dbobject->selectall("teacher",array("id"=>$id));
			$name=$user_det['tfname']." ".$user_det['tlname'];
		}
        elseif($type=="parent")
		{
			$user_det=$dbobject->selectall("parent",array("id"=>$id));
			$name=$user_det['fname'];
		}
         return $name;		
}
function lastchatid($learn_id)
{
		$dbobject = new DB();
		$dbobject->getCon();
		$sql="SELECT * FROM `online_chat` WHERE `type`='learning_mod' and `type_id`='".$learn_id."'";
		$sql.="ORDER BY `online_chat`.`id` DESC";
		$qry=$dbobject->select($sql);
		$row=$dbobject->fetch_array($qry);		
         return $row['id'];		
}
	function latest_conversation_learnid($learnid,$lastchatid)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		$sql="SELECT * FROM `online_chat` WHERE `type`='learning_mod' and `type_id`='".$learnid."'";
		//$sql.=" and `type_from` IN ('".$type_to."','".$type_from."') and `id_to` IN ('".$id_to."','".$id_from."') and `id_from` IN ('".$id_to."','".$id_from."')";
		$sql.="and `id`>'".$lastchatid."' ORDER BY `online_chat`.`id` ASC";
		$qry=$dbobj->select($sql);
		$i=0;
		while($row=$dbobj->fetch_array($qry))
		{
			$data[$i]['id']=$row['id'];
			$data[$i]['body']=$row['body'];
			$data[$i]['type_from']=$row['type_from'];
			$data[$i]['type_to']=$row['type_to'];
			$data[$i]['id_from']=$row['id_from'];
			$data[$i]['id_to']=$row['id_to'];
			$data[$i]['date']=$row['date'];
			$data[$i]['type']=$row['type'];
			$data[$i]['type_id']=$row['type_id'];
			$data[$i]['seen']=$row['seen'];
			$i++;
		}
		return $data;
	}
	function insert_online_chat_tempid($learnid,$lastchatid,$user_type,$userid)
	{
		$dbobj = new DB();
		$dbobj->getCon();
		//$del=$dbobj->exe_qry("DELETE FROM `online_chat_temp` WHERE `user_type`='".$user_type."' and `userid`='".$userid."' and `type`='learning_mod' and `type_id`='".$learnid."'");
		$sql2="SELECT * FROM `online_chat_temp` WHERE `user_type`='".$user_type."' and `userid`='".$userid."' and `type`='learning_mod' and `type_id`='".$learnid."' ";
		$qry2=$dbobj->select($sql2);
		$chk=mysql_num_rows($qry2);
		if($chk==0)
		{
		$sql="INSERT INTO `online_chat_temp`(`lastchatid`, `user_type`, `userid`, `type`, `type_id`) ";
		$sql.="VALUES ('".$lastchatid."','".$user_type."','".$userid."','learning_mod','".$learnid."')";
		$qry=$dbobj->exe_qry($sql);				
		}
		else
		{
		$row2=$dbobj->fetch_array($qry2);	
		$sql="UPDATE `online_chat_temp` SET `lastchatid`='".$lastchatid."' where `id`='".$row2['id']."'";
		$qry=$dbobj->exe_qry($sql);				
		}
	
	}
	function get_online_chat_tempid($learnid,$user_type,$userid)
	{
		$dbobj = new DB();
		$dbobj->getCon();		
		$sql="SELECT * FROM `online_chat_temp` WHERE `user_type`='".$user_type."' and `userid`='".$userid."' and `type`='learning_mod' and `type_id`='".$learnid."' ";
		$qry=$dbobj->select($sql);
        $row=$dbobj->fetch_array($qry);	
        return 	$row['lastchatid'];	
	}	
}
?>