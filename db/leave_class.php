<?php
include_once('createdb.php');
class Leave
{
	function leave_data($userid,$status,$acyear)
	 {
		$dbobject = new DB();
		$dbobject->getCon(); 
		$date_from= date('Y-m-d', strtotime($date_from));
		$date_to= date('Y-m-d', strtotime($date_to));
		$qry="select `leave_application`.*,`teacher`.`name`,`teacher`.`designation` from  `leave_application` left join `teacher` on `teacher`.`userid`=`leave_application`.`userid` where `leave_application`.`acyear`='".$acyear."'";
		if($userid!="-1")
		{
		$qry.=" and `leave_application`.`userid`='".$userid."'";			
		}
		if($status!="-1")
		{
			$qry.=" and `leave_application`.`approval`='".$status."'";
		}		
       // $qry.=" and `leave_application`.`approval` in ('0','1')";		
		$sql=$dbobject->select($qry);
		$i=0;
		while($row=$dbobject->fetch_array($sql))
		{
			$teacher[$i]['id']=$row['id'];
			$teacher[$i]['userid']=$row['userid'];
			$teacher[$i]['username']=$row['name'];
			$teacher[$i]['fromdate']=$row['fromdate'];
			$teacher[$i]['todate']=$row['todate'];
				$teacher[$i]['title']=$row['title'];
			$teacher[$i]['reason']=$row['reason'];
		$teacher[$i]['designation']=$row['designation'];
			$teacher[$i]['leave_type']=$row['leave_type'];
			$teacher[$i]['leave_mode']=$row['leave_mode'];
			$teacher[$i]['entry_date']=$row['entry_date'];
			$teacher[$i]['entry_by']=$row['entry_by'];
			$teacher[$i]['approval']=$row['approval'];
			$teacher[$i]['approval_date']=$row['approval_date'];
			$teacher[$i]['approval_by']=$row['approval_by'];
			$teacher[$i]['acyear']=$row['acyear'];
            $leave_BY_designation=$this->leave_BY_designation($acyear);
            $teacher[$i]['total_days']=$leave_BY_designation[$row['designation']];				
            $teacher[$i]['leave_taken']=$this->leave_taken($row['userid'],$acyear);				
            if($teacher[$i]['total_days']>=$teacher[$i]['leave_taken'])
			{
			$teacher[$i]['available_days']=$leave_BY_designation[$row['designation']]-$teacher[$i]['leave_taken'];				
			}
			else
			{
			$teacher[$i]['available_days']=0;				
			}
			 $teacher[$i]['no_of_days']=$this->no_of_days($row['fromdate'],$row['todate']);
			$i++;
		}
		return $teacher;
	 }	
	function leave_data_insert($userid,$fromdate,$todate,$acyear,$reason,$leave_type,$leave_mode,$entry_by,$title)
	 {
		$dbobject = new DB();
		$dbobject->getCon();
		$fromdate= date('Y-m-d', strtotime($fromdate));
		$todate= date('Y-m-d', strtotime($todate));		
		$entry_date= date('Y-m-d');		
		$num_of_days=$this->no_of_days($fromdate,$todate);
        $no_of_days=$this->leave_calculate($num_of_days,$leave_type);		
        $insert=$dbobject->exe_qry("INSERT INTO `leave_application`( `userid`, `fromdate`, `todate`, `reason`, `leave_type`, `leave_mode`, `entry_date`, `entry_by`, `acyear`,`no_of_days`,`title`) VALUES ( '".$userid."', '".$fromdate."', '".$todate."', '".addslashes($reason)."', '".$leave_type."', '".$leave_mode."', '".$entry_date."','".$entry_by."','".$acyear."','".$no_of_days."','".$title."')");
        $id=$dbobject->mysql_insert_id();
		return $id;
	 }	
	function leave_data_updation($id,$fromdate,$todate,$reason,$leave_type,$leave_mode,$entry_by,$title)
	 {
		$dbobject = new DB();
		$dbobject->getCon();
		$fromdate= date('Y-m-d', strtotime($fromdate));
		$todate= date('Y-m-d', strtotime($todate));		
		$entry_date= date('Y-m-d');		
		$num_of_days=$this->no_of_days($fromdate,$todate);
        $no_of_days=$this->leave_calculate($num_of_days,$leave_type);
        $update=$dbobject->exe_qry("UPDATE `leave_application` SET `fromdate`='".$fromdate."',`todate`='".$todate."',`reason`='".addslashes($reason)."',`leave_type`='".$leave_type."',`leave_mode`='".$leave_mode."',`entry_date`='".$entry_date."',`entry_by`='".$entry_by."',`no_of_days`='".$no_of_days."',`title`='".$title."' where `id`='".$id."'");
		return $id;
	 }
	 function leave_calculate($no_of_days,$leave_type)
	 {
        if($leave_type=="half_day")
		{
			$no_of_days=$no_of_days/2;
		}
         return $no_of_days;		
	 }	 
	function leave_data_approval_action($id,$approval_by,$val)
	 {
		$dbobject = new DB();
		$dbobject->getCon();
		$approval_date= date('Y-m-d');	
		$delete=$dbobject->exe_qry("UPDATE `leave_application` SET `approval`='".$val."',`approval_by`='".$approval_by."',`approval_date`='".$approval_date."'  WHERE `id`='".$id."'");
		
	 }	

	function leave_data_approval_button_value($approval)
	 {
         if($approval==0)
		 {
			 $data['val']=1;
			 $data['button']="Approve";;
			 
		 }
		 elseif($approval==1)
		 {
			 $data['val']=0;
			 $data['button']="Cancel";;			 
		 }
		 return $data;
		
	 }	 
	function leave_data_delete_action($id,$approval,$userid)
	 {
		$dbobject = new DB();
		$dbobject->getCon();
		$removal_date= date('Y-m-d');
		$delete=$dbobject->exe_qry("DELETE FROM `leave_application` where `id`='".$id."'");
	 }
	function leave_data_reject_action($id,$approval,$userid)
	 {
		$dbobject = new DB();
		$dbobject->getCon();
		$removal_date= date('Y-m-d');
		$delete=$dbobject->exe_qry("UPDATE `leave_application` SET `approval`='2',`approval_date`='".$removal_date."',`approval_by`='".$userid."' where `id`='".$id."'");
	 }	
function leave_type()
{
	return $leave_type=array("full_day","half_day");
}
function leave_mode()
{
	return $leave_mode=array("leave","maternity","other");
}
function leave_type_option()
{
	$leave_type=$this->leave_type();
	if(!empty($leave_type))
	{
	    $i=0;
		foreach($leave_type as $val)
		{
			$name=$this->leave_name($val);
			$data[$i]['name']=$name;
			$data[$i]['value']=$val;
			$i++;
		}
	}
	return $data;
}
function leave_mode_option()
{
	$leave_mode=$this->leave_mode();
	if(!empty($leave_mode))
	{
	   $i=0;
		foreach($leave_mode as $val)
		{
			$name=$this->leave_name($val);
			$data[$i]['name']=$name;
			$data[$i]['value']=$val;
			$i++;
		}
	}
		return $data;
}
function leave_name($val)
{
	if($val=="full_day")
	{
		$data="FULL DAY";
	}
	elseif($val=="half_day")
	{
		$data="HALF DAY";
	}	
	elseif($val=="sick_leave")
	{
		$data="SICK LEAVE";
	}
    else
	{
		$data=$val;
	}	
	return $data;
}	
function leave_BY_designation($acyear)
{
		$dbobject = new DB();
		$dbobject->getCon();	
	    $qry=$dbobject->select("SELECT * FROM `leave_data_staff`");
		while($row=$dbobject->fetch_array($qry))
		{
			$data[$row['designation']]=$row['days'];
		}
		return $data;
}
function leave_taken($userid,$acyear)
{
		$dbobject = new DB();
		$dbobject->getCon(); 
		$sql="select *  from  `leave_application`  where `leave_application`.`acyear`='".$acyear."'";
		$sql.=" and `leave_application`.`userid`='".$userid."'";			
		$sql.=" and `leave_application`.`approval`='1'";
		$holidays=$this->holidays_by_year($acyear);
		$qry=$dbobject->select($sql);
		$no_of_days=0;
		while($row=$dbobject->fetch_array($qry))
		{
			$dt_frm=$row['fromdate'];
			$dt_to=$row['todate'];
			$dateRange=$dbobject->createDateRangeArray1($dt_frm,$dt_to);
			if(!empty($dateRange))
			  {
					foreach($dateRange as $date)
					{
						$date_data[$date]=$date;
					}
			  }				
		}
		if(!empty($date_data))
		{
				$i=0;
			foreach($date_data as $date_dt)
			{
				
					if($holidays[$date_dt]!=$date_dt)
					{
						$weekend=$this->isWeekend($date_dt);                    
						if($weekend!=1)
						{
							$i++;						
						}						
					}				
			}
		}		
		return $i;

}
 
  function no_of_days($dt_frm,$dt_to)
{
   	    $obj=new DB();
		$obj->getCon();     
		$acyear=$obj->get_acyear();     
   		$dateRange=$obj->createDateRangeArray1($dt_frm,$dt_to);
		$holidays=$this->holidays_by_year($acyear);
		if(!empty($dateRange))
		  {
				$i=0;
				foreach($dateRange as $date)
				{
					
					if($holidays[$date]!=$date)
					{
						$weekend=$this->isWeekend($date);                    
						if($weekend!=1)
						{
							$i++;						
						}						
					}

				}
	    }

    return $i;
} 
function holidays_by_year($acyear)
{
   	    $obj=new DB();
		$obj->getCon();
        $qry=$obj->select("SELECT * FROM `calendar_events` where `acyear`='".$acyear."' and  `type`='holiday'");		
        while($row=$obj->fetch_array($qry))
		{
			$dt_frm=$row['st_date'];
			$dt_to=$row['end_date'];
			$dateRange=$obj->createDateRangeArray1($dt_frm,$dt_to);
			if(!empty($dateRange))
			  {
				foreach($dateRange as $date)
					{
						$data[$date]=$date;
					}
			  }				
		}
		return $data;
}
function isWeekend($date)
{
    return (date('N', strtotime($date)) >= 7);
}
function leave_data_admin($acyear)
{
		$dbobject = new DB();
		$dbobject->getCon(); 
        $all_staffid=$dbobject->all_staffid();		
		$i=0;
		if(!empty($all_staffid))
		{
		foreach($all_staffid as $row)
		{
			$teacher[$i]['userid']=$row['userid'];
			$teacher[$i]['username']=$row['name'];
            $leave_BY_designation=$this->leave_BY_designation($acyear);
            $teacher[$i]['total_days']=$leave_BY_designation[$row['designation']];				
            $teacher[$i]['leave_taken']=$this->leave_taken($row['userid'],$acyear);				
            if($teacher[$i]['total_days']>=$teacher[$i]['leave_taken'])
			{
			$teacher[$i]['available_days']=$leave_BY_designation[$row['designation']]-$teacher[$i]['leave_taken'];				
			}
			else
			{
			$teacher[$i]['available_days']=0;				
			}
				
			$i++;
		}
       }
		return $teacher;
}
function leave_data_BY_Id($id,$userid)
{
		$dbobject = new DB();
		$dbobject->getCon(); 
        $row=$dbobject->selectall("leave_application",array("id"=>$id));
        $teacher_det=$dbobject->selectall("teacher",array("userid"=>$userid));
		$data['fromdate']=$row['fromdate'];
		$data['todate']=$row['todate'];
		$data['reason']=$row['reason'];
		$data['designation']=$teacher_det['designation'];
		$data['datas']['leave_type']=$this->leave_type();
		$data['datas']['leave_mode']=$this->leave_mode();		
		$data['leave_type']=$row['leave_type'];
		$data['leave_mode']=$row['leave_mode'];
		$data['entry_date']=$row['entry_date'];
		$data['entry_by']=$row['entry_by'];
		$data['approval']=$row['approval'];
		$data['approval_date']=$row['approval_date'];
		$data['approval_by']=$row['approval_by'];
		$data['acyear']=$row['acyear'];
				$acyear=$row['acyear'];
            $leave_BY_designation=$this->leave_BY_designation($acyear);
            $data['total_days']=$leave_BY_designation[$teacher_det['designation']];				
            $data['leave_taken']=$this->leave_taken($row['userid'],$acyear);				
            if($data['total_days']>=$data['leave_taken'])
			{
			$data['available_days']=$leave_BY_designation[$teacher_det['designation']]-$data['leave_taken'];				
			}
			else
			{
			$data['available_days']=0;				
			}	
			 $data['no_of_days']=$this->no_of_days($row['fromdate'],$row['todate']);
		return $data;
}
}
?>