<?php
include_once('createdb.php');
include_once('../PHPMailer/class.phpmailer.php');
require_once('../PHPMailer/class.smtp.php');
include_once('notification.php');
require_once('Imap.php');
class Mail
{
	public function __construct($Username,$password)
	{
		$this->Username=$Username;
		$this->password=$password;
		
	}

    function host(){
       $host="mail.lcccecms.com";
       return $host;
    }
	function mail_from()
	{
		$from=$this->Username;
		return $from;
	}
	function User_name()
	{
		$username=$this->Username;
		return $username;
	}
	function password()
	{
		$password=$this->password;
		return $password;
	}
	function Sent_grp_mail($type,$email,$name,$to,$subject,$msg,$headers,$rsvp_mssg_id)
	{
		 $mail = new PHPMailer(true); //defaults to using php "mail()"; the true param means it will throw exceptions on errors, which we need to catch

		try {
			$mail->IsMail();
			$mail->CharSet = 'UTF-8';			
			$mail->Host= $this->host; // SMTP server example
			 // enables SMTP debug information (for testing)
			$mail->SMTPDebug  = 2;                 // set the SMTP port for the GMAIL server
			$mail->SMTPAuth   = true;   
			$mail->Username   = $this->User_name(); // SMTP account username example
			$mail->Password   =  $this->password();     // SMTP account password example
			$mail->From = $email;
			$mail->FromName = $name;      
			$mail->AddAddress($to, 'Information'); 
			$mail->AddReplyTo($email, 'Wale');
			$mail->SMTPDebug = false;
			$mail->Subject = $subject;
			$mail->addCustomHeader($headers);
			$mail->Body    =  $msg;
			$mail->IsHTML(true); 
			$mail->Send();
			return "Message Sent";
			} catch (phpmailerException $e) {
			return $e->errorMessage(); //Pretty error messages from PHPMailer
			} catch (Exception $e) {
			return $e->getMessage(); //Boring error messages from anything else!
		}
	}
	function mail_message($msg_id,$mtype,$title,$subject,$venue,$date,$location,$description,$messsage)
	{
		if($mtype=="Invitation")
		{
						$m=base64_encode( json_encode($msg_id));
					$ym=base64_encode( json_encode("yes"));
					$nm=base64_encode( json_encode("no"));
					$page_info=base64_encode( json_encode("rsvp"));
			$msg="<br><br><br>";
			$msg.="<div style='border-radius:5px;padding-left: 2cm;padding-right: 2cm;'>";
			$msg.="<table style='margin: auto;margin-left: 2cm;margin-right: 2cm;' width='100%'>";
			$msg.="<tr><td  colspan='2' style='text-align:center;color:red'>".strtoupper($title)."</td></tr>";
			$msg.="<tr><td  colspan='2' style='text-align:left'><table bgcolor='red' style='color:white;'><tr><td>".strtoupper($title)."</td></tr></table></td></tr>";
			$msg.="<tr><td  colspan='2' style='text-align:left;font-size:26px;color:red' >".strtoupper($subject)."</td></tr>";
			$msg.="<tr><td  colspan='2' style='text-align:left;font-size:18px;color:8c8c8c' >".strtoupper($venue)."</td></tr>";
			//$msg.="<tr><td  colspan='2' style='text-align:left;font-size:14px;' >Added by ".$added_by."</td></tr>";
			$msg.="<tr><td  colspan='2' style='border: .1px solid black;' ></td></tr>";
			$msg.="<tr><td style='text-align:left;font-size:14px;' >".date('l', strtotime($date)).",".date('F', strtotime($date))." ".date('d', strtotime($date)).",".date('Y', strtotime($date))."</td>";
			$msg.="<td rowspan='4' valign='top' align='right'>";
			$msg.="<table style='background-color:#8c8c8c;' width='120px' height='80px'>";
			$msg.="<tr><td>Are You Going ?</td></tr>";
			$msg.="<tr><td>";
			$msg.="<a href = 'http://jrboonsolutions.com/ecms/tick_status.php?tvalue=".$ym."&msg_id=".$m."&page_info=".$page_info."'  id='yes' style='font-size: 12px; padding:10px; color: #fff; background-color: #95b75d; border-color: #95b75d;'>Yes</a>&nbsp;&nbsp;";
			$msg.="<a href='http://jrboonsolutions.com/ecms/tick_status.php?tvalue=".$nm."&msg_id=".$m."&page_info=".$page_info."' name='no' id='no' style='background-color: #E04B4A; border-color: #E04B4A; color: #fff; font-size: 12px; padding:10px;'>No</a>";
			$msg.="</td></tr>";
			$msg.="</table>";
			$msg.="</td></tr>";
			$msg.="<tr><td colspan='2' style='text-align:left;font-size:14px;' height='80px' >".$location."<td></tr>";
			$msg.="<tr><td  colspan='2' style='border: .1px solid black;' ></td></tr>";
			$msg.="<tr><td  colspan='2' style='text-align:left' ><p align='justify'>";
			$length=strlen($description);
			if($length<=200)
			{
				$out=$description;
				$l_val=0;
			}
			else
			{
				$l_val=1;
				$out=substr($description,0,200)."..............";
			}
			$msg.=$out."</p></td></tr>";
			if($l_val==1)
			{
			$msg.="<tr><td  colspan='2' style='text-align:left;font-size:14px;' ><a href='http://jrboonsolutions.com/ecms/tick_status.php?msg_id=".$m."&page_info=".$page_info."'>LEARN MORE</a></td></tr>";		
			}
			$msg.="</table>";
			$msg.="</div>";	
		}
		elseif($mtype=="Message")
		{
			$msg.=$messsage;
		}	
        return 	$msg;	
	}
function user_login($msg_id)
{
	$dbobject = new DB();
$dbobject->getCon();
$group_message_det=$dbobject->selectall("group_message",array("id"=>$msg_id));
$user_login_dir=$group_message_det['utype'];
return "http://jrboonsolutions.com/ecms/$user_login_dir";
	
}
	function Sent_mail($type,$type_data,$subject,$message,$from_type,$to_type,$from_id,$to,$date,$attach)
	 {  
	     $dbobj = new DB();
         $dbobj->getCon();
		 $notifyobj = new notification();
		// $attachf=$this->upload($attach);
		 $msg_tocken=md5(time().'-'.mt_rand());
		 $ins_head=$dbobj->exe_qry("insert into `mail_head` (`tocken`,`type`,`type_data`) VALUES ('".$msg_tocken."','".$type."','".$type_data."')");
		 //print_r($attachf);exit;
		 $h=0;
		 if(!empty($to))
		 {
		  foreach($to as $i)
			{
					if($to_type=="parent")
					{
						$parent_det=$dbobj->selectall("parent",array("id"=>$i));
						$sent_mail_id=$parent_det['femail'];	
							//$to="aneesh.j4source@gmail.com";	
					}
					elseif($to_type=="student")
					{
						$parent_det=$dbobj->selectall("parent",array("id"=>$i));
						$sent_mail_id=$parent_det['femail'];	
							//$to="aneesh.j4source@gmail.com";	
					}		
					else
					{
						$teacher_det=$dbobj->selectall("teacher",array("id"=>$i));
						$sent_mail_id=$teacher_det['email'];
						
					}					
					$ins= $dbobj->exe_qry("insert into email_history(`subject`,`msg`,`from_type`,`to_type`,`from_id`,`to_id`,`date`,`attachment`,`tocken`,`sent_mail_id`) values('".$subject."','".$message."','".$from_type."','".$to_type[$h]."','".$from_id."','".$i."','".$date."','".$attachf['name']."','".$msg_tocken."','".$sent_mail_id."')");
					$msg_id=$dbobj->mysql_insert_id();
					$notify_id=$notifyobj->insert('email',$from_type,$from_id,$msg_id,$to_type[$h],$i);
					//$notify=$notifyobj->push_notification($notify_id,'message',$from_type,$from_id,$msg_id,$to_type[$h],$i);
					$h++;
			}
         }
		if($msg_id!="")
		{
         $val=1;
		}
		else
		{
		 $val=2;
		}
		return $val;
	 }
	function Sent_mail_message($from_type,$from_id,$to_type,$to_id,$subject,$msg)
	{
		
	    $dbobject = new DB();
        $dbobject->getCon();
		if($to_type=="parent")
		{
			$parent_det=$dbobject->selectall("parent",array("id"=>$to_id));
			$to=$parent_det['femail'];	
				//$to="aneesh.j4source@gmail.com";	
		}
		elseif($to_type=="student")
		{
			$parent_det=$dbobject->selectall("parent",array("id"=>$to_id));
			$to=$parent_det['femail'];	
				//$to="aneesh.j4source@gmail.com";	
		}		
		else
		{
		    $teacher_det=$dbobject->selectall("teacher",array("id"=>$to_id));
			$to=$teacher_det['email'];
			
		}
	
        $insert=$dbobject->exe_qry("INSERT INTO `message_history`( `from_type`, `to_type`, `from_id`, `to_id`, `msg`, `subject`, `date`,`sent_mail_id`) VALUES ( '".$from_type."', '".$to_type."','".$from_id."','".$to_id."','".$msg."', '".$subject."','".date('Y-m-d')."','".$to."')");		
		if($to!="")
		{
		// return $sent_mail=$this->email_sent_action($to,$subject,$msg);
		}
		else
		{
			return "No Emailid";
		}

	}	
function Sent_mail_by_mailid($from_type,$from_id,$to_type,$to_id,$subject,$msg,$to,$message_id)
{
 	    $dbobject = new DB();
		$dbobject->getCon();
		 $insert=$dbobject->exe_qry("INSERT INTO `email_history`( `from_type`, `to_type`, `from_id`, `to_id`, `msg`, `subject`, `date`,`sent_mail_id`) VALUES ( '".$from_type."', '".$to_type."','".$from_id."','".$to_id."','".$msg."', '".$subject."','".date('Y-m-d')."','".$to."')");	
 		if($to!="")
		{
		 return $sent_mail=$this->email_reply($to,$subject,$msg,$message_id);
		}
		else
		{
			return "No Emailid";
		}
}	
function email_sent_action($to_mail,$subject,$msg)
{
		$mail = new PHPMailer(true); // create a new object
		try {
                 $mail->SMTPAuth = true; // authentication enabled
                 $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
                 $mail->Host = $this->host();
                 $mail->Port = 465; 
                 $mail->Mailer = "smtp";
                 $mail->SMTPDebug='0';
                 $mail->isHTML(true);;
      			 $mail->Username   = $this->User_name(); // SMTP account username example
    			 $mail->Password   =  $this->password();     // SMTP account password example
                 $mail->From=$this->mail_from();
				 $mail->FromName="eCMS";
                 $mail->Subject = $subject;
                 $mail->Body = $msg;
                 $mail->AddAddress($to_mail, $to_mail);
                 $mail->Send();
                 //$this->save_mail($mail);
    			return "Message Sent";
    			} catch (phpmailerException $e) {
    			return $e->errorMessage(); //Pretty error messages from PHPMailer
    			} catch (Exception $e) {
    			return $e->getMessage(); //Boring error messages from anything else!
		}
 
					
}
function save_mail($mail){
    $s_username = $this->User_name();
	$s_password = $this->password();
	$imap=new Imap($s_username,$s_password);
    $s_host=$imap->get_host("INBOX.Sent");
	$mbox = imap_open($s_host,$s_username,$s_password,OP_HALFOPEN)
      or die("can't connect: ".imap_last_error());
       $result = imap_append($mbox, $s_host, $mail->getSentMIMEMessage());
      imap_close($mbox);
    return $result;
}
function email_reply($to_mail,$subject,$msg,$message_id){
    		$mail = new PHPMailer(true); // create a new object
		try {
				$mail->SMTPDebug='0';
                 $mail->SMTPAuth = true; // authentication enabled
                 $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
                 $mail->Host = $this->host();
                 $mail->Port = 465; 
                 $mail->Mailer = "smtp";
                 $mail->isHTML(true);;
      			 $mail->Username   = $this->User_name(); // SMTP account username example
    			 $mail->Password   =  $this->password();     // SMTP account password example
                 $mail->From=$this->mail_from();
                 $mail->FromName="eCMS";
                 $mail->addCustomHeader('In-Reply-To', $message_id);
                 $mail->Subject = $subject;
                 $mail->Body = $msg;
                 $mail->AddAddress($to_mail, $to_mail);
                 $mail->Send();
                // $this->save_mail($mail);
    			return "Message Sent";
    			} catch (phpmailerException $e) {
    			return $e->errorMessage(); //Pretty error messages from PHPMailer
    			} catch (Exception $e) {
    			return $e->getMessage(); //Boring error messages from anything else!
		}
}
function mailbox_sentitems($from_type,$from_id)
{
	$dbobject = new DB();
    $dbobject->getCon();
    $sql="SELECT * FROM `email_history` WHERE `from_type`='".$from_type."' and `from_id`='".$from_id."'";
	$qry=$dbobject->select($sql);
	$i=0;
	while($row=$dbobject->fetch_array($qry))
	{
		$data[$i]['from_type']=$row['from_type'];
		$data[$i]['from_id']=$row['from_id'];
		$data[$i]['to_type']=$row['to_type'];
		$data[$i]['to_id']=$row['to_id'];
		$data[$i]['msg']=$row['msg'];
		$data[$i]['sent_mail_id']=$row['sent_mail_id'];
		$data[$i]['subject']=$row['subject'];
		$data[$i]['date']=$row['date'];
		$data[$i]['id']=$row['id'];
		$i++;
	}
	return $data;				
}
function fetch_send_mail_by_distinct_subject_new_mail($id_from,$type)
	 {
      $dbobj = new DB();
      $dbobj->getCon();
//echo "select distinct `subject`,`date` from message_history where `from_id`='".$id_from."' and `from_type`='".$type."' order by `date`";	  
	  $ins= $dbobj->select("select distinct `subject`,`date` from email_history where `from_id`='".$id_from."' and `from_type`='".$type."' order by `date`");
	  $g=0;
	  while($row=$dbobj->fetch_array($ins))
	    {    
	        
			$sendmail[$g]['subject']=$row['subject'];
			$sendmail[$g]['date']=$row['date'];

			$g++;
	    }
	  return $sendmail;
	 }
function fetch_send_email_group($id_from,$type){
	$dbobj = new DB();
	$dbobj->getCon();
	$ins= $dbobj->select("SELECT DISTINCT `email_history`.`tocken`,`subject`,`date`,`mail_head`.`type`,`mail_head`.`type_data` from `email_history` INNER JOIN `mail_head` ON `email_history`.`tocken`=`mail_head`.`tocken` where `from_id`='".$id_from."' and `from_type`='".$type."' and `status`='0' order by `email_history`.`id` DESC");
	$g=0;
	while($row=$dbobj->fetch_array($ins))
	  {  
		  $sendmail[$g]['type']=$row['type'];
		  $sendmail[$g]['type_data']=$row['type_data'];  
		  $sendmail[$g]['subject']=$row['subject'];
		  $sendmail[$g]['tocken']=$row['tocken'];
		  $sendmail[$g]['date']=$row['date'];
		  //$sendmail[$g]['num_count']=$row['num_count'];
		  $g++;
	  }
	return $sendmail;
}	
function get_position_by_id($tid)
{
   $dbobject = new DB();
   $dbobject->getCon();
   //$acyear=$dbobject->get_acyear();
   $sql=$dbobject->select("select * from `teacher` where `userid`='".$tid."'");
   $thr= $dbobject->fetch_array($sql);
   $teacher=$thr['group'];
   return $teacher;
} 
function fetch_mail_teacher_details($msg_id)
{
 $dbobj = new DB();
 $tobj = new Teacher();
 $dbobj->getCon();	 
 $fetch= $dbobj->select("select `tocken`,`subject`,`date`,`msg`, COUNT(`tocken`) as num_count from `email_history` where `tocken`='".$msg_id."'");
 $fth= $dbobj->fetch_array($fetch);
 $details['id']=$fth['id'];
 $details['subject']=$fth['subject'];
 $details['body']=$fth['msg'];
 $details['date']=$fth['date'];
 $details['num_count']=$fth['num_count'];
 $details['attachment']=$fth['attachment'];
 $user_details=$dbobj->user_details($fth['to_type'],$fth['to_id']);
 $details['name']=$user_details['name'];
 return $details;
}
function fetch_send_Email_groupDel($id_from,$type){
	$dbobj = new DB();
	$dbobj->getCon();
	$ins= $dbobj->select("SELECT DISTINCT `tocken`,`subject`,`date` from `email_history` where `from_id`='".$id_from."' and `from_type`='".$type."' and `status`='1' order by `id` DESC");
	$g=0;
	while($row=$dbobj->fetch_array($ins))
	  {    
		  $sendmail[$g]['subject']=$row['subject'];
		  $sendmail[$g]['tocken']=$row['tocken'];
		  $sendmail[$g]['date']=$row['date'];
		  //$sendmail[$g]['num_count']=$row['num_count'];
		  $g++;
	  }
	return $sendmail;
 }
 function fetch_draft_message($id_from,$type)
 {
  $dbobj = new DB();
  $dbobj->getCon();
  $ins= $dbobj->select("select * from `message_draft` where `userid`='".$id_from."' and `draft_type`='email' order by `id` DESC");
  $g=0;
  while($row=$dbobj->fetch_array($ins))
	{    
		$sendmail[$g]['id']=$row['id'];
		$sendmail[$g]['tocken']=$row['tocken'];
		$sendmail[$g]['subject']=$row['subject'];
		$sendmail[$g]['body']=$row['body'];
		$g++;
	}
  return $sendmail;
 }
 function no_of_mail_sent($id_from,$type)
	 {
	  $dbobj = new DB();
      $dbobj->getCon();
	  $g=0;
	  $ins= $dbobj->select("select DISTINCT `tocken`  from `email_history` where  `from_id`='".$id_from."' and `from_type`='".$type."' and `status`='0' group by `tocken`");
	  $g=$dbobj->mysql_num_row($ins);
	  return $g;
	 }
	 function no_of_mail_draft($id_from)
	 {
	  $dbobj = new DB();
      $dbobj->getCon();
	  $g=0;
	  $ins= $dbobj->select("select COUNT(`id`) as `num_count`  from `message_draft` where  `userid`='".$id_from."' and `draft_type`='email'");
	  $g=$dbobj->fetch_array($ins);
	  return $g['num_count'];
	 }
	 function no_of_mail_Delete($id_from,$type_to)
	 {
	  $dbobj = new DB();
      $dbobj->getCon();
	  $g=0;
	  $ins= $dbobj->select("select DISTINCT `tocken` from `email_history` where  `from_id`='".$id_from."' and `from_type`='".$type_to."' and `status`='1'");
	  $g=$dbobj->mysql_num_row($ins);
	  return $g;
	 }	
	 function fetch_message_teacher_details($msg_id)
	 {
	  $dbobj = new DB();
	  $tobj = new Teacher();
      $dbobj->getCon();	 
	  echo "select `tocken`,`subject`,`date`,`msg`, COUNT(`tocken`) as num_count from email_history where `tocken`='".$msg_id."'";
	  $fetch= $dbobj->select("select `tocken`,`subject`,`date`,`msg`, COUNT(`tocken`) as num_count from email_history where `id`='".$msg_id."'");
	  $fth= $dbobj->fetch_array($fetch);
	  $details['id']=$fth['id'];
	  $details['subject']=$fth['subject'];
	  $details['body']=$fth['msg'];
	  $details['date']=$fth['date'];
	  $details['num_count']=$fth['num_count'];
	  $details['attachment']=$fth['attachment'];
	  $user_details=$dbobj->user_details($fth['to_type'],$fth['to_id']);
	  $details['name']=$user_details['name'];
	  return $details;
	 }	
}
?>