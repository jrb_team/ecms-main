<?php
include_once('createdb.php');
class MerchantWarrior
{

   public $MW_settings=array();
   public function __construct()
   {
       $object_mw=new DB();
       $object_mw->getCon();
       $this->poli_settings=$object_mw->selectall("pgpayment",array("pg_name"=>"MW"));
    }
   function BaseURL(){
      $url='https://sandbox.rest.paymentsapi.io/';
      return $url;
   }
   function API_Base(){
    return $url="https://base.merchantwarrior.com/post/";
   }
   function Username(){
       $username="2248.1564";
       return $username;
   }
   function Password(){
       $password="dc4a7b75-9899-4e6d-95d6-946445591586";
       return $password;
   }
   function MerchantUUID(){
       $MerchantUUID=$this->poli_settings['MerchantKey'];
       return $MerchantUUID;
   }
   function APIKey(){
       $APIKey=$this->poli_settings['Salt'];
       return $APIKey;
   }
   function APIPassphrase(){
       return $APIPassphrase=$this->poli_settings['api_passphrase'];
   }
   function HostedUrlCreditcard(){
    $url='https://securetest.merchantwarrior.com/';
    return $url;
   }
   function HostedUrlDDR(){
    $url='https://securetest.merchantwarrior.com/';
    return $url;
   }
   function method($type){
       if($type=="card"){
        $method="processCard";
       }
       else{
        $method="processDDebitAuth";
       }
        return $method;
    }
    function DDPaymentRes(){
        $url="http://paytracker.com.au/DDPaymentAction.php";
        return $url;
    }
   function currency(){
       return "AUD";
   }
   function notifyURL(){
    $url="http://paytracker.com.au/MWnotifyURL.php";
    return $url;
    }
    function CreditCardPaymentRes(){
        $url="http://paytracker.com.au/MWAction.php";
        return $url;
    }
    function urlHash($type){
        
        $url_hash=md5($this->APIPassphrase()).$this->merchantUUID();
        if($type=="creditcard"){
            $url_hash.=$this->CreditCardPaymentRes();
        }elseif($type=="DDR"){
            $url_hash.=$this->DDPaymentRes();
        }
        $url_hash.=$this->notifyURL();
        $url_hash=strtolower($url_hash);
        $url_hash=md5($url_hash);
        return $url_hash;
    }
    function Transactionhash($rate){
        $hash=md5($this->APIPassphrase()).$this->merchantUUID().$rate.$this->currency();
        $hash=strtolower($hash);
        $hash=md5($hash);
        return $hash;
    }
   function reference(){
    $reference = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
    return $reference;
   }
   function GRPreference(){
       $GRPreference="HALLAM";
       return $GRPreference;
   }
   function auth(){
    $auth="eyJhbGciOiJSUzI1NiIsImtpZCI6IjM4NkM3N0Q2NjNDMENFNENGRDIxNEQxNEIxMTAxM0YyRkVFMTU0NDJSUzI1NiIsInR5cCI6ImF0K2p3dCIsIng1dCI6Ik9HeDMxbVBBemt6OUlVMFVzUkFUOHY3aFZFSSJ9.eyJuYmYiOjE2MjIwOTE2NDAsImV4cCI6MTYyMjA5NTI0MCwiaXNzIjoiaHR0cHM6Ly9sb2NhbGhvc3QucHltbnRzLmNvbS5hdTo4MDAxIiwiYXVkIjoiaW50ZWdyYXBheS5hcGkucHVibGljIiwiY2xpZW50X2lkIjoiaW50ZWdyYXBheS5hcGkucHVibGljLmNsaWVudCIsInN1YiI6IjIyNDguMTU2NCIsImF1dGhfdGltZSI6MTYyMjA5MTY0MCwiaWRwIjoibG9jYWwiLCJlbWFpbCI6ImNyYWlnLmNvbGxpbnNAaW50ZWdyYXBheS5jb20iLCJCdXNpbmVzc0lEIjoiMjI0OCIsIkJ1c2luZXNzTmFtZSI6IkpSIEJvb24gU29sdXRpb25zIFRlc3QiLCJCdXNpbmVzc1R5cGVJRCI6IjMiLCJVc2VybmFtZSI6IjIyNDguMTU2NCIsInJvbGUiOlsiQlBheS5Bc3NpZ24iLCJCUGF5LlNlYXJjaCIsIkJ1c2luZXNzLlN0YXR1cyIsIkNvbmZpZy5DYXJkQmlucyIsIlBheWVyLkFjY291bnQuRGVsZXRlIiwiUGF5ZXIuQWNjb3VudC5TYXZlLkJhbmsiLCJQYXllci5BY2NvdW50LlNhdmUuQlBheSIsIlBheWVyLkFjY291bnQuU2F2ZS5DYXJkLlRva2VuIiwiUGF5ZXIuQWNjb3VudC5WaWV3IiwiUGF5ZXIuQWRkIiwiUGF5ZXIuU2F2ZSIsIlBheWVyLlNhdmUuU3RhdHVzIiwiUGF5ZXIuU2NoZWR1bGluZy5TYXZlIiwiUGF5ZXIuU2NoZWR1bGluZy5WaWV3IiwiUGF5ZXIuU2VhcmNoIiwiUGF5ZXIuVmlldyIsIlJlcG9ydHMuU2V0dGxlbWVudCIsIlNjaGVkdWxpbmcuU2VhcmNoIiwiVG9rZW5zLkdlbmVyYXRlIiwiVG9rZW5zLlZpZXciLCJUcmFuc2FjdGlvbnMuUHJvY2Vzcy5DYXJkLlJlZnVuZCIsIlRyYW5zYWN0aW9ucy5Qcm9jZXNzLkNhcmQuU3RvcmVkIiwiVHJhbnNhY3Rpb25zLlByb2Nlc3MuQ2FyZC5Ub2tlbiIsIlRyYW5zYWN0aW9ucy5TZWFyY2giLCJUcmFuc2FjdGlvbnMuU2VhcmNoLk5ld1N0YXR1cyIsIlRyYW5zYWN0aW9ucy5WaWV3Il0sImp0aSI6IkY2OUU0NkM4MjYzQTk2MzFEMTdBRDgwQUU4REZGNzY0IiwiaWF0IjoxNjIyMDkxNjQwLCJzY29wZSI6WyJpbnRlZ3JhcGF5LmFwaS5wdWJsaWMiXSwiYW1yIjpbIkF1dGhlbnRpY2F0ZWQiXX0.fcBpsyUBBYwU70tJ4oN6V1izkraYHR0DRJIpYnZQeJCF-RlIMN6epsOpZFFNYtL6EMbuDlHUfEEJcn_Ast4pFSA2BPrpV_ueStgYI_BqcxGVb1D-x_Maf9d7aeHAWSeLlIIWGeOILR-hentp3aQgy_ExymtNbBUIvJiERYNvZS2_9yYJhGnYFAfzDNHgb7RFu9qEFxPH66Xq4raN6Up6iHZe6hn2OD2kQ1r7GM-ShMjIuk2YqZYvjzE2kIO_ZM9VapaXR1WrTRbvcklEI3ndhf3TExR0tWdlXhflDhXZavt9buT1I_W3DnwWmCQ6V5D3I_hcWEd6MBPSR0WXvzZgaw";
    return $auth;
    }
   function SystemIp(){
       return "192.168.1.1";
   }
   function Schedulesinglepayment($feename,$acyear,$sid,$ref,$duedate,$amount,$classno,$parentid){
    $obj=new DB();
    $obj->getCon();
    $username=$this->username();
    $SystemIp=$this->SystemIp();
    $businessId=$this->businessId();
    $auth=$this->auth();
    $URL=$this->BaseURL()."businesses/".$businessId."/payers/".$ref."/schedules/payments";
    $json_builder = '{
        "Date": "'.$duedate.'",
        "Amount": '.$amount.',
        "Reference": "'.$feename.'-'.$acyear.'-'.$sid.'",
        "Description": "'.$feename.'",
        "SubBusinessId": null,
        "Audit": {
        "Username": "'.$username.'",
        "UserIP": "'.$SystemIp.'"
        }
    }';
    $header = array();
    $header[] = 'Content-Type: application/json';
    $header[] = 'Authorization: Bearer '.$auth;
    $ch = curl_init($URL);
    
  curl_setopt( $ch, CURLOPT_HTTPHEADER, $header);
  curl_setopt( $ch, CURLOPT_HEADER, 0);
  curl_setopt( $ch, CURLOPT_POST, 1);
  curl_setopt( $ch, CURLOPT_POSTFIELDS, $json_builder);
  curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 0);
  curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
  $response = curl_exec( $ch );
  if($response === false)
  {
      echo 'Curl error: ' . curl_error($ch);
  }
  else
  {
   
   $response_array=json_decode($response, true);
   if(!isset($response_array['errorCode'])){
   $sel=$obj->select("SELECT `amount` FROM `student_academic_fee` WHERE `sid`='".$sid."' AND `fee_name`='".$feename."' AND `ac_year`='".$acyear."'");
   $num=$obj->mysql_num_row($sel);
   if($num==0){
       $obj->exe_qry("INSERT INTO `student_academic_fee` (`sid`,`fee_name`,`amount`,`pgScheduleId`,`ac_year`,`due_date`,`classid`,`parent_id`) VALUES ('".$sid."','".$feename."','".$amount."','".$response_array['scheduledPaymentId']."','".$acyear."','".$duedate."','".$classno."','".$parentid."')");
   }
  }
  else {
      echo $response;
  }
   }
   }
   function number_format($amount){
    return number_format((float)$amount, 2, '.', ''); 
   }
}
?>