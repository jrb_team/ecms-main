<?php
class Colour
{
	public function __construct($subject)
	{
		$this->subject=$subject;
		
	}
	function sub_table()
	{
		$subject=$this->subject;
		if($subject=="Moral Science")
		{
			$colour="#F7FE2E";
		}
		elseif($subject=="English" || $subject=="English I")
		{
			$colour="#0040FF";
		}
		elseif($subject=="English II")
		{
			$colour="#8000FF";
		}
		elseif($subject=="Malayalam")
		{
			$colour="#088A85";
		}
		elseif($subject=="EVS")
		{
			$colour="#DF013A";
		}
		elseif($subject=="Computer" || $subject=="Computer Appln.")
		{
			$colour="#DA81F5";
		}
		elseif($subject=="G.K")
		{
			$colour="#CEECF5";
		}
		elseif($subject=="Hindi")
		{
			$colour="#A9F5A9";
		}
		elseif($subject=="Social Studies" || $subject=="S.Studies")
		{
			$colour="#F5A9BC";
		}
		elseif($subject=="Mathematics")
		{
			$colour="#FAAC58";
		}
		elseif($subject=="Mathematics")
		{
			$colour="#9F81F7";
		}
		elseif($subject=="Science " || $subject=="Science")
		{
			$colour="#F6CEF5";
		}
		elseif($subject=="Physics")
		{
			$colour="#F781BE";
		}
		elseif($subject=="Chemistry")
		{
			$colour="#82FA58";
		}
		elseif($subject=="Biology")
		{
			$colour="#B43104";
		}
		elseif($subject=="Geography")
		{
			$colour="#0080FF";
		}
		elseif($subject=="History" || $subject=="History & Civics")
		{
			$colour="#A9A9F5";
		}
		elseif($subject=="Aquatic (Boys)" || $subject=="Aquatic (Girls)")
		{
			$colour="#045FB4";
		}
		elseif($subject=="Spoken English")
		{
			$colour="#FA5882";
		}
		elseif($subject=="Library")
		{
			$colour="#F78181";
		}
		elseif($subject=="PT")
		{
			$colour="#CC2EFA";
		}
		elseif($subject=="Sahithya Samajam")
		{
			$colour="#D8CEF6";
		}
		elseif($subject=="Computer Lab")
		{
			$colour="#F6CED8";
		}
		elseif($subject=="Commerce")
		{
			$colour="#F7819F";
		}
		elseif($subject=="Economics")
		{
			$colour="#9F81F7";
		}
		elseif($subject=="Accountancy")
		{
			$colour="#ACFA58";
		}
		elseif($subject=="Zoology")
		{
			$colour="#81F79F";
		}
		return $colour;
	}
}
?>